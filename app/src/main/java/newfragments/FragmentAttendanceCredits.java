package newfragments;


import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Utilities.Attendancedb;
import Utilities.DatabaseHandler;
import Utilities.Utilss;
import books.Creditsadapter;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.RoundedImageView;
import helper.Studentdetails;

import static android.content.Context.MODE_PRIVATE;

public class FragmentAttendanceCredits extends Fragment {
    RoundedImageView profileimage;
    TextView batchname,attendance;
    ListView studentlist;
    Fragment fragment;
    EditText search;
    TextView searchheading;
    TextView studentphoto;
    TextView studentrollno;
    TextView name;
    TextView totalcredits;
    TextView obtained;
    TextView month;
    TextView tildate;
    ImageView handraise;
    ListView attendancreditslist;
    ArrayList<Studentdetails> student;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    DatabaseHandler dp;
    Attendancedb db;
    int teachercredits;
    int thismonthcredits;
    ImageView back;
    Creditsadapter adapter;
    ImageView dnd;
    ArrayList<Studentdetails> student1;
    Utilss utils;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_attend_credit,
                container, false);
        pref=getActivity().getSharedPreferences("Teacher",MODE_PRIVATE);
        edit = pref.edit();
        dp=new DatabaseHandler(getActivity(),pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
        db=new Attendancedb(getActivity(),pref.getString("attendancedb",""),Attendancedb.DATABASE_VERSION);
        profileimage= (RoundedImageView) rootView.findViewById(R.id.profileimage);
        batchname= (TextView) rootView.findViewById(R.id.textView);
        attendance= (TextView) rootView.findViewById(R.id.classname);
        searchheading= (TextView) rootView.findViewById(R.id.creditsubject);
        search= (EditText) rootView.findViewById(R.id.creditsearch);
        studentphoto= (TextView) rootView.findViewById(R.id.studentpoto);
        name= (TextView) rootView.findViewById(R.id.name);
        back= (ImageView) rootView.findViewById(R.id.back);
        studentrollno= (TextView) rootView.findViewById(R.id.rollno);
        month= (TextView) rootView.findViewById(R.id.thismonth);
        tildate= (TextView) rootView.findViewById(R.id.tildate);
        totalcredits= (TextView) rootView.findViewById(R.id.totalcredits);
        obtained= (TextView) rootView.findViewById(R.id.obtainedcredits);
        studentlist= (ListView) rootView.findViewById(R.id.creidtlist);
        handraise= (ImageView) rootView.findViewById(R.id.handrise);


        setbackground(profileimage, pref.getString("image", ""));
        batchname.setText(pref.getString("classname", ""));
        searchheading.setText(pref.getString("subjectname", ""));

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<Studentdetails> searchstring = new ArrayList<Studentdetails>();
                if (student.size() > 0) {
                    for (int i = 0; i < student.size(); i++) {
                        if (student.get(i).getFirstName().toLowerCase().contains(s.toString().toLowerCase())) {
                            searchstring.add(student.get(i));
                        }
                    }
                    if (searchstring.size() > 0) {
                        adapter = new Creditsadapter(getActivity(), R.layout.creditsitem, searchstring, pref);
                        attendancreditslist.setAdapter(adapter);
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        utils=new Utilss(getActivity());

        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(getActivity());
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(getActivity(), getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        dnd= (ImageView) rootView.findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getBoolean("dnd",false))
                {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata=senddata+"@false";
                    dnd.setImageResource(R.drawable.inactive);


                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                }
                else
                {
                    String senddata = "DND";

                    senddata=senddata+"@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    dnd.setImageResource(R.drawable.active);

                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
            }
        });

        attendancreditslist= (ListView) rootView.findViewById(R.id.creidtlist);


        teachercredits=db.gettotalcreditsforsubject(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")));

        thismonthcredits=db.gettotalcreditsforsubjectthismonth(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")), Integer.parseInt(currentmonth()), Integer.parseInt(getyear()));
        //  student= dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")),  Integer.parseInt(pref.getString("staffid", "0")));
        student1=dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")),  Integer.parseInt(pref.getString("staffid", "0")));

        student=new ArrayList<>();
        for(int j=0; j<student1.size(); j++)
        {
            Studentdetails studentdetails=student1.get(j);

            boolean addtostudent=false;
            String rooms[]=studentdetails.getRoominfo().split(",");

          /*  for(int r=0; r<rooms.length; r++)
            {
                if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                {
                    addtostudent=true;
                    break;
                }
            }
            if(addtostudent)
            {*/
            student.add(studentdetails);
            // }


        }


        for(int i=0; i<student.size(); i++)
        {

            student.get(i).setTeachercredits(teachercredits);
            int studentcredits=db.gettotalcreditsforsubjectstudent(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")),student.get(i).getStudentID());

            int thisstudentcredits=db.gettotalcreditsforsubjectstudentthismonth(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")), student.get(i).getStudentID(), Integer.parseInt(currentmonth()), Integer.parseInt(getyear()));
            student.get(i).setStudentcredits(studentcredits);
            student.get(i).setThismonthteachercredit(thismonthcredits);
            student.get(i).setThismonthcreditforstudent(thisstudentcredits);

        }
        adapter=new Creditsadapter(getActivity(),R.layout.creditsitem,student,pref);
        attendancreditslist.setAdapter(adapter);
        utils.setTextviewtypeface(3,studentrollno);
        utils.setTextviewtypeface(3,studentphoto);
        utils.setTextviewtypeface(3,name);
        utils.setTextviewtypeface(3,totalcredits);
        utils.setTextviewtypeface(3,obtained);
        utils.setTextviewtypeface(3,tildate);
        utils.setTextviewtypeface(3,month);
        utils.setTextviewtypeface(5,attendance);
        utils.setTextviewtypeface(3,batchname);
        utils.setTextviewtypeface(5,searchheading);
        return rootView;
    }
    public static String currentmonth()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());


        SimpleDateFormat df = new SimpleDateFormat("MM");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getyear()
    {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());


        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }
    /*DateFormat dateFormat = new SimpleDateFormat("MM");
    Date date = new Date();*/
    //2017-05-15 06:51:40
    public static  String currenttime()

    {
        DateFormat df = new SimpleDateFormat("hh:mm a");
        String date = df.format(Calendar.getInstance().getTime());
        return date;
    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

}
