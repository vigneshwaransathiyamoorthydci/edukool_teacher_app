package newfragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Utilities.DatabaseHandler;
import Utilities.Service;
import Utilities.Url;
import Utilities.Utilss;
import exam.ExamDetails;
import exam.ExamResponsePojo;
import exam.ExamTeacherHome;
import exam.QuestionDetails;
import exam.QuizWrapper;
import exam.ReceiveandSendMarkActivity;
import exam.ViewQuestionListActivity;
import helper.Studentdetails;

import static android.content.Context.MODE_PRIVATE;

public class FragmentAssessment extends Fragment {
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    //int QuestionID;
    String ExamDescription;
    String BatchIDVal ;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    int Mark;
    String SubjectIDVal;
    String Subject ;
    int ObtainedMark ;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal ;
    int NegativeMark;
    int IsCorrect;
    String MarkForAnswer ;
    String ExamType ;
    String ExamDate ;
    int AspectID;
    int ExamTypeID;
    int TopicID;
    String TopicIDVal,Topic,AspectIDVal,Aspect,QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    String ExamIDVal;
    int QuestionNumber;
    String question;


    ImageView download_exams,back,worst_perfor,pie_chart,receive_and_send_ans,view_questions_list;
    private Dialog dialog;
    private Button closeButton;
    TextView classstaff,transrecei;
    SharedPreferences.Editor edit;
    int classidVal;
    int batchidVal;
    int staffidVal;
    BroadcastReceiver questionreceiver;
    SharedPreferences pref;
    ImageView profilImageView;
    String storagepath;

    DatabaseHandler db;
    ArrayList<Integer> newAL;
    String classid;
    String batchid;
    int ExamIDRes;
    int QuestionID;
    int ExamResponseID;
    int StudentResponseID;
    Utilss utils;
    ArrayList<NameValuePair> downloadexam = new ArrayList<NameValuePair>();
    ProgressDialog dia;
    String staffid;
    String portalstaffid;
    String MY_PREFS_NAME = "EXAMDETAILS";
    int count;
    TextView title,tv_english,tv_maths,tv_science,tv_tamil,tv_soscience;
    Fragment fragment;
    public void setFragment(Fragment fragments) {
        fragment = fragments;
        if (fragment != null) {

            android.support.v4.app.FragmentManager fragmentManager = getChildFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, "About Us");


            fragmentTransaction.commit();
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_assesment,
                container, false);
        download_exams = (ImageView)rootView.findViewById(R.id.download_exams);
        back= (ImageView)rootView.findViewById(R.id.back);
        pref=getActivity().getSharedPreferences("Teacher", MODE_PRIVATE);
        edit=pref.edit();
        title=(TextView) rootView.findViewById(R.id.title);
        title.setText("Assessment");
        tv_english=(TextView) rootView.findViewById(R.id.tv_english);
        tv_maths=(TextView) rootView.findViewById(R.id.tv_maths);
        tv_science=(TextView) rootView.findViewById(R.id.tv_science);
        tv_tamil=(TextView) rootView.findViewById(R.id.tv_tamil);
        tv_soscience=(TextView) rootView.findViewById(R.id.tv_soscience);







        db = new DatabaseHandler(getActivity(),pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
        worst_perfor = (ImageView)rootView.findViewById(R.id.worst_perfor);
        pie_chart = (ImageView)rootView.findViewById(R.id.pie_chart);
        transrecei= (TextView)rootView.findViewById(R.id.transrecei);
        profilImageView= (ImageView) rootView.findViewById(R.id.profileimage);
        back= (ImageView) rootView.findViewById(R.id.back);
        receive_and_send_ans = (ImageView)rootView.findViewById(R.id.receive_and_send_ans);
        view_questions_list = (ImageView)rootView.findViewById(R.id.view_questions_list);
        classstaff= (TextView)rootView.findViewById(R.id.classstaff);
        classstaff.setText(pref.getString("classname", ""));
       // setbackground(profilImageView, pref.getString("image", ""));
        classid = (pref.getString("bookbin", ""));
        batchid = (pref.getString("batchid", ""));
        staffid= (pref.getString("staffid", ""));
        portalstaffid= (pref.getString("portalstaffid", ""));
        classidVal = Integer.parseInt(classid);
        batchidVal = Integer.parseInt(batchid);
        staffidVal = Integer.parseInt(staffid);
        System.out.println(classid+"classid"+batchid+"batchid"+staffid+"staffid");
        List<Studentdetails> getdetails = db.getdetails(classidVal,staffidVal);
        utils=new Utilss(getActivity());
        utils.setTextviewtypeface(3,title);
        utils.setTextviewtypeface(5,tv_english);
        utils.setTextviewtypeface(5,tv_maths);
        utils.setTextviewtypeface(5,tv_science);
        utils.setTextviewtypeface(5,tv_tamil);
        utils.setTextviewtypeface(5,tv_soscience);

        List<ExamResponsePojo> examResponsePojosh = db.getAllExamResponse();

        for (ExamResponsePojo cn : examResponsePojosh) {
            String log = "ExamID: " + cn.getExamID() + " ,ExamResponseID: " + cn.getExamResponseID();
            // Writing Contacts to log
            ExamIDRes =cn.getExamID();
            //QuestionID =  cn.
            ExamResponseID = cn.getExamResponseID();
            // StudentResponseID =
        }

        transrecei.setText("0"+"/"+getdetails.size());
        download_exams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String login_str="UserName:"+portalstaffid+"|Function:DownloadExam"+"|StaffId:"+staffid;
                // String login_str="";
                downloadexam.clear();
                byte[] data ;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if(utils.hasConnection()) {
                        downloadexam.clear();
                        downloadexam.add(new BasicNameValuePair("WS", base64_register));

                        Load_Download_WS load_plan_list = new Load_Download_WS(getActivity(), downloadexam);
                        load_plan_list.execute();
                    }
                    else
                    {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                // stratExamPopup();
                //  Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

//        worst_perfor.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
//            }
//        });
//
//        pie_chart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
//            }
//        });

        receive_and_send_ans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ReceiveandSendMarkActivity.class);
                startActivity(intent);
            }
        });

        view_questions_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExamTeacherHome)getActivity()).setFragment(new FragmentViewQuestionList());
                //setFragment(new FragmentViewQuestionList());

//
//                Intent intent = new Intent(getActivity(), ViewQuestionListActivity.class);
//                startActivity(intent);
            }
        });
        setSubjectbackround();
        return rootView;
    }
    public void setSubjectbackround(){
        if (pref.getString("subjectname","")!=null&&!pref.getString("subjectname","").equals("")){
            String subname=pref.getString("subjectname","").toLowerCase();

            switch (subname){
                case "english":
                    tv_english.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_english.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "maths":
                    tv_maths.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_maths.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "tamil":
                    tv_tamil.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_tamil.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "sceince":
                    tv_science.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_science.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "social science":
                    tv_soscience.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_soscience.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        }

    }



    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        float curBrightnessValue = 0;
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//        try {
//            curBrightnessValue = Settings.System.getInt(
//                    getContentResolver(),
//                    Settings.System.SCREEN_BRIGHTNESS);
//        } catch (Settings.SettingNotFoundException e) {
//            e.printStackTrace();
//        }
//        float brightness = curBrightnessValue / (float)255;
//        WindowManager.LayoutParams lp = getWindow().getAttributes();
//        lp.screenBrightness = brightness;
//        getWindow().setAttributes(lp);
//        newAL= new ArrayList<Integer>();
//
//        questionreceiver=new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                try {
//                    Bundle bundle = intent.getExtras();
//                    String Receive = bundle.getString("Receive");
//
//                    String[] separated = Receive.split("@");
//                    String ReceiveStr =  separated[0].trim(); // this will contain "Fruit"
//                    String studentID = separated[1].trim(); // this will contain " they taste good"
//                    String StudentRoll = separated[2].trim();
//                    String file =  separated[3].trim();
//                    String ID =  separated[4].trim();
//                    SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
//                    int examidPref = prefs.getInt("EXAMID", 0);
//                    int studentcount = prefs.getInt("studentcount", 0);
//                    //newAL.add(count++);
//                    System.out.println(ReceiveStr+"ReceiveStr");
//                    System.out.println(studentID+"studentID");
//                    System.out.println(StudentRoll+"StudentRoll");
//                    System.out.println(file+"file");
//                    System.out.println(ID+"ID");
//                    List<Studentdetails> getdetails = db.getdetails(classidVal,staffidVal);
//                    String test= examidPref+"";
//                    if(test.equalsIgnoreCase(ID)){
//                        if(studentcount==getdetails.size()){
//
//                            Toast.makeText(getApplicationContext(),"All Students Received Questions", Toast.LENGTH_LONG).show();
//                        }
//                        else{
//                            transrecei.setText(studentcount+"/"+getdetails.size());
//                        }
//                    }
//                    // loadQuestions();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        };
//        IntentFilter intent=new IntentFilter("StudentCount");
//        registerReceiver(questionreceiver, intent);
//    }



//    @Override
//    protected void onPause() {
//        super.onPause();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(questionreceiver);
//
//
//    }

    public void stratExamPopup() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.download_popup);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        closeButton = (Button) dialog.findViewById(R.id.button2);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }



    class Load_Download_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public Load_Download_WS(Context context_ws,
                                ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(getActivity());
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(getActivity());
                jsonResponseString = sr.getLogin(downloadexam, Url.baseurl
                        /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(final String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            Log.e("jsonResponse", "Login" + jsonResponse);
            //dialog.dismiss();
            //  Toast.makeText(getApplicationContext(),jsonResponse,Toast.LENGTH_LONG).show();
            try {



                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success")) {
                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(getActivity());
                            dia.setMessage("DOWNLOADING");

                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {




                                Log.e("examdetails,","details");

                                JSONArray Exam_arr=jObj.getJSONObject("examDetails").getJSONArray("exam");

                                for (int i = 0; i < Exam_arr.length(); i++) {
                                    JSONObject objexam=Exam_arr.getJSONObject(i);
                                    ExamIDVal = objexam.getString("ExamID");
                                    ExamID = Integer.parseInt(ExamIDVal);
                                    ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                                    ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                                    ExamCategory = objexam.getString("ExamCategory");
                                    ExamCode = objexam.getString("ExamCode");
                                    ExamDescription = objexam.getString("ExamDescription");
                                    BatchIDVal = objexam.getString("BatchID");
                                    ExamDurationVal = objexam.getString("ExamDuration");
                                    BatchID = Integer.parseInt(BatchIDVal);
                                    ExamDuration = Integer.parseInt(ExamDurationVal);

                                    SubjectIDVal = objexam.getString("SubjectID");
                                    Subject = objexam.getString("Subject");

                                    SubjectID = Integer.parseInt(SubjectIDVal);
                                    ExamTypeIDVal = objexam.getString("ExamTypeID");

                                    ExamType = objexam.getString("ExamType");

                                    // ExamDate = objexam.getString("ExamDate");

                                    ExamTypeID = Integer.parseInt(ExamTypeIDVal);

                                    int ExamSequence =0;
                                    int SchoolID =0;
                                    int IsResultPublished =0;
                                    int ExamShelfID =0;
                                    int ClassID =0;
                                    int TimeTaken = 0;
                                    int TotalScore =0;
                                    String DateAttended ="01/06/2017 10:00:00";

                                    boolean isExamIdexist = db.CheckIsIDAlreadyInDBorNot(ExamIDVal);
                                    if(isExamIdexist) {
                                        db.downloadUpdateExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));
                                    }

                                    else{
                                        db.addExamDetails(new ExamDetails(ExamID,staffid, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));
                                    }
                                    List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

                                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                                    QuizWrapper newItemObject = null;

                                    for(int j = 0; j < jsonArrayquestion.length(); j++){
                                        JSONObject jsonChildNode = null;
                                        try {
                                            jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                            String QuestionIDval = jsonChildNode.getString("QuestionID");
                                            QuestionID = Integer.parseInt(QuestionIDval);
                                            TopicIDVal = jsonChildNode.getString("TopicID");
                                            if(TopicIDVal.equalsIgnoreCase("")){
                                                TopicID=1;
                                            }
                                            else{
                                                TopicID = Integer.parseInt(TopicIDVal);

                                            }

                                            // int TopicID=1;
                                            Topic = jsonChildNode.getString("Topic");
                                            AspectIDVal = jsonChildNode.getString("AspectID");
                                            if(AspectIDVal.equalsIgnoreCase("")){
                                                AspectID=1;
                                            }
                                            else{
                                                AspectID = Integer.parseInt(AspectIDVal);

                                            }
                                            Aspect = jsonChildNode.getString("Aspect");
                                            QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                            QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                            question= jsonChildNode.getString("Question");


                                            JSONArray options = jsonChildNode.getJSONArray("Options");
                                            CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                            CorrectAnswerVal = 1;
                                            MarkVal = jsonChildNode.getString("Mark");
                                            Mark = Integer.parseInt(MarkVal);

                                            NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                            NegativeMark = Integer.parseInt(NegativeMarkVal);
                                            String Created_on = "01/06/2017 10:00:00";
                                            String ModifiedOn = "01/06/2017 10:00:00";
                                            StudentAnswer ="";
                                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                                            newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                                            String Evaluation = pref.getString("Evaluation", "0");
                                            File username=new File(Evaluation+"/"+ExamID);
                                            username.mkdir();
                                            if(question.contains("img")){
                                                String questionImg = testX(question,username.getAbsolutePath());
                                                question=questionImg;
                                            }

                                            for(int z = 0; z < options.length(); z++){
                                                String[] s = new String[options.length()];
                                                String storagepath = testX(options.get(z).toString(),username.getAbsolutePath());
                                                if(storagepath.contains(Evaluation)){
                                                    options.put(z,storagepath);
                                                }
                                            }



                                            if(isExamIdexist) {
                                                if (options.length() > 3) {
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(),options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                } else if (options.length() > 2) {
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect,question, QuestionNumber, options.get(0).toString(),options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                } else if (options.length() > 1) {
                                                    db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }
                                            }
                                            else{

                                                if (options.length() > 3) {
                                                    db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(),options.get(3).toString(), CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                } else if (options.length() > 2) {
                                                    db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(),options.get(2).toString(), "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                } else if (options.length() > 1) {
                                                    db.addExamQuestions(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect,question, QuestionNumber, options.get(0).toString(),options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                            NegativeMark, StudentAnswer,
                                                            IsCorrect,
                                                            ObtainedMark,
                                                            Created_on,
                                                            ModifiedOn));
                                                }
                                            }
                                            List<ExamDetails> examdetailsList = db.getAllExamsDetails(staffid);
                                            List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                            Calendar calendar = Calendar.getInstance();
                                            SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                            String strDate = mdformat.format(calendar.getTime());

                                            for (ExamDetails cn : examdetailsList) {
                                                String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                                                // Writing Contacts to log
                                                Log.d("Exam2: ", log);
                                            }

                                            for (QuestionDetails cn : questiondetailsList) {
                                                String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                                // Writing Contacts to log
                                                Log.d("Question2: ", log);
                                            }

                                            jsonObject.add(newItemObject);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                }







                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                            new validateUserTask2(jsonResponse).execute("");

                            Toast.makeText(getActivity(),"Exam Downloaded Successfully", Toast.LENGTH_LONG).show();

                            // edit.putBoolean("login",true);
                            //edit.commit();
                            //startActivity(new Intent(ExamTeacherHome.this,MainActivity.class));

                        }
                    }.execute();

                }
                else
                {
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                System.out.println(e.toString() + "zcx");
            }

        }
    }
    public String testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        String test="";
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
            String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));
            // prints http://www.01net.com/images/article/mea/150.100.790233.jpg

            String batchfile=m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            downloadfile(m.group(1).substring(1), path, batchfile);
            storagepath= path+"/"+batchfile;

            test = hjdsf.replaceAll(m.group(1).substring(1),storagepath);

            System.out.println(test+"test");
            //pathArray.add(test);
        }


        return test;

    }



    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response,statusres;

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(getActivity());
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();}

        validateUserTask2(String statusres){
            this.statusres=statusres;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:SyncComplete|StaffId:" + pref.getString("staffid", "0") + "|Portal User ID:" + pref.getString("portalstaffid", "0")+"|Type:exam";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(getActivity());

                        res = sr.getLogin(postParameters,
                                Url.baseurl);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                } else {
                    Log.e("error", "ss");
                }

            }
            catch(Exception e){
                if(dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(dia.isShowing())
                dia.cancel();
            //  Toast.makeText(getApplicationContext(), "ok"+result,Toast.LENGTH_LONG).show();


        }//close onPostExecute
    }
}
