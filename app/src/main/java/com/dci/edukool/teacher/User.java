package com.dci.edukool.teacher;

/**
 * Created by kirubakaranj on 4/25/2017.
 */

public class User {
    public String name=new String();
    public String hometown=new String();

    public User(String name, String hometown) {
        this.name = name;
        this.hometown = hometown;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }
}