package com.dci.edukool.teacher;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import Utilities.Utilss;
import books.PulseQuestionActivity;

public abstract class BaseActivity extends AppCompatActivity {
    private final Handler handler = new Handler();
    public LinearLayout home_lay, pulse_lay, quizelay, wifilay,
            update_lay, bright_lay, spinner_lay,linearDashboard;
    public ImageView home_icon, pulse_icon, quiz_icon, wifi_icon, getupdate_icon, brightness_icon;
    public TextView tv_home, tv_pulse, tv_quize, tv_wifi, tv_update, tv_bright, username;
    public Utilss utils;
    SharedPreferences pref;
    ProgressDialog dia;
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    ListView listView;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    ArrayList<User> arrayOfUsers;
    UsersAdapter adapter;
    SharedPreferences.Editor edit;
    private PopupWindow pwindo, listwindo;
    //    UsersAdapter  adapter;
//    ArrayList<User> arrayOfUsers;
//    WifiManager mainWifi;
//    PopupWindow listwindo;
//    WifiReceiver receiverWifi;
//    ListView listView;
//    private PopupWindow pwindo;
    private String wifipass;

    public static String getCurrentSsid(Context context, WifiManager wifiManager) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void downloadSuccess(String content) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_download_success);
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        TextView logout = (TextView) dialog.findViewById(R.id.logout);
        TextView textView5 = (TextView) dialog.findViewById(R.id.textView5);
        TextView textView6 = (TextView) dialog.findViewById(R.id.textView6);
        utils.setTextviewtypeface(5,ok_button);
        textView6.setText(content);
        utils.setTextviewtypeface(5,logout);
        utils.setTextviewtypeface(3,textView5);
        utils.setTextviewtypeface(5,textView6);

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear();
                edit.commit();
                finish();
                dialog.dismiss();
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void sessionBreak() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_session_break);
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        TextView logout = (TextView) dialog.findViewById(R.id.logout);
        TextView textView5 = (TextView) dialog.findViewById(R.id.textView5);
        TextView textView6 = (TextView) dialog.findViewById(R.id.textView6);
        utils.setTextviewtypeface(5,ok_button);
        utils.setTextviewtypeface(5,logout);
        utils.setTextviewtypeface(3,textView5);
        utils.setTextviewtypeface(5,textView6);

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear();
                edit.commit();
                finish();
                dialog.dismiss();
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // super.onAttachedToWindow();
        super.onCreate(savedInstanceState);
        setContentView(getlayout());
        home_lay = (LinearLayout) findViewById(R.id.home_lay);
        pulse_lay = (LinearLayout) findViewById(R.id.pulse_lay);
        quizelay = (LinearLayout) findViewById(R.id.quizelay);
        wifilay = (LinearLayout) findViewById(R.id.wifilay);
        linearDashboard = (LinearLayout) findViewById(R.id.linearDashboard);
        update_lay = (LinearLayout) findViewById(R.id.update_lay);
        bright_lay = (LinearLayout) findViewById(R.id.bright_lay);
        spinner_lay = (LinearLayout) findViewById(R.id.spinner_lay);
        spinner_lay.setVisibility(View.GONE);
        username = (TextView) findViewById(R.id.username);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();


        home_icon = (ImageView) findViewById(R.id.home_icon);
        pulse_icon = (ImageView) findViewById(R.id.pulse_icon);
        quiz_icon = (ImageView) findViewById(R.id.quiz_icon);
        wifi_icon = (ImageView) findViewById(R.id.wifi_icon);
        getupdate_icon = (ImageView) findViewById(R.id.getupdate_icon);
        brightness_icon = (ImageView) findViewById(R.id.brightness_icon);

        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_pulse = (TextView) findViewById(R.id.tv_pulse);
        tv_quize = (TextView) findViewById(R.id.tv_quize);
        tv_wifi = (TextView) findViewById(R.id.tv_wifi);
        tv_update = (TextView) findViewById(R.id.tv_update);
        tv_bright = (TextView) findViewById(R.id.tv_bright);

        utils = new Utilss(this);
        utils.setTextviewtypeface(5, tv_home);
        utils.setTextviewtypeface(5, tv_pulse);
        utils.setTextviewtypeface(5, tv_bright);
        utils.setTextviewtypeface(5, tv_update);
        utils.setTextviewtypeface(5, tv_wifi);
        utils.setTextviewtypeface(3, username);
        quizelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {
                    startActivity(new Intent(BaseActivity.this, Quizzactivity.class));
                  /*  Videoname video = videos.get(position);

                    File file = new File(video.getVideopath());



                    stratExamCompletedPopup(video);*/
                } else {
                    Utilss.lockpoopup(BaseActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }
            }
        });
        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
//
//        }
                Intent intent = new Intent(BaseActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        bright_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.System.canWrite(getApplicationContext())) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 200);

                    } else {
                        Intent intent1 = new Intent(BaseActivity.this, Brightness.class);
                        startActivity(intent1);
                    }
                } else {
                    Intent intent1 = new Intent(BaseActivity.this, Brightness.class);
                    startActivity(intent1);
                }


            }
        });
        if (pref.getString("uname", "") != null) {
            username.setText(pref.getString("uname", ""));

        }
        pulse_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
                if (!pref.getString("roomname", "").
                        equalsIgnoreCase("Session Break")) {
                    Intent intent = new Intent(BaseActivity.this, PulseQuestionActivity.class);
                    startActivity(intent);
                } else {
                    Utilss.lockpoopup(BaseActivity.this, "EduKool", "You can access this feature once you come out of the session break.");
                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");
                }
            }
        });
        update_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }*/

                final Dialog dialog = new Dialog(BaseActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.getupdate);

                TextView notnow = (TextView) dialog.findViewById(R.id.notnow);

                TextView update = (TextView) dialog.findViewById(R.id.update);
                notnow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String osversion = Build.VERSION.RELEASE;
                        String devname = android.os.Build.MODEL;
                        String ostype = "Android";
                        int versionCode = BuildConfig.VERSION_CODE;
                        String versionName = BuildConfig.VERSION_NAME;

                        String additionalparams = "|Device Type:" + ostype + "|GCMKey:" + "" +
                                "|DeviceID:" + "" +
                                "|AppID:" + versionCode +
                                "|IMEINumber:" + "" +
                                "|AppVersion:" + versionName +
                                "|MACAddress:" + "" +
                                "|OSVersion:" + osversion;
                        String login_str = "UserName:" + pref.getString("teachername", "") + "|Password:" + pref.getString("teacherpass", "") + "|Function:StaffLogin|Update:Yes" + additionalparams;
                        login.clear();
                        byte[] data;
                        try {
                            data = login_str.getBytes("UTF-8");
                            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                            if (utils.hasConnection()) {
                                login.clear();
                                login.add(new BasicNameValuePair("WS", base64_register));

                                validateUserTask load_plan_list = new validateUserTask(BaseActivity.this, login);
                                load_plan_list.execute();
                            } else {
                                utils.Showalert();
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                        dialog.dismiss();

                    }
                });

                dialog.show();

            }
        });
        wifilay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
                Listpopup();
            }
        });


    }

    private void Listpopup() {
        LayoutInflater inflater = (LayoutInflater) BaseActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_popup,
                (ViewGroup) findViewById(R.id.listparent));
        listwindo = new PopupWindow(layout, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT, true);
        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        listView = (ListView) layout.findViewById(R.id.list);
        final Button close = (Button) layout.findViewById(R.id.close);

        arrayOfUsers = new ArrayList<User>();
        //   for(int i=0;i<6;i++)
        arrayOfUsers.add(new User("", ""));

        adapter = new UsersAdapter(this, arrayOfUsers);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);

        doInback();
        ImageView imgclose = (ImageView) layout.findViewById(R.id.closeimg);
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = adapter.getItem(position);
                initiatePopupWindow(user.name);
                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();
            }
        });

    }

    private void initiatePopupWindow(final String ssid) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) BaseActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup2,
                    (ViewGroup) findViewById(R.id.popup_element));
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);

            dialog.setContentView(layout);
            dialog.show();

//            pwindo = new PopupWindow(layout, 450, 130, true);
//            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            final EditText password = (EditText) layout.findViewById(R.id.edit);

            Button btnconnect = (Button) layout.findViewById(R.id.button1);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    wifipass = password.getText().toString();
                    if (!wifipass.equalsIgnoreCase("")) {
                        WifiConfiguration wc = new WifiConfiguration();
                        wc.SSID = String.format("\"%s\"", ssid);
                        wc.preSharedKey = String.format("\"%s\"", wifipass);
                        wc.status = WifiConfiguration.Status.ENABLED;
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                        int netId = mainWifi.addNetwork(wc);
                        mainWifi.disconnect();
                        mainWifi.enableNetwork(netId, true);
                        mainWifi.reconnect();
                        doInback();
                    }
                    hideKeyboard();
                    dialog.dismiss();

                   // pwindo.dismiss();
                }
            });
            Button btncancel = (Button) layout.findViewById(R.id.button2);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doInback() {

        handler.postDelayed(new Runnable() {

            @SuppressLint("WifiManagerLeak")
            @Override
            public void run() {
                // TODO Auto-generated method stub

                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new WifiReceiver();
                registerReceiver(receiverWifi, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                if (mainWifi.isWifiEnabled() == false) {
                    mainWifi.setWifiEnabled(true);
                }

                mainWifi.startScan();
                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);

                // doInback();
            }
        }, 1000);

    }

    public abstract int getlayout();

    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

            arrayOfUsers.clear();

            ArrayList<String> connections = new ArrayList<String>();
            ArrayList<Float> Signal_Strenth = new ArrayList<Float>();

            StringBuilder sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();
            for (int i = 0; i < wifiList.size(); i++) {
                connections.add(wifiList.get(i).SSID);
            }

            //  adapter.clear();
            Log.d("con-size", connections.size() + "");
            if (connections.size() == 1) {
                doInback();
            }
            for (int y = 0; y < connections.size(); y++) {


                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                String mode = "Not connected";
                String compare = null;
                //   Log.d("connectedssid", ssid);
                //      Log.d("listssid",connections.get(y));
                //    Log.e("ssidiyyappa","iyyappa"+ssid);

                if (ssid != null) {
                    compare = ssid.replace("\"", "");
                }


                if (ssid != null) {
                    if (compare.equalsIgnoreCase(connections.get(y))) {
                        mode = "connected";
                    } else {
                        mode = "Not connected";
                    }
                }

                User newUser = new User("" + connections.get(y), mode);
                arrayOfUsers.add(newUser);
                //adapter.add(newUser);


            }
            for (int i = 0; i < arrayOfUsers.size(); i++) {


                //    Log.e("size",""+arrayOfUsers.size());
                if (arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected")) {
                    int j = i;
                    if (i == 0) {
                        break;
                    } else {
                        User use = arrayOfUsers.get(i);
                        arrayOfUsers.remove(i);
                        arrayOfUsers.add(0, use);
                        break;
                    }
                }

                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
                    {

                    }*/
            }

            adapter.notifyDataSetChanged();


        }
    }
//    class WifiReceiver extends BroadcastReceiver {
//        public void onReceive(Context c, Intent intent) {
//
//            arrayOfUsers.clear();
//
//            ArrayList<String> connections = new ArrayList<String>();
//            ArrayList<Float> Signal_Strenth = new ArrayList<Float>();
//
//           StringBuilder sb = new StringBuilder();
//            List<ScanResult> wifiList;
//            wifiList = mainWifi.getScanResults();
//            for (int i = 0; i < wifiList.size(); i++) {
//                connections.add(wifiList.get(i).SSID);
//            }
//
//            //  adapter.clear();
//            Log.d("con-size", connections.size() + "");
//            if (connections.size() == 1) {
//                doInback();
//            }
//            for (int y = 0; y < connections.size(); y++) {
//
//
//                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
//                String mode = "Not connected";
//                String compare = null;
//                //   Log.d("connectedssid", ssid);
//                //      Log.d("listssid",connections.get(y));
//                //    Log.e("ssidiyyappa","iyyappa"+ssid);
//
//                if (ssid != null) {
//                    compare = ssid.replace("\"", "");
//                }
//
//
//                if (ssid != null) {
//                    if (compare.equalsIgnoreCase(connections.get(y))) {
//                        mode = "connected";
//                    } else {
//                        mode = "Not connected";
//                    }
//                }
//
//                User newUser = new User("" + connections.get(y), mode);
//                arrayOfUsers.add(newUser);
//                //adapter.add(newUser);
//
//
//            }
//            for (int i = 0; i < arrayOfUsers.size(); i++) {
//
//
//                //    Log.e("size",""+arrayOfUsers.size());
//                if (arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected")) {
//                    int j = i;
//                    if (i == 0) {
//                        break;
//                    } else {
//                        User use = arrayOfUsers.get(i);
//                        arrayOfUsers.remove(i);
//                        arrayOfUsers.add(0, use);
//                        break;
//                    }
//                }
//
//                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
//                    {
//
//                    }*/
//            }
//
//            adapter.notifyDataSetChanged();
//
//
//        }
//    }
//    public static String getCurrentSsid(Context context, WifiManager wifiManager) {
//        String ssid = null;
//        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//        if (networkInfo.isConnected()) {
//            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
//            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
//            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
//                ssid = connectionInfo.getSSID();
//            }
//        }
//        return ssid;
//    }
//
//    private void Listpopup() {
//        LayoutInflater inflater = (LayoutInflater) BaseActivity.this
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View layout = inflater.inflate(R.layout.list_popup,
//                (ViewGroup) findViewById(R.id.listparent));
//        listwindo = new PopupWindow(layout, 600, 600, true);
//        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
//        listView = (ListView) layout.findViewById(R.id.list);
//        final Button close = (Button) layout.findViewById(R.id.close);
//
//        arrayOfUsers = new ArrayList<User>();
//        //   for(int i=0;i<6;i++)
//        arrayOfUsers.add(new User("", ""));
//
//        adapter = new UsersAdapter(this, arrayOfUsers);
//        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
//        listView.setAdapter(adapter);
//
//        doInback();
//        ImageView imgclose = (ImageView) layout.findViewById(R.id.closeimg);
//        imgclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listwindo.dismiss();
//
//            }
//        });
//
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                User user = adapter.getItem(position);
//                initiatePopupWindow(user.name);
//                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
//            }
//        });
//
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listwindo.dismiss();
//            }
//        });
//
//    }
//    private void initiatePopupWindow(final String ssid) {
//        try {
//// We need to get the instance of the LayoutInflater
//            LayoutInflater inflater = (LayoutInflater) BaseActivity.this
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View layout = inflater.inflate(R.layout.screen_popup2,
//                    (ViewGroup) findViewById(R.id.popup_element));
//            pwindo = new PopupWindow(layout, 450, 130, true);
//            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
//
//            final EditText password = (EditText) layout.findViewById(R.id.edit);
//
//            Button btnconnect = (Button) layout.findViewById(R.id.button1);
//            btnconnect.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    wifipass = password.getText().toString();
//                    if (!wifipass.equalsIgnoreCase("")) {
//                        WifiConfiguration wc = new WifiConfiguration();
//                        wc.SSID = String.format("\"%s\"", ssid);
//                        wc.preSharedKey = String.format("\"%s\"", wifipass);
//                        wc.status = WifiConfiguration.Status.ENABLED;
//                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
//                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
//
//                        int netId = mainWifi.addNetwork(wc);
//                        mainWifi.disconnect();
//                        mainWifi.enableNetwork(netId, true);
//                        mainWifi.reconnect();
//                        doInback();
//                    }
//
//                    pwindo.dismiss();
//                }
//            });
//
//            Button btncancel = (Button) layout.findViewById(R.id.button2);
//            btncancel.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    pwindo.dismiss();
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    public void doInback() {
//
//        handler.postDelayed(new Runnable() {
//
//            @SuppressLint("WifiManagerLeak")
//            @Override
//            public void run() {
//                // TODO Auto-generated method stub
//
//                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//
//                receiverWifi = new WifiReceiver();
//                registerReceiver(receiverWifi, new IntentFilter(
//                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//                if (mainWifi.isWifiEnabled() == false) {
//                    mainWifi.setWifiEnabled(true);
//                }
//
//                mainWifi.startScan();
//                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);
//
//                // doInback();
//            }
//        }, 1000);
//
//    }
//    public abstract int getlayout();


}
