package com.dci.edukool.teacher;
import android.app.Activity;
import android.content.Context;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Utilities.Utilss;

public class GridAdapter extends ArrayAdapter {
    private static final String TAG = GridAdapter.class.getSimpleName();
    private LayoutInflater mInflater;
    private List<Date> monthlyDates;
    private Calendar currentDate;
    Context context;
    Utilss utils;
    private List<Date> highlightdate;

    public GridAdapter(Context context, List<Date> monthlyDates, Calendar currentDate, List<Date> selectdate) {
        super(context, R.layout.single_cell_layout);
        this.monthlyDates = monthlyDates;
        this.currentDate = currentDate;
        this.context=context;
        utils=new Utilss((Activity) context);
        this.highlightdate = selectdate;
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Date mDate = monthlyDates.get(position);
        Calendar cal = Calendar.getInstance();
        int current =  cal.get(Calendar.MONTH)+1;
        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(mDate);
        int dayValue = dateCal.get(Calendar.DAY_OF_MONTH);
        int displayMonth = dateCal.get(Calendar.MONTH) + 1;
        int displayYear = dateCal.get(Calendar.YEAR);
        int currentMonth = currentDate.get(Calendar.MONTH) + 1;
        int currentYear = currentDate.get(Calendar.YEAR);
        int currentday=currentDate.get(Calendar.DATE);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(mDate);



        Log.i("month","dispmoth"+displayMonth+"currentmonth"+currentMonth+current);
        View view = convertView;
        if(view == null){
            view = mInflater.inflate(R.layout.single_cell_layout, parent, false);
        }
        TextView cellNumber = (TextView)view.findViewById(R.id.calendar_date_id);
        cellNumber.setText(String.valueOf(dayValue));
        cellNumber.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);
        if(displayMonth == currentMonth && displayYear == currentYear){
            utils.setTextviewtypeface(1,cellNumber);
            cellNumber.setTextColor(context.getResources().getColor(R.color.calendertext_color));
           // view.setBackgroundColor(Color.parseColor("#FF5733"));
        }else{
            utils.setTextviewtypeface(0,cellNumber);
            cellNumber.setTextColor(context.getResources().getColor(R.color.classname_color));
           // view.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        if (current == displayMonth && dayValue==currentday){
            cellNumber.setBackgroundResource(R.drawable.circle_textview);
            cellNumber.setTextColor(context.getResources().getColor(R.color.white));

        }
        else {
            cellNumber.setBackgroundResource(0);
            cellNumber.setTextColor(context.getResources().getColor(R.color.black));
        }

        if (highlightdate!= null && highlightdate.size()>0){
            for (int j = 0 ; j < highlightdate.size(); j++){
                SimpleDateFormat dff = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDatee = dff.format(highlightdate.get(j));
                if (formattedDate.equals(formattedDatee)){
                    cellNumber.setBackgroundResource(R.drawable.circle_textview);
                    cellNumber.setTextColor(context.getResources().getColor(R.color.white));
                }

            }
        }

        /*for (int k = 0 ; k < highlightdate.size(); k++){
            Date lDate =(Date)highlightdate.get(k);
            String ds = formattedDate.format(String.valueOf(lDate));
            if (formattedDate.equals(ds)){
                cellNumber.setBackgroundResource(R.drawable.circle_textview);
                cellNumber.setTextColor(context.getResources().getColor(R.color.white));
            }
        }*/


        //Add day to calendar
//        TextView cellNumber = (TextView)view.findViewById(R.id.calendar_date_id);
//        cellNumber.setText(String.valueOf(dayValue));
        //Add events to the calendar
        TextView eventIndicator = (TextView)view.findViewById(R.id.event_id);
        Calendar eventCalendar = Calendar.getInstance();
//        for(int i = 0; i < allEvents.size(); i++){
//            eventCalendar.setTime(allEvents.get(i).getDate());
//            if(dayValue == eventCalendar.get(Calendar.DAY_OF_MONTH) && displayMonth == eventCalendar.get(Calendar.MONTH) + 1
//                    && displayYear == eventCalendar.get(Calendar.YEAR)){
//                eventIndicator.setBackgroundColor(Color.parseColor("#FF4081"));
//            }
//        }
        return view;
    }
    @Override
    public int getCount() {
        return monthlyDates.size();
    }
    @Nullable
    @Override
    public Object getItem(int position) {
        return monthlyDates.get(position);
    }
    @Override
    public int getPosition(Object item) {
        return monthlyDates.indexOf(item);
    }
}