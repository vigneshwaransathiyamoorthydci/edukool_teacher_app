package com.dci.edukool.teacher;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.anastr.flattimelib.FlatClockView;
import com.github.anastr.flattimelib.intf.OnClockTick;

import java.util.ArrayList;

import Utilities.Utilss;
import books.Attendancewithcheckparentlayout;
import connection.Buzzerclass;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Buzzerhelper;

/**
 * Created by abimathi on 30-May-17.
 */
public class Quizzactivity extends BaseActivity implements OnClockTick {
    public static ArrayList<Buzzerhelper> buzzer = new ArrayList<>();
    TextView timer;
    ImageView close,back;
    RelativeLayout bottom;
    TextView newquizz;
    // ImageView start;
    //ImageView reset;
    TextView timertext2;
    TextView timertext3, title;
    TextView img_reset,class_name;
    TextView img_start, semicolon1, semicolon2;
    BroadcastReceiver startquizz = null;
    CountDownTimer timercount;
    Buzzerclass buzz = null;
    BroadcastReceiver closealert = null;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    boolean starttime;
    TextView tv_stop_watch;
    Utilss utils;
    FlatClockView clockImageView;
    TextView tv_english, tv_maths, tv_science, tv_tamil, tv_soscience;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        tv_english = (TextView) findViewById(R.id.tv_english);
        tv_maths = (TextView) findViewById(R.id.tv_maths);
        tv_science = (TextView) findViewById(R.id.tv_science);
        tv_tamil = (TextView) findViewById(R.id.tv_tamil);
        class_name = (TextView)findViewById(R.id.class_name);
        tv_soscience = (TextView) findViewById(R.id.tv_soscience);
        this.setFinishOnTouchOutside(false);
        utils = new Utilss(this);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();
        tv_stop_watch = (TextView) findViewById(R.id.stop_watch);
        title = (TextView) findViewById(R.id.title);
        title.setText("Quiz");
        back= (ImageView) findViewById(R.id.back);
        class_name.setText( pref.getString("classname", ""));
        clockImageView = (FlatClockView) findViewById(R.id.clock_face);
        clockImageView.setOnClockTick(this);
        quizelay.setBackgroundColor(getResources().getColor(R.color.oldbg));
        quiz_icon.setImageDrawable(getResources().getDrawable(R.drawable.help));
        tv_quize.setTextColor(getResources().getColor(R.color.white));
        timer = (TextView) findViewById(R.id.timertext);
        semicolon1 = (TextView) findViewById(R.id.semicolon1);
        semicolon2 = (TextView) findViewById(R.id.semicolon2);
        timertext2 = (TextView) findViewById(R.id.timertext2);
        timertext3 = (TextView) findViewById(R.id.timertext3);
        img_reset = (TextView) findViewById(R.id.resetimage);
        img_start = (TextView) findViewById(R.id.startimage);
        close = (ImageView) findViewById(R.id.close);
        bottom = (RelativeLayout) findViewById(R.id.bottomlayout);
        newquizz = (TextView) findViewById(R.id.newquiz);
        // start = (ImageView) findViewById(R.id.start);
        //  reset= (ImageView) findViewById(R.id.reset);
        buzzer.clear();
        img_start.setEnabled(false);
        img_reset.setEnabled(false);
        bottom.setEnabled(false);
        bottom.setVisibility(View.GONE);

        timercreate();
        edit.putBoolean("quizz", false);
        edit.commit();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    timercount.cancel();
                } catch (Exception e) {

                }
                if (starttime) {
                    String senddata = "Quiz@reset";
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
                buzzer.clear();
                buzzer.clear();
                finish();
            }
        });

        utils.setTextviewtypeface(1, tv_stop_watch);
        utils.setTextviewtypeface(4, timertext2);
        utils.setTextviewtypeface(4, timertext3);
        utils.setTextviewtypeface(4, timer);
        utils.setTextviewtypeface(4, semicolon1);
        utils.setTextviewtypeface(4, semicolon2);
        utils.setTextviewtypeface(5, img_start);
        utils.setTextviewtypeface(5, img_reset);
        utils.setTextviewtypeface(3, title);
        utils.setTextviewtypeface(3, title);
        utils.setTextviewtypeface(5, tv_english);
        utils.setTextviewtypeface(5, tv_maths);
        utils.setTextviewtypeface(5, tv_science);
        utils.setTextviewtypeface(5, tv_tamil);
        utils.setTextviewtypeface(5, tv_soscience);


       /* close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        img_start.setEnabled(true);
        newquizz.setEnabled(false);

        newquizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newquizz.setEnabled(false);
                img_start.setEnabled(true);
                img_reset.setEnabled(true);
                bottom.setEnabled(true);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    timercount.cancel();
                } catch (Exception e) {

                }
                if (starttime) {
                    String senddata = "Quiz@reset";
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
                buzzer.clear();
                buzzer.clear();
                finish();
            }
        });


        img_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_start.setBackground(getResources().getDrawable(R.drawable.btn_bg));
                img_start.setTextColor(getResources().getColor(R.color.white));
                img_reset.setBackground(getResources().getDrawable(R.drawable.shape_reset));
                img_reset.setTextColor(getResources().getColor(R.color.oldbg));
                img_reset.setEnabled(true);
                stratExamCompletedPopup("");
                // img_start.setEnabled(false);


           /*   timercount=  new CountDownTimer(35000, 1) {

                    public void onTick(long millisUntilFinished) {
                        //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
                        //here you can have your logic to set text to edittext
                        timer.setText("" + millisUntilFinished / 1000);
                        timertext3.setText("" + millisUntilFinished % 1000);


                    }

                    public void onFinish() {
                        img_start.setEnabled(true);
                        timer.setText("00");
                        timertext3.setText("00");
                        //finish();
                    }

                }.start();*/


            }
        });
        img_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                img_reset.setBackground(getResources().getDrawable(R.drawable.btn_bg));
                img_reset.setTextColor(getResources().getColor(R.color.white));
                img_start.setBackground(getResources().getDrawable(R.drawable.shape_reset));
                img_start.setTextColor(getResources().getColor(R.color.oldbg));
                clockImageView.setTime("00:00:00");
                clockImageView.setOnClockTick(Quizzactivity.this);
                //  timercount.cancel();
                starttime = false;
                img_reset.setEnabled(false);
                img_start.setEnabled(true);
                timer.setText("00");
                timertext3.setText("00");
                String senddata = "Quiz@reset";
                buzzer.clear();
                try {
                    timercount.cancel();
                } catch (Exception e) {

                }

                new clientThread(MultiThreadChatServerSync.thread, senddata).start();

            }
        });
        bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    timercount.cancel();
                } catch (Exception e) {

                }
                String senddata = "Quiz@reset";
                new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                buzzer.clear();

                finish();
            }
        });


/*
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///dialog.dismiss();
            }
        });*/
        setSubjectbackround();

    }

    public void setSubjectbackround() {
        if (pref.getString("subjectname", "") != null && !pref.getString("subjectname", "").equals("")) {
            String subname = pref.getString("subjectname", "").toLowerCase();

            switch (subname) {
                case "english":
                    tv_english.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_english.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "maths":
                    tv_maths.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_maths.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "tamil":
                    tv_tamil.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_tamil.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "sceince":
                    tv_science.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_science.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "social science":
                    tv_soscience.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_soscience.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        }

    }

    @Override
    public int getlayout() {
        return R.layout.quizz;
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Attendancewithcheckparentlayout.starttimer) {
            Attendancewithcheckparentlayout.starttimer = false;
            timercount.start();

            img_start.setEnabled(false);
            img_reset.setEnabled(true);
            buzzer.clear();
            starttime = true;
        }
        if (startquizz == null) {
            startquizz = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    timercount.start();

                    img_start.setEnabled(false);
                    buzzer.clear();
                    starttime = true;


                }
            };
            IntentFilter quizfilter = new IntentFilter("quizz");
            registerReceiver(startquizz, quizfilter);
        }

      /*  if(closealert==null) {
            closealert = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    try {
                        timercount.cancel();
                    } catch (Exception e) {

                    }
                    finish();
                }
            };
            IntentFilter fileter = new IntentFilter("closealert");
            registerReceiver(closealert, fileter);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    void timercreate() {
        timercount = new CountDownTimer(5000, 1) {

            public void onTick(long millisUntilFinished) {
                //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
                //here you can have your logic to set text to edittext
                timer.setText("" + millisUntilFinished / 1000);
                timertext3.setText("" + millisUntilFinished % 1000);


            }
            public void onFinish() {
                img_start.setEnabled(true);
                timer.setText("00");
                timertext3.setText("00");
                clockImageView.setTime("00:00:00");
                clockImageView.setOnClockTick(Quizzactivity.this);
                if (buzzer.size() > 0) {
                    startActivity(new Intent(Quizzactivity.this, Buzzerlistactivity.class));
                    finish();
                } else {
                    Toast.makeText(Quizzactivity.this, "No one has press buzzer", Toast.LENGTH_SHORT).show();
                }
          /*  if(!pref.getBoolean("quizz",false))
            Toast.makeText(Quizzactivity.this,"No one has press buzzer",Toast.LENGTH_SHORT).show();*/
                //finish();
            }

        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(startquizz);
        // unregisterReceiver(closealert);

    }


    public void stratExamCompletedPopup(String sendata) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        TextView content = (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

              /*  try
                {
                    timercount.cancel();
                }
                catch (Exception e)
                {

                }
                String senddata="Quiz@reset";
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();
                buzzer.clear();
                dialog.dismiss();*/
                //finish();


                // finish();
            }
        });
        title.setText("QUIZZ");
        content.setText("Share Quizz to?");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // File file=new File(video.getVideopath());

                //  String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();
                String senddata = "Quiz@Open";
                new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                Intent in = new Intent("quizz");
                sendBroadcast(in);
                clockImageView.setTime("00:00:00");
                clockImageView.setOnClockTick(null);
              /*  if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  File file=new File(video.getVideopath());

             /*   if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                // String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                String senddata = "Quiz@Open";

                Intent in = new Intent(Quizzactivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("Title", "QUIZZ");
                in.putExtra("senddata", senddata);
                startActivity(in);


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onTick() {
        clockImageView.setTime("00:00:00");
    }
}
