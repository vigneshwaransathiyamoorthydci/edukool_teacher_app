package com.dci.edukool.teacher;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import Utilities.Utilss;

public class CalenderCategoryAdapter extends
        RecyclerView.Adapter<CalenderCategoryAdapter.MyViewHolder> {
    private List<helper.Calendarpojo> TimetablemodelsList;
    Utilss utils;
    int pos=-1;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        LinearLayout whole_lay;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            whole_lay=(LinearLayout)view.findViewById(R.id.whole_lay);
        }
    }
    public CalenderCategoryAdapter(List<helper.Calendarpojo> TimetablemodelsList, Context context) {
        this.TimetablemodelsList = TimetablemodelsList;
        utils=new Utilss((Activity) context);
        this.context=context;
    }

    @Override
    public CalenderCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_calendar_cat, parent, false);
        return new CalenderCategoryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CalenderCategoryAdapter.MyViewHolder holder, final int position) {
        helper.Calendarpojo Timetablemodel = TimetablemodelsList.get(position);
        holder.title.setText(Timetablemodel.getCalendarname());
        utils.setTextviewtypeface(5,holder.title);
        holder.whole_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos=position;
                notifyDataSetChanged();
            }
        });
        if (position==pos){
            holder.whole_lay.setBackground(ContextCompat.getDrawable(context,R.drawable.border_purple));
            holder.title.setTextColor(context.getResources().getColor(R.color.white));
        }
        else {
            holder.whole_lay.setBackground(ContextCompat.getDrawable(context,R.drawable.border_lightwhite));
            holder.title.setTextColor(context.getResources().getColor(R.color.oldbg));
        }
    }

    @Override
    public int getItemCount() {
        return TimetablemodelsList.size();
    }
}