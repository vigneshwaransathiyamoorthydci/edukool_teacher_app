package com.dci.edukool.teacher;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import Utilities.Utilss;
import books.Attendancewithcheckparentlayout;
import connection.Filesharingtoclient;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.*;
import Utilities.*;
import models.Timetablemodel;


public class TimeTableActivity extends BaseActivity {
    Button er;
    LinearLayout lin;
    Staffdetails sch_details;
    Staffloingdetails staffdetails;
    String currentday;
    ArrayList<Subject> period = new ArrayList<>();
    ArrayList<Subject> mon = new ArrayList<>();
    ArrayList<Subject> tue = new ArrayList<>();
    ArrayList<Subject> wed = new ArrayList<>();
    ArrayList<Subject> thu = new ArrayList<>();
    ArrayList<Subject> firday = new ArrayList<>();
    ArrayList<Subject> satday = new ArrayList<>();
    ArrayList<Subject> sunday = new ArrayList<>();
    ArrayList<Subject> secperiod = new ArrayList<>();
    ArrayList<Subject> secmon = new ArrayList<>();
    ArrayList<Subject> sectue = new ArrayList<>();
    ArrayList<Subject> secwed = new ArrayList<>();
    ArrayList<Subject> secthu = new ArrayList<>();
    ArrayList<Subject> secfirday = new ArrayList<>();
    ArrayList<Subject> secsatday = new ArrayList<>();
    ArrayList<Subject> secsunday = new ArrayList<>();
    RoundedImageView profileimage;
    TextView batchname, attendance;
    ArrayList<Subject> common;
    ImageView back;
    Utilss utils;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    ImageView handraise;
    ImageView dnd;
    ImageView down;
    TextView mon_text, tue_text, wed_text, thurs_text, fri_text, sat_text, sun_text, dat;
    TextView secmon_text, sectue_text, secwed_text, secthurs_text, secfri_text, secsat_text, secsun_text;
    TextView secclassname,class_name;

    LinearLayout lin2;
    RelativeLayout share, secondalayout;
    ImageView but;
    String calendarpath;
    ProgressDialog dia;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    DatabaseHandler db;
    RecyclerView recyclerView;
    TimetableAdapter adapter;
    TextView tv_fri,tv_thu,tv_wed,tvtue,tvmonday,title,date_range,date,tv_tamil,
            eng,math,Science,ssci,note,details;

    public static String currentdate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        utils = new Utilss(TimeTableActivity.this);
        title=(TextView)findViewById(R.id.title);
        date_range= (TextView) findViewById(R.id.date_range);
        date= (TextView) findViewById(R.id.date);
        tv_tamil= (TextView) findViewById(R.id.tv_tamil);
        eng= (TextView) findViewById(R.id.eng);
        math= (TextView) findViewById(R.id.math);
        class_name = (TextView)findViewById(R.id.class_name);
        Science= (TextView) findViewById(R.id.Science);
        ssci= (TextView) findViewById(R.id.ssci);
        note=(TextView) findViewById(R.id.note);
        details=(TextView) findViewById(R.id.details);
        utils.setTextviewtypeface(1,note);
        utils.setTextviewtypeface(5,details);

        recyclerView= (RecyclerView) findViewById(R.id.recycle_timetable);
        adapter=new TimetableAdapter(getdata(),this);
        LinearLayoutManager linearLayoutManager=new GridLayoutManager(TimeTableActivity.this,5);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        tv_fri=(TextView) findViewById(R.id.tv_fri);
        tv_thu=(TextView) findViewById(R.id.tv_thu);
        tv_wed=(TextView) findViewById(R.id.tv_wed);
        tvtue=(TextView) findViewById(R.id.tvtue);
        tvmonday=(TextView) findViewById(R.id.tvmonday);
        utils.setTextviewtypeface(1,tv_fri);
        utils.setTextviewtypeface(1,tv_thu);
        utils.setTextviewtypeface(1,tv_wed);
        utils.setTextviewtypeface(1,tvtue);
        utils.setTextviewtypeface(4,title);
        utils.setTextviewtypeface(1,tvmonday);
        utils.setTextviewtypeface(0,date_range);

        utils.setTextviewtypeface(3,tv_tamil);
        utils.setTextviewtypeface(3,math);
        utils.setTextviewtypeface(3,ssci);
        utils.setTextviewtypeface(3,date);
        utils.setTextviewtypeface(3,eng);
        utils.setTextviewtypeface(3,Science);


        lin = (LinearLayout) findViewById(R.id.linearLayout1);
        lin2 = (LinearLayout) findViewById(R.id.lay1);
        share = (RelativeLayout) findViewById(R.id.sharelayout);
        secondalayout = (RelativeLayout) findViewById(R.id.secondtable);
        secclassname = (TextView) findViewById(R.id.classnameofsecondlayout);
        but = (ImageView) findViewById(R.id.but);
        down = (ImageView) findViewById(R.id.download);
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        back = (ImageView) findViewById(R.id.back);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        // db=new DatabaseHandler(this);
        db = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        edit = pref.edit();
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stratExamCompletedPopup();
            }
        });


        secmon_text = (TextView) findViewById(R.id.monday);
        sectue_text = (TextView) findViewById(R.id.tuedat);
        secwed_text = (TextView) findViewById(R.id.weday);
        secthurs_text = (TextView) findViewById(R.id.thurdat);
        secfri_text = (TextView) findViewById(R.id.friday);
        secsat_text = (TextView) findViewById(R.id.saturday);
        secsun_text = (TextView) findViewById(R.id.sunday);
        mon_text = (TextView) findViewById(R.id.mon);
        tue_text = (TextView) findViewById(R.id.tue);
        wed_text = (TextView) findViewById(R.id.wed);
        thurs_text = (TextView) findViewById(R.id.thur);
        fri_text = (TextView) findViewById(R.id.fri);
        sat_text = (TextView) findViewById(R.id.sat);
        sun_text = (TextView) findViewById(R.id.sun);
        dat = (TextView) findViewById(R.id.date);
        batchname = (TextView) findViewById(R.id.textView);
        attendance = (TextView) findViewById(R.id.classname);
        currentday = getCurrentDay();
        setbackground(profileimage, pref.getString("image", ""));
        batchname.setText(pref.getString("classname", ""));
        secclassname.setText(pref.getString("classname", ""));
        class_name.setText( pref.getString("classname", ""));
        // classname.setText(pref.getString("classname",""));

        dat.setText(currentday + " " + currentdate());
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        handraise = (ImageView) findViewById(R.id.handrise);


        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(TimeTableActivity.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(TimeTableActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });


        dnd = (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((!pref.getBoolean("break", false))) {


                    if (pref.getBoolean("dnd", false)) {
                        dnd.setImageResource(R.drawable.dci);
                        edit.putBoolean("dnd", false);
                        edit.commit();

                        String senddata = "DND";

                        senddata = senddata + "@false";
                        dnd.setImageResource(R.drawable.inactive);

                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                    } else {
                        String senddata = "DND";

                        senddata = senddata + "@true";
                        edit.putBoolean("dnd", true);
                        edit.commit();
                        //  dnd=dnd+1;
                        dnd.setImageResource(R.drawable.active);

                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    }

                } else {
                    Toast.makeText(TimeTableActivity.this, "You can access this feature once you come out of the session break.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String osversion = Build.VERSION.RELEASE;
                String devname = android.os.Build.MODEL;
                String ostype = "Android";
                int versionCode = BuildConfig.VERSION_CODE;
                String versionName = BuildConfig.VERSION_NAME;
                String additionalparams = "|Device Type:" + ostype + "|GCMKey:" + "" +
                        "|DeviceID:" + "" +
                        "|AppID:" + versionCode +
                        "|IMEINumber:" + "" +
                        "|AppVersion:" + versionName +
                        "|MACAddress:" + "" +
                        "|OSVersion:" + osversion;
                String login_str = "UserName:" + pref.getString("teachername", "") + "|Password:" + pref.getString("teacherpass", "") + "|Function:StaffLogin|Update:Yes" + additionalparams;
                login.clear();
                byte[] data;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        login.clear();
                        login.add(new BasicNameValuePair("WS", base64_register));

                        validateUserTask load_plan_list = new validateUserTask(TimeTableActivity.this, login);
                        load_plan_list.execute();
                    } else {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

        });

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                period.clear();
                mon.clear();
                tue.clear();
                wed.clear();
                thu.clear();
                firday.clear();
                parseXML();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                ArrayList<ArrayList<Subject>> all = new ArrayList<ArrayList<Subject>>();
                all.add(period);
                all.add(mon);
                all.add(tue);
                all.add(wed);
                all.add(thu);
                all.add(firday);

                for (int y = 0; y < period.size(); y++) {
                    Subject sub = new Subject();
                    sub.setSubjectname("");
                    satday.add(sub);

                }
                all.add(satday);

                for (int y = 0; y < period.size(); y++) {
                    Subject sub = new Subject();
                    sub.setSubjectname("");
                    sunday.add(sub);

                }
                all.add(sunday);


                for (int i = 0; i < all.size(); i++) {
                    final View hiddenInfo = getLayoutInflater().inflate(
                            R.layout.dynamicview, null, false);
                    LinearLayout hor = (LinearLayout) hiddenInfo.findViewById(R.id.dynlin);
                    for (int j = 0; j < all.get(i).size(); j++) {
                        final View hiddenInfo2 = getLayoutInflater().inflate(
                                R.layout.dyntext, null, false);
                        TextView text = (TextView) hiddenInfo2.findViewById(R.id.dyn);
                        text.setText(all.get(i).get(j).getSubjectname());

                        if (currentday.equalsIgnoreCase("Monday")) {
                            if (i == 1) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                mon_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        } else if (currentday.equalsIgnoreCase("Tuesday")) {
                            if (i == 2) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                tue_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        } else if (currentday.equalsIgnoreCase("Wednesday")) {
                            if (i == 3) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                wed_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        } else if (currentday.equalsIgnoreCase("Thursday")) {
                            if (i == 4) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                thurs_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        } else if (currentday.equalsIgnoreCase("Friday")) {
                            if (i == 5) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                fri_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        } else if (currentday.equalsIgnoreCase("Saturday")) {
                            if (i == 6) {
                                text.setBackgroundResource(R.drawable.yellow_border);
                                sat_text.setBackgroundResource(R.drawable.yellow_border);
                            }
                        }


                        hor.addView(hiddenInfo2);
                    }
                    lin.addView(hor);
                }


                if (!pref.getBoolean("roomornot", false) && (!pref.getBoolean("break", false))) {
                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {
                            period.clear();
                            mon.clear();
                            tue.clear();
                            wed.clear();
                            thu.clear();
                            firday.clear();
                            parseXML2();

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            ArrayList<ArrayList<Subject>> all = new ArrayList<ArrayList<Subject>>();
                            all.add(period);
                            all.add(mon);
                            all.add(tue);
                            all.add(wed);
                            all.add(thu);
                            all.add(firday);

                            for (int y = 0; y < period.size(); y++) {
                                Subject sub = new Subject();
                                sub.setSubjectname("");
                                satday.add(sub);

                            }
                            all.add(satday);

                            for (int y = 0; y < period.size(); y++) {
                                Subject sub = new Subject();
                                sub.setSubjectname("");
                                sunday.add(sub);

                            }
                            all.add(sunday);


                            for (int i = 0; i < all.size(); i++) {
                                final View hiddenInfo = getLayoutInflater().inflate(
                                        R.layout.dynamicview, null, false);
                                LinearLayout hor = (LinearLayout) hiddenInfo.findViewById(R.id.dynlin);
                                for (int j = 0; j < all.get(i).size(); j++) {
                                    final View hiddenInfo2 = getLayoutInflater().inflate(
                                            R.layout.dyntext, null, false);
                                    TextView text = (TextView) hiddenInfo2.findViewById(R.id.dyn);
                                    text.setText(all.get(i).get(j).getSubjectname());

                                    if (currentday.equalsIgnoreCase("Monday")) {
                                        if (i == 1) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            secmon_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    } else if (currentday.equalsIgnoreCase("Tuesday")) {
                                        if (i == 2) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            sectue_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    } else if (currentday.equalsIgnoreCase("Wednesday")) {
                                        if (i == 3) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            secwed_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    } else if (currentday.equalsIgnoreCase("Thursday")) {
                                        if (i == 4) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            secthurs_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    } else if (currentday.equalsIgnoreCase("Friday")) {
                                        if (i == 5) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            secfri_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    } else if (currentday.equalsIgnoreCase("Saturday")) {
                                        if (i == 6) {
                                            text.setBackgroundResource(R.drawable.yellow_border);
                                            secsat_text.setBackgroundResource(R.drawable.yellow_border);
                                        }
                                    }


                                    hor.addView(hiddenInfo2);
                                }
                                lin2.addView(hor);
                            }

                        }
                    }.execute();
                } else {
                    share.setVisibility(View.GONE);
                    secondalayout.setVisibility(View.GONE);

                }


            }
        }.execute();


    }

    @Override
    public int getlayout() {
        return R.layout.timetable;
    }

    public List<Timetablemodel> getdata(){
        List<Timetablemodel> list=new ArrayList<>();

        Timetablemodel timetablemodel=new Timetablemodel();
        timetablemodel.setData("9.30-10.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("10.30-11.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("11.45-12.45");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("02.00-03.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("03.00-04.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("9.30-10.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("10.30-11.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("11.45-12.45");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("02.00-03.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("03.00-04.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("9.30-10.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("10.30-11.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("11.45-12.45");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("02.00-03.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("03.00-04.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("9.30-10.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("10.30-11.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("11.45-12.45");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("02.00-03.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("03.00-04.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("9.30-10.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("10.30-11.30");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("11.45-12.45");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("02.00-03.00");
        list.add(timetablemodel);

        timetablemodel=new Timetablemodel();
        timetablemodel.setData("03.00-04.00");
        list.add(timetablemodel);
        return list;

    }

    public String getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:

                return "Sunday";
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";

            // etc.
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.timetable, menu);
        return true;
    }

    private void parseXML() {
        //  AssetManager assetManager = getBaseContext().getAssets();
        try {
            InputStream is = new FileInputStream(pref.getString("stafftimetable", ""));/*assetManager.open("1493791668.xml");*/
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            ItemXMLHandler myXMLHandler = new ItemXMLHandler();
            xr.setContentHandler(myXMLHandler);
            Log.d("inpurt", is.toString());
            InputSource inStream = new InputSource(is);
            xr.parse(inStream);

            is.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void parseXML2() {
        //  AssetManager assetManager = getBaseContext().getAssets();
        try {
            InputStream is = new FileInputStream(pref.getString("timetable", ""));/*assetManager.open("1493791668.xml");*/
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            ItemXMLHandler myXMLHandler = new ItemXMLHandler();
            xr.setContentHandler(myXMLHandler);
            Log.d("inpurt", is.toString());
            InputSource inStream = new InputSource(is);
            xr.parse(inStream);

            is.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        if (pref.getBoolean("dnd", false)) {
            dnd.setImageResource(R.drawable.active);

        } else {
            dnd.setImageResource(R.drawable.inactive);

        }
        if (LoginActivity.handraise!=null){
            if (LoginActivity.handraise.size() > 0) {
                handraise.setImageResource(R.drawable.handraiseenable);

                // Utils.Listpopup(BookBinActivity.this);
            } else {
                handraise.setImageResource(R.drawable.handrise);

                //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
            }
        }


    }

    public void stratExamCompletedPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        title.setText("Timetable SHARING");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                File file = new File(pref.getString("timetable", "0"));

                String senddata = "Timetable@" + pref.getString("bookbin", "0") + "@" + pref.getString("batchid", "0") + "@" + file.getName() + "@" + file.length();
                new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                if (MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(pref.getString("timetable", "0"));

                String senddata = "Timetable@" + pref.getString("bookbin", "0") + "@" + pref.getString("batchid", "0") + "@" + file.getName() + "@" + file.length();

                if (MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                //  String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                Intent in = new Intent(TimeTableActivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("senddata", senddata);
                startActivity(in);


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    class ItemXMLHandler extends DefaultHandler {

        Boolean currentElement = false;
        String currentValue = "";

        Subject sub;

        @Override
        public void startElement(String uri, String localName, String qName,
                                 Attributes attributes) throws SAXException {

            currentElement = true;
            currentValue = "";
            if (localName.equals("Time")) {
                sub = new Subject();
            } else if (localName.equals("Mon")) {
                common = new ArrayList<>();
            } else if (localName.equals("Tue")) {
                common = new ArrayList<>();
            } else if (localName.equals("Wed")) {
                common = new ArrayList<>();
            } else if (localName.equals("Thu")) {
                common = new ArrayList<>();
            } else if (localName.equals("Fri")) {
                common = new ArrayList<>();
            }


        }

        // Called when tag closing
        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {

            currentElement = false;

            /** set value */
            if (localName.equalsIgnoreCase("period")) {
                Subject sub = new Subject();
                sub.setSubjectname(currentValue);
                period.add(sub);
            } else if (localName.equals("subject")) {
                Subject sub = new Subject();
                sub.setSubjectname(currentValue);
                common.add(sub);
            } else if (localName.equalsIgnoreCase("Mon")) {
                mon.addAll(common);
                common.clear();

            } else if (localName.equalsIgnoreCase("Tue")) {
                tue.addAll(common);
                common.clear();

            } else if (localName.equalsIgnoreCase("Wed")) {
                wed.addAll(common);
                common.clear();

            } else if (localName.equalsIgnoreCase("Thu")) {
                thu.addAll(common);
                common.clear();

            } else if (localName.equalsIgnoreCase("Fri")) {
                firday.addAll(common);
                common.clear();
               /* sub.setSubjectname(currentValue);
                period.add(sub);*/
            }

        }

        // Called to get tag characters
        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {

            if (currentElement) {
                currentValue = currentValue + new String(ch, start, length);
            }

        }

    }

    private class validateUserTask extends com.artifex.mupdfdemo.AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;

        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public validateUserTask(Context context_ws,
                                ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(TimeTableActivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                Service sr = new Service(TimeTableActivity.this);
                jsonResponseString = sr.getLogin(login, Url.baseurl
                        /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;


        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            try {


                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success"))

                {
                    calendarpath = pref.getString("calender", "");
                    Log.e("calendar", calendarpath);


                    new com.artifex.mupdfdemo.AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {

                            try {


                                JSONObject schooinfo = jObj.getJSONObject("schoolInfo");//school
                                sch_details = new Staffdetails();

                                sch_details.setSchoolID(schooinfo.has("ID") ? Integer.parseInt(schooinfo.getString("ID")) : 0);
                                sch_details.setAcronym(schooinfo.has("SchoolAcronym") ? (schooinfo.getString("SchoolAcronym")) : "Dci");
                                // sch_details.setCityID();
                                //sch_details.setCountryID();
                                sch_details.setSchoolLogo(schooinfo.has("Logo") ? (schooinfo.getString("Logo")) : "Dci");
                                sch_details.setSchoolName(schooinfo.has("Name") ? (schooinfo.getString("Name")) : "Dci");
                                sch_details.setBackground(schooinfo.has("BackgroundImage") ? schooinfo.getString("BackgroundImage") : "");

//File creation


    /*if(!schoolname.exists())
    {*/

                                File schoolname = new File(Environment.getExternalStorageDirectory(), sch_details.getAcronym() + "_" + sch_details.getSchoolID());

                                schoolname.mkdirs();

                                File users = new File(schoolname.getAbsolutePath() + "/" + "Users");
                                users.mkdir();
                                File assessts = new File(schoolname.getAbsolutePath() + "/" + "Assets");
                                assessts.mkdir();


                                //}


                                String filename = sch_details.getSchoolLogo().substring(sch_details.getSchoolLogo().lastIndexOf("/") + 1, sch_details.getSchoolLogo().length());

                                String backgroundimage = sch_details.getBackground().substring(sch_details.getBackground().lastIndexOf("/") + 1, sch_details.getBackground().length());

                                try {
                                    downloadfile(sch_details.getSchoolLogo(), assessts.getAbsolutePath(), "logo_" + filename);
                                    downloadfile(sch_details.getBackground(), assessts.getAbsolutePath(), "back_" + backgroundimage);
                                } catch (Exception e) {
                                    Log.e("first", "first" + e.getMessage());
                                }


                                sch_details.setSchoolLogo(assessts.getAbsolutePath() + "/" + "logo_" + filename);
                                sch_details.setBackground(assessts.getAbsolutePath() + "/" + "back_" + backgroundimage);


                                JSONObject staffinfo = jObj.getJSONObject("staffInfo");

                                staffdetails = new Staffloingdetails();
                                staffdetails.setStaffID(staffinfo.has("ID") ? Integer.parseInt(staffinfo.getString("ID")) : 0);
                                staffdetails.setFirstName(staffinfo.has("FirstName") ? staffinfo.getString("FirstName") : "");
                                staffdetails.setDOJ(staffinfo.has("DateOfJoining") ? staffinfo.getString("DateOfJoining") : "");
                                staffdetails.setPortalLoginID(staffinfo.has("PortalLoginID") ? staffinfo.getString("PortalLoginID") : "");
                                //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
                                staffdetails.setLastName(staffinfo.has("LastName") ? staffinfo.getString("LastName") : "");
                                staffdetails.setGender(staffinfo.has("Gender") ? staffinfo.getString("Gender") : "");
                                staffdetails.setDOB(staffinfo.has("DateOfBirth") ? staffinfo.getString("DateOfBirth") : "");
                                staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus") ? staffinfo.getString("MaritalStatus") : "");
                                ;
                                staffdetails.setSpouseName(staffinfo.has("SpuseName") ? staffinfo.getString("SpuseName") : "");
                                staffdetails.setFatherName(staffinfo.has("FatherName") ? staffinfo.getString("FatherName") : "");
                                staffdetails.setMotherName(staffinfo.has("MotherName") ? staffinfo.getString("MotherName") : "");
                                staffdetails.setPhone((staffinfo.has("PhoneNumber") ? staffinfo.getString("PhoneNumber") : ""));
                                staffdetails.setPhotoFilename(staffinfo.has("ProfileImage") ? staffinfo.getString("ProfileImage") : "");
                                staffdetails.setEmail(staffinfo.has("Email") ? staffinfo.getString("Email") : "");
                                staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade") ? staffinfo.getString("EmploymentGrade") : "");
                                staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory") ? Integer.parseInt(staffinfo.getString("StaffCategory")) : 0);
                                staffdetails.setTimetable(staffinfo.has("TimeTable") ? staffinfo.getString("TimeTable") : "");

                                String staffroom = staffinfo.has("Room") ? staffinfo.getString("Room") : "";
                                staffroom = staffroom.replaceAll("\\[", "").replaceAll("\\]", "");
                                staffroom = staffroom.replace("\"", "");
                                //details.setRoominfo(subarray);

                                if (staffroom.isEmpty()) {
                                    staffdetails.setRoominfo("0");

                                } else {
                                    staffdetails.setRoominfo(staffroom);

                                }

                                File username = new File(users.getAbsolutePath() + "/" + "T_" + staffdetails.getStaffID());
                                username.mkdir();
                                File materials = new File(username.getAbsolutePath() + "/" + "Materials");
                                materials.mkdir();

                                File Academic = new File(materials.getAbsolutePath() + "/" + "Academic");
                                Academic.mkdir();

                                File Reference = new File(materials.getAbsolutePath() + "/" + "Reference");
                                Reference.mkdir();
                                File evaluation = new File(username.getAbsolutePath() + "/" + "Evaluation");
                                evaluation.mkdir();
                                File notes = new File(username.getAbsolutePath() + "/" + "Notes");
                                notes.mkdir();
                                File calender = new File(username.getAbsolutePath() + "/" + "Calendar");
                                calender.mkdir();

                                edit.putString("calender", calender.getAbsolutePath());
                                edit.putString("academic", Academic.getAbsolutePath());
                                edit.putString("notes", notes.getAbsolutePath());
                                edit.putString("reference", Reference.getAbsolutePath());
                                edit.putString("Evaluation", evaluation.getAbsolutePath());
                                edit.commit();
                                edit.commit();
                                calendarpath = pref.getString("calender", "");
                                Log.e("calendar", calendarpath);

                                String filename1 = staffdetails.getPhotoFilename().substring(staffdetails.getPhotoFilename().lastIndexOf("/") + 1, staffdetails.getPhotoFilename().length());

                                String filename2 = staffdetails.getTimetable().substring(staffdetails.getTimetable().lastIndexOf("/") + 1, staffdetails.getTimetable().length());

                                try {
                                    downloadfile(staffdetails.getPhotoFilename(), username.getAbsolutePath(), filename1);
                                    downloadfile(staffdetails.getTimetable(), username.getAbsolutePath(), filename2);
                                } catch (Exception e) {
                                    Log.e("second", "second" + e.getMessage());
                                }

                                staffdetails.setTimetable(username.getAbsolutePath() + "/" + filename2);


                                staffdetails.setPhotoFilename(username.getAbsolutePath() + "/" + filename1);

                                if (db.checkschooldetails(sch_details.getSchoolID()).getCount() > 0) {
                                    Log.e("update", "school");
                                    db.updateschooldetails(sch_details);

                                } else {
                                    db.schooldetails(sch_details);
                                }
                                /// db.getAllContacts();

                                if (db.checkstaffdetails(staffdetails.getStaffID()).getCount() > 0) {
                                    Log.e("update", "staff");

                                    db.updatestafflogindetails(staffdetails);
                                } else {
                                    db.stafflogindetails(staffdetails);
                                }
                                // db.getallstafflogindetails();


                                JSONObject getmaster = jObj.getJSONObject("masterInfo");
                                JSONArray getmasterarray = getmaster.getJSONArray("Subjects");
                                JSONArray getcalendararray = getmaster.getJSONArray("CalendarType");
                                JSONArray roomsarray = getmaster.getJSONArray("Rooms");
                                // ArrayList<Rooms> roomsarraylist=new ArrayList<Rooms>();
                                for (int r = 0; r < roomsarray.length(); r++) {
                                    Rooms room = new Rooms();
                                    JSONObject roomobject = roomsarray.getJSONObject(r);
                                    try {
                                        room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);

                                    } catch (Exception e) {
                                        room.setId(0);
                                    }
                                    room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                                    room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                                    room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");

                                    if (db.checkroomid(room.getId()).getCount() > 0) {
                                        Log.e("room", "update");
                                        db.updateaddmasterroom(room);

                                    } else {
                                        db.addmasterroom(room);
                                    }
                                }

                                //calendararrayl


                                for (int cal = 0; cal < getcalendararray.length(); cal++) {
                                    JSONObject getcal = getcalendararray.getJSONObject(cal);
                                    helper.Calendarpojo pojo = new helper.Calendarpojo();
                                    pojo.setCalendarname(getcal.has("Name") ? getcal.getString("Name") : "0");
                                    pojo.setCalendarid(getcal.has("ID") ? Integer.parseInt(getcal.getString("ID")) : 0);
                                    pojo.setCalendardescription(getcal.has("Description") ? getcal.getString("Description") : "0");

                                    String imagename = getcal.has("Image") ? getcal.getString("Image") : "0";

                                    String batchfile = imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());

                                    try {
                                        downloadfile(imagename, calendarpath, batchfile);

                                    } catch (Exception e) {
                                        Log.e("third", "third" + e.getMessage());
                                    }


                                    pojo.setCalendarimage(calendarpath + "/" + batchfile);


                                    if (db.checkcalendar(pojo.getCalendarid()).getCount() > 0) {
                                        Log.e("calendar", "update");

                                        db.updateaddcalendaremaster(pojo);
                                    } else {
                                        db.addcalendaremaster(pojo);

                                    }

                                }


                                Log.e("loading", "masterinfo");
                                for (int m = 0; m < getmasterarray.length(); m++) {
                                    JSONObject getm = getmasterarray.getJSONObject(m);
                                    Masterinfo minfo = new Masterinfo();
                                    minfo.setID(getm.has("ID") ? Integer.parseInt(getm.getString("ID")) : 0);
                                    minfo.setSubjectName(getm.has("SubjectName") ? getm.getString("SubjectName") : "");

                                    minfo.setSchoolID(getm.has("SchoolID") ? Integer.parseInt(getm.getString("SchoolID")) : 0);
                                    minfo.setStatus(getm.has("Status") ? getm.getString("Status") : "");
                                    minfo.setSubjectDescription(getm.has("SubjectDescription") ? getm.getString("SubjectDescription") : "");

                                    if (db.checksubjectid(minfo.getID()).getCount() > 0) {
                                        Log.e("masterinfo", "update");

                                        db.updatemasterinfo(minfo);
                                    } else {
                                        db.masterinfo(minfo);

                                    }
                      /*  "ID":"1",
                            "SubjectName":"English",
                            "SubjectDescription":"English - First Language",
                            "CreatedBy":"1",
                            "CreatedDate":"2017-04-18 00:00:00",
                            "ModifiedBy":"1",
                            "ModifiedDate":"2017-04-18 00:00:00",
                            "Status":"1",
                            "SchoolID":"1"*/

                                }

                                final JSONArray studentinfo = jObj.getJSONArray("studentDetails");

                                for (int s = 0; s < studentinfo.length(); s++) {
                                    JSONObject getstud = studentinfo.getJSONObject(s);

                                    helper.Studentdetails details = new helper.Studentdetails();


                                    details.setStudentLoginID(getstud.getString("StudentLoginID"));
                                    details.setStudentID(Integer.parseInt(getstud.getString("ID")));
                                    details.setAdmissionNumber(getstud.getString("AdmissionNumber"));

                                    details.setRollNo(getstud.getString("RollNumber"));

                                    details.setFirstName(getstud.getString("FirstName"));
                                    details.setLastName(getstud.getString("LastName"));
                                    details.setDOB(getstud.getString("DateOfBirth"));
                                    details.setDOA(getstud.getString("AdmissionDate"));
                                    details.setGender(getstud.getString("DateOfBirth"));
                                    details.setPhone_1(getstud.getString("Phone"));
                                    details.setFatherName(getstud.getString("FatherName"));
                                    details.setGuardianMobileNumber(getstud.getString("GuardianPhone"));
                                    details.setMotherName(getstud.getString("GuardianPhone"));
                                    details.setGuardianMobileNumber(getstud.getString("MotherName"));
                                    details.setEmail(getstud.getString("Email"));
                                    try {
                                        details.setBatchID(Integer.parseInt(getstud.getString("Batch")));
                                    } catch (Exception e) {
                                        details.setBatchID(0);
                                    }

                                    String subarray = getstud.getJSONArray("Room").toString();
                                    //  subarray=String str = "[Chrissman-@1]";
                                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                    subarray = subarray.replace("\"", "");
                                    if (subarray.isEmpty()) {
                                        details.setRoominfo("0");

                                    } else {
                                        details.setRoominfo(subarray);
                                    }

                                    details.setStaffID(staffdetails.getStaffID());
                                    //  details.setBatchID();
                                    details.setSchoolID(sch_details.getSchoolID());
                                    details.setPhotoFilename(getstud.getString("ProfileImage"));
                                    details.setClassID(Integer.parseInt(getstud.getString("ClassId")));
                                    String photofilename = "s_" + details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/") + 1, details.getPhotoFilename().length());


                                    try {
                                        Log.e("imagedownloading", photofilename);
                                        downloadfile(details.getPhotoFilename(), assessts.getAbsolutePath(), photofilename);
                                    } catch (Exception e) {
                                        Log.e("fourth", e.getMessage() + "fourth");
                                    }
                                    details.setPhotoFilename(assessts.getAbsolutePath() + "/" + photofilename/*details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/")+1,details.getPhotoFilename().length()*/);


                                    try {
                                        if (Integer.parseInt(details.getRollNo()) > 0) {

                                        }
                                    } catch (Exception e) {
                                        details.setRollNo("0");
                                    }
                                    if (db.checkstudentdetails(details.getStudentID()).getCount() > 0) {
                                        Log.e("update", "student");

                                        db.updateStudent(details);
                                    } else {
                                        db.Student(details);
                                    }

                     /*   "ID":"1",
                            "AdmissionNumber":"60",
                            "AdmissionDate":"2013-04-18",
                            "RollNumber":"1201",
                            "FirstName":"kumaran",
                            "LastName":"R",
                            "MiddleName":"",
                            "DateOfBirth":"2002-02-18",
                            "Gender":"Male",
                            "Phone":"9898989898",
                            "FatherName":"Kannan",
                            "AddressLine1":"1\/481, main road, somarasam pettai, neart axia ATM",
                            "GuardianPhone":"7402516995",
                            "MotherName":"Divya",
                            "Email":"test@gmail.com",
                            "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/students\/profile_image\/1.jpg",
                            "DeviceControl":"0",
                            "StudentLoginID":"EK-001-S0000000001",
                            "ClassId":"1"*/


                                }
                                db.deletesubject();
                                //   db.deletesubject(pref.getString("staffid","0"));


                                JSONArray classess = jObj.getJSONArray("Classes");
                                for (int c = 0; c < classess.length(); c++) {
                                    JSONObject getclassobj = classess.getJSONObject(c);
                                    tblclasses getclass = new tblclasses();
                                    getclass.setClassID(getclassobj.has("ID") ? Integer.parseInt(getclassobj.getString("ID")) : 0);
                                    getclass.setClassName(getclassobj.has("ClassName") ? (getclassobj.getString("ClassName")) : "");
                                    getclass.setGrade(getclassobj.has("Grade") ? (getclassobj.getString("Grade")) : "");
                                    getclass.setStaffID(staffdetails.getStaffID());

                                    getclass.setClassCode(getclass.getClassName() + "-" + (getclassobj.has("Section") ? (getclassobj.getString("Section")) : ""));
                                    String getclassname = getclass.getClassCode();
                                    getclass.setDepartmentID((getclassobj.has("Department") ? Integer.parseInt(getclassobj.getString("Department")) : 0));
                                    getclass.setInternetSSID(getclassobj.has("InternetSSID") ? (getclassobj.getString("InternetSSID")) : "");
                                    ;
                                    getclass.setInternetPassword(getclassobj.has("InternetPassword") ? (getclassobj.getString("InternetPassword")) : "");
                                    ;
                                    getclass.setInternetType(getclassobj.has("InternetType") ? (getclassobj.getString("InternetType")) : "");
                                    ;


                                    JSONArray batch = getclassobj.getJSONArray("batch");

                                    for (int b = 0; b < batch.length(); b++) {
                                        Batchdetails bdetails = new Batchdetails();
                                        JSONObject bobject = batch.getJSONObject(b);
                           /* "ID":"1",
                                "Subjects":[  ],
                            "Coordinator":"3",
                                "StartDate":"2017-04-18",
                                "EndDate":"2017-04-18",
                                "TimeTable":"http:\/\/schoolproject.dci.in\/images\/staffs\/time_table\/testtimetable.xml",
                                "AcademicYear":"2016-2017",
                                "subject":[  ]*/
                                        bdetails.setStaffID(staffdetails.getStaffID());

                                        bdetails.setSchoolID(sch_details.getSchoolID());
                                        bdetails.setTimeTable(bobject.has("TimeTable") ? bobject.getString("TimeTable") : "");
                                        bdetails.setAcademicYear(bobject.has("AcademicYear") ? bobject.getString("AcademicYear") : "");
                                        // getclass.setClassName(getclass.getClassName()+"-"+(getclassobj.has("Section")?(getclassobj.getString("Section")):"")+""+bdetails.getAcademicYear());
                                        bdetails.setBatchID(bobject.has("ID") ? Integer.parseInt(bobject.getString("ID")) : 0);
                                        bdetails.setClassID(getclass.getClassID());
                                        // bdetails.setTimeTable();
                                        getclass.setClassCode(getclassname + " " + bdetails.getAcademicYear());
                                        getclass.setBatchid(bdetails.getBatchID() + "");

                                        if (db.checkclasses(getclass.getClassID(), getclass.getSchoolID(), staffdetails.getStaffID()).getCount() > 0) {
                                            Log.e("update", "class");

                                            db.updatetbclasses(getclass);

                                        } else {
                                            db.tbclasses(getclass);

                                        }

                                        bdetails.setBatchcode(getclass.getClassCode() + "" + bdetails.getAcademicYear());

                                        String batchfile = bdetails.getTimeTable().substring(bdetails.getTimeTable().lastIndexOf("/") + 1, bdetails.getTimeTable().length());

                                        try {
                                            downloadfile(bdetails.getTimeTable(), username.getAbsolutePath(), batchfile);

                                        } catch (Exception e) {
                                            Log.e("third", "third" + e.getMessage());
                                        }


                                        bdetails.setTimeTable(username.getAbsolutePath() + "/" + batchfile);


                                        //  bdetails.set
                                        if (db.checkbatch(bdetails.getClassID(), bdetails.getBatchID(), staffdetails.getStaffID()).getCount() > 0) {
                                            Log.e("update", "batch");

                                            db.updatebatch(bdetails);

                                        } else {
                                            db.batch(bdetails);

                                        }
                                        if (b == 0) {
                                            Log.e("delete", "subject");

                                        }
                                        tblSubject subject = new tblSubject();

                                        subject.setClassID(bdetails.getClassID());
                                        subject.setBatchID(bdetails.getBatchID());
                                        subject.setSchoolID(bdetails.getSchoolID());
                                        subject.setStaffID(staffdetails.getStaffID());

                                        //  subject.setStaffID(bobject.has("Coordinator")?Integer.parseInt(bobject.getString("Coordinator")):0);


                                        if (bobject.has("subject")) {

                                            String subarray = bobject.getJSONArray("subject").toString();
                                            //  subarray=String str = "[Chrissman-@1]";
                                            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                            subarray = subarray.replace("\"", "");
                                            String sSplit[] = subarray.split(",");
                                            if (sSplit.length > 0) {
                                                for (int s = 0; s < sSplit.length; s++) {
                                                    subject.setSubjectID(Integer.parseInt(sSplit[s]));
                                                    db.subject(subject);

                                                }
                                                //   for(int s=0; s<subarray.length(); s++)

                                            } else {
                                                subject.setSubjectID(-1);
                                                db.subject(subject);

                                            }
                                        } else {
                                            subject.setSubjectID(-1);
                                            db.subject(subject);
                                        }


                                        // bdetails.sets

                                    }


                                }


                            } catch (Exception e) {

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            Toast.makeText(TimeTableActivity.this, "Updated Successfully", Toast.LENGTH_LONG).show();
                            //changemethod();
                            if (dia.isShowing())
                                dia.dismiss();
                        }
                    }.execute();

                } else {
                    Toast.makeText(TimeTableActivity.this, jsonResponse.toUpperCase(), Toast.LENGTH_LONG).show();
                    if (dia.isShowing())
                        dia.dismiss();
                }
            } catch (Exception e) {
                if (dia.isShowing())
                    dia.dismiss();
            }
        }

        void downloadfile(String urls, String filepath, String filename) {

            int count;
            try {
                //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
                URL url = new URL(urls);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream

                OutputStream output = new FileOutputStream(filepath
                        + "/" + filename);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }


        }

    }
}