package com.dci.edukool.teacher;


import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import Utilities.Utilss;
import models.Timetablemodel;

public class TimetableAdapter extends RecyclerView.Adapter<TimetableAdapter.MyViewHolder> {

    private List<Timetablemodel> TimetablemodelsList;
    Utilss utils;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);

        }
    }


    public TimetableAdapter(List<Timetablemodel> TimetablemodelsList, Context context) {
        this.TimetablemodelsList = TimetablemodelsList;
        utils=new Utilss((Activity) context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_time_table, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Timetablemodel Timetablemodel = TimetablemodelsList.get(position);
        holder.title.setText(Timetablemodel.getData());
        utils.setTextviewtypeface(5,holder.title);

    }

    @Override
    public int getItemCount() {
        return TimetablemodelsList.size();
    }
}