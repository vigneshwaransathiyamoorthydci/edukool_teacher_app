package com.dci.edukool.teacher;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.List;

import at.markushi.ui.CircleButton;
import helper.OnItemClickListener;
import helper.RoundedImageView;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.MyViewHolder> {
    private List<Integer> colorlist;
   OnItemClickListener listener;
   int pos=-1;
   Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        CircleButton imgcolor;
        public MyViewHolder(final View view) {
            super(view);
            //title = (TextView) view.findViewById(R.id.title);
            imgcolor= (CircleButton) view.findViewById(R.id.imgcolor);
            imgcolor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(v,getAdapterPosition());
                    pos=getAdapterPosition();
                }
            });
        }
    }


    public ColorAdapter(List<Integer> colorlist, Context context) {
        this.colorlist = colorlist;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listitem_color, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.imgcolor.setColor(colorlist.get(position));
        if (pos==position){
            holder.imgcolor.setBackground(context.getResources().getDrawable(R.drawable.round_border));
        }
        else {
            holder.imgcolor.setBackground(context.getResources().getDrawable(R.drawable.roundshape_white));
        }
        Log.d("dineshdata", "onBindViewHolder: "+"color"+String.valueOf(colorlist.get(position)));
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener=listener;
    }
    @Override
    public int getItemCount() {
        return colorlist.size();
    }
}
