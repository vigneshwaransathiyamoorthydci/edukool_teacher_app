package com.dci.edukool.teacher;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;

import adapter.Buzzeradapter;

/**
 * Created by abimathi on 01-Jun-17.
 */
public class Buzzerlistactivity extends Activity {
    ListView buzzerlist;
    ImageView buzzerimage;
    TextView buzzertext;
    TextView buzzerrollnumber;
    ImageView close;
    Buzzeradapter adpater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buzzerlayout);
        buzzerlist= (ListView) findViewById(R.id.buzzerlist);
        buzzerimage= (ImageView) findViewById(R.id.buzzerimage);
        buzzertext= (TextView) findViewById(R.id.buzzername);
        buzzerrollnumber= (TextView) findViewById(R.id.buzzerrollnumber);
        close= (ImageView) findViewById(R.id.close);
        adpater=new Buzzeradapter(Buzzerlistactivity.this,R.layout.buzzeritem,Quizzactivity.buzzer);
        buzzerlist.setAdapter(adpater);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Quizzactivity.buzzer.clear();
                finish();
            }
        });
        if(Quizzactivity.buzzer.size()>0) {
            putfirstitemselect();
        }

    buzzerlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        for (int i = 0; i < Quizzactivity.buzzer.size(); i++) {
            if (position == i) {
                Quizzactivity.buzzer.get(i).setBackground(true);
            } else {
                Quizzactivity.buzzer.get(i).setBackground(false);
            }
        }
        adpater.notifyDataSetChanged();
        buzzertext.setText(Quizzactivity.buzzer.get(position).getBuzzername().toUpperCase());

        if(Quizzactivity.buzzer.get(position).getBuzzerid().toUpperCase().equalsIgnoreCase("0"))
        {
            buzzerrollnumber.setText("-");
        }
        else {
            buzzerrollnumber.setText(Quizzactivity.buzzer.get(position).getBuzzerid().toUpperCase());
        }
        setbackground1(buzzerimage, Quizzactivity.buzzer.get(position).getBuzzerimage());

    }


});

    }
    void putfirstitemselect()
    {
            for (int i=0; i<Quizzactivity.buzzer.size(); i++)
            {
                if(i==0)
                {
                    Quizzactivity.buzzer.get(i).setBackground(true);
                }
                else
                {
                    Quizzactivity.buzzer.get(i).setBackground(false);
                }
            }


        adpater.notifyDataSetChanged();
        buzzertext.setText(Quizzactivity.buzzer.get(0).getBuzzername().toUpperCase());
        if(Quizzactivity.buzzer.get(0).getBuzzerid().toUpperCase().equalsIgnoreCase("0"))
        {
            buzzerrollnumber.setText("-");
        }
        else {
            buzzerrollnumber.setText(Quizzactivity.buzzer.get(0).getBuzzerid().toUpperCase());
        }
        setbackground1(buzzerimage, Quizzactivity.buzzer.get(0).getBuzzerimage());
    }
    void  setbackground1(ImageView view,String filepath)
    {
        File imgFile=new File(filepath);
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }
}
