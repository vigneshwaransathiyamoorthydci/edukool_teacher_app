package com.dci.edukool.teacher;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.os.Environment;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.AsyncTask;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import Utilities.DatabaseHandler;
import Utilities.Utilss;
/*import adapter.Customadapter;*/
import books.Attendace;
import books.BirthdayActivity;
import books.Downspinner;
import books.Pulse;
import books.PulseQuestionActivity;
import books.TimerPulseActivity;
import books.VideoBinActivity;
import calendar.Calendaractivity;
import connection.Communicationclient;
import connection.Communicationsend;
import connection.Violationclient;
import connection.Violationreceive;
import drawboard.Whiteboard;
import Wificonnectivity.WifiBase;
import books.BookBinActivity;
import connection.Examzipfile;
import connection.Filesharingtoclient;
import connection.MultiThreadChatServerSync;
import connection.Serversocket;
import connection.clientThread;
import exam.ExamResponsePojo;
import exam.ExamTeacherHome;
import exam.PulseResponsePojo;
import exam.StudentQuestionResultPojo;
import handraise.Handraise;
import helper.*;
import books.Upperspinner;
import Utilities.Attendancedb;
import helper.Studentdetails;
import messaging.CommunicationActivity;
import messaging.Communicationforrooms;
import pulseexam.PulseExamPojo;
import workbook.WorkbookActivity;
import Utilities.*;

/*
 * Permission needed:
 * <uses-permission android:name="android.permission.INTERNET"/>
 * <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
 */
public class MainActivity extends BaseActivity implements
        WifiBase.WifiBaseListener {
    static final int SocketServerPORT = 8080;
    public static List<String> answerA = new ArrayList<String>();
    public static List<String> answerB = new ArrayList<String>();
    public static List<String> answerC = new ArrayList<String>();
    public static List<String> answerD = new ArrayList<String>();
    public static List<String> none = new ArrayList<String>();
    public static Examzipfile examzipfile = null;
    public static Filesharingtoclient startshare = null;
    private final Handler handler = new Handler();
    // TextView infoIp, infoPort;
    int SurveyPulseID;
    int SurveyPulseQuestionID;
    ProgressDialog dia;
    ServerSocket serverSocket;
    ArrayList<Socket> soc;
    String ip;
    int PulseQuestionID;
    String PulseType;
    TextView title;
    String PulseQuestion;
    String CorrectAnswerpulse;
    String IsActive;
    int VersionPulse = 1;
    int StaffIDPluse;
    int SchoolIDPulse;
    int IsActivePulse;
    String CreatedOnPluse;
    String UpdatedOn;
    // TextView message;
    String getclient;
    ServerSocketThread serverSocketThread;
    Serversocket server;
    /*
        public static Filesharingtoclient startshare;
    */
    String MY_PREFS_NAME = "EXAMDETAILS";
    int reconnect = 0;
    Socket socket = null;
    int i;
    String calendarpath;
    LinearLayout attendance;
    Examzipfile zipfile;
    MultiThreadChatServerSync multi = null;
    LinearLayout bookbin, workbook;
    ImageView logout;
    LinearLayout communication,
            whiteboard, exam, timetable, calendar,
            multimedia, birthday,log_lay;
    //int dnd=0;
    int count;
    RoundedImageView profileimage;
    TextView studentname;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    BroadcastReceiver wifilisetner;
    ImageView schoollogo, backgroundlogo;
    //ImageLoader imageLoader;
    DatabaseHandler dp;
    Spinner upperspinner;
    Spinner downspinner;
    Upperspinner upper;
    ArrayList<tblclasses> gettable;
    ArrayList<Subjectnameandid> subjectname;
    Staffdetails details;
    Staffloingdetails logindetails;
    TextView classname;
    DatabaseHandler db;
    int ExamResponseIDval;
    TextView bookbintext;
    TextView multimediatext;
    TextView whitboardtext;
    TextView examtext,tv_logout;
    TextView timetabletext;
    TextView attendancetext;
    TextView communicatuiontext;
    TextView calendertext;
    Utilss utils;
    Attendancedb attenddb;
    ImageView handraise;
    BroadcastReceiver lockreceiver;
    ImageView dnd, opendraw;
    Communicationsend reply;
    Violationreceive violationreceiver;
    ImageView deviceviolation;
    Staffdetails sch_details;
    Staffloingdetails staffdetails;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    LinearLayout drawer;
    BroadcastReceiver changeroom;
    ArrayList<Rooms> overallrooms = new ArrayList<>();
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    Spinner roomspinner;
    WifiManager mainWifi;
    WifiReceiver receiverWifi;
    ArrayList<User> arrayOfUsers;
    UsersAdapter adapter;
    ListView listView;
    String wifipass = "";
    LabeledSwitch labeledSwitch;
    LinearLayout sidelay;
    StringBuilder sb = new StringBuilder();
    String breakstate;
    ImageView online;
    boolean firstime;
    TextView username, class_name, sessionbreak, tv_subject;
    String res;
    Violationmessage vmsg = new Violationmessage() {
        @Override
        public void onMessageReceived(String paramString) {
            Log.e("teacher received", paramString.toString());
            try {
                //violation@@SANDHYA S@@192.168.1.104@@[com.dci.edukool.student, android,

                String splitofviolation[] = paramString.split("@@");
                Set<String> set = pref.getStringSet("getviolation", null);
                if (set == null) {
                    set = new HashSet<>();
                }
                set.add(paramString);
                edit.putStringSet("getviolation", set);
                edit.commit();
                new Violationclient(splitofviolation[2], "").run();
            } catch (Exception e) {

            }
        }
    };

    Message msg = new Message() {
        @Override
        public void onMessageReceived(final String getmessage) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    // Toast.makeText(MainActivity.this, getmessage, Toast.LENGTH_LONG).show();
                    String signal = "Signal";
                    if (getmessage.contains("invS")) {
                        try {
                            /* synchronized (this) {
                             */
                            String studendetails[] = getmessage.split("\\@");
                            final Studentinfo info = new Studentinfo();
                            info.setStudentip(studendetails[2]);
                            info.setStudentrollnumber(studendetails[3]);
                            info.setStudentschooid(studendetails[4]);
                            info.setStudentname(studendetails[5]);
                            info.setStudentid(studendetails[6]);
                            Cursor c = dp.retriveIsSentNot(Integer.parseInt(info.getStudentid()));
                            if (c.getCount() > 0) {
                                while (c.moveToNext()) {
                                    int msgid = c.getInt(c.getColumnIndex("MsgID"));
                                    Cursor rec = dp.retriveMessages(msgid);
                                    if (rec.getCount() > 0) {
                                        while (c.moveToNext()) {
                                            final JSONObject send = new JSONObject();
                                            send.put("MsgID", "" + c.getInt(c.getColumnIndex("MsgID")));
                                            send.put("ReceiverID", Integer.parseInt(info.getStudentid()));
                                            send.put("SenderID", pref.getString("staffid", "0"));
                                            send.put("DateOfCommunication", c.getString(c.getColumnIndex("DateOfCommunication")));
                                            send.put("Title", c.getString(c.getColumnIndex("Title")));
                                            send.put("Content", c.getString(c.getColumnIndex("Content")));
                                            send.put("SubjectID", c.getInt(c.getColumnIndex("SubjectID")));
                                            send.put("Action", "RCVMSG");

                                            new AsyncTask<Void, Void, Void>() {

                                                @Override
                                                protected Void doInBackground(Void... params) {
                                                    Communicationclient soc = new Communicationclient(info.getStudentip(), "Circular@" + send.toString());
                                                    soc.run();
                                                    return null;
                                                }
                                            }.execute();
                                        }
                                    }
                                }
                            }
                            //}
                        }
                        /*
                                "CommunicationID integer primary key AUTOINCREMENT,\n" +
                                "SenderID integer,\n" +

                                "DateOfCommunication datetime,\n" +
                                "Title varchar,\n" +
                                "Content varchar,\n" +
                                "BatchID integer,\n" +
                                "CreatedOn datetime,\n" +
                                "UpdatedOn datetime,\n" +
                                "StaffID integer,\n" +
                                "SchoolID integer," +
                                "SubjectID integer"+*/ catch (Exception e) {

                        }
                      /*  String studendetails[] = getmessage.split("\\@");
                        Studentinfo info = new Studentinfo();
                        info.setStudentip(studendetails[2]);
                        info.setStudentrollnumber(studendetails[3]);
                        info.setStudentschooid(studendetails[4]);
                        info.setStudentname(studendetails[5]);
                        info.setStudentid(studendetails[6]);


                        Attendancepojo details=new Attendancepojo();
                        details.setSchoolID(Integer.parseInt(pref.getString("schoolid","0")));
                        details.setBatchID(Integer.parseInt(pref.getString("batchid", "0")));
                        details.setRollNo(Integer.parseInt(info.getStudentrollnumber()));
                        details.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
                        details.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));
                        details.setStudentID(Integer.parseInt(info.getStudentid()));
                        details.setAttendanceDateTime(studendetails[7]);
                        if(!attenddb.attendancecheckpojo(details.getRollNo(),details.getBatchID(),details.getSubjectID()))
                        attenddb.attendancetaken(details);
                        else
                            attenddb.updateattendance(details);*/

                    } else if (getmessage.contains("PulseAnswer")) {
                        try {
                            List<String> getmsg = Arrays.asList(getmessage.split("@"));
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date date = new Date();

                            String Answer = getmsg.get(2);
                            int QuestionId = Integer.parseInt(getmsg.get(1));
                            int StudentId = Integer.parseInt(getmsg.get(3));
                            String createdDate = dateFormat.format(date);
                            String UpdatedDate = dateFormat.format(date);

                            db.addPuseResponse(new PulseResponsePojo(QuestionId, StudentId, Answer, createdDate, UpdatedDate));
                            synchronized (this) {
                                if (Answer.equalsIgnoreCase("1")) {
                                    Pulse.AnswerA++;
                                } else if (Answer.equalsIgnoreCase("2")) {
                                    Pulse.AnswerB++;
                                } else if (Answer.equalsIgnoreCase("3")) {
                                    Pulse.AnswerC++;
                                } else if (Answer.equalsIgnoreCase("4")) {
                                    Pulse.AnswerD++;
                                } else if (Answer.equalsIgnoreCase("5")) {
                                    Pulse.none++;
                                }
                                Pulse.totalResponse++;
                            }


                            TimerPulseActivity.questionID.add(SurveyPulseQuestionID);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (getmessage.contains("StudentResponseID")) {

                        try {
                            JSONObject emp = (new JSONObject(getmessage)).getJSONObject("Result");
                            String DateAttended = emp.getString("DateAttended");
                            int TotalScore = emp.getInt("TotalScore");
                            //int TimeTaken=emp.getInt("TimeTaken");
                            int TimeTaken = 1;
                            int ExamResponseID = emp.getInt("ExamResponseID");
                            int ExamID = emp.getInt("ExamID");
                            int StudentID = emp.getInt("StudentID");
                            String SrtudentName = emp.getString("StudentName");
                            JSONArray StudentResponse = emp.getJSONArray("StudentResponse");

                            boolean isexaist = db.CheckIsDataAlreadyInDBorNot(StudentID, ExamID);

                            if (isexaist) {
                                db.updateExamDetails(new ExamResponsePojo(ExamResponseID, ExamID, StudentID, 00111, SrtudentName, TimeTaken, DateAttended, TotalScore));
                                List<ExamResponsePojo> examresp = db.getallresponse();
                                for (ExamResponsePojo cn : examresp) {
                                    String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getExamID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                    Log.d("ExamResponfa: ", log);
                                }
                                List<ExamResponsePojo> examrespojo = db.addStudentExamResponseUsingExamId(ExamID, StudentID);
                                for (ExamResponsePojo cn : examrespojo) {
                                    String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getExamID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                    // Writing Contacts to log
                                    Log.d("Exam2: ", log);
                                    ExamResponseIDval = cn.getExamResponseID();

                                }


                                for (int i = 0; i < StudentResponse.length(); i++) {
                                    JSONObject c = StudentResponse.getJSONObject(i);
                                    int MarkForAnswer = c.getInt("MarkForAnswer");
                                    String StudentAnswer = c.getString("StudentAnswer");
                                    int IsCorrect = c.getInt("IsCorrect");
                                    int QuestionID = c.getInt("QuestionID");
                                    int StudentResponseID = c.getInt("StudentResponseID");
                                    int ObtainedMark = c.getInt("ObtainedMark");
                                    db.updateResponseDetails(new StudentQuestionResultPojo(StudentResponseID, ExamResponseIDval, QuestionID, StudentAnswer, IsCorrect, MarkForAnswer, ObtainedMark));
                                    Log.d("update ot not", "" + db.updateResponseDetails(new StudentQuestionResultPojo(StudentResponseID, ExamResponseIDval, QuestionID, StudentAnswer, IsCorrect, MarkForAnswer, ObtainedMark)));


                                }


                                // Toast.makeText(getApplicationContext(), "isexaist"+isexaist, Toast.LENGTH_SHORT).show();
                            } else {
                                db.addStudentExamResponse(new ExamResponsePojo(ExamResponseID, ExamID, StudentID, 00111, SrtudentName, TimeTaken, DateAttended, TotalScore));
                                List<ExamResponsePojo> examrespojo = db.addStudentExamResponseUsingExamId(ExamID);
                                for (ExamResponsePojo cn : examrespojo) {
                                    String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getExamID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                    // Writing Contacts to log
                                    Log.d("Exam2: ", log);
                                    ExamResponseIDval = cn.getExamResponseID();
                                }
                                for (int i = 0; i < StudentResponse.length(); i++) {
                                    JSONObject c = StudentResponse.getJSONObject(i);
                                    int MarkForAnswer = c.getInt("MarkForAnswer");
                                    String StudentAnswer = c.getString("StudentAnswer");
                                    int IsCorrect = c.getInt("IsCorrect");
                                    int QuestionID = c.getInt("QuestionID");
                                    int StudentResponseID = c.getInt("StudentResponseID");
                                    int ObtainedMark = c.getInt("ObtainedMark");

                                    db.addtblStudentQuestionResult(new StudentQuestionResultPojo(StudentResponseID, ExamResponseIDval, QuestionID, StudentAnswer, IsCorrect, MarkForAnswer, ObtainedMark));
                                }

                                //  Toast.makeText(getApplicationContext(), "isexaist"+isexaist, Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (getmessage.startsWith("ACK")) {
                        try {
                            String Split[] = getmessage.split("\\@");
                            final JSONObject object = new JSONObject(Split[1]);

                            if (object.getString("Action").equalsIgnoreCase("RCVMSG")) {
                                db.UpdateMap(Integer.parseInt(object.getString("MsgID")), Integer.parseInt(object.getString("ReceiverID")));

                            } else {
                                // { Action = "RCVACK", MsgID = msgID, Status = 3, ReceiverID = receiverID, SubjectID = subjectID }

                                final JSONObject sendobj = new JSONObject();
                                sendobj.put("Action", "RCVACK");
                                sendobj.put("MsgID", object.getString("MsgID"));
                                sendobj.put("Status", object.getString("Status"));
                                sendobj.put("ReceiverID", (object.getString("ReceiverID")));
                                sendobj.put("SubjectID", object.getString("SubjectID"));


                                new AsyncTask<Void, Void, Void>() {

                                    @Override
                                    protected Void doInBackground(Void... params) {

                                        Communicationclient soc = null;
                                        try {
                                            soc = new Communicationclient(object.getString("IPAddress"), "RCVACK@" + sendobj.toString());
                                            soc.run();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        return null;
                                    }
                                }.execute();

                                //   new Communication(object.getString("IPAddress"),sendobj.toString()).start();


                                db.UpdateMapack(Integer.parseInt(object.getString("MsgID")), Integer.parseInt(object.getString("ReceiverID")));

                            }


                            ///  Action = "ACK", MsgID = msgID, Status = 3, ReceiverID = receiverID, SubjectID = subjectID, IPAddress = MyIPAddress }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (getmessage.contains("Receive")) {
                        Intent in = new Intent("StudentCount");
                        Bundle mBundle = new Bundle();
                        mBundle.putString("Receive", getmessage);
                        in.putExtras(mBundle);
                        sendBroadcast(in);
                        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                        int studentcount = prefs.getInt("studentcount", 0) + 1;

                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putInt("studentcount", studentcount);
                        editor.commit();

                    } else if (getmessage.toLowerCase().startsWith(signal.toLowerCase())) {
                        // messagelistener.onMessageReceived(message);


                        try {
                            Handraise raise = new Handraise();
                            String Splitof[] = getmessage.split("\\@");
                            raise.setStudentname(Splitof[1]);
                            raise.setStudentid(Splitof[2]);
                            raise.setStudentrollnumber(Splitof[3]);
                            raise.setIp(Splitof[4]);

                            boolean update = false;

                            for (int i = 0; i < LoginActivity.handraise.size(); i++) {
                                if (raise.getStudentid().equalsIgnoreCase(LoginActivity.handraise.get(i).getStudentid())) {
                                    update = true;
                                    LoginActivity.handraise.remove(i);
                                    LoginActivity.handraise.add(i, raise);
                                    break;
                                }
                            }

                            if (!update)
                                LoginActivity.handraise.add(raise);
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "Handraise error", Toast.LENGTH_SHORT).show();
                        }
                        if (LoginActivity.handraise.size() > 0) {
                            handraise.setImageResource(R.drawable.raise_sel);
                        } else {
                            handraise.setImageResource(R.drawable.raise_unsel);

                        }


                        Intent in = new Intent("handraise");
                        sendBroadcast(in);
                    } else if (getmessage.toLowerCase().startsWith("buzz")) {
                        /*  synchronized (this) {*/

                           /* if (!pref.getBoolean("quizz", false)) {
                                edit.putBoolean("quizz", true);
                                edit.commit();*/

                        String splitofquizz[] = getmessage.split("\\@");
                        helper.Studentdetails details = db.getstudentdetailsbyrollnumber(splitofquizz[1]);
                        Buzzerhelper buzzer = new Buzzerhelper();
                        buzzer.setBuzzerid(details.getRollNo());
                        buzzer.setBuzzerimage(details.getPhotoFilename());
                        buzzer.setBuzzername(details.getFirstName());
                        Quizzactivity.buzzer.add(buzzer);


                            /*  try {
                                  Utils.buzzer(MainActivity.this, details);
                              }
                              catch (Exception e)
                              {

                              }*/
                                /*sendBroadcast(new Intent("closealert"));
                                String senddata = "Quiz@reset";
                                new clientThread(MultiThreadChatServerSync.thread, senddata).start();*/


                        // }
                        // }
                    }


                    //  Toast.makeText(MainActivity.this, getmessage, Toast.LENGTH_LONG).show();
                }
            });

            Log.e("coming", "coning");
        }
    };
    // Button sync;
    private Broadcastlistener broadcastUtils;
    BroadcastMessageListener broadcast = new BroadcastMessageListener() {
        @Override
        public void onMessageReceived(String paramString) {
            Log.e("coming", "coning");
            if (paramString.contains("invS")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        connect();
                    }
                });

            }
        }
    };
    private BroadcastMessageListener broadcastMessageListener;
    private ScheduledExecutorService scheduleTaskExecutor;
    private WifiBase mWifiBase;
    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private PopupWindow pwindo, listwindo;

    public static String getCurrentSsid(Context context, WifiManager wifiManager) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }

    @Override
    public int getlayout() {
        return R.layout.dashboardnewdash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // super.onAttachedToWindow();
        super.onCreate(savedInstanceState);
        spinner_lay.setVisibility(View.VISIBLE);
        home_lay.setBackgroundColor(getResources().getColor(R.color.oldbg));
        home_icon.setImageDrawable(getResources().getDrawable(R.drawable.homes));
        tv_home.setTextColor(getResources().getColor(R.color.white));
        log_lay=(LinearLayout)findViewById(R.id.log_lay);
        sidelay = (LinearLayout) findViewById(R.id.sidelay);
        wifilay.bringToFront();
        wifilay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getBoolean("break", false)) {
                    if (Build.VERSION.SDK_INT == 24 || Build.VERSION.SDK_INT == 25 ) {
                        createLocationRequest();
                    } else {
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0x12345);
                            } else {

                            }
                        }*/
                        Listpopup(); // the actual wifi scanning
                    }
                }
                else {
                    sessionBreak();
                }

            }
        });

        quizelay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                }
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {
                    startActivity(new Intent(MainActivity.this, Quizzactivity.class));
                  /*  Videoname video = videos.get(position);

                    File file = new File(video.getVideopath());



                    stratExamCompletedPopup(video);*/
                } else {
                    Utilss.lockpoopup(MainActivity.this, "EduKool",
                            "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }
            }
        });





        //  imageLoader = NetworkController.getInstance().getImageLoader();
        //  dp=new DatabaseHandler(this);
        attendance = (LinearLayout) findViewById(R.id.attendacelinear);
        username = (TextView) findViewById(R.id.username);
        class_name = (TextView) findViewById(R.id.class_name);
        title= (TextView) findViewById(R.id.title);

//        tv_home = (TextView) findViewById(R.id.tv_home);
//        tv_pulse = (TextView) findViewById(R.id.tv_pulse);
//        tv_quize = (TextView) findViewById(R.id.tv_quize);
//        tv_wifi = (TextView) findViewById(R.id.tv_wifi);
//        tv_bright = (TextView) findViewById(R.id.tv_bright);
//        tv_update = (TextView) findViewById(R.id.tv_update);
        tv_subject = (TextView) findViewById(R.id.tv_subject);
        sessionbreak = (TextView) findViewById(R.id.sessionbreak);
        tv_logout= (TextView) findViewById(R.id.tv_logout);
        workbook=(LinearLayout)findViewById(R.id.workbook);
        spinner_lay.setVisibility(View.VISIBLE);

        drawer = (LinearLayout) findViewById(R.id.drawer);
        communication = (LinearLayout) findViewById(R.id.communicationlinear);
        whiteboard = (LinearLayout) findViewById(R.id.whiteboard);
        exam = (LinearLayout) findViewById(R.id.examlin);
        multimedia = (LinearLayout) findViewById(R.id.multimedia);
        timetable = (LinearLayout) findViewById(R.id.timetablelin);
        calendar = (LinearLayout) findViewById(R.id.calendarlin);
        logout = (ImageView) findViewById(R.id.logout);
        schoollogo = (ImageView) findViewById(R.id.schoollogo);
        opendraw = (ImageView) findViewById(R.id.opendraw);
        classname = (TextView) findViewById(R.id.class_name);
        backgroundlogo = (ImageView) findViewById(R.id.backgroundlogo);
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        studentname = (TextView) findViewById(R.id.teachename);
        upperspinner = (Spinner) findViewById(R.id.upperspinner);
        roomspinner = (Spinner) findViewById(R.id.roomspinner);
        downspinner = (Spinner) findViewById(R.id.downspinner);
        bookbintext = (TextView) findViewById(R.id.bookbintext);
        bookbin = (LinearLayout) findViewById(R.id.bookbin);
        multimediatext = (TextView) findViewById(R.id.bookbintext);
        whitboardtext = (TextView) findViewById(R.id.whitboardtext);
        examtext = (TextView) findViewById(R.id.examtext);
        online = (ImageView) findViewById(R.id.staus);
        timetabletext = (TextView) findViewById(R.id.timetabletext);
        attendancetext = (TextView) findViewById(R.id.attendancetext);
        communicatuiontext = (TextView) findViewById(R.id.communicatuiontext);
        calendertext = (TextView) findViewById(R.id.calendertext);
        handraise = (ImageView) findViewById(R.id.handrise);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        deviceviolation = (ImageView) findViewById(R.id.exclematry);
        dnd = (ImageView) findViewById(R.id.dnd);
        birthday = (LinearLayout) findViewById(R.id.birthdaylin);
        firstime = true;
        edit = pref.edit();
        overallrooms.clear();
        utils = new Utilss(MainActivity.this);
        db = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);
        dp = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);
        // db=new DatabaseHandler(this);
        //attenddb=new Attendancedb(this);
        attenddb = new Attendancedb(this, pref.getString("attendancedb", ""), Attendancedb.DATABASE_VERSION);
        classname.setText("CLASS : " + pref.getString("classname", ""));
        utils.setTextviewtypeface(2, bookbintext);
        utils.setTextviewtypeface(2, multimediatext);
        utils.setTextviewtypeface(2, whitboardtext);
        utils.setTextviewtypeface(2, examtext);
        utils.setTextviewtypeface(2, attendancetext);
        utils.setTextviewtypeface(2, communicatuiontext);
        utils.setTextviewtypeface(2, calendertext);
        utils.setTextviewtypeface(4, title);

        utils.setTextviewtypeface(3, sessionbreak);
        utils.setTextviewtypeface(3, username);
        utils.setTextviewtypeface(5, class_name);
        utils.setTextviewtypeface(0, tv_subject);
//        utils.setTextviewtypeface(5, tv_home);
//        utils.setTextviewtypeface(5, tv_pulse);
//        utils.setTextviewtypeface(5, tv_bright);
//        utils.setTextviewtypeface(5, tv_update);
//        utils.setTextviewtypeface(5, tv_wifi);
//        utils.setTextviewtypeface(5, tv_bright);
//        utils.setTextviewtypeface(6, tv_logout);


        getSupportActionBar().hide();

        changemethod();
        pulse_lay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                }
                if (!pref.getString("roomname", "").
                        equalsIgnoreCase("Session Break")) {
                    Intent intent = new Intent(MainActivity.this, PulseQuestionActivity.class);
                    startActivity(intent);
                } else {
                    Utilss.lockpoopup(MainActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }
            }
        });
        whiteboard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;


                }
                startActivity(new Intent(MainActivity.this, Whiteboard.class));
            }
        });
        log_lay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (breakstate.isEmpty()) {
//
//
//                    logoutfunction();
//                    // finish();
//                } else
                    if (pref.getBoolean("break", false)) {
                    logoutfunction();
                    // finish();
                } else if (pref.getBoolean("break", false)==false) {
                    Toast.makeText(MainActivity.this,
                            "You can exit from the classroom only during session breaks.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        exam.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                }
                //assessment
                Intent intent = new Intent(MainActivity.this, ExamTeacherHome.class);
                startActivity(intent);

            }
        });

        workbook.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;

                    //workbook
                }
                // toast();
                startActivity(new Intent(MainActivity.this, WorkbookActivity.class));

            }
        });


        i = 0;
        // infoIp.setText(getIpAddress());
        // Toast.makeText(MainActivity.this,getIpAddress(),Toast.LENGTH_LONG).show();
        soc = new ArrayList<>();
        soc.clear();

        Permission.verifyStoragePermissions(this);
        initialize();

        multimedia.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                }
                // reference
                startActivity(new Intent(MainActivity.this, VideoBinActivity.class));

            }
        });
        timetable.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;

                    //timetable
                }
                //toast();
                startActivity(new Intent(MainActivity.this, TimeTableActivity.class));

            }
        });
        calendar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                    //calendar
                }
                if ((!pref.getBoolean("break", false))) {
                    startActivity(new Intent(MainActivity.this, Calendaractivity.class));

                    /*if (!pref.getBoolean("roomornot", false)) {
                        Intent in = new Intent(MainActivity.this, CommunicationActivity.class);
                        startActivity(in);
                    } else {
                        Intent in = new Intent(MainActivity.this, Communicationforrooms.class);
                        startActivity(in);
                    }*/
                } else {
                    Toast.makeText(MainActivity.this, "You can access this feature once you come out of the session break.", Toast.LENGTH_SHORT).show();
                }

            }
        });
        attendance.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                    //attendance

                }
                startActivity(new Intent(MainActivity.this, Attendace.class));

                // toast();

              /*  File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "screenshots.zip");


                String senddata="Examfile"+"@"+file.getName()+"@"+file.length();
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();


                if (zipfile == null) {
                    zipfile = new Examzipfile(file);
                    zipfile.start();


                } else {
                    zipfile.stopsocket();
                    zipfile = null;
                    zipfile = new Examzipfile(file);
                    zipfile.start();
                }
*/
            }
        });

//        wifilay.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Listpopup();
//
//            }
//        });
     /*   update_lay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String osversion = Build.VERSION.RELEASE;
                String devname = android.os.Build.MODEL;
                String ostype = "Android";
                int versionCode = BuildConfig.VERSION_CODE;
                String versionName = BuildConfig.VERSION_NAME;

                String additionalparams = "|Device Type:" + ostype + "|GCMKey:" + "" +
                        "|DeviceID:" + "" +
                        "|AppID:" + versionCode +
                        "|IMEINumber:" + "" +
                        "|AppVersion:" + versionName +
                        "|MACAddress:" + "" +
                        "|OSVersion:" + osversion;
                String login_str = "UserName:" + pref.getString("teachername", "") + "|Password:" + pref.getString("teacherpass", "") + "|Function:StaffLogin|Update:Yes" + additionalparams;
                login.clear();
                byte[] data;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        login.clear();
                        login.add(new BasicNameValuePair("WS", base64_register));

                        validateUserTask load_plan_list = new validateUserTask(MainActivity.this, login);
                        load_plan_list.execute();
                    } else {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        });*/


        communication.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //circular

                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                    //academic
                }

                Intent in = new Intent(MainActivity.this, CommunicationActivity.class);
                startActivity(in);

               /* if ((!pref.getBoolean("break", false))) {
                    if (!pref.getBoolean("roomornot", false)) {
                        Log.d("roomdata", "onClick: "+"room");
                        Intent in = new Intent(MainActivity.this, CommunicationActivity.class);
                        startActivity(in);
                    } else {
                        Intent in = new Intent(MainActivity.this, Communicationforrooms.class);
                        startActivity(in);
                    }
                } else {
                    sessionBreak();
                }*/

                // toast();

               /* String senddata = "DND";
                if(dnd%2==0)
                {
                    senddata=senddata+"@true";
                }
                else
                {
                    senddata=senddata+"@false";

                }
                dnd=dnd+1;
                new clientThread(MultiThreadChatServerSync.thread, senddata).start();*/

            }
        });
        labeledSwitch = (LabeledSwitch)findViewById(R.id.switcher);

//        if(pref.getBoolean("break",false)==false){
//            labeledSwitch.setOn(false);
//            edit.putBoolean("break", false);
//
//        }
//        else {
//            edit.putBoolean("break", true);
//            labeledSwitch.setOn(true);
//        }
        labeledSwitch.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                if (!isOn) {
                    sidelay.setVisibility(View.VISIBLE);
                    linearDashboard.setAlpha(1);
                    edit.putBoolean("roomornot", true);
                    edit.putBoolean("break", false);

//                     edit.putString("roomid", "" + gettable.get(position).getClassID());
//                     edit.putString("roompassword", gettable.get(position).getInternetPassword());
//                     edit.putString("roomssid", gettable.get(position).getInternetSSID());
//                     edit.putString("roomname", gettable.get(position).getClassName());
                    edit.commit();
                    //  mWifiBase = new WifiBase(MainActivity.this);

                //    subjectname = dp.getbatchdetailsfrommasterinfo();


                } else {
                    sidelay.setVisibility(View.GONE);
                    // edit.putString("roomname", gettable.get(position).getClassName());
//                    upperspinner.setVisibility(View.VISIBLE);
//                    upperspinner.setSelection(0);
//                    upperspinner.setVisibility(View.GONE);
                    linearDashboard.setAlpha(0.5F);
                    edit.putBoolean("break", true);
                    edit.commit();


                }
            }
        });

        bookbin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                    //academic
                }


                startActivity(new Intent(MainActivity.this, BookBinActivity.class));

                //uncomment
       /* File file = new File(
                Environment.getExternalStorageDirectory(),
                "pp.png");

        String senddata="Filesharing"+"@"+file.getName()+"@"+file.length();
        new clientThread(MultiThreadChatServerSync.thread,senddata).start();
        if (startshare == null) {
            startshare = new Filesharingtoclient(file);
            startshare.start();


        } else {
            startshare.stopsocket();
            startshare = null;
            startshare = new Filesharingtoclient(file);
            startshare.start();
        }*/


            }
        });


        opendraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDrawerLayout.openDrawer(Gravity.LEFT);

            }
        });

        birthday.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("break", false)) {
                    sessionBreak();
                    return;
                    //birthday

                }
                startActivity(new Intent(MainActivity.this, BirthdayActivity.class));
            }
        });

        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        DataModel[] drawerItem = new DataModel[5];
        //drawerItem[0] = new DataModel(R.drawable.wifi, "Wifi");
        drawerItem[0] = new DataModel(R.drawable.pulse, "Pulse");
        // drawerItem[2] = new DataModel(R.drawable.sync, "Connect");
        drawerItem[1] = new DataModel(R.drawable.quiz, "Quiz");

        // drawerItem[2] = new DataModel(R.drawable.rooms, "Room");
        drawerItem[2] = new DataModel(R.drawable.wifi, "Wifi");
        drawerItem[3] = new DataModel(R.drawable.brightness, "Brightness");
        drawerItem[4] = new DataModel(R.drawable.download, "Get Update");
        //drawerItem[4] = new DataModel(R.drawable.cancel, "Close");

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setupDrawerToggle();

    }

    private void createLocationRequest() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setSmallestDisplacement(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new
                LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        Task<LocationSettingsResponse> task= LocationServices.getSettingsClient(this)
                .checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                Listpopup();
            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                try {
                    // Cast to a resolvable exception.
                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    resolvable.startResolutionForResult(
                            MainActivity.this,
                            101);
                } catch (IntentSender.SendIntentException e3) {
                    // Ignore the error.
                } catch (ClassCastException e2) {
                    // Ignore, should be an impossible error.
                }
                // Toast.makeText(MainActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });

    }

    void changemethod() {
        // gettable=dp.getclassdetails();
        details = dp.getschooldetails();

        logindetails = dp.staffdetailsuserlogin(pref.getString("portalstaffid", ""));

        studentname.setText(logindetails.getFirstName().toUpperCase());
        username.setText(logindetails.getFirstName().toUpperCase());

        Log.d("uname", "changemethod: "+logindetails.getFirstName().toUpperCase());

        setbackground(backgroundlogo, details.getBackground());
        setbackground(schoollogo, details.getSchoolLogo());
        edit.putString("image", logindetails.getPhotoFilename());
        edit.putString("stafftimetable", logindetails.getTimetable());
        edit.commit();
        Log.e("photofilename", logindetails.getPhotoFilename());
        setbackground1(profileimage, logindetails.getPhotoFilename());
        edit.putString("staffid", "" + logindetails.getStaffID());
        edit.putString("portalstaffid", "" + logindetails.getPortalLoginID());
        edit.commit();
        edit.putString("uname",logindetails.getFirstName().toUpperCase());
        gettable = dp.getclassdetails(pref.getString("staffid", "0"));


        String roomdetails[] = logindetails.getRoominfo().split(",");
        for (int r = 0; r < roomdetails.length; r++)

        {
            if (!roomdetails[r].equalsIgnoreCase("0")) {
                try {
                    Rooms room = db.getroomdata(Integer.parseInt(roomdetails[r]));
                    tblclasses roomclass = new tblclasses();
                    roomclass.setRommornot(true);
                    roomclass.setClassID(room.getId());
                    roomclass.setInternetPassword(room.getPassword());
                    roomclass.setClassName(room.getRoomsname());
                    roomclass.setInternetSSID(room.getSsid());
                    gettable.add(roomclass);
                } catch (Exception e) {

                }
            }


        }
        tblclasses roomclass = new tblclasses();
        roomclass.setRommornot(true);
        roomclass.setClassID(-1);
        roomclass.setInternetPassword("");
        roomclass.setClassName("Session Break");
        roomclass.setInternetSSID("");
        gettable.add(roomclass);
        final Downspinner down = new Downspinner(MainActivity.this, R.layout.custom_spinner_item, gettable);
        Log.d("tag", "changemethod: " + gettable.size());

        // Customadapter adapter=new Customadapter(getApplicationContext(),gettable);
        // upper=new Upperspinner(this,android.R.layout.simple_spinner_item,gettable);
        upperspinner.setAdapter(down);
        if (gettable.size() > 0) {
            upperspinner.setSelection(0);
        }
       /* roomadapter=new Roomspinner(MainActivity.this,android.R.layout.simple_spinner_item,overallrooms);
        roomspinner.setAdapter(roomadapter);
        if(overallrooms.size()>0)
        {
            roomspinner.setSelection(0);
        }
*/
        deviceviolation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Set<String> set = pref.getStringSet("getviolation", null);


                if (set != null) {
                    //ArrayList<Violationpojo>violation=new ArrayList<>();
                    if (set.size() > 0) {
                        startActivity(new Intent(MainActivity.this, ViolationActivity.class));
                        // Utils.Listpopupforvioloation(MainActivity.this, pref);
                    } else {
                        Toast.makeText(getApplicationContext(), "No violation", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "No violation", Toast.LENGTH_SHORT).show();
                }

                //startActivity(new Intent(MainActivity.this,Quizzactivity.class));
                //Utils.buzzer(MainActivity.this);
            }
        });
        handraise.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(MainActivity.this);
                    handraise.setImageResource(R.drawable.raise_sel);
                } else {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });

        dnd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!pref.getBoolean("break", false))) {


                    if (pref.getBoolean("dnd", false)) {
                        dnd.setImageResource(R.drawable.dci);
                        edit.putBoolean("dnd", false);
                        edit.commit();

                        String senddata = "DND";

                        senddata = senddata + "@false";
                        dnd.setImageResource(R.drawable.emp_sel);


                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                    } else {
                        String senddata = "DND";

                        senddata = senddata + "@true";
                        edit.putBoolean("dnd", true);
                        edit.commit();
                        dnd.setImageResource(R.drawable.emp_unsel);

                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "You can access this feature once you come out of the session break.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        roomspinner.setVisibility(View.GONE);

      /*  roomspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                edit.putString("roomid",""+overallrooms.get(position).getId());
                edit.putString("roompassword",overallrooms.get(position).getPassword());
                edit.putString("roomssid",overallrooms.get(position).getSsid());
                edit.putString("roomname", overallrooms.get(position).getRoomsname());
                edit.commit();
              //  mWifiBase = new WifiBase(MainActivity.this);
                breakstate=overallrooms.get(position).getRoomsname();
                if(breakstate.equalsIgnoreCase("Session Break"))
                {
                    online.setImageResource(R.drawable.newoffline);
                }
                else
                {
                    mWifiBase = new WifiBase(MainActivity.this);
                    online.setImageResource(R.drawable.online);

                }
                MultiThreadChatServerSync.thread.clear();

                if(!pref.getString("roomname","").equalsIgnoreCase("Session Break")) {

                   // connect();
                    downspinner.setVisibility(View.VISIBLE);
                }
                else {
                    connect();
                    downspinner.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelectswed(AdapterView<?> parent) {

            }
        });*/

        upperspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (!gettable.get(position).isRommornot()) {
                    classname.setText((gettable.get(position).getClassCode().toString()));

                    edit.putString("classname", classname.getText().toString());
                    edit.putString("schoolid", "" + details.getSchoolID());
                    edit.putBoolean("roomornot", false);
                    edit.putBoolean("break", false);
                    edit.putString("roomname", gettable.get(position).getClassName());


                    edit.putString("roompassword", gettable.get(position).getInternetPassword());
                    edit.putString("roomssid", gettable.get(position).getInternetSSID());
                    edit.putString("bookbin", "" + gettable.get(position).getClassID());
                    edit.putString("batchid", "" + gettable.get(position).getBatchid());
                    edit.commit();
                    //  Toast.makeText(MainActivity.this, pref.getString("schoolid", "0"), Toast.LENGTH_SHORT).show();
                    edit.commit();
                    subjectname = dp.getbatchdetails(gettable.get(position).getClassID(), Integer.parseInt(gettable.get(position).getBatchid())
                            , Integer.parseInt(pref.getString("staffid", "0")));
                    edit.putString("bookbin", "" + gettable.get(position).getClassID());
                    edit.commit();
                } else if (!gettable.get(position).getClassName().equalsIgnoreCase("Session Break")) {
                    edit.putBoolean("roomornot", true);
                    edit.putBoolean("break", false);
                    edit.putString("roomid", "" + gettable.get(position).getClassID());
                    edit.putString("roompassword", gettable.get(position).getInternetPassword());
                    edit.putString("roomssid", gettable.get(position).getInternetSSID());
                    edit.putString("roomname", gettable.get(position).getClassName());
                    edit.commit();
                    //  mWifiBase = new WifiBase(MainActivity.this);

                    subjectname = dp.getbatchdetailsfrommasterinfo();


                } else {
                    edit.putString("roomname", gettable.get(position).getClassName());
                    edit.putBoolean("break", true);
                    edit.commit();


                }
                breakstate = gettable.get(position).getClassName();

                MultiThreadChatServerSync.thread.clear();

                upper = new Upperspinner(MainActivity.this, R.layout.custom_spinner_item, subjectname);
                downspinner.setAdapter(upper);
                if (subjectname.size() > 0) {
                    firstime = true;
                    downspinner.setSelection(0);
                } else {
                    downspinner.setVisibility(View.GONE);

                }
                if (breakstate.equalsIgnoreCase("Session Break")) {
                    online.setImageResource(R.drawable.newoffline);
                    connect();

                } else {
                    mainWifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                    if (ssid != null) {
                        ssid = ssid.replace("\"", "");
                    }
                    if (ssid != null) {

                        if (pref.getString("roomssid", "").equals(ssid)) {
                           /* new connectTask().execute();
                            coonnectedtowifi=false;*/
                            connect();
                        } else {
                            mWifiBase = new WifiBase(MainActivity.this);

                        }

                    } else {
                        mWifiBase = new WifiBase(MainActivity.this);
                    }


                    online.setImageResource(R.drawable.online);

                }

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    // connect();
                    tv_subject.setVisibility(View.VISIBLE);
                    downspinner.setVisibility(View.VISIBLE);
                } else {
                    // connect();
                    tv_subject.setVisibility(View.GONE);
                    downspinner.setVisibility(View.GONE);

                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        downspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                edit.putString("batchid", subjectname.get(position).getBatchid());
                edit.putString("subjectid", subjectname.get(position).getId());
                edit.putString("subjectname", subjectname.get(position).getName());
                edit.commit();
                edit.putString("timetable", db.getbatchtimetable(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("batchid", "0"))).getTimeTable());
                edit.commit();
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {
                    if (!firstime) {
                        MultiThreadChatServerSync.thread.clear();

                        connect();
                    } else {
                        MultiThreadChatServerSync.thread.clear();

                        connect();
                      /*  MultiThreadChatServerSync.thread.clear();

                        connect();*/
                        firstime = false;
                    }
                   downspinner.setVisibility(View.VISIBLE);
                } else {

                    downspinner.setVisibility(View.GONE);

                }
                upperspinner.setVisibility(View.VISIBLE);
                // Toast.makeText(MainActivity.this,db.getbatchtimetable(Integer.parseInt(pref.getString("bookbin","0")),Integer.parseInt(pref.getString("batchid","0"))).getTimeTable(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //mWifiBase = new WifiBase(this);


       /* imageLoader.get(pref.getString("profileimage",""), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    profileimage.setImageBitmap(response.getBitmap());
                    // load image into imageview
                   // imageView.setImageBitmap(response.getBitmap());
                }
            }
        });

        imageLoader.get(pref.getString("profileimage",""), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    profileimage.setImageBitmap(response.getBitmap());
                    // load image into imageview
                    // imageView.setImageBitmap(response.getBitmap());
                }
            }
        });*/
     /*   imageLoader.get(pref.getString("backgroundlogo",""), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    backgroundlogo.setImageBitmap(response.getBitmap());
                    // load image into imageview
                    // imageView.setImageBitmap(response.getBitmap());
                }
            }
        });
        imageLoader.get(pref.getString("schoologo",""), new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    schoollogo.setImageBitmap(response.getBitmap());
                    // load image into imageview
                    // imageView.setImageBitmap(response.getBitmap());
                }
            }
        });*/

       /* if(pref.getString("name","iyyappa").equalsIgnoreCase("iyyappa"))
        {
            profileimage.setImageResource(R.drawable.student3);
            studentname.setText("Iyyappa");
        }
        else
        {
            profileimage.setImageResource(R.drawable.student3);
            studentname.setText(pref.getString("name","Iyyapparaj"));


        }*/
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                //    fragment = new ConnectFragment();

                //   Toast.makeText(getApplicationContext(),"first",Toast.LENGTH_LONG).show();
           //     mDrawerLayout.closeDrawer(drawer);

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {
                    Intent intent = new Intent(MainActivity.this, PulseQuestionActivity.class);
                    startActivity(intent);
                } else {
                    Utilss.lockpoopup(MainActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }
                break;
            case 1:


               // mDrawerLayout.closeDrawer(drawer);

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {
                    startActivity(new Intent(MainActivity.this, Quizzactivity.class));
                  /*  Videoname video = videos.get(position);

                    File file = new File(video.getVideopath());



                    stratExamCompletedPopup(video);*/
                } else {
                    Utilss.lockpoopup(MainActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }

                // Toast.makeText(getApplicationContext(),"second",Toast.LENGTH_LONG).show();
                //     fragment = new FixturesFragment();
                //mDrawerLayout.closeDrawer(mDrawerList);

                break;
           /* case 2:
              //  Toast.makeText(getApplicationContext(),"third",Toast.LENGTH_LONG).show();
                //    fragment = new TableFragment();
               // mDrawerLayout.closeDrawer(mDrawerList);
                Utils.listofrooms(MainActivity.this,overallrooms);

                mDrawerLayout.closeDrawer(drawer);
                break;*/
            case 2:
               // mDrawerLayout.closeDrawer(drawer);
                Listpopup();
                break;
            case 3:
               // mDrawerLayout.closeDrawer(drawer);
                Intent intent1 = new Intent(MainActivity.this, Brightness.class);
                startActivity(intent1);
                //  Toast.makeText(getApplicationContext(),"third",Toast.LENGTH_LONG).show();
                //    fragment = new TableFragment();
                // mDrawerLayout.closeDrawer(mDrawerList);
              //  mDrawerLayout.closeDrawer(drawer);
                break;
            case 4:


              /*  String osversion = Build.VERSION.RELEASE;
                String devname = android.os.Build.MODEL;
                String ostype = "Android";
                int versionCode = BuildConfig.VERSION_CODE;
                String versionName = BuildConfig.VERSION_NAME;

                String additionalparams = "|Device Type:" + ostype + "|GCMKey:" + "" +
                        "|DeviceID:" + "" +
                        "|AppID:" + versionCode +
                        "|IMEINumber:" + "" +
                        "|AppVersion:" + versionName +
                        "|MACAddress:" + "" +
                        "|OSVersion:" + osversion;
                String login_str = "UserName:" + pref.getString("teachername", "") + "|Password:" + pref.getString("teacherpass", "") + "|Function:StaffLogin|Update:Yes" + additionalparams;
                login.clear();
                byte[] data;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        login.clear();
                        login.add(new BasicNameValuePair("WS", base64_register));

                        validateUserTask load_plan_list = new validateUserTask(MainActivity.this, login);
                        load_plan_list.execute();
                    } else {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }*/


            default:
                break;
        }

      /*  if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }*/
    }

    void setupDrawerToggle() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, null, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    private void Listpopup() {
        LayoutInflater inflater = (LayoutInflater) MainActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_popup,
                (ViewGroup) findViewById(R.id.listparent));
        listwindo = new PopupWindow(layout, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT, true);
       // listwindo = new PopupWindow(layout, 600, 600, true);
        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        listView = (ListView) layout.findViewById(R.id.list);
        final Button close = (Button) layout.findViewById(R.id.close);

        arrayOfUsers = new ArrayList<User>();
        //   for(int i=0;i<6;i++)
        arrayOfUsers.add(new User("", ""));

        adapter = new UsersAdapter(this, arrayOfUsers);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);

        doInback();
        ImageView imgclose = (ImageView) layout.findViewById(R.id.closeimg);
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();

            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user = adapter.getItem(position);
                initiatePopupWindow(user.name);
                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();
            }
        });

    }

    private void initiatePopupWindow(final String ssid) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) MainActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View layout = inflater.inflate(R.layout.screen_popup2,
//                    (ViewGroup) findViewById(R.id.popup_element));
//            pwindo = new PopupWindow(layout, 450, 130, true);
//            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            View layout = inflater.inflate(R.layout.screen_popup2,
                    (ViewGroup) findViewById(R.id.popup_element));
            final Dialog dialog = new Dialog(this);
            dialog.setCancelable(true);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(layout);
            dialog.show();

            final EditText password = (EditText) layout.findViewById(R.id.edit);

            Button btnconnect = (Button) layout.findViewById(R.id.button1);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    wifipass = password.getText().toString();
                    if (!wifipass.equalsIgnoreCase("")) {
                        WifiConfiguration wc = new WifiConfiguration();
                        wc.SSID = String.format("\"%s\"", ssid);
                        wc.preSharedKey = String.format("\"%s\"", wifipass);
                        wc.status = WifiConfiguration.Status.ENABLED;
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                        int netId = mainWifi.addNetwork(wc);
                        mainWifi.disconnect();
                        mainWifi.enableNetwork(netId, true);
                        mainWifi.reconnect();
                        doInback();
                    }

                    hideKeyboard();
                    dialog.dismiss();
                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.button2);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doInback() {

        handler.postDelayed(new Runnable() {

            @SuppressLint("WifiManagerLeak")
            @Override
            public void run() {
                // TODO Auto-generated method stub

                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new WifiReceiver();
                IntentFilter intentFilter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
                intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
                registerReceiver(receiverWifi, intentFilter);
                if (mainWifi.isWifiEnabled() == false) {
                    mainWifi.setWifiEnabled(true);
                }

                mainWifi.startScan();
                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);

                // doInback();
            }
        }, 1000);

    }

   /* @Override
    public void onAttachedToWindow(){
       // Log.i("TESTE", "onAttachedToWindow");
        this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);

        KeyguardManager   keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();
    }*/
    /*public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_HOME) {
            Log.i("TESTE", "BOTAO HOME");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
*/

    void toast() {

        Toast.makeText(getApplicationContext(), "Under Developement", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (pref.getBoolean("dnd", false)) {
            dnd.setImageResource(R.drawable.emp_sel);

        } else {
            dnd.setImageResource(R.drawable.emp_unsel);

        }
        wifilisetner = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!intent.getBooleanExtra("wifi", false)) {
                    MultiThreadChatServerSync.thread.clear();

                    connect();
                    // Toast.makeText(getApplicationContext(), "wifi", Toast.LENGTH_LONG).show();
                } else {
                    online.setImageResource(R.drawable.newoffline);

                    // Toast.makeText(getApplicationContext(), "Room not in range", Toast.LENGTH_LONG).show();

                }
            }
        };
        IntentFilter wififilter = new IntentFilter("wififilter");
        registerReceiver(wifilisetner, wififilter);


        lockreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


               /* if(intent.getBooleanExtra("unlockorlock",false))
                {
                    String senddata = "lock";
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    edit.putBoolean("lockdevice",true);
                    edit.commit();
                }
                else {
                    String senddata = "unlock";
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    edit.putBoolean("lockdevice",false);
                    edit.commit();
                }*/

            }
        };
        IntentFilter lockdevice = new IntentFilter("lock");
        registerReceiver(lockreceiver, lockdevice);
        if (LoginActivity.handraise!=null){
            if (LoginActivity.handraise.size() > 0) {
                handraise.setImageResource(R.drawable.raise_sel);
            } else {
                handraise.setImageResource(R.drawable.raise_unsel);

            }
        }




        changeroom = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

            }
        };

        IntentFilter filter = new IntentFilter("changeroom");
        registerReceiver(changeroom, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(wifilisetner);
        unregisterReceiver(changeroom);
        unregisterReceiver(lockreceiver);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(changeroom);
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private String getIpAddress() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
                    .getNetworkInterfaces();
            while (enumNetworkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = enumNetworkInterfaces
                        .nextElement();
                Enumeration<InetAddress> enumInetAddress = networkInterface
                        .getInetAddresses();
                while (enumInetAddress.hasMoreElements()) {
                    InetAddress inetAddress = enumInetAddress.nextElement();

                    if (inetAddress.isSiteLocalAddress()) {
                        ip = inetAddress.getHostAddress();
                    }

                }

            }

        } catch (SocketException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            ip += "Something Wrong! " + e.toString() + "\n";
        }

        return ip;
    }

    @Override
    public String getWifiSSID() {
        return /*"Edimax_DCI3"*/pref.getString("roomssid", "");
    }

    @Override
    public String getWifiPass() {
        return /*"Dci@SecureWeb#"*/pref.getString("roompassword", "");
    }

    @Override
    public int getSecondsTimeout() {
        return 50;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    void initialize() {

        this.broadcastUtils = new Broadcastlistener();
        this.broadcastUtils.addBroadcastMessageListener(this.broadcast);
        this.broadcastUtils.initialize();

        this.broadcastUtils.receive();
        //   connect();
        updateConnectionOnInterval();
        if (reply == null) {
            reply = new Communicationsend();
            reply.addBroadcastMessageListener(msg);


        }
        if (violationreceiver == null) {
            violationreceiver = new Violationreceive();
            violationreceiver.addBroadcastMessageListener(vmsg);
        }
        if (multi == null) {


            multi = new MultiThreadChatServerSync();
            multi.addBroadcastMessageListener(msg, pref, db);
       /*  try {
             server=new Serversocket();
             server.start();
         } catch (Exception e) {
             e.printStackTrace();
         }*/
        }


    }

    public void discoonnect() {
        if (multi != null) {
            try {
                multi.socketclose();
            } catch (IOException e) {
                e.printStackTrace();
            }
            multi = null;
        }

    }


    public void connect() {
        if (haveNetworkConnection(this) /*&& sessionId != 0*/) {
            Log.d("broad", "broadcasting teacher details");
            if (this.broadcastUtils != null) {

                final JSONObject jsonObj = new JSONObject();
                try {

                    jsonObj.put("Username", "iyyappa");
                    jsonObj.put("sessionid", "12345");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // ip=getIpAddress();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);

                        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

                        // Convert little-endian to big-endianif needed
                        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
                            ipAddress = Integer.reverseBytes(ipAddress);
                        }

                        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

                        String ipAddressString;
                        try {
                            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
                        } catch (UnknownHostException ex) {
                            Log.e("WIFIIP", "Unable to get host address.");
                            ipAddressString = null;
                        }


                        ip = ipAddressString;


                        broadcastUtils.broadcastMessage("invT" + "@" + ip + "@" + pref.getString("staffid", "0") + "@" + pref.getString("roomid", "0") + "@" + pref.getString("subjectid", "0") + "@" + pref.getBoolean("roomornot", false) + "@" + pref.getString("bookbin", "0") + "@" + pref.getBoolean("break", false) + "@" + pref.getString("batchid", "0"), getBroadcastIp());

                    }
                }.execute();


            }
        } else if (!haveNetworkConnection(this)) {

        }
    }

    void disconnect() {
        if (haveNetworkConnection(this) /*&& sessionId != 0*/) {
            Log.d("broad", "broadcasting teacher details");
            if (this.broadcastUtils != null) {

                this.broadcastUtils.broadcastMessage("invs@Disconnect", getBroadcastIp());

            }
        } else if (!haveNetworkConnection(this)) {

        }

    }

    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {

        }
    }

    void setbackground1(RoundedImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);
               // view.setImageDrawable(getResources().getDrawable(R.drawable.userimage));

            }
            else {
                view.setImageDrawable(getResources().getDrawable(R.drawable.userimage));
            }
        } catch (Exception e) {
            view.setImageDrawable(getResources().getDrawable(R.drawable.userimage));
        }
    }

    public void updateConnectionOnInterval() {

        /*if(reconnect<=2)
        {*/
        this.scheduleTaskExecutor = Executors.newScheduledThreadPool(1);
        this.scheduleTaskExecutor.scheduleAtFixedRate(new Runnable() {
            public void run() {
                if (reconnect <= 2) {

                    Log.e("caliingtimer", "" + reconnect);
                    //  connect();
                    //MainActivity.this.connect();
                    reconnect = reconnect + 1;
                }
/*
                Log.i("info about resending the connection ", "sending connection", null);
*/
            }
        }, 0, 5000, TimeUnit.SECONDS);

    }

    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        for (NetworkInfo ni : ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getAllNetworkInfo()) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI") && ni.isConnected()) {
                haveConnectedWifi = true;
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE") && ni.isConnected()) {
                haveConnectedMobile = true;
            }
        }
        if (haveConnectedWifi || haveConnectedMobile) {
            return true;
        }
        return false;
    }

    public String getBroadcastIp() {
        String broadcastAddr = "255.255.255.255";
        try {
            for (InterfaceAddress addr : NetworkInterface.getByInetAddress(InetAddress.getByName(ip)).getInterfaceAddresses()) {
                if (addr.getAddress().getHostAddress().equals(getIpAddress())) {
                    broadcastAddr = addr.getBroadcast().getHostAddress();
                    break;
                }
            }
        } catch (SocketException e) {
            IOException e2 = e;
            e2.printStackTrace();
        } catch (UnknownHostException e3) {

        } catch (Exception e4) {/* e2 = e3;
            e2.printStackTrace();*/
            e4.printStackTrace();
        }
        return broadcastAddr;
    }

    public void logoutfunction() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        TextView log = (TextView) dialog.findViewById(R.id.notsaved);
        revisit.setText("Yes");
        valuate.setText("No");
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        title.setText("LOGOUT");
        log.setText("Do you want to logout?");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear();
                edit.commit();


                finish();
                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            arrayOfUsers.clear();
            ArrayList<String> connections = new ArrayList<String>();
            ArrayList<Float> Signal_Strenth = new ArrayList<Float>();
            sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();
            for (int i = 0; i < wifiList.size(); i++) {
                connections.add(wifiList.get(i).SSID);
            }

            //  adapter.clear();
            Log.d("con-size", connections.size() + "");
            if (connections.size() == 1) {
                doInback();
            }
            for (int y = 0; y < connections.size(); y++) {
                String ssid = getCurrentSsid(getApplicationContext(), mainWifi);
                String mode = "Not connected";
                String compare = null;
                //   Log.d("connectedssid", ssid);
                //      Log.d("listssid",connections.get(y));
                //    Log.e("ssidiyyappa","iyyappa"+ssid);

                if (ssid != null) {
                    compare = ssid.replace("\"", "");
                }


                if (ssid != null) {
                    if (compare.equalsIgnoreCase(connections.get(y))) {
                        mode = "connected";
                    } else {
                        mode = "Not connected";
                    }
                }

                User newUser = new User("" + connections.get(y), mode);
                arrayOfUsers.add(newUser);
                //adapter.add(newUser);


            }
            for (int i = 0; i < arrayOfUsers.size(); i++) {


                //    Log.e("size",""+arrayOfUsers.size());
                if (arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected")) {
                    int j = i;
                    if (i == 0) {
                        break;
                    } else {
                        User use = arrayOfUsers.get(i);
                        arrayOfUsers.remove(i);
                        arrayOfUsers.add(0, use);
                        break;
                    }
                }

                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
                    {

                    }*/
            }
            Log.d("wifi_data", "onReceive: "+arrayOfUsers.size());

            adapter.notifyDataSetChanged();


        }
    }

    public class ServerSocketThread extends Thread {

        @Override
        public void run() {

            try {
                serverSocket = new ServerSocket(SocketServerPORT);

                while (true) {
                    socket = serverSocket.accept();


                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // Toast.makeText(MainActivity.this,socket.getInetAddress().toString(),Toast.LENGTH_LONG).show();
                            /*infoPort.setText("I'm waiting here: "
                                    + serverSocket.getLocalPort());*/
                        }
                    });

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            boolean alreadysocket = false;
                            for (int i = 0; i < soc.size(); i++) {
                                if (soc.get(i).getInetAddress().toString().equalsIgnoreCase(socket.getInetAddress().toString())) {

                                    soc.remove(i);
                                    soc.add(i, socket);

                                }
                            }


                        }
                    }).start();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public class FileTxThread extends Thread {
        Socket socket;

        FileTxThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            if (socket.isConnected()) {
                File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "signature.png");

                byte[] bytes = new byte[(int) file.length()];
                BufferedInputStream bis;
                try {
                    bis = new BufferedInputStream(new FileInputStream(file));
                    // bis.read(bytes, 0, bytes.length);

    /*ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
    oos.writeObject(bytes);
    oos.flush();*/
                    DataInputStream dis = new DataInputStream(bis);
                    dis.readFully(bytes, 0, bytes.length);
                    OutputStream os = socket.getOutputStream();

                    //Sending file name and file size to the server
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(file.getName()
                    );
                    dos.writeLong(bytes.length);

                    dos.write(bytes, 0, bytes.length);


                    dos.flush();

                    //Sending file data to the server
                    os.write(bytes, 0, bytes.length);
                    os.flush();

                    //Closing socket
                    os.close();
                    dos.close();

                    final String sentMsg = "File sent to: " + socket.getInetAddress();
                    socket.close();

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                           /* Toast.makeText(MainActivity.this,
                                    sentMsg,
                                    Toast.LENGTH_LONG).show();*/
                        }
                    });

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            } else {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    public class FileTxThreadread extends Thread {
        Socket socket;

        FileTxThreadread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            if (socket.isConnected()) {

                try {
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    Object o = ois.readObject();
                    //return o;
                } catch (Exception ex) {
                    //Log.v("Serialization Read Error : ", ex.getMessage());
                    ex.printStackTrace();
                }
            } else {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
    }

    ///wifiactivity
    public class validateUserTask extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;

        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public validateUserTask(Context context_ws,
                                ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(MainActivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                Service sr = new Service(MainActivity.this);
                jsonResponseString = sr.getLogin(login, Url.baseurl
                        /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;


        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);

            try {


                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success"))

                {
                    calendarpath = pref.getString("calender", "");
                    Log.e("calendar", calendarpath);


                    new AsyncTask<Void, Void, Void>() {

                        @Override
                        protected Void doInBackground(Void... params) {

                            try {


                                JSONObject schooinfo = jObj.getJSONObject("schoolInfo");//school


                                sch_details = new Staffdetails();

                                sch_details.setSchoolID(schooinfo.has("ID") ? Integer.parseInt(schooinfo.getString("ID")) : 0);
                                sch_details.setAcronym(schooinfo.has("SchoolAcronym") ? (schooinfo.getString("SchoolAcronym")) : "Dci");
                                // sch_details.setCityID();
                                //sch_details.setCountryID();
                                sch_details.setSchoolLogo(schooinfo.has("Logo") ? (schooinfo.getString("Logo")) : "Dci");
                                sch_details.setSchoolName(schooinfo.has("Name") ? (schooinfo.getString("Name")) : "Dci");
                                sch_details.setBackground(schooinfo.has("BackgroundImage") ? schooinfo.getString("BackgroundImage") : "");

//File creation


    /*if(!schoolname.exists())
    {*/

                                File schoolname = new File(Environment.getExternalStorageDirectory(), sch_details.getAcronym() + "_" + sch_details.getSchoolID());

                                schoolname.mkdirs();

                                File users = new File(schoolname.getAbsolutePath() + "/" + "Users");
                                users.mkdir();
                                File assessts = new File(schoolname.getAbsolutePath() + "/" + "Assets");
                                assessts.mkdir();


                                //}


                                String filename = sch_details.getSchoolLogo().substring(sch_details.getSchoolLogo().lastIndexOf("/") + 1, sch_details.getSchoolLogo().length());

                                String backgroundimage = sch_details.getBackground().substring(sch_details.getBackground().lastIndexOf("/") + 1, sch_details.getBackground().length());

                                try {
                                    downloadfile(sch_details.getSchoolLogo(), assessts.getAbsolutePath(), "logo_" + filename);
                                    downloadfile(sch_details.getBackground(), assessts.getAbsolutePath(), "back_" + backgroundimage);
                                } catch (Exception e) {
                                    Log.e("first", "first" + e.getMessage());
                                }


                                sch_details.setSchoolLogo(assessts.getAbsolutePath() + "/" + "logo_" + filename);
                                sch_details.setBackground(assessts.getAbsolutePath() + "/" + "back_" + backgroundimage);


                                JSONObject staffinfo = jObj.getJSONObject("staffInfo");

                                staffdetails = new Staffloingdetails();
                                staffdetails.setStaffID(staffinfo.has("ID") ? Integer.parseInt(staffinfo.getString("ID")) : 0);
                                staffdetails.setFirstName(staffinfo.has("FirstName") ? staffinfo.getString("FirstName") : "");
                                staffdetails.setDOJ(staffinfo.has("DateOfJoining") ? staffinfo.getString("DateOfJoining") : "");
                                staffdetails.setPortalLoginID(staffinfo.has("PortalLoginID") ? staffinfo.getString("PortalLoginID") : "");
                                //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
                                staffdetails.setLastName(staffinfo.has("LastName") ? staffinfo.getString("LastName") : "");
                                staffdetails.setGender(staffinfo.has("Gender") ? staffinfo.getString("Gender") : "");
                                staffdetails.setDOB(staffinfo.has("DateOfBirth") ? staffinfo.getString("DateOfBirth") : "");
                                staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus") ? staffinfo.getString("MaritalStatus") : "");
                                ;
                                staffdetails.setSpouseName(staffinfo.has("SpuseName") ? staffinfo.getString("SpuseName") : "");
                                staffdetails.setFatherName(staffinfo.has("FatherName") ? staffinfo.getString("FatherName") : "");
                                staffdetails.setMotherName(staffinfo.has("MotherName") ? staffinfo.getString("MotherName") : "");
                                staffdetails.setPhone((staffinfo.has("PhoneNumber") ? staffinfo.getString("PhoneNumber") : ""));
                                staffdetails.setPhotoFilename(staffinfo.has("ProfileImage") ? staffinfo.getString("ProfileImage") : "");
                                staffdetails.setEmail(staffinfo.has("Email") ? staffinfo.getString("Email") : "");
                                staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade") ? staffinfo.getString("EmploymentGrade") : "");
                                staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory") ? Integer.parseInt(staffinfo.getString("StaffCategory")) : 0);
                                staffdetails.setTimetable(staffinfo.has("TimeTable") ? staffinfo.getString("TimeTable") : "");

                                String staffroom = staffinfo.has("Room") ? staffinfo.getString("Room") : "";
                                staffroom = staffroom.replaceAll("\\[", "").replaceAll("\\]", "");
                                staffroom = staffroom.replace("\"", "");
                                //details.setRoominfo(subarray);

                                if (staffroom.isEmpty()) {
                                    staffdetails.setRoominfo("0");

                                } else {
                                    staffdetails.setRoominfo(staffroom);

                                }

                                File username = new File(users.getAbsolutePath() + "/" + "T_" + staffdetails.getStaffID());
                                username.mkdir();
                                File materials = new File(username.getAbsolutePath() + "/" + "Materials");
                                materials.mkdir();

                                File Academic = new File(materials.getAbsolutePath() + "/" + "Academic");
                                Academic.mkdir();

                                File Reference = new File(materials.getAbsolutePath() + "/" + "Reference");
                                Reference.mkdir();
                                File evaluation = new File(username.getAbsolutePath() + "/" + "Evaluation");
                                evaluation.mkdir();
                                File notes = new File(username.getAbsolutePath() + "/" + "Notes");
                                notes.mkdir();
                                File calender = new File(username.getAbsolutePath() + "/" + "Calendar");
                                calender.mkdir();

                                edit.putString("calender", calender.getAbsolutePath());
                                edit.putString("academic", Academic.getAbsolutePath());
                                edit.putString("notes", notes.getAbsolutePath());
                                edit.putString("reference", Reference.getAbsolutePath());
                                edit.putString("Evaluation", evaluation.getAbsolutePath());
                                edit.commit();
                                calendarpath = pref.getString("calender", "");
                                Log.e("calendar", calendarpath);

                                String filename1 = staffdetails.getPhotoFilename().substring(staffdetails.getPhotoFilename().lastIndexOf("/") + 1, staffdetails.getPhotoFilename().length());

                                String filename2 = staffdetails.getTimetable().substring(staffdetails.getTimetable().lastIndexOf("/") + 1, staffdetails.getTimetable().length());

                                try {
                                    downloadfile(staffdetails.getPhotoFilename(), username.getAbsolutePath(), filename1);
                                    downloadfile(staffdetails.getTimetable(), username.getAbsolutePath(), filename2);
                                } catch (Exception e) {
                                    Log.e("second", "second" + e.getMessage());
                                }

                                staffdetails.setTimetable(username.getAbsolutePath() + "/" + filename2);


                                staffdetails.setPhotoFilename(username.getAbsolutePath() + "/" + filename1);

                                if (db.checkschooldetails(sch_details.getSchoolID()).getCount() > 0) {
                                    Log.e("update", "school");
                                    db.updateschooldetails(sch_details);

                                } else {
                                    db.schooldetails(sch_details);
                                }
                                /// db.getAllContacts();

                                if (db.checkstaffdetails(staffdetails.getStaffID()).getCount() > 0) {
                                    Log.e("update", "staff");

                                    db.updatestafflogindetails(staffdetails);
                                } else {
                                    db.stafflogindetails(staffdetails);
                                }
                                // db.getallstafflogindetails();


                                JSONObject getmaster = jObj.getJSONObject("masterInfo");
                                JSONArray getmasterarray = getmaster.getJSONArray("Subjects");
                                JSONArray getcalendararray = getmaster.getJSONArray("CalendarType");
                                JSONArray roomsarray = getmaster.getJSONArray("Rooms");
                                // ArrayList<Rooms> roomsarraylist=new ArrayList<Rooms>();
                                for (int r = 0; r < roomsarray.length(); r++) {
                                    Rooms room = new Rooms();
                                    JSONObject roomobject = roomsarray.getJSONObject(r);
                                    try {
                                        room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);

                                    } catch (Exception e) {
                                        room.setId(0);
                                    }
                                    room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                                    room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                                    room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");

                                    if (db.checkroomid(room.getId()).getCount() > 0) {
                                        Log.e("room", "update");
                                        db.updateaddmasterroom(room);

                                    } else {
                                        db.addmasterroom(room);
                                    }
                                }

                                //calendararrayl


                                for (int cal = 0; cal < getcalendararray.length(); cal++) {
                                    JSONObject getcal = getcalendararray.getJSONObject(cal);
                                    helper.Calendarpojo pojo = new helper.Calendarpojo();
                                    pojo.setCalendarname(getcal.has("Name") ? getcal.getString("Name") : "0");
                                    pojo.setCalendarid(getcal.has("ID") ? Integer.parseInt(getcal.getString("ID")) : 0);
                                    pojo.setCalendardescription(getcal.has("Description") ? getcal.getString("Description") : "0");

                                    String imagename = getcal.has("Image") ? getcal.getString("Image") : "0";

                                    String batchfile = imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());

                                    try {
                                        downloadfile(imagename, calendarpath, batchfile);

                                    } catch (Exception e) {
                                        Log.e("third", "third" + e.getMessage());
                                    }


                                    pojo.setCalendarimage(calendarpath + "/" + batchfile);


                                    if (db.checkcalendar(pojo.getCalendarid()).getCount() > 0) {
                                        Log.e("calendar", "update");

                                        db.updateaddcalendaremaster(pojo);
                                    } else {
                                        db.addcalendaremaster(pojo);

                                    }

                                }


                                Log.e("loading", "masterinfo");
                                for (int m = 0; m < getmasterarray.length(); m++) {
                                    JSONObject getm = getmasterarray.getJSONObject(m);
                                    Masterinfo minfo = new Masterinfo();
                                    minfo.setID(getm.has("ID") ? Integer.parseInt(getm.getString("ID")) : 0);
                                    minfo.setSubjectName(getm.has("SubjectName") ? getm.getString("SubjectName") : "");

                                    minfo.setSchoolID(getm.has("SchoolID") ? Integer.parseInt(getm.getString("SchoolID")) : 0);
                                    minfo.setStatus(getm.has("Status") ? getm.getString("Status") : "");
                                    minfo.setSubjectDescription(getm.has("SubjectDescription") ? getm.getString("SubjectDescription") : "");

                                    if (db.checksubjectid(minfo.getID()).getCount() > 0) {
                                        Log.e("masterinfo", "update");

                                        db.updatemasterinfo(minfo);
                                    } else {
                                        db.masterinfo(minfo);

                                    }
                      /*  "ID":"1",
                            "SubjectName":"English",
                            "SubjectDescription":"English - First Language",
                            "CreatedBy":"1",
                            "CreatedDate":"2017-04-18 00:00:00",
                            "ModifiedBy":"1",
                            "ModifiedDate":"2017-04-18 00:00:00",
                            "Status":"1",
                            "SchoolID":"1"*/

                                }

                                final JSONArray studentinfo = jObj.getJSONArray("studentDetails");

                                for (int s = 0; s < studentinfo.length(); s++) {
                                    JSONObject getstud = studentinfo.getJSONObject(s);

                                    helper.Studentdetails details = new Studentdetails();


                                    details.setStudentLoginID(getstud.getString("StudentLoginID"));
                                    details.setStudentID(Integer.parseInt(getstud.getString("ID")));
                                    details.setAdmissionNumber(getstud.getString("AdmissionNumber"));

                                    details.setRollNo(getstud.getString("RollNumber"));

                                    details.setFirstName(getstud.getString("FirstName"));
                                    details.setLastName(getstud.getString("LastName"));
                                    details.setDOB(getstud.getString("DateOfBirth"));
                                    details.setDOA(getstud.getString("AdmissionDate"));
                                    details.setGender(getstud.getString("DateOfBirth"));
                                    details.setPhone_1(getstud.getString("Phone"));
                                    details.setFatherName(getstud.getString("FatherName"));
                                    details.setGuardianMobileNumber(getstud.getString("GuardianPhone"));
                                    details.setMotherName(getstud.getString("GuardianPhone"));
                                    details.setGuardianMobileNumber(getstud.getString("MotherName"));
                                    details.setEmail(getstud.getString("Email"));
                                    try {
                                        details.setBatchID(Integer.parseInt(getstud.getString("Batch")));
                                    } catch (Exception e) {
                                        details.setBatchID(0);
                                    }

                                    String subarray = getstud.getJSONArray("Room").toString();
                                    //  subarray=String str = "[Chrissman-@1]";
                                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                    subarray = subarray.replace("\"", "");
                                    if (subarray.isEmpty()) {
                                        details.setRoominfo("0");

                                    } else {
                                        details.setRoominfo(subarray);
                                    }

                                    details.setStaffID(staffdetails.getStaffID());
                                    //  details.setBatchID();
                                    details.setSchoolID(sch_details.getSchoolID());
                                    details.setPhotoFilename(getstud.getString("ProfileImage"));
                                    details.setClassID(Integer.parseInt(getstud.getString("ClassId")));
                                    String photofilename = "s_" + details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/") + 1, details.getPhotoFilename().length());


                                    try {
                                        Log.e("imagedownloading", photofilename);
                                        downloadfile(details.getPhotoFilename(), assessts.getAbsolutePath(), photofilename);
                                    } catch (Exception e) {
                                        Log.e("fourth", e.getMessage() + "fourth");
                                    }
                                    details.setPhotoFilename(assessts.getAbsolutePath() + "/" + photofilename/*details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/")+1,details.getPhotoFilename().length()*/);


                                    try {
                                        if (Integer.parseInt(details.getRollNo()) > 0) {

                                        }
                                    } catch (Exception e) {
                                        details.setRollNo("0");
                                    }
                                    if (db.checkstudentdetails(details.getStudentID()).getCount() > 0) {
                                        Log.e("update", "student");

                                        db.updateStudent(details);
                                    } else {
                                        db.Student(details);
                                    }

                     /*   "ID":"1",
                            "AdmissionNumber":"60",
                            "AdmissionDate":"2013-04-18",
                            "RollNumber":"1201",
                            "FirstName":"kumaran",
                            "LastName":"R",
                            "MiddleName":"",
                            "DateOfBirth":"2002-02-18",
                            "Gender":"Male",
                            "Phone":"9898989898",
                            "FatherName":"Kannan",
                            "AddressLine1":"1\/481, main road, somarasam pettai, neart axia ATM",
                            "GuardianPhone":"7402516995",
                            "MotherName":"Divya",
                            "Email":"test@gmail.com",
                            "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/students\/profile_image\/1.jpg",
                            "DeviceControl":"0",
                            "StudentLoginID":"EK-001-S0000000001",
                            "ClassId":"1"*/


                                }
                                // db.deletesubject(pref.getString("staffid","0"));
                                db.deletesubject();

                                JSONArray classess = jObj.getJSONArray("Classes");
                                for (int c = 0; c < classess.length(); c++) {
                                    JSONObject getclassobj = classess.getJSONObject(c);
                                    tblclasses getclass = new tblclasses();
                                    getclass.setClassID(getclassobj.has("ID") ? Integer.parseInt(getclassobj.getString("ID")) : 0);
                                    getclass.setClassName(getclassobj.has("ClassName") ? (getclassobj.getString("ClassName")) : "");
                                    getclass.setGrade(getclassobj.has("Grade") ? (getclassobj.getString("Grade")) : "");
                                    getclass.setStaffID(staffdetails.getStaffID());

                                    getclass.setClassCode(getclass.getClassName() + "-" + (getclassobj.has("Section") ? (getclassobj.getString("Section")) : ""));
                                    String getclassname = getclass.getClassCode();
                                    getclass.setDepartmentID((getclassobj.has("Department") ? Integer.parseInt(getclassobj.getString("Department")) : 0));
                                    getclass.setInternetSSID(getclassobj.has("InternetSSID") ? (getclassobj.getString("InternetSSID")) : "");
                                    ;
                                    getclass.setInternetPassword(getclassobj.has("InternetPassword") ? (getclassobj.getString("InternetPassword")) : "");
                                    ;
                                    getclass.setInternetType(getclassobj.has("InternetType") ? (getclassobj.getString("InternetType")) : "");
                                    ;


                                    JSONArray batch = getclassobj.getJSONArray("batch");

                                    for (int b = 0; b < batch.length(); b++) {
                                        Batchdetails bdetails = new Batchdetails();
                                        JSONObject bobject = batch.getJSONObject(b);
                           /* "ID":"1",
                                "Subjects":[  ],
                            "Coordinator":"3",
                                "StartDate":"2017-04-18",
                                "EndDate":"2017-04-18",
                                "TimeTable":"http:\/\/schoolproject.dci.in\/images\/staffs\/time_table\/testtimetable.xml",
                                "AcademicYear":"2016-2017",
                                "subject":[  ]*/
                                        bdetails.setSchoolID(sch_details.getSchoolID());
                                        bdetails.setTimeTable(bobject.has("TimeTable") ? bobject.getString("TimeTable") : "");
                                        bdetails.setAcademicYear(bobject.has("AcademicYear") ? bobject.getString("AcademicYear") : "");
                                        // getclass.setClassName(getclass.getClassName()+"-"+(getclassobj.has("Section")?(getclassobj.getString("Section")):"")+""+bdetails.getAcademicYear());
                                        bdetails.setBatchID(bobject.has("ID") ? Integer.parseInt(bobject.getString("ID")) : 0);
                                        bdetails.setClassID(getclass.getClassID());
                                        bdetails.setStaffID(staffdetails.getStaffID());

                                        // bdetails.setTimeTable();
                                        getclass.setClassCode(getclassname + " " + bdetails.getAcademicYear());
                                        getclass.setBatchid(bdetails.getBatchID() + "");

                                        if (db.checkclasses(getclass.getClassID(), getclass.getSchoolID(), staffdetails.getStaffID()).getCount() > 0) {
                                            Log.e("update", "class");

                                            db.updatetbclasses(getclass);

                                        } else {
                                            db.tbclasses(getclass);

                                        }

                                        bdetails.setBatchcode(getclass.getClassCode() + "" + bdetails.getAcademicYear());

                                        String batchfile = bdetails.getTimeTable().substring(bdetails.getTimeTable().lastIndexOf("/") + 1, bdetails.getTimeTable().length());

                                        try {
                                            downloadfile(bdetails.getTimeTable(), username.getAbsolutePath(), batchfile);

                                        } catch (Exception e) {
                                            Log.e("third", "third" + e.getMessage());
                                        }


                                        bdetails.setTimeTable(username.getAbsolutePath() + "/" + batchfile);


                                        //  bdetails.set
                                        if (db.checkbatch(bdetails.getClassID(), bdetails.getBatchID(), staffdetails.getStaffID()).getCount() > 0) {
                                            Log.e("update", "batch");

                                            db.updatebatch(bdetails);

                                        } else {
                                            db.batch(bdetails);

                                        }
                                        if (b == 0) {
                                            Log.e("delete", "subject");

                                        }
                                        tblSubject subject = new tblSubject();

                                        subject.setClassID(bdetails.getClassID());
                                        subject.setBatchID(bdetails.getBatchID());
                                        subject.setSchoolID(bdetails.getSchoolID());
                                        subject.setStaffID(staffdetails.getStaffID());

                                        //subject.setStaffID(bobject.has("Coordinator")?Integer.parseInt(bobject.getString("Coordinator")):0);


                                        if (bobject.has("subject")) {

                                            String subarray = bobject.getJSONArray("subject").toString();
                                            //  subarray=String str = "[Chrissman-@1]";
                                            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                            subarray = subarray.replace("\"", "");
                                            String sSplit[] = subarray.split(",");
                                            if (sSplit.length > 0) {
                                                for (int s = 0; s < sSplit.length; s++) {
                                                    subject.setSubjectID(Integer.parseInt(sSplit[s]));
                                                    db.subject(subject);

                                                }
                                                //   for(int s=0; s<subarray.length(); s++)

                                            } else {
                                                subject.setSubjectID(-1);
                                                db.subject(subject);

                                            }
                                        } else {
                                            subject.setSubjectID(-1);
                                            db.subject(subject);
                                        }


                                        // bdetails.sets

                                    }


                                }
                                try {


                                    JSONArray pulsequestion = jObj.getJSONArray("PulseQuestionDetails");
                                    for (int c1 = 0; c1 < pulsequestion.length(); c1++) {
                                        JSONObject pulse = pulsequestion.getJSONObject(c1);
                                        PulseQuestionID = Integer.parseInt(pulse.getString("PulseQuestionID"));
                                        PulseType = pulse.getString("PulseType");
                                        PulseQuestion = pulse.getString("PulseQuestion");
                                        CorrectAnswerpulse = pulse.getString("CorrectAnswer");
                                        IsActive = pulse.getString("IsActive");
                                        Calendar calendar = Calendar.getInstance();
                                        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                        String strDate = mdformat.format(calendar.getTime());


                                        JSONArray batch1 = pulse.getJSONArray("options");
                                        boolean isExamIdexist = db.CheckIsIDAlreadypukseDBorNot(PulseQuestionID);


                                        if (isExamIdexist) {

                                            if (batch1.length() > 3) {
                                                db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), batch1.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));
                                            } else if (batch1.length() > 2) {
                                                db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                            } else if (batch1.length() > 1) {
                                                db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                            }

                                        } else {


                                            if (batch1.length() > 3) {
                                                db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), batch1.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));
                                            } else if (batch1.length() > 2) {
                                                db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                            } else if (batch1.length() > 1) {
                                                db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                            }

                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                                //pulse questionupdate


                            } catch (Exception e) {

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);

                            Toast.makeText(MainActivity.this, "Updated Successfully", Toast.LENGTH_LONG).show();
                            changemethod();
                            if (dia.isShowing())
                                dia.dismiss();
                        }
                    }.execute();

                } else {
                    Toast.makeText(MainActivity.this, jsonResponse.toUpperCase(), Toast.LENGTH_LONG).show();
                    if (dia.isShowing())
                        dia.dismiss();
                }
            } catch (Exception e) {
                if (dia.isShowing())
                    dia.dismiss();
            }
        }

        void downloadfile(String urls, String filepath, String filename) {

            int count;
            try {
                //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
                URL url = new URL(urls);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream

                OutputStream output = new FileOutputStream(filepath
                        + "/" + filename);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }


        }

    }

}




   /* public class FileTxThread implements Runnable {
        private final Socket socket;

        FileTxThread(Socket socket){
            this.socket= socket;
        }

        @Override
        public void run() {
            if(socket.isConnected())
            {
                File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "my.mp4");

                byte[] bytes = new byte[(int) file.length()];
                BufferedInputStream bis;
                try {
                    bis = new BufferedInputStream(new FileInputStream(file));
                    bis.read(bytes, 0, bytes.length);

                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(bytes);
                    oos.flush();


                    final String sentMsg = "File sent to: " + socket.getInetAddress();
                    socket.close();

                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    sentMsg,
                                    Toast.LENGTH_LONG).show();
                        }});

                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
            else
            {
                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Device disconnected",
                                Toast.LENGTH_LONG).show();
                    }});
            }
        }
    }*/

