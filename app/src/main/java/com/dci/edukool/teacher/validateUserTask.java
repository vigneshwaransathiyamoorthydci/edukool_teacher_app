package com.dci.edukool.teacher;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.AsyncTask;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Utilities.DatabaseHandler;
import Utilities.Service;
import Utilities.Url;
import helper.Batchdetails;
import helper.Calendarpojo;
import helper.Masterinfo;
import helper.Rooms;
import helper.Staffdetails;
import helper.Staffloingdetails;
import helper.Studentdetails;
import helper.tblSubject;
import helper.tblclasses;
import pulseexam.PulseExamPojo;

import static android.content.Context.MODE_PRIVATE;

public class validateUserTask extends AsyncTask<String, String, String> {

    String jsonResponseString;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();

    InputStream inputstream = null;
    Context context;
    String calendarpath;
    SharedPreferences pref;
    int StaffIDPluse;
    SharedPreferences.Editor edit;
    DatabaseHandler dp;
    DatabaseHandler db;
    Staffdetails sch_details;
    Staffloingdetails staffdetails;
    int SchoolIDPulse;
    Dialog loginDialog;
    //	String str = "WS";

    ProgressDialog dia;

    public validateUserTask(Context context_ws,
                            ArrayList<NameValuePair> loginws) {
        dia = new ProgressDialog(context_ws);
        this.context=context_ws;
        pref = context.getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();
        db = new DatabaseHandler(context, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);
        dp = new DatabaseHandler(context, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        // TODO Auto-generated constructor stub


    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        dia.setMessage("DOWNLOADING");
        dia.setCancelable(true);
        dia.show();
        // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

    }

    @Override
    protected String doInBackground(String... params) {


        try {

            Service sr = new Service(context);
            jsonResponseString = sr.getLogin(login, Url.baseurl
                    /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
        } catch (Exception e) {
            Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return jsonResponseString;


    }

    @Override
    protected void onPostExecute(String jsonResponse) {
        super.onPostExecute(jsonResponse);

        try {


            final JSONObject jObj = new JSONObject(jsonResponse);


            String status = jObj.getString("status");

            if (status.toString().equalsIgnoreCase("Success"))

            {
                calendarpath = pref.getString("calender", "");
                Log.e("calendar", calendarpath);


                new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {

                        try {


                            JSONObject schooinfo = jObj.getJSONObject("schoolInfo");//school


                            sch_details = new Staffdetails();

                            sch_details.setSchoolID(schooinfo.has("ID") ? Integer.parseInt(schooinfo.getString("ID")) : 0);
                            sch_details.setAcronym(schooinfo.has("SchoolAcronym") ? (schooinfo.getString("SchoolAcronym")) : "Dci");
                            // sch_details.setCityID();
                            //sch_details.setCountryID();
                            sch_details.setSchoolLogo(schooinfo.has("Logo") ? (schooinfo.getString("Logo")) : "Dci");
                            sch_details.setSchoolName(schooinfo.has("Name") ? (schooinfo.getString("Name")) : "Dci");
                            sch_details.setBackground(schooinfo.has("BackgroundImage") ? schooinfo.getString("BackgroundImage") : "");

//File creation


    /*if(!schoolname.exists())
    {*/

                            File schoolname = new File(Environment.getExternalStorageDirectory(), sch_details.getAcronym() + "_" + sch_details.getSchoolID());

                            schoolname.mkdirs();

                            File users = new File(schoolname.getAbsolutePath() + "/" + "Users");
                            users.mkdir();
                            File assessts = new File(schoolname.getAbsolutePath() + "/" + "Assets");
                            assessts.mkdir();


                            //}


                            String filename = sch_details.getSchoolLogo().substring(sch_details.getSchoolLogo().lastIndexOf("/") + 1, sch_details.getSchoolLogo().length());

                            String backgroundimage = sch_details.getBackground().substring(sch_details.getBackground().lastIndexOf("/") + 1, sch_details.getBackground().length());

                            try {
                                downloadfile(sch_details.getSchoolLogo(), assessts.getAbsolutePath(), "logo_" + filename);
                                downloadfile(sch_details.getBackground(), assessts.getAbsolutePath(), "back_" + backgroundimage);
                            } catch (Exception e) {
                                Log.e("first", "first" + e.getMessage());
                            }


                            sch_details.setSchoolLogo(assessts.getAbsolutePath() + "/" + "logo_" + filename);
                            sch_details.setBackground(assessts.getAbsolutePath() + "/" + "back_" + backgroundimage);


                            JSONObject staffinfo = jObj.getJSONObject("staffInfo");

                            staffdetails = new Staffloingdetails();
                            staffdetails.setStaffID(staffinfo.has("ID") ? Integer.parseInt(staffinfo.getString("ID")) : 0);
                            staffdetails.setFirstName(staffinfo.has("FirstName") ? staffinfo.getString("FirstName") : "");
                            staffdetails.setDOJ(staffinfo.has("DateOfJoining") ? staffinfo.getString("DateOfJoining") : "");
                            staffdetails.setPortalLoginID(staffinfo.has("PortalLoginID") ? staffinfo.getString("PortalLoginID") : "");
                            //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
                            staffdetails.setLastName(staffinfo.has("LastName") ? staffinfo.getString("LastName") : "");
                            staffdetails.setGender(staffinfo.has("Gender") ? staffinfo.getString("Gender") : "");
                            staffdetails.setDOB(staffinfo.has("DateOfBirth") ? staffinfo.getString("DateOfBirth") : "");
                            staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus") ? staffinfo.getString("MaritalStatus") : "");
                            ;
                            staffdetails.setSpouseName(staffinfo.has("SpuseName") ? staffinfo.getString("SpuseName") : "");
                            staffdetails.setFatherName(staffinfo.has("FatherName") ? staffinfo.getString("FatherName") : "");
                            staffdetails.setMotherName(staffinfo.has("MotherName") ? staffinfo.getString("MotherName") : "");
                            staffdetails.setPhone((staffinfo.has("PhoneNumber") ? staffinfo.getString("PhoneNumber") : ""));
                            staffdetails.setPhotoFilename(staffinfo.has("ProfileImage") ? staffinfo.getString("ProfileImage") : "");
                            staffdetails.setEmail(staffinfo.has("Email") ? staffinfo.getString("Email") : "");
                            staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade") ? staffinfo.getString("EmploymentGrade") : "");
                            staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory") ? Integer.parseInt(staffinfo.getString("StaffCategory")) : 0);
                            staffdetails.setTimetable(staffinfo.has("TimeTable") ? staffinfo.getString("TimeTable") : "");

                            String staffroom = staffinfo.has("Room") ? staffinfo.getString("Room") : "";
                            staffroom = staffroom.replaceAll("\\[", "").replaceAll("\\]", "");
                            staffroom = staffroom.replace("\"", "");
                            //details.setRoominfo(subarray);

                            if (staffroom.isEmpty()) {
                                staffdetails.setRoominfo("0");

                            } else {
                                staffdetails.setRoominfo(staffroom);

                            }

                            File username = new File(users.getAbsolutePath() + "/" + "T_" + staffdetails.getStaffID());
                            username.mkdir();
                            File materials = new File(username.getAbsolutePath() + "/" + "Materials");
                            materials.mkdir();

                            File Academic = new File(materials.getAbsolutePath() + "/" + "Academic");
                            Academic.mkdir();

                            File Reference = new File(materials.getAbsolutePath() + "/" + "Reference");
                            Reference.mkdir();
                            File evaluation = new File(username.getAbsolutePath() + "/" + "Evaluation");
                            evaluation.mkdir();
                            File notes = new File(username.getAbsolutePath() + "/" + "Notes");
                            notes.mkdir();
                            File calender = new File(username.getAbsolutePath() + "/" + "Calendar");
                            calender.mkdir();

                            edit.putString("calender", calender.getAbsolutePath());
                            edit.putString("academic", Academic.getAbsolutePath());
                            edit.putString("notes", notes.getAbsolutePath());
                            edit.putString("reference", Reference.getAbsolutePath());
                            edit.putString("Evaluation", evaluation.getAbsolutePath());
                            edit.commit();
                            calendarpath = pref.getString("calender", "");
                            Log.e("calendar", calendarpath);

                            String filename1 = staffdetails.getPhotoFilename().substring(staffdetails.getPhotoFilename().lastIndexOf("/") + 1, staffdetails.getPhotoFilename().length());

                            String filename2 = staffdetails.getTimetable().substring(staffdetails.getTimetable().lastIndexOf("/") + 1, staffdetails.getTimetable().length());

                            try {
                                downloadfile(staffdetails.getPhotoFilename(), username.getAbsolutePath(), filename1);
                                downloadfile(staffdetails.getTimetable(), username.getAbsolutePath(), filename2);
                            } catch (Exception e) {
                                Log.e("second", "second" + e.getMessage());
                            }

                            staffdetails.setTimetable(username.getAbsolutePath() + "/" + filename2);


                            staffdetails.setPhotoFilename(username.getAbsolutePath() + "/" + filename1);

                            if (db.checkschooldetails(sch_details.getSchoolID()).getCount() > 0) {
                                Log.e("update", "school");
                                db.updateschooldetails(sch_details);

                            } else {
                                db.schooldetails(sch_details);
                            }
                            /// db.getAllContacts();

                            if (db.checkstaffdetails(staffdetails.getStaffID()).getCount() > 0) {
                                Log.e("update", "staff");

                                db.updatestafflogindetails(staffdetails);
                            } else {
                                db.stafflogindetails(staffdetails);
                            }
                            // db.getallstafflogindetails();


                            JSONObject getmaster = jObj.getJSONObject("masterInfo");
                            JSONArray getmasterarray = getmaster.getJSONArray("Subjects");
                            JSONArray getcalendararray = getmaster.getJSONArray("CalendarType");
                            JSONArray roomsarray = getmaster.getJSONArray("Rooms");
                            // ArrayList<Rooms> roomsarraylist=new ArrayList<Rooms>();
                            for (int r = 0; r < roomsarray.length(); r++) {
                                Rooms room = new Rooms();
                                JSONObject roomobject = roomsarray.getJSONObject(r);
                                try {
                                    room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);

                                } catch (Exception e) {
                                    room.setId(0);
                                }
                                room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                                room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                                room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");

                                if (db.checkroomid(room.getId()).getCount() > 0) {
                                    Log.e("room", "update");
                                    db.updateaddmasterroom(room);

                                } else {
                                    db.addmasterroom(room);
                                }
                            }

                            //calendararrayl


                            for (int cal = 0; cal < getcalendararray.length(); cal++) {
                                JSONObject getcal = getcalendararray.getJSONObject(cal);
                                Calendarpojo pojo = new Calendarpojo();
                                pojo.setCalendarname(getcal.has("Name") ? getcal.getString("Name") : "0");
                                pojo.setCalendarid(getcal.has("ID") ? Integer.parseInt(getcal.getString("ID")) : 0);
                                pojo.setCalendardescription(getcal.has("Description") ? getcal.getString("Description") : "0");

                                String imagename = getcal.has("Image") ? getcal.getString("Image") : "0";

                                String batchfile = imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());

                                try {
                                    downloadfile(imagename, calendarpath, batchfile);

                                } catch (Exception e) {
                                    Log.e("third", "third" + e.getMessage());
                                }


                                pojo.setCalendarimage(calendarpath + "/" + batchfile);


                                if (db.checkcalendar(pojo.getCalendarid()).getCount() > 0) {
                                    Log.e("calendar", "update");

                                    db.updateaddcalendaremaster(pojo);
                                } else {
                                    db.addcalendaremaster(pojo);

                                }

                            }


                            Log.e("loading", "masterinfo");
                            for (int m = 0; m < getmasterarray.length(); m++) {
                                JSONObject getm = getmasterarray.getJSONObject(m);
                                Masterinfo minfo = new Masterinfo();
                                minfo.setID(getm.has("ID") ? Integer.parseInt(getm.getString("ID")) : 0);
                                minfo.setSubjectName(getm.has("SubjectName") ? getm.getString("SubjectName") : "");

                                minfo.setSchoolID(getm.has("SchoolID") ? Integer.parseInt(getm.getString("SchoolID")) : 0);
                                minfo.setStatus(getm.has("Status") ? getm.getString("Status") : "");
                                minfo.setSubjectDescription(getm.has("SubjectDescription") ? getm.getString("SubjectDescription") : "");

                                if (db.checksubjectid(minfo.getID()).getCount() > 0) {
                                    Log.e("masterinfo", "update");

                                    db.updatemasterinfo(minfo);
                                } else {
                                    db.masterinfo(minfo);

                                }
                      /*  "ID":"1",
                            "SubjectName":"English",
                            "SubjectDescription":"English - First Language",
                            "CreatedBy":"1",
                            "CreatedDate":"2017-04-18 00:00:00",
                            "ModifiedBy":"1",
                            "ModifiedDate":"2017-04-18 00:00:00",
                            "Status":"1",
                            "SchoolID":"1"*/

                            }

                            final JSONArray studentinfo = jObj.getJSONArray("studentDetails");

                            for (int s = 0; s < studentinfo.length(); s++) {
                                JSONObject getstud = studentinfo.getJSONObject(s);

                                Studentdetails details = new Studentdetails();


                                details.setStudentLoginID(getstud.getString("StudentLoginID"));
                                details.setStudentID(Integer.parseInt(getstud.getString("ID")));
                                details.setAdmissionNumber(getstud.getString("AdmissionNumber"));

                                details.setRollNo(getstud.getString("RollNumber"));

                                details.setFirstName(getstud.getString("FirstName"));
                                details.setLastName(getstud.getString("LastName"));
                                details.setDOB(getstud.getString("DateOfBirth"));
                                details.setDOA(getstud.getString("AdmissionDate"));
                                details.setGender(getstud.getString("DateOfBirth"));
                                details.setPhone_1(getstud.getString("Phone"));
                                details.setFatherName(getstud.getString("FatherName"));
                                details.setGuardianMobileNumber(getstud.getString("GuardianPhone"));
                                details.setMotherName(getstud.getString("GuardianPhone"));
                                details.setGuardianMobileNumber(getstud.getString("MotherName"));
                                details.setEmail(getstud.getString("Email"));
                                try {
                                    details.setBatchID(Integer.parseInt(getstud.getString("Batch")));
                                } catch (Exception e) {
                                    details.setBatchID(0);
                                }

                                String subarray = getstud.getJSONArray("Room").toString();
                                //  subarray=String str = "[Chrissman-@1]";
                                subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                subarray = subarray.replace("\"", "");
                                if (subarray.isEmpty()) {
                                    details.setRoominfo("0");

                                } else {
                                    details.setRoominfo(subarray);
                                }

                                details.setStaffID(staffdetails.getStaffID());
                                //  details.setBatchID();
                                details.setSchoolID(sch_details.getSchoolID());
                                details.setPhotoFilename(getstud.getString("ProfileImage"));
                                details.setClassID(Integer.parseInt(getstud.getString("ClassId")));
                                String photofilename = "s_" + details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/") + 1, details.getPhotoFilename().length());


                                try {
                                    Log.e("imagedownloading", photofilename);
                                    downloadfile(details.getPhotoFilename(), assessts.getAbsolutePath(), photofilename);
                                } catch (Exception e) {
                                    Log.e("fourth", e.getMessage() + "fourth");
                                }
                                details.setPhotoFilename(assessts.getAbsolutePath() + "/" + photofilename/*details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/")+1,details.getPhotoFilename().length()*/);


                                try {
                                    if (Integer.parseInt(details.getRollNo()) > 0) {

                                    }
                                } catch (Exception e) {
                                    details.setRollNo("0");
                                }
                                if (db.checkstudentdetails(details.getStudentID()).getCount() > 0) {
                                    Log.e("update", "student");

                                    db.updateStudent(details);
                                } else {
                                    db.Student(details);
                                }

                     /*   "ID":"1",
                            "AdmissionNumber":"60",
                            "AdmissionDate":"2013-04-18",
                            "RollNumber":"1201",
                            "FirstName":"kumaran",
                            "LastName":"R",
                            "MiddleName":"",
                            "DateOfBirth":"2002-02-18",
                            "Gender":"Male",
                            "Phone":"9898989898",
                            "FatherName":"Kannan",
                            "AddressLine1":"1\/481, main road, somarasam pettai, neart axia ATM",
                            "GuardianPhone":"7402516995",
                            "MotherName":"Divya",
                            "Email":"test@gmail.com",
                            "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/students\/profile_image\/1.jpg",
                            "DeviceControl":"0",
                            "StudentLoginID":"EK-001-S0000000001",
                            "ClassId":"1"*/


                            }
                            // db.deletesubject(pref.getString("staffid","0"));
                            db.deletesubject();

                            JSONArray classess = jObj.getJSONArray("Classes");
                            for (int c = 0; c < classess.length(); c++) {
                                JSONObject getclassobj = classess.getJSONObject(c);
                                tblclasses getclass = new tblclasses();
                                getclass.setClassID(getclassobj.has("ID") ? Integer.parseInt(getclassobj.getString("ID")) : 0);
                                getclass.setClassName(getclassobj.has("ClassName") ? (getclassobj.getString("ClassName")) : "");
                                getclass.setGrade(getclassobj.has("Grade") ? (getclassobj.getString("Grade")) : "");
                                getclass.setStaffID(staffdetails.getStaffID());

                                getclass.setClassCode(getclass.getClassName() + "-" + (getclassobj.has("Section") ? (getclassobj.getString("Section")) : ""));
                                String getclassname = getclass.getClassCode();
                                getclass.setDepartmentID((getclassobj.has("Department") ? Integer.parseInt(getclassobj.getString("Department")) : 0));
                                getclass.setInternetSSID(getclassobj.has("InternetSSID") ? (getclassobj.getString("InternetSSID")) : "");
                                ;
                                getclass.setInternetPassword(getclassobj.has("InternetPassword") ? (getclassobj.getString("InternetPassword")) : "");
                                ;
                                getclass.setInternetType(getclassobj.has("InternetType") ? (getclassobj.getString("InternetType")) : "");
                                ;


                                JSONArray batch = getclassobj.getJSONArray("batch");

                                for (int b = 0; b < batch.length(); b++) {
                                    Batchdetails bdetails = new Batchdetails();
                                    JSONObject bobject = batch.getJSONObject(b);
                           /* "ID":"1",
                                "Subjects":[  ],
                            "Coordinator":"3",
                                "StartDate":"2017-04-18",
                                "EndDate":"2017-04-18",
                                "TimeTable":"http:\/\/schoolproject.dci.in\/images\/staffs\/time_table\/testtimetable.xml",
                                "AcademicYear":"2016-2017",
                                "subject":[  ]*/
                                    bdetails.setSchoolID(sch_details.getSchoolID());
                                    bdetails.setTimeTable(bobject.has("TimeTable") ? bobject.getString("TimeTable") : "");
                                    bdetails.setAcademicYear(bobject.has("AcademicYear") ? bobject.getString("AcademicYear") : "");
                                    // getclass.setClassName(getclass.getClassName()+"-"+(getclassobj.has("Section")?(getclassobj.getString("Section")):"")+""+bdetails.getAcademicYear());
                                    bdetails.setBatchID(bobject.has("ID") ? Integer.parseInt(bobject.getString("ID")) : 0);
                                    bdetails.setClassID(getclass.getClassID());
                                    bdetails.setStaffID(staffdetails.getStaffID());

                                    // bdetails.setTimeTable();
                                    getclass.setClassCode(getclassname + " " + bdetails.getAcademicYear());
                                    getclass.setBatchid(bdetails.getBatchID() + "");

                                    if (db.checkclasses(getclass.getClassID(), getclass.getSchoolID(), staffdetails.getStaffID()).getCount() > 0) {
                                        Log.e("update", "class");

                                        db.updatetbclasses(getclass);

                                    } else {
                                        db.tbclasses(getclass);

                                    }

                                    bdetails.setBatchcode(getclass.getClassCode() + "" + bdetails.getAcademicYear());

                                    String batchfile = bdetails.getTimeTable().substring(bdetails.getTimeTable().lastIndexOf("/") + 1, bdetails.getTimeTable().length());

                                    try {
                                        downloadfile(bdetails.getTimeTable(), username.getAbsolutePath(), batchfile);

                                    } catch (Exception e) {
                                        Log.e("third", "third" + e.getMessage());
                                    }


                                    bdetails.setTimeTable(username.getAbsolutePath() + "/" + batchfile);


                                    //  bdetails.set
                                    if (db.checkbatch(bdetails.getClassID(), bdetails.getBatchID(), staffdetails.getStaffID()).getCount() > 0) {
                                        Log.e("update", "batch");

                                        db.updatebatch(bdetails);

                                    } else {
                                        db.batch(bdetails);

                                    }
                                    if (b == 0) {
                                        Log.e("delete", "subject");

                                    }
                                    tblSubject subject = new tblSubject();

                                    subject.setClassID(bdetails.getClassID());
                                    subject.setBatchID(bdetails.getBatchID());
                                    subject.setSchoolID(bdetails.getSchoolID());
                                    subject.setStaffID(staffdetails.getStaffID());

                                    //subject.setStaffID(bobject.has("Coordinator")?Integer.parseInt(bobject.getString("Coordinator")):0);


                                    if (bobject.has("subject")) {

                                        String subarray = bobject.getJSONArray("subject").toString();
                                        //  subarray=String str = "[Chrissman-@1]";
                                        subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                        subarray = subarray.replace("\"", "");
                                        String sSplit[] = subarray.split(",");
                                        if (sSplit.length > 0) {
                                            for (int s = 0; s < sSplit.length; s++) {
                                                subject.setSubjectID(Integer.parseInt(sSplit[s]));
                                                db.subject(subject);

                                            }
                                            //   for(int s=0; s<subarray.length(); s++)

                                        } else {
                                            subject.setSubjectID(-1);
                                            db.subject(subject);

                                        }
                                    } else {
                                        subject.setSubjectID(-1);
                                        db.subject(subject);
                                    }


                                    // bdetails.sets

                                }


                            }
                            try {


                                JSONArray pulsequestion = jObj.getJSONArray("PulseQuestionDetails");
                                for (int c1 = 0; c1 < pulsequestion.length(); c1++) {
                                    JSONObject pulse = pulsequestion.getJSONObject(c1);
                                   int PulseQuestionID = Integer.parseInt(pulse.getString("PulseQuestionID"));
                                   String PulseType = pulse.getString("PulseType");
                                   String PulseQuestion = pulse.getString("PulseQuestion");
                                   String CorrectAnswerpulse = pulse.getString("CorrectAnswer");
                                   String IsActive = pulse.getString("IsActive");
                                    Calendar calendar = Calendar.getInstance();
                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                    String strDate = mdformat.format(calendar.getTime());


                                    JSONArray batch1 = pulse.getJSONArray("options");
                                    boolean isExamIdexist = db.CheckIsIDAlreadypukseDBorNot(PulseQuestionID);


                                    if (isExamIdexist) {

                                        if (batch1.length() > 3) {
                                            db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), batch1.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));
                                        } else if (batch1.length() > 2) {
                                            db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                        } else if (batch1.length() > 1) {
                                            db.downloadUpdatePulse(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                        }

                                    } else {


                                        if (batch1.length() > 3) {
                                            db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), batch1.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));
                                        } else if (batch1.length() > 2) {
                                            db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), batch1.get(2).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                        } else if (batch1.length() > 1) {
                                            db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion, PulseType, batch1.get(0).toString(), batch1.get(1).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                        }

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            //pulse questionupdate


                        } catch (Exception e) {


                        }

                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);


                        Toast.makeText(context, "Updated Successfully", Toast.LENGTH_LONG).show();
                        //changemethod();
                        if (dia.isShowing())
                            dia.dismiss();
                    }
                }.execute();

            } else {
                Toast.makeText(context, jsonResponse.toUpperCase(), Toast.LENGTH_LONG).show();
                if (dia.isShowing())
                    dia.dismiss();
            }
        } catch (Exception e) {
            if (dia.isShowing())
                dia.dismiss();
        }
    }

    void downloadfile(String urls, String filepath, String filename) {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


    }



}
