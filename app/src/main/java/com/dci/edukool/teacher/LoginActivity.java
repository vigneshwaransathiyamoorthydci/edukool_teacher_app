package com.dci.edukool.teacher;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import Utilities.Service;
import Utilities.Utilss;
import Utilities.Url;
import  Utilities.DatabaseHandler;
import calendar.Calendarpojo;
import handraise.Handraise;
import helper.*;
import helper.Studentdetails;
import pulseexam.PulseExamPojo;
import Utilities.*;

/**
 * Created by iyyapparajr on 4/17/2017.
 */
public class LoginActivity extends Activity {
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal ;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    int Mark;
    String SubjectIDVal;
    String Subject ;
    int ObtainedMark ;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal ;
    int NegativeMark;
    int IsCorrect;
    String MarkForAnswer ;
    String ExamType ;
    String ExamDate ;
    int AspectID;
    int ExamTypeID;
    int TopicID;
    String TopicIDVal,Topic,AspectIDVal,Aspect,QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    int PulseQuestionID;
    String PulseType;
    int VersionPulse  =1;
    int StaffIDPluse;
    int SchoolIDPulse ;
    int IsActivePulse ;
    String CreatedOnPluse ;
    String UpdatedOn ;

    String PulseQuestion ;
    String CorrectAnswerpulse;
    String IsActive;
    EditText username;
    EditText password;
    TextView signin;
    RelativeLayout loginrel;
    String dbname;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    SharedPreferences pref;
    String ExamIDVal;
    SharedPreferences.Editor edit;
    int QuestionNumber;
    String question;
    Url url;
    Utilss utils;
    ProgressDialog dia;
    DatabaseHandler db;
    StringBuilder sb = new StringBuilder();
    Staffdetails sch_details;
    Staffloingdetails staffdetails;
    WifiManager mainWifi;
    LoginActivity.WifiReceiver receiverWifi;
    ArrayList<User> arrayOfUsers;
    UsersAdapter adapter;
    ListView listView;
    private PopupWindow pwindo,listwindo;
    String wifipass="";
    ImageView imgwifi;
    Staffdetails details;
    public static ArrayList<Handraise>handraise;
    private final Handler handler = new Handler();
    String res;
    String staffdbname;
    Attendancedb ab;
    String attenddbname;
    TextView text,text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginlayouttemp);
        text=(TextView) findViewById(R.id.text);
        text2=(TextView) findViewById(R.id.text2);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        // db=new DatabaseHandler(this);
        // db.addContact();
        // db.getclassdetails();
        Log.e("inserting", "insert");
        handraise=new ArrayList<>();
        edit=pref.edit();
        Permission.verifyStoragePermissions(this);
        if(pref.getBoolean("login",false))
        {
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
        utils=new Utilss(LoginActivity.this);
        username = (EditText) findViewById(R.id.usernameedittext);
        password = (EditText) findViewById(R.id.passwordedittext);
       /* username.setText("EK-004-T2018DEMO014");
        password.setText("EK2018DEMO014");*/
        signin = (TextView) findViewById(R.id.signinbiutton);
        loginrel= (RelativeLayout) findViewById(R.id.loginrel);
        imgwifi= (ImageView) findViewById(R.id.imgwifi);
        utils.setEdittexttypeface(5,username);
        utils.setEdittexttypeface(5, password);
        utils.setTextviewtypeface(5, signin);
        utils.setTextviewtypeface(3, text);
        utils.setTextviewtypeface(3, text2);
        imgwifi.bringToFront();
        //  username.setText( "EK-003-T1497361447");
        //  password.setText("EK-003-T1497361447");
        imgwifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "onClick: ");
                arrayOfUsers=new ArrayList<User>();
                Listpopup();
            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().isEmpty())
                {
                    username.setError("User ID not empty");
                }
                else if(password.getText().toString().isEmpty())
                {
                    password.setError("Password not empty");
                }
                else
                {

                    String split[]=username.getText().toString().split("-");

                   /* if(split.length>2)
                    {*/
                       /* String checkdb=split[0]+split[1]+"staff" ;

                        String attendb=split[0]+split[1]+"attendance" ;*/


                    String checkdb=username.getText().toString()+"staff" ;
                    String attendb=username.getText().toString()+"attendance";
                    dbname=checkdb;
                    attenddbname=attendb;

                    if(doesDatabaseExist(LoginActivity.this,checkdb))
                    {
                        edit.putString("staffdbname",dbname);
                        edit.putString("teachername",username.getText().toString());
                        edit.putString("teacherpass",password.getText().toString());
                        edit.putString("attendancedb",attenddbname);
                        edit.commit();

                        //    Toast.makeText(LoginActivity.this,"dbdone",Toast.LENGTH_SHORT).show();

                        db=new DatabaseHandler(LoginActivity.this,pref.getString("staffdbname",""), DatabaseHandler.DATABASE_VERSION);
                        ab=new Attendancedb(LoginActivity.this,pref.getString("attendancedb",""), Attendancedb.DATABASE_VERSION);

                        if(db.getstafflogindetails(username.getText().toString()).getCount()>0)
                        {
                            Staffloingdetails logindetails=db.staffdetailsuserlogin(username.getText().toString());
                            details=db.getschooldetails();
                            File schoolname= new File(Environment.getExternalStorageDirectory(), details.getAcronym()+"_"+details.getSchoolID());
                            schoolname.mkdirs();

                            File users=new File(schoolname.getAbsolutePath()+"/"+"Users");
                            users.mkdir();
                            File assessts=new File(schoolname.getAbsolutePath()+"/"+"Assets");
                            assessts.mkdir();

                            File username=new File(users.getAbsolutePath()+"/"+"T_"+logindetails.getStaffID());
                            username.mkdir();
                            File materials=new File(username.getAbsolutePath()+"/"+"Materials");
                            materials.mkdir();

                            File Academic=new File(materials.getAbsolutePath()+"/"+"Academic");
                            Academic.mkdir();

                            File Reference=new File(materials.getAbsolutePath()+"/"+"Reference");
                            Reference.mkdir();
                            File evaluation=new File(username.getAbsolutePath()+"/"+"Evaluation");
                            evaluation.mkdir();
                            File notes=new File(username.getAbsolutePath() + "/" + "Notes");
                            notes.mkdir();
                            File calender= new File(username.getAbsolutePath()+"/"+"Calendar");
                            calender.mkdir();

                            edit.putString("calender", calender.getAbsolutePath());
                            edit.putString("academic",Academic.getAbsolutePath());
                            edit.putString("notes",notes.getAbsolutePath());
                            edit.putString("reference",Reference.getAbsolutePath());
                            edit.putString("Evaluation",evaluation.getAbsolutePath());
                            edit.commit();
                            edit.putString("staffid", "" + logindetails.getStaffID());
                            edit.putString("portalstaffid", "" + logindetails.getPortalLoginID());
                            edit.putBoolean("login", true);
                            edit.commit();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();

                        }
                        else
                        {
                            String osversion = Build.VERSION.RELEASE;
                            String devname = android.os.Build.MODEL;
                            String ostype = "Android";
                            int versionCode = BuildConfig.VERSION_CODE;
                            String versionName = BuildConfig.VERSION_NAME;

                            String additionalparams="|Device Type:"+ostype+"|GCMKey:" +""+
                                    "|DeviceID:" +""+
                                    "|AppID:" +versionCode+
                                    "|IMEINumber:" +""+
                                    "|AppVersion:" +versionName+
                                    "|MACAddress:" +""+
                                    "|OSVersion:"+osversion;
                            String login_str="UserName:"+username.getText().toString()+"|Password:"+password.getText().toString()+"|Function:StaffLogin"+additionalparams;
                            login.clear();
                            byte[] data ;
                            try {
                                data = login_str.getBytes("UTF-8");
                                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                                if(utils.hasConnection()) {
                                    login.clear();
                                    login.add(new BasicNameValuePair("WS", base64_register));

                                    LoginActivity.Load_Login_WS load_plan_list = new LoginActivity.Load_Login_WS(LoginActivity.this, login);
                                    load_plan_list.execute();
                                }
                                else
                                {
                                    utils.Showalert();
                                }
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                         /*   edit.putString("staffid", "" + logindetails.getStaffID());
                            edit.putString("portalstaffid", "" + logindetails.getPortalLoginID());*/


                    }else
                    {
                        // Toast.makeText(LoginActivity.this,"dbnotcreated",Toast.LENGTH_SHORT).show();

                        String osversion = Build.VERSION.RELEASE;
                        String devname = android.os.Build.MODEL;
                        String ostype = "Android";
                        int versionCode = BuildConfig.VERSION_CODE;
                        String versionName = BuildConfig.VERSION_NAME;

                        String additionalparams="|Device Type:"+ostype+"|GCMKey:" +""+
                                "|DeviceID:" +""+
                                "|AppID:" +versionCode+
                                "|IMEINumber:" +""+
                                "|AppVersion:" +versionName+
                                "|MACAddress:" +""+
                                "|OSVersion:"+osversion;
                        String login_str="UserName:"+username.getText().toString()+"|Password:"+password.getText().toString()+"|Function:StaffLogin"+additionalparams;
                        login.clear();
                        byte[] data ;
                        try {
                            data = login_str.getBytes("UTF-8");
                            String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                            if(utils.hasConnection()) {
                                login.clear();
                                login.add(new BasicNameValuePair("WS", base64_register));

                                LoginActivity.Load_Login_WS load_plan_list = new LoginActivity.Load_Login_WS(LoginActivity.this, login);
                                load_plan_list.execute();
                            }
                            else
                            {
                                utils.Showalert();
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    //}




                }



                /*startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();*/
/*
                if (username.getText().toString().trim().isEmpty()) {

                    username.setError("Space not allowed");
                } else if (password.getText().toString().trim().isEmpty()) {

                    password.setError("Space not allowed");
                } else if (username.getText().toString().trim() != null
                        && password.getText().toString().trim() != null) {
                    byte[] data;

                    try {

                        String login_string = "method:login|user_loginid:"
                                + password.getText().toString()
                                + "|user_password:"
                                + password.getText().toString();

                        data = login_string.getBytes("UTF-8");
                        String base64_register = Base64.encodeToString(data, Base64.DEFAULT);*/
                //  System.out.println("Login result" + base64_register);

                //sr = new Service(getApplicationContext());

                // login.add(new BasicNameValuePair("WS", "VXNlck5hbWU6RUstMDAxLVQwMDAwMDAwMDF8UGFzc3dvcmQ6RUstMDAxLVQwMDAwMDAwMDF8RnVuY3Rpb246U3RhZmZMb2dpbg=="));

                //  login.add(new BasicNameValuePair("WS", "RUstMDAxLVQwMDAwMDAxMTF8UGFzc3dvcmQ6RUstMDAxLVQwMDAwMDAxMTF8RnVuY3Rpb246U3RhZmZMb2dpbg=="));


                    /*


                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else {
                    System.out.println("HAI ITS WORKING IN ELSE PART");
                }
*/



                /*Intent in=new Intent(LoginActivity.this,MainActivity.class);
                startActivity(in);
                finish();*/
            }
        });
        loginrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utilss.hideSoftKeyboard(LoginActivity.this);
            }
        });


    }
    private static boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        return dbFile.exists();
    }


    class Load_Login_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public Load_Login_WS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(LoginActivity.this);
            dia.setMessage("GET IN");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service  sr = new Service(LoginActivity.this);
                jsonResponseString = sr.getLogin(login,Url.baseurl
                        /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @SuppressLint("StaticFieldLeak")
        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            res=jsonResponse;
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {



                final JSONObject jObj = new JSONObject(String.valueOf(jsonResponse));


                String status = jObj.getString("status");

                if (status.equalsIgnoreCase("Success")) {


                    // pref.getString("teachername", "")+"|Password:"+pref.getString("teacherpass","")
                    edit.putString("staffdbname",dbname);
                    edit.putString("teachername",username.getText().toString());
                    edit.putString("teacherpass",password.getText().toString());
                    edit.putString("attendancedb",attenddbname);
                    edit.commit();
                    db=new DatabaseHandler(LoginActivity.this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
                    ab=new Attendancedb(LoginActivity.this,pref.getString("attendancedb",""),Attendancedb.DATABASE_VERSION);


                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(LoginActivity.this);
                            dia.setMessage("DOWNLOADING");
                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {




                                JSONObject schooinfo=jObj.getJSONObject("schoolInfo");//school
                                sch_details=new Staffdetails();

                                sch_details.setSchoolID(schooinfo.has("ID")?Integer.parseInt(schooinfo.getString("ID")):0);
                                sch_details.setAcronym(schooinfo.has("SchoolAcronym")?(schooinfo.getString("SchoolAcronym")):"Dci");
                                // sch_details.setCityID();
                                //sch_details.setCountryID();
                                sch_details.setSchoolLogo(schooinfo.has("Logo")?(schooinfo.getString("Logo")):"Dci");
                                sch_details.setSchoolName(schooinfo.has("Name")?(schooinfo.getString("Name")):"Dci");
                                sch_details.setBackground(schooinfo.has("BackgroundImage")?schooinfo.getString("BackgroundImage"):"");

//File creation


    /*if(!schoolname.exists())
    {*/

                                File schoolname= new File(Environment.getExternalStorageDirectory(), sch_details.getAcronym()+"_"+sch_details.getSchoolID());

                                schoolname.mkdirs();

                                File users=new File(schoolname.getAbsolutePath()+"/"+"Users");
                                users.mkdir();
                                File assessts=new File(schoolname.getAbsolutePath()+"/"+"Assets");
                                assessts.mkdir();


                                //}



                                String filename=sch_details.getSchoolLogo().substring(sch_details.getSchoolLogo().lastIndexOf("/") + 1, sch_details.getSchoolLogo().length());

                                String backgroundimage=sch_details.getBackground().substring(sch_details.getBackground().lastIndexOf("/")+1,sch_details.getBackground().length());

                                try {
                                    downloadfile(sch_details.getSchoolLogo(), assessts.getAbsolutePath(), "logo_" + filename);
                                    downloadfile(sch_details.getBackground(), assessts.getAbsolutePath(), "back_" + backgroundimage);
                                }
                                catch (Exception e)
                                {
                                    Log.e("first","first"+e.getMessage());
                                }



                                sch_details.setSchoolLogo(assessts.getAbsolutePath() + "/" + "logo_" + filename);
                                sch_details.setBackground(assessts.getAbsolutePath() + "/" + "back_" + backgroundimage);


                                JSONObject staffinfo=jObj.getJSONObject("staffInfo");

                                staffdetails=new Staffloingdetails();
                                staffdetails.setStaffID(staffinfo.has("ID") ? Integer.parseInt(staffinfo.getString("ID")) : 0);
                                staffdetails.setFirstName(staffinfo.has("FirstName") ? staffinfo.getString("FirstName") : "");
                                staffdetails.setDOJ(staffinfo.has("DateOfJoining") ? staffinfo.getString("DateOfJoining") : "");
                                staffdetails.setPortalLoginID(staffinfo.has("PortalLoginID") ? staffinfo.getString("PortalLoginID") : "");
                                //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
                                staffdetails.setLastName(staffinfo.has("LastName") ? staffinfo.getString("LastName") : "");
                                staffdetails.setGender(staffinfo.has("Gender") ? staffinfo.getString("Gender") : "");
                                staffdetails.setDOB(staffinfo.has("DateOfBirth") ? staffinfo.getString("DateOfBirth") : "");
                                staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus") ? staffinfo.getString("MaritalStatus") : "");;
                                staffdetails.setSpouseName(staffinfo.has("SpuseName") ? staffinfo.getString("SpuseName") : "");
                                staffdetails.setFatherName(staffinfo.has("FatherName") ? staffinfo.getString("FatherName") : "");
                                staffdetails.setMotherName(staffinfo.has("MotherName") ? staffinfo.getString("MotherName") : "");
                                staffdetails.setPhone((staffinfo.has("PhoneNumber") ? staffinfo.getString("PhoneNumber") : ""));
                                staffdetails.setPhotoFilename(staffinfo.has("ProfileImage") ? staffinfo.getString("ProfileImage") : "");
                                staffdetails.setEmail(staffinfo.has("Email") ? staffinfo.getString("Email") : "");
                                staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade") ? staffinfo.getString("EmploymentGrade") : "");
                                staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory") ? Integer.parseInt(staffinfo.getString("StaffCategory")) : 0);
                                staffdetails.setTimetable(staffinfo.has("TimeTable") ? staffinfo.getString("TimeTable") : "");
                                String staffroom=staffinfo.has("Room") ? staffinfo.getString("Room") : "";
                                staffroom    = staffroom.replaceAll("\\[", "").replaceAll("\\]", "");
                                staffroom = staffroom.replace("\"", "");
                                //details.setRoominfo(subarray);
                                edit.putString("portalstaffid", "" + staffdetails.getPortalLoginID());
                                edit.commit();


                                if(staffroom.isEmpty())
                                {
                                    staffdetails.setRoominfo("0");

                                }
                                else
                                {
                                    staffdetails.setRoominfo(staffroom);

                                }

                                File username=new File(users.getAbsolutePath()+"/"+"T_"+staffdetails.getStaffID());
                                username.mkdir();
                                File materials=new File(username.getAbsolutePath()+"/"+"Materials");
                                materials.mkdir();

                                File Academic=new File(materials.getAbsolutePath()+"/"+"Academic");
                                Academic.mkdir();

                                File Reference=new File(materials.getAbsolutePath()+"/"+"Reference");
                                Reference.mkdir();
                                File evaluation=new File(username.getAbsolutePath()+"/"+"Evaluation");
                                evaluation.mkdir();
                                File notes=new File(username.getAbsolutePath() + "/" + "Notes");
                                notes.mkdir();
                                File calender= new File(username.getAbsolutePath()+"/"+"Calendar");
                                calender.mkdir();

                                edit.putString("calender", calender.getAbsolutePath());
                                edit.putString("academic",Academic.getAbsolutePath());
                                edit.putString("notes",notes.getAbsolutePath());
                                edit.putString("reference",Reference.getAbsolutePath());
                                edit.putString("Evaluation",evaluation.getAbsolutePath());
                                edit.commit();
                                //edit.commit();

                                String filename1=staffdetails.getPhotoFilename().substring(staffdetails.getPhotoFilename().lastIndexOf("/")+1,staffdetails.getPhotoFilename().length());
                                String filename2=staffdetails.getTimetable().substring(staffdetails.getTimetable().lastIndexOf("/")+1,staffdetails.getTimetable().length());

                                try {
                                    downloadfile(staffdetails.getPhotoFilename(), username.getAbsolutePath(), filename1);
                                    downloadfile(staffdetails.getTimetable(),username.getAbsolutePath(),filename2);
                                }
                                catch (Exception e)
                                {
                                    Log.e("second","second"+e.getMessage());
                                }

                                staffdetails.setTimetable(username.getAbsolutePath() + "/" + filename2);
                                staffdetails.setPhotoFilename(username.getAbsolutePath() + "/" + filename1);

                                if(db.checkschooldetails(sch_details.getSchoolID()).getCount()>0) {
                                    Log.e("update","school");
                                    db.updateschooldetails(sch_details);

                                }
                                else
                                {
                                    db.schooldetails(sch_details);
                                }



                                if(db.checkstaffdetails(staffdetails.getStaffID()).getCount()>0) {
                                    Log.e("update","staff");

                                    db.updatestafflogindetails(staffdetails);
                                }
                                else {
                                    db.stafflogindetails(staffdetails);
                                }

                               // db.schooldetails(sch_details);
                                //db.getAllContacts();
                                //db.stafflogindetails(staffdetails);
                                //db.getallstafflogindetails();





                           /*   "BloodGroup":"A1 +ve",

                                "TotalExperience":"14.5",
                                "ExperienceDetails":"I have taught courses to UG students, Masters students with and without work experience and MBA students as well as executive MBA-type courses in the UK and Dubai, for companies such as the BBC, Ciba-Geigy and Emirates. My teaching experience includes a wide range of delivery methods:\r\nDistance learning (Open University)\r\nProblem based learning in groups of 10-15 students (Tilburg University, Maastricht University)\r\nLectures, seminars and tutorials (University of Bradford, University of Melbourne)",
                                "HighestQualification":"Ph.D.",
                                "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/staffs\/profile_image\/3.jpg",
                                "Designation":"Teacher",

                                "StaffCategory":"1",
                                "ModifiedBy":"1",
                                "TimeTable":"http:\/\/schoolproject.dci.in\/images\/staffs\/profile_image\/timetable\/1493791655.xml",
                                "SchoolID":"1",
                                "DeviceControl":"0",
                                "PortalLoginID":"EK-001-T000000001",
                                "Resume":""*/


                                //master info




                                JSONObject getmaster=jObj.getJSONObject("masterInfo");
                                JSONArray getmasterarray=getmaster.getJSONArray("Subjects");
                                JSONArray getcalendararray=getmaster.getJSONArray("CalendarType");
                                JSONArray roomsarray= getmaster.getJSONArray("Rooms");
                                // ArrayList<Rooms> roomsarraylist=new ArrayList<Rooms>();
                                for(int r=0; r<roomsarray.length(); r++)
                                {
                                    Rooms room=new Rooms();
                                    JSONObject roomobject=roomsarray.getJSONObject(r);
                                    try {
                                        room.setId(roomobject.has("ID") ? Integer.parseInt(roomobject.getString("ID")) : 0);

                                    }
                                    catch (Exception e)
                                    {
                                        room.setId(0);
                                    }
                                    room.setPassword(roomobject.has("Password") ? (roomobject.getString("Password")) : "0");
                                    room.setRoomsname(roomobject.has("RoomName") ? (roomobject.getString("RoomName")) : "0");
                                    room.setSsid(roomobject.has("SSID") ? (roomobject.getString("SSID")) : "0");
                                    if(db.checkroomid(room.getId()).getCount()>0)
                                    {
                                        Log.e("room","update");
                                        db.updateaddmasterroom(room);

                                    }
                                    else
                                    {
                                        db.addmasterroom(room);
                                    }

                                   // db.addmasterroom(room);
                                }

                                //calendararrayl


                                for(int cal=0; cal<getcalendararray.length(); cal++)
                                {
                                    JSONObject getcal=getcalendararray.getJSONObject(cal);
                                    helper.Calendarpojo pojo=new  helper.Calendarpojo();
                                    pojo.setCalendarname(getcal.has("Name") ? getcal.getString("Name") : "0");
                                    pojo.setCalendarid(getcal.has("ID")?Integer.parseInt(getcal.getString("ID")):0);
                                    pojo.setCalendardescription(getcal.has("Description") ? getcal.getString("Description") :"0");

                                    String imagename=getcal.has("Image")?getcal.getString("Image"):"0";

                                    String batchfile=imagename.substring(imagename.lastIndexOf("/") + 1, imagename.length());

                                    try
                                    {
                                        downloadfile(imagename,calender.getAbsolutePath(),batchfile);

                                    }
                                    catch (Exception e)
                                    {
                                        Log.e("third","third"+e.getMessage());
                                    }


                                    pojo.setCalendarimage(calender.getAbsolutePath() + "/" + batchfile);



                                    if(db.checkcalendar(pojo.getCalendarid()).getCount()>0) {
                                        Log.e("calendar","update");

                                        db.updateaddcalendaremaster(pojo);
                                    }
                                    else {
                                        db.addcalendaremaster(pojo);

                                    }

                                    // db.addcalendaremaster(pojo);

                                }



                                Log.e("loading","masterinfo");
                                for(int m=0; m<getmasterarray.length(); m++)
                                {
                                    JSONObject getm=getmasterarray.getJSONObject(m);
                                    Masterinfo minfo=new Masterinfo();
                                    minfo.setID(getm.has("ID") ? Integer.parseInt(getm.getString("ID")) : 0);
                                    minfo.setSubjectName(getm.has("SubjectName") ? getm.getString("SubjectName") : "");

                                    minfo.setSchoolID(getm.has("SchoolID") ? Integer.parseInt(getm.getString("SchoolID")) : 0);
                                    minfo.setStatus(getm.has("Status") ? getm.getString("Status") : "");
                                    minfo.setSubjectDescription(getm.has("SubjectDescription") ? getm.getString("SubjectDescription") : "");



                                    if(db.checksubjectid(minfo.getID()).getCount()>0) {
                                        Log.e("masterinfo","update");

                                        db.updatemasterinfo(minfo);
                                    }
                                    else
                                    {
                                        db.masterinfo(minfo);

                                    }

                                   // db.masterinfo(minfo);
                      /*  "ID":"1",
                            "SubjectName":"English",
                            "SubjectDescription":"English - First Language",
                            "CreatedBy":"1",
                            "CreatedDate":"2017-04-18 00:00:00",
                            "ModifiedBy":"1",
                            "ModifiedDate":"2017-04-18 00:00:00",
                            "Status":"1",
                            "SchoolID":"1"*/

                                }
                               // db.masterinfodetails();




                                JSONArray pulsequestion=jObj.getJSONArray("PulseQuestionDetails");
                                for(int c=0; c<pulsequestion.length(); c++)
                                {
                                    JSONObject getclassobj=pulsequestion.getJSONObject(c);
                                    PulseQuestionID = Integer.parseInt(getclassobj.getString("PulseQuestionID"));
                                    PulseType = getclassobj.getString("PulseType");
                                    PulseQuestion =  getclassobj.getString("PulseQuestion");
                                    CorrectAnswerpulse =getclassobj.getString("CorrectAnswer");
                                    IsActive = getclassobj.getString("IsActive");
                                    Calendar calendar = Calendar.getInstance();
                                    SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                    String strDate = mdformat.format(calendar.getTime());


                                    JSONArray batch=getclassobj.getJSONArray("options");

                                    if(batch.length()>3){
                                        db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion,PulseType, batch.get(0).toString(), batch.get(1).toString(), batch.get(2).toString(), batch.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));
                                    }

                                    else if(batch.length()>2){
                                        db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion,PulseType, batch.get(0).toString(), batch.get(1).toString(), batch.get(2).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                    }

                                    else if(batch.length()>1){
                                        db.addPuseQuestion(new PulseExamPojo(PulseQuestionID, PulseQuestion,PulseType, batch.get(0).toString(), batch.get(1).toString(), batch.get(3).toString(), CorrectAnswerpulse, 1, StaffIDPluse, SchoolIDPulse, 1, strDate, strDate));

                                    }

                                }

                              db.deletesubject(""+staffdetails.getStaffID());

//batch and class

                                JSONArray classess=jObj.getJSONArray("Classes");
                                for(int c=0; c<classess.length(); c++)
                                {
                                    JSONObject getclassobj=classess.getJSONObject(c);
                                    tblclasses getclass=new tblclasses();
                                    getclass.setClassID(getclassobj.has("ID") ? Integer.parseInt(getclassobj.getString("ID")) : 0);
                                    getclass.setClassName(getclassobj.has("ClassName") ? (getclassobj.getString("ClassName")) : "");
                                    getclass.setGrade(getclassobj.has("Grade") ? (getclassobj.getString("Grade")) : "");
                                    getclass.setStaffID(staffdetails.getStaffID());
                                    getclass.setClassCode(getclass.getClassName() + "-" + (getclassobj.has("Section") ? (getclassobj.getString("Section")) : ""));
                                    String getclassname=getclass.getClassCode();
                                    getclass.setDepartmentID((getclassobj.has("Department")?Integer.parseInt(getclassobj.getString("Department")):0));
                                    getclass.setInternetSSID(getclassobj.has("InternetSSID") ? (getclassobj.getString("InternetSSID")) : "");;
                                    getclass.setInternetPassword(getclassobj.has("InternetPassword")?(getclassobj.getString("InternetPassword")):"");;
                                    getclass.setInternetType(getclassobj.has("InternetType")?(getclassobj.getString("InternetType")):"");;


                                    JSONArray batch=getclassobj.getJSONArray("batch");

                                    for(int b=0; b<batch.length(); b++)
                                    {
                                        Batchdetails bdetails=new Batchdetails();
                                        JSONObject bobject=batch.getJSONObject(b);
                           /* "ID":"1",
                                "Subjects":[  ],
                            "Coordinator":"3",
                                "StartDate":"2017-04-18",
                                "EndDate":"2017-04-18",
                                "TimeTable":"http:\/\/schoolproject.dci.in\/images\/staffs\/time_table\/testtimetable.xml",
                                "AcademicYear":"2016-2017",
                                "subject":[  ]*/
                                        bdetails.setSchoolID(sch_details.getSchoolID());
                                        bdetails.setTimeTable(bobject.has("TimeTable") ? bobject.getString("TimeTable") : "");
                                        bdetails.setAcademicYear(bobject.has("AcademicYear") ? bobject.getString("AcademicYear") : "");
                                        // getclass.setClassName(getclass.getClassName()+"-"+(getclassobj.has("Section")?(getclassobj.getString("Section")):"")+""+bdetails.getAcademicYear());
                                        bdetails.setBatchID(bobject.has("ID")?Integer.parseInt(bobject.getString("ID")):0);
                                        bdetails.setClassID(getclass.getClassID());
                                        // bdetails.setTimeTable();
                                        bdetails.setStaffID(staffdetails.getStaffID());

                                        getclass.setClassCode(getclassname+" "+bdetails.getAcademicYear());
                                        getclass.setBatchid(bdetails.getBatchID() + "");
                                        // getclass.setStaffID();

                                     //   db.tbclasses(getclass);

                                        if(db.checkclasses(getclass.getClassID(),getclass.getSchoolID(),staffdetails.getStaffID()).getCount()>0)
                                        {
                                            Log.e("update","class");

                                            db.updatetbclasses(getclass);

                                        }
                                        else
                                        {
                                            db.tbclasses(getclass);

                                        }

                                        bdetails.setBatchcode(getclass.getClassCode()+""+bdetails.getAcademicYear());

                                        String batchfile=bdetails.getTimeTable().substring(bdetails.getTimeTable().lastIndexOf("/")+1,bdetails.getTimeTable().length());

                                        try
                                        {
                                            downloadfile(bdetails.getTimeTable(),username.getAbsolutePath(),batchfile);

                                        }
                                        catch (Exception e)
                                        {
                                            Log.e("third","third"+e.getMessage());
                                        }


                                        bdetails.setTimeTable(username.getAbsolutePath() + "/" + batchfile);


                                        //  bdetails.set
                                       // db.batch(bdetails);

                                        if(db.checkbatch(bdetails.getClassID(),bdetails.getBatchID(),staffdetails.getStaffID()).getCount()>0)
                                        {
                                            Log.e("update","batch");

                                            db.updatebatch(bdetails);

                                        }
                                        else
                                        {
                                            db.batch(bdetails);

                                        }
                                        tblSubject subject=new tblSubject();

                                        subject.setClassID(bdetails.getClassID());
                                        subject.setBatchID(bdetails.getBatchID());
                                        subject.setSchoolID(bdetails.getSchoolID());
                                        //  getclass.setStaffID(staffdetails.getStaffID());

                                        subject.setStaffID(staffdetails.getStaffID());
                                       // subject.setStaffID(bobject.has("Coordinator")?Integer.parseInt(bobject.getString("Coordinator")):0);

                                        if(bobject.has("subject")) {

                                            String subarray = bobject.getJSONArray("subject").toString();
                                            Log.i("subjectarray",subarray);
                                            //  subarray=String str = "[Chrissman-@1]";
                                            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                            subarray = subarray.replace("\"", "");
                                            String sSplit[] = subarray.split(",");
                                            Log.i("subjectarray1",""+ Arrays.toString(sSplit));
                                            if (sSplit.length > 0) {
                                                for (int s = 0; s < sSplit.length; s++) {
                                                    Log.i("subjectarray2",""+Integer.parseInt(sSplit[s].trim()));
                                                    subject.setSubjectID(Integer.parseInt(sSplit[s].trim()));
                                                    db.subject(subject);

                                                }
                                                //   for(int s=0; s<subarray.length(); s++)

                                            } else {
                                                subject.setSubjectID(-1);
                                                db.subject(subject);

                                            }
                                        }else
                                        {
                                            subject.setSubjectID(-1);
                                            db.subject(subject);
                                        }




                                        // bdetails.sets

                                    }




                                }
                                // db.getbatchdetails(1);
                  /*  JSONArray content= jObj.getJSONArray("contentDetails");
                    for(int c=0; c<content.length(); c++)
                    {
                        Tablecontent g_content=new Tablecontent();
                        JSONObject con=content.getJSONObject(c);
                      //  g_content.setBatchID();

                        g_content.setContentID(Integer.parseInt(con.getString("ID")));
                        g_content.setContentFilename(con.getString("ContentFileName"));
                        g_content.setContentCatalogType(con.getString("ContentType"));
                        g_content.setContentDescription(con.getString("ContentTitle"));
                        g_content.setVaporize(con.getString("Vaporize"));
                        g_content.setSubject(con.getJSONArray("Subject").toString());
                        g_content.setClasses(con.getJSONArray("Class").toString());

                        g_content.setType(con.getString("Catalog"));




                        String contentfilename=g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/")+1,g_content.getContentFilename().length());
                       Log.e("contentfile",contentfilename);

                        if(con.getString("Catalog").equalsIgnoreCase("Academic")) {

                            try
                            {
                           downloadfile(g_content.getContentFilename(), Academic.getAbsolutePath(), contentfilename);

                            }
                            catch (Exception e)
                            {
                                Log.e("third","third"+e.getMessage());
                            }
                            g_content.setContentFilename(Academic.getAbsolutePath() + "/" + contentfilename*//*g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/")+1,g_content.getContentFilename().length())*//*);

                        }
                        else
                        {
                            Log.e("catlog", "reference");

                            try
                            {
                              downloadfile(g_content.getContentFilename(), Reference.getAbsolutePath(), contentfilename);

                            }
                            catch (Exception e)
                            {
                                Log.e("video","third"+e.getMessage());
                            }

                            g_content.setContentFilename(Reference.getAbsolutePath() + "/" + contentfilename*//*g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/")+1,g_content.getContentFilename().length())*//*);

                        }
                     db.content(g_content);
                        //db.s
                        *//*"ContentTitle":"CS V2L9-4",
                            "ContentType":"Video",
                            "Vaporize":"F",
                            "Auther":"",
                            "ContentFileName":"http:\/\/schoolproject.dci.in\/contents\/uploads\/1493790487.mp4",
                            "Catalog":"Reference",
                            "Thumbnail":"*//*

                    }
             */  /*     "ID":"1",
                            "AdmissionNumber":"60",
                            "AdmissionDate":"2013-04-18",
                            "RollNumber":"1201",
                            "FirstName":"kumaran",
                            "LastName":"R",
                            "MiddleName":"",
                            "DateOfBirth":"2002-02-18",
                            "Gender":"Male",
                            "Phone":"9898989898",
                            "FatherName":"Kannan",
                            "AddressLine1":"1\/481, main road, somarasam pettai, neart axia ATM",
                            "GuardianPhone":"7402516995",
                            "MotherName":"Divya",
                            "Email":"test@gmail.com",
                            "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/students\/profile_image\/1.jpg",
                            "DeviceControl":"0",
                            "StudentLoginID":"EK-001-S0000000001",
                            "ClassId":"1"*/

                                JSONArray CalendarArray= jObj.getJSONArray("Calender");
                                ArrayList<Calendarpojo> calendarlist=new ArrayList<Calendarpojo>();


                                Log.e("calenlensize",CalendarArray.length()+"");
                                for(int c=0; c<CalendarArray.length(); c++)
                                {
                                    Calendarpojo cpojo=new Calendarpojo();
                                    JSONObject con=CalendarArray.getJSONObject(c);
                                    //  g_content.setBatchID();

                                    cpojo.setEventid(Integer.parseInt(con.getString("ID")));
                                    cpojo.setEventname(con.getString("EventName"));
                                    cpojo.setEventDesc(con.getString("EventDescription"));
                                    cpojo.setEventstart(con.getString("EventStartDate"));
                                    cpojo.setEventend(con.getString("EventEndDate"));
                                    cpojo.setCategory(con.getString("Category"));
                                    cpojo.setCategoryid(Integer.parseInt(con.getString("CategoryID")));

                                    String calfile=con.getString("CategoryImage").substring(con.getString("CategoryImage").lastIndexOf("/")+1,con.getString("CategoryImage").length());

                                    try {
                                        downloadfile(con.getString("CategoryImage"), calender.getAbsolutePath(), calfile);
                                    }catch(Exception e){}

                                    cpojo.setCategoryIcon(con.getString("CategoryImage"));
                                    calendarlist.add(cpojo);

                                }
                                db.TableCalendar(calendarlist);


                                final JSONArray studentinfo= jObj.getJSONArray("studentDetails");

                                for(int s=0; s<studentinfo.length(); s++)
                                {
                                    JSONObject getstud=studentinfo.getJSONObject(s);

                                    Studentdetails details=new Studentdetails();


                                    details.setStudentLoginID(getstud.getString("StudentLoginID"));
                                    details.setStudentID(Integer.parseInt(getstud.getString("ID")));
                                    details.setAdmissionNumber(getstud.getString("AdmissionNumber"));

                                    details.setRollNo(getstud.getString("RollNumber"));

                                    details.setFirstName(getstud.getString("FirstName"));
                                    details.setLastName(getstud.getString("LastName"));
                                    details.setDOB(getstud.getString("DateOfBirth"));
                                    details.setDOA(getstud.getString("AdmissionDate"));
                                    details.setGender(getstud.getString("DateOfBirth"));
                                    details.setPhone_1(getstud.getString("Phone"));
                                    details.setFatherName(getstud.getString("FatherName"));
                                    details.setGuardianMobileNumber(getstud.getString("GuardianPhone"));
                                    details.setMotherName(getstud.getString("GuardianPhone"));
                                    details.setGuardianMobileNumber(getstud.getString("MotherName"));
                                    details.setEmail(getstud.getString("Email"));
                                    try {
                                        details.setBatchID(Integer.parseInt(getstud.getString("Batch")));
                                    }
                                    catch (Exception e)
                                    {
                                        details.setBatchID(0);
                                    }

                                    String subarray = getstud.getJSONArray("Room").toString();
                                    //  subarray=String str = "[Chrissman-@1]";
                                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                    subarray = subarray.replace("\"", "");
                                    if(subarray.isEmpty()) {
                                        details.setRoominfo("0");

                                    }
                                    else
                                    {
                                        details.setRoominfo(subarray);
                                    }

                                    details.setStaffID(staffdetails.getStaffID());
                                    //  details.setBatchID();
                                    details.setSchoolID(sch_details.getSchoolID());
                                    details.setPhotoFilename(getstud.getString("ProfileImage"));
                                    details.setClassID(Integer.parseInt(getstud.getString("ClassId")));
                                    String photofilename="s_"+details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/")+1,details.getPhotoFilename().length());




                                    try {
                                        Log.e("imagedownloading",photofilename);
                                        downloadfile(details.getPhotoFilename(), assessts.getAbsolutePath(), photofilename);
                                    }
                                    catch (Exception e)
                                    {
                                        Log.e("fourth",e.getMessage()+"fourth");
                                    }
                                    details.setPhotoFilename(assessts.getAbsolutePath() + "/" + photofilename/*details.getPhotoFilename().substring(details.getPhotoFilename().lastIndexOf("/")+1,details.getPhotoFilename().length()*/);


                                    try {
                                        if(Integer.parseInt(details.getRollNo())>0) {

                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        details.setRollNo("0");
                                    }

                                    if(db.checkstudentdetails(details.getStudentID()).getCount()>0)
                                    {
                                        Log.e("update","student");

                                        db.updateStudent(details);
                                    }
                                    else {
                                        db.Student(details);
                                    }

                                  //  db.Student(details);

                     /*   "ID":"1",
                            "AdmissionNumber":"60",
                            "AdmissionDate":"2013-04-18",
                            "RollNumber":"1201",
                            "FirstName":"kumaran",
                            "LastName":"R",
                            "MiddleName":"",
                            "DateOfBirth":"2002-02-18",
                            "Gender":"Male",
                            "Phone":"9898989898",
                            "FatherName":"Kannan",
                            "AddressLine1":"1\/481, main road, somarasam pettai, neart axia ATM",
                            "GuardianPhone":"7402516995",
                            "MotherName":"Divya",
                            "Email":"test@gmail.com",
                            "ProfileImage":"http:\/\/schoolproject.dci.in\/images\/students\/profile_image\/1.jpg",
                            "DeviceControl":"0",
                            "StudentLoginID":"EK-001-S0000000001",
                            "ClassId":"1"*/


                                }
                                db.studentinfo();



                                Log.e("examdetails,","details");

                                // JSONArray Exam_arr=jObj.getJSONObject("examDetails").getJSONArray("exam");

  /*for (int i = 0; i < Exam_arr.length(); i++) {
        JSONObject objexam=Exam_arr.getJSONObject(i);
        ExamIDVal = objexam.getString("ExamID");
        ExamID = Integer.parseInt(ExamIDVal);
        ExamCategoryIDVal = objexam.getString("ExamCategoryID");
        ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
        ExamCategory = objexam.getString("ExamCategory");
        ExamCode = objexam.getString("ExamCode");
        ExamDescription = objexam.getString("ExamDescription");
        BatchIDVal = objexam.getString("BatchID");
        ExamDurationVal = objexam.getString("ExamDuration");
        BatchID = Integer.parseInt(BatchIDVal);
        ExamDuration = Integer.parseInt(ExamDurationVal);

        SubjectIDVal = objexam.getString("SubjectID");
        Subject = objexam.getString("Subject");

        SubjectID = Integer.parseInt(SubjectIDVal);
        ExamTypeIDVal = objexam.getString("ExamTypeID");

        ExamType = objexam.getString("ExamType");

        // ExamDate = objexam.getString("ExamDate");

        ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   *//* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*//*
        // String Questions = objexam.getString("Questions");

        int ExamSequence =0;
        int SchoolID =0;
        int IsResultPublished =0;
        int ExamShelfID =0;
        int ClassID =0;
        int TimeTaken = 0;
        int TotalScore =0;
        String DateAttended ="01/06/2017 10:00:00";
        db.addExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                BatchID, IsResultPublished,
                ExamShelfID,
                TimeTaken,
                DateAttended,
                TotalScore));
        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

        JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
        QuizWrapper newItemObject = null;

        for(int j = 0; j < jsonArrayquestion.length(); j++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArrayquestion.getJSONObject(j);
                String QuestionIDval = jsonChildNode.getString("QuestionID");
                QuestionID = Integer.parseInt(QuestionIDval);
                TopicIDVal = jsonChildNode.getString("TopicID");
                if(TopicIDVal.equalsIgnoreCase("")){
                    TopicID=1;
                }
                else{
                    TopicID = Integer.parseInt(TopicIDVal);

                }

                // int TopicID=1;
                Topic = jsonChildNode.getString("Topic");
                AspectIDVal = jsonChildNode.getString("AspectID");
                if(AspectIDVal.equalsIgnoreCase("")){
                    AspectID=1;
                }
                else{
                    AspectID = Integer.parseInt(AspectIDVal);

                }
                Aspect = jsonChildNode.getString("Aspect");
                QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                QuestionNumber = Integer.parseInt(QuestionNumberVal);
                question= jsonChildNode.getString("Question");
                JSONArray options = jsonChildNode.getJSONArray("Options");
                CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                CorrectAnswerVal = 1;
                MarkVal = jsonChildNode.getString("Mark");
                Mark = Integer.parseInt(MarkVal);

                NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                NegativeMark = Integer.parseInt(NegativeMarkVal);
                String Created_on = "01/06/2017 10:00:00";
                String ModifiedOn = "01/06/2017 10:00:00";
                StudentAnswer ="";
                newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                if(options.length()>3){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }

                else if(options.length()>2){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }

                else if(options.length()>1){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }
                List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                String strDate = mdformat.format(calendar.getTime());

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam2: ", log);
                }

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question2: ", log);
                }

                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Adding child data

        // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
        //jsonObject.add(newItemObject);

    }*/



                                //JSONArray selfExam_arr=jObj.getJSONObject("selfExamDetails").getJSONArray("exam");
  /*/*  for (int i = 0; i < selfExam_arr.length(); i++) /*{
        JSONObject objexam=selfExam_arr.getJSONObject(i);
        ExamIDVal = objexam.getString("ExamID");
        ExamID = Integer.parseInt(ExamIDVal);
        ExamCategoryIDVal = objexam.getString("ExamCategoryID");
        ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
        ExamCategory = objexam.getString("ExamCategory");
        ExamCode = objexam.getString("ExamCode");
        ExamDescription = objexam.getString("ExamDescription");
        BatchIDVal = objexam.getString("BatchID");
        ExamDurationVal = objexam.getString("ExamDuration");
        BatchID = Integer.parseInt(BatchIDVal);
        ExamDuration = Integer.parseInt(ExamDurationVal);

        SubjectIDVal = objexam.getString("SubjectID");
        Subject = objexam.getString("Subject");

        SubjectID = Integer.parseInt(SubjectIDVal);
        ExamTypeIDVal = objexam.getString("ExamTypeID");

        ExamType = objexam.getString("ExamType");

        // ExamDate = objexam.getString("ExamDate");

        ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   *//* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*//*
        // String Questions = objexam.getString("Questions");
        int ExamSequence =0;
        int SchoolID =0;
        int IsResultPublished =0;
        int ExamShelfID =0;
        int ClassID =0;
        int TimeTaken = 0;
        int TotalScore =0;
        String DateAttended ="01/06/2017 10:00:00";
        db.addExamDetails(new ExamDetails(ExamID, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                BatchID, IsResultPublished,
                ExamShelfID,
                TimeTaken,
                DateAttended,
                TotalScore));
        List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();

        JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
        QuizWrapper newItemObject = null;

        for(int j = 0; j < jsonArrayquestion.length(); j++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArrayquestion.getJSONObject(j);
                String QuestionIDval = jsonChildNode.getString("QuestionID");
                QuestionID = Integer.parseInt(QuestionIDval);
                TopicIDVal = jsonChildNode.getString("TopicID");
                if(TopicIDVal.equalsIgnoreCase("")){
                    TopicID=1;
                }
                else{
                    TopicID = Integer.parseInt(TopicIDVal);

                }

                // int TopicID=1;
                Topic = jsonChildNode.getString("Topic");
                AspectIDVal = jsonChildNode.getString("AspectID");
                if(AspectIDVal.equalsIgnoreCase("")){
                    AspectID=1;
                }
                else{
                    AspectID = Integer.parseInt(AspectIDVal);
                }
                Aspect = jsonChildNode.getString("Aspect");
                QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                QuestionNumber = Integer.parseInt(QuestionNumberVal);
                question= jsonChildNode.getString("Question");
                JSONArray options = jsonChildNode.getJSONArray("Options");
                CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                CorrectAnswerVal = 1;
                MarkVal = jsonChildNode.getString("Mark");
                Mark = Integer.parseInt(MarkVal);

                NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                NegativeMark = Integer.parseInt(NegativeMarkVal);
                String Created_on = "01/06/2017 10:00:00";
                String ModifiedOn = "01/06/2017 10:00:00";
                StudentAnswer ="";
                newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                if(options.length()>3){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(),CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }

                else if(options.length()>2){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }

                else if(options.length()>1){
                    db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), "", "",CorrectAnswer , Mark,
                            NegativeMark, StudentAnswer,
                            IsCorrect,
                            ObtainedMark,
                            Created_on,
                            ModifiedOn));
                }
                List<ExamDetails> examdetailsList = db.getAllExamsDetails();
                List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                String strDate = mdformat.format(calendar.getTime());

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam2: ", log);
                }

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question2: ", log);
                }

                jsonObject.add(newItemObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Adding child data

        // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
        //jsonObject.add(newItemObject);

    }*/


                   /* String staffname=jObj.getJSONObject("staffInfo").getString("FirstName");
                    edit.putString("name",staffname);
                    edit.putString("schoologo",jObj.getJSONObject("schoolInfo").getString("Logo"));
                    edit.putString("backgroundlogo",jObj.getJSONObject("schoolInfo").getString("BackgroundImage"));
                    edit.putString("profileimage",jObj.getJSONObject("staffInfo").getString("ProfileImage"));

                    edit.commit();
                    startActivity(new Intent(LoginActivity.this,MainActivity.class));

                    System.out.println("USER RESPONSE IF");*/
                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                            edit.putBoolean("login",true);
                            edit.commit();
                            //  new  validateUserTask2(res).execute();

                            startActivity(new Intent(LoginActivity.this,MainActivity.class));
                            finish();

                        }
                    }.execute();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                System.out.println(e.toString() + "zcx");
            }

        }
    }

    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        registerReceiver(receiverWifi, new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        synchronized (this) {
            if(receiverWifi!=null)
                unregisterReceiver(receiverWifi);
        }
    }

    public void doInback() {

        handler.postDelayed(new Runnable() {

            @SuppressLint("WifiManagerLeak")
            @Override
            public void run() {
                // TODO Auto-generated method stub

                mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

                receiverWifi = new WifiReceiver();
                registerReceiver(receiverWifi, new IntentFilter(
                        WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                if (mainWifi.isWifiEnabled() == false) {
                    mainWifi.setWifiEnabled(true);
                }

                mainWifi.startScan();
                //  String ssid=getCurrentSsid(getApplicationContext(),mainWifi);

                // doInback();
            }
        }, 1000);

    }

    private void initiatePopupWindow(final String ssid) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) LoginActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup2,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 450, 130, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            final EditText password=(EditText)layout.findViewById(R.id.edit);

            Button btnconnect = (Button) layout.findViewById(R.id.button1);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    wifipass=password.getText().toString();
                    if(!wifipass.equalsIgnoreCase("")) {
                        WifiConfiguration wc = new WifiConfiguration();
                        wc.SSID = String.format("\"%s\"", ssid);
                        wc.preSharedKey = String.format("\"%s\"", wifipass);
                        wc.status = WifiConfiguration.Status.ENABLED;
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

                        int netId = mainWifi.addNetwork(wc);
                        mainWifi.disconnect();
                        mainWifi.enableNetwork(netId, true);
                        mainWifi.reconnect();
                        doInback();
                    }

                    pwindo.dismiss();
                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.button2);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void Listpopup(){
        Log.d("Listpopup", "Listpopup: "+"calling" );
        LayoutInflater inflater = (LayoutInflater) LoginActivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.list_popup,
                (ViewGroup) findViewById(R.id.listparent));
        listwindo = new PopupWindow(layout, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT, true);
        listwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);
        listView=(ListView)layout.findViewById(R.id.list);
        ImageView imgclose= (ImageView) layout.findViewById(R.id.closeimg);

        final Button close=(Button)layout.findViewById(R.id.close);

        arrayOfUsers = new ArrayList<User>();
        //   for(int i=0;i<6;i++)
        arrayOfUsers.add(new User("", ""));

        adapter = new UsersAdapter(this, arrayOfUsers);
        listView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(adapter);
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();

            }
        });

        doInback();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User user=adapter.getItem(position);
                initiatePopupWindow(user.name);
                // Toast.makeText(getApplicationContext(),"position"+user.name,Toast.LENGTH_LONG).show();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listwindo.dismiss();
            }
        });


    }
    class WifiReceiver extends BroadcastReceiver
    {
        public void onReceive(Context c, Intent intent)
        {

            arrayOfUsers.clear();

            ArrayList<String> connections=new ArrayList<String>();
            ArrayList<Float> Signal_Strenth= new ArrayList<Float>();

            sb = new StringBuilder();
            List<ScanResult> wifiList;
            wifiList = mainWifi.getScanResults();
            for(int i = 0; i < wifiList.size(); i++)
            {
                connections.add(wifiList.get(i).SSID);
            }

            //  adapter.clear();
            Log.d("con-size", connections.size() + "");
            if(connections.size()==1){
                doInback();
            }
            for(int y=0;y<connections.size();y++){



                String ssid=getCurrentSsid(getApplicationContext(),mainWifi);
                String mode="Not connected";
                String compare=null;
                //   Log.d("connectedssid", ssid);
                //      Log.d("listssid",connections.get(y));
                //    Log.e("ssidiyyappa","iyyappa"+ssid);

                if(ssid!=null)  { compare=ssid.replace("\"", "");}



                if(ssid!=null) {
                    if (compare.equalsIgnoreCase(connections.get(y))) {
                        mode="connected";
                    } else {
                        mode="Not connected";
                    }
                }

                User newUser = new User("" + connections.get(y), mode);
                arrayOfUsers.add(newUser);
                //adapter.add(newUser);


            }
            for(int i=0; i<arrayOfUsers.size(); i++)
            {


                //    Log.e("size",""+arrayOfUsers.size());
                if(arrayOfUsers.get(i).getHometown().equalsIgnoreCase("Connected"))
                {
                    int j=i;
                    if(i==0)
                    {
                        break;
                    }
                    else
                    {
                        User use=arrayOfUsers.get(i);
                        arrayOfUsers.remove(i);
                        arrayOfUsers.add(0,use);
                        break;
                    }
                }

                    /*for(int j=i+1; j<arrayOfUsers.size(); j++)
                    {

                    }*/
            }

            adapter.notifyDataSetChanged();




        }
    }

    public static String getCurrentSsid(Context context,WifiManager wifiManager) {
        String ssid = null;
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            //wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();
            }
        }
        return ssid;
    }

    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response,statusres;

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  dia=new ProgressDialog(LoginActivity.this);
            dia.setMessage("Downloading...");
            dia.setCancelable(false);
            dia.show();*/}

        validateUserTask2(String statusres){
            this.statusres=statusres;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {
                    Staffloingdetails logindetails=db.staffdetails();;

                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:SyncComplete|StaffId:" + logindetails.getStaffID() + "|Portal User ID:" + logindetails.getPortalLoginID()+"|Type:all";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(LoginActivity.this);

                        res = sr.getLogin(postParameters,Url.baseurl
                                /*"http://api.schoolproject.dci.in/api/"*/);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                } else {
                    Log.e("error", "ss");
                }

            }
            catch(Exception e){
               /* if(dia.isShowing())
                    dia.cancel();*/
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
           /* if(dia.isShowing())
                dia.cancel();*/
            Toast.makeText(getApplicationContext(), result,Toast.LENGTH_LONG).show();


        }//close onPostExecute
    }// close validateUserTask
}
