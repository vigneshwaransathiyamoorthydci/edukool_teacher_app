package com.dci.edukool.teacher;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

import Utilities.Utilss;
import adapter.Violationadapter;
import helper.Violationpojo;

/**
 * Created by abimathi on 03-Nov-17.
 */
public class ViolationActivity extends Activity {
    ListView listView;
    ImageView close;
    TextView clear;
    SharedPreferences pref;
    BroadcastReceiver clearall;
   // Utils utils

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.violationlist);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        listView=(ListView)findViewById(R.id.handriselistvie);

        close= (ImageView)findViewById(R.id.close);
        clear= (TextView)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilss.logoutfunction(ViolationActivity.this, pref);
            }
        });
        Set<String> set=pref.getStringSet("getviolation",null);

        clearall=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        IntentFilter closev=new IntentFilter("Violationclose");
        registerReceiver(clearall,closev);
        if(set!=null) {
            ArrayList<Violationpojo> violation=new ArrayList<>();
            if (set.size() > 0) {
                try {
                    for(String aSiteId: set) {
                        //siteId = aSiteId;
                        Violationpojo pojo=new Violationpojo();
                        try
                        {
                            String splitofstring[]=aSiteId.split("@@");
                            pojo.setName(splitofstring[1]);
                            pojo.setOpenapps(splitofstring[3]);
                        }
                        catch (Exception e)
                        {

                        }
                        violation.add(pojo);

                    }
                    Violationadapter handraiseadapter = new Violationadapter(ViolationActivity.this, violation);
                    listView.setAdapter(handraiseadapter);
                }
                catch (Exception e)
                {

                }

				/*Handraiseadapter handraiseadapter = new Handraiseadapter(act, LoginActivity.handraise);
				listView.setAdapter(handraiseadapter);

				new Handraiseconnectin(LoginActivity.handraise, "Ack").start();
*/

            }
        }




        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //LoginActivity.handraise.clear();
                // dialog.dismiss();
            }
        });
       // dialog.show();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(clearall);
    }
}
