package connection;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import helper.Studentinfo;

public class Examzipfile extends Thread {

    int portnumber=2223;
    ServerSocket filesharesocket;
    Socket sendfile;
    File file;



 public Examzipfile(File file)
 {
     this.file=file;
 }



    @Override
    public void run() {

        try {
            filesharesocket = new ServerSocket(portnumber);
        } catch (IOException e) {
            System.out.println(e);
        }

        while(true)
        {

            try {
                sendfile=filesharesocket.accept();

                // sendfiletosocket send=
                Runnable r = new sendfiletosocket(sendfile);
                Thread t = new Thread(r);
                t.start();



            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }

    public void stopsocket()
    {
        try {
            filesharesocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    private class sendfiletosocket implements  Runnable
    {
        private Socket chsocket;

        private Studentinfo information;
        sendfiletosocket(Socket socket) {
            this.chsocket = socket;
            // information=info;
        }
        @Override
        public void run() {



            try {
                /*helper.Filesharing fileshare = new helper.Filesharing();
                fileshare.setFilename("");
                fileshare.setFiletype("pdf");
                File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "Screenshots.zip");*/


                // String string="myfilename.txt@@";

                byte[] bytes = new byte[(int) file.length()];
                BufferedInputStream bis;

                bis = new BufferedInputStream(new FileInputStream(file));

                DataInputStream dis = new DataInputStream(bis);
                dis.readFully(bytes, 0, bytes.length);
                OutputStream os = chsocket.getOutputStream();

                //Sending file name and file size to the server
                DataOutputStream dos = new DataOutputStream(os);
               /* dos.writeUTF(file.getName());

                dos.writeLong(bytes.length);*/

                dos.write(bytes, 0, bytes.length);


                dos.flush();

                //Sending file data to the server
                os.write(bytes, 0, bytes.length);
                os.flush();

                //Closing socket
                os.close();
                dos.close();

                chsocket.close();








            } catch (Exception e) {

                Log.e("TCP", "S: Error", e);

            }



        }
    }


    private boolean unpackZip(String path)
    {
        InputStream is;
        ZipInputStream zis;
        try
        {
            String filename;
            is = new FileInputStream(path);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null)
            {
                // zapis do souboru
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(path + filename);

                // cteni zipu a zapis
                while ((count = zis.read(buffer)) != -1)
                {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}