package connection;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by iyyapparajr on 4/17/2017.
 */
public class Serversocket extends Thread {
   // public static HomeScreen homeScreen;
    private final int receiverPort;
    private ServerSocket serverSocket;

    public Serversocket() {
        this.receiverPort = 2220;
      //  homeScreen = parent;
    }

    public void run() {
        try {
            setServerSocket(new ServerSocket(2220));
          //  Log.i("OneMasterT", "In ServerSocketThread run: before while, isServerRunning " + HomeScreen.isServerRunning);
            while (true) {
                try {
                    Log.i("OneMasterT", "In serversocket connection: waiting for socket request");
                    if (!getServerSocket().isClosed()) {
                        Socket connection = getServerSocket().accept();
                        Log.i("OneMasterT", "In serversocket connection: accepted the connection");
                        Log.i("OneMasterT", "In serversocket connection: connection is " + connection);
                        //RTCUtilities.executorService1.execute(new MultipleSocketServer(homeScreen, getMessageFromTheConnection(connection)));
                    }
                } catch (SocketException e) {
                    e.printStackTrace();
                    Log.d("ServerSocketThread Error", "SocketException");
                } catch (IOException e2) {
                    e2.printStackTrace();
                    //Log.d("ServerSocketThread Error", EXTHeader.DEFAULT_VALUE);
                } catch (Exception e3) {
                    e3.printStackTrace();
                    Log.d("ServerSocketThread Error", "Exception");
                }
            }
        } catch (SocketException e4) {
            e4.printStackTrace();
            Log.d("ServerSocketThread", "SocketException out");
        } catch (IOException e22) {
            Log.d("ServerSocketThread", "IOException out");
            e22.printStackTrace();
        } catch (Exception e32) {
            Log.d("ServerSocketThread", "Exception out");
            e32.printStackTrace();
        }
    }

    private String getMessageFromTheConnection(Socket connection) {
        String message = null;
        try {
            InputStreamReader isr = new InputStreamReader(new BufferedInputStream(connection.getInputStream()));
            StringBuilder process = new StringBuilder();
            synchronized (this) {
                while (true) {
                    try {
                        int character = isr.read();
                        if (!(character == 13 || character == -1)) {
                            process.append((char) character);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
           // message = process.toString();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return message;
    }

    public ServerSocket getServerSocket() {
        return this.serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }
}
