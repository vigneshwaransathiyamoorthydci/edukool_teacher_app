

package connection;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import helper.BroadcastMessageListener;
import helper.Message;
import helper.Student;
import helper.Studentinfo;
import helper.Violationmessage;

/**
 *
 * @author mohammed
 */

// the Server class
public class Violationreceive {
    // The server socket.
    private static ServerSocket violationsocket = null;
    // The client socket.
    private static Socket clientSocket = null;
    int portnumber =3031;
    Violationmessage messagelistener;
    // This chat server can accept up to maxClientsCount clients' connections.
    private static final int maxClientsCount = 10;
    public static final clientThread[] threads = new clientThread[maxClientsCount];
    public static final ArrayList<clientThread> thread=new ArrayList<>();

    public void addBroadcastMessageListener(Violationmessage paramBroadcastMessageListener)
    {
        this.messagelistener = paramBroadcastMessageListener;
    }

    public void socketclose() throws IOException {
        violationsocket.close();
        thread.clear();

    }

    public Violationreceive()

    {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    violationsocket = new ServerSocket(portnumber);
                } catch (IOException e) {
                    System.out.println(e);
                }

                while (true) {
                    try {
                        clientSocket = violationsocket.accept();
                        int i = 0;

                        Runnable r = new MyThreadHandler(clientSocket);
                        Thread t = new Thread(r);
                        t.start();

           /* break;
          }*/
                        // }
        /*if (i == maxClientsCount) {
          PrintStream os = new PrintStream(clientSocket.getOutputStream());
          os.println("Server too busy. Try later.");
          os.close();
          clientSocket.close();
        }*/
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }

            }
        }).start();

    }





    private class checksocket implements  Runnable
    {
        private Socket chsocket;

        private Studentinfo information;
        checksocket(Socket socket,Studentinfo info) {
            this.chsocket = socket;
            information=info;
        }
        @Override
        public void run() {

            if(thread.size()>0) {
                boolean check=false;
                for (int i = 0; i < thread.size(); i++) {
                    if (thread.get(i).clientSocket.getInetAddress().toString().equalsIgnoreCase(chsocket.getInetAddress().toString())) {
                        thread.remove(i);
                        clientThread runnable = new clientThread(clientSocket, thread, information);

                        thread.add(i, runnable);
                        check=true;
                        break;
                    }
                }
                if(!check)
                {
                    clientThread runnable = new clientThread(clientSocket, thread,information);

                    thread.add(runnable);

                }
            }
            else
            {
                clientThread runnable = new clientThread(clientSocket, thread, information);

                thread.add( runnable);
            }


        }
    }
    private  class MyThreadHandler implements Runnable {
        private Socket socket;

        MyThreadHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
     /* clients++;
      System.out.println(clients + " JSONClient(s) connected on port: " + socket.getPort());
*/
            try {
                // For JSON Protocol
                JSONObject jsonObject = receiveJSON();

                //sendJSON(jsonObject);

            } catch (Exception e) {
                e.printStackTrace();

            } finally {
        /*try {
         // closeSocket();
        } catch (IOException e) {
          e.printStackTrace();
        }*/
            }
        }

        public void closeSocket() throws IOException {
            // socket.close();
        }



        public JSONObject receiveJSON() throws IOException {

            try {

                String message= getMessageFromTheConnection(socket);
                String signal="Signal";
                if(message.length()>0) {


                    if(message.contains("invS"))
                    {
                        String studendetails[]=message.split("\\@");
                        Studentinfo info=new Studentinfo();
                        info.setStudentip(studendetails[2]);
                        info.setStudentrollnumber(studendetails[3]);
                        info.setStudentschooid(studendetails[4]);
                        info.setStudentname(studendetails[5]);
                        info.setStudentid(studendetails[6]);
                        Runnable r = new checksocket(socket,  info);
                        Thread t = new Thread(r);

                        t.start();

                    }

                    else if(message.toLowerCase().contains(signal.toLowerCase()))
                    {
                        messagelistener.onMessageReceived(message);

                    }
                    else {
                        messagelistener.onMessageReceived(message);

                    }
               /* if(message.contains("invS")) {


*/
                  /*  Runnable r = new checksocket(socket, new Studentinfo());
                    Thread t = new Thread(r);

                    t.start();*/
               /* }
                else
                {
                    System.out.println(message+"message json from student");
 //prateeba
                }*/
                }

           /* messagelistener.onMessageReceived("empty");


            InputStream in = socket.getInputStream();

            DataInputStream clientData = new DataInputStream(in);
            StringBuffer inputLine = new StringBuffer();

           int size=0;
            int bytesRead=0;

            byte[] b = new byte[4096];

                clientData.readFully(b, 0, b.length);
                String str = new String(b, "UTF-8");
                Log.e("byte", "bytes" + str);
                Log.e("frommessage", str);


                String getmessage[] = str.split("\\@");


                if (str.contains("Connect")) {
                    Studentinfo info = new Studentinfo();
                    info.setStudentip(getmessage[2]);
                    info.setStudentrollnumber(getmessage[3]);
                    Runnable r = new checksocket(socket, info);
                    Thread t = new Thread(r);

                    t.start();
                } else if (str.contains("Handraise")) {

                    //here write handraisesignal message
                }
                messagelistener.onMessageReceived(str);*/

                // }
                //  jsonObject = new JSONObject(str);
                // Studentinfo info=new Studentinfo();
           /* info.setStudentip(jsonObject.getString("studentip"));
            info.setStudentrollnumber(jsonObject.getString("studentrollno"));*/

            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }


        private String getMessageFromTheConnection(Socket connection) {
            String message = null;
            try {
                InputStreamReader isr = new InputStreamReader(new BufferedInputStream(connection.getInputStream()));
                StringBuilder process = new StringBuilder();
                synchronized (this) {
                    while (true) {
                        try {
                            Log.e("reading","reading");
                            int character = isr.read();
                            Log.e("character",""+character);
                            if (!(character == 13 || character == -1)) {
                                process.append((char) character);
                            }
                            else
                            {
                                message = process.toString();
                                return  message;
                                //break;
                            }
                            message=process.toString();
                            Log.e("reading","reading"+message);
                        } catch (Exception e) {
                            Log.e("catch","reading"+process.toString());

                            e.printStackTrace();
                        }
                    }
                    //Log.e("ending","end");

                /*  Log.e("messaging","msg"+message);
                  messagelistener.onMessageReceived(message);*/
                }


            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return message;
        }


    /*public void sendJSON(JSONObject jsonObject) throws IOException {
      JSONObject jsonObject2 = new JSONObject();
      jsonObject2.put("key", new Paper(250,369));

      OutputStream out = socket.getOutputStream();
      ObjectOutputStream o = new ObjectOutputStream(out);
      o.writeObject(jsonObject2);
      out.flush();
      System.out.println("Sent to server: " + " " + jsonObject2.get("key").toString());
    }*/
    }
    public void closesocket()
    {
        try {
            if (violationsocket != null) {
                violationsocket.close();
            }
        }
        catch (Exception e)
        {

        }
    }


}
