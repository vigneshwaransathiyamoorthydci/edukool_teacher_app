package exam;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.dci.edukool.teacher.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import connection.MultiThreadChatServerSync;
import connection.clientThread;

/**
 * Created by pratheeba on 5/4/2017.
 */
public class TeacherViewExams extends Activity {
    ImageView imageView2,delete,pie_chart,imageView3;
    RelativeLayout startlay;
    TextView push;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_exam_layout);
        startlay = (RelativeLayout)findViewById(R.id.startlay);
        delete = (ImageView)findViewById(R.id.delete);
        imageView2 = (ImageView)findViewById(R.id.imageView2);
        push = (TextView)findViewById(R.id.push);
        imageView3 = (ImageView)findViewById(R.id.imageView3);



        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        startlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fileContent = new String();
                //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
                File sdcard = Environment.getExternalStorageDirectory();

//Get the text file
                File file = new File(sdcard,"question_json.txt");
                JSONObject quesObj;



//Read text from file
                StringBuilder text = new StringBuilder();

                try {
                    BufferedReader br = new BufferedReader(new FileReader(file));
                    String line;

                    while ((line = br.readLine()) != null) {
                        text.append(line);
                        text.append('\n');
                    }
                    fileContent = text.toString();

                    br.close();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
                System.out.println("fileContent.length()" + fileContent.length());
                String quetion=fileContent;

                String senddata="Examquestion"+"@"+quetion;
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();

             //   Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });

        push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();

            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }
}
