package exam;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Utilities.DatabaseHandler;
import Utilities.Utilss;


/**
 * Created by pratheeba on 4/26/2017.
 */
public class ExamPreviewActivity extends Activity {
    ExamPreviewAdapter selfevalutation_adapter;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    static JSONArray quesList;
    String ExamIsVal;
    TextView exanname;
    int ExamID;
    String StaffID;
    SharedPreferences pref;
     Utilss utils;

    ImageView close;
    ListView listView,self_asses_ques_ans_list;
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exam_preview_layout);
        Bundle bundle = getIntent().getExtras();
        ExamID = bundle.getInt("ExamID");
        utils=new Utilss(this);

        self_asses_ques_ans_list = (ListView) findViewById(R.id.listView);
        exanname= (TextView) findViewById(R.id.exanname);
        close= (ImageView) findViewById(R.id.close);
        utils.setTextviewtypeface(3,exanname);
        try {
            loadQuestions();
        }
        catch (Exception e){
            e.printStackTrace();
        }


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void loadQuestions() throws Exception {
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
//Get the text file
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            pref=getSharedPreferences("Teacher", MODE_PRIVATE);
            StaffID =pref.getString("staffid", "");
                DatabaseHandler db = new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
              String descriptionval  = db.getAllExamsDetailsusingExamId(ExamID,StaffID);

            exanname.setText(Html.fromHtml(descriptionval));
                List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(ExamID);

                for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);
                    plan_list_array.add(new QuestionPojo(cn.getQuestion(), cn.getOptionA(), cn.getOptionB(), cn.getOptionC(), cn.getOptionD(), String.valueOf(cn.getCorrectAnswer()), "1", "StudentName", 001, 1, "TotalNoQuestion", "StudentPhotPath",0,0,0));

                }
                    selfevalutation_adapter = new ExamPreviewAdapter(ExamPreviewActivity.this, R.layout.exam_preview_adapter,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);


        }
        catch (Exception e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }

    @Override
    public void onBackPressed() {

    }

    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            curBrightnessValue = Settings.System.getInt(
                    getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }
}
