package exam;

/**
 * Created by pratheeba on 4/28/2017.
 */
public class ExamPojo {


    String exam_name;

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public String getExam_id() {
        return exam_id;
    }

    public void setExam_id(String exam_id) {
        this.exam_id = exam_id;
    }

    String exam_id;


    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    int student_id;

    public ExamPojo(String exam_name, String exam_id,int student_id) {
        this.exam_name=exam_name;
        this.exam_id=exam_id;
        this. student_id =student_id;
    }
}
