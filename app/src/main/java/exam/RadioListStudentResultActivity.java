package exam;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import Utilities.DatabaseHandler;


/**
 * Created by pratheeba on 4/28/2017.
 */
public class RadioListStudentResultActivity extends Activity{
    ArrayList<ExamPojo> examPojo;
    ExamListAdapter adapter;
    private RelativeLayout linearLayout1;
    private RelativeLayout linearLayout2;
    BroadcastReceiver questionreceiver;
    ListView listView,self_asses_ques_ans_list;
    TextView exam_name,check,totalmark;
    private LinearLayout mainLinearLayout;
    ArrayList<QuestionPojo> plan_list_array = new ArrayList<QuestionPojo>();
    SelfTestQuestionAdapter selfevalutation_adapter;
    static JSONArray quesList;
    String Question;
    String OptionA;
    String OptionB;
    String OptionC;
    String OptionD;
    int mark;
    int QuestionID;
    String  StudentAnswer;
    int IsCorrect;
    int MarkForAnswer;
    int  ObtainedScore;

    String  CorrectAnswer;
    int  TotalScore;
    int ExamResponseID;
    int ExamIDVal;
    int studentID;
    String StudName;
    int TimeTaken;
    String DateAttended;
    int  StudentID;
    int  RollNo;
    TextView studentname;
    ImageView profileimage,back;
    SharedPreferences.Editor edit;
    SharedPreferences pref;
    String exam ;
    int  TotalScoremark;
    String subject ;
    String studentnameVal;
    int totalmarkVal;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_exam_list_layout);
        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        back = (ImageView) findViewById(R.id.back);
        studentname= (TextView) findViewById(R.id.studentname);
        profileimage= (ImageView) findViewById(R.id.profileimage);
        linearLayout1 = (RelativeLayout) View.inflate(this,
                R.layout.student_self_asses_list, null);
        linearLayout2 = (RelativeLayout) View.inflate(this,
                R.layout.self_asses_ques_ans_list, null);
        examPojo= new ArrayList<>();
        pref=getSharedPreferences("Teacher", MODE_PRIVATE);
        edit=pref.edit();
        String  StaffID =pref.getString("staffid", "");
        DatabaseHandler db = new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
        studentname.setText(pref.getString("classname",""));
        setbackground(profileimage, pref.getString("image", ""));

        Bundle bundle = getIntent().getExtras();
        plan_list_array.clear();
         ExamIDVal = bundle.getInt("ExamID");
        List<ExamResponsePojo> examrespojo = db.addStudentExamResponseUsingExamId(ExamIDVal);

        for (ExamResponsePojo cn : examrespojo) {
            String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getExamID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo()  +  " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
            // Writing Contacts to log
            Log.d("Exam2: ", log);
            int ExamResponseID=  cn.getExamResponseID();
            int  ExamID=  cn.getExamID();
            String StudName=  cn.getStudName();
            int TimeTaken=  cn.getTimeTaken();
            String DateAttended=  cn.getDateAttended();
              //TotalScoremark=  cn.getTotalScore();
            int  StudentID=  cn.getStudentID();
            int  RollNo=  cn.getRollNo();
            studentnameVal = cn.getDateAttended();
            totalmarkVal = cn.getTotalScore();

            examPojo.add(new ExamPojo("Name"+":"+cn.getStudName()+":"+cn.getDateAttended()+":"+cn.getTotalScore(),"StudName"+"|"+cn.getDateAttended()+"|"+cn.getTotalScore(),cn.getStudentID()));

        }

        self_asses_ques_ans_list = (ListView) linearLayout2.findViewById(R.id.listView);

        List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(ExamIDVal,StaffID);
        for (ExamDetails cn : examdetailsList) {
             exam = cn.getExamCategoryName();
             subject = cn.getSubject();
            //TotalScoremark=  cn.getTotalScore();
        }


        //examPojo.add(new ExamPojo("","DEMO1"));
        listView = (ListView) linearLayout1.findViewById(R.id.listView);

        exam_name = (TextView) linearLayout2.findViewById(R.id.exam_name);
        check = (TextView) linearLayout2.findViewById(R.id.check);
        totalmark = (TextView) linearLayout2.findViewById(R.id.totalmark);
        studentname= (TextView) linearLayout2.findViewById(R.id.studentname);
        adapter= new ExamListAdapter(examPojo,getApplicationContext());
        listView.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });



        mainLinearLayout.addView(linearLayout1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                   // Toast.makeText(getApplicationContext(), "onclick", Toast.LENGTH_LONG).show();
                   // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }
    private void loadQuestions() throws Exception {
        try {
                DatabaseHandler db = new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);


               /* for (QuestionDetails cn : questiondetailsList) {
                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                    // Writing Contacts to log
                    Log.d("Question3: ", log);
                     Question =cn.getQuestion();
                     OptionA =cn.getOptionA();
                     OptionB =cn.getOptionB();
                     OptionC =cn.getOptionC();
                     OptionD =cn.getOptionD();
                      CorrectAnswer = cn.getCorrectAnswer();
                }*/

            exam_name.setText(exam);
            check.setText(subject);


            List<ExamResponsePojo> examrespojo = db.addStudentExamResponseUsingExamId(ExamIDVal,studentID);

                for (ExamResponsePojo cn : examrespojo) {
                    String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getStudentID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo()  +  " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    Log.d("Exam2: ", log);
                     ExamResponseID=  cn.getExamResponseID();
                    int  ExamID=  cn.getExamID();
                    String StudName=  cn.getStudName();
                    int TimeTaken=  cn.getTimeTaken();
                    String DateAttended=  cn.getDateAttended();
                      TotalScore=  cn.getTotalScore();
                    int  StudentID=  cn.getStudentID();
                    int  RollNo=  cn.getRollNo();
                    studentname.setText(DateAttended + "");


                    // examPojo.add(new ExamPojo("StudName" + "|" + cn.getDateAttended() + "|" + cn.getTotalScore(), "StudName" + "|" + cn.getDateAttended() + "|" + cn.getTotalScore()));

                }
                List<StudentQuestionResultPojo> resultspojo = db.tblStudentQuestionResultusingresponseID(ExamResponseID);
            List<StudentQuestionResultPojo> resul = db.tblStudentQuestionResultusingresponseIDall();

/*
            for (StudentQuestionResultPojo cn : resul) {

                String log = "QuestionID: " + cn.getQuestionID() + " ,StudentAnswer: " + cn.getStudentAnswer()
                        +"StudentAnswer"+ cn.getStudentAnswer()
                        +"IsCorrect"+ cn.getIsCorrect()
                        +"MarkForAnswer"+ cn.getMarkForAnswer()
                        +"ObtainedScore"+ cn.getObtainedScore()
                        +"responseid"+ cn.getResponseID();

                QuestionID=  cn.getQuestionID();
                StudentAnswer=  cn.getStudentAnswer();
                IsCorrect=  cn.getIsCorrect();
                MarkForAnswer=  cn.getMarkForAnswer();
                ObtainedScore=  cn.getObtainedScore();

                Log.d("studentanswerfrom: ", log);

            }*/

                for (StudentQuestionResultPojo cn : resultspojo) {

                    String log = "QuestionID: " + cn.getQuestionID() + " ,StudentAnswer: " + cn.getStudentAnswer();

                    QuestionID=  cn.getQuestionID();
                      StudentAnswer=  cn.getStudentAnswer();
                     IsCorrect=  cn.getIsCorrect();
                     MarkForAnswer=  cn.getMarkForAnswer();
                    TotalScoremark =TotalScoremark+MarkForAnswer;
                      ObtainedScore=  cn.getObtainedScore();
                    totalmark.setText("Mark: "+TotalScore+"/"+TotalScoremark);

                    Log.d("studentanswerfrom: ", log);
                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamIdquestionidf(ExamIDVal, QuestionID);
                    for (QuestionDetails cn1 : questiondetailsList) {
                        // Writing Contacts to log
                        Log.d("Question3: ", log);
                        Question = cn1.getQuestion();
                        OptionA = cn1.getOptionA();
                        OptionB = cn1.getOptionB();
                        OptionC = cn1.getOptionC();
                        OptionD = cn1.getOptionD();
                        CorrectAnswer = cn1.getCorrectAnswer();

                        try {
                            plan_list_array.add(new QuestionPojo(Question, OptionA, OptionB, OptionC, OptionD, CorrectAnswer, StudentAnswer, StudName, RollNo, StudentID, "TotalNoQuestion", "StudentPhotPath",IsCorrect,MarkForAnswer,ObtainedScore));
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }




                selfevalutation_adapter = new SelfTestQuestionAdapter(RadioListStudentResultActivity.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
                mainLinearLayout.addView(linearLayout2);


        }
        catch (Exception e) {
            //You'll need to add proper error handling here
        }




//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }


  /*  public void loadQuestions() throws Exception {
        String fileContent = new String();
        //Find the directory for the SD Card using the API
/*//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();
//Get the text file
        File file = new File(sdcard,"question.txt");
        JSONObject quesObj;
//Read text from file
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            fileContent = text.toString();
            br.close();
            System.out.println("fileContent.length()" + fileContent.length());
            if (fileContent.length() > 0) {
                quesObj = new JSONObject(fileContent);
                quesList = shuffleJsonArray(quesObj.getJSONArray("questions"));
                int totalquestions = quesList.length();
                System.out.println("totalquestions" + totalquestions);
                JSONObject resultObject = null;
                JSONArray jsonArray = null;
                QuizWrapper newItemObject = null;
                try {
                    resultObject = new JSONObject(fileContent);
                    System.out.println("Testing the water " + resultObject.toString());
                    jsonArray = resultObject.optJSONArray("questions");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonChildNode = null;
                    try {
                        jsonChildNode = jsonArray.getJSONObject(i);
                        int id = 1;
                        String question = jsonChildNode.getString("question");
                        JSONArray answerOptions = jsonChildNode.getJSONArray("answers");
                        int correctAnswer = jsonChildNode.getInt("correctIndex");
                        try {
                            plan_list_array.add(new QuestionPojo(question, answerOptions, answerOptions, answerOptions, answerOptions, String.valueOf(correctAnswer), "1", "StudentName", "StudentRollNumber", "StudentID", "TotalNoQuestion", "StudentPhotPath"));
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                selfevalutation_adapter = new SelfTestQuestionAdapter(StudentExamListActivity.this, R.layout.self_asses_quesandans,
                        plan_list_array);
                self_asses_ques_ans_list.setAdapter(selfevalutation_adapter);
                mainLinearLayout.addView(linearLayout2);

            } else {
                quesList = new JSONArray();
                // examDetailList = new JSONArray();
            }
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
        }

//Find the view by its id
        // TextView tv = (TextView)findViewById(R.id.textView);

//Set the text
        // tv.setText(text);

    }*/
    public static JSONArray shuffleJsonArray(JSONArray array) throws JSONException {
        Random rnd = new Random();
        for (int i = array.length() - 1; i >= 0; i--) {
            int j = rnd.nextInt(i + 1);
            Object object = array.get(j);
            array.put(j, array.get(i));
            array.put(i, object);
        }
        return array;
    }



    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    Bundle bundle = intent.getExtras();
                    studentID = bundle.getInt("studentID");
                    plan_list_array.clear();
                    mark = 0;
                    TotalScoremark =0;
                    loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("Exam");

        registerReceiver(questionreceiver,intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
    }
}
