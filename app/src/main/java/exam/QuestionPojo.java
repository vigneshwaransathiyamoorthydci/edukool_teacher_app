package exam;

/**
 * Created by pratheeba on 4/21/2017.
 */

public class QuestionPojo
{

    public String getAnswer1() {
        return Answer1;
    }

    public void setAnswer1(String answer1) {
        Answer1 = answer1;
    }

    public String getAnswer2() {
        return Answer2;
    }

    public void setAnswer2(String answer2) {
        Answer2 = answer2;
    }

    public String getAnswer3() {
        return Answer3;
    }

    public void setAnswer3(String answer3) {
        Answer3 = answer3;
    }

    public String getAnswer4() {
        return Answer4;
    }

    public void setAnswer4(String answer4) {
        Answer4 = answer4;
    }

    public String Answer1;
    public String Answer2;
    public String Answer3;
    public String Answer4;
    public String CorrectAnswer;
    public String Question;
    public String SelectedAnswer;
    int IsCorrect;
    int MarkForAnswer;

    public int getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        IsCorrect = isCorrect;
    }

    public int getMarkForAnswer() {
        return MarkForAnswer;
    }

    public void setMarkForAnswer(int markForAnswer) {
        MarkForAnswer = markForAnswer;
    }

    public int getObtainedScore() {
        return ObtainedScore;
    }

    public void setObtainedScore(int obtainedScore) {
        ObtainedScore = obtainedScore;
    }

    int  ObtainedScore;

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int StudentID;
    public String StudentName;


    public String getCorrectAnswer() {
        return CorrectAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        CorrectAnswer = correctAnswer;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getSelectedAnswer() {
        return SelectedAnswer;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        SelectedAnswer = selectedAnswer;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public String getStudentPhotPath() {
        return StudentPhotPath;
    }

    public void setStudentPhotPath(String studentPhotPath) {
        StudentPhotPath = studentPhotPath;
    }



    public String getTotalNoQuestion() {
        return TotalNoQuestion;
    }

    public void setTotalNoQuestion(String totalNoQuestion) {
        TotalNoQuestion = totalNoQuestion;
    }

    public String StudentPhotPath;
    public int StudentRollNumber;

    public int getStudentRollNumber() {
        return StudentRollNumber;
    }

    public void setStudentRollNumber(int studentRollNumber) {
        StudentRollNumber = studentRollNumber;
    }

    public String TotalNoQuestion;

    public QuestionPojo()
    {
    }

    public QuestionPojo(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, int paramString9, int paramString10, String paramString11, String paramString12,int paramString13,int paramString14,int paramString15)
    {
        this.Question = paramString1;
        this.Answer1 = paramString2;
        this.Answer2 = paramString3;
        this.Answer3 = paramString4;
        this.Answer4 = paramString5;
        this.CorrectAnswer = paramString6;
        this.SelectedAnswer = paramString7;
        this.StudentName = paramString8;
        this.StudentRollNumber = paramString9;
        this.StudentID = paramString10;
        this.TotalNoQuestion = paramString11;
        this.StudentPhotPath = paramString12;
        this.IsCorrect = paramString13;
        this.MarkForAnswer = paramString14;
        this.ObtainedScore = paramString15;
    }
}


