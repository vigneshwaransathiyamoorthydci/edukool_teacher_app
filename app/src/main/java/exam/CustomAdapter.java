package exam;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.MainActivity;
import com.dci.edukool.teacher.R;

import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import books.Attendancewithcheckparentlayout;
import connection.Examzipfile;
import connection.MultiThreadChatServerSync;
import connection.clientThread;


/**
 * Created by pratheeba on 4/25/2017.
 */
public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener {

    Context mContext;
    String studentResponse;
    int ExamCategoryID;
    int ExamID;
    String ExamCategory;
    String ExamCode;
    String ExamDescription;
    int BatchID;
    int ExamDuration;
    int SubjectID;
    String Subject;
    int ExamTypeID;
    String ExamType;
    String ExamDate;
    String MY_PREFS_NAME = "EXAMDETAILS";
    DatabaseHandler db;
    SharedPreferences.Editor edit;
    SharedPreferences pref;
    ArrayList<String> batcharray = new ArrayList<>();
    Activity act;
    Utilss utils;
    int BUFFER = 2048;
    private ArrayList<DataModel> dataSet;
    private int lastPosition = -1;

    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.view_exam_layout, data);
        this.dataSet = data;
        this.mContext = context;
        act = (Activity) context;
        utils = new Utilss((Activity) context);

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        DataModel dataModel = (DataModel) object;

        Toast.makeText(mContext, "onclick", Toast.LENGTH_LONG).show();

       /* switch (v.getId())
        {
          *//*  case R.id.item_info:
                Snackbar.make(v, "Release date " + dataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;*//*
        }*/
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        pref = mContext.getSharedPreferences("Teacher", mContext.MODE_PRIVATE);
        db = new DatabaseHandler(mContext, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.view_exam_layout, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.download_exams_txt);
            viewHolder.parlay = (RelativeLayout) convertView.findViewById(R.id.startlay);
            viewHolder.push = (TextView) convertView.findViewById(R.id.push);
            viewHolder.delete = (ImageView) convertView.findViewById(R.id.delete);
            viewHolder.preview = (TextView) convertView.findViewById(R.id.preview);
            viewHolder.start = (TextView) convertView.findViewById(R.id.start);
            utils.setTextviewtypeface(5, viewHolder.txtName);
            utils.setTextviewtypeface(3, viewHolder.preview);
            utils.setTextviewtypeface(3, viewHolder.start);


            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }


        if (position % 2 == 1) {
            // convertView.setBackgroundResource(R.color.appbg);
            viewHolder.delete.setBackgroundResource(R.drawable.delete);
            // viewHolder.parlay.setBackgroundResource(R.color.headercolor);

        } else {
            // convertView.setBackgroundResource(R.color.headercolor);
            viewHolder.delete.setBackgroundResource(R.drawable.delete_br_color);
            // viewHolder.parlay.setBackgroundResource(R.color.appbg);

        }

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent("ExamDELETEPop");
                Bundle mBundle = new Bundle();
                mBundle.putInt("ExamIDValue", dataModel.getExamId());
                mBundle.putInt("ExamIDposition", position);

                in.putExtras(mBundle);
                mContext.sendBroadcast(in);
            }
        });
        viewHolder.push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "Under Construction", Toast.LENGTH_LONG).show();

            }
        });
        viewHolder.preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int ExamId = dataModel.getExamId();
                Intent intent = new Intent(mContext, ExamPreviewActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putInt("ExamID", ExamId);
                intent.putExtras(mBundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

            }
        });
        viewHolder.parlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stratExamCompletedPopup(dataModel);
              /*  Toast.makeText(mContext, "Sending questions to all students", Toast.LENGTH_LONG).show();

                {
                    pref=mContext.getSharedPreferences("Teacher", mContext.MODE_PRIVATE);
                    edit=pref.edit();
                    String  StaffID =pref.getString("staffid", "");
                    String Evaluation = pref.getString("Evaluation", "0");
                    File username=new File(Evaluation+"/"+dataModel.getExamId());

                    DatabaseHandler db = new DatabaseHandler(mContext,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(dataModel.getExamId());
                    List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(dataModel.getExamId(),StaffID);


                    for (ExamDetails cn : examdetailsList) {
                        String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                        // Writing Contacts to log
                        Log.d("Exam2: ", log);
                        ExamID=  cn.getExamID();
                         ExamCategoryID =  cn.getExamCategoryID();
                         ExamCategory = cn.getExamCategoryName();
                         ExamCode = cn.getExamCode();
                         ExamDescription =  cn.getExamDescription();
                         BatchID = cn.getBatchID();
                         ExamDuration = cn.getExamDuration();
                         SubjectID = cn.getSubjectID();
                         Subject =  cn.getSubject();
                         ExamTypeID =  cn.getExamTypeID();
                         ExamType = cn.getExamType();
                         ExamDate =cn.getExamDate();

                    }

                        JSONObject parent = new JSONObject();
                    JSONObject parent1 = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    JSONArray jsonArray1 = new JSONArray();
                    JSONObject list2 = new JSONObject();
                    try {
                        list2.put("ExamID",ExamID).toString();
                        list2.put("ExamCategoryID", ExamCategoryID).toString();
                        list2.put("ExamCategory", ExamCategory).toString();
                        list2.put("ExamCode", ExamCode).toString();
                        list2.put("ExamDescription", ExamDescription).toString();
                        list2.put("BatchID", BatchID).toString();
                        list2.put("ExamDuration", ExamDuration).toString();
                        list2.put("SubjectID", SubjectID).toString();
                        list2.put("Subject", Subject).toString();
                        list2.put("ExamTypeID", ExamTypeID).toString();
                        list2.put("ExamType", ExamType).toString();
                        list2.put("ExamDate", ExamDate).toString();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    for (QuestionDetails cn : questiondetailsList) {
                        String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                        // Writing Contacts to log
                        Log.d("Question3: ", log);
                        JSONArray optionArray = new JSONArray();
                        String optionA = cn.getOptionA().toString();
                        String optionb = cn.getOptionB().toString();
                        String optionC = cn.getOptionC().toString();
                        String optionD = cn.getOptionD().toString();
                        optionA = optionA.replaceAll("'", "\\\\'");
                        optionA = optionA.replaceAll("\"", "\\\\\"");
                        optionb = optionb.replaceAll("'", "\\\\'");
                        optionb = optionb.replaceAll("\"", "\\\\\"");

                        optionC = optionC.replaceAll("'", "\\\\'");
                        optionC = optionC.replaceAll("\"", "\\\\\"");

                        optionD = optionD.replaceAll("'", "\\\\'");
                        optionD = optionD.replaceAll("\"", "\\\\\"");


*//*
                        optionArray.put(optionA);
                        optionArray.put(optionb);
                        optionArray.put(optionC);
                        optionArray.put(optionD);
*//*



                        if(cn.getOptionA().contains("img")){
                            String text = cn.getOptionA().replaceAll(username.getAbsolutePath()+"/", "");

                            optionArray.put(text);


                        }
                        else{
                            optionArray.put(cn.getOptionA());
                        }
                        if(cn.getOptionB().contains("img")){
                            String text = cn.getOptionB().replaceAll(username.getAbsolutePath()+"/", "");

                            optionArray.put(text);


                        }
                        else{
                            optionArray.put(cn.getOptionB());
                        }
                        if(cn.getOptionC().contains("img")){
                            String text = cn.getOptionC().replaceAll(username.getAbsolutePath()+"/", "");

                            optionArray.put(text);


                        }
                        else{
                            optionArray.put(cn.getOptionC());
                        }
                        if(cn.getOptionD().contains("img")){
                            String text = cn.getOptionD().replaceAll(username.getAbsolutePath()+"/", "");

                            optionArray.put(text);
                        }
                        else{
                            optionArray.put(cn.getOptionD());
                        }

                        for(int z = 0; z < optionArray.length(); z++){
                            String[] s = new String[optionArray.length()];

                            try {
                                testX(optionArray.get(z).toString(), username.getAbsolutePath());
                            }
                            catch (Exception e){
                            }

                        }

                        zip(batcharray,username.getAbsolutePath()+ ".zip");

                      *//*  optionArray.put(cn.getOptionA());
                        optionArray.put(cn.getOptionB());
                        optionArray.put(cn.getOptionC());
                        optionArray.put(cn.getOptionD());*//*



                        try {


                            JSONObject list1 = new JSONObject();
                            if(cn.getQuestion().toString().contains("img")){
                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath()+"/", "");

                                list1.put("Questions",text);


                            }
                            else{
                                list1.put("Questions",cn.getQuestion().toString()).toString();
                            }
                            list1.put("QuestionID",cn.getQuestionID()).toString();
                            list1.put("TopicID", cn.getTopicID()).toString();
                            list1.put("Topic", cn.getTopicName().toString());
                            list1.put("AspectID", cn.getAspectID()).toString();
                            list1.put("Aspect",cn.getAspect()).toString();
                            list1.put("QuestionNumber",cn.getQuestionNumber()).toString();
                            if(cn.getQuestion().toString().contains("img")){

                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath()+"/", "");
                                testX(cn.getQuestion().toString(), username.getAbsolutePath());
                                list1.put("question",text);
                                zip(batcharray, username.getAbsolutePath()+ ".zip");

                            }
                            else{
                                list1.put("question", cn.getQuestion()).toString();
                            }
                            list1.put("options", optionArray);
                            list1.put("CorrectAnswer",cn.getCorrectAnswer()).toString();
                            list1.put("Mark",cn.getMark()).toString();
                            list1.put("NegativeMark",cn.getNegative_Mark()).toString();
                            jsonArray.put(list1);

                            // new connectTask().execute();
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }


                    }
                    try {
                        list2.put("Questions", jsonArray).toString();
                        parent1.put("Exam", jsonArray1).toString();

                        jsonArray1.put(list2);

                        parent.put("status", "Success").toString();
                        parent.put("StatusCode", "200").toString();
                        parent.put("ExamDetails", parent1).toString();

                        if(batcharray.size()==0){
                            parent.put("Examsharing","NO");
                            String senddata1=  "Examquestion" + "@" + parent.toString();

                            new clientThread(MultiThreadChatServerSync.thread,senddata1).start();

                        }
                        else{
                            File file=new File(username.getAbsolutePath()+".zip");
                            parent.put("Examsharing","Yes");
                            parent.put("Filename",file.getName());
                            parent.put("Filelen",""+file.length());
                           // String senddata1="Examsharing"+"@"+file.getName()+"@"+file.length();

                            String senddata1=  "Examquestion" + "@" + parent.toString();
                           new clientThread(MultiThreadChatServerSync.thread,senddata1).start();

                            if ( MainActivity.examzipfile == null) {
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();


                            } else {
                                MainActivity.examzipfile.stopsocket();
                                MainActivity.examzipfile = null;
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();
                            }

                        }

                        studentResponse = parent.toString();
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    Log.d("output", parent.toString());

                    String senddata = "Examquestion" + "@" + parent.toString();
                 //   String jsonFormattedString = senddata.replaceAll("\\\\", "");

                    try {
                        StringEntity se = new StringEntity(senddata.toString(), "UTF-8");
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                    SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE).edit();
                    editor.putInt("studentcount", 0);
                    editor.commit();
                    Intent in=new Intent("FinishStart");
                    Bundle mBundle = new Bundle();
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);*/

                //  new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                //Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
                //}
            }
        });

        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        // result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(Html.fromHtml(dataModel.getName()));

        // Return the completed view to render on screen
        return convertView;
    }

    public void stratExamCompletedPopup(final DataModel dataModel) {
        final Dialog dialog = new Dialog(act);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        TextView notsaved = (TextView) dialog.findViewById(R.id.notsaved);
        notsaved.setText("SHARE THIS EXAM TO ?");
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        title.setText("EXAM SHARING");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Sending questions to all students", Toast.LENGTH_LONG).show();

                {
                    pref = mContext.getSharedPreferences("Teacher", mContext.MODE_PRIVATE);
                    edit = pref.edit();
                    String StaffID = pref.getString("staffid", "");
                    String Evaluation = pref.getString("Evaluation", "0");
                    File username = new File(Evaluation + "/" + dataModel.getExamId());

                    DatabaseHandler db = new DatabaseHandler(mContext, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(dataModel.getExamId());
                    List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(dataModel.getExamId(), StaffID);


                    for (ExamDetails cn : examdetailsList) {
                        String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID() + " ,BatchID: " + cn.getBatchID() + " ,IsResultPublished: " + cn.getIsResultPublished() + " ,ExamShelfID: " + cn.getExamShelfID() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                        // Writing Contacts to log
                        Log.d("Exam2: ", log);
                        ExamID = cn.getExamID();
                        ExamCategoryID = cn.getExamCategoryID();
                        ExamCategory = cn.getExamCategoryName();
                        ExamCode = cn.getExamCode();
                        ExamDescription = cn.getExamDescription();
                        BatchID = cn.getBatchID();
                        ExamDuration = cn.getExamDuration();
                        SubjectID = cn.getSubjectID();
                        Subject = cn.getSubject();
                        ExamTypeID = cn.getExamTypeID();
                        ExamType = cn.getExamType();
                        ExamDate = cn.getExamDate();

                    }

                    JSONObject parent = new JSONObject();
                    JSONObject parent1 = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    JSONArray jsonArray1 = new JSONArray();
                    JSONObject list2 = new JSONObject();
                    try {
                        list2.put("ExamID", ExamID).toString();
                        list2.put("ExamCategoryID", ExamCategoryID).toString();
                        list2.put("ExamCategory", ExamCategory).toString();
                        list2.put("ExamCode", ExamCode).toString();
                        list2.put("ExamDescription", ExamDescription).toString();
                        list2.put("BatchID", BatchID).toString();
                        list2.put("ExamDuration", ExamDuration).toString();
                        list2.put("SubjectID", SubjectID).toString();
                        list2.put("Subject", Subject).toString();
                        list2.put("ExamTypeID", ExamTypeID).toString();
                        list2.put("ExamType", ExamType).toString();
                        list2.put("ExamDate", ExamDate).toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    for (QuestionDetails cn : questiondetailsList) {
                        String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                        // Writing Contacts to log
                        Log.d("Question3: ", log);
                        JSONArray optionArray = new JSONArray();
                        String optionA = cn.getOptionA().toString();
                        String optionb = cn.getOptionB().toString();
                        String optionC = cn.getOptionC().toString();
                        String optionD = cn.getOptionD().toString();
                        optionA = optionA.replaceAll("'", "\\\\'");
                        optionA = optionA.replaceAll("\"", "\\\\\"");
                        optionb = optionb.replaceAll("'", "\\\\'");
                        optionb = optionb.replaceAll("\"", "\\\\\"");

                        optionC = optionC.replaceAll("'", "\\\\'");
                        optionC = optionC.replaceAll("\"", "\\\\\"");

                        optionD = optionD.replaceAll("'", "\\\\'");
                        optionD = optionD.replaceAll("\"", "\\\\\"");


/*
                        optionArray.put(optionA);
                        optionArray.put(optionb);
                        optionArray.put(optionC);
                        optionArray.put(optionD);
*/


                        if (cn.getOptionA().contains("img")) {
                            String text = cn.getOptionA().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionA());
                        }
                        if (cn.getOptionB().contains("img")) {
                            String text = cn.getOptionB().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionB());
                        }
                        if (cn.getOptionC().contains("img")) {
                            String text = cn.getOptionC().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionC());
                        }
                        if (cn.getOptionD().contains("img")) {
                            String text = cn.getOptionD().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);
                        } else {
                            optionArray.put(cn.getOptionD());
                        }

                        for (int z = 0; z < optionArray.length(); z++) {
                            String[] s = new String[optionArray.length()];

                            try {
                                testX(optionArray.get(z).toString(), username.getAbsolutePath());
                            } catch (Exception e) {
                            }

                        }

                        zip(batcharray, username.getAbsolutePath() + ".zip");

                      /*  optionArray.put(cn.getOptionA());
                        optionArray.put(cn.getOptionB());
                        optionArray.put(cn.getOptionC());
                        optionArray.put(cn.getOptionD());*/


                        try {


                            JSONObject list1 = new JSONObject();
                            if (cn.getQuestion().toString().contains("img")) {
                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath() + "/", "");

                                list1.put("Questions", text);


                            } else {
                                list1.put("Questions", cn.getQuestion().toString()).toString();
                            }
                            list1.put("QuestionID", cn.getQuestionID()).toString();
                            list1.put("TopicID", cn.getTopicID()).toString();
                            list1.put("Topic", cn.getTopicName().toString());
                            list1.put("AspectID", cn.getAspectID()).toString();
                            list1.put("Aspect", cn.getAspect()).toString();
                            list1.put("QuestionNumber", cn.getQuestionNumber()).toString();
                            if (cn.getQuestion().toString().contains("img")) {

                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath() + "/", "");
                                testX(cn.getQuestion().toString(), username.getAbsolutePath());
                                list1.put("question", text);
                                zip(batcharray, username.getAbsolutePath() + ".zip");

                            } else {
                                list1.put("question", cn.getQuestion()).toString();
                            }
                            list1.put("options", optionArray);
                            list1.put("CorrectAnswer", cn.getCorrectAnswer()).toString();
                            list1.put("Mark", cn.getMark()).toString();
                            list1.put("NegativeMark", cn.getNegative_Mark()).toString();
                            jsonArray.put(list1);

                            // new connectTask().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                    try {
                        list2.put("Questions", jsonArray).toString();
                        parent1.put("Exam", jsonArray1).toString();

                        jsonArray1.put(list2);

                        parent.put("status", "Success").toString();
                        parent.put("StatusCode", "200").toString();
                        parent.put("ExamDetails", parent1).toString();

                        if (batcharray.size() == 0) {
                            parent.put("Examsharing", "NO");
                            String senddata1 = "Examquestion" + "@" + parent.toString();

                            new clientThread(MultiThreadChatServerSync.thread, senddata1).start();

                        } else {
                            File file = new File(username.getAbsolutePath() + ".zip");
                            parent.put("Examsharing", "Yes");
                            parent.put("Filename", file.getName());
                            parent.put("Filelen", "" + file.length());
                            // String senddata1="Examsharing"+"@"+file.getName()+"@"+file.length();

                            String senddata1 = "Examquestion" + "@" + parent.toString();
                            new clientThread(MultiThreadChatServerSync.thread, senddata1).start();

                            if (MainActivity.examzipfile == null) {
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();


                            } else {
                                MainActivity.examzipfile.stopsocket();
                                MainActivity.examzipfile = null;
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();
                            }

                        }

                        studentResponse = parent.toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("output", parent.toString());

                    String senddata = "Examquestion" + "@" + parent.toString();
                    //   String jsonFormattedString = senddata.replaceAll("\\\\", "");

                    try {
                        StringEntity se = new StringEntity(senddata.toString(), "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE).edit();
                    editor.putInt("studentcount", 0);
                    editor.commit();
                    Intent in = new Intent("FinishStart");
                    Bundle mBundle = new Bundle();
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);
                }


                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "Sending questions to all students", Toast.LENGTH_LONG);

                {
                    pref = mContext.getSharedPreferences("Teacher", mContext.MODE_PRIVATE);
                    edit = pref.edit();
                    String StaffID = pref.getString("staffid", "");
                    String Evaluation = pref.getString("Evaluation", "0");
                    File username = new File(Evaluation + "/" + dataModel.getExamId());

                    DatabaseHandler db = new DatabaseHandler(mContext, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(dataModel.getExamId());
                    List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(dataModel.getExamId(), StaffID);


                    for (ExamDetails cn : examdetailsList) {
                        String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID() + " ,BatchID: " + cn.getBatchID() + " ,IsResultPublished: " + cn.getIsResultPublished() + " ,ExamShelfID: " + cn.getExamShelfID() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                        // Writing Contacts to log
                        Log.d("Exam2: ", log);
                        ExamID = cn.getExamID();
                        ExamCategoryID = cn.getExamCategoryID();
                        ExamCategory = cn.getExamCategoryName();
                        ExamCode = cn.getExamCode();
                        ExamDescription = cn.getExamDescription();
                        BatchID = cn.getBatchID();
                        ExamDuration = cn.getExamDuration();
                        SubjectID = cn.getSubjectID();
                        Subject = cn.getSubject();
                        ExamTypeID = cn.getExamTypeID();
                        ExamType = cn.getExamType();
                        ExamDate = cn.getExamDate();

                    }

                    JSONObject parent = new JSONObject();
                    JSONObject parent1 = new JSONObject();
                    JSONArray jsonArray = new JSONArray();
                    JSONArray jsonArray1 = new JSONArray();
                    JSONObject list2 = new JSONObject();
                    try {
                        list2.put("ExamID", ExamID).toString();
                        list2.put("ExamCategoryID", ExamCategoryID).toString();
                        list2.put("ExamCategory", ExamCategory).toString();
                        list2.put("ExamCode", ExamCode).toString();
                        list2.put("ExamDescription", ExamDescription).toString();
                        list2.put("BatchID", BatchID).toString();
                        list2.put("ExamDuration", ExamDuration).toString();
                        list2.put("SubjectID", SubjectID).toString();
                        list2.put("Subject", Subject).toString();
                        list2.put("ExamTypeID", ExamTypeID).toString();
                        list2.put("ExamType", ExamType).toString();
                        list2.put("ExamDate", ExamDate).toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    for (QuestionDetails cn : questiondetailsList) {
                        String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                        // Writing Contacts to log
                        Log.d("Question3: ", log);
                        JSONArray optionArray = new JSONArray();
                        String optionA = cn.getOptionA().toString();
                        String optionb = cn.getOptionB().toString();
                        String optionC = cn.getOptionC().toString();
                        String optionD = cn.getOptionD().toString();
                        optionA = optionA.replaceAll("'", "\\\\'");
                        optionA = optionA.replaceAll("\"", "\\\\\"");
                        optionb = optionb.replaceAll("'", "\\\\'");
                        optionb = optionb.replaceAll("\"", "\\\\\"");

                        optionC = optionC.replaceAll("'", "\\\\'");
                        optionC = optionC.replaceAll("\"", "\\\\\"");

                        optionD = optionD.replaceAll("'", "\\\\'");
                        optionD = optionD.replaceAll("\"", "\\\\\"");


/*
                        optionArray.put(optionA);
                        optionArray.put(optionb);
                        optionArray.put(optionC);
                        optionArray.put(optionD);
*/


                        if (cn.getOptionA().contains("img")) {
                            String text = cn.getOptionA().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionA());
                        }
                        if (cn.getOptionB().contains("img")) {
                            String text = cn.getOptionB().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionB());
                        }
                        if (cn.getOptionC().contains("img")) {
                            String text = cn.getOptionC().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);


                        } else {
                            optionArray.put(cn.getOptionC());
                        }
                        if (cn.getOptionD().contains("img")) {
                            String text = cn.getOptionD().replaceAll(username.getAbsolutePath() + "/", "");

                            optionArray.put(text);
                        } else {
                            optionArray.put(cn.getOptionD());
                        }

                        for (int z = 0; z < optionArray.length(); z++) {
                            String[] s = new String[optionArray.length()];

                            try {
                                testX(optionArray.get(z).toString(), username.getAbsolutePath());
                            } catch (Exception e) {
                            }

                        }

                        zip(batcharray, username.getAbsolutePath() + ".zip");

                      /*  optionArray.put(cn.getOptionA());
                        optionArray.put(cn.getOptionB());
                        optionArray.put(cn.getOptionC());
                        optionArray.put(cn.getOptionD());*/


                        try {


                            JSONObject list1 = new JSONObject();
                            if (cn.getQuestion().toString().contains("img")) {
                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath() + "/", "");

                                list1.put("Questions", text);


                            } else {
                                list1.put("Questions", cn.getQuestion().toString()).toString();
                            }
                            list1.put("QuestionID", cn.getQuestionID()).toString();
                            list1.put("TopicID", cn.getTopicID()).toString();
                            list1.put("Topic", cn.getTopicName().toString());
                            list1.put("AspectID", cn.getAspectID()).toString();
                            list1.put("Aspect", cn.getAspect()).toString();
                            list1.put("QuestionNumber", cn.getQuestionNumber()).toString();
                            if (cn.getQuestion().toString().contains("img")) {

                                String text = cn.getQuestion().replaceAll(username.getAbsolutePath() + "/", "");
                                testX(cn.getQuestion().toString(), username.getAbsolutePath());
                                list1.put("question", text);
                                zip(batcharray, username.getAbsolutePath() + ".zip");

                            } else {
                                list1.put("question", cn.getQuestion()).toString();
                            }
                            list1.put("options", optionArray);
                            list1.put("CorrectAnswer", cn.getCorrectAnswer()).toString();
                            list1.put("Mark", cn.getMark()).toString();
                            list1.put("NegativeMark", cn.getNegative_Mark()).toString();
                            jsonArray.put(list1);

                            // new connectTask().execute();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                    try {
                        list2.put("Questions", jsonArray).toString();
                        parent1.put("Exam", jsonArray1).toString();

                        jsonArray1.put(list2);

                        parent.put("status", "Success").toString();
                        parent.put("StatusCode", "200").toString();
                        parent.put("ExamDetails", parent1).toString();

                        if (batcharray.size() == 0) {
                            parent.put("Examsharing", "NO");
                            String senddata1 = "Examquestion" + "@" + parent.toString();
                            Intent in = new Intent(mContext, Attendancewithcheckparentlayout.class);
                            in.putExtra("senddata", senddata1);
                            mContext.startActivity(in);

                            //new clientThread(MultiThreadChatServerSync.thread, senddata1).start();

                        } else {
                            File file = new File(username.getAbsolutePath() + ".zip");
                            parent.put("Examsharing", "Yes");
                            parent.put("Filename", file.getName());
                            parent.put("Filelen", "" + file.length());
                            // String senddata1="Examsharing"+"@"+file.getName()+"@"+file.length();

                            String senddata1 = "Examquestion" + "@" + parent.toString();
                            // new clientThread(MultiThreadChatServerSync.thread, senddata1).start();

                            if (MainActivity.examzipfile == null) {
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();


                            } else {
                                MainActivity.examzipfile.stopsocket();
                                MainActivity.examzipfile = null;
                                MainActivity.examzipfile = new Examzipfile(file);
                                MainActivity.examzipfile.start();
                            }


                            Intent in = new Intent(mContext, Attendancewithcheckparentlayout.class);
                            in.putExtra("senddata", senddata1);
                            mContext.startActivity(in);
                            //String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();


                        }

                        studentResponse = parent.toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d("output", parent.toString());

                    String senddata = "Examquestion" + "@" + parent.toString();
                    //   String jsonFormattedString = senddata.replaceAll("\\\\", "");

                    try {
                        StringEntity se = new StringEntity(senddata.toString(), "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    SharedPreferences.Editor editor = mContext.getSharedPreferences(MY_PREFS_NAME, mContext.MODE_PRIVATE).edit();
                    editor.putInt("studentcount", 0);
                    editor.commit();


                    // String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                   /* Intent in = new Intent("FinishStart");
                    Bundle mBundle = new Bundle();
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);*/
                }
                //  File file=new File(video.getVideopath());

                /*if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();
*/
               /* Intent in=new Intent(BookBinActivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("senddata",senddata);
                startActivity(in);*/


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
            String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));

            String batchfile = m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            batcharray.add(path + "/" + batchfile);


        }

    }

    public void zip(ArrayList<String> _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

            for (int i = 0; i < _files.size(); i++) {
                Log.v("Compress", "Adding: " + _files.get(i));
                FileInputStream fi = new FileInputStream(_files.get(i));
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files.get(i).substring(_files.get(i).lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // View lookup cache
    private static class ViewHolder {
        TextView txtName, push;
        TextView preview, start;
        RelativeLayout parlay;
        ImageView delete;
    }

}
