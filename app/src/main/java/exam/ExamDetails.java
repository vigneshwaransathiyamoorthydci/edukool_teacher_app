package exam;

/**
 * Created by pratheeba on 5/3/2017.
 */
public class ExamDetails {
    int ExamID;
    int ExamCategoryID;
    String ExamCategoryName;
    String ExamCode;
    String ExamDescription;
    int ExamSequence;
    String StaffId;

    public String getStaffId() {
        return StaffId;
    }

    public void setStaffId(String staffId) {
        StaffId = staffId;
    }

    public int getExamID() {
        return ExamID;
    }

    public void setExamID(int examID) {
        ExamID = examID;
    }

    public int getExamCategoryID() {
        return ExamCategoryID;
    }

    public void setExamCategoryID(int examCategoryID) {
        ExamCategoryID = examCategoryID;
    }

    public String getExamCategoryName() {
        return ExamCategoryName;
    }

    public void setExamCategoryName(String examCategoryName) {
        ExamCategoryName = examCategoryName;
    }

    public String getExamCode() {
        return ExamCode;
    }

    public void setExamCode(String examCode) {
        ExamCode = examCode;
    }

    public String getExamDescription() {
        return ExamDescription;
    }

    public void setExamDescription(String examDescription) {
        ExamDescription = examDescription;
    }

    public int getExamSequence() {
        return ExamSequence;
    }

    public void setExamSequence(int examSequence) {
        ExamSequence = examSequence;
    }

    public String getExamDate() {
        return ExamDate;
    }

    public void setExamDate(String examDate) {
        ExamDate = examDate;
    }

    public int getExamTypeID() {
        return ExamTypeID;
    }

    public void setExamTypeID(int examTypeID) {
        ExamTypeID = examTypeID;
    }

    public String getExamType() {
        return ExamType;
    }

    public void setExamType(String examType) {
        ExamType = examType;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public int getExamDuration() {
        return ExamDuration;
    }

    public void setExamDuration(int examDuration) {
        ExamDuration = examDuration;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getIsResultPublished() {
        return IsResultPublished;
    }

    public void setIsResultPublished(int isResultPublished) {
        IsResultPublished = isResultPublished;
    }

    public int getExamShelfID() {
        return ExamShelfID;
    }

    public void setExamShelfID(int examShelfID) {
        ExamShelfID = examShelfID;
    }

    public int getTimeTaken() {
        return TimeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        TimeTaken = timeTaken;
    }

    public String getDateAttended() {
        return DateAttended;
    }

    public void setDateAttended(String dateAttended) {
        DateAttended = dateAttended;
    }

    public int getTotalScore() {
        return TotalScore;
    }

    public void setTotalScore(int totalScore) {
        TotalScore = totalScore;
    }

    String ExamDate;
    int ExamTypeID;
    String ExamType;
    int  SubjectID;
    String Subject;
    int ExamDuration;
    int SchoolID;
    int ClassID;
    int BatchID;
    int IsResultPublished;
    int ExamShelfID;
    int TimeTaken;
    String DateAttended;
    int TotalScore;

    public ExamDetails(){

    }

    public ExamDetails(int ExamID, int ExamCategoryID,String ExamCategoryName, String ExamCode,String ExamDescription,int ExamSequence ,String ExamDate,int ExamTypeID,String ExamType,int  SubjectID,String Subject,int ExamDuration,int SchoolID,int ClassID,
                       int BatchID,
                       int IsResultPublished,
                       int ExamShelfID,
                       int TimeTaken,
                       String DateAttended,
                       int TotalScore
    ){
        this.ExamID = ExamID;

        this.ExamCategoryID = ExamCategoryID;
        this.ExamCategoryName = ExamCategoryName;
        this.ExamCode = ExamCode;
        this.ExamDescription = ExamDescription;
        this.ExamSequence = ExamSequence;
        this.ExamDate = ExamDate;
        this.ExamTypeID = ExamTypeID;
        this.ExamType = ExamType;
        this.SubjectID = SubjectID;
        this.Subject = Subject;
        this.ExamDuration = ExamDuration;
        this.SchoolID = SchoolID;
        this.ClassID = ClassID;
        this.BatchID = BatchID;
        this.IsResultPublished = IsResultPublished;
        this.ExamShelfID = ExamShelfID;
        this.TimeTaken = TimeTaken;
        this.DateAttended = DateAttended;
        this.TotalScore = TotalScore;

    }

    public ExamDetails(int ExamID,int Timetaken, String DateAttended){
        this.ExamID = ExamID;
        this.TimeTaken = Timetaken;
        this.DateAttended = DateAttended;
    }
    public ExamDetails(int ExamID, String StaffId, int ExamCategoryID,String ExamCategoryName, String ExamCode,String ExamDescription,int ExamSequence ,String ExamDate,int ExamTypeID,String ExamType,int  SubjectID,String Subject,int ExamDuration,int SchoolID,int ClassID,
                       int BatchID,
            int IsResultPublished,
            int ExamShelfID,
            int TimeTaken,
            String DateAttended,
            int TotalScore
    ){
        this.ExamID = ExamID;
        this.StaffId = StaffId;

        this.ExamCategoryID = ExamCategoryID;
        this.ExamCategoryName = ExamCategoryName;
        this.ExamCode = ExamCode;
        this.ExamDescription = ExamDescription;
        this.ExamSequence = ExamSequence;
        this.ExamDate = ExamDate;
        this.ExamTypeID = ExamTypeID;
        this.ExamType = ExamType;
        this.SubjectID = SubjectID;
        this.Subject = Subject;
        this.ExamDuration = ExamDuration;
        this.SchoolID = SchoolID;
        this.ClassID = ClassID;
        this.BatchID = BatchID;
        this.IsResultPublished = IsResultPublished;
        this.ExamShelfID = ExamShelfID;
        this.TimeTaken = TimeTaken;
        this.DateAttended = DateAttended;
        this.TotalScore = TotalScore;

    }


}
