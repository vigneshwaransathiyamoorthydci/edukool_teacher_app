package exam;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;

/**
 * Created by pratheeba on 4/21/2017.
 */
;import Utilities.Utilss;
import workbook.URLImageParser;

public class ExamPreviewAdapter extends BaseAdapter {
    public static final String MY_PREFS_NAME = "GENERALINFO";
    static SharedPreferences preftutorial;
    static ArrayList<QuestionPojo> planListAdapter = new ArrayList<QuestionPojo>();
    public View view;
    public int currPosition = 0;
    Context context;
    int layoutId;
    ProgressDialog mProgressDialog;
    Holder holder;
    String user_id_main;
    String option1;
    String option2;
    String option3;
    String option4;
    Utilss utils;

    public ExamPreviewAdapter(Context context, int textViewResourceId,
                              ArrayList<QuestionPojo> list) {

        this.context = context;
        ExamPreviewAdapter.planListAdapter = list;
        layoutId = textViewResourceId;
        utils = new Utilss((Activity) context);
    }

    @Override
    public int getCount() {
        return planListAdapter.size();
    }

    @Override
    public QuestionPojo getItem(int position) {
        return planListAdapter.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        RelativeLayout layout;
        if (convertView == null) {
            // set layout view initialize the resource Id
            layout = (RelativeLayout) View.inflate(context, layoutId, null);
            holder = new Holder();
            holder.optiontext1 = (TextView) layout.findViewById(R.id.optiontext1);
            holder.optiontext2 = (TextView) layout.findViewById(R.id.optiontext2);
            holder.optiontext3 = (TextView) layout.findViewById(R.id.optiontext3);
            holder.optiontext4 = (TextView) layout.findViewById(R.id.optiontext4);
            holder.quiz_question = (TextView) layout.findViewById(R.id.quiz_question);
            holder.resultinquestion = (ImageView) layout.findViewById(R.id.resultinquestion);
            holder.option1 = (RadioButton) layout.findViewById(R.id.radio0);
            holder.option2 = (RadioButton) layout.findViewById(R.id.radio1);
            holder.option3 = (RadioButton) layout.findViewById(R.id.radio2);
            holder.option4 = (RadioButton) layout.findViewById(R.id.radio3);
            utils.setTextviewtypeface(1, holder.quiz_question);
            utils.setcheckboxtypeface(1, holder.option1);
            utils.setcheckboxtypeface(1, holder.option2);
            utils.setcheckboxtypeface(1, holder.option3);
            utils.setcheckboxtypeface(1, holder.option4);
            layout.setTag(holder);
        } else {
            layout = (RelativeLayout) convertView;
            view = layout;

        }
        holder = (Holder) layout.getTag();

      /*  int start = planListAdapter.get(position).getAnswer1().indexOf("src=\"")+5;
        int end =  planListAdapter.get(position).getAnswer1().indexOf("\"", start);
        String getAnswer1 = planListAdapter.get(position).getAnswer1().substring(start, end);

        System.out.print(getAnswer1 + "getAnswer1");*/


        try {
            String starttag = "<p>";
            String endtag = "</p>";
            option1 = planListAdapter.get(position).getAnswer1();
            option1 = option1.replace("<br>", "");
            option2 = planListAdapter.get(position).getAnswer2();
            option2 = option2.replace("<br>", "");


            option3 = planListAdapter.get(position).getAnswer3();
            option3 = option3.replace("<br>", "");

            option4 = planListAdapter.get(position).getAnswer4();
            option4 = option4.replace("<br>", "");

            if (option2.startsWith("<p>") && option2.endsWith("</p>")) {
                option2 = option2.substring(starttag.length(), option2.length() - endtag.length());
            }
            if (option3.startsWith("<p>") && option3.endsWith("</p>")) {
                option3 = option3.substring(starttag.length(), option3.length() - endtag.length());

            }
            if (option4.startsWith("<p>") && option4.endsWith("</p>")) {
                option4 = option4.substring(starttag.length(), option4.length() - endtag.length());

            }
            if (option1.startsWith("<p>") && option1.endsWith("</p>")) {
                option1 = option1.substring(starttag.length(), option1.length() - endtag.length());

            }

           /* File imgFile = new File("/sdcard/Images/test_image.jpg");

            String source = "this is a test of <b>ImageGetter</b> it contains " +
                    "two images: <br/>" +
                    "<img src=/sdcard/css_3/Users/T_4/no_staff.jpg\"><br/>and<br/>"
                  ;*/


/*new AsyncTask<Void,Void,Void>()
{

 *//*   Spanned htmlSpan ;
    Spanned htmlSpan1;
    Spanned htmlSpan2 ;
    Spanned htmlSpan3 ;
*//*
    @Override
    protected Void doInBackground(Void... params) {


        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        URLImageParser p = new URLImageParser(holder.option1, context);
        URLImageParser p1 = new URLImageParser(holder.option2, context);
        URLImageParser p2 = new URLImageParser(holder.option3, context);
        URLImageParser p3 = new URLImageParser(holder.option4, context);


        Spanned htmlSpan = Html.fromHtml(option1, p, null);
        Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);
        Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
        Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);
        holder.option1.setText(htmlSpan);
        holder.option2.setText(htmlSpan1);
        holder.option3.setText(htmlSpan2);
        holder.option4.setText(htmlSpan3);
    }


}.execute();*/
            URLImageParser question = new URLImageParser(holder.quiz_question, context);
            URLImageParser p = new URLImageParser(holder.option1, context);
            URLImageParser p1 = new URLImageParser(holder.option2, context);
            URLImageParser p2 = new URLImageParser(holder.option3, context);
            URLImageParser p3 = new URLImageParser(holder.option4, context);

            Spanned questionSpan = Html.fromHtml(planListAdapter.get(position).getQuestion(), question, null);
            Spanned htmlSpan = Html.fromHtml(option1, p, null);
            Spanned htmlSpan1 = Html.fromHtml(option2, p1, null);
            Spanned htmlSpan2 = Html.fromHtml(option3, p2, null);
            Spanned htmlSpan3 = Html.fromHtml(option4, p3, null);

            if (option1.contains("img")) {
                holder.option1.setText(htmlSpan);

            } else {
                holder.option1.setText(Html.fromHtml(htmlSpan.toString()).toString());

            }
            if (option2.contains("img")) {
                holder.option2.setText(htmlSpan1);

            } else {
                holder.option2.setText(Html.fromHtml(htmlSpan1.toString()).toString());

            }
            if (option3.contains("img")) {
                holder.option3.setText(htmlSpan2);

            } else {
                holder.option3.setText(Html.fromHtml(htmlSpan2.toString()).toString());

            }
            if (option4.contains("img")) {
                holder.option4.setText(htmlSpan3);

            } else {
                holder.option4.setText(Html.fromHtml(htmlSpan3.toString()).toString());
            }

            if (planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("A")) {
                holder.option1.setChecked(true);
                holder.option1.setTextColor(context.getResources().getColor(R.color.subject_title_grey));
                holder.option2.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option3.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option4.setTextColor(context.getResources().getColor(R.color.subject_grey));

            } else if (planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("B")) {
                holder.option2.setChecked(true);
                holder.option2.setTextColor(context.getResources().getColor(R.color.subject_title_grey));
                holder.option1.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option3.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option4.setTextColor(context.getResources().getColor(R.color.subject_grey));


            } else if (planListAdapter.get(position).getCorrectAnswer().equalsIgnoreCase("C")) {
                holder.option3.setChecked(true);
                holder.option3.setTextColor(context.getResources().getColor(R.color.subject_title_grey));
                holder.option2.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option1.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option4.setTextColor(context.getResources().getColor(R.color.subject_grey));

            } else {
                holder.option4.setTextColor(context.getResources().getColor(R.color.subject_title_grey));
                holder.option1.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option3.setTextColor(context.getResources().getColor(R.color.subject_grey));
                holder.option2.setTextColor(context.getResources().getColor(R.color.subject_grey));

                holder.option4.setChecked(true);
            }

            if (planListAdapter.get(position).getQuestion().contains("img")) {
                holder.quiz_question.setText(questionSpan);
            } else {
                holder.quiz_question.setText(Html.fromHtml(planListAdapter.get(position).getQuestion().toString()).toString());

            }
            System.out.println(planListAdapter.get(position).getCorrectAnswer() + "correct ans");


        } catch (Exception e) {

        }

        return layout;
    }

    public int getCurrentPosition() {
        return currPosition;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 1;
    }

    //Setting pojo for user comment
    private class Holder {
        public TextView quiz_question;
        public RadioButton option1;
        public RadioButton option2;
        public RadioButton option3;
        public RadioButton option4;
        public ImageView resultinquestion;
        TextView optiontext1, optiontext2, optiontext3, optiontext4;


    }


}

