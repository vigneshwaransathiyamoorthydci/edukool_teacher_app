package exam;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import Utilities.Service;
import Utilities.*;

import com.dci.edukool.teacher.MainActivity;
import com.dci.edukool.teacher.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import books.Attendancewithcheckparentlayout;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Subjectnameandid;

/**
 * Created by pratheeba on 5/9/2017.
 */
public class ReceiveandSendMarkActivity extends Activity {
    ArrayList<DataModel> dataModels;
    private static ViewAnswerAdapter adapter;
    ListView listView;
    DatabaseHandler db;
    ImageView back;
    TextView classstaff;
    int ExamIDValue;
    String senddata;
    String MY_PREFS_NAME = "EXAMDETAILS";
    SharedPreferences.Editor edit;
    SharedPreferences pref;
    ArrayList<NameValuePair> sharetoportal = new ArrayList<NameValuePair>();
    Utilss utils;
    ProgressDialog dia;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    int QuestionID;
    int StudentResponseId;
    String ExamDescription;
    String BatchIDVal ;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    String Subject ;
    TextView tatal_marks;
    int SubjectID;
    String ExamTypeIDVal ;
    int ioptionto;
    String ExamType ;
    int ExamTypeID;
    String ExamDate, studentname,profilename ;
    int ExamResponseID;
    String DateAttendedVal;
    ImageView profilImageView;
    BroadcastReceiver questionreceiver,ShareToPortal;
    String  StudentAnswer;
    int IsCorrect;
    int MarkForAnswer;
    int  ObtainedScore;
    String arrayOfExam;
    String StaffID;
    Spinner subjectspinner;
    List<StudentQuestionResultPojo> resultspojo;
    String StaffUserName;
    TextView ans_list;
    ArrayList<Integer> studenId = new ArrayList<>();
    List<Subjectnameandid> subjecNameArray;
    TextView view_ques_txt,knowassess;




    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_send_and_receive_lay);


        listView = (ListView)findViewById(R.id.listView);
        ans_list=(TextView)findViewById(R.id.view_ques_txt);
        knowassess=(TextView)findViewById(R.id.knowassess);


        profilImageView= (ImageView) findViewById(R.id.profileimage);
        subjectspinner= (Spinner) findViewById(R.id.subjectspinner);

        back = (ImageView)findViewById(R.id.back);
        pref=getSharedPreferences("Teacher", MODE_PRIVATE);
        edit=pref.edit();
        db= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        classstaff= (TextView)findViewById(R.id.classstaff);
        classstaff.setText(pref.getString("classname", ""));
        setbackground(profilImageView, pref.getString("image", ""));
        StaffID =pref.getString("staffid", "");
        StaffUserName=pref.getString("portalstaffid", "");
        dataModels= new ArrayList<>();
        adapter= new ViewAnswerAdapter(dataModels,this);
        if(!pref.getBoolean("roomornot",false)) {
            subjecNameArray = db.getbatchdetails(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("batchid", "0")),Integer.parseInt(pref.getString("staffid","0")));
        }
        else
        {
            subjecNameArray = db.getbatchdetailsfrommasterinfo();
        }
            CustomSpinnerAdapter down=new CustomSpinnerAdapter(ReceiveandSendMarkActivity.this,android.R.layout.simple_spinner_item,subjecNameArray);
        // Customadapter adapter=new Customadapter(getApplicationContext(),gettable);
        // upper=new Upperspinner(this,android.R.layout.simple_spinner_item,gettable);
        subjectspinner.setAdapter(down);

        subjectspinner.setSelection(0);


        utils=new Utilss(ReceiveandSendMarkActivity.this);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        subjectspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dataModels.clear();

                List<ExamDetails> examdetailsList = db.GetAllExamFromServer(subjecNameArray.get(position).getId(),StaffID);

                for (ExamDetails cn : examdetailsList) {
                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " +
                            cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                    // Writing Contacts to log
                    dataModels.add(new DataModel(cn.getExamDescription(),cn.getExamID()));

                    Log.d("Exam2: ", log+examdetailsList.size());
                }
                listView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        utils=new Utilss(this);
        utils.setTextviewtypeface(3,ans_list);
        utils.setTextviewtypeface(3,knowassess);
        utils.setTextviewtypeface(5,classstaff);

    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {


                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    stratExamCompletedPopup(ExamIDValue);
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putInt("EXAMID", ExamIDValue);
                    editor.commit();

                    // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };

        ShareToPortal=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {


                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    stratSharetoportal(ExamIDValue);

                    // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("ExamResultPop");
        registerReceiver(questionreceiver, intent);

        IntentFilter intent1=new IntentFilter("ShareToPortal");
        registerReceiver(ShareToPortal, intent1);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
        unregisterReceiver(ShareToPortal);
    }

    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


    public void stratSharetoportal(final int examID) {
        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_to_portal);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        title.setText("SHARE EXAM RESULT TO PORTAL");
        try {
            revisit.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               dialog.dismiss();

                                               JSONObject parent1 = new JSONObject();
                                               JSONArray jsonArray1 = new JSONArray();
                                               JSONArray examarray = new JSONArray();
                                               JSONArray resultarray = new JSONArray();

                                               JSONObject list2 = new JSONObject();
                                               JSONObject list3 = new JSONObject();
                                               JSONArray jsonArray = new JSONArray();
                                               JSONObject topParent = new JSONObject();
                                               JSONObject parent = new JSONObject();

                                               try {
                                                   list3.put("ExamID", examID).toString();
                                               } catch (Exception e) {
                                                   e.printStackTrace();

                                               }




                                               List<ExamResponsePojo> examrespojo = db.addStudentExamResponseahareportal(examID);

                                               for (ExamResponsePojo cn : examrespojo) {

/*
                                                   if (studenId.contains(cn.getStudentID())) {

                                                   } else {
                                                       studenId.add(cn.getStudentID());
*/
                                                   String log = "ExamResponseID: " + cn.getExamResponseID() + " ,ExamId: " + cn.getExamID() + " ,StudentID: " + cn.getStudentID() + " ,StudName: " + cn.getStudName() + " ,RollNo: " + cn.getRollNo() + " ,TimeTaken: " + cn.getTimeTaken() + " ,DateAttended: " + cn.getDateAttended() + " ,TotalScore: " + cn.getTotalScore();
                                                   // Writing Contacts to log
                                                   Log.d("ExamResponseID: ", log);
                                                   ExamResponseID = cn.getExamResponseID();
                                                   int ExamID = cn.getExamID();
                                                   String StudName = cn.getStudName();
                                                   int TimeTaken = cn.getTimeTaken();
                                                   DateAttendedVal = cn.getDateAttended();
                                                   int TotalScore = cn.getTotalScore();
                                                   int StudentID = cn.getStudentID();
                                                   int RollNo = cn.getRollNo();

                                                   try {
                                                       list2 = new JSONObject();
                                                       list2.put("ExamResponseID", ExamResponseID).toString();
                                                       list2.put("ExamID", ExamID).toString();
                                                       list2.put("TimeTaken", "1800").toString();
                                                       list2.put("StudentID", StudentID).toString();
                                                       list2.put("DateAttended", DateAttendedVal).toString();
                                                       list2.put("TotalScore", TotalScore).toString();
                                                       resultarray.put(list2);
                                                   } catch (Exception e) {
                                                       e.printStackTrace();
                                                   }

                                                   studenId.add(ExamResponseID);



                                                   resultspojo = db.tblStudentQuestionResultusingresponseID(ExamResponseID);
                                                   jsonArray = new JSONArray();
                                                   for (StudentQuestionResultPojo cn1 : resultspojo) {

                                                       StudentResponseId = cn1.getResponseID();
                                                       QuestionID = cn1.getQuestionID();
                                                       StudentAnswer = cn1.getStudentAnswer();
                                                       IsCorrect = cn1.getIsCorrect();
                                                       MarkForAnswer = cn1.getMarkForAnswer();
                                                       ObtainedScore = cn1.getObtainedScore();
                                                       // String log = "StudentResponseId: " + cn.getResponseID() + " ,QuestionID: " + cn.getQuestionID() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,MarkForAnswer: " + cn.getMarkForAnswer() + " ,ObtainedScore: " + cn.getObtainedScore();
                                                       // Writing Contacts to log
                                                       try {
                                                           JSONObject list1 = new JSONObject();
                                                           list1.put("StudentResponseID", StudentResponseId).toString();
                                                           list1.put("QuestionID", QuestionID).toString();
                                                           list1.put("StudentAnswer", StudentAnswer);
                                                           list1.put("IsCorrect", IsCorrect).toString();
                                                           list1.put("MarkForAnswer", MarkForAnswer).toString();
                                                           list1.put("ObtainedMark", ObtainedScore).toString();
                                                           jsonArray.put(list1);
                                                           list2.put("StudentResponse", jsonArray).toString();

                                                       } catch (Exception e) {
                                                           e.printStackTrace();
                                                       }
                                                   }
                                                   // }


                                               }
                                               for (int k = 0; k < studenId.size(); k++) {

                                               }

                                               studenId.clear();
                                               try {

                                                   list3.put("Result", resultarray).toString();

                                                   topParent.put("Exam", examarray).toString();
                                                   examarray.put(list3);
                                                   jsonArray1.put(topParent);
                                                   parent.put("ExamDetails", topParent).toString();
                                               } catch (Exception e) {
                                                   e.printStackTrace();
                                               }
                                               Log.d("output", parent.toString());
                                               arrayOfExam = parent.toString();
                                               String login_str = "|UserName:" + StaffUserName + "|StudentInfo:" + arrayOfExam + "|Function:SubmitExamResult" + "|StaffId:" + StaffID;
                                               // String login_str = "";
                                               sharetoportal.clear();
                                               byte[] data;
                                               try {
                                                   data = login_str.getBytes("UTF-8");
                                                   String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                                                   if (utils.hasConnection()) {
                                                       sharetoportal.clear();
                                                       sharetoportal.add(new BasicNameValuePair("WS", base64_register));
                                                   } else {
                                                       utils.Showalert();
                                                   }
                                                   Load_Download_WS load_plan_list = new Load_Download_WS(ReceiveandSendMarkActivity.this, sharetoportal);
                                                   load_plan_list.execute();

                                               } catch (UnsupportedEncodingException e) {
                                                   e.printStackTrace();
                                               }
                                           }
                                       }

            );
                valuate.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
    public void stratExamCompletedPopup(final int examID) {
        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_exam_result);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        ImageView back = (ImageView) dialog.findViewById(R.id.back);


        Button revisit = (Button) dialog.findViewById(R.id.button3);
        title.setText("ASSESMENT RESULT SHARING");
        try {
            revisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    JSONObject parent = new JSONObject();
                    JSONObject list1 = new JSONObject();
                    dialog.dismiss();
                    try {
                        parent.put("ResultStatus", "Completed").toString();
                        parent.put("ExamID", examID).toString();
                        list1.put("ResultFromTeacher", parent);
                        String senddata = "ExamResult" + "@" + parent.toString();
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            valuate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    JSONObject parent = new JSONObject();
                    JSONObject list1 = new JSONObject();
                    try {
                        parent.put("ResultStatus", "Completed").toString();
                        parent.put("ExamID", examID).toString();
                        list1.put("ResultFromTeacher", parent);
                         senddata = "ExamResult" + "@" + parent.toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(ReceiveandSendMarkActivity.this, Attendancewithcheckparentlayout.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("senddata", senddata);
                    intent.putExtras(mBundle);
                    startActivity(intent);

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }



    class Load_Download_WS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public Load_Download_WS(Context context_ws,
                                ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(ReceiveandSendMarkActivity.this);
            dia.setMessage("GET IN");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service sr = new Service(ReceiveandSendMarkActivity.this);
                jsonResponseString = sr.getLogin(sharetoportal,Url.baseurl
                       /* "http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            Log.e("jsonResponse", "share" + jsonResponse);
            try {



                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success")) {
                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(ReceiveandSendMarkActivity.this);
                            dia.setMessage("DOWNLOADING");
                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {




                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                            edit.putBoolean("login",true);
                            edit.commit();
                            startActivity(new Intent(ReceiveandSendMarkActivity.this,MainActivity.class));
                            finish();

                        }
                    }.execute();

                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                Toast.makeText(getApplicationContext(), "Failed to share web portal", Toast.LENGTH_LONG).show();

                System.out.println(e.toString() + "zcx");
            }

        }
    }

}
