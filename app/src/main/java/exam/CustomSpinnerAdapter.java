package exam;

/**
 * Created by Pratheeba on 6/5/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import Utilities.Utilss;

import helper.Subjectnameandid;


import java.util.List;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class CustomSpinnerAdapter extends ArrayAdapter {


    Utilss utils;
    private Context context;
    private List<Subjectnameandid> itemList;
    Activity act;
    public CustomSpinnerAdapter(Context context, int textViewResourceId,List<Subjectnameandid> itemList) {

        super(context, textViewResourceId,itemList);
        this.context=context;
        act= (Activity) context;
        this.itemList=itemList;
        utils=new Utilss(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(5,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(itemList.get(position).getName());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        utils.setTextviewtypeface(2,make);

        make.setText(itemList.get(position).getName());

        return row;
    }

}
