package exam;

/**
 * Created by pratheeba on 5/29/2017.
 */
public class PulseResponsePojo {

    int PulseResponseID;
    int PulseID;
    int StudentID;

    public int getPulseResponseID() {
        return PulseResponseID;
    }

    public void setPulseResponseID(int pulseResponseID) {
        PulseResponseID = pulseResponseID;
    }

    public int getPulseID() {
        return PulseID;
    }

    public void setPulseID(int pulseID) {
        PulseID = pulseID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    String Response;
    String CreatedOn;
    String ModifiedOn;

    public PulseResponsePojo(){

    }
    public PulseResponsePojo(int PulseResponseID, int PulseID,int StudentID, String Response,String CreatedOn,String ModifiedOn){
        this.PulseResponseID = PulseResponseID;
        this.PulseID = PulseID;
        this.StudentID = StudentID;
        this.Response = Response;
        this.CreatedOn = CreatedOn;
        this.ModifiedOn = ModifiedOn;

    }

    public PulseResponsePojo(int PulseID,int StudentID, String Response,String CreatedOn,String ModifiedOn){
        this.PulseID = PulseID;
        this.StudentID = StudentID;
        this.Response = Response;
        this.CreatedOn = CreatedOn;
        this.ModifiedOn = ModifiedOn;

    }
}
