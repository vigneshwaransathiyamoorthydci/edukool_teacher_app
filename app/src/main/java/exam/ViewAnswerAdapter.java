package exam;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;


/**
 * Created by pratheeba on 4/25/2017.
 */
public class ViewAnswerAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener{

    private ArrayList<DataModel> dataSet;
    Context mContext;
    String studentResponse;
    int ExamCategoryID;
    int ExamID;
    String ExamCategory;
    String ExamCode;
    String ExamDescription;
    int BatchID;
    int ExamDuration;
    int SubjectID;
    String Subject;
    int ExamTypeID;
    String ExamType;
    String ExamDate;
    SharedPreferences pref;
    Utilss utils;
    // View lookup cache
    private static class ViewHolder {
        TextView txtName,start,web_portal,share_studentlay1;
        RelativeLayout parlay,share_studentlay,web_portallay;
    }

    public ViewAnswerAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.view_ans_lay, data);
        this.dataSet = data;
        this.mContext=context;
        utils=new Utilss((Activity) mContext);
        pref=mContext.getSharedPreferences("Teacher", mContext.MODE_PRIVATE);
    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel dataModel=(DataModel)object;

        Toast.makeText(mContext, "onclick", Toast.LENGTH_LONG).show();

       /* switch (v.getId())
        {
          *//*  case R.id.item_info:
                Snackbar.make(v, "Release date " + dataModel.getFeature(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;*//*
        }*/
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.view_ans_lay, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.exam_list);
            viewHolder.start = (TextView) convertView.findViewById(R.id.start);
            viewHolder.web_portal = (TextView) convertView.findViewById(R.id.web_portal);
            viewHolder.share_studentlay1 = (TextView) convertView.findViewById(R.id.share_studentlay1);

            utils.setTextviewtypeface(3,viewHolder.txtName);
            utils.setTextviewtypeface(5,viewHolder.start);
            utils.setTextviewtypeface(5,viewHolder.web_portal);
            utils.setTextviewtypeface(5,viewHolder.share_studentlay1);
            viewHolder.parlay = (RelativeLayout) convertView.findViewById(R.id.view_student_mark);
            viewHolder.web_portallay = (RelativeLayout) convertView.findViewById(R.id.web_portallay);

            viewHolder.share_studentlay = (RelativeLayout) convertView.findViewById(R.id.share_studentlay);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }


        if (position % 2 == 1) {
           // convertView.setBackgroundResource(R.color.appbg);
        } else {
          //  convertView.setBackgroundResource(R.color.appbg);
        }



        viewHolder.web_portallay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent("ShareToPortal");
                Bundle mBundle = new Bundle();
                mBundle.putInt("ExamIDValue", dataModel.getExamId());
                in.putExtras(mBundle);
                mContext.sendBroadcast(in);

            }
        });



        viewHolder.share_studentlay.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                try {
                    Intent in=new Intent("ExamResultPop");
                    Bundle mBundle = new Bundle();
                    mBundle.putInt("ExamIDValue", dataModel.getExamId());
                    in.putExtras(mBundle);
                    mContext.sendBroadcast(in);
                    //  stu.loadQuestions();
                }
                catch (Exception e){
                    e.printStackTrace();
                }

               // stratExamCompletedPopup(dataModel.getExamId());

            }
        });


        viewHolder.parlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                {

                    DatabaseHandler db = new DatabaseHandler(mContext,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
                    String  StaffID =pref.getString("staffid", "");

                    List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(dataModel.getExamId());


                   // List<QuestionDetails> questiondetailsList = db.getAllExamQuestionsUsingExamId(dataModel.getExamId());
                    List<ExamDetails> examdetailsList = db.getAllExamUsingExamId(dataModel.getExamId(),StaffID);
                    List<ExamResponsePojo> examrespojo = db.addStudentExamResponseUsingExamId(dataModel.getExamId());

                    if(examrespojo.size()==0){

                        Toast.makeText(mContext,"Exam results not found",Toast.LENGTH_LONG).show();

                    }else{
                        int ExamId = dataModel.getExamId();
                        Intent intent = new Intent(mContext, RadioListStudentResultActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putInt("ExamID", ExamId);
                        intent.putExtras(mBundle);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);



                        for (ExamDetails cn : examdetailsList) {
                            String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                            // Writing Contacts to log
                            Log.d("Exam2: ", log);
                            ExamID=  cn.getExamID();
                            ExamCategoryID =  cn.getExamCategoryID();
                            ExamCategory = cn.getExamCategoryName();
                            ExamCode = cn.getExamCode();
                            ExamDescription =  cn.getExamDescription();
                            BatchID = cn.getBatchID();
                            ExamDuration = cn.getExamDuration();
                            SubjectID = cn.getSubjectID();
                            Subject =  cn.getSubject();
                            ExamTypeID =  cn.getExamTypeID();
                            ExamType = cn.getExamType();
                            ExamDate =cn.getExamDate();

                        }


                        for (ExamDetails cn : examdetailsList) {
                            String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                            // Writing Contacts to log
                            Log.d("Exam2: ", log);
                            ExamID=  cn.getExamID();
                            ExamCategoryID =  cn.getExamCategoryID();
                            ExamCategory = cn.getExamCategoryName();
                            ExamCode = cn.getExamCode();
                            ExamDescription =  cn.getExamDescription();
                            BatchID = cn.getBatchID();
                            ExamDuration = cn.getExamDuration();
                            SubjectID = cn.getSubjectID();
                            Subject =  cn.getSubject();
                            ExamTypeID =  cn.getExamTypeID();
                            ExamType = cn.getExamType();
                            ExamDate =cn.getExamDate();

                        }



                        for (QuestionDetails cn : questiondetailsList) {
                            String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark()+ " ,Negative_Mark: " + cn.getNegative_Mark()+ " ,StudentAnswer: " + cn.getStudentAnswer()+ " ,IsCorrect: " + cn.getIsCorrect()+ " ,ObtainedScore: " + cn.getObtainedScore()+ " ,CreatedOn: " + cn.getCreatedOn()+ " ,ModifiedOn: " + cn.getModifiedOn();
                            // Writing Contacts to log
                            Log.d("Question3: ", log);




                        }
                    }

                    //Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        // result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtName.setText(Html.fromHtml(dataModel.getName()));

                // Return the completed view to render on screen
        return convertView;
    }




    public void stratExamCompletedPopup(final int examID) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.share_exam_result);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        title.setText("ASSESMENT RESULT SHARING");
        try {
            revisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dialog.dismiss();
                }
            });
            valuate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject parent = new JSONObject();
                    JSONObject list1 = new JSONObject();

                    try {
                        parent.put("ResultStatus", "Completed").toString();
                        parent.put("ExamID", examID).toString();
                        list1.put("ResultFromTeacher", parent);
                        String senddata = "ExamResult" + "@" + parent.toString();
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
