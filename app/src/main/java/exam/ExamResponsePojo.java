package exam;

/**
 * Created by pratheeba on 5/9/2017.
 */
public class ExamResponsePojo {


    public int getExamResponseID() {
        return ExamResponseID;
    }

    public void setExamResponseID(int examResponseID) {
        ExamResponseID = examResponseID;
    }

    public int getExamID() {
        return ExamID;
    }

    public void setExamID(int examID) {
        ExamID = examID;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getRollNo() {
        return RollNo;
    }

    public void setRollNo(int rollNo) {
        RollNo = rollNo;
    }

    public String getStudName() {
        return StudName;
    }

    public void setStudName(String studName) {
        StudName = studName;
    }

    public int getTimeTaken() {
        return TimeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        TimeTaken = timeTaken;
    }

    public String getDateAttended() {
        return DateAttended;
    }

    public void setDateAttended(String dateAttended) {
        DateAttended = dateAttended;
    }

    public int getTotalScore() {
        return TotalScore;
    }

    public void setTotalScore(int totalScore) {
        TotalScore = totalScore;
    }
    int ExamResponseID;

    int ExamID;
    int StudentID;
    int RollNo;
    String StudName;
    int TimeTaken;
    String DateAttended;
    int TotalScore;
    public ExamResponsePojo(int ExamResponseID, int ExamID,int StudentID, int RollNo,String StudName,int TimeTaken ,String DateAttended,int TotalScore){
        this.ExamResponseID = ExamResponseID;
        this.ExamID = ExamID;
        this.StudentID = StudentID;
        this.RollNo = RollNo;
        this.StudName = StudName;
        this.TimeTaken = TimeTaken;
        this.DateAttended = DateAttended;
        this.TotalScore = TotalScore;

    }


    public ExamResponsePojo(){

    }

}
