package workbook;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Utilities.DatabaseHandler;
import exam.CustomSpinnerAdapter;
import exam.DataModel;
import exam.ExamDetails;
import exam.WorkAdapter;
import helper.Subjectnameandid;

/**
 * Created by pratheeba on 5/9/2017.
 */
public class WorkBookViewQuestionActivity extends Activity {
    ArrayList<DataModel> dataModels;
    private static WorkAdapter adapter;
    ListView listView;
    DatabaseHandler db;
    ImageView back;
    TextView classstaff;
    SharedPreferences.Editor edit;
    SharedPreferences pref;
    TextView view_ques_txt;
    ImageView profilImageView;
    BroadcastReceiver questionreceiver,questionreceiver1;
    int ExamIDValue,ExamIDposition;
    Spinner subjectspinner;
    String StaffID;
    List<Subjectnameandid> subjecNameArray;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_question_layout);

        listView = (ListView)findViewById(R.id.listView);
        back = (ImageView)findViewById(R.id.back);
        profilImageView= (ImageView) findViewById(R.id.profileimage);
        view_ques_txt = (TextView)findViewById(R.id.view_ques_txt);
        view_ques_txt.setText("LIST OF EXAMS");
        subjectspinner= (Spinner) findViewById(R.id.subjectspinner);
        pref=getSharedPreferences("Teacher", MODE_PRIVATE);
        edit=pref.edit();

        classstaff= (TextView)findViewById(R.id.classstaff);
        classstaff.setText(pref.getString("classname", ""));
        StaffID =pref.getString("staffid", "");
        setbackground(profilImageView, pref.getString("image", ""));

       // db=new DatabaseHandler(this);
        db=new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        dataModels= new ArrayList<>();
        adapter= new WorkAdapter(dataModels,WorkBookViewQuestionActivity.this);



        if(!pref.getBoolean("roomornot",false)) {
            subjecNameArray = db.getbatchdetails(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("batchid", "0")),Integer.parseInt(pref.getString("staffid","0")));
        }
        else
        {
            subjecNameArray = db.getbatchdetailsfrommasterinfo();
        }
      /*  for (ExamDetails cn : subjectlist) {
            String subjectName = cn.getSubject();
            int subjectID = cn.getSubjectID();
            subjecNameArray.add(cn.getSubject());

        }*/

        CustomSpinnerAdapter down=new CustomSpinnerAdapter(WorkBookViewQuestionActivity.this,android.R.layout.simple_spinner_item,subjecNameArray);
        // Customadapter adapter=new Customadapter(getApplicationContext(),gettable);
        // upper=new Upperspinner(this,android.R.layout.simple_spinner_item,gettable);
        subjectspinner.setAdapter(down);

        subjectspinner.setSelection(0);

        subjectspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                    dataModels.clear();

                    List<ExamDetails> examdetailsList = db.GetAllExamFromServerSelf(subjecNameArray.get(position).getId(),StaffID);

                    for (ExamDetails cn : examdetailsList) {
                        String log = "cn.getExamType()"+cn.getExamType()+"ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " +
                                cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                        // Writing Contacts to log
                        dataModels.add(new DataModel(cn.getExamDescription(), cn.getExamID()));

                        Log.d("Exam2: ", log);
                    }

                    adapter.notifyDataSetChanged();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

      /*  List<ExamDetails> examdetailsList = db.GetAllExamFromServer();

        for (ExamDetails cn : examdetailsList) {
            String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " +
                    cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + cn.getExamDate() + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
            // Writing Contacts to log
            dataModels.add(new DataModel(cn.getExamDescription(),cn.getExamID()));

            Log.d("Exam2: ", log);
        }*/


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        questionreceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {


                    Bundle bundle = intent.getExtras();
                    ExamIDValue = bundle.getInt("ExamIDValue");
                    ExamIDposition = bundle.getInt("ExamIDposition");
                    stratExamCompletedPopup(ExamIDValue);

                    // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent=new IntentFilter("ExamDELETEPop");
        registerReceiver(questionreceiver, intent);

        questionreceiver1=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {


                        finish();
                    // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent1=new IntentFilter("FinishStart");
        registerReceiver(questionreceiver1,intent1);




    }



    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver);
        unregisterReceiver(questionreceiver1);
    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


    public void stratExamCompletedPopup(final int examID) {
        final Dialog dialog = new Dialog(this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_exam);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        title.setText("DELETE EXAM");
        try {
            revisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    db.deleteToDo(examID);
                    dataModels.remove(ExamIDposition);
                    adapter.notifyDataSetChanged();
                    dialog.dismiss();

                }
            });
            valuate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     dialog.dismiss();
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}
