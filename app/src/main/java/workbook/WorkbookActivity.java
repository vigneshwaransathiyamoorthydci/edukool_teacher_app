package workbook;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.R;

import Utilities.Service;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import Utilities.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import Utilities.Utilss;
import exam.ExamDetails;
import exam.QuestionDetails;
import exam.QuizWrapper;
import helper.RoundedImageView;
import Utilities.DatabaseHandler;

/**
 * Created by pratheeba on 5/15/2017.
 */
public class WorkbookActivity extends BaseActivity{
    String studentname,profilename,PortalUserID;
    String StudentID;
    String RollNo;
    TextView stdname,class_name;
    RoundedImageView profileimage;
    ImageView download_exam;
    ArrayList<NameValuePair> selfasses = new ArrayList<NameValuePair>();
    Utilss utils;
    ImageView back;
    private ImageView results,view_questions_list;
    int ExamID;
    String ExamCategoryIDVal;
    int ExamCategoryID ;
    String ExamCategory ;
    String ExamCode ;
    int QuestionID;
    String ExamDescription;
    String BatchIDVal ;
    String storagepath;
    String ExamDurationVal;
    int BatchID ;
    int ExamDuration ;
    int Mark;
    String SubjectIDVal;
    String Subject ;
    int ObtainedMark ;
    String ObtainedMarkVal;
    String IsCorrectVal;
    String MarkVal;
    String NegativeMarkVal;
    int SubjectID;
    String ExamTypeIDVal ;
    int NegativeMark;
    ProgressDialog dia;
    int IsCorrect;
    String MarkForAnswer ;
    String ExamType ;
    String ExamDate ;
    int AspectID;
    int ExamTypeID;
    SharedPreferences.Editor edit;
    int TopicID;
    String TopicIDVal,Topic,AspectIDVal,Aspect,QuestionNumberVal;
    int CorrectAnswerVal;
    String CorrectAnswer;
    String StudentAnswer;
    String ExamIDVal;
    int QuestionNumber;
    String question;
    ArrayList<String> pathArray;
    String staffid;
    String portalstaffid;

    SharedPreferences pref;
    DatabaseHandler db;
    TextView title,tv_english,tv_maths,tv_science,tv_tamil,tv_soscience;
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        utils=new Utilss(WorkbookActivity.this);
        download_exam= (ImageView) findViewById(R.id.download_exams);
        results = (ImageView) findViewById(R.id.results);
        title=(TextView) findViewById(R.id.title);
        title.setText("WorkBook");
        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_english=(TextView) findViewById(R.id.tv_english);
        tv_maths=(TextView) findViewById(R.id.tv_maths);
        tv_science=(TextView) findViewById(R.id.tv_science);
        tv_tamil=(TextView) findViewById(R.id.tv_tamil);
        tv_soscience=(TextView) findViewById(R.id.tv_soscience);
        view_questions_list= (ImageView) findViewById(R.id.view_questions_list);
        stdname= (TextView)findViewById(R.id.studentname);
        class_name = (TextView)findViewById(R.id.class_name);
        profileimage= (RoundedImageView)findViewById(R.id.profileimage);
        back= (ImageView)findViewById(R.id.back);
        pref=getSharedPreferences("Teacher", MODE_PRIVATE);
        edit=pref.edit();
        staffid= (pref.getString("staffid", ""));
        portalstaffid= (pref.getString("portalstaffid", ""));
        pathArray = new ArrayList<>();
        //db = new DatabaseHandler(this);
        db=new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        stdname.setText(pref.getString("classname","").toUpperCase());
        class_name.setText(pref.getString("classname", ""));
        setbackground(profileimage, pref.getString("image", ""));

        if(profilename!=null)setbackground(profileimage,profilename);
        results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                   /* Intent intent = new Intent(WorkbookActivity.this, SelfEvaluationTotalExamList.class);
                    intent.putExtra("RecentExamID", ExamID);
                    intent.putExtra("RecentExamIDValue", "RecentExamID");

                    startActivity(intent);*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                   finish();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        utils.setTextviewtypeface(3,title);
        utils.setTextviewtypeface(5,tv_english);
        utils.setTextviewtypeface(5,tv_maths);
        utils.setTextviewtypeface(5,tv_science);
        utils.setTextviewtypeface(5,tv_tamil);
        utils.setTextviewtypeface(5,tv_soscience);


        view_questions_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(WorkbookActivity.this, WorkBookViewQuestionActivity.class);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        download_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String login_str="UserName:"+portalstaffid+"|Function:DownloadSelfExam"+"|StaffId:"+staffid;

                // String login_str="";
                selfasses.clear();
                byte[] data ;
                try {
                    data = login_str.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if(utils.hasConnection()) {
                        selfasses.clear();
                        selfasses.add(new BasicNameValuePair("WS", base64_register));


                       // String test = "<p><p><img src=\"http:\\/\\/www.edukool.com\\/imageUpload\\/examquestion\\/option1\\/286f9311cec47f0a2acfedfe3495b3bd.jpg\" style=\\\"\\\"><\\/p>This keeps the doctor away<\\/p>\"";


                     //   String all = "<img src=\"http://www.01net.com/images/article/mea/150.100.790233.jpg\""; // shortened it
                     //  String str = "<img src=\"http://www.01net.com/images/article/mea/150.100.790233.jpg\" width=\"150\" height=\"100\" border=0 alt=\"\" align=left style=\"margin-right:10px;margin-bottom:5px;\">A en croire CNet US, le gouvernement américain aurait cherché à obtenir les master keys de plusieurs acteurs du Web pour pouvoir déchiffrer les communications de leurs utilisateurs, protégées par le protocole SSL.<img width='1' height='1' src='http://rss.feedsportal.com/c/629/f/502199/s/2f34155b/mf.gif' border='0'/><div class='mf-viral'><table border='0'><tr><td valign='middle'><a href=\"http://share.feedsportal.com/share/twitter/?u=http%3A%2F%2Fwww.01net.com%2Feditorial%2F600625%2Fchiffrement-sur-le-web-fbi-et-nsa-voulaient-obtenir-les-cles-ssl-de-geants-du-net%2F%23%3Fxtor%3DRSS-16&t=Chiffrement+sur+le+Web%2C+FBI+et+NSA+voulaient+obtenir+les+cl%C3%A9s+SSL+de+g%C3%A9ants+du+Net\" target=\"_blank\"><img src=\"http://res3.feedsportal.com/social/twitter.png\" border=\"0\" /></a> <a href=\"http://share.feedsportal.com/share/facebook/?u=http%3A%2F%2Fwww.01net.com%2Feditorial%2F600625%2Fchiffrement-sur-le-web-fbi-et-nsa-voulaient-obtenir-les-cles-ssl-de-geants-du-net%2F%23%3Fxtor%3DRSS-16&t=Chiffrement+sur+le+Web%2C+FBI+et+NSA+voulaient+obtenir+les+cl%C3%A9s+SSL+de+g%C3%A9ants+du+Net\" target=\"_blank\"><img src=\"http://res3.feedsportal.com/social/facebook.png\" border=\"0\" /></a> <a href=\"http://share.feedsportal.com/share/linkedin/?u=http%3A%2F%2Fwww.01net.com%2Feditorial%2F600625%2Fchiffrement-sur-le-web-fbi-et-nsa-voulaient-obtenir-les-cles-ssl-de-geants-du-net%2F%23%3Fxtor%3DRSS-16&t=Chiffrement+sur+le+Web%2C+FBI+et+NSA+voulaient+obtenir+les+cl%C3%A9s+SSL+de+g%C3%A9ants+du+Net\" target=\"_blank\"><img src=\"http://res3.feedsportal.com/social/linkedin.png\" border=\"0\" /></a> <a href=\"http://share.feedsportal.com/share/gplus/?u=http%3A%2F%2Fwww.01net.com%2Feditorial%2F600625%2Fchiffrement-sur-le-web-fbi-et-nsa-voulaient-obtenir-les-cles-ssl-de-geants-du-net%2F%23%3Fxtor%3DRSS-16&t=Chiffrement+sur+le+Web%2C+FBI+et+NSA+voulaient+obtenir+les+cl%C3%A9s+SSL+de+g%C3%A9ants+du+Net\" target=\"_blank\"><img src=\"http://res3.feedsportal.com/social/googleplus.png\" border=\"0\" /></a> <a href=\"http://share.feedsportal.com/share/email/?u=http%3A%2F%2Fwww.01net.com%2Feditorial%2F600625%2Fchiffrement-sur-le-web-fbi-et-nsa-voulaient-obtenir-les-cles-ssl-de-geants-du-net%2F%23%3Fxtor%3DRSS-16&t=Chiffrement+sur+le+Web%2C+FBI+et+NSA+voulaient+obtenir+les+cl%C3%A9s+SSL+de+g%C3%A9ants+du+Net\" target=\"_blank\"><img src=\"http://res3.feedsportal.com/social/email.png\" border=\"0\" /></a></td><td valign='middle'></td></tr></table></div><br/><br/><a href=\"http://da.feedsportal.com/r/172449334514/u/218/f/502199/c/629/s/2f34155b/kg/342/a2.htm\"><img src=\"http://da.feedsportal.com/r/172449334514/u/218/f/502199/c/629/s/2f34155b/kg/342/a2.img\" border=\"0\"/></a><img width=\"1\" height=\"1\" src=\"http://pi.feedsportal.com/r/172449334514/u/218/f/502199/c/629/s/2f34155b/kg/342/a2t.img\" border=\"0\"/>";


                        SelfEvaluationWS load_plan_list = new SelfEvaluationWS(WorkbookActivity.this, selfasses);
                        load_plan_list.execute();
                    }
                    else
                    {
                        utils.Showalert();
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


            }
        });
        setSubjectbackround();

    }
    public void setSubjectbackround(){
        if (pref.getString("subjectname","")!=null&&!pref.getString("subjectname","").equals("")){
            String subname=pref.getString("subjectname","").toLowerCase();

            switch (subname){
                case "english":
                    tv_english.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_english.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "maths":
                    tv_maths.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_maths.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "tamil":
                    tv_tamil.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_tamil.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "sceince":
                    tv_science.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_science.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "social science":
                    tv_soscience.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_soscience.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        }

    }

    @Override
    public int getlayout() {
        return R.layout.workbook_home_layout;
    }

    public String testX(String hjdsf, String path) {
        Pattern p = Pattern.compile("<img.+?src=[\\\\\\\\\\\\\\\"'](.+?)[\\\\\\\\\\\\\\\"'].+?>");
        Matcher m = p.matcher(hjdsf);
        String test="";
        if (m.find()) {

            System.out.println(m.group(1).substring(1));
             String filename = m.group(1).substring(1).substring(0, m.group(1).substring(1).lastIndexOf('.'));
             // prints http://www.01net.com/images/article/mea/150.100.790233.jpg

            String batchfile=m.group(1).substring(1).substring(m.group(1).substring(1).lastIndexOf("/") + 1, m.group(1).substring(1).length());

            downloadfile(m.group(1).substring(1), path, batchfile);
            storagepath= path+"/"+batchfile;

             test = hjdsf.replaceAll(m.group(1).substring(1),storagepath);

            System.out.println(test+"test");
            //pathArray.add(test);
        }


        return test;

    }



    int BUFFER = 2048;

    public void zip(String _files, String zipFileName) {
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zipFileName);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            byte data[] = new byte[BUFFER];

          //  for (int i = 0; i < 1; i++) {
                Log.v("Compress", "Adding: " + _files);
                FileInputStream fi = new FileInputStream(_files);
                origin = new BufferedInputStream(fi, BUFFER);

                ZipEntry entry = new ZipEntry(_files.substring(_files.lastIndexOf("/") + 1));
                out.putNextEntry(entry);
                int count;

                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
           // }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setbackground(ImageView view, String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    class SelfEvaluationWS extends AsyncTask<String, String, String> {

        String jsonResponseString;

        InputStream inputstream = null;
        Dialog loginDialog;
        //	String str = "WS";

        ProgressDialog pd;

        public SelfEvaluationWS(Context context_ws,
                             ArrayList<NameValuePair> loginws) {
            // TODO Auto-generated constructor stub


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(WorkbookActivity.this);
           dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
            // showdialog();
               /* dialog=new Dialog(LoginActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.show();*/

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            try {

                Service  sr = new Service(WorkbookActivity.this);
                jsonResponseString = sr.getLogin(selfasses, Url.baseurl
                        /*"http://api.schoolproject.dci.in/api/"*/);
                   /* sr = new Service(context_aact);
                    jsonResponseString = sr.getLogin(loginact,
                            "http://api.schoolproject.dci.in/api/");*/
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return jsonResponseString;
        }

        @Override
        protected void onPostExecute(String jsonResponse) {
            super.onPostExecute(jsonResponse);
            if(dia.isShowing())
                dia.cancel();
            Log.e("jsonResponse", "Login" + jsonResponse);
            try {



                final JSONObject jObj = new JSONObject(jsonResponse);


                String status = jObj.getString("status");

                if (status.toString().equalsIgnoreCase("Success")) {
                    new  AsyncTask<Void, Void,Void>()
                    {


                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            dia=new ProgressDialog(WorkbookActivity.this);
                            dia.setMessage("DOWNLOADING");
                            dia.setCancelable(false);
                            dia.show();
                        }

                        @Override
                        protected Void doInBackground(Void... params) {

                            try
                            {


                                Log.e("examdetails,","details");

                                JSONArray Exam_arr=jObj.getJSONObject("examDetails").getJSONArray("exam");

                                for (int i = 0; i < Exam_arr.length(); i++) {
                                    JSONObject objexam=Exam_arr.getJSONObject(i);
                                    ExamIDVal = objexam.getString("ExamID");

                                    ExamID = Integer.parseInt(ExamIDVal);
                                    ExamCategoryIDVal = objexam.getString("ExamCategoryID");
                                    ExamCategoryID = Integer.parseInt(ExamCategoryIDVal);
                                    ExamCategory = objexam.getString("ExamCategory");
                                    ExamCode = objexam.getString("ExamCode");
                                    ExamDescription = objexam.getString("ExamDescription");
                                    BatchIDVal = objexam.getString("BatchID");
                                    ExamDurationVal = objexam.getString("ExamDuration");
                                    try {
                                        BatchID = Integer.parseInt(BatchIDVal);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                    ExamDuration = Integer.parseInt(ExamDurationVal);

                                    SubjectIDVal = objexam.getString("SubjectID");
                                    Subject = objexam.getString("Subject");

                                    SubjectID = Integer.parseInt(SubjectIDVal);
                                    ExamTypeIDVal = objexam.getString("ExamTypeID");

                                    ExamType = objexam.getString("ExamType");

                                    // ExamDate = objexam.getString("ExamDate");

                                    ExamTypeID = Integer.parseInt(ExamTypeIDVal);
                   /* String TimeTakenVal = objexam.getString("TimeTaken");

                    String DateAttended = objexam.getString("DateAttended");

                    String TotalScoreVal = objexam.getString("TotalScore");
                    int TimeTaken = Integer.parseInt(TimeTakenVal);
                    int TotalScore = Integer.parseInt(TotalScoreVal);*/
                                    // String Questions = objexam.getString("Questions");

                                    int ExamSequence =0;
                                    int SchoolID =0;
                                    int IsResultPublished =0;
                                    int ExamShelfID =0;
                                    int ClassID =0;
                                    int TimeTaken = 0;
                                    int TotalScore =0;
                                    String DateAttended ="01/06/2017 10:00:00";

                                    boolean isExamIdexist = db.CheckIsIDAlreadyInDBorNot(ExamIDVal);
                                    if(isExamIdexist){

                                        db.downloadUpdateExamDetails(new ExamDetails(ExamID, staffid,ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));
                                        Log.e("examdetails,","true");
                                        System.out.println("isExamIdexist"+ExamIDVal+isExamIdexist);
                                    }
                                    else{
                                        Log.e("examdetails,","false");
                                        System.out.println("isExamIdexist"+ExamIDVal+isExamIdexist);
                                        db.addExamDetails(new ExamDetails(ExamID,staffid, ExamCategoryID, ExamCategory, ExamCode, ExamDescription, ExamSequence, ExamDate, ExamTypeID, ExamType, SubjectID, Subject, ExamDuration, SchoolID, ClassID,
                                                BatchID, IsResultPublished,
                                                ExamShelfID,
                                                TimeTaken,
                                                DateAttended,
                                                TotalScore));

                                    }

                                    List<QuizWrapper> jsonObject = new ArrayList<QuizWrapper>();
                                    Log.e("examdetails,",""+objexam);
                                    JSONArray jsonArrayquestion = objexam.optJSONArray("Questions");
                                    if (jsonArrayquestion!=null){
                                        Log.e("examdetails,",""+jsonArrayquestion.length());
                                        QuizWrapper newItemObject = null;

                                        for(int j = 0; j < jsonArrayquestion.length(); j++){
                                            JSONObject jsonChildNode = null;
                                            try {
                                                jsonChildNode = jsonArrayquestion.getJSONObject(j);
                                                String QuestionIDval = jsonChildNode.getString("QuestionID");
                                                QuestionID = Integer.parseInt(QuestionIDval);
                                                TopicIDVal = jsonChildNode.getString("TopicID");
                                                if(TopicIDVal.equalsIgnoreCase("")){
                                                    TopicID=1;
                                                }
                                                else{
                                                    TopicID = Integer.parseInt(TopicIDVal);

                                                }

                                                // int TopicID=1;
                                                Topic = jsonChildNode.getString("Topic");
                                                AspectIDVal = jsonChildNode.getString("AspectID");
                                                if(AspectIDVal.equalsIgnoreCase("")){
                                                    AspectID=1;
                                                }
                                                else{
                                                    AspectID = Integer.parseInt(AspectIDVal);

                                                }
                                                Aspect = jsonChildNode.getString("Aspect");
                                                QuestionNumberVal = jsonChildNode.getString("QuestionNumber");
                                                QuestionNumber = Integer.parseInt(QuestionNumberVal);
                                                question= jsonChildNode.getString("Question");
                                                JSONArray options = jsonChildNode.getJSONArray("Options");
                                                CorrectAnswer = jsonChildNode.getString("CorrectAnswer");
                                                CorrectAnswerVal = 1;
                                                MarkVal = jsonChildNode.getString("Mark");
                                                Mark = Integer.parseInt(MarkVal);

                                                NegativeMarkVal = jsonChildNode.getString("NegativeMark");
                                                NegativeMark = Integer.parseInt(NegativeMarkVal);
                                                String Created_on = "01/06/2017 10:00:00";
                                                String ModifiedOn = "01/06/2017 10:00:00";
                                                StudentAnswer ="";
                                                newItemObject = new QuizWrapper(QuestionID, question, options, CorrectAnswerVal);
                                                String Evaluation = pref.getString("Evaluation", "0");
                                                File username=new File(Evaluation+"/"+ExamID);
                                                username.mkdir();
                                                if(question.contains("img")){
                                                    String questionImg = testX(question,username.getAbsolutePath());
                                                    question=questionImg;
                                                }

                                                for(int z = 0; z < options.length(); z++){
                                                    String[] s = new String[options.length()];
                                                    String storagepath = testX(options.get(z).toString(),username.getAbsolutePath());
                                                    if(storagepath.contains(Evaluation)){
                                                        options.put(z,storagepath);
                                                    }
                                                }

                                                if(isExamIdexist){

                                                    if(options.length()>3){
                                                        db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), options.get(3).toString(), CorrectAnswer, Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }

                                                    else if(options.length()>2){
                                                        db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "", CorrectAnswer, Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }

                                                    else if(options.length()>1){
                                                        db.downloadUpdateQuestionDetailsByID(new QuestionDetails(QuestionID, ExamID, TopicID, Topic, AspectID, Aspect, question, QuestionNumber,options.get(0 ).toString(),options.get(1).toString(), "", "", CorrectAnswer, Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }


                                                    System.out.println("isExamIdexist" + ExamIDVal + isExamIdexist);
                                                }
                                                else{
                                                    System.out.println("isExamIdexist" + ExamIDVal + isExamIdexist);
                                                    if(options.length()>3){
                                                        db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(),options.get(3).toString(),CorrectAnswer , Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }

                                                    else if(options.length()>2){
                                                        db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(), options.get(1).toString(), options.get(2).toString(), "",CorrectAnswer , Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }

                                                    else if(options.length()>1){
                                                        db.addExamQuestions(new QuestionDetails(QuestionID,ExamID,TopicID,Topic,AspectID,Aspect,question, QuestionNumber, options.get(0).toString(),options.get(1).toString(), "", "",CorrectAnswer , Mark,
                                                                NegativeMark, StudentAnswer,
                                                                IsCorrect,
                                                                ObtainedMark,
                                                                Created_on,
                                                                ModifiedOn));
                                                    }

                                                }

                                                List<ExamDetails> examdetailsList = db.getAllExamsDetails(staffid);
                                                List<QuestionDetails> questiondetailsList = db.getAllExamQuestions();
                                                Calendar calendar = Calendar.getInstance();
                                                SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMM-yyyy ");
                                                String strDate = mdformat.format(calendar.getTime());

                                                for (ExamDetails cn : examdetailsList) {
                                                    String log = "ExamID: " + cn.getExamID() + " ,ExamCategoryID: " + cn.getExamCategoryID() + " ,ExamCategoryName: " + cn.getExamCategoryName() + " ,ExamCode: " + cn.getExamCode() + " ,ExamDescription: " + cn.getExamDescription() + " ,ExamSequence: " + cn.getExamSequence() + " ,ExamDate: " + strDate + " ,ExamTypeID: " + cn.getExamTypeID() + " ,SubjectID " + cn.getSubjectID() + " ,Subject: " + cn.getSubject() + " ,ExamDuration: " + cn.getExamDuration() + " ,SchoolID: " + cn.getSchoolID() + " ,ClassID: " + cn.getClassID()+ " ,BatchID: " + cn.getBatchID()+ " ,IsResultPublished: " + cn.getIsResultPublished()+ " ,ExamShelfID: " + cn.getExamShelfID()+ " ,TimeTaken: " + cn.getTimeTaken()+ " ,DateAttended: " + cn.getDateAttended()+ " ,TotalScore: " + cn.getTotalScore();
                                                    // Writing Contacts to log
                                                    Log.d("Exam2: ", log);
                                                }

                                                for (QuestionDetails cn : questiondetailsList) {
                                                    String log = "QuestionID: " + cn.getQuestionID() + " ,ExamID: " + cn.getExamID() + " ,TopicID: " + cn.getTopicID() + " ,TopicName: " + cn.getTopicName() + " ,AspectID: " + cn.getAspectID() + " ,Question: " + cn.getQuestion() + " ,QuestionNumber: " + cn.getQuestionNumber() + " ,OptionA: " + cn.getOptionA() + " ,OptionB " + cn.getOptionB() + " ,OptionC: " + cn.getOptionC() + " ,OptionD: " + cn.getOptionD() + " ,CorrectAnswer: " + cn.getCorrectAnswer() + " ,Mark: " + cn.getMark() + " ,Negative_Mark: " + cn.getNegative_Mark() + " ,StudentAnswer: " + cn.getStudentAnswer() + " ,IsCorrect: " + cn.getIsCorrect() + " ,ObtainedScore: " + cn.getObtainedScore() + " ,CreatedOn: " + cn.getCreatedOn() + " ,ModifiedOn: " + cn.getModifiedOn();
                                                    // Writing Contacts to log
                                                    Log.d("Question2: ", log);
                                                }

                                                jsonObject.add(newItemObject);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }


                                    // Adding child data

                                    // newItemObject = new QuizWrapper(ExamID, ExamCategoryID, ExamDescription, ExamCode);
                                    //jsonObject.add(newItemObject);

                                }

                            }
                            catch (Exception e)
                            {
                                System.out.println(e.toString() + "zcx");

                            }

                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if(dia.isShowing())
                                dia.cancel();
                            downloadSuccess();

                           // Toast.makeText(getApplicationContext(),"Exam Downloaded Successfully", Toast.LENGTH_LONG).show();
                           // finish();

                        }
                    }.execute();

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Invalid Credentials",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                if(dia.isShowing())
                    dia.cancel();
                System.out.println(e.toString() + "zcx");
            }

        }
    }
    public void downloadSuccess() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_download_success);
        dialog.setCancelable(false);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        TextView logout = (TextView) dialog.findViewById(R.id.logout);
        TextView textView5 = (TextView) dialog.findViewById(R.id.textView5);
        TextView textView6 = (TextView) dialog.findViewById(R.id.textView6);
        utils.setTextviewtypeface(5,ok_button);
        utils.setTextviewtypeface(5,logout);
        utils.setTextviewtypeface(3,textView5);
        utils.setTextviewtypeface(5,textView6);

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.clear();
                edit.commit();
                finish();
                dialog.dismiss();
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }




    /*public downLoadExamSync (){

    }*/






}
