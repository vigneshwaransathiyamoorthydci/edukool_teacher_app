package helper;

/**
 * Created by abimathi on 21-Apr-17.
 */
public class Filesharing {
    String filetype;
    String filename;



    String displayname;
    String oringinalname;
    byte[] byets;


    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getOringinalname() {
        return oringinalname;
    }

    public void setOringinalname(String oringinalname) {
        this.oringinalname = oringinalname;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getByets() {
        return byets;
    }

    public void setByets(byte[] byets) {
        this.byets = byets;
    }
}
