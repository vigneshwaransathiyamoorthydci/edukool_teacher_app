package helper;

/**
 * Created by abimathi on 05-Jun-17.
 */
public class Calendarpojo {

    int calendarid;
    String calendarname;
    String calendardescription;
    String calendarimage;

    public int getCalendarid() {
        return calendarid;
    }

    public void setCalendarid(int calendarid) {
        this.calendarid = calendarid;
    }

    public String getCalendarname() {
        return calendarname;
    }

    public void setCalendarname(String calendarname) {
        this.calendarname = calendarname;
    }

    public String getCalendardescription() {
        return calendardescription;
    }

    public void setCalendardescription(String calendardescription) {
        this.calendardescription = calendardescription;
    }

    public String getCalendarimage() {
        return calendarimage;
    }

    public void setCalendarimage(String calendarimage) {
        this.calendarimage = calendarimage;
    }
}
