package helper;

/**
 * Created by iyyapparajr on 5/5/2017.
 */
public class tblclasses {

  int  ClassID;
   String ClassName;
    String SectionName;
    String  Grade ;
    String  ClassCode;
    int SchoolID;
    int  DepartmentID;
    int  StaffID ;
    String   InternetSSID ;
    String  InternetPassword ;
    String  InternetType;
    String  PushName;
    String  tblclassesCreatedOn;
    String tblclassesModifiedOn;
    String batchid;
    boolean rommornot;

    public boolean isRommornot() {
        return rommornot;
    }

    public void setRommornot(boolean rommornot) {
        this.rommornot = rommornot;
    }

    public String getBatchid() {
        return batchid;
    }

    public void setBatchid(String batchid) {
        this.batchid = batchid;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String grade) {
        Grade = grade;
    }

    public String getClassCode() {
        return ClassCode;
    }

    public void setClassCode(String classCode) {
        ClassCode = classCode;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public int getDepartmentID() {
        return DepartmentID;
    }

    public void setDepartmentID(int departmentID) {
        DepartmentID = departmentID;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getInternetSSID() {
        return InternetSSID;
    }

    public void setInternetSSID(String internetSSID) {
        InternetSSID = internetSSID;
    }

    public String getInternetPassword() {
        return InternetPassword;
    }

    public void setInternetPassword(String internetPassword) {
        InternetPassword = internetPassword;
    }

    public String getInternetType() {
        return InternetType;
    }

    public void setInternetType(String internetType) {
        InternetType = internetType;
    }

    public String getPushName() {
        return PushName;
    }

    public void setPushName(String pushName) {
        PushName = pushName;
    }

    public String getTblclassesCreatedOn() {
        return tblclassesCreatedOn;
    }

    public void setTblclassesCreatedOn(String tblclassesCreatedOn) {
        this.tblclassesCreatedOn = tblclassesCreatedOn;
    }

    public String getTblclassesModifiedOn() {
        return tblclassesModifiedOn;
    }

    public void setTblclassesModifiedOn(String tblclassesModifiedOn) {
        this.tblclassesModifiedOn = tblclassesModifiedOn;
    }
}
