package helper;

/**
 * Created by iyyapparajr on 5/13/2017.
 */
public class Attendancepojo {



    
    public int        SchoolID ;
    public int      BatchID ;
    public int      SubjectID ;
    public int        StaffID ;
    public String      AttendanceDateTime ;
    public int       Credits ;
    public int       StudentID ;
    public int       RollNo ;
    public String   StudName ;
    public String     Attendance  ;
    public int      ManualAttendance ;
    public int     IsUploaded=0; ;
    public String      CreatedOn ;
    public String      ModifiedOn ;
    public int Isnew=1;
    public int classid;
    public int roomid=0;

    public int getRoomid() {
        return roomid;
    }

    public void setRoomid(int roomid) {
        this.roomid = roomid;
    }

    public int getClassid() {
        return classid;
    }

    public void setClassid(int classid) {
        this.classid = classid;
    }

    public int getIsnew() {
        return Isnew;
    }

    public void setIsnew(int isnew) {
        Isnew = isnew;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getAttendanceDateTime() {
        return AttendanceDateTime;
    }

    public void setAttendanceDateTime(String attendanceDateTime) {
        AttendanceDateTime = attendanceDateTime;
    }

    public int getCredits() {
        return Credits;
    }

    public void setCredits(int credits) {
        Credits = credits;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public int getRollNo() {
        return RollNo;
    }

    public void setRollNo(int rollNo) {
        RollNo = rollNo;
    }

    public String getStudName() {
        return StudName;
    }

    public void setStudName(String studName) {
        StudName = studName;
    }

    public String getAttendance() {
        return Attendance;
    }

    public void setAttendance(String attendance) {
        Attendance = attendance;
    }

    public int getManualAttendance() {
        return ManualAttendance;
    }

    public void setManualAttendance(int manualAttendance) {
        ManualAttendance = manualAttendance;
    }

    public int getIsUploaded() {
        return IsUploaded;
    }

    public void setIsUploaded(int isUploaded) {
        IsUploaded = isUploaded;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
