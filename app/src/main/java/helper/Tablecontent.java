package helper;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Tablecontent {


    int     ContentID=0;
    String     ContentDescription;
    String     Author;
    int       ContentTypeID=0;
    String     ContentCatalogType;
    String     ContentFilename;
    int     BookShelfID=0;
    String     Vaporize;
    int      IsEncrypted=0;
    String   EncryptionSalt;
    int    SchoolID;
    int    BatchID;
    int    StaffID;
    String   CreatedOn;
    String   ModifiedOn;
    String classes;
    String subject;
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getContentID() {
        return ContentID;
    }

    public void setContentID(int contentID) {
        ContentID = contentID;
    }

    public String getContentDescription() {
        return ContentDescription;
    }

    public void setContentDescription(String contentDescription) {
        ContentDescription = contentDescription;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public int getContentTypeID() {
        return ContentTypeID;
    }

    public void setContentTypeID(int contentTypeID) {
        ContentTypeID = contentTypeID;
    }

    public String getContentCatalogType() {
        return ContentCatalogType;
    }

    public void setContentCatalogType(String contentCatalogType) {
        ContentCatalogType = contentCatalogType;
    }

    public String getContentFilename() {
        return ContentFilename;
    }

    public void setContentFilename(String contentFilename) {
        ContentFilename = contentFilename;
    }

    public int getBookShelfID() {
        return BookShelfID;
    }

    public void setBookShelfID(int bookShelfID) {
        BookShelfID = bookShelfID;
    }

    public String getVaporize() {
        return Vaporize;
    }

    public void setVaporize(String vaporize) {
        Vaporize = vaporize;
    }

    public int getIsEncrypted() {
        return IsEncrypted;
    }

    public void setIsEncrypted(int isEncrypted) {
        IsEncrypted = isEncrypted;
    }

    public String getEncryptionSalt() {
        return EncryptionSalt;
    }

    public void setEncryptionSalt(String encryptionSalt) {
        EncryptionSalt = encryptionSalt;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
