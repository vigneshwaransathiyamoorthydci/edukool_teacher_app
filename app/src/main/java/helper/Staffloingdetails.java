package helper;

/**
 * Created by iyyapparajr on 5/4/2017.
 */
public class Staffloingdetails {

    int StaffID;
     String EmployementNumber;
    String DOJ;
    String FirstName;
    String MiddleName;
    String LastName;
    String Gender;
    String DOB;
    String MaritalStatusID;
    String SpouseName;
    String FatherName;
    String MotherName;
    String Phone;
    String Email;
    String PhotoFilename;
    int StaffCategoryID;
    String SchoolID;
     int  DeptartmentID;
    int DesignationID;
    String PortalUserID;
    String PortalLoginID;
    String CreatedOn;
    String ModifiedOn;

    String roominfo;
    String timetable;

    public String getTimetable() {
        return timetable;
    }

    public void setTimetable(String timetable) {
        this.timetable = timetable;
    }

    public String getRoominfo() {
        return roominfo;
    }

    public void setRoominfo(String roominfo) {
        this.roominfo = roominfo;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getEmployementNumber() {
        return EmployementNumber;
    }

    public void setEmployementNumber(String employementNumber) {
        EmployementNumber = employementNumber;
    }

    public String getDOJ() {
        return DOJ;
    }

    public void setDOJ(String DOJ) {
        this.DOJ = DOJ;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getMaritalStatusID() {
        return MaritalStatusID;
    }

    public void setMaritalStatusID(String maritalStatusID) {
        MaritalStatusID = maritalStatusID;
    }

    public String getSpouseName() {
        return SpouseName;
    }

    public void setSpouseName(String spouseName) {
        SpouseName = spouseName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhotoFilename() {
        return PhotoFilename;
    }

    public void setPhotoFilename(String photoFilename) {
        PhotoFilename = photoFilename;
    }

    public int getStaffCategoryID() {
        return StaffCategoryID;
    }

    public void setStaffCategoryID(int staffCategoryID) {
        StaffCategoryID = staffCategoryID;
    }

    public String getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(String schoolID) {
        SchoolID = schoolID;
    }

    public int getDeptartmentID() {
        return DeptartmentID;
    }

    public void setDeptartmentID(int deptartmentID) {
        DeptartmentID = deptartmentID;
    }

    public int getDesignationID() {
        return DesignationID;
    }

    public void setDesignationID(int designationID) {
        DesignationID = designationID;
    }

    public String getPortalUserID() {
        return PortalUserID;
    }

    public void setPortalUserID(String portalUserID) {
        PortalUserID = portalUserID;
    }

    public String getPortalLoginID() {
        return PortalLoginID;
    }

    public void setPortalLoginID(String portalLoginID) {
        PortalLoginID = portalLoginID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
