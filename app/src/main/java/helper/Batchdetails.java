package helper;

/**
 * Created by iyyapparajr on 5/5/2017.
 */
public class Batchdetails {



    int        BatchID;
    int     CoordinatingStaffID;
    String    AcademicYear;
    int     SchoolID;
    int     ClassID;
    int     StaffID;
    String     BatchStartDate;
    String    BatchEndDate;
    String    TimeTable;
    int    IsActive=0;
    String    CreatedOn;
    String    ModifiedOn;
    String Batchcode;

    public String getBatchcode() {
        return Batchcode;
    }

    public void setBatchcode(String batchcode) {
        Batchcode = batchcode;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getCoordinatingStaffID() {
        return CoordinatingStaffID;
    }

    public void setCoordinatingStaffID(int coordinatingStaffID) {
        CoordinatingStaffID = coordinatingStaffID;
    }

    public String getAcademicYear() {
        return AcademicYear;
    }

    public void setAcademicYear(String academicYear) {
        AcademicYear = academicYear;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getBatchStartDate() {
        return BatchStartDate;
    }

    public void setBatchStartDate(String batchStartDate) {
        BatchStartDate = batchStartDate;
    }

    public String getBatchEndDate() {
        return BatchEndDate;
    }

    public void setBatchEndDate(String batchEndDate) {
        BatchEndDate = batchEndDate;
    }

    public String getTimeTable() {
        return TimeTable;
    }

    public void setTimeTable(String timeTable) {
        TimeTable = timeTable;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
