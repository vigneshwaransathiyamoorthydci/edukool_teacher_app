package helper;

/**
 * Created by abimathi on 01-Nov-17.
 */
public class Violationpojo {
    String name="";
    String openapps="";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOpenapps() {
        return openapps;
    }

    public void setOpenapps(String openapps) {
        this.openapps = openapps;
    }
}
