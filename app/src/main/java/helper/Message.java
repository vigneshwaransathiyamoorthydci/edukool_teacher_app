package helper;

/**
 * Created by abimathi on 20-Apr-17.
 */
public abstract interface Message
{
    public abstract void onMessageReceived(String paramString);
}

