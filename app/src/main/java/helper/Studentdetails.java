package helper;

import java.util.ArrayList;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Studentdetails {



    int StudentID;
    String   AdmissionNumber ;
    String    DOA ;
    String    FirstName ;
    String    LastName ;
    String     DOB ;
    String      Gender ;
    String     Phone_1 ;
    String      Phone_2 ;
    String     Email ;
    String     FatherName ;
    String     MotherName ;
    String    GuardianMobileNumber ;
    String     RollNo ;
    int    ClassID ;
    int    BatchID ;
    int     SchoolID ;
    String      AcademicYear ;
    String     Guardians ;
    String     PhotoFilename ;
    int     PortalUserID ;
    String  StudentLoginID ;
    int     Version ;
    int     StaffID ;
    int     CreditPoints ;
    int     ConnectionStatus ;
    int      ManualAttendance ;
    int     NoOfPresent ;
    int      Credits;
    String     CreatedOn ;
    String     ModifiedOn ;
    boolean checked;
    String presentornot;
    int teachercredits=0;
    int studentcredits=0;
    int thismonthteachercredit=0;
    int thismonthcreditforstudent=0;
    ArrayList<String>listofpresent=new ArrayList<>();
    String roominfo;

    public String getRoominfo() {
        return roominfo;
    }

    public void setRoominfo(String roominfo) {
        this.roominfo = roominfo;
    }

    public ArrayList<String> getListofpresent() {
        return listofpresent;
    }

    public void setListofpresent(ArrayList<String> listofpresent) {
        this.listofpresent = listofpresent;
    }

    public int getThismonthteachercredit() {
        return thismonthteachercredit;
    }

    public void setThismonthteachercredit(int thismonthteachercredit) {
        this.thismonthteachercredit = thismonthteachercredit;
    }

    public int getThismonthcreditforstudent() {
        return thismonthcreditforstudent;
    }

    public void setThismonthcreditforstudent(int thismonthcreditforstudent) {
        this.thismonthcreditforstudent = thismonthcreditforstudent;
    }

    public int getTeachercredits() {
        return teachercredits;
    }

    public void setTeachercredits(int teachercredits) {
        this.teachercredits = teachercredits;
    }

    public int getStudentcredits() {
        return studentcredits;
    }

    public void setStudentcredits(int studentcredits) {
        this.studentcredits = studentcredits;
    }

    public String getPresentornot() {
        return presentornot;
    }

    public void setPresentornot(String presentornot) {
        this.presentornot = presentornot;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public String getAdmissionNumber() {
        return AdmissionNumber;
    }

    public void setAdmissionNumber(String admissionNumber) {
        AdmissionNumber = admissionNumber;
    }

    public String getDOA() {
        return DOA;
    }

    public void setDOA(String DOA) {
        this.DOA = DOA;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPhone_1() {
        return Phone_1;
    }

    public void setPhone_1(String phone_1) {
        Phone_1 = phone_1;
    }

    public String getPhone_2() {
        return Phone_2;
    }

    public void setPhone_2(String phone_2) {
        Phone_2 = phone_2;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getGuardianMobileNumber() {
        return GuardianMobileNumber;
    }

    public void setGuardianMobileNumber(String guardianMobileNumber) {
        GuardianMobileNumber = guardianMobileNumber;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public String getAcademicYear() {
        return AcademicYear;
    }

    public void setAcademicYear(String academicYear) {
        AcademicYear = academicYear;
    }

    public String getGuardians() {
        return Guardians;
    }

    public void setGuardians(String guardians) {
        Guardians = guardians;
    }

    public String getPhotoFilename() {
        return PhotoFilename;
    }

    public void setPhotoFilename(String photoFilename) {
        PhotoFilename = photoFilename;
    }

    public int getPortalUserID() {
        return PortalUserID;
    }

    public void setPortalUserID(int portalUserID) {
        PortalUserID = portalUserID;
    }

    public String getStudentLoginID() {
        return StudentLoginID;
    }

    public void setStudentLoginID(String studentLoginID) {
        StudentLoginID = studentLoginID;
    }

    public int getVersion() {
        return Version;
    }

    public void setVersion(int version) {
        Version = version;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public int getCreditPoints() {
        return CreditPoints;
    }

    public void setCreditPoints(int creditPoints) {
        CreditPoints = creditPoints;
    }

    public int getConnectionStatus() {
        return ConnectionStatus;
    }

    public void setConnectionStatus(int connectionStatus) {
        ConnectionStatus = connectionStatus;
    }

    public int getManualAttendance() {
        return ManualAttendance;
    }

    public void setManualAttendance(int manualAttendance) {
        ManualAttendance = manualAttendance;
    }

    public int getNoOfPresent() {
        return NoOfPresent;
    }

    public void setNoOfPresent(int noOfPresent) {
        NoOfPresent = noOfPresent;
    }

    public int getCredits() {
        return Credits;
    }

    public void setCredits(int credits) {
        Credits = credits;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
