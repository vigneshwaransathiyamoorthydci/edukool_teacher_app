package helper;

/**
 * Created by abimathi on 21-Apr-17.
 */
public class Studentinfo {
    String studentname;
    String studentip;
    String studentrollnumber;
    String studentschooid;
    String studentid;
    boolean setchecked;
    String setdatetime;
    boolean changebackground;
    boolean visibornot=true;
    String manual="1";
    String Studentnumber;
    int uploaded=0;
    int studentclassid;

    public int getStudentclassid() {
        return studentclassid;
    }

    public void setStudentclassid(int studentclassid) {
        this.studentclassid = studentclassid;
    }

    public int getUploaded() {
        return uploaded;
    }

    public void setUploaded(int uploaded) {
        this.uploaded = uploaded;
    }

    public String getStudentnumber() {
        return Studentnumber;
    }

    public void setStudentnumber(String studentnumber) {
        Studentnumber = studentnumber;
    }

    public String getManual() {
        return manual;
    }

    public void setManual(String manual) {
        this.manual = manual;
    }

    public boolean isVisibornot() {
        return visibornot;
    }

    public void setVisibornot(boolean visibornot) {
        this.visibornot = visibornot;
    }

    public boolean isChangebackground() {
        return changebackground;
    }

    public void setChangebackground(boolean changebackground) {
        this.changebackground = changebackground;
    }

    public String getSetdatetime() {
        return setdatetime;
    }

    public void setSetdatetime(String setdatetime) {
        this.setdatetime = setdatetime;
    }

    public boolean isSetchecked() {
        return setchecked;
    }

    public void setSetchecked(boolean setchecked) {
        this.setchecked = setchecked;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getStudentschooid() {
        return studentschooid;
    }

    public void setStudentschooid(String studentschooid) {
        this.studentschooid = studentschooid;
    }

    // "invS@Connect@IPAddress@RollNumber@schoolid@studentname"
    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname;
    }

    public String getStudentip() {
        return studentip;
    }

    public void setStudentip(String studentip) {
        this.studentip = studentip;
    }

    public String getStudentrollnumber() {
        return studentrollnumber;
    }

    public void setStudentrollnumber(String studentrollnumber) {
        this.studentrollnumber = studentrollnumber;
    }
}
