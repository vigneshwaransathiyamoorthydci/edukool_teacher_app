package helper;

/**
 * Created by abimathi on 01-Jun-17.
 */
public class Buzzerhelper {

    String buzzerid;
    String buzzername;
    String buzzerimage;
    boolean background;

    public boolean isBackground() {
        return background;
    }

    public void setBackground(boolean background) {
        this.background = background;
    }

    public String getBuzzerid() {
        return buzzerid;
    }

    public void setBuzzerid(String buzzerid) {
        this.buzzerid = buzzerid;
    }

    public String getBuzzername() {
        return buzzername;
    }

    public void setBuzzername(String buzzername) {
        this.buzzername = buzzername;
    }

    public String getBuzzerimage() {
        return buzzerimage;
    }

    public void setBuzzerimage(String buzzerimage) {
        this.buzzerimage = buzzerimage;
    }
}
