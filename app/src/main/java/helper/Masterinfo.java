package helper;

/**
 * Created by iyyapparajr on 5/5/2017.
 */
public class Masterinfo {


    int ID;
    String   SubjectName;
    String  Status;
    int   SchoolID;
    String  CreatedOn;
    String   ModifiedOn;
    String SubjectDescription;

    public String getSubjectDescription() {
        return SubjectDescription;
    }

    public void setSubjectDescription(String subjectDescription) {
        SubjectDescription = subjectDescription;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public int getSchoolID() {
        return SchoolID;
    }

    public void setSchoolID(int schoolID) {
        SchoolID = schoolID;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }
}
