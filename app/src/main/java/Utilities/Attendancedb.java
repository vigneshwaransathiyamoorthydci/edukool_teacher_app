package Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import helper.Attendancecredentialpojo;
import helper.Attendancepojo;
import helper.StudentAttendancecredenitpojo;

/**
 * Created by iyyapparajr on 5/13/2017.
 */
public class Attendancedb extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "attendance";

  /*  public Attendancedb(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }*/

   /* public Attendancedb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
*/
    public Attendancedb(Context context, String name, int version) {
        super(context, name, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String attendance="CREATE TABLE \"tblAttendance\"" +
                "("+
                "\"AttendanceID\"integer primary key autoincrement not null,"+
                "\"SchoolID\" integer,"+
                "\"BatchID\" integer,"+
                "\"SubjectID\" integer,"+
                "\"StaffID\" integer,"+
                "\"AttendanceDateTime\" datetime,"+
                "\"Credits\" integer,"+
                "\"StudentID\" integer,"+
                "\"RollNo\" integer,"+
                "\"StudName\" varchar,"+
                "\"Attendance\" varchar ,"+
                "\"ManualAttendance\" integer,"+
                "\"IsUploaded\" integer,"+
                "\"isnew\" integer,"+
                "\"CreatedOn\" datetime,"+
                "\"ModifiedOn\" datetime,"+
                "\"ClassID\" Integer,"+
                 "\"RoomID\" Integer"+")";
        db.execSQL(attendance);

        String attendancecredential="CREATE TABLE \"tblAttendanceCredits\"\n" +
                "(\n" +
                "\"AttendanceCreditID\" integer primary key autoincrement not null,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar,"+
                "\"RoomID\" Integer"+

                ")";

        db.execSQL(attendancecredential);
        String studentattendancecredits="CREATE TABLE \"tblStudentAttendanceCredits\"\n" +
                "(\n" +
                "\"StudentAttendanceCreditID\" integer primary key autoincrement not null,\n" +
                "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar,"+
                "\"RoomID\" Integer"+
                ")";

        db.execSQL(studentattendancecredits);

    }

public int gettotalcreditsforsubject(int batchid,int subjectid,int staffid)
{

    String selectQuery = "SELECT  * FROM tblAttendanceCredits where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
            +staffid+"'" ;


    // Attendancepojo details=new Attendancepojo();
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    if (cursor.moveToFirst()) {

        int credits=cursor.getInt(2);
        cursor.close();
        return  credits;

    }


    return 0;
}

    public int gettotalcreditsforsubjectforrooms(int roomid,int subjectid,int staffid)
    {

        String selectQuery = "SELECT  * FROM tblAttendanceCredits where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(2);
            cursor.close();
            return  credits;

        }


        return 0;
    }


    public int gettotalcreditsforsubjectthismonth(int batchid,int subjectid,int staffid,int month, int year)
    {

        String selectQuery = "SELECT  * FROM tblAttendanceCredits where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"'and Month= '"+month+"' and Year= '"+year+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(2);
            cursor.close();
            return  credits;

        }


        return 0;
    }

    public int gettotalcreditsforsubjectthismonthforrooms(int roomid,int subjectid,int staffid,int month, int year)
    {

        String selectQuery = "SELECT  * FROM tblAttendanceCredits where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"'and Month= '"+month+"' and Year= '"+year+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(2);
            cursor.close();
            return  credits;

        }


        return 0;
    }
    public int gettotalcreditsforsubjectstudent(int batchid,int subjectid,int staffid,int StudentID)
    {

        String selectQuery = "SELECT  * FROM tblStudentAttendanceCredits where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"'and StudentID = '"+StudentID+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(3);
            cursor.close();
            return  credits;

        }


        return 0;
    }
    public int gettotalcreditsforsubjectstudentforrooms(int roomid,int subjectid,int staffid,int StudentID)
    {

        String selectQuery = "SELECT  * FROM tblStudentAttendanceCredits where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"'and StudentID = '"+StudentID+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(3);
            cursor.close();
            return  credits;

        }


        return 0;
    }



    public int gettotalcreditsforsubjectstudentthismonth(int batchid,int subjectid,int staffid,int StudentID,int month,int year)
    {

        String selectQuery = "SELECT  * FROM tblStudentAttendanceCredits where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"'and StudentID = '"+StudentID+"'and Month= '"+month+"' and Year= '"+year+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(3);
            cursor.close();
            return  credits;

        }


        return 0;
    }


    public int gettotalcreditsforsubjectstudentthismonthforrooms(int roomid,int subjectid,int staffid,int StudentID,int month,int year)
    {

        String selectQuery = "SELECT  * FROM tblStudentAttendanceCredits where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"'and StudentID = '"+StudentID+"'and Month= '"+month+"' and Year= '"+year+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {

            int credits=cursor.getInt(3);
            cursor.close();
            return  credits;

        }


        return 0;
    }


    public void insertstudentcredits(StudentAttendancecredenitpojo details)
 {
     SQLiteDatabase db = this.getWritableDatabase();

     ContentValues values = new ContentValues();

     values.put("StudentID", details.getStudentID());
     values.put("Month" ,details.getMonth());
     values.put("Credits" ,details.getCredits());
     values.put("StaffID" ,details.getStaffID());
     values.put("SubjectID", details.getSubjectID());
     values.put("BatchID", details.getBatchID());
     values.put("IsPosted" ,details.getIsPosted());
     values.put("CreatedOn",details.getCreatedOn() );
     values.put("ModifiedOn",details.getModifiedOn() );
     values.put("Year", details.getYear());
     values.put("RoomID",details.getRoomid());
     db.insert("tblStudentAttendanceCredits", null, values);
     db.close();
 }
    public Attendancepojo studentpresentornot(int batchid,int subjectid,int staffid,String startdate,int studentid )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime ='" + startdate + "' and StudentID= '"+studentid+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                details.setManualAttendance(cursor.getInt(11));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setAttendance(cursor.getString(10));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }
    public Attendancepojo studentpresentornotforrooms(int roomid,int subjectid,int staffid,String startdate,int studentid )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime ='" + startdate + "' and StudentID= '"+studentid+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                details.setManualAttendance(cursor.getInt(11));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setAttendance(cursor.getString(10));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }


    public Attendancepojo studentpresentornotcheck(int batchid,int staffid,String startdate,int studentid )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where  BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime ='" + startdate + "' and StudentID= '"+studentid+"'" ;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                details.setManualAttendance(cursor.getInt(11));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setAttendance(cursor.getString(10));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }


public void updateattendance(Attendancepojo details)
{

    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues values = new ContentValues();

    values.put("SchoolID",details.getSchoolID() );
    values.put("BatchID" ,details.getBatchID());
   // values.put("SubjectID" ,details.getSubjectID());
    values.put("StaffID" ,details.getStaffID());
    values.put("AttendanceDateTime",details.getAttendanceDateTime() );
    values.put("Credits" ,details.getCredits());
    values.put("StudentID",details.getStudentID() );
   // values.put("RollNo",details.getRollNo() );
    values.put("StudName",details.getStudName() );
    values.put("Attendance" ,details.getAttendance());
    values.put("ManualAttendance",details.getManualAttendance() );
    values.put("IsUploaded" ,details.getIsUploaded());
    values.put("CreatedOn",details.getCreatedOn());
    values.put("ModifiedOn",details.getModifiedOn());

    db.update("tblAttendance",
            values,
            "RollNo" + " = ? AND " + "subjectid" + " = ?",
            new String[]{"" + details.getRollNo(), "" + details.getSubjectID()});
    db.close();

}

    public int getoverallcreditsforteacherthismonth(int month,int year,int staffid,int subjectid,int batchid)
    {
/*
        "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+*/
        SQLiteDatabase db = this.getReadableDatabase();


        String selectQuery = "SELECT   sum(Credits) FROM tblAttendanceCredits where Month= '"+month+"' and Year= '"+year+ "' and StaffID= '"
                +staffid+"' and SubjectID= '"+subjectid+"'and BatchID= '"+batchid+"'";
       Cursor c = db.rawQuery(selectQuery, null);
        int amount=0;
        if(c.moveToFirst())
             amount = c.getInt(0);
        else
            amount = -1;
        c.close();
        return  amount;


    }

    public  ArrayList<StudentAttendancecredenitpojo> getoverallcreditsforstudentthismonth(int month,int year,int staffid,int subjectid,int batchid)
    {


        ArrayList<StudentAttendancecredenitpojo>s_pojo=new ArrayList<>();
      /*  "\"StudentAttendanceCreditID\" integer primary key autoincrement not null,\n" +
                "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+*/
        SQLiteDatabase db = this.getReadableDatabase();


      /*  String selectQuery = "SELECT    credits_Stu.StudentID,credits_Stu.BatchID,credits_Stu.StaffID,credits_Stu.SubjectID,attendance.RollNo,attendance.StudName FROM tblStudentAttendanceCredits credits_Stu " +
                "left join tblAttendance attendance on credits_Stu.StudentID= attendance.StudentID where credits_Stu.Month= '"+month+"' and credits_Stu.Year= '"+year+ "' and credits_Stu.StaffID= '"
                +staffid+"' and credits_Stu.SubjectID= '"+subjectid+"'and credits_Stu.BatchID= '"+batchid+"'";;*/
      /* String selectQuery = "SELECT   sum(Credits) FROM tblStudentAttendanceCredits ";

                " where Month= '"+month+"' and Year= '"+year+ "' and StaffID= '"
                +staffid+"' and SubjectID= '"+subjectid+"'and BatchID= '"+batchid+"'and StudentID="''*/

       // Cursor c = db.rawQuery(selectQuery, null);
       /* int amount;
        if(c.moveToFirst())
             amount = c.getInt(0);*/
/*
       if(c.moveToFirst())
        do {
Log.e("name",c.getString(5));
        }while (c.moveToNext());*/
      /*  int amount=0;
        if(c.moveToFirst())
            amount = c.getInt(0);
        else
            amount = -1;
        c.close();*/
        return  s_pojo;


    }


 public int getoverallcreditteacher(int subjectid,int staffid,int year,int batchid)
    {

        SQLiteDatabase db = this.getReadableDatabase();


        String selectQuery = "SELECT   sum(Credits) FROM tblAttendanceCredits where Year= '"+year+ "' and StaffID= '"
                +staffid+"' and SubjectID= '"+subjectid+"'and BatchID= '"+batchid+"'";
        Cursor c = db.rawQuery(selectQuery, null);
        int amount=0;
        if(c.moveToFirst())
            amount = c.getInt(0);
        else
            amount = -1;
        c.close();
        return  amount;

    }



  //  where Date BETWEEN '" + startDateQueryDate + "' AND '" + endDateQueryDate +


    public boolean alereadytakeattendance(int batchid,int subjectid,int staffid,String startdate,String endate,int classid )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"' and ClassID ="+classid;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public boolean alereadytakeattendanceforroom(int roomid,int subjectid,int staffid,String startdate,String endate )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"'";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
    public Attendancepojo alereadytakeattendancelatestrecord(int batchid,int subjectid,int staffid,String startdate,String endate,int classid )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"' and ClassID ='"+ classid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"'"+" ORDER BY AttendanceDateTime DESC LIMIT 1";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));
                details.setIsUploaded(cursor.getInt(12));
                details.setIsnew(cursor.getInt(13));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }


    public Attendancepojo alereadytakeattendancelatestrecordforroom(int roomid,int subjectid,int staffid,String startdate,String endate )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and RoomID= '"+roomid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"'"+" ORDER BY AttendanceDateTime DESC LIMIT 1";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));
                details.setIsUploaded(cursor.getInt(12));
                details.setIsnew(cursor.getInt(13));
                details.setRoomid(cursor.getInt(cursor.getColumnIndex("RoomID")));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }


    ///list ofdate


public String getpresentornot(int studentid,String searchdate)
{
   /* "\"AttendanceID\"integer primary key autoincrement not null,"+
            "\"SchoolID\" integer,"+
            "\"BatchID\" integer,"+
            "\"SubjectID\" integer,"+
            "\"StaffID\" integer,"+
            "\"AttendanceDateTime\" datetime,"+
            "\"Credits\" integer,"+
            "\"StudentID\" integer,"+
            "\"RollNo\" integer,"+
            "\"StudName\" varchar,"+
            "\"Attendance\" varchar ,"+
            "\"ManualAttendance\" integer,"+
            "\"IsUploaded\" integer,"+
            "\"CreatedOn\" datetime,"+
            "\"ModifiedOn\" datetime)";*/
    String selectquery="select Attendance from tblAttendance where StudentID='"+studentid+"' and AttendanceDateTime='"+searchdate+"'";

    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectquery, null);
    if (cursor.moveToFirst()) {
        String report = cursor.getString(0);
        cursor.close();
        return report;
    }
    return "A";

}


    public ArrayList<Attendancepojo> todayatttendance(int batchid,int staffid,String startdate )
    {

        String selectQuery = "SELECT  distinct(AttendanceDateTime) FROM tblAttendance  where BatchID= '"+batchid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + startdate+ " 23:59:59" +"'";

        ArrayList<Attendancepojo> today=new ArrayList<Attendancepojo>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();

              /*  details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));*/
                details.setAttendanceDateTime(cursor.getString(0));

                // details.setClassid(cursor.getInt(cursor.getColumnIndex("ClassID")));
               /* details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));*/
                today.add(details);

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  today;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }

    public ArrayList<Attendancepojo> todayatttendanceforrooms(int roomsid,int staffid,String startdate )
    {

        String selectQuery = "SELECT  distinct(AttendanceDateTime) FROM tblAttendance  where RoomID= '"+roomsid+ "' and StaffID= '"
                +staffid+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + startdate+ " 23:59:59" +"'";

        ArrayList<Attendancepojo> today=new ArrayList<Attendancepojo>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();

              /*  details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));*/
                details.setAttendanceDateTime(cursor.getString(0));

                // details.setClassid(cursor.getInt(cursor.getColumnIndex("ClassID")));
               /* details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));*/
                today.add(details);

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  today;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }

///get unpostedattendance and load

    public ArrayList<Attendancepojo> uploadtoserver( String datetimestr)
    {

        String selectQuery = "SELECT  distinct(ClassID) FROM tblAttendance where AttendanceDateTime = '"+datetimestr+"'";


        ArrayList<Attendancepojo> today=new ArrayList<Attendancepojo>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();

              /*  details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));*/
                details.setClassid(cursor.getInt(0));
               /* details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));*/
                today.add(details);

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  today;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }



public ArrayList<Attendancepojo> uploadtoserver(int batchid,int subjectid )
{

    String selectQuery = "SELECT  distinct(AttendanceDateTime) FROM tblAttendance where (IsUploaded=0 and isnew=1 and BatchID='"+batchid+"'and SubjectID = '"+subjectid+"')"+ "or (IsUploaded=1 and isnew=0 and BatchID='"+batchid+"'and SubjectID = '"+subjectid+"')";


    ArrayList<Attendancepojo> today=new ArrayList<Attendancepojo>();
    // Attendancepojo details=new Attendancepojo();
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);


    if (cursor.moveToFirst()) {
        do {
            Attendancepojo details=new Attendancepojo();

              /*  details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));*/
            details.setAttendanceDateTime(cursor.getString(0));
               /* details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));*/
            today.add(details);

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


        } while (cursor.moveToNext());
    }
    db.close();
    return  today;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
}

    public ArrayList<Attendancepojo> uploadtoservertoall( )
    {

        String selectQuery = "SELECT  distinct(AttendanceDateTime) FROM tblAttendance where (IsUploaded=0 and isnew=1 )"+ "or (IsUploaded=1 and isnew=0 )";


        ArrayList<Attendancepojo> today=new ArrayList<Attendancepojo>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();

              /*  details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));*/
                details.setAttendanceDateTime(cursor.getString(0));
               /* details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));*/
                today.add(details);

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  today;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }
//geting studentid


    public ArrayList<Attendancepojo> getuploadattendforstudents(String date1 ,int classid)
    {

        String selectQuery = "SELECT  * FROM tblAttendance where AttendanceDateTime = '"+date1+"' and ClassID ="+classid;

        ArrayList<Attendancepojo>getdetails=new ArrayList<>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);



        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();
                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));
                details.setIsUploaded(cursor.getInt(12));
                details.setIsnew(cursor.getInt(13));
                details.setAttendance(cursor.getString(10));
                details.setClassid(cursor.getInt(cursor.getColumnIndex("ClassID")));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/
                getdetails.add(details);


            } while (cursor.moveToNext());
        }
        db.close();
        return  getdetails;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }




    public ArrayList<Attendancepojo> getuploadattendforstudents(String date1 )
    {

        String selectQuery = "SELECT  * FROM tblAttendance where AttendanceDateTime = '"+date1+"'";

        ArrayList<Attendancepojo>getdetails=new ArrayList<>();
        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);



        if (cursor.moveToFirst()) {
            do {
                Attendancepojo details=new Attendancepojo();
                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));
                details.setIsUploaded(cursor.getInt(12));
                details.setIsnew(cursor.getInt(13));
                details.setAttendance(cursor.getString(10));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/
                getdetails.add(details);


            } while (cursor.moveToNext());
        }
        db.close();
        return  getdetails;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }



    public Attendancepojo alereadytakeattendancelatestrecordindividual(int batchid,int subjectid,int staffid,String startdate,int studentid ,String endate)
    {
       /* "\"AttendanceID\"integer primary key autoincrement not null,"+
                "\"SchoolID\" integer,"+
                "\"BatchID\" integer,"+


                "\"SubjectID\" integer,"+
                "\"StaffID\" integer,"+
                "\"AttendanceDateTime\" datetime,"+
                "\"Credits\" integer,"+
                "\"StudentID\" integer,"+
                "\"RollNo\" integer,"+
                "\"StudName\" varchar,"+
                "\"Attendance\" varchar ,"+
                "\"ManualAttendance\" integer,"+
                "\"IsUploaded\" integer,"+
                "\"CreatedOn\" datetime,"+
                "\"ModifiedOn\" datetime)";*/


        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and  BatchID= '"+batchid+ "' and  StaffID= '"
                +staffid+"' and  StudentID= '"+studentid+"'";/*+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"' ORDER BY AttendanceDateTime DESC LIMIT 1";*/


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                 details.setAttendance(cursor.getString(10));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));


              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }

    public Attendancepojo alereadytakeattendancelatestrecordindividualforrooms(int roomid,int subjectid,int staffid,String startdate,int studentid ,String endate)
    {
       /* "\"AttendanceID\"integer primary key autoincrement not null,"+
                "\"SchoolID\" integer,"+
                "\"BatchID\" integer,"+


                "\"SubjectID\" integer,"+
                "\"StaffID\" integer,"+
                "\"AttendanceDateTime\" datetime,"+
                "\"Credits\" integer,"+
                "\"StudentID\" integer,"+
                "\"RollNo\" integer,"+
                "\"StudName\" varchar,"+
                "\"Attendance\" varchar ,"+
                "\"ManualAttendance\" integer,"+
                "\"IsUploaded\" integer,"+
                "\"CreatedOn\" datetime,"+
                "\"ModifiedOn\" datetime)";*/


        String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and  RoomID= '"+roomid+ "' and  StaffID= '"
                +staffid+"' and  StudentID= '"+studentid+"'";/*+"' and AttendanceDateTime BETWEEN '" + startdate + "' AND '" + endate +"' ORDER BY AttendanceDateTime DESC LIMIT 1";*/


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Attendancepojo details=new Attendancepojo();

        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
                details.setAttendance(cursor.getString(10));
                details.setStudName(cursor.getString(9));
                details.setManualAttendance(cursor.getInt(11));
                details.setRoomid(cursor.getInt(cursor.getColumnIndex("RoomID")));


              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return  details;


       /* if(cursor.getCount()>0)
            return true;
        else
            return false;*/
    }


   // ORDER BY column DESC LIMIT 1;

    public boolean checkcreditsforthismont(int month,int subjectid,int staffid,int batchid ,String year)
    {
       // StaffID SubjectID BatchID
       String selectQuery="select * from tblAttendanceCredits where Month='"+month+"' and StaffID ='"
               +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' and Year ='"+year+"'";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }

    public boolean checkcreditsforthismontforrooms(int month,int subjectid,int staffid,int roomid ,String year)
    {
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and RoomID = '"+roomid+"' and Year ='"+year+"'";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }

//student
    public boolean checkcreditsforthismontstudent(int month,int subjectid,int staffid,int batchid,int studentid,String year)
    {

       /* "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+
                ")";*/
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblStudentAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' and Year ='"+year+"' and StudentID='"+studentid+"'";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }


    public boolean checkcreditsforthismontstudentforroom(int month,int subjectid,int staffid,int studentid,String year,int roomid)
    {

       /* "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+
                ")";*/
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblStudentAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and Year ='"+year+"' and StudentID='"+studentid+"' and RoomID="+roomid;


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
    public StudentAttendancecredenitpojo getstudentcreditsrecord(int month,int subjectid,int staffid,int batchid ,int studentid ,String year)
    {

        /* "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+
                ")";*/
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblStudentAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' and Year ='"+year+"' and StudentID='"+studentid+"'";


        StudentAttendancecredenitpojo details=new StudentAttendancecredenitpojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            details.setStudentID(cursor.getInt(1));
            details.setMonth(cursor.getInt(2));
            details.setCredits(cursor.getInt(3));
            details.setStaffID(cursor.getInt(4));
            details.setSubjectID(cursor.getInt(5));
            details.setBatchID(cursor.getInt(6));
            details.setIsPosted(cursor.getInt(7));
            cursor.close();
            db.close();
            return details;


        }
        return  details;


    }


    public StudentAttendancecredenitpojo getstudentcreditsrecordforroomss(int month,int subjectid,int staffid,int studentid ,String year,int roomid)
    {

        /* "\"StudentID\" integer,\n" +
                "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+
                ")";*/
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblStudentAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and Year ='"+year+"' and StudentID='"+studentid+"'and RoomID ="+roomid;


        StudentAttendancecredenitpojo details=new StudentAttendancecredenitpojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            details.setStudentID(cursor.getInt(1));
            details.setMonth(cursor.getInt(2));
            details.setCredits(cursor.getInt(3));
            details.setStaffID(cursor.getInt(4));
            details.setSubjectID(cursor.getInt(5));
            details.setBatchID(cursor.getInt(6));
            details.setIsPosted(cursor.getInt(7));
            cursor.close();
            db.close();
            return details;


        }
        return  details;


    }
    public  boolean updatecreditstostudentforthismonth(int month,int subjectid,int staffid,int batchid ,int studentid,String year,StudentAttendancecredenitpojo details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Month" ,details.getMonth());
        values.put("Credits",details.getCredits() );
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID",details.getSubjectID() );
        values.put("BatchID",details.getBatchID() );
        values.put("IsPosted",details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn());
      //  values.put("Year", details.getYear());

        int updatequery= db.update("tblStudentAttendanceCredits",
                values,
                "Month" + " = ? AND " + "SubjectID" + " = ? AND " + "BatchID" + " = ? AND " + "StaffID" + " = ? AND " + "StudentID" + " = ? AND " + "Year" + " = ?",
                new String[]{"" + month, "" + subjectid, "" + batchid, "" + staffid, "" + studentid, year});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }

       /* if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
           cursor.close();
            db.close();
            return details;


        }*/




        // return  details;
    }
    public  boolean updatecreditstostudentforthismonthforroom(int month,int subjectid,int staffid,int roomid ,int studentid,String year,StudentAttendancecredenitpojo details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Month" ,details.getMonth());
        values.put("Credits",details.getCredits() );
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID",details.getSubjectID() );
        values.put("BatchID",details.getBatchID() );
        values.put("IsPosted",details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn());
        //  values.put("Year", details.getYear());

        int updatequery= db.update("tblStudentAttendanceCredits",
                values,
                "Month" + " = ? AND " + "SubjectID" + " = ? AND " + "RoomID" + " = ? AND " + "StaffID" + " = ? AND " + "StudentID" + " = ? AND " + "Year" + " = ?",
                new String[]{"" + month, "" + subjectid, "" + roomid, "" + staffid, "" + studentid, year});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }

       /* if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
           cursor.close();
            db.close();
            return details;


        }*/




        // return  details;
    }


    public Attendancecredentialpojo getteachercreditsrecord(int month,int subjectid,int staffid,int batchid ,String year)
    {
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' and Year ='"+year+"'";


         Attendancecredentialpojo details=new Attendancecredentialpojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

         if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
           cursor.close();
            db.close();
            return details;


        }
        return  details;


    }
    public Attendancecredentialpojo getteachercreditsrecordforrooms(int month,int subjectid,int staffid,int roomid ,String year)
    {
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and RoomID = '"+roomid+"' and Year ='"+year+"'";


        Attendancecredentialpojo details=new Attendancecredentialpojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
            details.setRoomid(cursor.getInt(cursor.getColumnIndex("RoomID")));
            cursor.close();
            db.close();
            return details;


        }
        return  details;


    }


  //  public int getisuploadt(int batchid,int staffid,int subjectid,int studentid,String datetime)

    public  boolean updatecreditstoteacherforthismonth(int month,int subjectid,int staffid,int batchid ,String year,Attendancecredentialpojo details)
    {
        // StaffID SubjectID BatchID
       /* String selectQuery="select * from Credits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' Year ='"+year+"'";
*/

        // Attendancepojo details=new Attendancepojo();
        /*SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Attendancecredentialpojo details=new Attendancecredentialpojo();*/
/*
        "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+*/

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Month" ,details.getMonth());
        values.put("Credits",details.getCredits() );
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID",details.getSubjectID() );
        values.put("BatchID",details.getBatchID() );
        values.put("IsPosted",details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn());
      //  values.put("Year", details.getYear());

        int updatequery= db.update("tblAttendanceCredits",
                values,
                "Month" + " = ? AND " + "SubjectID" + " = ? AND " + "BatchID" + " = ? AND " + "StaffID" + " = ? AND " + "Year" + " = ?",
                new String[]{""+month, ""+subjectid,""+batchid,""+staffid,year});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }

       /* if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
           cursor.close();
            db.close();
            return details;


        }*/




       // return  details;
    }



    public  boolean updatecreditstoteacherforthismonthforrooms(int month,int subjectid,int staffid,int roomid ,String year,Attendancecredentialpojo details)
    {
        // StaffID SubjectID BatchID
       /* String selectQuery="select * from Credits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' Year ='"+year+"'";
*/

        // Attendancepojo details=new Attendancepojo();
        /*SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        Attendancecredentialpojo details=new Attendancecredentialpojo();*/
/*
        "\"Month\" integer,\n" +
                "\"Credits\" integer,\n" +
                "\"StaffID\" integer,\n" +
                "\"SubjectID\" integer,\n" +
                "\"BatchID\" integer,\n" +
                "\"IsPosted\" integer,\n" +
                "\"CreatedOn\" datetime,\n" +
                "\"ModifiedOn\" datetime, \n" +
                "\"Year\" varchar"+*/

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Month" ,details.getMonth());
        values.put("Credits",details.getCredits() );
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID",details.getSubjectID() );
        values.put("BatchID",details.getBatchID() );
        values.put("IsPosted",details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn());
        //  values.put("Year", details.getYear());

        int updatequery= db.update("tblAttendanceCredits",
                values,
                "Month" + " = ? AND " + "SubjectID" + " = ? AND " + "RoomID" + " = ? AND " + "StaffID" + " = ? AND " + "Year" + " = ?",
                new String[]{""+month, ""+subjectid,""+roomid,""+staffid,year});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }

       /* if (cursor.moveToFirst()) {
            details.setMonth(cursor.getInt(1));
            details.setCredits(cursor.getInt(2));
            details.setStaffID(cursor.getInt(3));
            details.setSubjectID(cursor.getInt(4));
            details.setBatchID(cursor.getInt(5));
            details.setIsPosted(cursor.getInt(6));
           cursor.close();
            db.close();
            return details;


        }*/




        // return  details;
    }


    public boolean checkcreditsforstudentthismonth(int month,int subjectid,int studentid,int batchid,int staffid,String year )
    {
        // StaffID SubjectID BatchID
        String selectQuery="select * from tblAttendanceCredits where Month='"+month+"' and StaffID ='"
                +staffid+"' and SubjectID='"+subjectid+"' and BatchID = '"+batchid+"' and  StudentID='"+studentid+"'";


        // Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
//get particular student id

    public int uplooaddateforstudent(int batchid,int subjectid,int staffid,String datestring ,Attendancepojo details)
    {

        int isupload=0;
        String selectQuery = "SELECT  * FROM tblAttendance where StaffID= '"+staffid+
                "' and SubjectID= '"+subjectid+"' and BatchID= '"+batchid+"' and AttendanceDateTime='"+datestring+"'";


        //Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {


            isupload=cursor.getInt(12);
            Log.e("int","int"+cursor.getInt(13));
            cursor.close();
            return isupload;

        }
        /*SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        // values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        // values.put("AttendanceDateTime",details.getAttendanceDateTime() );
        values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        // values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance", details.getManualAttendance());
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        values.put("isnew",details.getIsnew());
        int updatequery= db.update("tblAttendance",
                values,
                "BatchID" + " = ? AND " + "subjectid" + " = ? AND " + "StaffID" + " = ? AND " + "AttendanceDateTime" +  " = ? AND  "+" StudentID = ?",
                new String[]{""+batchid, ""+subjectid,""+staffid,datestring,""+details.getStudentID()});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }*/


return  0;
    }


    public int uplooaddateforstudentforroom(int batchid,int subjectid,int staffid,String datestring ,Attendancepojo details)
    {

        int isupload=0;
        String selectQuery = "SELECT  * FROM tblAttendance where StaffID= '"+staffid+
                "' and SubjectID= '"+subjectid+"' and BatchID= '"+batchid+"' and AttendanceDateTime='"+datestring+"'";


        //Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {


            isupload=cursor.getInt(12);
            Log.e("int","int"+cursor.getInt(13));
            cursor.close();
            return isupload;

        }
        /*SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        // values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        // values.put("AttendanceDateTime",details.getAttendanceDateTime() );
        values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        // values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance", details.getManualAttendance());
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        values.put("isnew",details.getIsnew());
        int updatequery= db.update("tblAttendance",
                values,
                "BatchID" + " = ? AND " + "subjectid" + " = ? AND " + "StaffID" + " = ? AND " + "AttendanceDateTime" +  " = ? AND  "+" StudentID = ?",
                new String[]{""+batchid, ""+subjectid,""+staffid,datestring,""+details.getStudentID()});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }*/


        return  0;
    }



    public boolean updateattendancedb(int batchid,int subjectid,int staffid,String datestring ,Attendancepojo details)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        // values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        values.put("AttendanceDateTime",details.getAttendanceDateTime() );
       // values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        // values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance", details.getManualAttendance());
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        values.put("isnew",details.getIsnew());
      int updatequery= db.update("tblAttendance",
                values,
                "BatchID" + " = ? AND " + "subjectid" + " = ? AND " + "StaffID" + " = ? AND " + "AttendanceDateTime" +  " = ? AND  "+" StudentID = ?",
                new String[]{""+batchid, ""+subjectid,""+staffid,datestring,""+details.getStudentID()});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }



    }


    public boolean updateattendancedbforroom(int roomid,int subjectid,int staffid,String datestring ,Attendancepojo details)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        // values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        values.put("AttendanceDateTime",details.getAttendanceDateTime() );
        // values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        // values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance", details.getManualAttendance());
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        values.put("isnew",details.getIsnew());
        int updatequery= db.update("tblAttendance",
                values,
                "RoomID" + " = ? AND " + "subjectid" + " = ? AND " + "StaffID" + " = ? AND " + "AttendanceDateTime" +  " = ? AND  "+" StudentID = ?",
                new String[]{""+roomid, ""+subjectid,""+staffid,datestring,""+details.getStudentID()});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }



    }
    public boolean updatepostedattendance(String date)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("IsUploaded" ,1);
        values.put("isnew" ,1);

        int updatequery= db.update("tblAttendance",
                values,
                 "AttendanceDateTime = ?" ,
                new String[]{""+date});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }



    }


    public boolean updatepostedattendance(int batchid,int subjectid,String date)
    {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("IsUploaded" ,1);
        values.put("isnew" ,1);

        int updatequery= db.update("tblAttendance",
                values,
                "BatchID" + " = ? AND " + "subjectid" + " = ?  AND " + "AttendanceDateTime = ?" ,
                new String[]{""+batchid, ""+subjectid,""+date});
        db.close();

        if(updatequery>0)
        {
            return true;
        }
        else {
            return false;
        }



    }




    public boolean attendancecheckpojo(int rollno,int batchid,int subjectid )
{

    String selectQuery = "SELECT  * FROM tblAttendance where SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and RollNo= '"+rollno+"'";


   // Attendancepojo details=new Attendancepojo();
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    if(cursor.getCount()>0)
        return true;
        else
        return false;
}


    public void attendancetaken(Attendancepojo details)
    {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        values.put("AttendanceDateTime",details.getAttendanceDateTime() );
        values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance",details.getManualAttendance() );
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("isnew",details.getIsnew());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        values.put("ClassID",details.getClassid());
        values.put("RoomID",details.getRoomid());
        db.insert("tblAttendance", null, values);
        db.close();
    }


    public Attendancepojo getdetails(int rollnumber,int subjectid,int batchid,int staffid)
    {
        String selectQuery = "SELECT  * FROM tblAttendance where StaffID= '"+staffid+
                "' and SubjectID= '"+subjectid+"' and BatchID= '"+batchid+ "' and RollNo= '"+rollnumber+"'";


       Attendancepojo details=new Attendancepojo();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(1));
                details.setBatchID(cursor.getInt(2));
                details.setSubjectID(cursor.getInt(3));
                details.setStaffID(cursor.getInt(4));
                details.setAttendanceDateTime(cursor.getString(5));
                details.setCredits(cursor.getInt(6));
                details.setStudentID(cursor.getInt(7));
                details.setRollNo(cursor.getInt(8));
               // details.setAttendance(cursor.getInt(1));
                details.setStudName(cursor.getString(9));

              /*  values.put("SchoolID",details.getSchoolID() );
                values.put("BatchID" ,details.getBatchID());
                values.put("SubjectID" ,details.getSubjectID());
                values.put("StaffID" ,details.getStaffID());
                values.put("AttendanceDateTime",details.getAttendanceDateTime() );
                values.put("Credits" ,details.getCredits());
                values.put("StudentID",details.getStudentID() );
                values.put("RollNo",details.getRollNo() );
                values.put("StudName",details.getStudName() );
                values.put("Attendance" ,details.getAttendance());
                values.put("ManualAttendance",details.getManualAttendance() );
                values.put("IsUploaded" ,details.getIsUploaded());
                values.put("CreatedOn",details.getCreatedOn());
                values.put("ModifiedOn",details.getModifiedOn() );*/


            } while (cursor.moveToNext());
        }
        db.close();
        return  details;

    }



    public void attendancecredetinal(Attendancecredentialpojo details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("Month" ,details.getMonth());
        values.put("Credits",details.getCredits() );
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID",details.getSubjectID() );
        values.put("BatchID",details.getBatchID() );
        values.put("IsPosted",details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn());
        values.put("Year",details.getYear());
        values.put("RoomID",details.getRoomid());
        db.insert("tblAttendanceCredits", null, values);
        db.close();
    }

    public void studentattendace(StudentAttendancecredenitpojo details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("StudentID", details.getStudentID());
        values.put("Month" ,details.getMonth());
        values.put("Credits" ,details.getCredits());
        values.put("StaffID" ,details.getStaffID());
        values.put("SubjectID", details.getSubjectID());
        values.put("BatchID", details.getBatchID());
        values.put("IsPosted" ,details.getIsPosted());
        values.put("CreatedOn",details.getCreatedOn() );
        values.put("ModifiedOn",details.getModifiedOn() );
        db.insert("tblStudentAttendanceCredits", null, values);
        db.close();
    }






    public void insertrollnumber(Attendancepojo details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put("SchoolID",details.getSchoolID() );
        values.put("BatchID" ,details.getBatchID());
        values.put("SubjectID" ,details.getSubjectID());
        values.put("StaffID" ,details.getStaffID());
        values.put("AttendanceDateTime",details.getAttendanceDateTime() );
        values.put("Credits" ,details.getCredits());
        values.put("StudentID",details.getStudentID() );
        values.put("RollNo",details.getRollNo() );
        values.put("StudName",details.getStudName() );
        values.put("Attendance" ,details.getAttendance());
        values.put("ManualAttendance",details.getManualAttendance() );
        values.put("IsUploaded" ,details.getIsUploaded());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn() );
        db.insert("tblAttendance", null, values);
        db.close();
    }





    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS tblAttendance" );
       db.execSQL("DROP TABLE IF EXISTS tblAttendanceCredits" );
        db.execSQL("DROP TABLE IF EXISTS tblStudentAttendanceCredits" );

       /* db.execSQL("DROP TABLE IF EXISTS " );
        db.execSQL("DROP TABLE IF EXISTS " );*/
    }
}
