package Utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import calendar.Calendarpojo;
import exam.ExamDetails;
import exam.ExamResponsePojo;
import exam.PulseResponsePojo;
import exam.QuestionDetails;
import exam.StudentQuestionResultPojo;
import helper.Batchdetails;
import helper.Masterinfo;
import helper.Rooms;
import helper.Staffdetails;
import helper.Staffloingdetails;
import helper.Subjectnameandid;
import helper.Tablecontent;
import helper.tblclasses;
import pulseexam.PulseExamPojo;
import pulseexam.PulseSurveyPojo;

/**
 * Created by iyyapparajr on 5/4/2017.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    public  static final int DATABASE_VERSION = 2;

    // Database Name
    private static final String DATABASE_NAME = "staff";

    // Contacts table name
    private static final String Table_school = "tblSchool";

    // Contacts Table Columns names
//staffdetails pojo
    public static final String school_id= "SchoolID" ;
    public static final String SchoolCategoryID=  "SchoolCategoryID" ;
    public static final String SchoolName= "SchoolName";
    public static final String Acronym=   "Acronym";
    public static final String SchoolLogo= "SchoolLogo" ;
    public static final String CityID=   "CityID" ;
    public static final String StateID= "StateID" ;
    public static final String CountryID=   "CountryID" ;
    public static final String CreatedOn=   "CreatedOn";
    public static final String ModifiedOn=  "ModifiedOn";
    public static final String Background="Background";

    //stafflogindetails pojo
    private static final String Tablestaff= "tblStaff";

    public static final String   StaffID=  "StaffID" ;
    public static final String   EmployementNumber=   "EmployementNumber" ;
    public static final String      DOJ= "DOJ" ;
    public static final String      FirstName= "FirstName";
    public static final String      MiddleName= "MiddleName";
    public static final String     LastName= "LastName" ;
    public static final String     Gender= "Gender";
    public static final String     DOB= "DOB" ;
    public static final String     MaritalStatusID="MaritalStatusID" ;
    public static final String    SpouseName= "SpouseName" ;
    public static final String   FatherName=  "FatherName" ;
    public static final String    MotherName= "MotherName" ;
    public static final String    Phone=  "Phone" ;
    public static final String    Email=  "Email" ;
    public static final String     PhotoFilename=  "PhotoFilename" ;
    public static final String     StaffCategoryID= "StaffCategoryID" ;
    public static final String    SchoolID=  "SchoolID";
    public static final String      DeptartmentID= "DeptartmentID" ;
    public static final String    DesignationID=  "DesignationID" ;
    public static final String     PortalUserID= "PortalUserID" ;
    public static final String    PortalLoginID=  "PortalLoginID" ;
    public static final String    tablestaffCreatedOn= "CreatedOn" ;
    public static final String    tablestaffModifiedOn= "ModifiedOn";
    public static final String    tableroominfo= "room";
    public static final String timetable="Timetable";


//tablclass pojo

    public static final String tblClasses="tblClasses";

    public static final String  ClassID="ClassID";
    public static final String   ClassName= "ClassName" ;
    public static final String   SectionName ="SectionName";
    public static final String    Grade ="Grade";
    public static final String   ClassCode = "ClassCode";
    public static final String   tblclassSchoolID = "SchoolID";
    public static final String    DepartmentID= "DepartmentID";
    public static final String  tblclassStaffID  ="StaffID" ;
    public static final String  InternetSSID  ="InternetSSID";
    public static final String   InternetPassword ="InternetPassword" ;
    public static final String    InternetType= "InternetType" ;
    public static final String     PushName="PushName" ;
    public static final String    tblclassesCreatedOn ="CreatedOn" ;
    public static final String   tblclassesModifiedOn="ModifiedOn" ;
    public static final String classesBatchID="batchid";//for futrue purpose


//tblsubject pojo

    public static final String  tblSubject="tblSubject";

    public static final String    SubjectID  = "SubjectID" ;
    public static final String    SubjectName  =    "SubjectName"  ;
    public static final String    tblSubjectSchoolID  =    "SchoolID"  ;
    public static final String     tblSubjectStaffID =  "StaffID"  ;
    public static final String     tblSubjectClassID =  "ClassID"  ;
    public static final String      BatchID =  "BatchID" ;
    public static final String      tblsubectCreatedOn=  "CreatedOn" ;
    public static final String      tblsubectModifiedOn = "ModifiedOn" ;



    //Pratheeba Sekaran Tables 10th may
//ExamDetails
    private static final String TABLE_EXAM_DETAILS = "ExamDetails";
    private static final String EXAM_ID = "ExamID";
    private static final String STAFF_ID = "StaffID";
    private static final String EXAM__CATEGORY_ID = "ExamCategoryID";
    private static final String EXAM_CATEGORY_NAME = "ExamCategoryName";
    private static final String EXAM_CODE = "ExamCode";
    private static final String EXAM_DESCRIPTION = "ExamDescription";
    private static final String EXAM_SEQUENCE = "ExamSequence";
    private static final String EXAM_DATE = "ExamDate";
    private static final String EXAM_TYPE_ID = "ExamTypeID";
    private static final String EXAM_TYPE = "ExamType";
    private static final String SUBJECT_ID = "SubjectID";
    private static final String SUBJECT = "Subject";
    private static final String EXAM_DURATION = "ExamDuration";
    private static final String SCHOOL_ID = "SchoolID";
    private static final String CLASS_ID = "ClassID";
    private static final String BATCH_ID = "BatchID";
    private static final String IS_RESULT_PUBLISHED = "IsResultPublished";
    private static final String EXAM_SHELF_ID = "ExamShelfID";
    private static final String TIME_TAKEN = "TimeTaken";
    private static final String DATE_ATTENDED = "DateAttended";
    private static final String TOTAL_SCORE = "TotalScore";

    //tblPulseQuestion


    private static final String TABLE_PULSE_QUESTION= "tblPulseQuestion";

    public static final String   PulseQuestionID=  "PulseQuestionID" ;
    public static final String   PulseQuestion=   "PulseQuestion" ;
    public static final String      PulseType= "PulseType" ;
    public static final String      AnswerOption_A= "AnswerOption_A";
    public static final String      AnswerOption_B= "AnswerOption_B";
    public static final String     AnswerOption_C= "AnswerOption_C" ;
    public static final String     AnswerOption_D= "AnswerOption_D";
    public static final String     CorrectAnswerOption= "CorrectAnswerOption" ;
    public static final String     VersionPulse="VersionPulse" ;
    public static final String    StaffIDPluse= "StaffIDPluse" ;
    public static final String    SchoolIDPulse= "SchoolIDPulse" ;
    public static final String     IsActivePulse= "IsActivePulse" ;
    public static final String     CreatedOnPluse="CreatedOnPluse" ;
    public static final String    UpdatedOn= "UpdatedOn" ;

//tblPulseSurvey

    private static final String TABLE_PULSE_SURVEY= "tblPulseSurvey";

    public static final String   SurveyPulseID=  "PulseID" ;
    public static final String   SurveyPulseQuestionID=   "PulseQuestionID" ;
    public static final String      ContentID= "ContentID" ;
    public static final String      SurveyBatchID= "BatchID";
    public static final String      SurveySubjectID= "SubjectID";
    public static final String    SurveyStaffID= "StaffID" ;
    public static final String     PulseDatetime= "PulseDatetime";
    public static final String     PageNumber= "PageNumber" ;
    public static final String     Posted="Posted" ;
    public static final String    optionA_Percentage= "optionA_Percentage" ;
    public static final String    optionB_Percentage= "optionB_Percentage" ;
    public static final String     optionC_Percentage= "optionC_Percentage" ;
    public static final String     optionD_Percentage="optionD_Percentage" ;
    public static final String    NoResponse_Percentage= "NoResponse_Percentage" ;
    public static final String    NoOfPresentees= "NoOfPresentees" ;
    public static final String    SurveyCreatedOn= "CreatedOn" ;
    public static final String    SurveyModifiedOn= "ModifiedOn" ;
    public static final String    SurveyUpdatedOn= "UpdatedOn" ;



//tblPulseResponses

    private static final String TABLE_PULSE_RESPONSE= "tblPulseResponses";

    public static final String   PulseResponseID=  "PulseResponseID" ;
    public static final String   PulseID=   "PulseID" ;
    public static final String      StudentID= "StudentID" ;
    public static final String      Response= "Response";
    public static final String      ResponseCreatedOn= "CreatedOn";
    public static final String     ResponseModifiedOn= "ModifiedOn" ;
    //tblStudentQuestionResult
    private static final String TABLE_STUDENT_QUESTION_RESULT = "tblStudentQuestionResult";
    private static final String RESPONSE_ID = "ResponseID";
    private static final String EXAM_RESPONSE_ID = "ExamResponseID";
    private static final String RESPONSE_QUESTION_ID = "QuestionID";
    private static final String RESPONSE_STUDENT_ANSWER = "StudentAnswer";
    private static final String RESPONSE_IS_CORRECT = "IsCorrect";
    private static final String MARK_FOR_ANSWER = "MarkForAnswer";
    private static final String RESPONSE_OBTAINED_SCORE = "ObtainedScore";


    //tblStudentExamResponse
    private static final String TABLE_STUDENT_EXAM_RESPONSE = "tblStudentExamResponse";
    private static final String TABLE_EXAM_RESPONSE_ID = "StuExamResponseID";
    private static final String RESPONSE_EXAM_ID = "ResponseExamID";
    private static final String RESPONSE_STUDENT_ID = "StudentID";
    private static final String RESPONSE_ROLL_NO = "RollNo";
    private static final String RESPONSE_STUDENT_NAME = "StudName";
    private static final String RESPONSE_TIME_TAKEN = "TimeTaken";
    private static final String RESPONSE_DATE_ATTENDED = "DateAttended";
    private static final String RESPONSE_TOTAL_SCORE = "TotalScore";



    //tblExamQuestions
    private static final String TABLE_EXAM_QUESTION= "ExamQuestions";
    private static final String QUESTION_ID = "QuestionID";
    private static final String QUESTION_EXAM_ID = "ExamID";
    private static final String TOPIC_ID = "TopicID";
    private static final String TOPIC_NAME = "TopicName";
    private static final String ASPECT_ID = "AspectID";
    private static final String ASPECT = "Aspect";
    private static final String QUESTION = "Question";
    private static final String QUESTION_NUMBER = "QuestionNumber";
    private static final String OPTION_A = "OptionA";
    private static final String OPTION_B = "OptionB";
    private static final String OPTION_C = "OptionC";
    private static final String OPTION_D = "OptionD";
    private static final String QUESTION_CORRECT_ANSWER = "CorrectAnswer";
    private static final String MARK = "Mark";
    private static final String NEGATIVE_MARK = "Negative_Mark";
    private static final String STUDENT_ANSWER = "StudentAnswer";
    private static final String IS_CORRECT = "IsCorrect";
    private static final String OBTAINED_SCORE = "ObtainedScore";
    private static final String CREATED_ON = "CreatedOn";
    private static final String MODIFIED_ON = "ModifiedOn";

    //tblAspectReport
    private static final String TABLE_ASPECT_REPORT= "AspectReport";
    private static final String AS_REPORT_ID = "ReportID";
    private static final String AS_EXAM_ID = "ExamID";
    private static final String AS_TOPIC_ID = "TopicID";
    private static final String AS_TOPIC_NAME = "TopicName";
    private static final String AS_ASPECT_ID = "AspectID";
    private static final String AS_ASPECT = "Aspect";
    private static final String ASPECT_QUESTION_ID = "QuestionID";
    private static final String NO_OF_CORRECT_ANSWER = "NoOfCorrectAnswers";
    private static final String NO_OF_WRONG_ANSWER = "NoOfWrongAnswers";




 /*   public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }*/

    public DatabaseHandler(Context context, String name, int version) {
        super(context, name, null, DATABASE_VERSION);
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        /*String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_PH_NO + " TEXT" + ")";*/

        String createschool="CREATE TABLE "+ Table_school+"("+ school_id +" INTEGER PRIMARY KEY," + SchoolCategoryID +" TEXT,"+ SchoolName+ " TEXT," + Acronym +" TEXT," + SchoolLogo +" TEXT,"+ CityID +" INTEGER,"+ StateID + " INTEGER,"+ CountryID +" INTEGER,"+CreatedOn + " TEXT,"+ModifiedOn +" TEXT,"+Background+ " TEXT"+")";

        //Log.e("createschool", createschool);
        db.execSQL(createschool);
        String createstaff="CREATE TABLE "+ Tablestaff +"("+
                StaffID  +"  INTEGER PRIMARY KEY,"+
                EmployementNumber+" TEXT,"+
                DOJ+" TEXT,"+
                FirstName+" TEXT,"+
                MiddleName+" TEXT,"+
                LastName +" TEXT,"+
                Gender +" TEXT,"+
                DOB +" TEXT,"+
                MaritalStatusID +" INTEGER,"+
                SpouseName +" TEXT,"+
                FatherName +" TEXT,"+
                MotherName +" TEXT,"+
                Phone +" TEXT,"+
                Email +" TEXT,"+
                PhotoFilename +" TEXT,"+
                StaffCategoryID +" INTEGER,"+
                SchoolID +" TEXT,"+
                DeptartmentID +" INTEGER,"+
                DesignationID +" INTEGER,"+
                PortalUserID +" TEXT,"+
                PortalLoginID +" TEXT,"+
                tablestaffCreatedOn+" TEXT,"+
                tablestaffModifiedOn +" TEXT,"+
                tableroominfo+" Text,"+
                timetable+" Text"+")";





        db.execSQL(createstaff);

        String pulsequestion= "CREATE TABLE "+TABLE_PULSE_QUESTION +"("+
                PulseQuestionID  +" INTEGER,"+
                PulseQuestion +" TEXT,"+
                PulseType+" TEXT,"+
                AnswerOption_A +" TEXT,"+
                AnswerOption_B +" TEXT," +
                AnswerOption_C +" INTEGER,"+
                AnswerOption_D+" INTEGER,"+
                CorrectAnswerOption +" TEXT,"+
                VersionPulse +" INTEGER,"+
                SchoolIDPulse+" INTEGER,"+
                StaffIDPluse +" INTEGER,"+
                IsActivePulse+" INTEGER,"+
                CreatedOnPluse+" TEXT,"+
                UpdatedOn+" TEXT"+")";

        db.execSQL(pulsequestion);
        String createclass= "CREATE TABLE "+tblClasses +"("+
                ClassID  +" INTEGER,"+
                ClassName +" TEXT,"+
                SectionName+" TEXT,"+
                Grade +" TEXT,"+
                ClassCode +" TEXT," +
                tblclassSchoolID +" INTEGER,"+
                DepartmentID+" INTEGER,"+
                tblclassStaffID +" INTEGER,"+
                InternetSSID +" TEXT,"+
                InternetPassword +" TEXT,"+
                InternetType+" TEXT,"+
                PushName+" TEXT,"+
                tblclassesCreatedOn+" TEXT,"+
                tblclassesModifiedOn+" TEXT,"+
                classesBatchID+" TEXT"+")";

        db.execSQL(createclass);

        String createtblsubject="CREATE TABLE "+tblSubject +"("+

                SubjectID +" INTEGER,"+
                SubjectName +" TEXT," +
                tblSubjectSchoolID +" INTEGER,"+
                tblSubjectStaffID +" INTEGER,"+
                tblSubjectClassID +" INTEGER,"+
                BatchID+" INTEGER,"+
                tblsubectCreatedOn +" TEXT," +
                tblsubectModifiedOn +" TEXT" +")";
        db.execSQL(createtblsubject);

        String createbatch="CREATE TABLE \"tblBatch\"" +
                "(" +
                "\"BatchID\" integer primary key not null ," +
                "\"CoordinatingStaffID\" Integer ," +
                "\"AcademicYear\" varchar ," +
                "\"SchoolID\" integer," +
                "\"ClassID\" Integer," +
                "\"StaffID\" Integer," +
                "\"BatchStartDate\" datetime," +
                "\"BatchEndDate\" datetime, " +
                "\"TimeTable\" varchar," +
                "\"IsActive\" integer," +
                "\"CreatedOn\" datetime," +
                "\"ModifiedOn\" datetime," +
                "\"Batchcode\" varchar" +
                ")";

        db.execSQL(createbatch);
        
      /*  String createtablesubject="CREATE TABLE \"tblSubject\"" +
                "(" +
                "\"SubjectID\" integer ," +
                "\"SubjectName\" varchar ," +
                "\"SchoolID\" integer ," +
                "\"StaffID\" integer," +
                "\"ClassID\" integer ," +
                "\"BatchID\" integer ," +
                "\"CreatedOn\" datetime," +
                "\"ModifiedOn\" datetime" +
                ")";
        db.execSQL(createtablesubject);*/
        /*"\"ContentID\" integer primary key not null ," +*/
        String createtablecontent="CREATE TABLE \"tblContent\"" +
                "(" +
                "\"ContentID\" integer primary key not null ,"+
                "\"ContentDescription\" varchar ," +
                "\"Author\" varchar ," +
                "\"ContentTypeID\" integer ," +
                "\"ContentCatalogType\" varchar ," +
                "\"ContentFilename\" varchar ," +
                "\"BookShelfID\" integer," +
                "\"Vaporize\" varchar ," +
                "\"IsEncrypted\" integer ," +
                "\"EncryptionSalt\" varchar ," +
                "\"SchoolID\" integer ," +
                "\"BatchID\" integer," +
                "\"StaffID\" integer," +
                "\"CreatedOn\" datetime," +
                "\"Class\" varchar," +
                "\"Subject\" varchar ," +
                "\"type\" varchar "+
        ")";

        db.execSQL(createtablecontent);

      /*  CREATE TABLE "tblSchool"
        (
                "SchoolID" integer primary key  not null,
                "SchoolCategoryID" varchar,
                "SchoolName" varchar,
                "Acronym" varchar,
                "SchoolLogo" varchar,
                "CityID" integer,
                "StateID" integer,
                "CountryID" integer,
                "CreatedOn" Datetime,
                "ModifiedOn" Datetime
        );*/
       /* String  attendance=CREATE TABLE "tblAttendance"
        (
                "AttendanceID" integer primary key autoincrement not null,
                "SchoolID" integer,
                "BatchID" integer,
                "SubjectID" integer,
                "StaffID" integer,
                "AttendanceDateTime" datetime,
                "Credits" integer,
                "StudentID" integer,
                "RollNo" integer,
                "StudName" varchar,
                "Attendance" varchar ,
                "ManualAttendance" integer,
                "IsUploaded" integer,
                "CreatedOn" datetime,
                "ModifiedOn" datetime
        );*/


        String tablecontentbranch="CREATE TABLE \"tblContentBatch\"" +
                "(" +
                "\"ContentBatchID\" integer primary key not null ," +
                "\"ContentID\" integer ," +
                "\"ContentTypeID\" integer ," +
                "\"SchoolID\" integer ," +
                "\"BatchID\" integer ," +
                "\"SujectID\" integer ," +
                "\"StaffID\" integer," +
                "\"CreatedOn\" datetime ," +
                "\"ModifiedOn\" datetime " +
                " )";
        db.execSQL(tablecontentbranch);


        String tablestudent="CREATE TABLE \"tblStudent\"" +
                "(" +
                "\"StudentID\" integer primary key not null ," +
                "\"AdmissionNumber\" varchar ," +
                "\"DOA\" datetime ," +
                "\"FirstName\" varchar ," +
                "\"LastName\" varchar ," +
                "\"DOB\" datetime ," +
                "\"Gender\" varchar ," +
                "\"Phone_1\" varchar ," +
                "\"Phone_2\" varchar ," +
                "\"Email\" varchar ," +
                "\"FatherName\" varchar ," +
                "\"MotherName\" varchar ," +
                "\"GuardianMobileNumber\" varchar ," +
                "\"RollNo\" varchar ," +
                "\"ClassID\" integer ," +
                "\"BatchID\" integer ," +
                "\"SchoolID\" integer ," +
                "\"AcademicYear\" varchar ," +
                "\"Guardians\" varchar ," +
                "\"PhotoFilename\" varchar ," +
                "\"PortalUserID\" integer ," +
                "\"StudentLoginID\" varchar ," +
                "\"Version\" integer ," +
                "\"StaffID\" integer ," +
                "\"CreditPoints\" integer ," +
                "\"ConnectionStatus\" integer ," +
                "\"ManualAttendance\" integer ," +
                "\"NoOfPresent\" integer ," +
                "\"Credits\" integer," +
                "\"CreatedOn\" datetime ," +
                "\"ModifiedOn\" datetime ," +
                tableroominfo+" Text"+
                " )";
        db.execSQL(tablestudent);



        //master info




        String masterinfo="CREATE TABLE \"masterinfo\"" +
                "(" +
                "\"ID\" integer primary key not null ," +
                "\"SubjectName\" varchar ," +
                "\"SubjectDescription\" varchar ," +

                "\"Status\" varchar ," +
                "\"SchoolID\" integer ," +
                "\"CreatedOn\" datetime ," +
                "\"ModifiedOn\" datetime " +
                " )";
        db.execSQL(masterinfo);




        String CREATE_TABLE_EXAM_DETAILS = "CREATE TABLE " + TABLE_EXAM_DETAILS + "("
                + EXAM_ID + " INTEGER PRIMARY KEY,"+ STAFF_ID + " INTEGER,"+ EXAM__CATEGORY_ID + " INTEGER," + EXAM_CATEGORY_NAME + " TEXT,"+ EXAM_CODE + " TEXT,"+EXAM_DESCRIPTION + " TEXT,"+EXAM_SEQUENCE + " TEXT,"+EXAM_DATE + " TEXT,"+EXAM_TYPE_ID + " INTEGER,"+EXAM_TYPE + " TEXT,"+SUBJECT_ID + " INTEGER,"+SUBJECT + " TEXT,"+EXAM_DURATION + " INTEGER,"+SCHOOL_ID + " INTEGER,"+CLASS_ID + " INTEGER,"+BATCH_ID + " INTEGER,"+ IS_RESULT_PUBLISHED + " INTEGER,"+EXAM_SHELF_ID + " INTEGER,"+TIME_TAKEN + " INTEGER,"+DATE_ATTENDED + " TEXT," + TOTAL_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_EXAM_DETAILS);



        String CREATE_TABLE_EXAM_QUESTION = "CREATE TABLE " + TABLE_EXAM_QUESTION + "("
                + QUESTION_ID + " INTEGER PRIMARY KEY,"+ QUESTION_EXAM_ID + " INTEGER," + TOPIC_ID + " INTEGER,"+ TOPIC_NAME + " TEXT,"+ASPECT_ID + " INTEGER,"+ASPECT + " TEXT,"+QUESTION + " TEXT,"+QUESTION_NUMBER + " INTEGER,"+OPTION_A + " TEXT,"+OPTION_B + " TEXT,"+OPTION_C + " TEXT,"+OPTION_D + " TEXT,"+QUESTION_CORRECT_ANSWER + " TEXT,"+MARK + " INTEGER,"+NEGATIVE_MARK + " INTEGER,"+ STUDENT_ANSWER + " TEXT,"+IS_CORRECT + " INTEGER,"+OBTAINED_SCORE + " INTEGER," +CREATED_ON + " TEXT," + MODIFIED_ON + " TEXT" + ")";
        db.execSQL(CREATE_TABLE_EXAM_QUESTION);


        //tblStudentExamResponse

        //tblStudentExamResponse


        String CREATE_TABLE_STUDENT_EXAM_RESPONSE = "CREATE TABLE " + TABLE_STUDENT_EXAM_RESPONSE + "("
                + TABLE_EXAM_RESPONSE_ID + " INTEGER PRIMARY KEY   AUTOINCREMENT,"+ RESPONSE_EXAM_ID + " INTEGER," + RESPONSE_STUDENT_ID + " INTEGER,"+ RESPONSE_ROLL_NO + " INTEGER,"+RESPONSE_STUDENT_NAME + " TEXT,"+RESPONSE_TIME_TAKEN + " INTEGER,"+RESPONSE_DATE_ATTENDED + " TEXT,"  + RESPONSE_TOTAL_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_STUDENT_EXAM_RESPONSE);


        String CREATE_TABLE_STUDENT_QUESTION_RESULT = "CREATE TABLE " + TABLE_STUDENT_QUESTION_RESULT + "("
                + RESPONSE_ID + " INTEGER PRIMARY KEY   AUTOINCREMENT,"+ EXAM_RESPONSE_ID + " INTEGER," + RESPONSE_QUESTION_ID + " INTEGER,"+ RESPONSE_STUDENT_ANSWER + " TEXT,"+RESPONSE_IS_CORRECT + " INTEGER,"+MARK_FOR_ANSWER + " INTEGER,"+ RESPONSE_OBTAINED_SCORE + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_STUDENT_QUESTION_RESULT);



        String CREATE_TABLE_ASPECT_REPORT = "CREATE TABLE " + TABLE_ASPECT_REPORT + "("
                + AS_REPORT_ID + " INTEGER PRIMARY KEY,"+ AS_EXAM_ID + " INTEGER," + AS_TOPIC_ID + " INTEGER,"+ AS_TOPIC_NAME + " TEXT,"+AS_ASPECT_ID + " INTEGER,"+AS_ASPECT + " TEXT,"+ASPECT_QUESTION_ID + " INTEGER,"+QUESTION_NUMBER + " INTEGER,"+NO_OF_CORRECT_ANSWER + " INTEGER," + NO_OF_WRONG_ANSWER + " INTEGER" + ")";
        db.execSQL(CREATE_TABLE_ASPECT_REPORT);


       /* "ID":"1",
                "SubjectName":"English",
                "SubjectDescription":"English - First Language",
                "CreatedBy":"1",
                "CreatedDate":"2017-04-18 00:00:00",
                "ModifiedBy":"1",
                "ModifiedDate":"2017-04-18 00:00:00",
                "Status":"1",
                "SchoolID":"1"*/

        String tableCommunication="CREATE TABLE \"tblCommunication\"\n" +
                "(\n" +
                "CommunicationID integer primary key AUTOINCREMENT,\n" +
                "SenderID integer,\n" +

                "DateOfCommunication datetime,\n" +
                "Title varchar,\n" +
                "Content varchar,\n" +
                "BatchID integer,\n" +
                "CreatedOn datetime,\n" +
                "UpdatedOn datetime,\n" +
                "StaffID integer,\n" +
                "SchoolID integer," +
                "SubjectID integer"+
                ")";
        db.execSQL(tableCommunication);

        String pulsesurveytable= "CREATE TABLE "+TABLE_PULSE_SURVEY +"("+
                SurveyPulseID  +" INTEGER,"+
                SurveyPulseQuestionID +" INTEGER,"+
                ContentID+" INTEGER,"+
                SurveyBatchID +" INTEGER,"+
                SurveySubjectID +" INTEGER," +
                SurveyStaffID +" INTEGER,"+
                PulseDatetime+" TEXT,"+
                PageNumber +" TEXT,"+
                Posted +" TEXT,"+
                optionA_Percentage +" INTEGER,"+
                optionB_Percentage+" INTEGER,"+
                optionC_Percentage +" INTEGER,"+
                optionD_Percentage+" INTEGER,"+
                NoResponse_Percentage+" INTEGER,"+
                NoOfPresentees+" INTEGER,"+
                SurveyCreatedOn+" TEXT,"+
                SurveyModifiedOn+" TEXT"+")";

        db.execSQL(pulsesurveytable);
        String pulseresponse= "CREATE TABLE "+TABLE_PULSE_RESPONSE +"("+
                PulseResponseID + " INTEGER PRIMARY KEY   AUTOINCREMENT,"+
                PulseID +" INTEGER,"+
                StudentID+" INTEGER,"+
                Response +" TEXT,"+
                ResponseCreatedOn +" DATETIME," +
                ResponseModifiedOn+" DATETIME"+")";

        db.execSQL(pulseresponse);

      /*  "*//*ID": "5",
                "Name": "Assignment",
                "Description": "Assignment category",
                "Image": "http://schoolproject.dci.in/images/event_category/assignment.png"*/
        String tablecomm="CREATE TABLE \"tblMessageStudentMap\"\n" +
                "(\n" +
                "MsgStudentMapID integer primary key AUTOINCREMENT,\n" +
                "MsgID integer,\n" +
                "ReceiverID integer,\n" +

                "Status integer," +
                "IsAck integer,"+
                "IsSent integer"+
                ")";
        db.execSQL(tablecomm);

        String calendetyper="CREATE TABLE \"tblcalendartype\"\n" +
                "(\n" +
                "Calenderid integer,\n" +
                "Name varchar,\n" +
                "Description varchar,\n" +

                "Image varchar" +

                ")";
        db.execSQL(calendetyper);

      /*  "ID": "2",
                "RoomName": "Computer Science",
                "SSID": "CSE",
                "Password": "CSR_CSE"*/

        String rooms="CREATE TABLE \"tblrooms\"\n" +
                "(\n" +
                "ID integer,\n" +
                "RoomName varchar,\n" +
                "SSID varchar,\n" +

                "Password varchar" +

                ")";
        db.execSQL(rooms);

        String tableCalendar=  "CREATE TABLE tblCalendar(EventID integer primary key not null, EventName text, EventDescription text, EventStartDate datetime, EventEndDate datetime, CreatedBy integer, Category text, CategoryID integer,CategoryIcon text, CreatedOn datetime, ModifiedBy integer, ModifiedOn datetime, Status integer, StaffID integer)";

        db.execSQL(tableCalendar);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Table_school);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXAM_QUESTION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASPECT_REPORT);
        db.execSQL("DROP TABLE IF EXISTS " + "tblCalendar");
        // Create tables again
        onCreate(db);
    }
    public Cursor retriveCalendarCommon(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar", null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }


    public Cursor reriveformcontentid(int id)
    {
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblContent where ContentID="+id, null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }


    public void content(Tablecontent details)
    {
 /*   g_content.setContentFilename(con.getString("ContentFileName"));
    g_content.setContentCatalogType(con.getString("ContentType"));
    g_content.setContentDescription(con.getString("ContentTitle"));
    g_content.setVaporize(con.getString("Vaporize"));
    g_content.setSubject(con.getJSONArray("Subject").toString());
    g_content.setClasses(con.getJSONArray("Class").toString());*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
/*
    "\"ContentDescription\" varchar ," +
            "\"Author\" varchar ," +
            "\"ContentTypeID\" integer ," +
            "\"ContentCatalogType\" varchar ," +
            "\"ContentFilename\" varchar ," +
            "\"BookShelfID\" integer," +
            "\"Vaporize\" varchar ," +
            "\"IsEncrypted\" integer ," +
            "\"EncryptionSalt\" varchar ," +
            "\"SchoolID\" integer ," +
            "\"BatchID\" integer," +
            "\"StaffID\" integer," +
            "\"CreatedOn\" datetime," +
            "\"Class\" varchar," +
            "\"Subject\" varchar " +*/

    /*g_content.setContentFilename(con.getString("ContentFileName"));
    g_content.setContentCatalogType(con.getString("ContentType"));
    g_content.setContentDescription(con.getString("ContentTitle"));
    g_content.setVaporize(con.getString("Vaporize"));
    g_content.setSubject(con.getJSONArray("Subject").toString());
    g_content.setClasses(con.getJSONArray("Class").toString());
*/

        values.put("ContentFilename",details.getContentFilename());
        values.put("Vaporize",details.getVaporize());
        values.put("ContentDescription",details.getContentDescription());
        values.put("Class",details.getClasses());
        values.put("Subject",details.getSubject());
        values.put("ContentCatalogType", details.getContentCatalogType());
        values.put("ContentID", details.getContentID());
        values.put("type", details.getType());
        db.insert("tblContent", null, values);
        db.close();



    }


    public void updatecontent(Tablecontent details)
    {
 /*   g_content.setContentFilename(con.getString("ContentFileName"));
    g_content.setContentCatalogType(con.getString("ContentType"));
    g_content.setContentDescription(con.getString("ContentTitle"));
    g_content.setVaporize(con.getString("Vaporize"));
    g_content.setSubject(con.getJSONArray("Subject").toString());
    g_content.setClasses(con.getJSONArray("Class").toString());*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
/*
    "\"ContentDescription\" varchar ," +
            "\"Author\" varchar ," +
            "\"ContentTypeID\" integer ," +
            "\"ContentCatalogType\" varchar ," +
            "\"ContentFilename\" varchar ," +
            "\"BookShelfID\" integer," +
            "\"Vaporize\" varchar ," +
            "\"IsEncrypted\" integer ," +
            "\"EncryptionSalt\" varchar ," +
            "\"SchoolID\" integer ," +
            "\"BatchID\" integer," +
            "\"StaffID\" integer," +
            "\"CreatedOn\" datetime," +
            "\"Class\" varchar," +
            "\"Subject\" varchar " +*/

    /*g_content.setContentFilename(con.getString("ContentFileName"));
    g_content.setContentCatalogType(con.getString("ContentType"));
    g_content.setContentDescription(con.getString("ContentTitle"));
    g_content.setVaporize(con.getString("Vaporize"));
    g_content.setSubject(con.getJSONArray("Subject").toString());
    g_content.setClasses(con.getJSONArray("Class").toString());
*/

        values.put("ContentFilename",details.getContentFilename());
        values.put("Vaporize",details.getVaporize());
        values.put("ContentDescription",details.getContentDescription());
        values.put("Class",details.getClasses());
        values.put("Subject",details.getSubject());
        values.put("ContentCatalogType", details.getContentCatalogType());
        values.put("ContentID", details.getContentID());
        values.put("type", details.getType());

        db.update("tblContent", values, "ContentID " + " = ?",
                new String[]{"" + details.getContentID()});
      /*  db.insert("tblContent", null, values);
        db.close();
*/


    }

    // Adding new examswritten
    public void addPuseQuestion(PulseExamPojo pulseExamPojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PulseQuestionID, pulseExamPojo.getPulseQuestionID());
        values.put(PulseQuestion, pulseExamPojo.getPulseQuestion());
        values.put(PulseType, pulseExamPojo.getPulseType());
        values.put(AnswerOption_A, pulseExamPojo.getAnswerOption_A());
        values.put(AnswerOption_B, pulseExamPojo.getAnswerOption_B());
        values.put(AnswerOption_C, pulseExamPojo.getAnswerOption_C());
        values.put(AnswerOption_D, pulseExamPojo.getAnswerOption_D());
        values.put(CorrectAnswerOption, pulseExamPojo.getCorrectAnswerOption());
        values.put(VersionPulse, pulseExamPojo.getVersionPulse());
        values.put(StaffIDPluse, pulseExamPojo.getStaffIDPluse());
        values.put(SchoolIDPulse, pulseExamPojo.getSchoolIDPulse());
        values.put(IsActivePulse, pulseExamPojo.getIsActivePulse());
        values.put(CreatedOnPluse, pulseExamPojo.getCreatedOnPluse());
        values.put(UpdatedOn, pulseExamPojo.getUpdatedOn());



        // Inserting Row
        db.insert(TABLE_PULSE_QUESTION, null, values);
        db.close(); // Closing database connection
    }



    public int downloadUpdatePulse(PulseExamPojo pulseExamPojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PulseQuestionID, pulseExamPojo.getPulseQuestionID());
        values.put(PulseQuestion, pulseExamPojo.getPulseQuestion());
        values.put(PulseType, pulseExamPojo.getPulseType());
        values.put(AnswerOption_A, pulseExamPojo.getAnswerOption_A());
        values.put(AnswerOption_B, pulseExamPojo.getAnswerOption_B());
        values.put(AnswerOption_C, pulseExamPojo.getAnswerOption_C());
        values.put(AnswerOption_D, pulseExamPojo.getAnswerOption_D());
        values.put(CorrectAnswerOption, pulseExamPojo.getCorrectAnswerOption());
        values.put(VersionPulse, pulseExamPojo.getVersionPulse());
        values.put(StaffIDPluse, pulseExamPojo.getStaffIDPluse());
        values.put(SchoolIDPulse, pulseExamPojo.getSchoolIDPulse());
        values.put(IsActivePulse, pulseExamPojo.getIsActivePulse());
        values.put(CreatedOnPluse, pulseExamPojo.getCreatedOnPluse());
        values.put(UpdatedOn, pulseExamPojo.getUpdatedOn());

        // updating row
        return db.update(TABLE_PULSE_QUESTION, values, PulseQuestionID + " = ?",
                new String[] { String.valueOf(pulseExamPojo.getPulseQuestionID()) });
    }


    public Cursor  checkroomid(int roomid)
    {
        String selectQuery = "select *from tblrooms where ID="+roomid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }

    public Cursor  checksubjectid(int subjectid)
    {
        String selectQuery = "select *from masterinfo where ID="+subjectid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }

    public Cursor  checkcalendar(int subjectid)
    {
        String selectQuery = "select *from tblcalendartype where Calenderid="+subjectid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }


    public void updateaddmasterroom(Rooms room)
    {

       /* "ID integer,\n" +
                "RoomName varchar,\n" +
                "SSID varchar,\n" +

                "Password varchar" +

                ")";*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ID", room.getId());
        values.put("RoomName" , room.getRoomsname());
        values.put("SSID", room.getSsid());
        values.put("Password",room.getPassword());
        //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
       // db.insert("tblrooms", null, values);

        int updatequery= db.update("tblrooms",
                values,
                "ID" + " = ?",
                new String[]{"" + room.getId()});
        db.close();
       // db.close(); // Closing database connection
    }

    public void addmasterroom(Rooms room)
    {

       /* "ID integer,\n" +
                "RoomName varchar,\n" +
                "SSID varchar,\n" +

                "Password varchar" +

                ")";*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ID", room.getId());
        values.put("RoomName" , room.getRoomsname());
        values.put("SSID", room.getSsid());
        values.put("Password",room.getPassword());
        //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        db.insert("tblrooms", null, values);
        db.close(); // Closing database connection
    }
public Rooms getroomdata(int id)
{
    String selectQuery = "select *from tblrooms where ID="+id+"";
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    Rooms rooms=new Rooms();

    if (cursor.moveToFirst()) {
          rooms.setId(cursor.getInt(0));
         rooms.setRoomsname(cursor.getString(1));
        rooms.setSsid(cursor.getString(2));
        rooms.setPassword(cursor.getString(3));
        return  rooms;


    }
    return rooms;
       /* do {
            helper.Calendarpojo pojo=new helper.Calendarpojo();
            pojo.setCalendarid(cursor.getInt(0));
            pojo.setCalendarname(cursor.getString(1));
            pojo.setCalendardescription(cursor.getString(2));
            pojo.setCalendarimage(cursor.getString(3));

        }while (cursor.moveToNext());*/





}

    public void updateaddcalendaremaster(helper.Calendarpojo pulseresposePojo) {

     /*   "(\n" +
                "Calenderid integer,\n" +
                "Name varchar,\n" +
                "Description varchar,\n" +

                "Image varchar" +*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Calenderid", pulseresposePojo.getCalendarid());
        values.put("Name" , pulseresposePojo.getCalendarname());
        values.put("Description", pulseresposePojo.getCalendardescription());
        values.put("Image", pulseresposePojo.getCalendarimage());
        //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        //db.insert("tblcalendartype", null, values);
        int updatequery= db.update("tblcalendartype",
                values,
                "Calenderid" + " = ?",
                new String[]{"" + pulseresposePojo.getCalendarid()});
        db.close(); // Closing database connection
    }
    public void addcalendaremaster(helper.Calendarpojo pulseresposePojo) {

     /*   "(\n" +
                "Calenderid integer,\n" +
                "Name varchar,\n" +
                "Description varchar,\n" +

                "Image varchar" +*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("Calenderid", pulseresposePojo.getCalendarid());
        values.put("Name" , pulseresposePojo.getCalendarname());
        values.put("Description", pulseresposePojo.getCalendardescription());
        values.put("Image", pulseresposePojo.getCalendarimage());
                                //values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        db.insert("tblcalendartype", null, values);
        db.close(); // Closing database connection
    }
    public ArrayList<helper.Calendarpojo>getmastercalendar()
    {
        String selectQuery = "select *from tblcalendartype";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        ArrayList<helper.Calendarpojo>lisofevent=new ArrayList<>();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                helper.Calendarpojo pojo=new helper.Calendarpojo();
                pojo.setCalendarid(cursor.getInt(0));
                pojo.setCalendarname(cursor.getString(1));
                pojo.setCalendardescription(cursor.getString(2));
                pojo.setCalendarimage(cursor.getString(3));
                lisofevent.add(pojo);
            }while (cursor.moveToNext());

            }
        return  lisofevent;
    }


    public void TableCalendar(ArrayList<Calendarpojo> calendarlist){
        SQLiteDatabase db = this.getWritableDatabase();
        for(int i=0;i<calendarlist.size();i++){
            ContentValues calendarcv=new ContentValues();
            calendarcv.put("EventID",calendarlist.get(i).getEventid());
            calendarcv.put("EventName",calendarlist.get(i).getEventname());
            calendarcv.put("EventDescription",calendarlist.get(i).getEventDesc());
            calendarcv.put("EventStartDate",calendarlist.get(i).getEventstart());
            calendarcv.put("EventEndDate",calendarlist.get(i).getEventend());
            calendarcv.put("Category",calendarlist.get(i).getCategory());
            calendarcv.put("CategoryIcon",calendarlist.get(i).getCategoryIcon());
            calendarcv.put("CategoryID",calendarlist.get(i).getCategoryid());

            db.insert("tblCalendar", null, calendarcv);

        }
        db.close();
    }
    public void insertCalendar(ContentValues cv){
        SQLiteDatabase db = this.getWritableDatabase();


            db.insert("tblCalendar", null, cv);


        db.close();
    }
    public Cursor retriveTypeCalendar(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c=db.rawQuery("select * from tblcalendartype where Calenderid="+id+"",null);
        return c;
    }

    public  ArrayList<Calendarpojo>  retriveCalendar(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar", null);

        ArrayList<Calendarpojo> calendarlist=new ArrayList<>();
        while(c.moveToNext()){
            Calendarpojo cpojo=new Calendarpojo();
            cpojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
            cpojo.setEventname(c.getString(c.getColumnIndex("EventName")));
            cpojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
            cpojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
            cpojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));

          //  cpojo.setCategoryid(c.getInt(c.getColumnIndex("CategoryID")));
            int id=c.getInt(c.getColumnIndex("CategoryID"));
            cpojo.setCategoryid(id);
            Cursor typecur=retriveCalendarType(id);
            while(typecur.moveToNext()){
                cpojo.setCategory(typecur.getString(typecur.getColumnIndex("Name")));

                cpojo.setCategoryIcon(typecur.getString(typecur.getColumnIndex("Image")));
            }

            //cpojo.setCategory(c.getString(c.getColumnIndex("Category")));
          //  cpojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));

            calendarlist.add(cpojo);

        }
        return calendarlist;
    }

    public Cursor  retriveCalendarType(int _id) {
        // TODO Auto-generated method stub
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select * from tblcalendartype where Calenderid="+_id, null);
        return c;
    }
    public Cursor retriveCategory_Calendar() {
        SQLiteDatabase con = getReadableDatabase();
        Cursor c = con.rawQuery("SELECT DISTINCT Category FROM tblCalendar", null);
        return c;
    }
    public Cursor retriveCategory_Calendartype() {
        SQLiteDatabase con = getReadableDatabase();
        Cursor c = con.rawQuery("SELECT * FROM tblcalendartype", null);
        return c;
    }

    public Cursor retriveEvent(int evid){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar where EventID="+evid, null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }

    public Cursor retriveStartDate(String date){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar where EventStartDate="+"'"+date+"'", null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }

   /* public Cursor retriveStartdate(String date){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select Category from tblCalendar where EventStartDate="+date, null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }*/

    public Cursor retriveEnddate(String date){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCalendar where EventEndDate="+"'"+date+"'", null);
        //  Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;
    }
    // Updating single examswritten
    public int updateResponseDetails(StudentQuestionResultPojo studentques) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_RESPONSE_ID, studentques.getExamResponseID());
        values.put(RESPONSE_QUESTION_ID, studentques.getQuestionID());
        values.put(RESPONSE_STUDENT_ANSWER, studentques.getStudentAnswer());
        values.put(RESPONSE_IS_CORRECT, studentques.getIsCorrect());
        values.put(MARK_FOR_ANSWER, studentques.getMarkForAnswer());
        values.put(RESPONSE_OBTAINED_SCORE, studentques.getObtainedScore());
        return db.update(TABLE_STUDENT_QUESTION_RESULT, values, EXAM_RESPONSE_ID + " = ? AND "+RESPONSE_QUESTION_ID + " = ?",
                new String[] { String.valueOf(studentques.getExamResponseID()),String.valueOf(studentques.getQuestionID()) });

        // updating row
    }
    // Getting All examswritten
    public List<ExamResponsePojo> getallresponse() {
        List<ExamResponsePojo> examrespojo = new ArrayList<ExamResponsePojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_EXAM_RESPONSE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamResponsePojo ansdetails = new ExamResponsePojo();
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setStudentID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setRollNo(Integer.parseInt(cursor.getString(3)));
                ansdetails.setStudName(cursor.getString(4));
                ansdetails.setTimeTaken(Integer.parseInt(cursor.getString(5)));
                ansdetails.setDateAttended(cursor.getString(6));
                ansdetails.setTotalScore(Integer.parseInt(cursor.getString(7)));
                // Adding examswritten to list
                examrespojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examrespojo;
    }

    public void TableCommunication(ContentValues comcv){

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert("tblCommunication",null,comcv);
        db.close();

    }
    public void TableCommunicationMap(ContentValues mscv){

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert("tblMessageStudentMap",null,mscv);
        db.close();

    }

    public Cursor MapRetrive(int msgid){

        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid, null);
        return c;

    }

    public Cursor AckStudent(int msgid){

        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select ReceiverID from tblMessageStudentMap where MsgID="+msgid+" AND IsAck=1", null);
        return c;

    }
    public Cursor MapRetriveall(){

        SQLiteDatabase con = getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblMessageStudentMap", null);
        return c;

    }
    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServer(String subjectID, String staffID) {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE+" = '"+ "Teacher-Conducted"+"' and "+SUBJECT_ID+"= '"+subjectID+"'"+" and "+STAFF_ID+"= '"+staffID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(5));
                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }
    public void UpdateMap(int msgid,int receiverid){
        SQLiteDatabase db = this.getWritableDatabase();
       /// SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

       /* "MsgStudentMapID integer primary key AUTOINCREMENT,\n" +
                "MsgID integer,\n" +
                "ReceiverID integer,\n" +

                "Status integer," +
                "IsAck integer"+*/

        values.put("IsSent" ,1);

        //  values.put("Year", details.getYear());

        int updatequery= db.update("tblMessageStudentMap",
                values,
                "MsgID" + " = ? AND " + "ReceiverID" + " = ?",
                new String[]{"" + msgid, "" + receiverid});
        db.close();
    }

    public void UpdateMapack(int msgid, int receiverid) {
        SQLiteDatabase db = this.getWritableDatabase();
        /// SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

       /* "MsgStudentMapID integer primary key AUTOINCREMENT,\n" +
                "MsgID integer,\n" +
                "ReceiverID integer,\n" +

                "Status integer," +
                "IsAck integer"+*/

        values.put("IsSent", 1);
        values.put("IsAck", 1);

        //  values.put("Year", details.getYear());

        int updatequery= db.update("tblMessageStudentMap",
                values,
                "MsgID" + " = ? AND " + "ReceiverID" + " = ?",
                new String[]{"" + msgid, "" + receiverid});
        db.close();
    }
    public  Cursor retriveIsSentNot(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblMessageStudentMap where IsSent=0 AND ReceiverID=" + id, null);
        return c;
    }
    public  Cursor retriveMessages(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCommunication where MsgID="+id, null);
        return c;
    }

    public  Cursor retriveCommunication(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select CommunicationID, SenderID, DateOfCommunication, Title, SubjectID, Content from tblCommunication", null);
        return c;
    }
    public  Cursor todayCommunication(String startdate,String dtime){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("select * from tblCommunication where DateOfCommunication BETWEEN '"+startdate+"' AND '"+dtime+"'", null);
        return c;
    }



    public void deletesurvey() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PULSE_SURVEY);

    }
    public void deletesurveyresponse() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PULSE_RESPONSE);

    }

// Getting All examswritten

    public List<PulseSurveyPojo> PulseSurveyUsingExamId(int surveyid) {
        List<PulseSurveyPojo> PulseSurveyPojo = new ArrayList<PulseSurveyPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PULSE_SURVEY +" where " +SurveyPulseID+" = '"+ surveyid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                PulseSurveyPojo SurveyPojo = new PulseSurveyPojo();
                SurveyPojo.setPulseSurveyID(Integer.parseInt(cursor.getString(0)));
                SurveyPojo.setPulseQuestionID(Integer.parseInt(cursor.getString(1)));
                SurveyPojo.setContentID(Integer.parseInt(cursor.getString(2)));
                SurveyPojo.setBatchID(Integer.parseInt(cursor.getString(3)));
                SurveyPojo.setSubjectID(Integer.parseInt(cursor.getString(4)));
                SurveyPojo.setStaffID(Integer.parseInt(cursor.getString(5)));
                SurveyPojo.setPulseDatetime(cursor.getString(6));
                SurveyPojo.setPageNumber(Integer.parseInt(cursor.getString(7)));
                SurveyPojo.setPosted(Integer.parseInt(cursor.getString(8)));
                SurveyPojo.setOptionA_Percentage(Integer.parseInt(cursor.getString(9)));
                SurveyPojo.setOptionB_Percentage(Integer.parseInt(cursor.getString(10)));
                SurveyPojo.setOptionC_Percentage(Integer.parseInt(cursor.getString(11)));
                SurveyPojo.setOptionD_Percentage(Integer.parseInt(cursor.getString(12)));
                SurveyPojo.setNoResponse_Percentage(Integer.parseInt(cursor.getString(13)));
                SurveyPojo.setNoOfPresentees(Integer.parseInt(cursor.getString(14)));
                SurveyPojo.setCreatedOn((cursor.getString(15)));
                SurveyPojo.setModifiedOn((cursor.getString(16)));


                // Adding examswritten to list
                PulseSurveyPojo.add(SurveyPojo);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return PulseSurveyPojo;
    }
    // Getting All examswritten
    public List<PulseExamPojo> getAllPulseExamDetailsLastUsingId(int pulId) {
        List<PulseExamPojo> PulseExamPojo = new ArrayList<PulseExamPojo>();
        // Select All Query
        //  String selectQuery = "SELECT  * FROM " + TABLE_PULSE_QUESTION+" where " +PulseQuestionID+" = '"+ pulId+"'";
        String selectQuery = "SELECT * FROM " +TABLE_PULSE_QUESTION+" where " +PulseQuestionID+" = '"+ pulId+"'"+" ORDER BY " +PulseQuestionID+ " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToLast()) {
            do {
                PulseExamPojo pulsedetails = new PulseExamPojo();
                pulsedetails.setPulseQuestionID(Integer.parseInt(cursor.getString(0)));
                pulsedetails.setPulseQuestion(cursor.getString(1));
                pulsedetails.setPulseType(cursor.getString(2));
                pulsedetails.setAnswerOption_A(cursor.getString(3));
                pulsedetails.setAnswerOption_B(cursor.getString(4));
                pulsedetails.setAnswerOption_C(cursor.getString(5));
                pulsedetails.setAnswerOption_D(cursor.getString(6));
                pulsedetails.setCorrectAnswerOption(cursor.getString(7));
                pulsedetails.setVersionPulse(Integer.parseInt(cursor.getString(8)));
                pulsedetails.setStaffIDPluse(Integer.parseInt(cursor.getString(9)));
                pulsedetails.setSchoolIDPulse(Integer.parseInt(cursor.getString(10)));
                pulsedetails.setIsActivePulse(Integer.parseInt(cursor.getString(11)));
                pulsedetails.setCreatedOnPluse(cursor.getString(12));
                pulsedetails.setUpdatedOn(cursor.getString(13));
                // Adding examswritten to list
                PulseExamPojo.add(pulsedetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return PulseExamPojo;
    }


    // Getting All examswritten
    public List<PulseExamPojo> getAllPulseExamDetailsUsingId(int pulId) {
        List<PulseExamPojo> PulseExamPojo = new ArrayList<PulseExamPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PULSE_QUESTION+" where " +PulseQuestionID+" = '"+ pulId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PulseExamPojo pulsedetails = new PulseExamPojo();
                pulsedetails.setPulseQuestionID(Integer.parseInt(cursor.getString(0)));
                pulsedetails.setPulseQuestion(cursor.getString(1));
                pulsedetails.setPulseType(cursor.getString(2));
                pulsedetails.setAnswerOption_A(cursor.getString(3));
                pulsedetails.setAnswerOption_B(cursor.getString(4));
                pulsedetails.setAnswerOption_C(cursor.getString(5));
                pulsedetails.setAnswerOption_D(cursor.getString(6));
                pulsedetails.setCorrectAnswerOption(cursor.getString(7));
                pulsedetails.setVersionPulse(Integer.parseInt(cursor.getString(8)));
                pulsedetails.setStaffIDPluse(Integer.parseInt(cursor.getString(9)));
                pulsedetails.setSchoolIDPulse(Integer.parseInt(cursor.getString(10)));
                pulsedetails.setIsActivePulse(Integer.parseInt(cursor.getString(11)));
                pulsedetails.setCreatedOnPluse(cursor.getString(12));
                pulsedetails.setUpdatedOn(cursor.getString(13));
                // Adding examswritten to list
                PulseExamPojo.add(pulsedetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return PulseExamPojo;
    }
    public List<ExamResponsePojo> addStudentExamByStudentID(int ExamId ) {
        List<ExamResponsePojo> examrespojo = new ArrayList<ExamResponsePojo>();
        // Select All Query

        // SELECT * FROM  TABLE WHERE   ID = (SELECT MAX(ID)  FROM TABLE);
/*
        private static final String TABLE_EXAM_RESPONSE_ID = "StuExamResponseID";
        private static final String RESPONSE_EXAM_ID = "ResponseExamID";
        private static final String RESPONSE_STUDENT_ID = "StudentID";
        private static final String RESPONSE_ROLL_NO = "RollNo";
        private static final String RESPONSE_STUDENT_NAME = "StudName";
        private static final String RESPONSE_TIME_TAKEN = "TimeTaken";
        private static final String RESPONSE_DATE_ATTENDED = "DateAttended";
        private static final String RESPONSE_TOTAL_SCORE = "TotalScore";*/
        // String selectQuery = "SELECT DISTINCT("+RESPONSE_STUDENT_ID+"),"+TABLE_EXAM_RESPONSE_ID+" FROM " +TABLE_STUDENT_EXAM_RESPONSE+" where " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'"+" ORDER BY " +RESPONSE_DATE_ATTENDED+ " DESC ";
        String selectQuery = "SELECT DISTINCT("+RESPONSE_STUDENT_ID+")"+" FROM " +TABLE_STUDENT_EXAM_RESPONSE+" where " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'";

        //  String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_EXAM_RESPONSE +" where " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamResponsePojo ansdetails = new ExamResponsePojo();
                //ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setStudentID(Integer.parseInt(cursor.getString(0)));
                // Adding examswritten to list
                examrespojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examrespojo;
    }
    public List<ExamResponsePojo> addStudentExamResponseahareportal(int ExamId ) {
        List<ExamResponsePojo> examrespojo = new ArrayList<ExamResponsePojo>();
        // Select All Query

        // SELECT * FROM  TABLE WHERE   ID = (SELECT MAX(ID)  FROM TABLE);
      //  String selectQuery = "SELECT * FROM " + TABLE_STUDENT_EXAM_RESPONSE+" where " +RESPONSE_EXAM_ID+ " = '" + ExamId + "'" + " ORDER BY " + TABLE_EXAM_RESPONSE_ID+ " DESC LIMIT 1";

         String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_EXAM_RESPONSE +" where " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamResponsePojo ansdetails = new ExamResponsePojo();
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setStudentID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setRollNo(Integer.parseInt(cursor.getString(3)));
                ansdetails.setStudName(cursor.getString(4));
                ansdetails.setTimeTaken(Integer.parseInt(cursor.getString(5)));
                ansdetails.setDateAttended(cursor.getString(6));
                ansdetails.setTotalScore(Integer.parseInt(cursor.getString(7)));
                // Adding examswritten to list
                examrespojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examrespojo;
    }
    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServer() {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE+" = '"+ "Teacher-Conducted"+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(5));
                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }
    // Getting All examswritten
    public List<PulseExamPojo> getAllPulseExamDetails() {
        List<PulseExamPojo> PulseExamPojo = new ArrayList<PulseExamPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PULSE_QUESTION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PulseExamPojo pulsedetails = new PulseExamPojo();
                pulsedetails.setPulseQuestionID(Integer.parseInt(cursor.getString(0)));
                pulsedetails.setPulseQuestion(cursor.getString(1));
                pulsedetails.setPulseType(cursor.getString(2));
                pulsedetails.setAnswerOption_A(cursor.getString(3));
                pulsedetails.setAnswerOption_B(cursor.getString(4));
                pulsedetails.setAnswerOption_C(cursor.getString(5));
                pulsedetails.setAnswerOption_D(cursor.getString(6));
                pulsedetails.setCorrectAnswerOption(cursor.getString(7));
                pulsedetails.setVersionPulse(Integer.parseInt(cursor.getString(8)));
                pulsedetails.setStaffIDPluse(Integer.parseInt(cursor.getString(9)));
                pulsedetails.setSchoolIDPulse(Integer.parseInt(cursor.getString(10)));
                pulsedetails.setIsActivePulse(Integer.parseInt(cursor.getString(11)));
                pulsedetails.setCreatedOnPluse(cursor.getString(12));
                pulsedetails.setUpdatedOn(cursor.getString(13));
                // Adding examswritten to list
                PulseExamPojo.add(pulsedetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return PulseExamPojo;
    }
    // Getting All examswritten
    public List<PulseResponsePojo> GetAllpulseFromquesid(int  PulseIDval) {
        List<PulseResponsePojo> pulse = new ArrayList<PulseResponsePojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PULSE_RESPONSE + " where " +PulseID+" = '"+ PulseIDval+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                PulseResponsePojo PulseResponsePojo = new PulseResponsePojo();
                PulseResponsePojo.setPulseID(Integer.parseInt(cursor.getString(1)));
                PulseResponsePojo.setPulseResponseID(Integer.parseInt(cursor.getString(0)));
                PulseResponsePojo.setStudentID(Integer.parseInt(cursor.getString(2)));
                PulseResponsePojo.setResponse(cursor.getString(3));
                PulseResponsePojo.setCreatedOn(cursor.getString(4));
                PulseResponsePojo.setModifiedOn(cursor.getString(5));
                pulse.add(PulseResponsePojo);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return pulse;
    }
    // Adding new examswritten
    public void addPuseSurveyResponse(PulseSurveyPojo PulseSurveyPojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SurveyPulseID, PulseSurveyPojo.getPulseSurveyID());
        values.put(SurveyPulseQuestionID, PulseSurveyPojo.getPulseQuestionID());
        values.put(ContentID, PulseSurveyPojo.getContentID());
        values.put(SurveyBatchID, PulseSurveyPojo.getBatchID());
        values.put(SurveySubjectID, PulseSurveyPojo.getSubjectID());
        values.put(SurveyStaffID, PulseSurveyPojo.getPulseSurveyID());
        values.put(PulseDatetime, PulseSurveyPojo.getPulseDatetime());
        values.put(PageNumber, PulseSurveyPojo.getPageNumber());
        values.put(Posted, PulseSurveyPojo.getPosted());
        values.put(optionA_Percentage, PulseSurveyPojo.getOptionA_Percentage());
        values.put(optionB_Percentage, PulseSurveyPojo.getOptionB_Percentage());
        values.put(optionC_Percentage, PulseSurveyPojo.getOptionC_Percentage());
        values.put(optionD_Percentage, PulseSurveyPojo.getOptionD_Percentage());
        values.put(NoResponse_Percentage, PulseSurveyPojo.getNoResponse_Percentage());
        values.put(NoOfPresentees, PulseSurveyPojo.getNoOfPresentees());
        values.put(SurveyCreatedOn, PulseSurveyPojo.getCreatedOn());
        values.put(SurveyModifiedOn, PulseSurveyPojo.getModifiedOn());


        // Inserting Row
        db.insert(TABLE_PULSE_SURVEY, null, values);
        db.close(); // Closing database connection
    }


    // Adding new examswritten
    public void addPuseResponse(PulseResponsePojo pulseresposePojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PulseID, pulseresposePojo.getPulseID());
        values.put(StudentID, pulseresposePojo.getStudentID());
        values.put(Response, pulseresposePojo.getResponse());
        values.put(ResponseCreatedOn, pulseresposePojo.getCreatedOn());
        values.put(ResponseModifiedOn, pulseresposePojo.getModifiedOn());

        // Inserting Row
        db.insert(TABLE_PULSE_RESPONSE, null, values);
        db.close(); // Closing database connection
    }
    public  Cursor OrderCommunication(){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c = con.rawQuery("SELECT * FROM tblCommunication ORDER BY datetime(DateOfCommunication) DESC", null);
        return c;
    }
    public Cursor getStudentName(int id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select FirstName from tblStudent where StudentID="+id, null);
        return c;
    }

    public Cursor getstafflogindetails(String id){
        SQLiteDatabase con=getReadableDatabase();
        Cursor c=con.rawQuery("select FirstName from tblstaff where PortalLoginID='"+id+"'", null);
        return c;
    }
    public ArrayList<Tablecontent> getcontentdetails()
    {
        ArrayList<Tablecontent>getcontent=new ArrayList<>();
     /*   "\"ContentDescription\" varchar ," +
                "\"Author\" varchar ," +
                "\"ContentTypeID\" integer ," +
                "\"ContentCatalogType\" varchar ," +
                "\"ContentFilename\" varchar ," +
                "\"BookShelfID\" integer," +
                "\"Vaporize\" varchar ," +
                "\"IsEncrypted\" integer ," +
                "\"EncryptionSalt\" varchar ," +
                "\"SchoolID\" integer ," +
                "\"BatchID\" integer," +
                "\"StaffID\" integer," +
                "\"CreatedOn\" datetime," +
                "\"Class\" varchar," +
                "\"Subject\" varchar " +*/

        String selectQuery = "SELECT  * FROM " + "tblContent";

        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        ///Staffdetails details=new Staffdetails();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Tablecontent content=new Tablecontent();

                content.setContentID(cursor.getInt(0));
                content.setContentFilename(cursor.getString(5));
                content.setVaporize(cursor.getString(7));
                content.setContentDescription(cursor.getString(1));
                content.setClasses(cursor.getString(14));
                content.setSubject(cursor.getString(15));
                content.setType(cursor.getString(16));
                content.setContentCatalogType(cursor.getString(4));

                // Log.e("name",cursor.getString(3));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
                getcontent.add(content);
            } while (cursor.moveToNext());
        }
        db.close();
        return  getcontent;


    }


public Cursor  checkschooldetails(int id)
{
    String selectQuery = "select *from tblSchool where SchoolID="+id+"";
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    return  cursor;
}



    public void updateschooldetails(Staffdetails details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

       /* public static final String school_id= "SchoolID" ;
        public static final String SchoolCategoryID=  "SchoolCategoryID" ;
        public static final String SchoolName= "SchoolName";
        public static final String Acronym=   "Acronym";
        public static final String SchoolLogo= "SchoolLogo" ;
        public static final String CityID=   "CityID" ;
        public static final String StateID= "StateID" ;
        public static final String CountryID=   "CountryID" ;
        public static final String CreatedOn=   "CreatedOn";
        public static final String ModifiedOn=  "ModifiedOn";*/

        values.put(school_id,details.getSchoolID());
        values.put(SchoolName,details.getSchoolName());
        values.put(SchoolLogo,details.getSchoolLogo());
        values.put(Acronym,details.getAcronym());
        values.put(Background, details.getBackground());

        int updatequery= db.update(Table_school,
                values,
                "SchoolID" + " = ?",
                new String[]{"" + details.getSchoolID()});

       // db.insert(Table_school, null, values);
        db.close(); // Closing database connection
    }
    public void schooldetails(Staffdetails details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

       /* public static final String school_id= "SchoolID" ;
        public static final String SchoolCategoryID=  "SchoolCategoryID" ;
        public static final String SchoolName= "SchoolName";
        public static final String Acronym=   "Acronym";
        public static final String SchoolLogo= "SchoolLogo" ;
        public static final String CityID=   "CityID" ;
        public static final String StateID= "StateID" ;
        public static final String CountryID=   "CountryID" ;
        public static final String CreatedOn=   "CreatedOn";
        public static final String ModifiedOn=  "ModifiedOn";*/

        values.put(school_id,details.getSchoolID());
        values.put(SchoolName,details.getSchoolName());
        values.put(SchoolLogo,details.getSchoolLogo());
        values.put(Acronym,details.getAcronym());
        values.put(Background,details.getBackground());



        db.insert(Table_school, null, values);
        db.close(); // Closing database connection
    }


    public Staffdetails getschooldetails()
    {
        //  String createschool="CREATE TABLE "+ Table_school+"("+ school_id +" INTEGER PRIMARY KEY," + SchoolCategoryID +" TEXT,"+ SchoolName+ " TEXT," + Acronym +" TEXT," + SchoolLogo +" TEXT,"+ CityID +" INTEGER,"+ StateID + " INTEGER,"+ CountryID +" INTEGER,"+CreatedOn + " TEXT,"+ModifiedOn +" TEXT,"+Background+ " TEXT"+")";

        String selectQuery = "SELECT  * FROM " + Table_school;

        //  SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Staffdetails details=new Staffdetails();

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                details.setSchoolID(cursor.getInt(0));
                details.setSchoolName(cursor.getString(2));
                details.setAcronym(cursor.getString(3));
                details.setBackground(cursor.getString(10));
                details.setSchoolLogo(cursor.getString(4));
                // Log.e("name",cursor.getString(3));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  details;


    }

    public void updatemasterinfo(Masterinfo details)
    {
  /*  String masterinfo="CREATE TABLE \"masterinfo\"" +
            "(" +
            "\"ID\" integer primary key not null ," +
            "\"SubjectName\" varchar ," +
            "\"SubjectDescription\" varchar ," +

            "\"Status\" varchar ," +
            "\"SchoolID\" integer ," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +
            " )";
    db.execSQL(masterinfo);*/

        SQLiteDatabase db = this.getWritableDatabase();
  /*  minfo.setID(getm.has("ID")?Integer.parseInt(getm.getString("ID")):0);
    minfo.setSubjectName(getm.has("SubjectName")?getm.getString("SubjectName"):"");

    minfo.setSchoolID(getm.has("SchoolID")?Integer.parseInt(getm.getString("SchoolID")):0);
    minfo.setStatus(getm.has("Status")?getm.getString("Status"):"");
    minfo.setSubjectDescription(getm.has("SubjectDescription")?getm.getString("SubjectDescription"):"");*/

        ContentValues values = new ContentValues();
        values.put("ID",details.getID() );
        values.put("SubjectName" ,details.getSubjectName() );
        values.put("SubjectDescription" ,details.getSubjectDescription() );
        values.put("Status"  ,details.getStatus());
        values.put("SchoolID",details.getSchoolID());
       // db.insert("masterinfo", null, values);

        int updatequery= db.update("masterinfo",
                values,
                "ID" + " = ?",
                new String[]{"" + details.getID()});
        db.close();
       // db.close();


    }

    public void masterinfo(Masterinfo details)
    {
  /*  String masterinfo="CREATE TABLE \"masterinfo\"" +
            "(" +
            "\"ID\" integer primary key not null ," +
            "\"SubjectName\" varchar ," +
            "\"SubjectDescription\" varchar ," +

            "\"Status\" varchar ," +
            "\"SchoolID\" integer ," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +
            " )";
    db.execSQL(masterinfo);*/

        SQLiteDatabase db = this.getWritableDatabase();
  /*  minfo.setID(getm.has("ID")?Integer.parseInt(getm.getString("ID")):0);
    minfo.setSubjectName(getm.has("SubjectName")?getm.getString("SubjectName"):"");

    minfo.setSchoolID(getm.has("SchoolID")?Integer.parseInt(getm.getString("SchoolID")):0);
    minfo.setStatus(getm.has("Status")?getm.getString("Status"):"");
    minfo.setSubjectDescription(getm.has("SubjectDescription")?getm.getString("SubjectDescription"):"");*/

        ContentValues values = new ContentValues();
        values.put("ID",details.getID() );
        values.put("SubjectName" ,details.getSubjectName() );
        values.put("SubjectDescription" ,details.getSubjectDescription() );
        values.put("Status"  ,details.getStatus());
        values.put("SchoolID",details.getSchoolID());
        db.insert("masterinfo", null, values);
        db.close();


    }
    public Cursor checkstudentdetails(int studentid)
    {
        String selectQuery = "select *from tblStudent where StudentID="+studentid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }

   /* public Cursor checkstudentdetails(int studentid,String staffid)
    {

        String selectQuery = "select *from tblStudent where StudentID="+studentid+"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }*/

    public void updateStudent(helper.Studentdetails details)
    {
       /* "\"StudentID\" integer primary key not null ," +
                "\"AdmissionNumber\" varchar ," +
                "\"DOA\" datetime ," +
                "\"FirstName\" varchar ," +
                "\"LastName\" varchar ," +
                "\"DOB\" datetime ," +
                "\"Gender\" varchar ," +
                "\"Phone_1\" varchar ," +
                "\"Phone_2\" varchar ," +
                "\"Email\" varchar ," +
                "\"FatherName\" varchar ," +
                "\"MotherName\" varchar ," +
                "\"GuardianMobileNumber\" varchar ," +
                "\"RollNo\" varchar ," +
                "\"ClassID\" integer ," +
                "\"BatchID\" integer ," +
                "\"SchoolID\" integer ," +
                "\"AcademicYear\" varchar ," +
                "\"Guardians\" varchar ," +
                "\"PhotoFilename\" varchar ," +
                "\"PortalUserID\" integer ," +
                "\"StudentLoginID\" varchar ," +
                "\"Version\" integer ," +
                "\"StaffID\" integer ," +
                "\"CreditPoints\" integer ," +
                "\"ConnectionStatus\" integer ," +
                "\"ManualAttendance\" integer ," +
                "\"NoOfPresent\" integer ," +
                "\"Credits\" integer," +
                "\"CreatedOn\" datetime ," +
                "\"ModifiedOn\" datetime " +
                " )";*/


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("StudentID",details.getStudentID());
        values.put("DOA",details.getDOA());
        values.put("AdmissionNumber",details.getAdmissionNumber());
        values.put("FirstName",details.getFirstName());
        values.put("LastName",details.getLastName());
        values.put("DOB",details.getDOB());
        values.put("Gender",details.getGender());
        values.put("Phone_1",details.getPhone_1());
        values.put("Phone_2",details.getPhone_2());

        values.put("Email",details.getEmail());
        values.put("FatherName",details.getFatherName());
        values.put("MotherName",details.getMotherName());
        values.put("GuardianMobileNumber",details.getGuardianMobileNumber());
        values.put("RollNo",details.getRollNo());
        values.put("ClassID",details.getClassID());
        values.put("BatchID",details.getBatchID());
        values.put("SchoolID",details.getSchoolID());
        values.put("AcademicYear",details.getAcademicYear());
        values.put("Guardians",details.getGuardians());
        values.put("PhotoFilename",details.getPhotoFilename());
        values.put("PortalUserID",details.getPortalUserID());
        values.put("StudentLoginID",details.getStudentLoginID());

        values.put("Version" ,details.getVersion());
        values.put("StaffID",details.getStaffID());
        values.put("CreditPoints",details.getCreditPoints());
        values.put("ConnectionStatus",details.getConnectionStatus());


        values.put("ManualAttendance",details.getManualAttendance());
        values.put("NoOfPresent",details.getNoOfPresent());
        values.put("Credits",details.getCredits());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn());
        values.put(tableroominfo, details.getRoominfo());

        int updatequery= db.update("tblStudent",
                values,
                "StudentID" + " = ?",
                new String[]{"" + details.getStudentID()});
    //    db.insert("tblStudent", null, values);


        db.close();


    }


    public void Student(helper.Studentdetails details)
    {
       /* "\"StudentID\" integer primary key not null ," +
                "\"AdmissionNumber\" varchar ," +
                "\"DOA\" datetime ," +
                "\"FirstName\" varchar ," +
                "\"LastName\" varchar ," +
                "\"DOB\" datetime ," +
                "\"Gender\" varchar ," +
                "\"Phone_1\" varchar ," +
                "\"Phone_2\" varchar ," +
                "\"Email\" varchar ," +
                "\"FatherName\" varchar ," +
                "\"MotherName\" varchar ," +
                "\"GuardianMobileNumber\" varchar ," +
                "\"RollNo\" varchar ," +
                "\"ClassID\" integer ," +
                "\"BatchID\" integer ," +
                "\"SchoolID\" integer ," +
                "\"AcademicYear\" varchar ," +
                "\"Guardians\" varchar ," +
                "\"PhotoFilename\" varchar ," +
                "\"PortalUserID\" integer ," +
                "\"StudentLoginID\" varchar ," +
                "\"Version\" integer ," +
                "\"StaffID\" integer ," +
                "\"CreditPoints\" integer ," +
                "\"ConnectionStatus\" integer ," +
                "\"ManualAttendance\" integer ," +
                "\"NoOfPresent\" integer ," +
                "\"Credits\" integer," +
                "\"CreatedOn\" datetime ," +
                "\"ModifiedOn\" datetime " +
                " )";*/


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("StudentID",details.getStudentID());
        values.put("DOA",details.getDOA());
        values.put("AdmissionNumber",details.getAdmissionNumber());
        values.put("FirstName",details.getFirstName());
        values.put("LastName",details.getLastName());
        values.put("DOB",details.getDOB());
        values.put("Gender",details.getGender());
        values.put("Phone_1",details.getPhone_1());
        values.put("Phone_2",details.getPhone_2());

        values.put("Email",details.getEmail());
        values.put("FatherName",details.getFatherName());
        values.put("MotherName",details.getMotherName());
        values.put("GuardianMobileNumber",details.getGuardianMobileNumber());
        values.put("RollNo",details.getRollNo());
        values.put("ClassID",details.getClassID());
        values.put("BatchID",details.getBatchID());
        values.put("SchoolID",details.getSchoolID());
        values.put("AcademicYear",details.getAcademicYear());
        values.put("Guardians",details.getGuardians());
        values.put("PhotoFilename",details.getPhotoFilename());
        values.put("PortalUserID",details.getPortalUserID());
        values.put("StudentLoginID",details.getStudentLoginID());

        values.put("Version" ,details.getVersion());
        values.put("StaffID",details.getStaffID());
        values.put("CreditPoints",details.getCreditPoints());
        values.put("ConnectionStatus",details.getConnectionStatus());


        values.put("ManualAttendance",details.getManualAttendance());
        values.put("NoOfPresent",details.getNoOfPresent());
        values.put("Credits",details.getCredits());
        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn());
        values.put(tableroominfo,details.getRoominfo());


        db.insert("tblStudent", null, values);
        db.close();


    }
    public ArrayList<helper.Studentdetails>getstudentdetails()
    {

        ArrayList<helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM " + "tblStudent";

        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                helper.Studentdetails details =new helper.Studentdetails();
                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                get.add(details);
           /* Log.e("name",cursor.getString(1));
              *//*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  get;

    }


    public helper.Studentdetails getstudentdetailsbyrollnumber(String rollnumber)
    {

   // <helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM " + "tblStudent where StudentID='"+rollnumber+"'";

        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                helper.Studentdetails details =new helper.Studentdetails();
                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
             //   get.add(details);
           /* Log.e("name",cursor.getString(1));
              *//*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
                return details;
            } while (cursor.moveToNext());
        }
        db.close();
        return  null;

    }
    public Cursor  checkclasses(int classesid,int schoolid)
    {
        String selectQuery = "select *from tblClasses where ClassID="+classesid+" and  SchoolID ="+schoolid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }


    public Cursor  checkclasses(int classesid,int schoolid,int staffid)
    {
        String selectQuery = "select *from tblClasses where ClassID="+classesid+" and  SchoolID ="+schoolid+" and  StaffID ="+staffid ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }

    public void updatetbclasses(tblclasses details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

      /*  String createclass= "CREATE TABLE "+tblClasses +"("+
                ClassID  +" INTEGER,"+
                ClassName +" TEXT,"+
                SectionName+" TEXT,"+
                Grade +" TEXT,"+
                ClassCode +" TEXT," +
                tblclassSchoolID +" INTEGER,"+
                DepartmentID+" INTEGER,"+
                tblclassStaffID +" INTEGER,"+
                InternetSSID +" TEXT,"+
                InternetPassword +" TEXT,"+
                InternetType+" TEXT,"+
                PushName+" TEXT,"+
                tblclassesCreatedOn+" TEXT,"+
                tblclassesModifiedOn+" TEXT"+")";*/

        ContentValues values = new ContentValues();
        values.put(ClassID,details.getClassID());
        values.put(ClassName,details.getClassName());
        values.put(SectionName,details.getSectionName());
        values.put(Grade,details.getGrade());
        values.put(ClassCode,details.getClassCode());
        values.put(tblclassSchoolID,details.getSchoolID());
        values.put(tblclassStaffID,details.getStaffID());
        values.put(InternetSSID,details.getInternetSSID());
        values.put(InternetPassword, details.getInternetPassword());

        values.put(InternetType,details.getInternetType());
        values.put(PushName,details.getPushName());
        values.put(tblclassesCreatedOn,details.getTblclassesCreatedOn());
        values.put(tblclassesModifiedOn,details.getTblclassesModifiedOn());
        values.put(classesBatchID, details.getBatchid());
        int updatequery=  db.update("tblClasses",
                values,
                "ClassID" + " = ? AND " + "SchoolID" + " = ?",
                new String[]{"" + details.getClassID(), "" + details.getSchoolID()});
        //db.insert(tblClasses, null, values);
        db.close();




    }

    public void tbclasses(tblclasses details)
    {
        SQLiteDatabase db = this.getWritableDatabase();

      /*  String createclass= "CREATE TABLE "+tblClasses +"("+
                ClassID  +" INTEGER,"+
                ClassName +" TEXT,"+
                SectionName+" TEXT,"+
                Grade +" TEXT,"+
                ClassCode +" TEXT," +
                tblclassSchoolID +" INTEGER,"+
                DepartmentID+" INTEGER,"+
                tblclassStaffID +" INTEGER,"+
                InternetSSID +" TEXT,"+
                InternetPassword +" TEXT,"+
                InternetType+" TEXT,"+
                PushName+" TEXT,"+
                tblclassesCreatedOn+" TEXT,"+
                tblclassesModifiedOn+" TEXT"+")";*/

        ContentValues values = new ContentValues();
        values.put(ClassID,details.getClassID());
        values.put(ClassName,details.getClassName());
        values.put(SectionName,details.getSectionName());
        values.put(Grade,details.getGrade());
        values.put(ClassCode,details.getClassCode());
        values.put(tblclassSchoolID,details.getSchoolID());
        values.put(tblclassStaffID,details.getStaffID());
        values.put(InternetSSID,details.getInternetSSID());
        values.put(InternetPassword, details.getInternetPassword());

        values.put(InternetType,details.getInternetType());
        values.put(PushName,details.getPushName());
        values.put(tblclassesCreatedOn,details.getTblclassesCreatedOn());
        values.put(tblclassesModifiedOn,details.getTblclassesModifiedOn());
        values.put(classesBatchID,details.getBatchid());

        db.insert(tblClasses, null, values);
        db.close();




    }


    //get classdetails


    public ArrayList<tblclasses> getclassdetails(String staffid)
    {
        String selectQuery = "SELECT  * FROM " + tblClasses +" where StaffID ='"+staffid+"'";

        ArrayList<tblclasses> addclasses=new ArrayList<>();
        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {




/*
                String createclass= "CREATE TABLE "+tblClasses +"("+
                        ClassID  +" INTEGER,"+
                        ClassName +" TEXT,"+
                        SectionName+" TEXT,"+
                        Grade +" TEXT,"+
                        ClassCode +" TEXT," +
                        tblclassSchoolID +" INTEGER,"+
                        DepartmentID+" INTEGER,"+
                        tblclassStaffID +" INTEGER,"+
                        InternetSSID +" TEXT,"+
                        InternetPassword +" TEXT,"+
                        InternetType+" TEXT,"+
                        PushName+" TEXT,"+
                        tblclassesCreatedOn+" TEXT,"+
                        tblclassesModifiedOn+" TEXT"+")";*/

                tblclasses in_class=new tblclasses();
                in_class.setClassID(cursor.getInt(0));
                in_class.setClassName(cursor.getString(1));
                in_class.setSectionName(cursor.getString(2));
                in_class.setGrade(cursor.getString(3));
                in_class.setClassCode(cursor.getString(4));
                in_class.setSchoolID(cursor.getInt(5));
                in_class.setDepartmentID(cursor.getInt(6));
                in_class.setStaffID(cursor.getInt(7));
                in_class.setInternetSSID(cursor.getString(8));
                in_class.setInternetPassword(cursor.getString(9));
                in_class.setInternetType(cursor.getString(10));
                in_class.setBatchid(cursor.getString(14));
                in_class.setRommornot(false);

                addclasses.add(in_class);


            } while (cursor.moveToNext());
        }
        db.close();

        return addclasses;

    }


    public ArrayList<tblclasses> getclassdetails()
    {
        String selectQuery = "SELECT  * FROM " + tblClasses;

        ArrayList<tblclasses> addclasses=new ArrayList<>();
        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {




/*
                String createclass= "CREATE TABLE "+tblClasses +"("+
                        ClassID  +" INTEGER,"+
                        ClassName +" TEXT,"+
                        SectionName+" TEXT,"+
                        Grade +" TEXT,"+
                        ClassCode +" TEXT," +
                        tblclassSchoolID +" INTEGER,"+
                        DepartmentID+" INTEGER,"+
                        tblclassStaffID +" INTEGER,"+
                        InternetSSID +" TEXT,"+
                        InternetPassword +" TEXT,"+
                        InternetType+" TEXT,"+
                        PushName+" TEXT,"+
                        tblclassesCreatedOn+" TEXT,"+
                        tblclassesModifiedOn+" TEXT"+")";*/

                tblclasses in_class=new tblclasses();
                in_class.setClassID(cursor.getInt(0));
                in_class.setClassName(cursor.getString(1));
                in_class.setSectionName(cursor.getString(2));
                in_class.setGrade(cursor.getString(3));
                in_class.setClassCode(cursor.getString(4));
                in_class.setSchoolID(cursor.getInt(5));
                in_class.setDepartmentID(cursor.getInt(6));
                in_class.setStaffID(cursor.getInt(7));
                in_class.setInternetSSID(cursor.getString(8));
                in_class.setInternetPassword(cursor.getString(9));
                in_class.setInternetType(cursor.getString(10));
                in_class.setBatchid(cursor.getString(14));
                in_class.setRommornot(false);

                addclasses.add(in_class);


            } while (cursor.moveToNext());
        }
        db.close();

        return addclasses;

    }



   /* public ArrayList<tblclasses> getclassdetails(String staffid)
    {
        String selectQuery = "SELECT  * FROM " + tblClasses+"where StaffID ='"+staffid+"'";

        ArrayList<tblclasses> addclasses=new ArrayList<>();
        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {




*//*
                String createclass= "CREATE TABLE "+tblClasses +"("+
                        ClassID  +" INTEGER,"+
                        ClassName +" TEXT,"+
                        SectionName+" TEXT,"+
                        Grade +" TEXT,"+
                        ClassCode +" TEXT," +
                        tblclassSchoolID +" INTEGER,"+
                        DepartmentID+" INTEGER,"+
                        tblclassStaffID +" INTEGER,"+
                        InternetSSID +" TEXT,"+
                        InternetPassword +" TEXT,"+
                        InternetType+" TEXT,"+
                        PushName+" TEXT,"+
                        tblclassesCreatedOn+" TEXT,"+
                        tblclassesModifiedOn+" TEXT"+")";*//*

                tblclasses in_class=new tblclasses();
                in_class.setClassID(cursor.getInt(0));
                in_class.setClassName(cursor.getString(1));
                in_class.setSectionName(cursor.getString(2));
                in_class.setGrade(cursor.getString(3));
                in_class.setClassCode(cursor.getString(4));
                in_class.setSchoolID(cursor.getInt(5));
                in_class.setDepartmentID(cursor.getInt(6));
                in_class.setStaffID(cursor.getInt(7));
                in_class.setInternetSSID(cursor.getString(8));
                in_class.setInternetPassword(cursor.getString(9));
                in_class.setInternetType(cursor.getString(10));
                in_class.setBatchid(cursor.getString(14));
                in_class.setRommornot(false);

                addclasses.add(in_class);


            } while (cursor.moveToNext());
        }
        db.close();

        return addclasses;

    }*/
/*public ArrayList<Subjectnameandid>getbatchdetails(int id)
{
    String selectQuery = "SELECT  * FROM " + "tblBatch where ClassID = '"+ id+"'"  ;

    ArrayList<Subjectnameandid> addclasses=new ArrayList<>();
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
        do {
            String getsubjectid = "select * from tblSubject where BatchID = '" + cursor.getInt(0) + "'";
            SQLiteDatabase subject = this.getWritableDatabase();
            Cursor cursorsub = subject.rawQuery(getsubjectid, null);
            if (cursorsub.moveToFirst()) {

              *//*  String masterinfo="CREATE TABLE \"masterinfo\"" +
                        "(" +
                        "\"ID\" integer primary key not null ," +
                        "\"SubjectName\" varchar ," +
                        "\"SubjectDescription\" varchar ," +

                        "\"Status\" varchar ," +
                        "\"SchoolID\" integer ," +
                        "\"CreatedOn\" datetime ," +
                        "\"ModifiedOn\" datetime " +
                        " )";*//*
                do {


                    String getsubjectname = "select * from masterinfo where ID = '" + cursorsub.getInt(0) + "'";
                    SQLiteDatabase subjectname = this.getWritableDatabase();
                    Cursor cursorsubname = subjectname.rawQuery(getsubjectname, null);
                    if (cursorsubname.moveToFirst()) {

                        Subjectnameandid nameid=new Subjectnameandid();
                        nameid.setId(""+cursorsubname.getInt(0));
                        nameid.setName(cursorsubname.getString(1));
                        addclasses.add(nameid);

                        Log.e("subjectname", cursorsubname.getString(1));
                    }
                    cursorsubname.close();


                } while (cursorsub.moveToNext());


            }
            cursorsub.close();

        } while (cursor.moveToNext()) ;
    }
    db.close();

    return  addclasses;
}*/


    public ArrayList<Subjectnameandid> getbatchdetailsfrommasterinfo()
    {
        String selectQuery = "SELECT  * FROM " + "masterinfo"  ;

        ArrayList<Subjectnameandid> addclasses=new ArrayList<>();
        //   SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Subjectnameandid nameid=new Subjectnameandid();

                nameid.setId(""+cursor.getInt(0));
                nameid.setName(cursor.getString(1));
                nameid.setBatchid("0");
                addclasses.add(nameid);
            }while (cursor.moveToNext());
            }
        return addclasses;

    }


    public ArrayList<Subjectnameandid> getbatchdetails(int id,int batchid,int staffid)
    {
        String selectQuery = "SELECT  * FROM " + "tblBatch where ClassID = '"+ id+"' and BatchID = '"+batchid+"' and StaffID = '"+staffid+"'"  ;

        ArrayList<Subjectnameandid> addclasses=new ArrayList<>();
        //   SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String getsubjectid = "select * from tblSubject where BatchID = '" + cursor.getInt(0) + "'";
                // SQLiteDatabase subject = this.getWritableDatabase();
                SQLiteDatabase subject = this.getReadableDatabase();

                Cursor cursorsub = subject.rawQuery(getsubjectid, null);
                if (cursorsub.moveToFirst()) {

              /*  String masterinfo="CREATE TABLE \"masterinfo\"" +
                        "(" +
                        "\"ID\" integer primary key not null ," +
                        "\"SubjectName\" varchar ," +
                        "\"SubjectDescription\" varchar ," +

                        "\"Status\" varchar ," +
                        "\"SchoolID\" integer ," +
                        "\"CreatedOn\" datetime ," +
                        "\"ModifiedOn\" datetime " +
                        " )";*/
                    do {


                        String getsubjectname = "select * from masterinfo where ID = '" + cursorsub.getInt(0) + "'";
                        // SQLiteDatabase subjectname = this.getWritableDatabase();
                        SQLiteDatabase subjectname = this.getReadableDatabase();
                        Cursor cursorsubname = subjectname.rawQuery(getsubjectname, null);
                        if (cursorsubname.moveToFirst()) {

                            Subjectnameandid nameid=new Subjectnameandid();
                            nameid.setId(""+cursorsubname.getInt(0));
                            nameid.setName(cursorsubname.getString(1));
                            nameid.setBatchid(""+cursorsub.getInt(5));
                            addclasses.add(nameid);

                            Log.e("subjectname", cursorsubname.getString(1));
                        }
                        cursorsubname.close();


                    } while (cursorsub.moveToNext());


                }
                cursorsub.close();

            } while (cursor.moveToNext()) ;
        }
        db.close();

        return  addclasses;
    }

    public  boolean CheckIsIDAlreadypukseDBorNot(int fieldValue) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_PULSE_QUESTION + " where " + PulseQuestionID + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public  boolean CheckIsIDAlreadyInDBorNot(String fieldValue) {
        SQLiteDatabase sqldb = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_EXAM_DETAILS + " where " + EXAM_ID + " = " + fieldValue;
        Cursor cursor = sqldb.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }




    // Updating single examswritten
    public int downloadUpdateQuestionDetailsByID(QuestionDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, examlist.getQuestionID()); // examswritten Name
        values.put(QUESTION_EXAM_ID, examlist.getExamID()); // examswritten Phone
        values.put(TOPIC_ID, examlist.getTopicID()); // examswritten Phone
        values.put(TOPIC_NAME, examlist.getTopicName()); // examswritten Phone
        values.put(ASPECT_ID, examlist.getAspectID()); // examswritten Phone
        values.put(ASPECT, examlist.getAspect()); // examswritten Phone
        values.put(QUESTION, examlist.getQuestion()); // examswritten Phone
        values.put(QUESTION_NUMBER, examlist.getQuestionNumber()); // examswritten Phone
        values.put(OPTION_A, examlist.getOptionA()); // examswritten Phone
        values.put(OPTION_B, examlist.getOptionB()); // examswritten Phone
        values.put(OPTION_C, examlist.getOptionC()); // examswritten Phone
        values.put(OPTION_D, examlist.getOptionD()); // examswritten Phone
        values.put(QUESTION_CORRECT_ANSWER, examlist.getCorrectAnswer()); // examswritten Phone
        values.put(MARK, examlist.getMark()); // examswritten Phone
        values.put(NEGATIVE_MARK, examlist.getNegative_Mark()); // examswritten Phone
        values.put(STUDENT_ANSWER, examlist.getStudentAnswer()); // examswritten Phone
        values.put(IS_CORRECT, examlist.getIsCorrect()); // examswritten Phone
        values.put(OBTAINED_SCORE, examlist.getObtainedScore()); // examswritten Phone
        values.put(CREATED_ON, examlist.getCreatedOn()); // examswritten Phone
        values.put(MODIFIED_ON, examlist.getModifiedOn()); // examswritten Phone

        // updating row
        return db.update(TABLE_EXAM_QUESTION, values, QUESTION_ID + " = ?",
                new String[] { String.valueOf(examlist.getExamID()) });
    }
    // Updating single examswritten
    public int downloadUpdateExamDetails(ExamDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_ID, examlist.getExamID());
        values.put(EXAM__CATEGORY_ID, examlist.getExamCategoryID());
        values.put(EXAM_CATEGORY_NAME, examlist.getExamCategoryName());
        values.put(EXAM_CODE, examlist.getExamCode());
        values.put(EXAM_DESCRIPTION, examlist.getExamDescription());
        values.put(EXAM_SEQUENCE, examlist.getExamSequence());
        values.put(EXAM_DATE, examlist.getExamDate());
        values.put(EXAM_TYPE_ID, examlist.getExamTypeID());
        values.put(EXAM_TYPE, examlist.getExamType());
        values.put(SUBJECT_ID, examlist.getSubjectID());
        values.put(SUBJECT, examlist.getSubject());
        values.put(EXAM_DURATION, examlist.getExamDuration());
        values.put(SCHOOL_ID, examlist.getSchoolID());
        values.put(CLASS_ID, examlist.getClassID());
        values.put(BATCH_ID, examlist.getBatchID());
        values.put(IS_RESULT_PUBLISHED, examlist.getIsResultPublished());
        values.put(EXAM_SHELF_ID, examlist.getExamShelfID());
        values.put(TIME_TAKEN, examlist.getTimeTaken());
        values.put(DATE_ATTENDED, examlist.getDateAttended());
        values.put(TOTAL_SCORE, examlist.getTotalScore());

        // updating row
        return db.update(TABLE_EXAM_DETAILS, values, EXAM_ID + " = ?",
                new String[] { String.valueOf(examlist.getExamID()) });
    }

    // Getting All examswritten
    public List<ExamDetails> GetAllExamFromServerSelf(String subjectID, String Staffid) {
        List<ExamDetails> examdetails = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_TYPE+" = '"+ "Self-Evaluation"+"' and "+SUBJECT_ID+"= '"+subjectID+"'"+" and "+STAFF_ID+"= '"+Staffid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetailscal = new ExamDetails();
                examdetailscal.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetailscal.setExamDescription(cursor.getString(4));
                examdetails.add(examdetailscal);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetails;
    }
    public Cursor  checkbatch(int classesid,int batchid)
    {
        String selectQuery = "select *from tblBatch where ClassID="+classesid+" and  BatchID ="+batchid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }

    public Cursor  checkbatch(int classesid,int batchid,int staffid)
    {
        String selectQuery = "select *from tblBatch where ClassID="+classesid+" and  BatchID ="+batchid+" and  StaffID ="+staffid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }
    public void updatebatch(Batchdetails details)
    {
       /* "(" +
                "\"BatchID\" integer primary key not null ," +
                "\"CoordinatingStaffID\" Integer" +
                "\"AcademicYear\" varchar ," +
                "\"SchoolID\" integer," +
                "\"ClassID\" Integer," +
                "\"StaffID\" Integer," +
                "\"BatchStartDate\" datetime," +
                "\"BatchEndDate\" datetime, " +
                "\"TimeTable\" varchar," +
                "\"IsActive\" integer," +
                "\"CreatedOn\" datetime," +
                "\"ModifiedOn\" datetime" +
                ")";*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BatchID",details.getBatchID());
        values.put("CoordinatingStaffID",details.getCoordinatingStaffID());
        values.put("AcademicYear",details.getAcademicYear());
        values.put("SchoolID" ,details.getSchoolID());
        values.put("ClassID",details.getClassID());
        values.put("StaffID",details.getStaffID());
        values.put("BatchStartDate",details.getBatchStartDate());
        values.put("BatchEndDate" ,details.getBatchEndDate());
        values.put("TimeTable",details.getTimeTable());
        values.put("IsActive",details.getIsActive());

        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn());
        values.put("Batchcode", details.getBatchcode());
        int updatequery=  db.update("tblBatch",
                values,
                "ClassID" + " = ? AND " + "BatchID" + " = ?",
                new String[]{"" + details.getClassID(), "" + details.getBatchID()});
       // db.insert("tblBatch", null, values);
        db.close();
    }




    public void batch(Batchdetails details)
    {
       /* "(" +
                "\"BatchID\" integer primary key not null ," +
                "\"CoordinatingStaffID\" Integer" +
                "\"AcademicYear\" varchar ," +
                "\"SchoolID\" integer," +
                "\"ClassID\" Integer," +
                "\"StaffID\" Integer," +
                "\"BatchStartDate\" datetime," +
                "\"BatchEndDate\" datetime, " +
                "\"TimeTable\" varchar," +
                "\"IsActive\" integer," +
                "\"CreatedOn\" datetime," +
                "\"ModifiedOn\" datetime" +
                ")";*/
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("BatchID",details.getBatchID());
        values.put("CoordinatingStaffID",details.getCoordinatingStaffID());
        values.put("AcademicYear",details.getAcademicYear());
        values.put("SchoolID" ,details.getSchoolID());
        values.put("ClassID",details.getClassID());
        values.put("StaffID",details.getStaffID());
        values.put("BatchStartDate",details.getBatchStartDate());
        values.put("BatchEndDate" ,details.getBatchEndDate());
        values.put("TimeTable",details.getTimeTable());
        values.put("IsActive",details.getIsActive());

        values.put("CreatedOn",details.getCreatedOn());
        values.put("ModifiedOn",details.getModifiedOn());
        values.put("Batchcode",details.getBatchcode());

        db.insert("tblBatch", null, values);
        db.close();
    }

//getroomwisestudent

    public ArrayList< helper.Studentdetails> getdetails(int staffid)
    {

        ArrayList< helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM   tblStudent where StaffID= '"+staffid+
                "'" ;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //  helper.Studentdetails details =new helper.Studentdetails();
                helper.Studentdetails  details = new helper.Studentdetails();

                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                details.setRoominfo(cursor.getString(31));
                get.add(details);
           /* Log.e("name",cursor.getString(1));
              //  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  get;

    }

    public ArrayList< helper.Studentdetails> getdetailsofbatch(int classid,int staffid,int batchid)
    {

        ArrayList< helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM   tblStudent where StaffID= '"+staffid+
                "' and ClassID= '"+classid+"' and BatchID ='"+batchid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //  helper.Studentdetails details =new helper.Studentdetails();
                helper.Studentdetails  details = new helper.Studentdetails();

                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                details.setRoominfo(cursor.getString(31));
                get.add(details);
           /* Log.e("name",cursor.getString(1));
              //  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  get;

    }

    public ArrayList< helper.Studentdetails> getdetails(int classid,int staffid)
    {

        ArrayList< helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM   tblStudent where StaffID= '"+staffid+
                "' and ClassID= '"+classid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //  helper.Studentdetails details =new helper.Studentdetails();
                helper.Studentdetails  details = new helper.Studentdetails();

                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                details.setRoominfo(cursor.getString(31));
                get.add(details);
           /* Log.e("name",cursor.getString(1));
              //  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  get;

    }

    public Batchdetails getbatchtimetable(int classid,int batchid)

    {

        String selectQuery = "SELECT  * FROM " + "tblBatch where ClassID = '" + classid + "' and BatchID = '" + batchid + "'";

        Batchdetails batchdetails = new Batchdetails();
        // ArrayList<tblclasses> addclasses=new ArrayList<>();
        // SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                batchdetails.setTimeTable(cursor.getString(cursor.getColumnIndex("TimeTable")));

            } while (cursor.moveToNext());
        }
        return batchdetails;

    }

    public Cursor  checksubject(int classesid,int batchid)
    {
        String selectQuery = "select *from tblBatch where ClassID="+classesid+" and  BatchID ="+batchid;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return  cursor;
    }
    public void updatesubject(helper.tblSubject details)
    {
       /* public static final String  tblSubject="tblSubject";

        public static final String    SubjectID  = "SubjectID" ;
        public static final String    SubjectName  =    "SubjectName"  ;
        public static final String    tblSubjectSchoolID  =    "SchoolID"  ;
        public static final String     tblSubjectStaffID =  "StaffID"  ;
        public static final String     tblSubjectClassID =  "ClassID"  ;
        public static final String      BatchID =  "BatchID" ;
        public static final String      tblsubectCreatedOn=  "CreatedOn" ;
        public static final String      tblsubectModifiedOn = "ModifiedOn" ;*/


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SubjectID,details.getSubjectID());
        values.put(SubjectName,details.getSubjectName());
        values.put(tblSubjectSchoolID,details.getSchoolID());
        values.put(tblSubjectStaffID ,details.getStaffID());
        values.put(tblSubjectClassID,details.getClassID());
        values.put( BatchID,details.getBatchID());
        values.put(tblsubectCreatedOn ,details.getCreatedOn());
        values.put(tblsubectModifiedOn,details.getModifiedOn());


        db.insert(tblSubject, null, values);
        db.close();


    }

    public void deletesubject() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + tblSubject);

    }

    public void deletesubject(String staffId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + tblSubject +" where StaffID ='"+staffId+"'");

    }

    public void subject(helper.tblSubject details)
    {
       /* public static final String  tblSubject="tblSubject";

        public static final String    SubjectID  = "SubjectID" ;
        public static final String    SubjectName  =    "SubjectName"  ;
        public static final String    tblSubjectSchoolID  =    "SchoolID"  ;
        public static final String     tblSubjectStaffID =  "StaffID"  ;
        public static final String     tblSubjectClassID =  "ClassID"  ;
        public static final String      BatchID =  "BatchID" ;
        public static final String      tblsubectCreatedOn=  "CreatedOn" ;
        public static final String      tblsubectModifiedOn = "ModifiedOn" ;*/


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SubjectID,details.getSubjectID());
        values.put(SubjectName,details.getSubjectName());
        values.put(tblSubjectSchoolID,details.getSchoolID());
        values.put(tblSubjectStaffID ,details.getStaffID());
        values.put(tblSubjectClassID,details.getClassID());
        values.put( BatchID,details.getBatchID());
        values.put(tblsubectCreatedOn ,details.getCreatedOn());
        values.put(tblsubectModifiedOn,details.getModifiedOn());


        db.insert(tblSubject, null, values);
        db.close();


    }

public Cursor checkstaffdetails(int staffid)
{
    String selectQuery = "select *from tblStaff where StaffID="+staffid+"";
    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);
    return  cursor;
}
    public void updatestafflogindetails(Staffloingdetails details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();



/*

        staffdetails=new Staffloingdetails();
        staffdetails.setStaffID(staffinfo.has("ID")?Integer.parseInt(staffinfo.getString("ID")):0);
        staffdetails.setFirstName(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
        staffdetails.setDOJ(staffinfo.has("DateOfJoining")?staffinfo.getString("DateOfJoining"):"");
        //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
        staffdetails.setLastName(staffinfo.has("LastName")?staffinfo.getString("LastName"):"");
        staffdetails.setGender(staffinfo.has("Gender")?staffinfo.getString("Gender"):"");
        staffdetails.setDOB(staffinfo.has("DateOfBirth")?staffinfo.getString("DateOfBirth"):"");
        staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus")?staffinfo.getString("MaritalStatus"):"");;
        staffdetails.setSpouseName(staffinfo.has("SpuseName")?staffinfo.getString("SpuseName"):"");
        staffdetails.setFatherName(staffinfo.has("FatherName")?staffinfo.getString("FatherName"):"");
        staffdetails.setMotherName(staffinfo.has("MotherName")?staffinfo.getString("MotherName"):"");
        staffdetails.setPhone((staffinfo.has("PhoneNumber")?staffinfo.getString("PhoneNumber"):""));
        staffdetails.setPhotoFilename(staffinfo.has("ProfileImage")?staffinfo.getString("ProfileImage"):"");
        staffdetails.setEmail(staffinfo.has("ProfileImage")?staffinfo.getString("ProfileImage"):"");
        staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade")?staffinfo.getString("EmploymentGrade"):"");
        staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory")?Integer.parseInt(staffinfo.getString("StaffCategory")):0);
*/
        values.put(StaffID,details.getStaffID());
        values.put(EmployementNumber,details.getEmployementNumber());
        values.put(DOJ,details.getDOJ());
        values.put(FirstName,details.getFirstName());
        values.put(MiddleName,details.getMiddleName());
        values.put(LastName,details.getLastName());
        values.put(Gender,details.getGender());
        values.put(DOB,details.getDOB());
        values.put(MaritalStatusID,details.getMaritalStatusID());
        values.put(SpouseName,details.getSpouseName());
        values.put(FatherName,details.getFatherName());
        values.put(MotherName,details.getMotherName());
        values.put(Phone,details.getPhone());
        values.put(Email,details.getEmail());
        values.put(PhotoFilename,details.getPhotoFilename());
        values.put(StaffCategoryID,details.getStaffCategoryID());
        values.put(SchoolID,details.getSchoolID());
        values.put(DeptartmentID,details.getDeptartmentID());
        values.put(DesignationID,details.getDesignationID());
        values.put(PortalUserID,details.getPortalUserID());
        values.put(PortalLoginID,details.getPortalLoginID());
        values.put(tablestaffCreatedOn,details.getCreatedOn());
        values.put(tablestaffModifiedOn,details.getModifiedOn());
        values.put(tableroominfo,details.getRoominfo());
        values.put(timetable,details.getTimetable());

      /*  values.put(school_id,details.getSchoolID());
        values.put(school_id,details.getSchoolID());*/







       /* private static final String Tablestaff= "tblStaff";

        public static final String   StaffID=  "StaffID" ;
        public static final String   EmployementNumber=   "EmployementNumber" ;
        public static final String      DOJ= "DOJ" ;
        public static final String      FirstName= "FirstName";
        public static final String      MiddleName= "MiddleName";
        public static final String     LastName= "LastName" ;
        public static final String     Gender= "Gender";
        public static final String     DOB= "DOB" ;
        public static final String     MaritalStatusID="MaritalStatusID" ;
        public static final String    SpouseName= "SpouseName" ;
        public static final String   FatherName=  "FatherName" ;
        public static final String    MotherName= "MotherName" ;
        public static final String    Phone=  "Phone" ;
        public static final String    Email=  "Email" ;
        public static final String     PhotoFilename=  "PhotoFilename" ;
        public static final String     StaffCategoryID= "StaffCategoryID" ;
        public static final String    SchoolID=  "SchoolID";
        public static final String      DeptartmentID= "DeptartmentID" ;
        public static final String    DesignationID=  "DesignationID" ;
        public static final String     PortalUserID= "PortalUserID" ;
        public static final String    PortalLoginID=  "PortalLoginID" ;
        public static final String    tablestaffCreatedOn= "CreatedOn" ;
        public static final String    tablestaffModifiedOn= "ModifiedOn";
        */

        int updatequery= db.update(Tablestaff,
                values,
                "StaffID" + " = ?",
                new String[]{"" + details.getStaffID()});

       // db.insert(Tablestaff, null, values);
        db.close(); // Closing database connection
    }


    public void stafflogindetails(Staffloingdetails details) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();



/*

        staffdetails=new Staffloingdetails();
        staffdetails.setStaffID(staffinfo.has("ID")?Integer.parseInt(staffinfo.getString("ID")):0);
        staffdetails.setFirstName(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
        staffdetails.setDOJ(staffinfo.has("DateOfJoining")?staffinfo.getString("DateOfJoining"):"");
        //staffdetails.setDeptartmentID(staffinfo.has("FirstName")?staffinfo.getString("FirstName"):"");
        staffdetails.setLastName(staffinfo.has("LastName")?staffinfo.getString("LastName"):"");
        staffdetails.setGender(staffinfo.has("Gender")?staffinfo.getString("Gender"):"");
        staffdetails.setDOB(staffinfo.has("DateOfBirth")?staffinfo.getString("DateOfBirth"):"");
        staffdetails.setMaritalStatusID(staffinfo.has("MaritalStatus")?staffinfo.getString("MaritalStatus"):"");;
        staffdetails.setSpouseName(staffinfo.has("SpuseName")?staffinfo.getString("SpuseName"):"");
        staffdetails.setFatherName(staffinfo.has("FatherName")?staffinfo.getString("FatherName"):"");
        staffdetails.setMotherName(staffinfo.has("MotherName")?staffinfo.getString("MotherName"):"");
        staffdetails.setPhone((staffinfo.has("PhoneNumber")?staffinfo.getString("PhoneNumber"):""));
        staffdetails.setPhotoFilename(staffinfo.has("ProfileImage")?staffinfo.getString("ProfileImage"):"");
        staffdetails.setEmail(staffinfo.has("ProfileImage")?staffinfo.getString("ProfileImage"):"");
        staffdetails.setEmployementNumber(staffinfo.has("EmploymentGrade")?staffinfo.getString("EmploymentGrade"):"");
        staffdetails.setStaffCategoryID(staffinfo.has("StaffCategory")?Integer.parseInt(staffinfo.getString("StaffCategory")):0);
*/
        values.put(StaffID,details.getStaffID());
        values.put(EmployementNumber,details.getEmployementNumber());
        values.put(DOJ,details.getDOJ());
        values.put(FirstName,details.getFirstName());
        values.put(MiddleName,details.getMiddleName());
        values.put(LastName,details.getLastName());
        values.put(Gender,details.getGender());
        values.put(DOB,details.getDOB());
        values.put(MaritalStatusID,details.getMaritalStatusID());
        values.put(SpouseName,details.getSpouseName());
        values.put(FatherName,details.getFatherName());
        values.put(MotherName,details.getMotherName());
        values.put(Phone,details.getPhone());
        values.put(Email,details.getEmail());
        values.put(PhotoFilename,details.getPhotoFilename());
        values.put(StaffCategoryID,details.getStaffCategoryID());
        values.put(SchoolID,details.getSchoolID());
        values.put(DeptartmentID,details.getDeptartmentID());
        values.put(DesignationID,details.getDesignationID());
        values.put(PortalUserID,details.getPortalUserID());
        values.put(PortalLoginID,details.getPortalLoginID());
        values.put(tablestaffCreatedOn,details.getCreatedOn());
        values.put(tablestaffModifiedOn,details.getModifiedOn());
        values.put(tableroominfo,details.getRoominfo());
        values.put(timetable,details.getTimetable());

      /*  values.put(school_id,details.getSchoolID());
        values.put(school_id,details.getSchoolID());*/







       /* private static final String Tablestaff= "tblStaff";

        public static final String   StaffID=  "StaffID" ;
        public static final String   EmployementNumber=   "EmployementNumber" ;
        public static final String      DOJ= "DOJ" ;
        public static final String      FirstName= "FirstName";
        public static final String      MiddleName= "MiddleName";
        public static final String     LastName= "LastName" ;
        public static final String     Gender= "Gender";
        public static final String     DOB= "DOB" ;
        public static final String     MaritalStatusID="MaritalStatusID" ;
        public static final String    SpouseName= "SpouseName" ;
        public static final String   FatherName=  "FatherName" ;
        public static final String    MotherName= "MotherName" ;
        public static final String    Phone=  "Phone" ;
        public static final String    Email=  "Email" ;
        public static final String     PhotoFilename=  "PhotoFilename" ;
        public static final String     StaffCategoryID= "StaffCategoryID" ;
        public static final String    SchoolID=  "SchoolID";
        public static final String      DeptartmentID= "DeptartmentID" ;
        public static final String    DesignationID=  "DesignationID" ;
        public static final String     PortalUserID= "PortalUserID" ;
        public static final String    PortalLoginID=  "PortalLoginID" ;
        public static final String    tablestaffCreatedOn= "CreatedOn" ;
        public static final String    tablestaffModifiedOn= "ModifiedOn";
        */



        db.insert(Tablestaff, null, values);
        db.close(); // Closing database connection
    }
    public void studentinfo() {

        String selectQuery = "SELECT  * FROM " + "tblStudent";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.e("name",cursor.getString(3));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
    }
    public void masterinfodetails() {

        String selectQuery = "SELECT  * FROM " + "masterinfo";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.e("name",cursor.getString(1));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
       /* SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SchoolCategoryID, "test"); // Contact Name
       // values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone Number

        // Inserting Row
        db.insert(Table_school, null, values);
        db.close(); // Closing database connection*/
    }
    public void stafflogin(Staffloingdetails details) {
       /* SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SchoolCategoryID, "test"); // Contact Name
       // values.put(KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone Number

        // Inserting Row
        db.insert(Table_school, null, values);
        db.close(); // Closing database connection*/
    }


    public Staffdetails getAllContacts() {
        //List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Table_school;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.e("name",cursor.getString(3));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        // return contact list
        return null;
    }


    public Staffloingdetails getallstafflogindetails() {
        //List<Contact> contactList = new ArrayList<Contact>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Tablestaff;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Log.e("name",cursor.getString(2));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }

        // return contact list
        return null;
    }





    public Staffloingdetails staffdetails()
    {


      /*  public static final String   StaffID=  "StaffID" ;
        public static final String   EmployementNumber=   "EmployementNumber" ;
        public static final String      DOJ= "DOJ" ;
        public static final String      FirstName= "FirstName";
        public static final String      MiddleName= "MiddleName";
        public static final String     LastName= "LastName" ;
        public static final String     Gender= "Gender";
        public static final String     DOB= "DOB" ;
        public static final String     MaritalStatusID="MaritalStatusID" ;
        public static final String    SpouseName= "SpouseName" ;
        public static final String   FatherName=  "FatherName" ;
        public static final String    MotherName= "MotherName" ;
        public static final String    Phone=  "Phone" ;
        public static final String    Email=  "Email" ;
        public static final String     PhotoFilename=  "PhotoFilename" ;
        public static final String     StaffCategoryID= "StaffCategoryID" ;
        public static final String    SchoolID=  "SchoolID";
        public static final String      DeptartmentID= "DeptartmentID" ;
        public static final String    DesignationID=  "DesignationID" ;
        public static final String     PortalUserID= "PortalUserID" ;
        public static final String    PortalLoginID=  "PortalLoginID" ;
        public static final String    tablestaffCreatedOn= "CreatedOn" ;
        public static final String    tablestaffModifiedOn= "ModifiedOn";*/

        String selectQuery = "SELECT  * FROM " + Tablestaff;

        Staffloingdetails details =new Staffloingdetails();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(0));
                details.setEmployementNumber(cursor.getString(1));
                details.setDOJ(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setMiddleName(cursor.getString(4));
                details.setLastName(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setDOB(cursor.getString(7));
                details.setMaritalStatusID(cursor.getString(8));
                details.setSpouseName(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setPhone(cursor.getString(12));
                details.setEmail(cursor.getString(13));
                details.setPhotoFilename(cursor.getString(14));
                details.setStaffCategoryID(cursor.getInt(15));
                details.setSchoolID(cursor.getString(16));
                details.setDeptartmentID(cursor.getInt(17));
                details.setDesignationID(cursor.getInt(18));
                details.setPortalUserID(cursor.getString(19));
                details.setPortalLoginID(cursor.getString(20));
                details.setCreatedOn(cursor.getString(21));
                details.setModifiedOn(cursor.getString(22));
                details.setRoominfo(cursor.getString(23));
                details.setTimetable(cursor.getString(cursor.getColumnIndex(timetable)));

                //Log.e("name",cursor.getString(2));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }

        return  details;
    }


    public Staffloingdetails staffdetailsuserlogin(String userlogin)
    {


      /*  public static final String   StaffID=  "StaffID" ;
        public static final String   EmployementNumber=   "EmployementNumber" ;
        public static final String      DOJ= "DOJ" ;
        public static final String      FirstName= "FirstName";
        public static final String      MiddleName= "MiddleName";
        public static final String     LastName= "LastName" ;
        public static final String     Gender= "Gender";
        public static final String     DOB= "DOB" ;
        public static final String     MaritalStatusID="MaritalStatusID" ;
        public static final String    SpouseName= "SpouseName" ;
        public static final String   FatherName=  "FatherName" ;
        public static final String    MotherName= "MotherName" ;
        public static final String    Phone=  "Phone" ;
        public static final String    Email=  "Email" ;
        public static final String     PhotoFilename=  "PhotoFilename" ;
        public static final String     StaffCategoryID= "StaffCategoryID" ;
        public static final String    SchoolID=  "SchoolID";
        public static final String      DeptartmentID= "DeptartmentID" ;
        public static final String    DesignationID=  "DesignationID" ;
        public static final String     PortalUserID= "PortalUserID" ;
        public static final String    PortalLoginID=  "PortalLoginID" ;
        public static final String    tablestaffCreatedOn= "CreatedOn" ;
        public static final String    tablestaffModifiedOn= "ModifiedOn";*/

        String selectQuery = "SELECT  * FROM " + Tablestaff+" where PortalLoginID ='"+userlogin+"'";

        Staffloingdetails details =new Staffloingdetails();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                details.setStaffID(cursor.getInt(0));
                details.setEmployementNumber(cursor.getString(1));
                details.setDOJ(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setMiddleName(cursor.getString(4));
                details.setLastName(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setDOB(cursor.getString(7));
                details.setMaritalStatusID(cursor.getString(8));
                details.setSpouseName(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setPhone(cursor.getString(12));
                details.setEmail(cursor.getString(13));
                details.setPhotoFilename(cursor.getString(14));
                details.setStaffCategoryID(cursor.getInt(15));
                details.setSchoolID(cursor.getString(16));
                details.setDeptartmentID(cursor.getInt(17));
                details.setDesignationID(cursor.getInt(18));
                details.setPortalUserID(cursor.getString(19));
                details.setPortalLoginID(cursor.getString(20));
                details.setCreatedOn(cursor.getString(21));
                details.setModifiedOn(cursor.getString(22));
                details.setRoominfo(cursor.getString(23));
                details.setTimetable(cursor.getString(cursor.getColumnIndex(timetable)));

                //Log.e("name",cursor.getString(2));
              /*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }

        return  details;
    }



    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestionsUsingExamIdquestionidf(int ExamId,int questionid) {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query

        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION +" where " +EXAM_ID+" = '"+ ExamId+"'"+ "and " +QUESTION_ID+" = '"+ questionid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setModifiedOn(cursor.getString(19));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }


    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestionsUsingExamId(int ExamId) {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION +" where " +EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setModifiedOn(cursor.getString(19));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }


    // Getting All examswritten
    public List<ExamDetails> getAllExamUsingExamId(int ExamId,String StaffId) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS +" where " +EXAM_ID+" = '"+ ExamId+"'"+" and "+STAFF_ID+"= '"+StaffId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setStaffId(cursor.getString(1));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(2)));
                examdetails.setExamCategoryName(cursor.getString(3));
                examdetails.setExamCode(cursor.getString(4));
                examdetails.setExamDescription(cursor.getString(5));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(6)));
                examdetails.setExamDate(cursor.getString(7));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(8)));
                examdetails.setExamType(cursor.getString(9));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(10)));
                examdetails.setSubject(cursor.getString(11));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(12)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(13)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(14)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(15)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(16)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(17)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(18)));
                examdetails.setDateAttended(cursor.getString(19));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(20)));
                // Adding examswritten to list
                examdetailsList.add(examdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetailsList;
    }


    public void deleteToDo(long tado_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXAM_DETAILS, EXAM_ID + " = ?",
                new String[] { String.valueOf(tado_id) });
    }
    // Getting All examswritten
    public List<ExamDetails> getAllExamsDetails(String staffID) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS+" where " +STAFF_ID+" = '"+ staffID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setStaffId(cursor.getString(1));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(2)));
                examdetails.setExamCategoryName(cursor.getString(3));
                examdetails.setExamCode(cursor.getString(4));
                examdetails.setExamDescription(cursor.getString(5));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(6)));
                examdetails.setExamDate(cursor.getString(7));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(8)));
                examdetails.setExamType(cursor.getString(9));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(10)));
                examdetails.setSubject(cursor.getString(11));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(12)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(13)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(14)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(15)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(16)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(17)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(18)));
                examdetails.setDateAttended(cursor.getString(19));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(20)));
                // Adding examswritten to list
                examdetailsList.add(examdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examdetailsList;
    }

    public boolean CheckIsDataAlreadyInDBorNot(int studentID
            , int fieldValue) {
        SQLiteDatabase db = this.getWritableDatabase();
        String Query = "Select * from " + TABLE_STUDENT_EXAM_RESPONSE + " where " + RESPONSE_EXAM_ID + " = '" + fieldValue+ "' and " +RESPONSE_STUDENT_ID+" = '"+ studentID+"'";
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    // Updating single examswritten
    public int updateExamDetails(ExamResponsePojo examrespojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RESPONSE_EXAM_ID, examrespojo.getExamID());
        values.put(RESPONSE_STUDENT_ID, examrespojo.getStudentID());
        values.put(RESPONSE_ROLL_NO, examrespojo.getRollNo());
        values.put(RESPONSE_STUDENT_NAME, examrespojo.getStudName());
        values.put(RESPONSE_TIME_TAKEN, examrespojo.getTimeTaken());
        values.put(RESPONSE_DATE_ATTENDED, examrespojo.getDateAttended());
        values.put(RESPONSE_TOTAL_SCORE, examrespojo.getTotalScore());


        // updating row
        return  db.update(TABLE_STUDENT_EXAM_RESPONSE,
                values,
                RESPONSE_EXAM_ID + " = ? AND " + RESPONSE_STUDENT_ID + " = ?",
                new String[]{examrespojo.getExamID()+"", examrespojo.getStudentID()+""});
    }
    // Adding new StudentExamResponse
    public void addStudentExamResponse(ExamResponsePojo examrespojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RESPONSE_EXAM_ID, examrespojo.getExamID());
        values.put(RESPONSE_STUDENT_ID, examrespojo.getStudentID());
        values.put(RESPONSE_ROLL_NO, examrespojo.getRollNo());
        values.put(RESPONSE_STUDENT_NAME, examrespojo.getStudName());
        values.put(RESPONSE_TIME_TAKEN, examrespojo.getTimeTaken());
        values.put(RESPONSE_DATE_ATTENDED, examrespojo.getDateAttended());
        values.put(RESPONSE_TOTAL_SCORE, examrespojo.getTotalScore());
        // Inserting Row
        db.insert(TABLE_STUDENT_EXAM_RESPONSE, null, values);
        db.close(); // Closing database connection
    }
    // Adding new tblStudentQuestionResult
    public void addtblStudentQuestionResult(StudentQuestionResultPojo studentques) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_RESPONSE_ID, studentques.getExamResponseID());
        values.put(RESPONSE_QUESTION_ID, studentques.getQuestionID());
        values.put(RESPONSE_STUDENT_ANSWER, studentques.getStudentAnswer());
        values.put(RESPONSE_IS_CORRECT, studentques.getIsCorrect());
        values.put(MARK_FOR_ANSWER, studentques.getMarkForAnswer());
        values.put(RESPONSE_OBTAINED_SCORE, studentques.getObtainedScore());
        // Inserting Row
        db.insert(TABLE_STUDENT_QUESTION_RESULT, null, values);
        db.close(); // Closing database connection
    }
    // Getting All examswritten

    public List<ExamResponsePojo> addStudentExamResponseUsingExamId(int ExamId,int StudentId ) {
        List<ExamResponsePojo> examrespojo = new ArrayList<ExamResponsePojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_EXAM_RESPONSE +" where " +RESPONSE_STUDENT_ID+" = '"+ StudentId +"'"+ "and " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamResponsePojo ansdetails = new ExamResponsePojo();
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setStudentID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setRollNo(Integer.parseInt(cursor.getString(3)));
                ansdetails.setStudName(cursor.getString(4));
                ansdetails.setTimeTaken(Integer.parseInt(cursor.getString(5)));
                ansdetails.setDateAttended(cursor.getString(6));
                ansdetails.setTotalScore(Integer.parseInt(cursor.getString(7)));
                // Adding examswritten to list
                examrespojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examrespojo;
    }

    // Getting All examswritten

    public List<ExamResponsePojo> addStudentExamResponseUsingExamId(int ExamId) {
        List<ExamResponsePojo> examrespojo = new ArrayList<ExamResponsePojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_EXAM_RESPONSE +" where " +RESPONSE_EXAM_ID+" = '"+ ExamId+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ExamResponsePojo ansdetails = new ExamResponsePojo();
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setStudentID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setRollNo(Integer.parseInt(cursor.getString(3)));
                ansdetails.setStudName(cursor.getString(4));
                ansdetails.setTimeTaken(Integer.parseInt(cursor.getString(5)));
                ansdetails.setDateAttended(cursor.getString(6));
                ansdetails.setTotalScore(Integer.parseInt(cursor.getString(7)));
                // Adding examswritten to list
                examrespojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return examrespojo;
    }
    // Adding new examswritten
    public void addExamDetails(ExamDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_ID, examlist.getExamID());
        values.put(STAFF_ID, examlist.getStaffId());
        values.put(EXAM__CATEGORY_ID, examlist.getExamCategoryID());
        values.put(EXAM_CATEGORY_NAME, examlist.getExamCategoryName());
        values.put(EXAM_CODE, examlist.getExamCode());
        values.put(EXAM_DESCRIPTION, examlist.getExamDescription());
        values.put(EXAM_SEQUENCE, examlist.getExamSequence());
        values.put(EXAM_DATE, examlist.getExamDate());
        values.put(EXAM_TYPE_ID, examlist.getExamTypeID());
        values.put(EXAM_TYPE, examlist.getExamType());
        values.put(SUBJECT_ID, examlist.getSubjectID());
        values.put(SUBJECT, examlist.getSubject());
        values.put(EXAM_DURATION, examlist.getExamDuration());
        values.put(SCHOOL_ID, examlist.getSchoolID());
        values.put(CLASS_ID, examlist.getClassID());
        values.put(BATCH_ID, examlist.getBatchID());
        values.put(IS_RESULT_PUBLISHED, examlist.getIsResultPublished());
        values.put(EXAM_SHELF_ID, examlist.getExamShelfID());
        values.put(TIME_TAKEN, examlist.getTimeTaken());
        values.put(DATE_ATTENDED, examlist.getDateAttended());
        values.put(TOTAL_SCORE, examlist.getTotalScore());



        // Inserting Row
        db.insert(TABLE_EXAM_DETAILS, null, values);
        db.close(); // Closing database connection
    }


    // Adding new tblExamQuestions
    public void addExamQuestions(QuestionDetails examlist) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(QUESTION_ID, examlist.getQuestionID()); // examswritten Name
        values.put(QUESTION_EXAM_ID, examlist.getExamID()); // examswritten Phone
        values.put(TOPIC_ID, examlist.getTopicID()); // examswritten Phone
        values.put(TOPIC_NAME, examlist.getTopicName()); // examswritten Phone
        values.put(ASPECT_ID, examlist.getAspectID()); // examswritten Phone
        values.put(ASPECT, examlist.getAspect()); // examswritten Phone
        values.put(QUESTION, examlist.getQuestion()); // examswritten Phone
        values.put(QUESTION_NUMBER, examlist.getQuestionNumber()); // examswritten Phone
        values.put(OPTION_A, examlist.getOptionA()); // examswritten Phone
        values.put(OPTION_B, examlist.getOptionB()); // examswritten Phone
        values.put(OPTION_C, examlist.getOptionC()); // examswritten Phone
        values.put(OPTION_D, examlist.getOptionD()); // examswritten Phone
        values.put(QUESTION_CORRECT_ANSWER, examlist.getCorrectAnswer()); // examswritten Phone
        values.put(MARK, examlist.getMark()); // examswritten Phone
        values.put(NEGATIVE_MARK, examlist.getNegative_Mark()); // examswritten Phone
        values.put(STUDENT_ANSWER, examlist.getStudentAnswer()); // examswritten Phone
        values.put(IS_CORRECT, examlist.getIsCorrect()); // examswritten Phone
        values.put(OBTAINED_SCORE, examlist.getObtainedScore()); // examswritten Phone
        values.put(CREATED_ON, examlist.getCreatedOn()); // examswritten Phone
        values.put(MODIFIED_ON, examlist.getModifiedOn()); // examswritten Phone



        // Inserting Row
        db.insert(TABLE_EXAM_QUESTION, null, values);
        db.close(); // Closing database connection
    }


    // Getting All examswritten
    public List<ExamResponsePojo> getAllExamResponse() {

        List<ExamResponsePojo> ExamResponsePojo = new ArrayList<ExamResponsePojo>();
        // Select All Query
      //  String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION;

       // String selectQuery = "SELECT * FROM " +TABLE_STUDENT_EXAM_RESPONSE+ " WHERE "   +TABLE_EXAM_RESPONSE_ID+ " = (SELECT MAX("+TABLE_EXAM_RESPONSE_ID+") FROM " +TABLE_STUDENT_EXAM_RESPONSE+")";
        String selectQuery = "SELECT * FROM " +TABLE_STUDENT_EXAM_RESPONSE+" ORDER BY " +TABLE_EXAM_RESPONSE_ID+ " DESC LIMIT 1";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToLast()) {
            do {
                ExamResponsePojo ExamResponsePojode = new ExamResponsePojo();
                ExamResponsePojode.setExamResponseID(Integer.parseInt(cursor.getString(0)));
                ExamResponsePojode.setExamID(Integer.parseInt(cursor.getString(1)));
                ExamResponsePojode.setStudentID(Integer.parseInt(cursor.getString(2)));
                ExamResponsePojode.setRollNo(Integer.parseInt(cursor.getString(3)));
                ExamResponsePojode.setStudName(cursor.getString(4));
                // Adding examswritten to list
                ExamResponsePojo.add(ExamResponsePojode);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return ExamResponsePojo;
    }

    // Getting All examswritten
    public List<QuestionDetails> getAllExamQuestions() {
        List<QuestionDetails> questiondetailsList = new ArrayList<QuestionDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_QUESTION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                QuestionDetails questionsdetails = new QuestionDetails();
                questionsdetails.setQuestionID(Integer.parseInt(cursor.getString(0)));
                questionsdetails.setExamID(Integer.parseInt(cursor.getString(1)));
                questionsdetails.setTopicID(Integer.parseInt(cursor.getString(2)));
                questionsdetails.setTopicName(cursor.getString(3));
                questionsdetails.setAspectID(Integer.parseInt(cursor.getString(4)));
                questionsdetails.setAspect(cursor.getString(5));
                questionsdetails.setQuestion(cursor.getString(6));
                questionsdetails.setQuestionNumber(Integer.parseInt(cursor.getString(7)));
                questionsdetails.setOptionA(cursor.getString(8));
                questionsdetails.setOptionB(cursor.getString(9));
                questionsdetails.setOptionC(cursor.getString(10));
                questionsdetails.setOptionD(cursor.getString(11));
                questionsdetails.setCorrectAnswer(cursor.getString(12));
                questionsdetails.setMark(Integer.parseInt(cursor.getString(13)));
                questionsdetails.setNegative_Mark(Integer.parseInt(cursor.getString(14)));
                questionsdetails.setStudentAnswer(cursor.getString(15));
                questionsdetails.setIsCorrect(Integer.parseInt(cursor.getString(16)));
                questionsdetails.setObtainedScore(Integer.parseInt(cursor.getString(17)));
                questionsdetails.setCreatedOn(cursor.getString(18));
                questionsdetails.setModifiedOn(cursor.getString(19));
                // Adding examswritten to list
                questiondetailsList.add(questionsdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return questiondetailsList;
    }


    // Adding new StudentExamResponse
    public void addStudentExamResponseUsingExamId(ExamResponsePojo examrespojo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(EXAM_RESPONSE_ID, examrespojo.getExamResponseID());
        values.put(TABLE_EXAM_RESPONSE_ID, examrespojo.getExamID());
        values.put(RESPONSE_STUDENT_ID, examrespojo.getStudentID());
        values.put(RESPONSE_ROLL_NO, examrespojo.getRollNo());
        values.put(RESPONSE_STUDENT_NAME, examrespojo.getStudName());
        values.put(RESPONSE_TIME_TAKEN, examrespojo.getTimeTaken());
        values.put(RESPONSE_DATE_ATTENDED, examrespojo.getDateAttended());
        values.put(RESPONSE_TOTAL_SCORE, examrespojo.getTotalScore());
        // Inserting Row
        db.insert(TABLE_STUDENT_EXAM_RESPONSE, null, values);
        db.close(); // Closing database connection
    }


    public List<StudentQuestionResultPojo> tblStudentQuestionResultusingresponseIDall() {
        List<StudentQuestionResultPojo> resultpojo = new ArrayList<StudentQuestionResultPojo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_QUESTION_RESULT;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentQuestionResultPojo ansdetails = new StudentQuestionResultPojo();
                ansdetails.setResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setQuestionID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setStudentAnswer(cursor.getString(3));
                ansdetails.setIsCorrect(Integer.parseInt(cursor.getString(4)));
                ansdetails.setMarkForAnswer(Integer.parseInt(cursor.getString(5)));
                ansdetails.setObtainedScore(Integer.parseInt(cursor.getString(6)));

                // Adding examswritten to list
                resultpojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return resultpojo;
    }




    public List<StudentQuestionResultPojo> tblStudentQuestionResultusingrquerstionid(int ExamresponseId,int  questionid) {
        List<StudentQuestionResultPojo> resultpojo = new ArrayList<StudentQuestionResultPojo>();
        // Select All Query


        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_QUESTION_RESULT +" where " +EXAM_RESPONSE_ID+" = '"+ ExamresponseId+"' and "+RESPONSE_QUESTION_ID+"= '"+questionid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentQuestionResultPojo ansdetails = new StudentQuestionResultPojo();
                ansdetails.setResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setQuestionID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setStudentAnswer(cursor.getString(3));
                ansdetails.setIsCorrect(Integer.parseInt(cursor.getString(4)));
                ansdetails.setMarkForAnswer(Integer.parseInt(cursor.getString(5)));
                ansdetails.setObtainedScore(Integer.parseInt(cursor.getString(6)));

                // Adding examswritten to list
                resultpojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return resultpojo;
    }


    // Getting All examswritten

    public List<StudentQuestionResultPojo> tblStudentQuestionResultusingresponseID(int ExamresponseId) {
        List<StudentQuestionResultPojo> resultpojo = new ArrayList<StudentQuestionResultPojo>();
        // Select All Query

       // String selectQuery = "SELECT * FROM " +TABLE_PULSE_QUESTION+" where " +PulseQuestionID+" = '"+ pulId+"'"+" ORDER BY " +PulseQuestionID+ " DESC LIMIT 1";

        String selectQuery = "SELECT  * FROM " + TABLE_STUDENT_QUESTION_RESULT +" where " +EXAM_RESPONSE_ID+" = '"+ ExamresponseId+"' order by "+ RESPONSE_QUESTION_ID;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentQuestionResultPojo ansdetails = new StudentQuestionResultPojo();
                ansdetails.setResponseID(Integer.parseInt(cursor.getString(0)));
                ansdetails.setExamResponseID(Integer.parseInt(cursor.getString(1)));
                ansdetails.setQuestionID(Integer.parseInt(cursor.getString(2)));
                ansdetails.setStudentAnswer(cursor.getString(3));
                ansdetails.setIsCorrect(Integer.parseInt(cursor.getString(4)));
                ansdetails.setMarkForAnswer(Integer.parseInt(cursor.getString(5)));
                ansdetails.setObtainedScore(Integer.parseInt(cursor.getString(6)));

                // Adding examswritten to list
                resultpojo.add(ansdetails);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return resultpojo;
    }




    public helper.Studentdetails getdetails(int rollnumber,int subjectid,int batchid,int staffid)
    {

        helper.Studentdetails  details=new helper.Studentdetails();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM   tblStudent where StaffID= '"+staffid+
                "' and SchoolID= '"+subjectid+"' and RollNo= '"+rollnumber+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //  helper.Studentdetails details =new helper.Studentdetails();
                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                //get.add(details);
           /* Log.e("name",cursor.getString(1));
              *//*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  details;

    }


    // Getting All examswritten
    public String getAllExamsDetailsusingExamId(int ExamID,String staffID) {
        List<ExamDetails> examdetailsList = new ArrayList<ExamDetails>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXAM_DETAILS + " where " +EXAM_ID+" = '"+ ExamID+"'"+" and "+STAFF_ID+"= '"+staffID+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);


        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
              /*  ExamDetails examdetails = new ExamDetails();
              /*  ExamDetails examdetails = new ExamDetails();
                examdetails.setExamID(Integer.parseInt(cursor.getString(0)));
                examdetails.setExamCategoryID(Integer.parseInt(cursor.getString(1)));
                examdetails.setExamCategoryName(cursor.getString(2));
                examdetails.setExamCode(cursor.getString(3));
                examdetails.setExamDescription(cursor.getString(4));
                examdetails.setExamSequence(Integer.parseInt(cursor.getString(5)));
                examdetails.setExamDate(cursor.getString(6));
                examdetails.setExamTypeID(Integer.parseInt(cursor.getString(7)));
                examdetails.setExamType(cursor.getString(8));
                examdetails.setSubjectID(Integer.parseInt(cursor.getString(9)));
                examdetails.setSubject(cursor.getString(10));
                examdetails.setExamDuration(Integer.parseInt(cursor.getString(11)));
                examdetails.setSchoolID(Integer.parseInt(cursor.getString(12)));
                examdetails.setClassID(Integer.parseInt(cursor.getString(13)));
                examdetails.setBatchID(Integer.parseInt(cursor.getString(14)));
                examdetails.setIsResultPublished(Integer.parseInt(cursor.getString(15)));
                examdetails.setExamShelfID(Integer.parseInt(cursor.getString(16)));
                examdetails.setTimeTaken(Integer.parseInt(cursor.getString(17)));
                examdetails.setDateAttended(cursor.getString(18));
                examdetails.setTotalScore(Integer.parseInt(cursor.getString(19)));*/
                // Adding examswritten to list
                //  examdetailsList.add(examdetails);
                return cursor.getString(5);
            } while (cursor.moveToNext());
        }

        // return examswritten list
        return "";
    }

//get studentlist


    public ArrayList< helper.Studentdetails> getdetails(int classid,int batchid,int staffid)
    {

        ArrayList< helper.Studentdetails>get=new ArrayList<>();
    /*"\"StudentID\" integer primary key not null ," +
            "\"AdmissionNumber\" varchar ," +
            "\"DOA\" datetime ," +
            "\"FirstName\" varchar ," +
            "\"LastName\" varchar ," +
            "\"DOB\" datetime ," +
            "\"Gender\" varchar ," +
            "\"Phone_1\" varchar ," +
            "\"Phone_2\" varchar ," +
            "\"Email\" varchar ," +
            "\"FatherName\" varchar ," +
            "\"MotherName\" varchar ," +
            "\"GuardianMobileNumber\" varchar ," +
            "\"RollNo\" varchar ," +
            "\"ClassID\" integer ," +
            "\"BatchID\" integer ," +
            "\"SchoolID\" integer ," +
            "\"AcademicYear\" varchar ," +
            "\"Guardians\" varchar ," +
            "\"PhotoFilename\" varchar ," +
            "\"PortalUserID\" integer ," +
            "\"StudentLoginID\" varchar ," +
            "\"Version\" integer ," +
            "\"StaffID\" integer ," +
            "\"CreditPoints\" integer ," +
            "\"ConnectionStatus\" integer ," +
            "\"ManualAttendance\" integer ," +
            "\"NoOfPresent\" integer ," +
            "\"Credits\" integer," +
            "\"CreatedOn\" datetime ," +
            "\"ModifiedOn\" datetime " +*/

        String selectQuery = "SELECT  * FROM   tblStudent where StaffID= '"+staffid+
                "' and BatchID= '"+batchid+"' and ClassID= '"+classid+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //  helper.Studentdetails details =new helper.Studentdetails();
                helper.Studentdetails  details=new helper.Studentdetails();

                details.setStudentID(cursor.getInt(0));
                details.setAdmissionNumber(cursor.getString(1));
                details.setDOA(cursor.getString(2));
                details.setFirstName(cursor.getString(3));
                details.setLastName(cursor.getString(4));
                details.setDOB(cursor.getString(5));
                details.setGender(cursor.getString(6));
                details.setPhone_1(cursor.getString(7));
                details.setPhone_2(cursor.getString(8));
                details.setEmail(cursor.getString(9));
                details.setFatherName(cursor.getString(10));
                details.setMotherName(cursor.getString(11));
                details.setGuardianMobileNumber(cursor.getString(12));
                details.setRollNo(cursor.getString(13));
                details.setClassID(cursor.getInt(14));
                details.setBatchID(cursor.getInt(15));
                details.setSchoolID(cursor.getInt(16));
                details.setAcademicYear(cursor.getString(17));
                details.setGuardians(cursor.getString(18));
                details.setPhotoFilename(cursor.getString(19));
                details.setPortalUserID(cursor.getInt(20));
                details.setStudentLoginID(cursor.getString(21));
                details.setVersion(cursor.getInt(22));
                details.setStaffID(cursor.getInt(23));
                details.setCreditPoints(cursor.getInt(24));
                details.setConnectionStatus(cursor.getInt(25));
                details.setManualAttendance(cursor.getInt(26));
                details.setNoOfPresent(cursor.getInt(27));
                details.setCredits(cursor.getInt(28));
                details.setCreatedOn(cursor.getString(29));
                details.setModifiedOn(cursor.getString(30));
                get.add(details);
           /* Log.e("name",cursor.getString(1));
              *//*  Contact contact = new Contact();
                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setName(cursor.getString(1));
                contact.setPhoneNumber(cursor.getString(2));
                // Adding contact to list
                contactList.add(contact);*/
            } while (cursor.moveToNext());
        }
        db.close();
        return  get;

    }
}

