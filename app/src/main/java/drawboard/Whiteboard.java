package drawboard;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.gesture.Gesture;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.ColorAdapter;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.MainActivity;
import com.dci.edukool.teacher.R;

import books.Attendancewithcheckparentlayout;
import books.PulseQuestionActivity;
import connection.Filesharingtoclient;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import Utilities.Utilss;
import helper.OnItemClickListener;

/**
 * This is the activity class that serves as the main entry point to the
 * app's user interface. This class contains the onCreate() method along
 * with some other methods. The onCreate() method is called by the system
 * when the application is launched from the device home screen.
 * This class is defined to be used as the main activity in the Android
 * manifest file, AndroidManifest.xml, which is at the root of
 * the project directory. In the manifest, this class is declared with an
 * <intent-filter> that includes the MAIN action and LAUNCHER category.
 * If either the MAIN action or LAUNCHER category are not declared for one
 * of the activities, then the application icon will not appear in the Home
 * screen's list of app's.
 *
 * @author PrateekMehrotra
 */
@SuppressLint("NewApi")
public class Whiteboard extends Activity implements OnClickListener, OnItemClickListener, ColorPickerDialog.OnColorChangedListener, OnGesturePerformedListener {

    private static final String COLOR_PREFERENCE_KEY = "color";
    private static final Integer[] COLORS = {Color.BLACK, Color.DKGRAY,
            Color.BLUE, Color.GREEN, Color.CYAN, Color.RED, Color.MAGENTA,
            0xFFFF6800, Color.YELLOW, Color.LTGRAY, Color.GRAY,};
    private static final int COLOR_PICKER_DIALOG_ID = 1;
    public static boolean synboard;
    //GestureOverlayView gestures;
    private static int RESULT_LOAD_IMAGE = 1;
    ImageView imageView;
    GestureLibrary mLibrary;
    ServerSocket serverSocket;
    ArrayList<String> movex;
    ArrayList<String> movey;
    ArrayList<String> linex;
    ArrayList<String> liney;
    ImageView home;
    ImageView sync;
    ImageView lock;
    ImageView handrise;
    ImageView cursor;
    ImageView projection;
    BroadcastReceiver forhandrais;
    boolean whiteboardsharing;
    ImageView syncwhiteboard;
    String filename;
    boolean colorpickch = false;
    boolean lockunlock = false;
    ImageView pulse;
    ArrayList<String> pathList;
    //boolean gestureDetected = false;
    int i = 0;
    SharedPreferences pref;
    String notepath;
    LinearLayout save_lay;
    ImageView sharewhite;
    RecyclerView color_recycle;
    List<Integer> colorlist = new ArrayList<>();
    Importadapter adapter;
    PopupWindow pwindo;
    ColorAdapter coloradapter;
    TextView entertitle;
    private CustomViewForDrawing drawView;
    private ImageButton currPaint;
    private ImageView drawBtn;
    private float smallBrush, mediumBrush, largeBrush;
    private ImageView eraseBtn, newBtn, saveBtn, colorPickerBtn, colorFillBtn, gestureBtn;
    private ImageView btn_undo, btn_redo, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        System.out.println("oncreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.whiteboard);
        synboard = false;
        drawView = (CustomViewForDrawing) findViewById(R.id.drawing);
        syncwhiteboard = (ImageView) findViewById(R.id.sync);
        back = (ImageView) findViewById(R.id.back);
        color_recycle = (RecyclerView) findViewById(R.id.color_recycle);
        coloradapter = new ColorAdapter(getlist(), Whiteboard.this);
        coloradapter.setOnItemClickListener(Whiteboard.this);
        entertitle = (TextView) findViewById(R.id.entertitle);
        save_lay = (LinearLayout) findViewById(R.id.save_lay);
        save_lay.setOnClickListener(this);
        Log.d("tag", "onCreate: " + getlist().size());
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        color_recycle.setLayoutManager(layoutManager);
        color_recycle.setAdapter(coloradapter);
        home = (ImageView) findViewById(R.id.home);
        //sync= (ImageView) findViewById(R.id.sync);
        lock = (ImageView) findViewById(R.id.lock);
        handrise = (ImageView) findViewById(R.id.gesture);
        cursor = (ImageView) findViewById(R.id.cursor);
        projection = (ImageView) findViewById(R.id.projection);

        sharewhite = (ImageView) findViewById(R.id.sharewhite);
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!lockunlock && !synboard) {

                    /*if(CustomViewForDrawing.paths.size()==0) {*/
                    CustomViewForDrawing.paths.clear();
                    finish();
					/*}
					else {
						confirmSave();
					}*/
                } else if (lockunlock) {
                    Utilss.lockpoopup(Whiteboard.this, "WhiteBoard", getResources().getString(R.string.lock));
                } else if (synboard) {
                  Utilss.lockpoopup(Whiteboard.this, "WhiteBoard", getResources().getString(R.string.whiteboardsync));

                }

            }
        });


        sharewhite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                whiteboardsharing = true;
                File f = new File(pref.getString("notes", ""));
                getfile(f);
                if (pathList.size() > 0) {
                    makeToast("Refreshing...");


                    ImportWindow(Whiteboard.this);
                } else {
                    Toast.makeText(Whiteboard.this, "Sorry, you do not have any saved notes.", Toast.LENGTH_SHORT).show();

                }

            }
        });

        //LinearLayout paintLayout = (LinearLayout) findViewById(R.id.paint_colors);

		/*try{currPaint = (ImageButton)paintLayout.getChildAt(0);
			currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));}
		catch(NullPointerException e){}*/

        //Brush initialization
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        notepath = pref.getString("notes", "");


        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);
        drawView.setContext(this);
        drawBtn = (ImageView) findViewById(R.id.draw_btn);
        drawBtn.setOnClickListener(this);

        drawView.setSizeForBrush(mediumBrush);

        eraseBtn = (ImageView) findViewById(R.id.erase_btn);
        eraseBtn.setOnClickListener(this);

        saveBtn = (ImageView) findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(this);

        newBtn = (ImageView) findViewById(R.id.new_btn);
        newBtn.setOnClickListener(this);

        btn_undo = (ImageView) findViewById(R.id.undo);

        pulse = (ImageView) findViewById(R.id.pulse);
        pulse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Whiteboard.this, PulseQuestionActivity.class);
                startActivity(intent);
            }
        });
        btn_undo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (CustomViewForDrawing.paths.size() != 0) {
                    drawBtn.setImageResource(R.drawable.paint_icon);//for change
                    eraseBtn.setImageResource(R.drawable.erase_icon);
                    btn_undo.setImageResource(R.drawable.sel_back_icon);

                    gestureBtn.setImageResource(R.drawable.folder_icon);
                  //  syncwhiteboard.setImageResource(R.drawable.sync_icon);
                   // colorPickerBtn.setImageResource(R.drawable.colorpicker);

//                    drawBtn.setBackgroundResource(R.color.white);//for change
//                    eraseBtn.setBackgroundResource(R.color.white);
//                    btn_undo.setBackgroundResource(R.color.grey);
//                    saveBtn.setBackgroundResource(R.color.white);
//                    gestureBtn.setBackgroundResource(R.color.white);
//                    syncwhiteboard.setImageResource(R.color.white);
//                    colorPickerBtn.setBackgroundResource(R.color.white);


                    Log.i("UndoTag", "Inside on click of undo");
                    if (synboard) {
                        if (CustomViewForDrawing.paths.size() != 0)
                            drawView.undo();
                    }


                    drawView.onClickUndo();
                } else {
                    Toast.makeText(Whiteboard.this, "No activity for undo further.", Toast.LENGTH_SHORT).show();
                }


            }
        });


        home.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!lockunlock && !synboard) {

                    /*if(CustomViewForDrawing.paths.size()==0) {*/
                    CustomViewForDrawing.paths.clear();
                    finish();
					/*}
					else {
						confirmSave();
					}*/
                } else if (lockunlock) {
                    Utilss.lockpoopup(Whiteboard.this, "WhiteBoard", getResources().getString(R.string.lock));
                } else if (synboard) {
                   Utilss.lockpoopup(Whiteboard.this, "WhiteBoard", getResources().getString(R.string.whiteboardsync));

                }

            }
        });
		/*sync.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toast();
			}
		});*/

        lock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //toast();

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!lockunlock) {
					/*String senddata = "lock";
					new clientThread(MultiThreadChatServerSync.thread, senddata).start();
					lockunlock=true;*/
					/*lockunlock=true;
					Intent in=new Intent("lock");

					in.putExtra("unlockorlock",true);
					sendBroadcast(in);*/
                        String senddata = "lock";
                        Toast.makeText(Whiteboard.this, "locked", Toast.LENGTH_SHORT).show();
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                        //   lock.setImageResource(R.drawable.lockpressed);
                        lockunlock = true;
                    } else {
                        String senddata = "unlock";
                        Toast.makeText(Whiteboard.this, "unlocked", Toast.LENGTH_SHORT).show();
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
					/*String senddata = "unlock";
					new clientThread(MultiThreadChatServerSync.thread, senddata).start();
					lockunlock=false;*/
					/*lockunlock=false;
					Intent in=new Intent("lock");
					in.putExtra("unlockorlock",false);
					sendBroadcast(in);*/
                        lockunlock = false;
                        //  lock.setImageResource(R.drawable.bookbinlock);


                    }
                } else {
                    Utilss.lockpoopup(Whiteboard.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(BookBinActivity.this, "Break", "You are in Session Break");

                }

            }
        });
        handrise.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    handrise.setImageResource(R.drawable.sel_raise_icon);
                   Utilss.Listpopup(Whiteboard.this);
                } else {
                    Toast.makeText(Whiteboard.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                    handrise.setImageResource(R.drawable.raise_icon);

                    //Toast.makeText(MuPDFActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                }
                //toast();
            }
        });
        cursor.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toast();
            }
        });
        projection.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toast();
            }
        });

        syncwhiteboard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //toast();
                //for image change

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

//                    drawBtn.setImageResource(R.drawable.paint_br);//for change
//                    eraseBtn.setImageResource(R.drawable.eraser);
//                    btn_undo.setImageResource(R.drawable.undo);
//                    saveBtn.setImageResource(R.drawable.save);
//                    gestureBtn.setImageResource(R.drawable.importimage);
//                    colorPickerBtn.setImageResource(R.drawable.colorpicker);


                    if (!synboard) {


                        if (CustomViewForDrawing.paths.size() == 0) {
                            synboard = true;
                            newDrawing1();
                            String senddata = "whiteboard@whiteboard:sync";
                            new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                            syncwhiteboard.setImageResource(R.drawable.sel_sync_icon);
                        } else {
                            confirmSave1();
                        }

                    } else {
                        //String senddata = "whiteboard@whiteboard:unsync";

                        drawView.close();
                        //	new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                        synboard = false;
                        syncwhiteboard.setImageResource(R.drawable.sync_icon);

                    }

                } else {
                   Utilss.lockpoopup(Whiteboard.this, "EduKool", "You can access this feature once you come out of the session break.");

                }
            }


        });

		/*btn_redo=(ImageView) findViewById(R.id.redo);
		btn_redo.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Log.i("RedoTag","Inside on click of redo");
				drawView.onClickRedo();

			}
		});*/


	/*	ActionBar actionBar = getActionBar();
		actionBar.show();
*/
        colorPickerBtn = (ImageView) findViewById(R.id.color_picker);
        colorPickerBtn.setOnClickListener(this);

        //	mLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures); if(!mLibrary.load()){ finish(); }
        //	gestures = (GestureOverlayView) findViewById(R.id.gestures);
        //	gestures.addOnGesturePerformedListener(this);

		/*colorFillBtn = (ImageView) findViewById(R.id.color_fill);
		colorFillBtn.setOnClickListener(this);*/
        //Gesture Overlay View Change 03/10/2013
        gestureBtn = (ImageView) findViewById(R.id.imp);
        gestureBtn.setOnClickListener(this);


        movex = new ArrayList<String>();
        movey = new ArrayList<String>();
        linex = new ArrayList<String>();
        liney = new ArrayList<String>();

        drawView.setList(movex, movey, linex, liney);
        Utilss utils = new Utilss(this);
        utils.setTextviewtypeface(5, entertitle);
		/*Thread socketServerThread = new Thread(new SocketServerThread());
		socketServerThread.start();*/
    }

    void toast() {
        Toast.makeText(getApplicationContext(), "Under Construction", Toast.LENGTH_LONG).show();
    }

    public List<Integer> getlist() {
        List<Integer> list = new ArrayList<>();
        list = Arrays.asList(COLORS);

        return list;
    }

    /**
     * This method is called when a gesture is input from the gesture
     * view. This method defines the logic for gesture matching using
     * prediction scores returned by the Gesture Builder API.
     */
    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mLibrary.recognize(gesture);
        btn_undo.setVisibility(View.GONE);
        btn_redo.setVisibility(View.GONE);
        if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
            String result = predictions.get(0).name;

            if ("new".equalsIgnoreCase(result)) {   //Gesture Overlay View Change 03/10/2013
                //gestures.setVisibility(View.GONE);
                //drawView.setVisibility(View.VISIBLE);
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);
                makeToast("Gesture Detected and Views Switched");
                //drawView.newStart();
                newDrawing();
                Toast.makeText(this, "new document", Toast.LENGTH_SHORT).show();
                //gestureDetected = true;
            } else if ("smooth".equalsIgnoreCase(result)) {
                //gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                drawView.setVisibility(View.VISIBLE);
                drawView.smoothStrokes = true;
                makeToast("Smoothening enabled!");
                //gestureDetected = true;
            } else if ("curve".equalsIgnoreCase(result)) {
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);
                drawView.smoothStrokes = false;
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);
                makeToast("Curve enabled, smoothening disabled!");

                //gestureDetected = true;
            } else if ("save".equalsIgnoreCase(result)) {    //Gesture Overlay View Change 03/10/2013
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                //drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
                drawView.setVisibility(View.VISIBLE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                makeToast("Gesture Detected and Views Switched");
                saveDrawing();
                Toast.makeText(this, "Saving the document", Toast.LENGTH_SHORT).show();
                //gestureDetected = true;
            }
            //Gesture Overlay View Change 03/10/2013
            else if ("erase".equalsIgnoreCase(result)) {
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                makeToast("Gesture Detected and Views Switched");
                eraseDraw();
                makeToast("Select Eraser Size!");
                //gestureDetected = true;
            } else if ("open".equalsIgnoreCase(result)) {
                makeToast("Gesture Detected and Views Switched");
                //onActivityResult(1, resultCode, data);
                makeToast("Refreshing...");
                Intent i = new Intent(
                        Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                startActivityForResult(i, RESULT_LOAD_IMAGE);

            } else if ("undo".equalsIgnoreCase(result)) {
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                makeToast("Gesture Detected and Views Switched");
                drawView.onClickUndo();
            } else if ("redo".equalsIgnoreCase(result)) {
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);
                btn_undo.setVisibility(View.VISIBLE);
                btn_redo.setVisibility(View.VISIBLE);

                makeToast("Gesture Detected and Views Switched");
                drawView.onClickRedo();
            } else if ("colorpicker".equalsIgnoreCase(result)) {
                //	gestures.setVisibility(View.GONE);
                drawView.setBackgroundColor(Color.WHITE);
                drawView.setVisibility(View.VISIBLE);

                int color = PreferenceManager.getDefaultSharedPreferences(
                        Whiteboard.this).getInt(COLOR_PREFERENCE_KEY,
                        Color.WHITE);
                new ColorPickerDialog(Whiteboard.this, Whiteboard.this,
                        color).show();
                makeToast("Choose color, center tap to select");

            }
            System.out.println(predictions.size() + " " + predictions.get(0).score);
        }
    }
    //Gesture Overlay View Change 03/10/2013
    //Overloaded for saving images when edited through gesture overlay

    /**
     * This method is called first when the eraser functionality is activated
     * by the user. This method is responsible for displaying a dialog box
     * showing various eraser sizes for the user to choose from. This method
     * also defines the OnClickListener functions for each of the eraser sizes.
     */
    public void eraseDraw() {
        System.out.println("when erase is clicked");
        final Dialog brushDialog = new Dialog(this);
        brushDialog.setTitle("Eraser size:");
        brushDialog.setContentView(R.layout.select_brush);
        //size buttons
        ImageButton smallBtn = (ImageButton) brushDialog.findViewById(R.id.small_brush);
        smallBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setErase(true);
                drawView.setSizeForBrush(smallBrush);
                brushDialog.dismiss();
            }
        });
        ImageButton mediumBtn = (ImageButton) brushDialog.findViewById(R.id.medium_brush);
        mediumBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setErase(true);
                drawView.setSizeForBrush(mediumBrush);
                brushDialog.dismiss();
            }
        });
        ImageButton largeBtn = (ImageButton) brushDialog.findViewById(R.id.large_brush);
        largeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                drawView.setErase(true);
                drawView.setSizeForBrush(largeBrush);
                brushDialog.dismiss();
            }
        });
        brushDialog.show();

    }

    /**
     * This method is called when the user enables the save functionality.
     * This method pops a dialog box asking a confirmation from the user of
     * the desired functionality. If the user selects "yes" then saveDrawing()
     * method is called by the "yes" button event listener to save the current
     * image into device gallery.
     *
     * @param bitmap Bitmap of current image
     */
    public void saveDrawing(final Bitmap bitmap) {
        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
        saveDialog.setTitle("Save drawing");
        saveDialog.setMessage("Save drawing to device Gallery?");
        saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String url = Images.Media.insertImage(getContentResolver(), bitmap, "title", ".png");
            }
        });
        saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        saveDialog.show();
    }

    /**
     * This method is called when the new drawing functionality is
     * enabled by the user. This function pops a dialog box to get
     * a confirmation on the desired functionality. If "yes" is
     * selected from the dialog box, the canvas is cleared, the
     * previous drawing is removed, and the canvas object is
     * re-initialized to its original state.
     */
    public void newDrawing() {
        drawView.setnewDraw(true);//i used this
        //drawView.itemValue();
        drawView.setBackgroundColor(Color.WHITE);
        drawView.newStart();
        drawView.setSizeForBrush(smallBrush);
        drawView.smoothStrokes = false;
        //new button

    }

    public void newDrawing1() {
        drawView.setnewDraw(true);//i used this
        //drawView.itemValue();
        drawView.setBackgroundColor(Color.WHITE);
        drawView.newStart();
        drawView.setSizeForBrush(smallBrush);
        drawView.smoothStrokes = false;
        //new button

    }

    /**
     * This method is called when the user selects "yes" from the save
     * dialog box. This method saves the current image on which the user
     * is working to the device gallery.
     */
    public void saveDrawing() {

        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            System.out.println("external available");
            System.out.println(getExternalFilesDir(null));
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            System.out.println("external available but readonly");
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
        saveDialog.setTitle("Save drawing");
        saveDialog.setMessage("Save drawing to device Gallery?");
        saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //save drawing
                Bitmap bitmap;
                //View v1 = findViewById(R.id.drawing);
                View v1 = drawView;
                v1.setDrawingCacheEnabled(true);
                bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                String url = Images.Media.insertImage(getContentResolver(), bitmap, "title", ".png");
                v1.setDrawingCacheEnabled(false);
				/*v1.setDrawingCacheEnabled(false);
		    	ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		    	bitmap.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
		    	String fileName = "test.jpg";
		    	File f = new File(Environment.getExternalStorageDirectory()
		    			+ File.separator + Environment.DIRECTORY_PICTURES + File.separator + fileName);*/

				/*f.createNewFile();
		    	FileOutputStream fo = null;
		    	fo = new FileOutputStream(f);
		    	fo.write(bytes.toByteArray());
		    	fo.close();*/
                //String url = Images.Media.insertImage(getContentResolver(), bitmap, "title", null);
            }
        });

        saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        saveDialog.show();
    }

    /**
     * This method is called when the user desires to change the color
     * to paint with. This method then sets the color object of the
     * view to the newly selected color.
     *
     * @param color Desired Color
     */
    @Override
    public void colorChanged(int color) {
        drawView.setPickerclose(true);
        //      drawView.socketrun();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(
                COLOR_PREFERENCE_KEY, color).commit();
        Integer i = new Integer(color);
        System.out.println("~~~~~~~~" + i.toHexString(color));
        System.out.println(Color.parseColor("#" + i.toHexString(color)));
        //String cc=i.toHexString(color);
        drawView.setColor("#" + i.toHexString(color));
    }

    /**
     * This method is called to initialize the options menu in
     * the action bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        System.out.println("oncreateoptionsmenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This method is called when the user desires to load an existing
     * image from device gallery into the application.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != 0) {
            //System.out.println("$$$$$$$$$$$$$$$$$$$$$$$"  + " " + new Integer(resultCode).toString() + " " + "$$$$$$$$$$$$$$$$$$");
            String picturePath = null;

            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
            }

            //System.out.println("<<<<<<<" + " " + picturePath + " " + "<<<<");
            // String picturePath contains the path of selected Image
            //imageView = (ImageView) findViewById(R.id.new_view);

            drawView.setUpDrawing();
            drawView.setSizeForBrush(mediumBrush);
            String filename = picturePath.substring(picturePath.lastIndexOf("/") + 1, picturePath.length());

            String filesplit[] = filename.split("\\.");
            drawView.sendimport(filesplit[0]);

            //Toast.makeText(getApplicationContext(),filename,Toast.LENGTH_LONG).show();

            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
            String bit = BitMapToString(bitmap);//for transfer with client
            drawView.setBitmap(bit);//for transfer with client
            //drawView.itemValue();//for transfer with client
            Drawable d = new BitmapDrawable(getResources(), bitmap);
            //drawView.newStart();
            drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
            drawView.setBackgroundColor(Color.WHITE);
            drawView.newStart();
            drawView.setSizeForBrush(mediumBrush);
            drawView.smoothStrokes = false;

            drawView.setBackground(d);
            //drawView.invalidate();


        } else {
            drawView.setVisibility(View.VISIBLE);
        }
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] arr = baos.toByteArray();
        String result = Base64.encodeToString(arr, Base64.DEFAULT);
        return result;
    }

    private void initiatePopupWindow(final Context context, final ByteArrayOutputStream baos) {
        final PopupWindow pwindo;

        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) Whiteboard.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            Dialog dialog=new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(layout);
            dialog.show();
//            pwindo = new PopupWindow(layout, 450, 140, true);
//            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            final EditText name = (EditText) layout.findViewById(R.id.name);
            RelativeLayout header = (RelativeLayout) layout.findViewById(R.id.popupheader);
            header.setVisibility(View.VISIBLE);

            Button btnconnect = (Button) layout.findViewById(R.id.ok);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    filename = name.getText().toString();
                    if (!filename.equalsIgnoreCase("")) {
                        if (synboard) {
                            drawView.setSave(true);
                            drawView.setFilename(filename);
                            drawView.savefile();
                        }
                        String fileName = filename + ".png";

                        if (Environment.getExternalStorageState()
                                .equals(Environment.MEDIA_MOUNTED)) {
                            File sdCard = Environment.getExternalStorageDirectory();
                            File dir = new File(notepath);
                            //	boolean directoryCreated = dir.mkdirs();

                            File file = new File(dir, fileName);

                            FileOutputStream f;
                            try {
                                f = new FileOutputStream(file);
                                f.write(baos.toByteArray());
                                f.flush();
                                f.close();
                                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT)
                                        .show();
                                // Trigger gallery refresh on photo save
                                MediaScannerConnection.scanFile(
                                        getApplicationContext(),
                                        new String[]{file.toString()},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Save Failed!",
                                        Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }

                    }


                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.cancel);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmSave() {
        drawView.setDrawingCacheEnabled(true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        drawView.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, baos);
        //saveWhiteboardImage(this, baos);
        initiatePopupWindow(this, baos);
        drawView.destroyDrawingCache();
    }

    private void confirmSave1() {
        drawView.setDrawingCacheEnabled(true);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        drawView.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, baos);
        //saveWhiteboardImage(this, baos);
        initiatePopupWindow1(this, baos);
        drawView.destroyDrawingCache();
    }

    private void initiatePopupWindow1(final Context context, final ByteArrayOutputStream baos) {
        final PopupWindow pwindo;

        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) Whiteboard.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.screen_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 450, 140, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            final EditText name = (EditText) layout.findViewById(R.id.name);
            RelativeLayout header = (RelativeLayout) layout.findViewById(R.id.popupheader);
            header.setVisibility(View.VISIBLE);

            Button btnconnect = (Button) layout.findViewById(R.id.ok);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    filename = name.getText().toString();
                    if (!filename.equalsIgnoreCase("")) {
                        if (synboard) {
                            drawView.setSave(true);
                            drawView.setFilename(filename);
                            drawView.savefile();
                        }
                        String fileName = filename + ".png";

                        if (Environment.getExternalStorageState()
                                .equals(Environment.MEDIA_MOUNTED)) {
                            File sdCard = Environment.getExternalStorageDirectory();
                            File dir = new File(notepath);

                            //File dir = new File(sdCard.getAbsolutePath() + "/Whiteboard");
                            boolean directoryCreated = dir.mkdirs();

                            File file = new File(dir, fileName);

                            FileOutputStream f;
                            try {
                                f = new FileOutputStream(file);
                                f.write(baos.toByteArray());
                                f.flush();
                                f.close();
                                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT)
                                        .show();
                                // Trigger gallery refresh on photo save
                                MediaScannerConnection.scanFile(
                                        getApplicationContext(),
                                        new String[]{file.toString()},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(getApplicationContext(), "Save Failed!",
                                        Toast.LENGTH_SHORT).show();
                            }
                            newDrawing();
                            pwindo.dismiss();
                        }

                    }


                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.cancel);
            btncancel.setText("Clear");
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newDrawing();
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveWhiteboardImage(final Context context, final ByteArrayOutputStream baos) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirm canvas save")
                .setMessage("Do you want to save the canvas?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        drawView.setSave(true);
                        drawView.itemValue();
                        Date date = new Date();
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss", Locale.US);
                        String fileName = formatter.format(date) + ".png";
                        if (Environment.getExternalStorageState()
                                .equals(Environment.MEDIA_MOUNTED)) {
                            File sdCard = Environment.getExternalStorageDirectory();
                            File dir = new File(sdCard.getAbsolutePath() + "/Whiteboard");
                            boolean directoryCreated = dir.mkdirs();

                            File file = new File(dir, fileName);

                            FileOutputStream f;
                            try {
                                f = new FileOutputStream(file);
                                f.write(baos.toByteArray());
                                f.flush();
                                f.close();
                                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT)
                                        .show();
                                // Trigger gallery refresh on photo save
                                MediaScannerConnection.scanFile(
                                        context,
                                        new String[]{file.toString()},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Save Failed!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    /**
     * This method is used to return the current status of the drawView
     *
     * @return drawView the current status of the drawView
     */
    public CustomViewForDrawing getDrawView() {
        return drawView;
    }

    /**
     * This method is called to display a toast message on device
     * screen.
     *
     * @param message
     */
    public void makeToast(String message) {
        // with jam obviously
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    //user clicks on paint
    public void paintClicked(View view) {
        System.out.println("paintclicked");
        drawView.setErase(false);
        drawView.setSizeForBrush(drawView.getPrevBrushSize());

        if (view != currPaint) {
            ImageButton imgView = (ImageButton) view;
            String color = view.getTag().toString();

            drawView.setColor(color);

            imgView.setImageDrawable(getResources().getDrawable(R.drawable.paint_pressed));
            currPaint.setImageDrawable(getResources().getDrawable(R.drawable.paint));
            currPaint = (ImageButton) view;
        }
    }

    /**
     * This function defines all the event listeners for the
     * icons in the main layout of the application.
     */
    @Override
    public void onClick(View v) {
        //drawView.setVisibility(View.VISIBLE);
        //imageView.setVisibility(View.GONE);
        System.out.println("onclick");
        // TODO Auto-generated method stub
        if (v.getId() == R.id.draw_btn) {
//            drawBtn.setBackgroundResource(R.color.grey);//for change
//            eraseBtn.setBackgroundResource(R.color.white);
//            btn_undo.setBackgroundResource(R.color.white);
//            saveBtn.setBackgroundResource(R.color.white);
//            gestureBtn.setBackgroundResource(R.color.white);
//            //syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setBackgroundResource(R.color.white);
//            drawBtn.setImageResource(R.drawable.unpaint_br);//for change
//            eraseBtn.setImageResource(R.drawable.eraser);
//            btn_undo.setImageResource(R.drawable.undo);
//            saveBtn.setImageResource(R.drawable.save);
//            gestureBtn.setImageResource(R.drawable.importimage);
//            //	syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setImageResource(R.drawable.colorpicker);
//            //draw button clicked
            final Dialog brushDialog = new Dialog(this);
            brushDialog.setTitle("Brush size:");
            brushDialog.setContentView(R.layout.select_brush);

            //Different buttons for varying size brushes; implement gestures in it
            ImageButton smallBtn = (ImageButton) brushDialog.findViewById(R.id.small_brush);
            smallBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //drawView.setErase(false);
                    drawView.setSizeForBrush(smallBrush);
                    drawView.setPrevBrushSize(smallBrush);
                    drawView.setErase(false);
                    if (synboard) {
                        drawView.setClickbrush(true);
                        drawView.sendpencil();
                    }
                    brushDialog.dismiss();
                }
            });
            ImageButton mediumBtn = (ImageButton) brushDialog.findViewById(R.id.medium_brush);
            mediumBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    //drawView.setErase(false);
                    drawView.setSizeForBrush(mediumBrush);
                    drawView.setPrevBrushSize(mediumBrush);
                    drawView.setErase(false);
                    if (synboard) {
                        drawView.setClickbrush(true);
                        drawView.sendpencil();
                    }
                    brushDialog.dismiss();
                }
            });
            ImageButton largeBtn = (ImageButton) brushDialog.findViewById(R.id.large_brush);
            largeBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawView.setErase(false);
                    drawView.setSizeForBrush(largeBrush);
                    drawView.setPrevBrushSize(largeBrush);
                    if (synboard) {
                        drawView.setClickbrush(true);
                        drawView.sendpencil();
                    }
                    brushDialog.dismiss();
                }
            });
            //show and wait for user interaction
            brushDialog.show();

        } else if (v.getId() == R.id.erase_btn) {
//for image change
//            drawBtn.setImageResource(R.drawable.paint_br);//for change
//            eraseBtn.setImageResource(R.drawable.unerase);
//            btn_undo.setImageResource(R.drawable.undo);
//            saveBtn.setImageResource(R.drawable.save);
//            gestureBtn.setImageResource(R.drawable.importimage);
//            //	syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setImageResource(R.drawable.colorpicker);

            drawBtn.setImageResource(R.drawable.paint_icon);//for change
            eraseBtn.setImageResource(R.drawable.sel_erase_icon);
            btn_undo.setImageResource(R.drawable.back_arrow);

            gestureBtn.setImageResource(R.drawable.folder_icon);
           // syncwhiteboard.setImageResource(R.drawable.sync_icon);

//            drawBtn.setBackgroundResource(R.color.white);//for change
//            eraseBtn.setBackgroundResource(R.color.grey);
//            btn_undo.setBackgroundResource(R.color.white);
//            saveBtn.setBackgroundResource(R.color.white);
//            gestureBtn.setBackgroundResource(R.color.white);
//            //syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setBackgroundResource(R.color.white);


            System.out.println("when erase is clicked");
            drawView.setErase(true);
            drawView.senderase();
            colorPickerBtn.setImageResource(R.drawable.colorpicker);
        } else if (v.getId() == R.id.save_btn) {
//for image change
//            drawBtn.setImageResource(R.drawable.paint_br);//for change
//            eraseBtn.setImageResource(R.drawable.eraser);
//            btn_undo.setImageResource(R.drawable.undo);
//            saveBtn.setImageResource(R.drawable.unsave);
//            gestureBtn.setImageResource(R.drawable.importimage);
//            //syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setImageResource(R.drawable.colorpicker);

            drawBtn.setBackgroundResource(R.color.white);//for change
            eraseBtn.setBackgroundResource(R.color.white);
            btn_undo.setBackgroundResource(R.color.white);
            saveBtn.setBackgroundResource(R.color.grey);
            gestureBtn.setBackgroundResource(R.color.white);
            //syncwhiteboard.setImageResource(R.drawable.sync);
            colorPickerBtn.setBackgroundResource(R.color.white);


            //	saveDrawing();
            confirmSave();
            colorPickerBtn.setImageResource(R.drawable.colorpicker);
        } else if (v.getId() == R.id.save_lay) {
            drawBtn.setBackgroundResource(R.color.white);//for change
            eraseBtn.setBackgroundResource(R.color.white);
            btn_undo.setBackgroundResource(R.color.white);
            saveBtn.setBackgroundResource(R.color.grey);
            gestureBtn.setBackgroundResource(R.color.white);
            //syncwhiteboard.setImageResource(R.drawable.sync);
            colorPickerBtn.setBackgroundResource(R.color.white);


            //	saveDrawing();
            confirmSave();
            colorPickerBtn.setImageResource(R.drawable.colorpicker);
        } else if (v.getId() == R.id.new_btn) {
            newDrawing();
            colorPickerBtn.setImageResource(R.drawable.colorpicker);
        } else if (v.getId() == R.id.color_picker) {
//            //for image change
//            drawBtn.setImageResource(R.drawable.paint_br);//for change
//            eraseBtn.setImageResource(R.drawable.eraser);
//            btn_undo.setImageResource(R.drawable.undo);
//            saveBtn.setImageResource(R.drawable.save);
//            gestureBtn.setImageResource(R.drawable.importimage);
//            ///	syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setImageResource(R.drawable.uncolorpicker);


            //if(!colorpickch){
            //  colorpickch=true;
            drawView.setErase(false);
            showDialog(COLOR_PICKER_DIALOG_ID);
            colorPickerBtn.setImageResource(R.drawable.uncolorpicker);
            //}
            //else{
            //	colorpickch=false;
            //   colorPickerBtn.setImageResource(R.drawable.colorpicker);
            //	}
			/*drawView.setPickeropen(true);
			drawView.itemValue();
			int color = PreferenceManager.getDefaultSharedPreferences(
					MainActivity.this).getInt(COLOR_PREFERENCE_KEY,
							Color.WHITE);
			new ColorPickerDialog(MainActivity.this, MainActivity.this,
					color).show();*/
        }
        //Gesture Overlay View Change 03/10/2013
	/*	else if(v.getId()==R.id.color_fill)
		{


			boolean smoothStatus = drawView.smoothStrokes;
			drawView.setUpDrawing();
			drawView.smoothStrokes = smoothStatus;
			drawView.setSizeForBrush(mediumBrush);
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
			drawView.getCanvas().drawColor(0, PorterDuff.Mode.SRC);
			int color = drawView.getColor();
			drawView.getCanvas().drawColor(color);
			drawView.invalidate();
			//paintClicked(drawView);

		}*/
        //Gesture Overlay View Change 03/10/2013
        else if (v.getId() == R.id.imp) {
            //for image change
            drawBtn.setImageResource(R.drawable.paint_icon);//for change
            eraseBtn.setImageResource(R.drawable.erase_icon);
            btn_undo.setImageResource(R.drawable.back_arrow);

            gestureBtn.setImageResource(R.drawable.sel_folder_icon);
        //    syncwhiteboard.setImageResource(R.drawable.sync_icon);

//            drawBtn.setImageResource(R.drawable.paint_br);//for change
//            eraseBtn.setImageResource(R.drawable.eraser);
//            btn_undo.setImageResource(R.drawable.undo);
//            saveBtn.setImageResource(R.drawable.save);
//            gestureBtn.setImageResource(R.drawable.unimport);
//            //	syncwhiteboard.setImageResource(R.drawable.sync);
//            colorPickerBtn.setImageResource(R.drawable.colorpicker);


            //	File root = new File(pref.getString("notes", "")
            //			+"/");
            //Uri uri = Uri.fromFile(root);

            File f = new File(pref.getString("notes", ""));
            getfile(f);

            whiteboardsharing = false;
            if (pathList.size() > 0) {
                makeToast("Refreshing...");


                ImportWindow(this);
            } else {
                Toast.makeText(Whiteboard.this, "Sorry, you do not have any saved notes.", Toast.LENGTH_SHORT).show();

            }
		/*	Intent intent = new Intent (Intent.ACTION_VIEW);
			var uri = Android.Net.Uri.Parse(Android.OS.Environment.ExternalStorageDirectory.Path + "/UnicardMerchant/Transactions/");
			intent.SetDataAndType (uri, "resource/folder");*/
            //StartActivity (Intent.CreateChooser (intent, "Open folder"));
            //startActivityForResult(i, RESULT_LOAD_IMAGE);


        }


    }

    public ArrayList<String> getfile(File dir) {

        pathList = new ArrayList<>();

        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].getName().endsWith(".png")
                        || listFile[i].getName().endsWith(".jpg")
                        || listFile[i].getName().endsWith(".jpeg")
                        || listFile[i].getName().endsWith(".gif")) {
                    //String temp = listFile.getPath().substring(0, file.getPath().lastIndexOf('/'));
                    // fileList.add(temp);
                    ;
                    Log.e("imagename", listFile[i].getAbsolutePath());
                    pathList.add(listFile[i].getAbsolutePath());
                }
            }
        }
        return pathList;
    }

    private void ImportWindow(final Context context) {

        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) Whiteboard.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.import_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 650, 530, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            GridView gridView = (GridView) layout.findViewById(R.id.gridView);


            adapter = new Importadapter(Whiteboard.this,
                    R.layout.booklistitem, pathList);

            gridView.setAdapter(adapter);


            Button btncancel = (Button) layout.findViewById(R.id.cancel);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setImportedImaged(String img) {

        pwindo.dismiss();


        if (!whiteboardsharing) {
            drawView.setUpDrawing();
            drawView.setSizeForBrush(mediumBrush);
            Bitmap bitmap = BitmapFactory.decodeFile(img);
            Drawable d = new BitmapDrawable(getResources(), bitmap);
            //drawView.newStart();
            drawView.getCanvas().drawColor(0, PorterDuff.Mode.CLEAR);
            drawView.setBackgroundColor(Color.WHITE);
            drawView.newStart();
            drawView.setSizeForBrush(mediumBrush);
            drawView.smoothStrokes = false;

            drawView.setBackground(d);

            if (synboard) {
                String filename = img.substring(img.lastIndexOf("/") + 1, img.length());

                String filesplit[] = filename.split("\\.");
                drawView.sendimport(filesplit[0]);
            }
        } else {
            File file = new File(img);

            stratExamCompletedPopup(file);
            //whiteboardsharing=false;


        }

    }

    public void stratExamCompletedPopup(final File file) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        title.setText("WHITEBOARD BOOK SHARING");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //File file=new File(video.getVideopath());

                String senddata = "Whiteboardnotes" + "@" + file.getName() + "@" + file.length();
                new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                if (MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //File file=new File(video.getVideopath());

                if (MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                String senddata = "Whiteboardnotes" + "@" + file.getName() + "@" + file.length();

                Intent in = new Intent(Whiteboard.this, Attendancewithcheckparentlayout.class);
                in.putExtra("senddata", senddata);
                startActivity(in);


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        forhandrais = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (LoginActivity.handraise.size() > 0) {
                    handrise.setImageResource(R.drawable.sel_raise_icon);
                    //Utils.Listpopup(Whiteboard.this);
                } else {
                    //Toast.makeText(Whiteboard.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                    handrise.setImageResource(R.drawable.raise_icon);

                    //Toast.makeText(MuPDFActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                }
            }
        };
        IntentFilter fileterhandraise = new IntentFilter("handraise");
        registerReceiver(forhandrais, fileterhandraise);
        if (LoginActivity.handraise!=null){
            if (LoginActivity.handraise.size() > 0) {
                handrise.setImageResource(R.drawable.sel_raise_icon);
                //Utils.Listpopup(Whiteboard.this);
            } else {
                //Toast.makeText(Whiteboard.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                handrise.setImageResource(R.drawable.raise_icon);

                //Toast.makeText(MuPDFActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(forhandrais);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {

            case COLOR_PICKER_DIALOG_ID:
                final Dialog dialog = new Dialog(this);
                dialog.setTitle("Choose colour");
                dialog.setContentView(R.layout.color_picker);
                GridView gridView = (GridView) dialog
                        .findViewById(R.id.color_picker_gridview);
                gridView.setAdapter(new ArrayAdapter<Integer>(this, 0, COLORS) {
                    @Override
                    public View getView(int position, View convertView,
                                        ViewGroup parent) {
                        final int color = COLORS[position];
                        View colorView = new View(Whiteboard.this);
                        colorView.setMinimumWidth(45);
                        colorView.setMinimumHeight(45);
                        colorView.setBackgroundColor(color);
                        return colorView;
                    }
                });
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View v,
                                            int position, long id) {
                        drawView.setPrimColor(COLORS[position]);
                        if (synboard)
                            drawView.sendpencil();

                        dialog.dismiss();
                    }
                });
                return dialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void OnItemClick(View view, int pos) {
        //view.setBackground(getResources().getDrawable(R.drawable.round_border));


        drawView.setPrimColor(COLORS[pos]);
        if (synboard)
            drawView.sendpencil();

    }

    public class SocketServerThread extends Thread {

        static final int SocketServerPORT = 1995;
        int count = 0;

        @Override
        public void run() {
            try {
                serverSocket = new ServerSocket(SocketServerPORT);
                Whiteboard.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "I am waiting" + serverSocket.getLocalPort(), Toast.LENGTH_LONG).show();
                        /*info.setText("I'm wai ting here: "
                                + serverSocket.getLocalPort());*/
                    }
                });

                while (true) {
                    final Socket socket = serverSocket.accept();
                    count++;
                    drawView.setSocket(socket);

                    // message += "#" + count + " from " + socket.getInetAddress()
                    //           + ":" + socket.getPort() + "\n";

                    Whiteboard.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            //  msg.setText(message);
                            i++;
                            if (i <= 4)
                                Toast.makeText(getApplicationContext(), "client" + socket.getInetAddress() + "," + socket.getPort(), Toast.LENGTH_LONG).show();
                        }
                    });

                    //   SocketServerReplyThread socketServerReplyThread = new SocketServerReplyThread(socket, count);
                    //     socketServerReplyThread.run();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }
}