package drawboard;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.R;

import connection.MultiThreadChatServerSync;
import connection.clientThread;

/**
 * This class contains the attributes for the main layout of
 * our application.
 * @author PrateekMehrotra
 *
 */
@SuppressLint("NewApi")
public class CustomViewForDrawing extends View{

	private CustomPath drawPath;
	private Path lastPath;
	private Bitmap canvasBitmap;
	private Bitmap backup;

	private Paint drawPaint;
	private Paint canvasPaint;
	private int paintColor = 0xFF787878;

	private Canvas drawCanvas;
	private Canvas backUpCanvas;

	private float brushSize;
	private float prevBrushSize; // when switching, store previous

	private boolean erase = false,pickeropen=false,pickerclose=false,newdraw=false,setsave=false,undo=false,clickbrush=false;
	public boolean smoothStrokes = false;
    String filename;
	private float startX;
	private float startY;
	private float endX;
	private float endY;
    private int soc=0;
	public ArrayList<String> movex;
	public ArrayList<String> movey;
	public ArrayList<String> linex;
	public ArrayList<String> liney;
    String bitmap;
	Socket socket;
	Activity activity;
	private static final String BRIGHTNESS_PREFERENCE_KEY = "brightness";
	private static final String COLOR_PREFERENCE_KEY = "color";
	TextView tv;
	public static ArrayList<CustomPath> paths = new ArrayList<CustomPath>();
	private ArrayList<CustomPath> undonePaths = new ArrayList<CustomPath>();
	//private Map<Path, Integer> colorsMap = new HashMap<Path, Integer>();
	public static int selectedColor;


	/**
	 * The constructor for CustomViewForDrawing
	 * This constructor calls the setupDrawing()
	 * method. This constructor is called only
	 * once when the application layout is first
	 * created upon launch.
	 * @param context
	 * @param attrs
	 */
	public CustomViewForDrawing(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		System.out.println("customviewfordrawing constructor");
		setUpDrawing();
	}
	public void setContext(Activity activity){
		this.activity=activity;
	}
	/**
	 * This function returns the current canvas
	 * being worked upon by the user.
	 * @return drawCanvas the current canvas being
	 * worked upon by the user
	 */
	public Canvas getCanvas()
	{
		return drawCanvas;
	}

	/**
	 * This method initializes the attributes of the
	 * CustomViewForDrawing class.
	 */
	public void setUpDrawing(){
		System.out.println("setup drawing");
		drawPaint = new Paint();;
		drawPath = new CustomPath(paintColor, brushSize);
		erase = false;
		smoothStrokes = false;
		drawPaint.setColor(paintColor);
		drawPaint.setAntiAlias(true);
		drawPaint.setStyle(Paint.Style.STROKE);
		drawPaint.setStrokeJoin(Paint.Join.ROUND);
		drawPaint.setStrokeCap(Paint.Cap.ROUND);

		canvasPaint = new Paint(Paint.DITHER_FLAG);


		brushSize = getResources().getInteger(R.integer.medium_size);
		prevBrushSize = brushSize;
		//selectedColor = getResources().getColor(Color.CYAN);
	}

	@Override
	protected void onSizeChanged(int w, int h, int wprev, int hprev){
		super.onSizeChanged(w, h, wprev, hprev);
		System.out.println("onsizechanged");
		canvasBitmap = Bitmap.createBitmap(w,h,Bitmap.Config.ARGB_8888);
		drawCanvas = new Canvas(canvasBitmap);
	}

	/**
	 * This method is called when a stroke is drawn on the canvas
	 * as a part of the painting.
	 */
	@Override
	protected void onDraw(Canvas canvas){
		//	System.out.println("onDraw");
		super.onDraw(canvas);
		canvas.drawBitmap(canvasBitmap, 0, 0, canvasPaint);

		for (CustomPath p : paths){
			drawPaint.setStrokeWidth(p.getBrushThickness());
			drawPaint.setColor(p.getColor());
			canvas.drawPath(p, drawPaint);
		}
		if(!drawPath.isEmpty()) {
			drawPaint.setStrokeWidth(drawPath.getBrushThickness());
			drawPaint.setColor(drawPath.getColor());
			canvas.drawPath(drawPath, drawPaint);
		}
	}

	public void setList(ArrayList<String> movex,ArrayList<String> movey,ArrayList<String> linex,ArrayList<String> liney){
		this.movex=movex;
		this.movey=movey;
		this.linex=linex;
		this.liney=liney;

	}

	public void setSocket(Socket socket){
		this.socket=socket;
	}

	public void  draw(){
		setSizeForBrush(30);
		drawPath.setColor(paintColor);
		drawPath.setBrushThickness(30);
		drawPath.moveTo(408,285);
		drawPath.lineTo(408,285);
		drawPath.lineTo(408,285);

		invalidate();

	}
	/**
	 * This method acts as an event listener when a touch
	 * event is detected on the device.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event){
		//	System.out.println("ontouchevent");
		float touchX = event.getX();
		float touchY = event.getY();
		//System.out.println(touchX + " " + touchY);

		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				setPickeropen(false);
				setnewDraw(false);
				setSave(false);
				setBitmap(null);
				setUndo(false);
				setClickbrush(false);
				if(erase){
					drawPath.setColor(Color.parseColor("#FFFFFF"));
				}else{
				drawPath.setColor(paintColor);}
				drawPath.setBrushThickness(brushSize);
				if(smoothStrokes & !erase) {
					startX = touchX;
					startY = touchY;
				}
				//undonePaths.clear();
				drawPath.reset();
				Log.d("moveto",touchX+","+touchY);


				drawPath.moveTo(touchX, touchY);
				movex.add(String.valueOf(Math.round(touchX)));
				movey.add(String.valueOf(Math.round(touchY)));


				break;
			case MotionEvent.ACTION_MOVE:
			    Log.d("lineto",touchX+","+touchY);

				drawPath.lineTo(touchX, touchY);
				linex.add(String.valueOf(Math.round(touchX)));
				liney.add(String.valueOf(Math.round(touchY)));


				break;
			case MotionEvent.ACTION_UP:
				if(smoothStrokes && !erase) {

					endX = touchX;
					endY = touchY;
					drawPath.reset();
					drawPath.moveTo(startX, startY);
					drawPath.lineTo(endX, endY);
				}

             // Log.d("Upevent","work");
				//if(Whiteboard)

				if(Whiteboard.synboard) {
					itemValue();
				}
				paths.add(drawPath);
				drawPath = new CustomPath(paintColor, brushSize);

				movex.clear();
				movey.clear();
				linex.clear();
				liney.clear();

				//drawPath.reset();
				break;
			default:
				return false;
		}
//		Log.d("Invalidate","method");
		invalidate();
		return true;
	}

	public void  senderase()
	{
		if(erase){
			String whiteboard = "color" + ":"+"-1";
			for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
				clientThread thread = MultiThreadChatServerSync.thread.get(i);

				String socketiddress=thread.info.getStudentip();

				//String socketiddress=thread.clientSocket.getInetAddress().toString().replace("/","");

				ThreadB b = new ThreadB(whiteboard,socketiddress);
				b.start();
			}
		}
	}

	public void  sendpencil()
	{

			String whiteboard = "color" + ":"+"1:"+getColor()+":"+getPrevBrushSize();
			for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
				clientThread thread = MultiThreadChatServerSync.thread.get(i);

				String socketiddress=thread.info.getStudentip();

				//String socketiddress=thread.clientSocket.getInetAddress().toString().replace("/","");

				ThreadB b = new ThreadB(whiteboard,socketiddress);
				b.start();
			}
		//}
	}

	public void sendbrush()
	{

		if(clickbrush) {

			String whiteboard = "size" + ":" + getPrevBrushSize();
			for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
				clientThread thread = MultiThreadChatServerSync.thread.get(i);

				String socketiddress=thread.info.getStudentip();

				//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

				ThreadB b = new ThreadB(whiteboard, socketiddress);
				b.start();
			}
		}
	}
	public  void sendimport(String filename)
	{

		String whiteboard="currentFile:"+filename;
		for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
			clientThread thread = MultiThreadChatServerSync.thread.get(i);

			String socketiddress=thread.info.getStudentip();

			//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

			ThreadB b = new ThreadB(whiteboard, socketiddress);
			b.start();
		}


	}

    public void sendcolorPicker(){

		String whiteboard = "color" + ":" + getColor();
		for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
			clientThread thread = MultiThreadChatServerSync.thread.get(i);

			String socketiddress=thread.info.getStudentip();

			//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

			ThreadB b = new ThreadB(whiteboard, socketiddress);
			b.start();
		}

	}
	public void undo()
	{

		String whiteboard = "undo";
		for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
			clientThread thread = MultiThreadChatServerSync.thread.get(i);

			String socketiddress=thread.info.getStudentip();

			//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

			ThreadB b = new ThreadB(whiteboard, socketiddress);
			b.start();
		}
	}

	public void close()
	{

		String whiteboard = "close";
		for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
			clientThread thread = MultiThreadChatServerSync.thread.get(i);

			String socketiddress=thread.info.getStudentip();

			//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

			ThreadB b = new ThreadB(whiteboard, socketiddress);
			b.start();
		}
	}


	public void savefile()
	{


			String whiteboard = "save" + ":"+getFilename();
			for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
				clientThread thread = MultiThreadChatServerSync.thread.get(i);

				String socketiddress=thread.info.getStudentip();

				//String socketiddress = thread.clientSocket.getInetAddress().toString().replace("/", "");

				ThreadB b = new ThreadB(whiteboard, socketiddress);
				b.start();
			}


	}


	public void itemValue(){

		Drawitem item;
		synchronized (this) {
			item = new Drawitem(movex, movey, linex, liney, erase ? "1" : "0", pickeropen ? "1" : "0", pickerclose ? "1" : "0", getColor(), newdraw ? "1" : "0", setsave ? "1" : "0", getBitmap(), undo ? "1" : "0", getPrevBrushSize());
			//	SocketServerReplyThread1 socketServerReplyThread1 = new SocketServerReplyThread1(item, socket, 0);

			//	socketServerReplyThread1.run();


			/*else{*/
				//String whiteboard = TextUtils.join(",", movex) + "#" +TextUtils.join(",", movey) + "#" + TextUtils.join(",", linex) + "#" + TextUtils.join(",", liney);

				String whiteboard=new String();
				for(int i=0; i<linex.size(); i++)
				{

					if(i==0)
					{
						whiteboard="0:"+linex.get(i)+":"+liney.get(i)+"#";
					}
					else if(i<linex.size()-1)
					{
						whiteboard=whiteboard+"2:"+linex.get(i)+":"+liney.get(i)+"#";
					}else
					{
						whiteboard=whiteboard+"1:"+linex.get(i)+":"+liney.get(i);
					}
				}

				for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
					clientThread thread = MultiThreadChatServerSync.thread.get(i);


					//String socketiddress=thread.clientSocket.getInetAddress().toString().replace("/","");
					String socketiddress=thread.info.getStudentip();
					ThreadB b = new ThreadB(whiteboard,socketiddress);
					b.start();
				}

			//}
			//String whiteboard = TextUtils.join(",", movex) + "@" +TextUtils.join(",", movey) + "@" + TextUtils.join(",", linex) + "@" + TextUtils.join(",", liney) + "@" + "" + getColor() + "@" + newdraw + "@" + setsave;


		/*synchronized(b) {
			try {
				System.out.println("Waiting for b to complete...");
				b.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}*/
		}
		}

	public void itemValue1(){

		Drawitem item1;
		item1=new Drawitem(movex,movey,linex,liney,erase?"1":"0",pickeropen?"1":"0",pickerclose?"1":"0",getColor(),newdraw?"1":"0",setsave?"1":"0",getBitmap(),undo?"1":"0",getPrevBrushSize());
		SocketServerReplyThread2 socketServerReplyThread2 = new SocketServerReplyThread2(item1, socket, 0);

		socketServerReplyThread2.run();
	}


	public void socketrun() {


	}
	class ThreadB extends Thread{
		int total;
		private Socket hostThreadSocket;
		int cnt;
		Drawitem item;
		String msg;
		String ip;
		ThreadB(String sendmsg,String ipaddress) {
			msg=sendmsg;
			ip=ipaddress;
			/*hostThreadSocket = socket;
			cnt = c;
			this.item=item;*/
		}
		@Override
		public void run(){
			synchronized(this){
				OutputStream outputStream;

				try {
					Socket newsocket=new Socket(ip,1995);
					byte[] b=msg.getBytes();
					int len=b.length;
					OutputStream os = newsocket.getOutputStream();
					DataOutputStream dos = new DataOutputStream(os);

					//  dos.writeInt(len);

					dos.write(b);

					dos.flush();
					dos.close();
					newsocket.close();
					//socket.close();

				/*	ObjectOutputStream objectOutput = new ObjectOutputStream(newsocket.getOutputStream());
			//	objectOutput.reset();
					objectOutput.writeObject(item);
					objectOutput.flush();*/

				} catch(SocketException e){
					e.printStackTrace();
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							//  msg.setText(message);

							//	Toast.makeText(activity.getApplicationContext(),"socket notconnected",Toast.LENGTH_LONG).show();
						}
					});
//					Toast.makeText(activity.getApplicationContext(),"socket timeout",Toast.LENGTH_LONG).show();
				} catch(IOException e){
					e.printStackTrace();
				}
				catch(NullPointerException e){
					//client not connected
					e.printStackTrace();
				}


				notify();
			}
		}
	}


	public class SocketServerReplyThread1 extends Thread {

		private Socket hostThreadSocket;
		int cnt;
		Drawitem item;
		SocketServerReplyThread1(Drawitem item,Socket socket, int c) {
			hostThreadSocket = socket;
			cnt = c;
			this.item=item;
		}


		@Override
		public void run() {
			OutputStream outputStream;

			try {

				ObjectOutputStream objectOutput = new ObjectOutputStream(hostThreadSocket.getOutputStream());
			//	objectOutput.reset();
				objectOutput.writeObject(item);
				objectOutput.flush();



			} catch(SocketException e){
				e.printStackTrace();
				Toast.makeText(activity.getApplicationContext(),"socket timeout",Toast.LENGTH_LONG).show();
			} catch(IOException e){
				e.printStackTrace();
			}
			catch(NullPointerException e){
				//client not connected
				e.printStackTrace();
			}


		}

	}

	public class SocketServerReplyThread2 extends Thread {

		private Socket hostThreadSocket;
		int cnt;
		Drawitem item1;
		SocketServerReplyThread2(Drawitem item1,Socket socket, int c) {
			hostThreadSocket = socket;
			cnt = c;
			this.item1=item1;
		}


		@Override
		public void run() {
			OutputStream outputStream;


			try {

				ObjectOutputStream objectOutput = new ObjectOutputStream(hostThreadSocket.getOutputStream());

				objectOutput.writeObject(item1);

				objectOutput.flush();

			} catch(SocketException e){
				e.printStackTrace();
				Toast.makeText(activity.getApplicationContext(),"socket timeout",Toast.LENGTH_LONG).show();
			} catch(IOException e){
				e.printStackTrace();
			}
			catch(NullPointerException e){
				//client not connected
				e.printStackTrace();
			}


		}

	}





	public void setPickeropen(Boolean pickeropen){
		this.pickeropen=pickeropen;
	}

	public void setPickerclose(Boolean pickerclose){
		this.pickerclose=pickerclose;
	}

	public void setnewDraw(Boolean newdraw){
		this.newdraw=newdraw;
	}
	public void setSave(Boolean setsave){
       this.setsave=setsave;
	}
    public void setBitmap(String bitmap){
     this.bitmap=bitmap;
	}
	public String getBitmap(){
		return bitmap;
	}
	public void setUndo(Boolean undo){
		this.undo=undo;
	}
    public void setClickbrush(Boolean clickbrush){
		this.clickbrush=clickbrush;
	}
    public void setFilename(String filename){
		this.filename=filename;
	}
	public String getFilename(){
		return filename;
	}

	/**
	 * This function is called when the user selects the undo
	 * command from the application. This function removes the
	 * last stroke input by the user depending on the
	 * number of times undo has been activated.
	 */
	public void onClickUndo () {
		if (paths.size()>0)  {

			undonePaths.add(paths.remove(paths.size()-1));
			invalidate();
		} else  {
			Log.i("undo", "Undo elsecondition");
		}
	}

	/**
	 * This function is called when the user selects the redo
	 * command from the application. This function redoes the
	 * last stroke undone by the user depending on the
	 * number of times redo has been activated.Max number of
	 * redo depends on the number of undo's activated by the user.
	 */
	public void onClickRedo (){
		if (undonePaths.size()>0)  {
			paths.add(undonePaths.remove(undonePaths.size()-1));
			invalidate();
		}
		else  {
			Log.i("undo", "Redo elsecondition");
		}
	}

	/**
	 * This function is called when the user desires a color change.
	 * This functions sets the paintColor object of CustomViewForDrawing.
	 * @param newColor
	 */
	public void setColor(String newColor){

		System.out.println("setcolor");
		//invalidate();
		System.out.println(newColor + " !!!!!!!!!!!!!!!!!!!!!!!!!!");
		paintColor = Color.parseColor(newColor);
		drawPaint.setColor(paintColor);
	}
	protected void setPrimColor(int c) {
		paintColor = c;
		drawPaint.setColor(paintColor);
	}
	/**
	 * This function returns the current color that has been
	 * selected.
	 * @return current color
	 */
	public int getColor()
	{
		return paintColor;
	}
    public void setPencilColor(int c){

	}
	/**This method is called when either the brush or the eraser
	 * sizes are to be changed. This method sets the brush/eraser
	 * sizes to the new values depending on user selection.
	 */
	public void setSizeForBrush(float newSize){
		System.out.println("setsizeforbrush");
		float pixelAmount = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, newSize, getResources().getDisplayMetrics());
		brushSize = pixelAmount;
		drawPaint.setStrokeWidth(brushSize);
	}

	/**
	 * This functions sets the previous brush
	 * size upon brush size change.
	 * @param prevSize
	 */
	public void setPrevBrushSize(float prevSize){
		System.out.println("prevsize");
		prevBrushSize = prevSize;
	}

	/**
	 * This function is called when the brush size
	 * is changed by the user. This function returns
	 * previous brush size used prior to brush size
	 * change
	 * @return
	 */
	public float getPrevBrushSize(){
		System.out.println("getprevbrushsize");
		return prevBrushSize;
	}

	/**
	 * This function sets the paint color
	 * to white when eraser functionality is
	 * selected by the user.
	 * @param bErase
	 */
	public void setErase(boolean bErase){
		System.out.println("setErase");
		erase = bErase;
		if(erase)
		{
			//drawPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

			drawPaint.setColor(Color.parseColor("#FFFFFF"));
		}
		else
			drawPaint.setXfermode(null);
	}

	/**
	 * This function is called when the new file
	 * functionality is enabled by the user. This
	 * function reinitializes the CustomViewForDrawing
	 * attributes when a new canvas is produced.
	 */
	public void newStart(){
		System.out.println("newstart");
		drawCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
		//changes 03/10/2013 to set default color to gey when opening new document
		paths.clear();
		undonePaths.clear();
		drawPaint.setColor(0xFF787878);
		invalidate();
	}
}

class CustomPath extends Path{
	private int color;
	private float brushThickness;

	public float getBrushThickness() {
		return brushThickness;
	}

	public void setBrushThickness(float brushThickness) {
		this.brushThickness = brushThickness;
	}

	public CustomPath(int color, float brushThickness) {
		super();
		this.color = color;
		this.brushThickness = brushThickness;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

}