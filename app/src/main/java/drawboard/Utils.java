package drawboard;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple class with utility functions.
 */
public class Utils {

    Socket socket;

   public void set(Socket socket){
       this.socket=socket;
   }


    public class SocketServerReplyThread extends Thread {

        private Socket hostThreadSocket;
        int cnt;

        SocketServerReplyThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;

        }

        @Override
        public void run() {
            OutputStream outputStream;
            int msgReplyx = 123;
            int msgReplyy = 130;
            try {
                outputStream = socket.getOutputStream();
                PrintStream printStream = new PrintStream(outputStream);
                printStream.print("erase");
              //  printStream.print("$$");
             //   printStream.print(msgReplyy);
                printStream.close();

                //message += "replayed: " + msgReply + "\n";

              /*  MyActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //      msg.setText(message);
                    }
                });*/

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //message += "Something wrong! " + e.toString() + "\n";
            }

           /* MyActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    //   msg.setText(message);
                }
            });*/
        }

    }


    /**
     * Function to confirm that the user want's to save the current whiteboard state as a image in
     * phone gallery
     *
     * @param context the current activity context
     * @param baos    the image represented as a Byte Array Output Stream
     */
    public static void saveWhiteboardImage(final Context context, final ByteArrayOutputStream baos) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Confirm canvas save")
                .setMessage("Do you want to save the canvas?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Date date = new Date();
                        Format formatter = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss", Locale.US);
                        String fileName = formatter.format(date) + ".png";
                        if (Environment.getExternalStorageState()
                                .equals(Environment.MEDIA_MOUNTED)) {
                            File sdCard = Environment.getExternalStorageDirectory();
                            File dir = new File(sdCard.getAbsolutePath() + "/drawboard");
                            boolean directoryCreated = dir.mkdirs();

                            File file = new File(dir, fileName);

                            FileOutputStream f;
                            try {
                                f = new FileOutputStream(file);
                                f.write(baos.toByteArray());
                                f.flush();
                                f.close();
                                Toast.makeText(context, "Saved", Toast.LENGTH_SHORT)
                                        .show();
                                // Trigger gallery refresh on photo save
                                MediaScannerConnection.scanFile(
                                        context,
                                        new String[]{file.toString()},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            public void onScanCompleted(String path, Uri uri) {
                                            }
                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(context, "Save Failed!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


}
