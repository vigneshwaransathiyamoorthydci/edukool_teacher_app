package drawboard;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
 public class Importadapter extends ArrayAdapter<String> {

    private ArrayList<String> pathList;
    Context con;
   // CoolReader mactivity;

    public Importadapter(Context context, int textViewResourceId,
                         ArrayList<String> pathList) {
        super(context, textViewResourceId, pathList);
        this.pathList = pathList;
        con=context;

    }

    private class ViewHolder {
ImageView bookiamge;
        TextView  name,pdfpath;
        RelativeLayout parentlayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.import_item, null);

            holder = new ViewHolder();
         
            holder.name = (TextView) convertView.findViewById(R.id.studentnametext);
            holder.pdfpath = (TextView) convertView.findViewById(R.id.bpath);
            holder.bookiamge = (ImageView) convertView.findViewById(R.id.studntimageview);
            holder.parentlayout = (RelativeLayout) convertView.findViewById(R.id.parentlayout);
            convertView.setTag(holder);

           
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String imageName=pathList.get(position);
        imageName=imageName.substring(imageName.lastIndexOf("/") + 1, imageName.length());

        holder.name.setText(imageName);
         holder.pdfpath.setText(pathList.get(position));

        if(pathList.get(position)!=null)
        setbackground(holder.bookiamge,pathList.get(position));
        // holder.pdfpath.setText(bookname.getBookpath());
       holder.parentlayout.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
             TextView pdfpth=(TextView)v.findViewById(R.id.bpath);
             TextView   bookname = (TextView) v.findViewById(R.id.studentnametext);


                ((Whiteboard) con).setImportedImaged(pdfpth.getText().toString());

  
            }
        });
       // holder.code.setText(" (" +  country.getCode() + ")");
    //  holder.name.setText(bookname.getName());
      
     //   holder.name.setTag(bookname);



        return convertView;

    }

    void setbackground(ImageView view, String filepath) {
        File imgFile = new File(filepath);
        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
            view.setImageBitmap(myBitmap);

        }
    }

}