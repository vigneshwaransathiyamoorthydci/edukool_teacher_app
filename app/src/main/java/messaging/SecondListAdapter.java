package messaging;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import Utilities.DatabaseHandler;
import Utilities.Utilss;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class SecondListAdapter extends BaseAdapter {
    DatabaseHandler obj;
    SharedPreferences pref;
    private Context context;
    private ArrayList<StudentItem> items;
    Utilss utils;

    public SecondListAdapter(Context context, ArrayList<StudentItem> items, SharedPreferences per) {
        this.context = context;
        this.items = items;
        pref = per;

    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.layout_item_circular, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        StudentItem currentItem = (StudentItem) getItem(position);

       /* viewHolder.msg.setText(currentItem.getSubject());
        viewHolder.comid.setText(currentItem.getComid());
        viewHolder.content.setText(currentItem.getContent());
        viewHolder.dat.setText(currentItem.getDateof());*/
        Log.i("subject",currentItem.getSubject()+" id : "+currentItem.getSubjectid());
       if (currentItem.getSubject().equals("")){
           viewHolder.textTitle.setText("No Title");
       }
       else  {
           viewHolder.textTitle.setText(currentItem.getSubject());
       }

       viewHolder.textContent.setText(currentItem.getContent());
       viewHolder.textDate.setText(setDate(currentItem.getDateof()));


        // obj = new DatabaseHandler(context);
        obj = new DatabaseHandler(context, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);


        String stdname = "";
        Cursor rcursor = obj.MapRetrive(Integer.parseInt(currentItem.getComid()));


        while (rcursor.moveToNext()) {
            stdname = stdname + rcursor.getInt(rcursor.getColumnIndex("ReceiverID")) + ",";
        }


        viewHolder.stdname.setText(stdname);

        viewHolder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView msg = (TextView) v.findViewById(R.id.msg);
                TextView stdname = (TextView) v.findViewById(R.id.stdnam);
                TextView content = (TextView) v.findViewById(R.id.content);
                TextView dat = (TextView) v.findViewById(R.id.dat);
                TextView cid = (TextView) v.findViewById(R.id.comid);
                showPopup(currentItem.getSubject(), stdname.getText().toString(), currentItem.getContent(), setDate(currentItem.getDateof()), Integer.parseInt(currentItem.getComid()));
            }
        });


        return convertView;
    }

    public void showPopup(String msg, String std, String cont, String dat, int msgid) {
        View popupView = LayoutInflater.from(context).inflate(R.layout.message_popup, null);
//        final PopupWindow popupWindow = new PopupWindow(popupView, 550, 300);
//        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        TextView datpop = (TextView) popupView.findViewById(R.id.dat_pop);
        TextView stdpop = (TextView) popupView.findViewById(R.id.stdnam_pop);
        TextView msgpop = (TextView) popupView.findViewById(R.id.msg_pop);
        TextView contpop = (TextView) popupView.findViewById(R.id.content_pop);
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(popupView);

        String stdname = "", checknam = "";
        String[] separated = std.split(",");
        stdpop.setMovementMethod(new ScrollingMovementMethod());

        DatabaseHandler obj = new DatabaseHandler(context, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);
        Cursor ackcursor = obj.AckStudent(msgid);

        ArrayList<Integer> stdidlist = new ArrayList<>();
        stdidlist.clear();
        while (ackcursor.moveToNext()) {
            stdidlist.add(ackcursor.getInt(ackcursor.getColumnIndex("ReceiverID")));
        }
        ackcursor.close();

        if (separated.length != 0) {
            for (int i = 0; i < separated.length; i++) {
                boolean color = false;

                if (stdidlist.size() != 0) {
                    for (int y = 0; y < stdidlist.size(); y++) {

                        color = false;

                        if (stdidlist.get(y) == Integer.parseInt(separated[i])) {
                            Cursor c = obj.getStudentName(Integer.parseInt(separated[i]));
                            color = true;
                            while (c.moveToNext()) {

                                stdname = stdname + "<font color='green'>" + c.getString(c.getColumnIndex("FirstName")) + "</font>" + ",";

                            }
                            break;
                        } /*else

                        {
                            Cursor c = obj.getStudentName(Integer.parseInt(separated[i]));
                            while (c.moveToNext()) {

                                stdname = stdname + c.getString(c.getColumnIndex("FirstName")) + ",";
                            }
                        }*/

                    }//for
                    if (!color) {
                        Cursor c = obj.getStudentName(Integer.parseInt(separated[i]));
                        while (c.moveToNext()) {

                            stdname = stdname + c.getString(c.getColumnIndex("FirstName")) + ",";
                        }
                    }

                } else {
                    if (separated[i] != "") {
                        Cursor c = obj.getStudentName(Integer.parseInt(separated[i]));
                        while (c.moveToNext()) {
                            stdname = stdname + c.getString(c.getColumnIndex("FirstName")) + ",";
                        }

                    }
                }

            }


        }


        datpop.setText(dat);
        stdpop.setText(Html.fromHtml(stdname));
        msgpop.setText(msg);
        contpop.setText(cont);

        Button btnDismiss = (Button) popupView.findViewById(R.id.close);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

      //  popupWindow.showAsDropDown(popupView, 0, 0);
    }

    public String setDate(String date){
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date past = format.parse(date);
            Date now = new Date();
            long seconds=TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days= TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if(seconds<60)
            {
                return  seconds+" seconds ago";

            }
            else if(minutes<60)
            {
                return  minutes+" minutes ago";
            }
            else if(hours<24)
            {
                return  hours+" hours ago";
            }
            else
            {
                return  days+" days ago";
            }
        }
        catch (Exception j){
            j.printStackTrace();
        }
        return "";
    }

    //ViewHolder inner class
    private class ViewHolder {
        CheckBox check;
        TextView comid;
        TextView msg, stdname, content, dat,textTitle,textDate,textContent;
        RelativeLayout rel;

        public ViewHolder(View view) {
            check = (CheckBox) view.findViewById(R.id.check);
            comid = (TextView) view.findViewById(R.id.comid);
            msg = (TextView) view.findViewById(R.id.msg);
            stdname = (TextView) view.findViewById(R.id.stdnam);
            content = (TextView) view.findViewById(R.id.content);
            dat = (TextView) view.findViewById(R.id.dat);
            rel = (RelativeLayout) view.findViewById(R.id.relativeParent);
            textTitle = (TextView) view.findViewById(R.id.textTitle);
            textDate = (TextView) view.findViewById(R.id.textDate);
            textContent =  (TextView) view.findViewById(R.id.textContent);

        }
    }

}