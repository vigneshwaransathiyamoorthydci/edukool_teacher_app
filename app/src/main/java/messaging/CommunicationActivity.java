package messaging;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import Utilities.Attendancedb;
import Utilities.DatabaseHandler;
import Utilities.Service;
import Utilities.Utilss;
import connection.Communication;
import connection.Examzipfile;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Attendancepojo;
import helper.RoundedImageView;
import helper.Staffloingdetails;
import helper.Studentdetails;
import helper.Studentinfo;

/**
 * Created by kirubakaranj on 5/16/2017.
 */

public class CommunicationActivity extends BaseActivity {
    public static boolean presentbool = false;
    public static boolean absentbool = false;
    PopupWindow pwindo;
    DatabaseHandler obj;
    ListView itemsListView;
    TextView staffclass, classname, dat;
    RoundedImageView profileimage;
    CheckBox today, present, absent, all;
    Staffloingdetails logindetails;
    Button msg;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    Map<Integer, Studentdetails> map = new HashMap<Integer, Studentdetails>();
    Map<Integer, Studentdetails> map2 = new HashMap<Integer, Studentdetails>();
    String staffid;
    ArrayList<StudentItem> stdlist;
    CustomListAdapter adapter;
    SecondListAdapter secondadapter;
    ListView secondlistview;
    Attendancedb db;
    ImageView back;
    ArrayList<Studentdetails> detailses;
    ArrayList<Studentdetails> detailsesbelongrooms;
    ArrayList<Studentdetails> dummy;
    ImageView handraise;
    Examzipfile zipfile;
    ImageView dnd;
    TextView cir;
    Utilss utils;
    int len = 0, seclen = 0;
    TextView title,send;
    EditText et_title,message;


    public static void setDetails(ArrayList<Studentdetails> items) {


    }

    public static String currentdate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String retrivedate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String currentdate1() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String headerdate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.communication);
        itemsListView = (ListView) findViewById(R.id.list_view_items);
        msg = (Button) findViewById(R.id.msg);
        staffclass = (TextView) findViewById(R.id.staffclas);
        classname = (TextView) findViewById(R.id.class_name);
        title=(TextView)findViewById(R.id.title);
        title.setText("Circular");
        dat = (TextView) findViewById(R.id.dat);
        back = (ImageView) findViewById(R.id.back);
        handraise = (ImageView) findViewById(R.id.handrise);
        dnd = (ImageView) findViewById(R.id.dnd);
        cir = (TextView) findViewById(R.id.cir);
        utils = new Utilss(this);
        utils.setTextviewtypeface(1, cir);
        et_title= (EditText) findViewById(R.id.et_title);
        message= (EditText) findViewById(R.id.message);
        send= (TextView) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sub=et_title.getText().toString();
                String mesg=message.getText().toString();
                if(!absent.isChecked()&&!present.isChecked()&&!all.isChecked()){
                    Toast.makeText(CommunicationActivity.this, "Please select attendance type.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mesg.equals("")){
                    Toast.makeText(CommunicationActivity.this, "Please enter your message..", Toast.LENGTH_SHORT).show();
                    return;
                }
                stdlist.clear();


                ContentValues communiValues = new ContentValues();
                communiValues.put("SenderID", Integer.parseInt(staffid));
                // communiValues.put("Receiver",entry.getValue().getStudentID());
                communiValues.put("DateOfCommunication", currentdate());/*currentdate()*/
                communiValues.put("Title", sub);
                communiValues.put("Content", mesg);
                communiValues.put("SubjectID", Integer.parseInt(pref.getString("subjectid", "0")));

                obj.TableCommunication(communiValues);

                Cursor ltcur = obj.retriveCommunication();
                ltcur.moveToLast();
                int msgid = ltcur.getInt(ltcur.getColumnIndex("CommunicationID"));
                ltcur.close();


                for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {
                    ContentValues Msgvalue = new ContentValues();
                    Msgvalue.put("MsgID", msgid);
                    Log.e("Receiverid", entry.getValue().getStudentID() + "");
                    Msgvalue.put("ReceiverID", entry.getValue().getStudentID());
                    Msgvalue.put("IsAck", 0);
                    Msgvalue.put("IsSent", 0);
                    Log.d("rec", "" + entry.getValue().getStudentID());
                    obj.TableCommunicationMap(Msgvalue);
                }

                for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
                    ContentValues Msgvalue = new ContentValues();
                    Msgvalue.put("MsgID", msgid);
                    Log.e("Receiverid", entry.getValue().getStudentID() + "");
                    Msgvalue.put("ReceiverID", entry.getValue().getStudentID());
                    Msgvalue.put("IsAck", 0);
                    Msgvalue.put("IsSent", 0);
                    Log.d("rec", "" + entry.getValue().getStudentID());
                    obj.TableCommunicationMap(Msgvalue);
                }
                Cursor mcc = obj.MapRetriveall();
                while (mcc.moveToNext()) {
                    //   Toast.makeText(getApplicationContext(),"msgid "+mcc.getInt(mcc.getColumnIndex("MsgID"))+",recid:"+mcc.getInt(mcc.getColumnIndex("ReceiverID")),Toast.LENGTH_LONG).show();
                }
                mcc.close();

                Cursor tc = obj.retriveCommunication();
                tc.moveToLast();
                StudentItem it = new StudentItem();
                it.setComid("" + tc.getInt(tc.getColumnIndex("CommunicationID")));
                //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                it.setDateof("" + tc.getString(tc.getColumnIndex("DateOfCommunication")));
                it.setSubject(tc.getString(tc.getColumnIndex("Title")));
                it.setContent(tc.getString(tc.getColumnIndex("Content")));
                it.setSubjectid(tc.getInt(tc.getColumnIndex("SubjectID")));
                stdlist.add(it);

                while (tc.moveToPrevious()) {
                    StudentItem item = new StudentItem();
                    item.setComid("" + tc.getInt(tc.getColumnIndex("CommunicationID")));
                    //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                    item.setDateof("" + tc.getString(tc.getColumnIndex("DateOfCommunication")));
                    item.setSubject(tc.getString(tc.getColumnIndex("Title")));
                    item.setContent(tc.getString(tc.getColumnIndex("Content")));
                    it.setSubjectid(tc.getInt(tc.getColumnIndex("SubjectID")));

                    stdlist.add(item);


                    //    Toast.makeText(getApplicationContext(),"comid "+tc.getInt(tc.getColumnIndex("CommunicationID")),Toast.LENGTH_LONG).show();

                }
                tc.close();
                if (map.size() > 0) {
                    ArrayList<Studentinfo> online = new ArrayList<Studentinfo>();
                    for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {

                        Studentdetails details = entry.getValue();
                        for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
                            clientThread thread = MultiThreadChatServerSync.thread.get(i);
                            Studentinfo info = thread.info;
                               /* if (info.getStudentrollnumber().equalsIgnoreCase(details.getRollNo())) {
                                    online.add(info);
                                }*/
                            if (info.getStudentid().equalsIgnoreCase("" + details.getStudentID())) {
                                online.add(info);
                            }
                            //info.setManual("0");
                            //online.add(thread.info);

                            //info.add(MultiThreadChatServerSync.thread.get(i).);
                        }
                    }
                    for (int in = 0; in < online.size(); in++) {
                        try {
                            JSONObject send = new JSONObject();
                            send.put("MsgID", "" + msgid);
                            send.put("ReceiverID", online.get(in).getStudentid());
                            send.put("SenderID", staffid);
                            send.put("DateOfCommunication", currentdate());
                            send.put("Title", sub);
                            send.put("Content", mesg);
                            send.put("SubjectID", pref.getString("subjectid", "0"));
                            send.put("Action", "Circular");
                            new Communication(online.get(in), "Circular@" + send.toString()).start();
                        } catch (Exception e) {

                        }


                    }
                }

//                if (!parent.isChecked()) {
//
//                    if (map2.size() > 0) {
//
//                        for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
//                            Studentdetails details = entry.getValue();
//                            try {
//                                SmsManager smsManager = SmsManager.getDefault();
//                                smsManager.sendTextMessage(details.getPhone_1(), null, subject.getText().toString() + ":" + content.getText().toString(), null, null);
//                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
//                                intent.putExtra("sms_body", content.getText().toString());
//                                startActivity(intent);*/
//                            } catch (Exception e) {
//
//                            }
//
//                        }
//                    }
//                } else {
                    if (map2.size() > 0) {

                        for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
                            Studentdetails details = entry.getValue();
                            try {
                                SmsManager smsManager = SmsManager.getDefault();
                                smsManager.sendTextMessage(details.getPhone_1(), null, sub + ":" +mesg, null, null);
                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
                                intent.putExtra("sms_body", content.getText().toString());
                                startActivity(intent);*/
                            } catch (Exception e) {

                            }

                        }
                    }

                    if (map.size() > 0) {

                        for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {
                            Studentdetails details = entry.getValue();
                            try {
                                SmsManager smsManager = SmsManager.getDefault();
                                smsManager.sendTextMessage(details.getPhone_1(), null, sub + ":" + mesg, null, null);
                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
                                intent.putExtra("sms_body", content.getText().toString());
                                startActivity(intent);*/
                            } catch (Exception e) {

                            }

                        }
                    }



                secondadapter = new SecondListAdapter(CommunicationActivity.this, stdlist, pref);
                // Toast.makeText(getApplicationContext(),"con "+stdlist.size(),Toast.LENGTH_LONG).show();
                //set custom adapter as adapter to our list view
                secondlistview.setAdapter(secondadapter);


           //     pwindo.dismiss();
            }
        });

        presentbool = false;
        absentbool = false;
        detailsesbelongrooms = new ArrayList<>();

        today = (CheckBox) findViewById(R.id.checkBox4);
        present = (CheckBox) findViewById(R.id.present);
        absent = (CheckBox) findViewById(R.id.absent);//        map.clear();
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        all = (CheckBox) findViewById(R.id.all);


        map.clear();
        map2.clear();
        // obj=new DatabaseHandler(this);
        //  db=new Attendancedb(this);


        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();
        db = new Attendancedb(this, pref.getString("attendancedb", ""), Attendancedb.DATABASE_VERSION);

        obj = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        logindetails = obj.staffdetails();
        setbackground(profileimage, logindetails.getPhotoFilename());
        Attendancepojo pojo = db.alereadytakeattendancelatestrecord(Integer.parseInt(pref.getString("batchid", "0")),
                Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")),
                Service.getdateforattendance(),
                Service.attendenacedate(), Integer.parseInt(pref.getString("bookbin", "0")));

        staffclass.setText(pref.getString("classname", ""));
        classname.setText( pref.getString("classname", ""));
        dat.setText("DATE : " + headerdate());
        utils.setTextviewtypeface(1,cir);
        utils.setTextviewtypeface(5,staffclass);
        utils.setTextviewtypeface(5,classname);
        utils.setTextviewtypeface(5,dat);
        utils.setTextviewtypeface(1,title);
        staffid = pref.getString("staffid", "0");
        dnd = (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("dnd", false)) {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata = senddata + "@false";

                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    dnd.setImageResource(R.drawable.inactive);


                } else {
                    dnd.setImageResource(R.drawable.active);

                    String senddata = "DND";

                    senddata = senddata + "@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
            }
        });


        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(CommunicationActivity.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(CommunicationActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });


        detailses = obj.getdetails(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("staffid", "0")));


        for (int j = 0; j < detailses.size(); j++) {
            String rooms[] = detailses.get(j).getRoominfo().split(",");
            boolean addtostudent = false;
           /* for(int r=0; r<rooms.length; r++)
            {
                if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                {
                    addtostudent=true;
                    break;
                }
            }
            if(addtostudent)*/
            detailsesbelongrooms.add(detailses.get(j));
        }


        try {
            for (int i = 0; i < detailsesbelongrooms.size(); i++) {


                Attendancepojo pojo1 = db.studentpresentornot(Integer.parseInt(pref.getString("batchid", "0")),
                        Integer.parseInt(pref.getString("subjectid", "0")),
                        Integer.parseInt(pref.getString("staffid", "0")),
                        pojo.getAttendanceDateTime(),
                        detailsesbelongrooms.get(i).getStudentID());


                try {
                    if (pojo1 != null) {
                        if (pojo1.getAttendance().equalsIgnoreCase("P")) {
                            detailsesbelongrooms.get(i).setPresentornot("P");

                        } else {
                            detailsesbelongrooms.get(i).setPresentornot("A");

                        }
                        detailsesbelongrooms.get(i).setManualAttendance(pojo1.getManualAttendance());

                    }
                } catch (Exception e) {
                    detailsesbelongrooms.get(i).setPresentornot("A");
                    detailsesbelongrooms.get(i).setManualAttendance(1);
                }


            }
        } catch (Exception e) {

        }


        dummy = detailses;

        //create adapter object
        adapter = new CustomListAdapter(this, detailsesbelongrooms, "0", "0");

        //set custom adapter as adapter to our list view
        itemsListView.setAdapter(adapter);
//        itemsListView.setItemsCanFocus(true);


        //second listview
        secondlistview = (ListView) findViewById(R.id.listsecond);

        stdlist = new ArrayList<StudentItem>();


        setSecondlistview();
        //create adapter object

        secondlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                // TODO Auto-generated method stub


            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommunicationActivity.this.finish();

            }
        });


        msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (map.size() != 0 || map2.size() != 0) {
                    initiatePopupWindow();
                } else {
                    Toast.makeText(getApplicationContext(), "Please select atleast one student", Toast.LENGTH_LONG).show();
                }
                //  allvalue();

            }
        });


        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (all.isChecked()) {
                    all.setBackground(getResources().getDrawable(R.drawable.btn_bg));
                    all.setTextColor(getResources().getColor(R.color.white));
                    present.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    present.setTextColor(getResources().getColor(R.color.oldbg));
                    absent.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    absent.setTextColor(getResources().getColor(R.color.oldbg));
                    if (present.isChecked()) {
                        present.performClick();
                        // present.setEnabled(false);
                    }
                    if (absent.isChecked()) {
                        absent.performClick();
                        //absent.setEnabled(false);
                    }
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        {
                            if (detailsesbelongrooms.get(i).getPresentornot().equalsIgnoreCase("P")) {
                                if (detailsesbelongrooms.get(i).getManualAttendance() == 1) {
                                    Absentmap(detailsesbelongrooms.get(i), i);
                                    detailsesbelongrooms.get(i).setChecked(true);
                                } else if (detailsesbelongrooms.get(i).getManualAttendance() == 0) {
                                    setMap(detailsesbelongrooms.get(i), i);
                                    detailsesbelongrooms.get(i).setChecked(true);
                                }
                            } else {
                                Absentmap(detailsesbelongrooms.get(i), i);
                                detailsesbelongrooms.get(i).setChecked(true);
                            }
                        }
                    }

                } else {

                    // present.setEnabled(true);
                    // absent.setEnabled(true);
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        {
                            setMap1(detailsesbelongrooms.get(i), i);
                            Absentmap1(detailsesbelongrooms.get(i), i);

                            detailsesbelongrooms.get(i).setChecked(false);
                        }
                    }
                }
                adapter.notifyDataSetChanged();

            }
        });

        present.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (present.isChecked()) {
                    present.setBackgroundColor((getResources().getColor(R.color.oldbg)));
                    present.setTextColor(getResources().getColor(R.color.white));
                    all.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    all.setTextColor(getResources().getColor(R.color.oldbg));
                    absent.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    absent.setTextColor(getResources().getColor(R.color.oldbg));

                    if (all.isChecked()) {
                        all.performClick();
                        // present.setEnabled(false);
                    }
                    if (absent.isChecked()) {
                        absent.performClick();
                        //absent.setEnabled(false);
                    }

                    presentbool = true;
                    int presentcount = 0;
               /* dummy.clear();
                dummy.addAll(detailses);*/
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        if (detailsesbelongrooms.get(i).getPresentornot().equalsIgnoreCase("P")) {
                            presentcount = 1;

                            if (detailsesbelongrooms.get(i).getManualAttendance() == 1) {
                                Absentmap(detailsesbelongrooms.get(i), i);
                                detailsesbelongrooms.get(i).setChecked(true);
                            } else if (detailsesbelongrooms.get(i).getManualAttendance() == 0) {
                                setMap(detailsesbelongrooms.get(i), i);
                                detailsesbelongrooms.get(i).setChecked(true);
                            }
                        } else if (!absentbool) {
                            detailsesbelongrooms.get(i).setChecked(false);

                            setMap1(detailsesbelongrooms.get(i), i);

                        } else {
                            detailsesbelongrooms.get(i).setChecked(true);

                        }


                    }
                    if (presentcount == 0)
                        stratExamCompletedPopup();
                    adapter.notifyDataSetChanged();


                } else {

                    presentbool = false;
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        if (detailsesbelongrooms.get(i).getPresentornot().equalsIgnoreCase("P")) {
                            setMap1(detailsesbelongrooms.get(i), i);

                            Absentmap1(detailsesbelongrooms.get(i), i);
                            detailsesbelongrooms.get(i).setChecked(false);

                        } else if (!absentbool) {
                            detailsesbelongrooms.get(i).setChecked(false);
                            Absentmap1(detailsesbelongrooms.get(i), i);

                            setMap1(detailsesbelongrooms.get(i), i);

                        } else {
                            detailsesbelongrooms.get(i).setChecked(true);

                        }

                    }
                    adapter.notifyDataSetChanged();

                    // dummy.clear();
                    // dummy.addAll(detailses);
                    /// adapter.notifyDataSetChanged();
                    //adapter.notifyDataSetChanged();
                    //  adapter = new CustomListAdapter(CommunicationActivity.this, detailses,"0","0");

                    //set custom adapter as adapter to our list view
                    //itemsListView.setAdapter(adapter);
                }


            }

        });

        absent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (absent.isChecked()) {
                    absent.setBackground(getResources().getDrawable(R.drawable.btn_bg));
                    absent.setTextColor(getResources().getColor(R.color.white));
                    all.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    all.setTextColor(getResources().getColor(R.color.oldbg));
                    present.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
                    present.setTextColor(getResources().getColor(R.color.oldbg));

                    if (all.isChecked()) {
                        all.performClick();
                        // present.setEnabled(false);
                    }
                    if (present.isChecked()) {
                        present.performClick();
                        //absent.setEnabled(false);
                    }
                    absentbool = true;
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        if (detailsesbelongrooms.get(i).getPresentornot().equalsIgnoreCase("A")) {
                            Absentmap(detailsesbelongrooms.get(i), i);
                            detailsesbelongrooms.get(i).setChecked(true);

                        } else if (!presentbool) {
                            detailsesbelongrooms.get(i).setChecked(false);
                            Absentmap1(detailsesbelongrooms.get(i), i);


                            setMap1(detailsesbelongrooms.get(i), i);

                        } else {
                            detailsesbelongrooms.get(i).setChecked(true);

                        }

                    }
                    adapter.notifyDataSetChanged();

                } else {
                    absentbool = false;
                    for (int i = 0; i < detailsesbelongrooms.size(); i++) {
                        if (detailsesbelongrooms.get(i).getPresentornot().equalsIgnoreCase("A")) {
                            detailsesbelongrooms.get(i).setChecked(false);
                            Absentmap1(detailsesbelongrooms.get(i), i);

                            setMap1(detailsesbelongrooms.get(i), i);
                        } else if (!presentbool) {
                            detailsesbelongrooms.get(i).setChecked(false);
                            Absentmap(detailsesbelongrooms.get(i), i);

                            setMap1(detailsesbelongrooms.get(i), i);

                        } else {
                            detailsesbelongrooms.get(i).setChecked(true);

                        }

                    }
                    adapter.notifyDataSetChanged();
                }
            }

        });


        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (today.isChecked()) {
                    stdlist.clear();
                    Cursor todaycur = obj.todayCommunication(retrivedate(), currentdate());
                    //   Toast.makeText(getApplicationContext(),"cursor"+todaycur.getCount(),Toast.LENGTH_LONG).show();
                    if (todaycur.getCount() > 0) {
                        todaycur.moveToLast();
                        StudentItem it = new StudentItem();
                        it.setComid("" + todaycur.getInt(todaycur.getColumnIndex("CommunicationID")));
                        it.setDateof("" + todaycur.getString(todaycur.getColumnIndex("DateOfCommunication")));
                        it.setSubject(todaycur.getString(todaycur.getColumnIndex("Title")));
                        it.setContent(todaycur.getString(todaycur.getColumnIndex("Content")));
                        stdlist.add(it);

                        while (todaycur.moveToPrevious()) {
                            StudentItem item1 = new StudentItem();
                            item1.setComid("" + todaycur.getInt(todaycur.getColumnIndex("CommunicationID")));
                            //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                            item1.setDateof("" + todaycur.getString(todaycur.getColumnIndex("DateOfCommunication")));
                            item1.setSubject(todaycur.getString(todaycur.getColumnIndex("Title")));
                            item1.setContent(todaycur.getString(todaycur.getColumnIndex("Content")));
                            stdlist.add(item1);
                        }
                        todaycur.close();
                        secondadapter = new SecondListAdapter(CommunicationActivity.this, stdlist, pref);
                        //   Toast.makeText(getApplicationContext(),"con "+stdlist.size(),Toast.LENGTH_LONG).show();
                        //set custom adapter as adapter to our list view
                        secondlistview.setAdapter(secondadapter);

                    } else {
                        //  Toast.makeText(getApplicationContext(),"Nocursor",Toast.LENGTH_LONG).show();
                    }
                } else {
                    setSecondlistview();
                }

            }
        });


    }

    @Override
    public int getlayout() {
        return R.layout.communication;
    }

    public void setSecondlistview() {
        Cursor comcursor = obj.retriveCommunication();
        stdlist.clear();
        if (comcursor.getCount() > 0) {
            comcursor.moveToLast();
            StudentItem it = new StudentItem();
            it.setComid("" + comcursor.getInt(comcursor.getColumnIndex("CommunicationID")));
            //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
            it.setDateof("" + comcursor.getString(comcursor.getColumnIndex("DateOfCommunication")));
            it.setSubject(comcursor.getString(comcursor.getColumnIndex("Title")));
            it.setContent(comcursor.getString(comcursor.getColumnIndex("Content")));
            stdlist.add(it);

            while (comcursor.moveToPrevious()) {
                StudentItem item1 = new StudentItem();
                item1.setComid("" + comcursor.getInt(comcursor.getColumnIndex("CommunicationID")));
                //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                item1.setDateof("" + comcursor.getString(comcursor.getColumnIndex("DateOfCommunication")));
                item1.setSubject(comcursor.getString(comcursor.getColumnIndex("Title")));
                item1.setContent(comcursor.getString(comcursor.getColumnIndex("Content")));
                stdlist.add(item1);
            }
            comcursor.close();
            secondadapter = new SecondListAdapter(CommunicationActivity.this, stdlist, pref);
            ;
            // secondadapter = new SecondListAdapter(CommunicationActivity.this, stdlist,pref);

            //set custom adapter as adapter to our list view
            secondlistview.setAdapter(secondadapter);


        }

    }

    public void setMap(Studentdetails d, int position) {
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map.remove(position);
        this.map.put(position, d);
        String id = "";

        for (Map.Entry<Integer, Studentdetails> entry : this.map.entrySet()) {
            //  Toast.makeText(getApplicationContext(),"value"+","+entry.getValue().getStudentID(),Toast.LENGTH_LONG).show();
        }

    }

    public void Absentmap(Studentdetails d, int position) {
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map2.remove(position);
        this.map2.put(position, d);
        String id = "";
       /* for(Map.Entry<Integer, Studentdetails> entry: this.map2.entrySet()) {
         Toast.makeText(getApplicationContext(),"map2"+","+entry.getValue().getStudentID(),Toast.LENGTH_LONG).show();
        }
*/
    }

    public void Absentmap1(Studentdetails d, int position) {
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map2.remove(position);
        ///this.map2.put(position,d);


    }

    public void setMap1(Studentdetails d, int position) {
        // Toast.makeText(getApplicationContext(),"hai"+i,Toast.LENGTH_LONG).show();

        //this.map.remove(
        this.map.remove(position);


    }

    void setbackground(RoundedImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {

        }
    }

    private void initiatePopupWindow() {


        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) CommunicationActivity.this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.circular_popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            pwindo = new PopupWindow(layout, 680, 330, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            final EditText subject = (EditText) layout.findViewById(R.id.subject);
            final EditText content = (EditText) layout.findViewById(R.id.content);
            final CheckBox parent = (CheckBox) layout.findViewById(R.id.parent);


            Button btnconnect = (Button) layout.findViewById(R.id.done);
            btnconnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    stdlist.clear();


                    ContentValues communiValues = new ContentValues();
                    communiValues.put("SenderID", Integer.parseInt(staffid));
                    // communiValues.put("Receiver",entry.getValue().getStudentID());
                    communiValues.put("DateOfCommunication", currentdate());/*currentdate()*/
                    communiValues.put("Title", subject.getText().toString());
                    communiValues.put("Content", content.getText().toString());
                    communiValues.put("SubjectID", Integer.parseInt(pref.getString("subjectid", "0")));

                    obj.TableCommunication(communiValues);

                    Cursor ltcur = obj.retriveCommunication();
                    ltcur.moveToLast();
                    int msgid = ltcur.getInt(ltcur.getColumnIndex("CommunicationID"));
                    ltcur.close();


                    for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {
                        ContentValues Msgvalue = new ContentValues();
                        Msgvalue.put("MsgID", msgid);
                        Log.e("Receiverid", entry.getValue().getStudentID() + "");
                        Msgvalue.put("ReceiverID", entry.getValue().getStudentID());
                        Msgvalue.put("IsAck", 0);
                        Msgvalue.put("IsSent", 0);
                        Log.d("rec", "" + entry.getValue().getStudentID());
                        obj.TableCommunicationMap(Msgvalue);
                    }

                    for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
                        ContentValues Msgvalue = new ContentValues();
                        Msgvalue.put("MsgID", msgid);
                        Log.e("Receiverid", entry.getValue().getStudentID() + "");
                        Msgvalue.put("ReceiverID", entry.getValue().getStudentID());
                        Msgvalue.put("IsAck", 0);
                        Msgvalue.put("IsSent", 0);
                        Log.d("rec", "" + entry.getValue().getStudentID());
                        obj.TableCommunicationMap(Msgvalue);
                    }
                    Cursor mcc = obj.MapRetriveall();
                    while (mcc.moveToNext()) {
                        //   Toast.makeText(getApplicationContext(),"msgid "+mcc.getInt(mcc.getColumnIndex("MsgID"))+",recid:"+mcc.getInt(mcc.getColumnIndex("ReceiverID")),Toast.LENGTH_LONG).show();
                    }
                    mcc.close();

                    Cursor tc = obj.retriveCommunication();
                    tc.moveToLast();
                    StudentItem it = new StudentItem();
                    it.setComid("" + tc.getInt(tc.getColumnIndex("CommunicationID")));
                    //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                    it.setDateof("" + tc.getString(tc.getColumnIndex("DateOfCommunication")));
                    it.setSubject(tc.getString(tc.getColumnIndex("Title")));
                    it.setContent(tc.getString(tc.getColumnIndex("Content")));
                    it.setSubjectid(tc.getInt(tc.getColumnIndex("SubjectID")));
                    stdlist.add(it);

                    while (tc.moveToPrevious()) {
                        StudentItem item = new StudentItem();
                        item.setComid("" + tc.getInt(tc.getColumnIndex("CommunicationID")));
                        //  item.setReceiver(""+tc.getInt(tc.getColumnIndex("Receiver")));
                        item.setDateof("" + tc.getString(tc.getColumnIndex("DateOfCommunication")));
                        item.setSubject(tc.getString(tc.getColumnIndex("Title")));
                        item.setContent(tc.getString(tc.getColumnIndex("Content")));
                        it.setSubjectid(tc.getInt(tc.getColumnIndex("SubjectID")));

                        stdlist.add(item);


                        //    Toast.makeText(getApplicationContext(),"comid "+tc.getInt(tc.getColumnIndex("CommunicationID")),Toast.LENGTH_LONG).show();

                    }
                    tc.close();
                    if (map.size() > 0) {
                        ArrayList<Studentinfo> online = new ArrayList<Studentinfo>();
                        for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {

                            Studentdetails details = entry.getValue();
                            for (int i = 0; i < MultiThreadChatServerSync.thread.size(); i++) {
                                clientThread thread = MultiThreadChatServerSync.thread.get(i);
                                Studentinfo info = thread.info;
                               /* if (info.getStudentrollnumber().equalsIgnoreCase(details.getRollNo())) {
                                    online.add(info);
                                }*/
                                if (info.getStudentid().equalsIgnoreCase("" + details.getStudentID())) {
                                    online.add(info);
                                }
                                //info.setManual("0");
                                //online.add(thread.info);

                                //info.add(MultiThreadChatServerSync.thread.get(i).);
                            }
                        }
                        for (int in = 0; in < online.size(); in++) {
                            try {
                                JSONObject send = new JSONObject();
                                send.put("MsgID", "" + msgid);
                                send.put("ReceiverID", online.get(in).getStudentid());
                                send.put("SenderID", staffid);
                                send.put("DateOfCommunication", currentdate());
                                send.put("Title", subject.getText().toString());
                                send.put("Content", content.getText().toString());
                                send.put("SubjectID", pref.getString("subjectid", "0"));
                                send.put("Action", "Circular");
                                new Communication(online.get(in), "Circular@" + send.toString()).start();
                            } catch (Exception e) {

                            }


                        }
                    }

                    if (!parent.isChecked()) {

                        if (map2.size() > 0) {

                            for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
                                Studentdetails details = entry.getValue();
                                try {
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage(details.getPhone_1(), null, subject.getText().toString() + ":" + content.getText().toString(), null, null);
                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
                                intent.putExtra("sms_body", content.getText().toString());
                                startActivity(intent);*/
                                } catch (Exception e) {

                                }

                            }
                        }
                    } else {
                        if (map2.size() > 0) {

                            for (Map.Entry<Integer, Studentdetails> entry : map2.entrySet()) {
                                Studentdetails details = entry.getValue();
                                try {
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage(details.getPhone_1(), null, subject.getText().toString() + ":" + content.getText().toString(), null, null);
                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
                                intent.putExtra("sms_body", content.getText().toString());
                                startActivity(intent);*/
                                } catch (Exception e) {

                                }

                            }
                        }

                        if (map.size() > 0) {

                            for (Map.Entry<Integer, Studentdetails> entry : map.entrySet()) {
                                Studentdetails details = entry.getValue();
                                try {
                                    SmsManager smsManager = SmsManager.getDefault();
                                    smsManager.sendTextMessage(details.getPhone_1(), null, subject.getText().toString() + ":" + content.getText().toString(), null, null);
                               /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(subject.getText().toString() + details.getPhone_1()));
                                intent.putExtra("sms_body", content.getText().toString());
                                startActivity(intent);*/
                                } catch (Exception e) {

                                }

                            }
                        }

                    }

                    secondadapter = new SecondListAdapter(CommunicationActivity.this, stdlist, pref);
                    // Toast.makeText(getApplicationContext(),"con "+stdlist.size(),Toast.LENGTH_LONG).show();
                    //set custom adapter as adapter to our list view
                    secondlistview.setAdapter(secondadapter);


                    pwindo.dismiss();
                }
            });

            Button btncancel = (Button) layout.findViewById(R.id.close);
            btncancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void stratExamCompletedPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.commlay);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        TextView attendance = (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);

        revisit.setText("OK");
        valuate.setVisibility(View.VISIBLE);
        revisit.setVisibility(View.GONE);
        valuate.setText("Ok");
        title.setText("Attendance");
        attendance.setText("Perhaps you have not yet captured attendance. Please switch to attendance tab to capture the attendance.");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  showpopup();

                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (pref.getBoolean("dnd", false)) {
            dnd.setImageResource(R.drawable.active);

        } else {
            dnd.setImageResource(R.drawable.inactive);

        }
        if (LoginActivity.handraise.size() > 0) {
            handraise.setImageResource(R.drawable.handraiseenable);

            // Utils.Listpopup(BookBinActivity.this);
        } else {
            handraise.setImageResource(R.drawable.handrise);

            //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
        }
    }
}
