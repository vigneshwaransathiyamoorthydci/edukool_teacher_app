package messaging;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Utilities.Utilss;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class CustomListAdapter extends BaseAdapter {
    private Context context;
    private  ArrayList<helper.Studentdetails> items;
    boolean[] checkBoxState;
    Map<Integer, helper.Studentdetails> map    = new HashMap<Integer, helper.Studentdetails>();;
    int i=0;
    String present="0",absent="0";
    Utilss utils;
    public CustomListAdapter(Context context,  ArrayList<helper.Studentdetails> items,String present,String absent) {
        this.context = context;
        this.items = items;
        checkBoxState=new boolean[items.size()];
        this.present=present;
        this.absent=absent;
        utils=new Utilss((Activity) context);
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_list_view_row_items, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
           /* if(i%2==0){
                viewHolder.rel.setBackgroundColor(Color.parseColor("#553276"));
                 i++;
            }
            else if(i%2==1){

                viewHolder.rel.setBackgroundColor(Color.parseColor("#73499B"));
                i++;*/
       // }
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.check.setOnCheckedChangeListener(null);
        }


        final helper.Studentdetails currentItem = (helper.Studentdetails) getItem(position);
       // viewHolder.rel.setTag(position);
        viewHolder.check.setTag(position);
        viewHolder.stdname.setText(currentItem.getFirstName().toUpperCase());

        viewHolder.rollno.setText(currentItem.getRollNo());

        viewHolder.studentid.setText(""+currentItem.getStudentID());
        utils.setTextviewtypeface(5,viewHolder.stdname);
        utils.setTextviewtypeface(5,viewHolder.rollno);
        if(currentItem.isChecked())
        {
            viewHolder.check.setChecked(true);
        }
        else
        {
            viewHolder.check.setChecked(true);

        }

        ///viewHolder.check.setChecked(checkBoxState[position]);

       /* if(CommunicationActivity.presentbool&&CommunicationActivity.absentbool)
        {
            viewHolder.check.setChecked(true);

            ///viewHolder.check.performClick();
           map.remove(position);
            map.put(position,currentItem);
            if(context instanceof CommunicationActivity){
                ((CommunicationActivity) context).setMap(currentItem,position);
            }



        }
        else if(CommunicationActivity.presentbool)
        {
            if(currentItem.getPresentornot().equalsIgnoreCase("P")) {
                viewHolder.check.setChecked(true);
              //  viewHolder.check.performClick();

               //  map.remove(position);
               //map.put(position,currentItem);
                if(context instanceof CommunicationActivity){
                    ((CommunicationActivity) context).setMap(currentItem,position);
                }
            }
            else
            {
                ((CommunicationActivity) context).setMap1(currentItem,position);

            }
        }
        else if(CommunicationActivity.absentbool)
        {
            if(currentItem.getPresentornot().equalsIgnoreCase("A")) {
                viewHolder.check.setChecked(true);
              //  viewHolder.check.performClick();

                 map.remove(position);
               map.put(position,currentItem);
                if(context instanceof CommunicationActivity){
                    ((CommunicationActivity) context).setMap(currentItem,position);
                }
            }
            else
            {
                ((CommunicationActivity) context).setMap1(currentItem,position);

            }
        }
        else
        {
            viewHolder.check.setChecked(false);
           /// viewHolder.check.performClick();
            map.remove(position);
            if(context instanceof CommunicationActivity){
                ((CommunicationActivity) context).setMap1(currentItem,position);
            }

            //

        }*/


       /* if(present.equals("1")){
        if(currentItem.getPresentornot().equalsIgnoreCase("P")) {
            viewHolder.check.setChecked(true);
        }}
        if(absent.equals("1")){
            if(currentItem.getPresentornot().equalsIgnoreCase("A")) {
                viewHolder.check.setChecked(true);
            }
        }*/

       viewHolder.check.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                if(((CheckBox)v).isChecked())
                    checkBoxState[position]=true;
                else
                    checkBoxState[position]=false;
                int posit=(int)v.getTag();
            //    Toast.makeText(context,"position"+posit,Toast.LENGTH_LONG).show();

                helper.Studentdetails selecteditem = (helper.Studentdetails) getItem(posit);


                if(((CheckBox)v).isChecked()){
                    items.get(posit).setChecked(true);
                  map.put(posit,selecteditem);
                    checkBoxState[position]=true;
                    if(selecteditem.getPresentornot().equalsIgnoreCase("P")) {
                        if (selecteditem.getManualAttendance() == 1) {
                            if(context instanceof CommunicationActivity) {
                                ((CommunicationActivity) context).Absentmap(selecteditem, position);
                            }
                            else
                            {
                                ((Communicationforrooms) context).Absentmap(selecteditem, position);

                            }

                        } else {
                            if(context instanceof CommunicationActivity) {
                                ((CommunicationActivity) context).setMap(selecteditem, position);
                            }else
                            {
                                ((Communicationforrooms) context).setMap(selecteditem, position);

                            }
                        }
                    }
                    else
                    {
                        if(context instanceof CommunicationActivity) {
                            ((CommunicationActivity) context).Absentmap(selecteditem, position);
                        }
                        else
                        {
                            ((Communicationforrooms) context).Absentmap(selecteditem, position);

                        }
                        //((CommunicationActivity) context).Absentmap(selecteditem, position);

                    }

                }else if(!((CheckBox)v).isChecked()){

                    checkBoxState[position]=false;
                    items.get(posit).setChecked(false);

                    // map.remove(posit);
                    if(context instanceof CommunicationActivity) {
                        ((CommunicationActivity) context).Absentmap1(selecteditem, position);

                        ((CommunicationActivity) context).setMap1(selecteditem, position);
                    }
                    else
                    {
                        ((Communicationforrooms) context).Absentmap1(selecteditem, position);

                        ((Communicationforrooms) context).setMap1(selecteditem, position);
                    }

                   // Toast.makeText(context,"not",Toast.LENGTH_LONG).show();
                }









                /*if(context instanceof CommunicationActivity){
                }*/


            }
        });
//        viewHolder.rel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                CheckBox chk = (CheckBox)v.findViewById(R.id.check);
//
//                int posit=(int)viewHolder.rel.getTag();
//                helper.Studentdetails selecteditem = (helper.Studentdetails) getItem(posit);
//                map = new HashMap<Integer, helper.Studentdetails>();
//
//                if(chk.isChecked()){
//                    chk.setChecked(false);
//                }else if(!chk.isChecked()) {
//                    chk.setChecked(true);
//                }
//
//                if(chk.isChecked()){
//                  //  map.put(posit,selecteditem);
//                    checkBoxState[position]=true;
//                }else if(!chk.isChecked()){
//
//                    checkBoxState[position]=false;
//                  //  map.remove(posit);
//                    Toast.makeText(context,"not",Toast.LENGTH_LONG).show();
//                }
//              /*  Toast.makeText(context,"map"+map.get(posit).getFirstName()+","+map.get(posit).getRollNo()+","+map.get(posit).getStudentID(),Toast.LENGTH_LONG).show();
//
//                ArrayList<helper.Studentdetails> items=new ArrayList<helper.Studentdetails> ();
//                helper.Studentdetails details= new helper.Studentdetails();
//                details.setFirstName(map.get(posit).getFirstName());
//                details.setRollNo(map.get(posit).getRollNo());
//                details.setStudentID(map.get(posit).getStudentID());
//                items.add(details);*/
//
//            }
//        });


        return convertView;
    }

    //ViewHolder inner class
    private class ViewHolder {
        CheckBox check;
        TextView stdname,rollno;
        RelativeLayout rel;
        TextView studentid;


        public ViewHolder(View view) {
            check = (CheckBox)view.findViewById(R.id.check);
            stdname = (TextView) view.findViewById(R.id.stdname);
            rollno = (TextView) view.findViewById(R.id.rollno);
            studentid = (TextView) view.findViewById(R.id.stdid);
            rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }
}
