package adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dci.edukool.teacher.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import Utilities.Utilss;
import helper.RoundedImageView;
import helper.Studentdetails;
import models.BirthdayTestData;

public class AdapterBirthDay extends RecyclerView.Adapter<AdapterBirthDay.MyViewHolder> {

    private ArrayList<Studentdetails> moviesList;
    Utilss utils;
    Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre,class_name,age;
        RoundedImageView profileimage;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.name);
            class_name = (TextView) view.findViewById(R.id.class_name);
            age = (TextView) view.findViewById(R.id.age);
            profileimage = (RoundedImageView) view.findViewById(R.id.profileimage);
        }
    }
    public AdapterBirthDay(ArrayList<Studentdetails> studentDetailsList, Context context) {
        this.moviesList = studentDetailsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_birthday, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Studentdetails movie = moviesList.get(position);
        holder.title.setText(movie.getFirstName());
        holder.age.setText(getAge(movie.getDOB()));
        utils.setTextviewtypeface(3,holder.title);
        utils.setTextviewtypeface(5,holder.age);
        try {
            File imgfile = new File(movie.getPhotoFilename());
            Picasso.with(context).load(imgfile).into(holder.profileimage);
        }
        catch (Exception e)
        {

        }
    }

    public int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
