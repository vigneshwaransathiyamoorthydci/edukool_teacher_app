package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.List;

import Utilities.Utilss;
import models.BirthdayTestData;

public class AdapterQuestionList extends RecyclerView.Adapter<AdapterQuestionList.MyViewHolder> {

    private List<BirthdayTestData> moviesList;
    Utilss utils;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, option1, option2,option3,option4;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.name);
            option1 = (TextView) view.findViewById(R.id.option1);
            option2 = (TextView) view.findViewById(R.id.option2);
            option3 = (TextView) view.findViewById(R.id.option3);
            option4 = (TextView) view.findViewById(R.id.option4);
            utils.setTextviewtypeface(4,title);
            utils.setTextviewtypeface(5,option1);
            utils.setTextviewtypeface(5,option2);
            utils.setTextviewtypeface(5,option3);
            utils.setTextviewtypeface(5,option4);
        }
    }
    public AdapterQuestionList(List<BirthdayTestData> moviesList, Context context) {
        this.moviesList = moviesList;
        utils=new Utilss(context);
    }
    @Override
    public AdapterQuestionList.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_question_list, parent, false);
        return new AdapterQuestionList.MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(AdapterQuestionList.MyViewHolder holder, int position) {
        BirthdayTestData movie = moviesList.get(position);
        holder.title.setText(movie.getData());

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
