package books;

/**
 * Created by iyyapparajr on 5/8/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;
import Utilities.Utilss;

import helper.Rooms;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Roomspinner extends ArrayAdapter {
    Utilss utils;
    private Context context;
    private ArrayList<Rooms> itemList;
    Activity act;
    public Roomspinner(Context context, int textViewResourceId,ArrayList<Rooms> itemList) {

        super(context, textViewResourceId,itemList);
        this.context=context;
        act= (Activity) context;
        this.itemList=itemList;
        utils=new Utilss(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(itemList.get(position).getRoomsname());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        utils.setTextviewtypeface(2,make);

        make.setText(itemList.get(position).getRoomsname());

        return row;
    }

}
