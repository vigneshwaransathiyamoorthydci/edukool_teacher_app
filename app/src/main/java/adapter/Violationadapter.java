package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;

import connection.Violation;
import handraise.Handraise;
import helper.Violationpojo;

/**
 * Created by abimathi on 01-Nov-17.
 */
/*public class Violationadapter {
}*/
public class Violationadapter extends ArrayAdapter<Violationpojo> {
    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView apps;

    }

    public Violationadapter(Context context, ArrayList<Violationpojo> users) {
        super(context, R.layout.violationitem, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.violationitem, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.studentname);
            viewHolder.apps= (TextView) convertView.findViewById(R.id.studentviolation);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        Violationpojo user = getItem(position);

        viewHolder.name.setText(user.getName());
        viewHolder.apps.setText(user.getOpenapps());
        // viewHolder.home.setText(user.hometown);
        // Return the completed view to render on screen
        return convertView;
    }
}