package adapter;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import Utilities.Utilss;
import helper.Buzzerhelper;
import helper.RoundedImageView;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Buzzeradapter extends ArrayAdapter<Buzzerhelper> {

    Context con;
    Utilss util;
    private ArrayList<Buzzerhelper> countryList;

    // CoolReader mactivity;

    public Buzzeradapter(Context context, int textViewResourceId,
                         ArrayList<Buzzerhelper> countryList) {
        super(context, textViewResourceId, countryList);
        this.countryList = new ArrayList<Buzzerhelper>();
        util = new Utilss((Activity) context);
        con = context;
        this.countryList.addAll(countryList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.buzzeritem, null);

            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.buzzeritemname);
            holder.bookiamge = (RoundedImageView) convertView.findViewById(R.id.buzzeritemimage);
            holder.rollnumber = (TextView) convertView.findViewById(R.id.buzzeritemrollnumber);
            holder.parentlayout = (LinearLayout) convertView.findViewById(R.id.background);
            util.setTextviewtypeface(1, holder.rollnumber);
            util.setTextviewtypeface(1, holder.name);
            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Buzzerhelper helper = countryList.get(position);
        holder.name.setText(helper.getBuzzername());
        if (helper.getBuzzerid().equalsIgnoreCase("0")) {
            holder.rollnumber.setVisibility(View.GONE);

        } else {
            holder.rollnumber.setVisibility(View.VISIBLE);
            holder.rollnumber.setText(helper.getBuzzerid());
        }
        setbackground1(holder.bookiamge, helper.getBuzzerimage());

        if (helper.isBackground()) {
            holder.rollnumber.setTextColor(con.getResources().getColor(R.color.white));
            holder.name.setTextColor(con.getResources().getColor(R.color.white));
            holder.parentlayout.setBackgroundResource(R.color.appbg);
        } else {
            holder.rollnumber.setTextColor(con.getResources().getColor(R.color.subject_title_grey));
            holder.name.setTextColor(con.getResources().getColor(R.color.subject_title_grey));
            holder.parentlayout.setBackgroundResource(R.color.white);


        }

        return convertView;

    }

    void setbackground1(RoundedImageView view, String filepath) {
        File imgFile = new File(filepath);
        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
             */
            view.setImageBitmap(myBitmap);

        }
    }

    private class ViewHolder {
        RoundedImageView bookiamge;
        TextView name;
        TextView rollnumber;
        LinearLayout parentlayout;
    }
}