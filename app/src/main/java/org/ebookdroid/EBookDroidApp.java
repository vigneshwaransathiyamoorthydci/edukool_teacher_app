package org.ebookdroid;

import org.ebookdroid.common.bitmaps.BitmapManager;
import org.ebookdroid.common.cache.CacheManager;
import org.ebookdroid.common.settings.AppSettings;
import org.ebookdroid.common.settings.BackupSettings;
import org.ebookdroid.common.settings.LibSettings;
import org.ebookdroid.common.settings.LibSettings.Diff;
import org.ebookdroid.common.settings.SettingsManager;
import org.ebookdroid.common.settings.listeners.IAppSettingsChangeListener;
import org.ebookdroid.common.settings.listeners.IBackupSettingsChangeListener;
import org.ebookdroid.common.settings.listeners.ILibSettingsChangeListener;
import org.ebookdroid.ui.library.RecentActivityController;

import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.dci.edukool.teacher.NetworkController;
import com.dci.edukool.teacher.R;

import org.emdev.BaseDroidApp;
import org.emdev.common.android.VMRuntimeHack;
import org.emdev.common.backup.BackupManager;
import org.emdev.common.fonts.FontManager;
import org.emdev.ui.actions.ActionController;
import org.emdev.ui.actions.ActionDialogBuilder;
import org.emdev.utils.concurrent.Flag;

import java.lang.reflect.Field;

import network.LruBitmapCache;

public class EBookDroidApp extends BaseDroidApp implements IAppSettingsChangeListener, IBackupSettingsChangeListener,
        ILibSettingsChangeListener {

    public static final Flag initialized = new Flag();

    private static EBookDroidApp instance;


    public static final String TAG = NetworkController.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    LruBitmapCache mLruBitmapCache;

    private static NetworkController mInstance;

    /**
     * {@inheritDoc}
     * 
     * @see android.app.Application#onCreate()
     */
    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;


        SettingsManager.init(this);
        CacheManager.init(this);
        FontManager.init();

        VMRuntimeHack.preallocateHeap(AppSettings.current().heapPreallocate);

        SettingsManager.addListener(this);
        onAppSettingsChanged(null, AppSettings.current(), null);
        onBackupSettingsChanged(null, BackupSettings.current(), null);

        initialized.set();
    }
    public static synchronized NetworkController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            getLruBitmapCache();
            mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
        }

        return this.mImageLoader;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    private void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            Log.e("Exception", "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);

        }
    }
    @Override
    public void onTerminate() {
        SettingsManager.onTerminate();
    }

    /**
     * {@inheritDoc}
     * 
     * @see android.app.Application#onLowMemory()
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        BitmapManager.clear("on Low Memory: ");
    }

    @Override
    public void onAppSettingsChanged(final AppSettings oldSettings, final AppSettings newSettings,
            final AppSettings.Diff diff) {

        BitmapManager.setPartSize(1 << newSettings.bitmapSize);
        BitmapManager.setUseEarlyRecycling(newSettings.useEarlyRecycling);
        BitmapManager.setUseBitmapHack(newSettings.useBitmapHack);
        BitmapManager.setUseNativeTextures(newSettings.useNativeTextures);

        setAppLocale(newSettings.lang);
    }

    @Override
    public void onBackupSettingsChanged(final BackupSettings oldSettings, final BackupSettings newSettings,
            final BackupSettings.Diff diff) {
        BackupManager.setMaxNumberOfAutoBackups(newSettings.maxNumberOfAutoBackups);
    }

    @Override
    public void onLibSettingsChanged(final LibSettings oldSettings, final LibSettings newSettings, final Diff diff) {
        if (diff.isCacheLocationChanged()) {
            CacheManager.setCacheLocation(newSettings.cacheLocation, !diff.isFirstTime());
        }
    }

    public static void checkInstalledFonts(final Context context) {
        if (!FontManager.external.hasInstalled()) {
            if (!SettingsManager.isInitialFlagsSet(SettingsManager.INITIAL_FONTS)) {
                SettingsManager.setInitialFlags(SettingsManager.INITIAL_FONTS);

                final ActionDialogBuilder b = new ActionDialogBuilder(context, new ActionController<Context>(context));
                final WebView view = new WebView(context);

                final String text = context.getResources().getString(R.string.font_reminder);
                final String content = "<html><body>" + text + "</body></html>";

                view.loadDataWithBaseURL("file:///fake/not_used", content, "text/html", "UTF-8", "");

                b.setTitle(R.string.font_reminder_title);
                b.setView(view);
                b.setPositiveButton(android.R.string.ok, R.id.actions_no_action);
                b.show();
            }
        }
    }

    public static void onActivityClose(final boolean finishing) {
        if (finishing && !SettingsManager.hasOpenedBooks() && !RecentActivityController.working.get()) {
            if (instance != null) {
                instance.onTerminate();
            }
            Log.i(APP_NAME, "Application finished");
            System.exit(0);
        }
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
