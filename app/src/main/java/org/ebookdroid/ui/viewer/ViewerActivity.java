package org.ebookdroid.ui.viewer;

import org.ebookdroid.EBookDroidApp;

import com.dci.edukool.teacher.R;

import org.ebookdroid.common.settings.AppSettings;
import org.ebookdroid.common.settings.books.BookSettings;
import org.ebookdroid.common.settings.books.Bookmark;
import org.ebookdroid.common.settings.types.ToastPosition;
import org.ebookdroid.common.touch.TouchManagerView;
import org.ebookdroid.ui.viewer.views.ManualCropView;
import org.ebookdroid.ui.viewer.views.PageViewZoomControls;
import org.ebookdroid.ui.viewer.views.SearchControls;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.LoginActivity;

import java.util.concurrent.atomic.AtomicLong;

import org.emdev.common.log.LogContext;
import org.emdev.common.log.LogManager;
import org.emdev.ui.AbstractActionActivity;
import org.emdev.ui.actions.ActionMethodDef;
import org.emdev.ui.actions.ActionTarget;
import org.emdev.ui.uimanager.IUIManager;
import org.emdev.utils.LengthUtils;

import Utilities.Utilss;
import books.PulseQuestionActivity;
import connection.MultiThreadChatServerSync;
import connection.clientThread;

@ActionTarget(
// action list
        actions = {
// start
                @ActionMethodDef(id = R.id.mainmenu_about, method = "showAbout")
// finish
        })
public class ViewerActivity extends AbstractActionActivity<ViewerActivity, ViewerActivityController> {

    public static final DisplayMetrics DM = new DisplayMetrics();

    private static final AtomicLong SEQ = new AtomicLong();
    public static boolean syn;
    final LogContext LCTX;
    IView view;
    TextView pagetext;
    boolean lock;
    int page;
    Bundle savedinstance;
    BroadcastReceiver forhandrais;
    boolean ac;
    String sendata,bookname;
    SharedPreferences pref;
    private Toast pageNumberToast;
    private Toast zoomToast;
    private PageViewZoomControls zoomControls;
    private SearchControls searchControls;
    private FrameLayout frameLayout;
    private TouchManagerView touchView;
    private boolean menuClosedCalled;
    private ManualCropView cropControls;
    private ImageView bookbinback;
    private ImageView bookbinhandrise;
    private ImageView bookbinblock;
    private ImageView bookbinarrow;
    private ImageView bookbinprojection;
    private ImageView bookbinorientation;
    private ImageView bookbinpulse;
    private ImageView bookbinsync;
    private ImageView bookbinlock;


    /**
     * Instantiates a new base viewer activity.
     */
    public ViewerActivity() {
        super();
        LCTX = LogManager.root().lctx(this.getClass().getSimpleName(), true).lctx("" + SEQ.getAndIncrement(), true);
    }

    /**
     * {@inheritDoc}
     *
     * @see AbstractActionActivity#createController()
     */
    @Override
    protected ViewerActivityController createController() {
        return new ViewerActivityController(this);
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onNewIntent(): " + intent);
        }
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(final Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onCreate(): " + getIntent());
        }

        restoreController();
        getController().beforeCreate(this);

        super.onCreate(savedInstanceState);

        getWindowManager().getDefaultDisplay().getMetrics(DM);
        LCTX.i("XDPI=" + DM.xdpi + ", YDPI=" + DM.ydpi);

        // frameLayout = new FrameLayout(this);

        view = AppSettings.current().viewType.create(getController());
        this.registerForContextMenu(view.getView());
        ac = getIntent().getBooleanExtra("ac", false);


        //  LayoutUtils.fillInParent(frameLayout, view.getView());

      /*  frameLayout.addView(view.getView());
        frameLayout.addView(getZoomControls());
        frameLayout.addView(getManualCropControls());
        frameLayout.addView(getSearchControls());
        frameLayout.addView(getTouchView());*/

        getController().afterCreate();

        setContentView(R.layout.newpdflayout);

        bookbinback = (ImageView) findViewById(R.id.bookbinback);
        bookbinhandrise = (ImageView) findViewById(R.id.handrise);
        bookbinblock = (ImageView) findViewById(R.id.block);

        bookbinarrow = (ImageView) findViewById(R.id.bookbinarrow);
        bookbinprojection = (ImageView) findViewById(R.id.projector);
        bookbinorientation = (ImageView) findViewById(R.id.orientation);
        bookbinpulse = (ImageView) findViewById(R.id.pulse);
        bookbinsync = (ImageView) findViewById(R.id.sync);
        bookbinlock = (ImageView) findViewById(R.id.lock);
        pagetext = (TextView) findViewById(R.id.pagetext);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);


        frameLayout = (FrameLayout) findViewById(R.id.frame);
        frameLayout.addView(view.getView());
        frameLayout.addView(getZoomControls());
        frameLayout.addView(getManualCropControls());
        frameLayout.addView(getSearchControls());
        frameLayout.addView(getTouchView());

        sendata = getIntent().getStringExtra("senddata");
        bookname = getIntent().getStringExtra("bookname");
        int pageNumber = 9;

      /*  final int pageCount = getController().getDocumentModel().getPageCount();
        if (pageNumber > 0 && pageNumber < pageCount) {
          //  final String msg = base.getContext().getString(R.string.bookmark_invalid_page, offset, pageCount - 1 + offset);
            //Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
           // return false;

        }*/
        // getController().jumpToPage(pageNumber, 0, 0, AppSettings.current().storeGotoHistory);


        bookbinback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String change;
                if (ac) {
                    change = "Academic Book";
                } else {
                    change = "References";

                }

                if (!lock && !syn) {
                    finish();
                } else if (lock) {
                    Utilss.lockpoopup(ViewerActivity.this, change, getResources().getString(R.string.lock));
                } else if (syn) {
                    Utilss.lockpoopup(ViewerActivity.this, change, getResources().getString(R.string.whiteboardsync1));

                }
                //finish();
            }
        });
		/*bookbinback.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});*/
        bookbinhandrise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    bookbinhandrise.setImageResource(R.drawable.handraiseenableforwhite);
                    Utilss.Listpopup(ViewerActivity.this);
                } else {
                    Toast.makeText(ViewerActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                    bookbinhandrise.setImageResource(R.drawable.bookbinraise);

                    //Toast.makeText(MuPDFActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                }
                //toast();
                //mDocView.setDisplayedViewIndex(10);
                //createUI(savedinstance,10);
            }
        });
        bookbinlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!lock) {
                        String senddata = "lock";
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                        bookbinlock.setImageResource(R.drawable.lockpressed);
                        lock = true;

					/*edit.putBoolean("lockdevice",true);
					edit.commit();*/
                    } else {
                        String senddata = "unlock";
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                        bookbinlock.setImageResource(R.drawable.bookbinlock);
                        lock = false;

					/*edit.putBoolean("lockdevice",false);
					edit.commit()*/
                        ;
                    }
                } else {
                    Utilss.lockpoopup(ViewerActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                }

				/*
					lock=true;
					Intent in=new Intent("lock");

					in.putExtra("unlockorlock",true);
					sendBroadcast(in);
					bookbinlock.setImageResource(R.drawable.lockpressed);
				}
				else {

					lock=false;
					Intent in=new Intent("lock");
					in.putExtra("unlockorlock",false);
					sendBroadcast(in);
					bookbinlock.setImageResource(R.drawable.bookbinlock);


				}*/
            }
        });
        bookbinprojection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* toast();*/
            }
        });
        bookbinorientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*toast();*/
            }
        });
        bookbinpulse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewerActivity.this, PulseQuestionActivity.class);
                startActivity(intent);

                //toast();
            }
        });
        bookbinsync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //toast();
                // Toast.makeText(ViewerActivity.this,sendata,Toast.LENGTH_SHORT).show();
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!syn) {
                        bookbinsync.setImageResource(R.drawable.unsync);
                        new clientThread(MultiThreadChatServerSync.thread, sendata + "@" + "" + ViewerActivityController.pagenumbergetting).start();
                        syn = true;
                    } else {
                        bookbinsync.setImageResource(R.drawable.bookbinsync);
                        syn = false;
                        new clientThread(MultiThreadChatServerSync.thread, "FileClose").start();

                    }
                } else {
                    Utilss.lockpoopup(ViewerActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                }

            }
        });

        bookbinblock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*toast();*/
                if (!lock) {
					/*String senddata = "lock";
					new clientThread(MultiThreadChatServerSync.thread, senddata).start();
					lockunlock=true;*/
					/*lock=true;
					Intent in=new Intent("lock");

					in.putExtra("unlockorlock",true);
					sendBroadcast(in);*/
                } else {
					/*String senddata = "unlock";
					new clientThread(MultiThreadChatServerSync.thread, senddata).start();
					lockunlock=false;*/
				/*	lock=false;
					Intent in=new Intent("lock");
					in.putExtra("unlockorlock",true);
					sendBroadcast(in);*/

                }


            }
        });

    }


    @Override
    protected void onResume() {
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onResume()");
        }

        getController().beforeResume();

        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);

        forhandrais = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (LoginActivity.handraise.size() > 0) {
                    bookbinhandrise.setImageResource(R.drawable.handraiseenableforwhite);
                    //Utils.Listpopup(Whiteboard.this);
                } else {
                    //Toast.makeText(Whiteboard.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                    bookbinhandrise.setImageResource(R.drawable.bookbinraise);

                    //Toast.makeText(MuPDFActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
                }
            }
        };
        IntentFilter fileterhandraise = new IntentFilter("handraise");
        registerReceiver(forhandrais, fileterhandraise);

        IUIManager.instance.onResume(this);
        if (syn) {
            bookbinsync.setImageResource(R.drawable.unsync);

        } else {
            bookbinsync.setImageResource(R.drawable.bookbinsync);
        }


        if (lock) {

            bookbinlock.setImageResource(R.drawable.lockpressed);
            // lock=true;

					/*edit.putBoolean("lockdevice",true);
					edit.commit();*/
        } else {

            bookbinlock.setImageResource(R.drawable.bookbinlock);
            // lock=false;

					/*edit.putBoolean("lockdevice",false);
					edit.commit()*/
            ;
        }
        getController().afterResume();
    }

    @Override
    protected void onPause() {
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onPause(): " + isFinishing());
        }

        getController().beforePause();

        super.onPause();
        unregisterReceiver(forhandrais);
        IUIManager.instance.onPause(this);

        getController().afterPause();
    }

    @Override
    public void onWindowFocusChanged(final boolean hasFocus) {
        if (hasFocus && this.view != null) {
            IUIManager.instance.setFullScreenMode(this, this.view.getView(), AppSettings.current().fullScreen);
        }
    }

    @Override
    protected void onDestroy() {
        final boolean finishing = isFinishing();
        if (LCTX.isDebugEnabled()) {
            LCTX.d("onDestroy(): " + finishing);
        }

        getController().beforeDestroy(finishing);
        super.onDestroy();
        getController().afterDestroy(finishing);

        EBookDroidApp.onActivityClose(finishing);
    }

    protected IView createView() {
        return AppSettings.current().viewType.create(getController());
    }

    public TouchManagerView getTouchView() {
        if (touchView == null) {
            touchView = new TouchManagerView(getController());
        }
        return touchView;
    }

    public void currentPageChanged(final String pageText, final String bookTitle) {
        if (LengthUtils.isEmpty(pageText)) {

            return;
        }

        final AppSettings app = AppSettings.current();
        if (IUIManager.instance.isTitleVisible(this) && app.pageInTitle) {
            // getWindow().setTitle("(" + pageText + ") " + bookTitle);
            pagetext.setText(pageText);

            getWindow().setTitle(bookTitle);
            return;
        }

        if (app.pageNumberToastPosition == ToastPosition.Invisible) {
            return;
        }
        if (pageNumberToast != null) {
            pageNumberToast.setText(pageText);
        } else {
            pageNumberToast = Toast.makeText(this, pageText, Toast.LENGTH_SHORT);
        }

        pageNumberToast.setGravity(app.pageNumberToastPosition.position, 0, 0);
        pageNumberToast.show();
    }

    public void zoomChanged(final float zoom) {
        if (getZoomControls().isShown()) {
            return;
        }

        final AppSettings app = AppSettings.current();

        if (app.zoomToastPosition == ToastPosition.Invisible) {
            return;
        }

        final String zoomText = String.format("%.2f", zoom) + "x";

        if (zoomToast != null) {
            zoomToast.setText(zoomText);
        } else {
            zoomToast = Toast.makeText(this, zoomText, Toast.LENGTH_SHORT);
        }

        zoomToast.setGravity(app.zoomToastPosition.position, 0, 0);
        zoomToast.show();
    }

    @Override
    protected void onPostCreate(final Bundle savedInstanceState) {
        getController().beforePostCreate();
        super.onPostCreate(savedInstanceState);
        getController().afterPostCreate();
    }

    public PageViewZoomControls getZoomControls() {
        if (zoomControls == null) {
            zoomControls = new PageViewZoomControls(this, getController().getZoomModel());
            zoomControls.setGravity(Gravity.RIGHT | Gravity.BOTTOM);
        }
        return zoomControls;
    }

    public SearchControls getSearchControls() {
        if (searchControls == null) {
            searchControls = new SearchControls(this);
        }
        return searchControls;
    }

    public ManualCropView getManualCropControls() {
        if (cropControls == null) {
            cropControls = new ManualCropView(getController());
        }
        return cropControls;
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, final View v, final ContextMenuInfo menuInfo) {
        menu.clear();
        menu.setHeaderTitle(R.string.app_name);
        menu.setHeaderIcon(R.drawable.application_icon);
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu_context, menu);
        updateMenuItems(menu);
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onCreateOptionsMenu(Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        menu.clear();

        final MenuInflater inflater = getMenuInflater();

        if (hasNormalMenu()) {
            inflater.inflate(R.menu.mainmenu, menu);
        } else {
            inflater.inflate(R.menu.mainmenu_context, menu);
        }

        return true;
    }

    protected boolean hasNormalMenu() {
        return true;
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onMenuOpened(int, Menu)
     */
    @Override
    public boolean onMenuOpened(final int featureId, final Menu menu) {
        view.changeLayoutLock(true);
        IUIManager.instance.onMenuOpened(this);
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    protected void updateMenuItems(final Menu menu) {
        final AppSettings as = AppSettings.current();

        setMenuItemChecked(menu, as.fullScreen, R.id.mainmenu_fullscreen);
        setMenuItemVisible(menu, false, R.id.mainmenu_showtitle);
        setMenuItemChecked(menu, getZoomControls().getVisibility() == View.VISIBLE, R.id.mainmenu_zoom);

        final BookSettings bs = getController().getBookSettings();
        if (bs == null) {
            return;
        }

        setMenuItemChecked(menu, bs.nightMode, R.id.mainmenu_nightmode);
        setMenuItemChecked(menu, bs.autoLevels, R.id.mainmenu_autolevels);
        setMenuItemChecked(menu, bs.cropPages, R.id.mainmenu_croppages);
        setMenuItemChecked(menu, bs.splitPages, R.id.mainmenu_splitpages, R.drawable.viewer_menu_split_pages,
                R.drawable.viewer_menu_split_pages_off);

        final MenuItem navMenu = menu.findItem(R.id.mainmenu_nav_menu);
        if (navMenu != null) {
            final SubMenu subMenu = navMenu.getSubMenu();
            subMenu.removeGroup(R.id.actions_goToBookmarkGroup);
            if (AppSettings.current().showBookmarksInMenu && LengthUtils.isNotEmpty(bs.bookmarks)) {
                for (final Bookmark b : bs.bookmarks) {
                    addBookmarkMenuItem(subMenu, b);
                }
            }
        }

    }

    protected void addBookmarkMenuItem(final Menu menu, final Bookmark b) {
        final MenuItem bmi = menu.add(R.id.actions_goToBookmarkGroup, R.id.actions_goToBookmark, Menu.NONE, b.name);
        bmi.setIcon(R.drawable.viewer_menu_bookmark);
        setMenuItemExtra(bmi, "bookmark", b);
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onPanelClosed(int, Menu)
     */
    @Override
    public void onPanelClosed(final int featureId, final Menu menu) {
        menuClosedCalled = false;
        super.onPanelClosed(featureId, menu);
        if (!menuClosedCalled) {
            onOptionsMenuClosed(menu);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see android.app.Activity#onOptionsMenuClosed(Menu)
     */
    @Override
    public void onOptionsMenuClosed(final Menu menu) {
        menuClosedCalled = true;
        IUIManager.instance.onMenuClosed(this);
        view.changeLayoutLock(false);
    }

    @Override
    public final boolean dispatchKeyEvent(final KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_MENU) {
            if (!hasNormalMenu()) {
                getController().getOrCreateAction(R.id.actions_openOptionsMenu).run();
                return true;
            }
        }

        if (getController().dispatchKeyEvent(event)) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    public void showToastText(final int duration, final int resId, final Object... args) {
        Toast.makeText(getApplicationContext(), getResources().getString(resId, args), duration).show();
    }

}
