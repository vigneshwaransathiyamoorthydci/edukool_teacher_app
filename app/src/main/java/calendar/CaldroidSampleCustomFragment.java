package calendar;

import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.util.ArrayList;

import Utilities.DatabaseHandler;

public class CaldroidSampleCustomFragment extends CaldroidFragment {

	SharedPreferences pref;
	ArrayList<Calendarpojo>list=new ArrayList<>();
	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		pref=getActivity().getSharedPreferences("Teacher", getActivity().MODE_PRIVATE);
		//edit=pref.edit();
		//DatabaseHandler db=new DatabaseHandler(getActivity().getApplicationContext());
		DatabaseHandler db=new DatabaseHandler(getActivity().getApplicationContext(),pref.getString("staffdbname", ""),DatabaseHandler.DATABASE_VERSION);;
        ArrayList<Calendarpojo> calendarlist =db.retriveCalendar();

		return new CaldroidSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData,calendarlist,pref);
	}

	private class MyTask extends AsyncTask<Void, Void,  ArrayList<Calendarpojo>> {
		@Override
		protected  ArrayList<Calendarpojo> doInBackground(Void... params) {
			//do stuff and return the value you want
			DatabaseHandler db=new DatabaseHandler(getActivity().getApplicationContext(),pref.getString("staffdbname", ""),DatabaseHandler.DATABASE_VERSION);;
			ArrayList<Calendarpojo> calendarlist =db.retriveCalendar();

			return calendarlist;
		}

		@Override
		protected void onPostExecute( ArrayList<Calendarpojo> result) {
			list.addAll(result);
			// Call activity method with results

		}
	}

}
