package calendar;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.dci.edukool.teacher.R;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidGridAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import Utilities.DatabaseHandler;
import hirondelle.date4j.DateTime;

public class CaldroidSampleCustomAdapter extends CaldroidGridAdapter {
	ArrayList<Calendarpojo> calendarlist;
	SharedPreferences pref;
	public CaldroidSampleCustomAdapter(Context context, int month, int year,
			Map<String, Object> caldroidData,
			Map<String, Object> extraData,ArrayList<Calendarpojo> calendarlist,SharedPreferences prefernece) {
		super(context, month, year, caldroidData, extraData);
		this.calendarlist=calendarlist;
		this.pref=prefernece;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View cellView = convertView;

		// For reuse
		if (convertView == null) {
			cellView = inflater.inflate(R.layout.custom_cell, null);
		}

		int topPadding = cellView.getPaddingTop();
		int leftPadding = cellView.getPaddingLeft();
		int bottomPadding = cellView.getPaddingBottom();
		int rightPadding = cellView.getPaddingRight();


		TextView tv1 = (TextView) cellView.findViewById(R.id.tv1);
		TextView tv2 = (TextView) cellView.findViewById(R.id.tv2);
		TextView tv3 = (TextView) cellView.findViewById(R.id.tv3);

		ImageView holiday = (ImageView) cellView.findViewById(R.id.holiday);
		ImageView exam = (ImageView) cellView.findViewById(R.id.exams);
		ImageView event = (ImageView) cellView.findViewById(R.id.events);
		ImageView competition = (ImageView) cellView.findViewById(R.id.competition);
		ImageView assign = (ImageView) cellView.findViewById(R.id.assign);

		tv1.setTextColor(Color.BLACK);

		// Get dateTime of this cell
		DateTime dateTime = this.datetimeList.get(position);
        cellView.setTag(position);
	//	Log.e("datetime",""+dateTime);
		Resources resources = context.getResources();

		// Set color of the dates in previous / next month
		if (dateTime.getMonth() != month) {
			tv1.setTextColor(resources
					.getColor(com.caldroid.R.color.caldroid_darker_gray));
		}

		boolean shouldResetDiabledView = false;
		boolean shouldResetSelectedView = false;

		// Customize for disabled dates and date outside min/max dates
		if ((minDateTime != null && dateTime.lt(minDateTime))
				|| (maxDateTime != null && dateTime.gt(maxDateTime))
				|| (disableDates != null && disableDates.indexOf(dateTime) != -1)) {

			tv1.setTextColor(CaldroidFragment.disabledTextColor);
			if (CaldroidFragment.disabledBackgroundDrawable == -1) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.disable_cell);
			} else {
				cellView.setBackgroundResource(CaldroidFragment.disabledBackgroundDrawable);
			}

			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.red_border_gray_bg);
			}

		} else {
			shouldResetDiabledView = true;
		}

		// Customize for selected dates
		if (selectedDates != null && selectedDates.indexOf(dateTime) != -1) {
			cellView.setBackgroundColor(resources
					.getColor(com.caldroid.R.color.caldroid_sky_blue));

			tv1.setTextColor(Color.BLACK);

		} else {
			shouldResetSelectedView = true;
		}

		if (shouldResetDiabledView && shouldResetSelectedView) {
			// Customize for today
			if (dateTime.equals(getToday())) {
				cellView.setBackgroundResource(com.caldroid.R.drawable.red_border);
			} else {
				cellView.setBackgroundResource(com.caldroid.R.drawable.cell_bg);
			}
		}
		Boolean chk=false,modi=false,oth=false;


		//DatabaseHandler db=new DatabaseHandler(context);
		DatabaseHandler db=new DatabaseHandler(context,pref.getString("staffdbname", ""),DatabaseHandler.DATABASE_VERSION);;


		String dateString=dateTime.toString();
		Log.i("datetimee",dateString);
		String[] separated = dateString.split(" ");

for(int i=0;i<calendarlist.size();i++) {
	Log.e("size",calendarlist.size()+"");
	//Log.e("eventst",calendarlist.get(i).getEventstart());
	//Log.e("eveend",calendarlist.get(i).getEventend());

	Date beforedate=null;
	Date enddate=null;
	Date comparedate=null;
	SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
	try {
		beforedate = format.parse(calendarlist.get(i).getEventstart());
		enddate = format.parse(calendarlist.get(i).getEventend());
		comparedate = format.parse(separated[0]);
	}
	catch (Exception e)
	{
		Log.e("issuedate",""+e.toString());
	}

	if (beforedate.compareTo(comparedate)==0) {
        Log.e("evid",calendarlist.get(i).getEventid()+"");
        tv2.setText(calendarlist.get(i).getEventstart());
        Log.i("text2",tv2.getText().toString());
		chk=true;


		String catname="";
		int id=calendarlist.get(i).getCategoryid();

		Cursor typecur=db.retriveCalendarType(id);
		while(typecur.moveToNext()){
			catname=typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
		}
		if (catname.equalsIgnoreCase("holiday")) {
			holiday.setBackgroundResource(R.drawable.mholiday);
		}
		if (catname.equalsIgnoreCase("Competition")) {
			competition.setBackgroundResource(R.drawable.mcompetition);
		}
		if (catname.equalsIgnoreCase("event")) {
			event.setBackgroundResource(R.drawable.mevent);
		}
		if (catname.equalsIgnoreCase("Examination")) {
			exam.setBackgroundResource(R.drawable.mexam);
		}
		if (catname.equalsIgnoreCase("Assignment")) {
			assign.setBackgroundResource(R.drawable.mexam);
		}
	}
	else{
		if(!chk && i==calendarlist.size()-1){
			tv2.setText("");
				/*viewHolder.holiday.setBackgroundResource(0);
				viewHolder.competition.setBackgroundResource(0);
				viewHolder.event.setBackgroundResource(0);
				viewHolder.exam.setBackgroundResource(0);*/}
	}


   /* Log.e("evenend",calendarlist.get(7).getEventend()+"");
	Log.e("category",calendarlist.get(7).getCategory()+"");*/



		  // }
	if(comparedate.after(beforedate) && comparedate.before(enddate)){
        Log.e("evid", calendarlist.get(i).getEventend() + "");
        tv3.setText(calendarlist.get(i).getEventend());
			Log.i("text3",tv3.getText().toString());

       oth=true;
        String catname="";
		int id=calendarlist.get(i).getCategoryid();

		Cursor typecur=db.retriveCalendarType(id);
		while(typecur.moveToNext()){
			catname=typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
		}
		if (catname.equalsIgnoreCase("holiday")) {
			holiday.setBackgroundResource(R.drawable.mholiday);
		}
		if (catname.equalsIgnoreCase("Competition")) {
			competition.setBackgroundResource(R.drawable.mcompetition);
		}
		if (catname.equalsIgnoreCase("event")) {
			event.setBackgroundResource(R.drawable.mevent);
		}
		if (catname.equalsIgnoreCase("Examination")) {
			exam.setBackgroundResource(R.drawable.mexam);
		}
		if (catname.equalsIgnoreCase("Assignment")) {
			assign.setBackgroundResource(R.drawable.mexam);
		}

	}
	else{
		if(!chk && !modi && i==calendarlist.size()-1){
			/*tv3.setText("");
			holiday.setBackgroundResource(0);
			competition.setBackgroundResource(0);
			event.setBackgroundResource(0);
			exam.setBackgroundResource(0);
			assign.setBackgroundResource(0);*/}
	}

	if (enddate.compareTo(comparedate)==0) {
		Log.e("evid", calendarlist.get(i).getEventend() + "");
		tv3.setText(calendarlist.get(i).getEventend());
		Log.i("text3",tv3.getText().toString());
		modi=true;

		String catname="";
		int id=calendarlist.get(i).getCategoryid();

		Cursor typecur=db.retriveCalendarType(id);
		while(typecur.moveToNext()){
			catname=typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
		}
		if (catname.equalsIgnoreCase("holiday")) {
			holiday.setBackgroundResource(R.drawable.mholiday);
		}
		if (catname.equalsIgnoreCase("Competition")) {
			competition.setBackgroundResource(R.drawable.mcompetition);
		}
		if (catname.equalsIgnoreCase("event")) {
			event.setBackgroundResource(R.drawable.mevent);
		}
		if (catname.equalsIgnoreCase("Examination")) {
			exam.setBackgroundResource(R.drawable.mexam);
		}
		if (catname.equalsIgnoreCase("Assignment")) {
			assign.setBackgroundResource(R.drawable.mexam);
		}

	}
	else{
		if(!chk && !modi && !oth && i==calendarlist.size()-1){
			tv3.setText("");
			holiday.setBackgroundResource(0);
			competition.setBackgroundResource(0);
			event.setBackgroundResource(0);
			exam.setBackgroundResource(0);
			assign.setBackgroundResource(0);}
	}




}

		  tv1.setText("" + dateTime.getDay());
            Log.i("today",dateTime.getDay().toString());
		if (dateTime.equals(getToday())) {

			ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
			Log.i("newtv2",tv2.getText().toString());
			Log.i("newtv3",tv3.getText().toString());
			if(tv2.getText().toString()!=null || tv2.getText().toString()!="") {
				Cursor c = db.retriveStartDate(tv2.getText().toString());


				if (c.getCount() > 0) {
					while (c.moveToNext()) {
						Calendarpojo pojo = new Calendarpojo();
						pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
						pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
						pojo.setCategory(c.getString(c.getColumnIndex("Category")));
						pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
						pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
						pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
						pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
						pojo.setCategoryid(c.getInt(c.getColumnIndex("CategoryID")));
						list.add(pojo);
					}

				}
			}
			if(!tv2.getText().toString().equalsIgnoreCase(tv3.getText().toString())) {
				if (tv3.getText().toString() != null || tv3.getText().toString() != "") {
					Cursor end = db.retriveEnddate(tv3.getText().toString());
					if (end.getCount() > 0) {
						while (end.moveToNext()) {
							Calendarpojo pojo = new Calendarpojo();
							pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
							pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
							pojo.setCategory(end.getString(end.getColumnIndex("Category")));
							pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
							pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
							pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
							pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
							pojo.setCategoryid(end.getInt(end.getColumnIndex("CategoryID")));
							list.add(pojo);
						}
					}


				}
			}
			if (list.size() != 0) {
				((Calendaractivity) context).setList(list);
			} else if (list.size() == 0) {
				((Calendaractivity) context).setList2();
			}

		}


		/*tv2.setText(""+);*/

        cellView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               TextView stdate=(TextView)v.findViewById(R.id.tv2);
			   TextView enddate=(TextView)v.findViewById(R.id.tv3);
				Log.i("text2",stdate.getText().toString());
				Log.i("text3",enddate.getText().toString());
				//set DateTime
                ((Calendaractivity) context).setDate(datetimeList.get((int)v.getTag()));
                //
				DatabaseHandler db=new DatabaseHandler(context,pref.getString("staffdbname", ""),DatabaseHandler.DATABASE_VERSION);;
			   ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();

        if(stdate.getText().toString()!=null || stdate.getText().toString()!="") {
			Cursor c = db.retriveStartDate(stdate.getText().toString());


			if (c.getCount() > 0) {
				while (c.moveToNext()) {
					Calendarpojo pojo = new Calendarpojo();
					pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
					pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
					pojo.setCategory(c.getString(c.getColumnIndex("Category")));
					pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
					pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
					pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
					pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
					pojo.setCategoryid(c.getInt(c.getColumnIndex("CategoryID")));
					list.add(pojo);
				}

			}
		}

		if(!stdate.getText().toString().equalsIgnoreCase(enddate.getText().toString())) {
			if (enddate.getText().toString() != null || enddate.getText().toString() != "") {
				Cursor end = db.retriveEnddate(enddate.getText().toString());
				if (end.getCount() > 0) {
					while (end.moveToNext()) {
						Calendarpojo pojo = new Calendarpojo();
						pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
						pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
						pojo.setCategory(end.getString(end.getColumnIndex("Category")));
						pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
						pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
						pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
						pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
						pojo.setCategoryid(end.getInt(end.getColumnIndex("CategoryID")));
						list.add(pojo);
					}
				}


			}

		}
				if (list.size() != 0) {
					Log.i("listsize",""+list.size());
					((Calendaractivity) context).setList(list);
				} else if (list.size() == 0) {
					((Calendaractivity) context).setList2();
				}
    /*else {
			((Calendaractivity) context).setList2();
		}*/


            }
        });




		/*if (dateTime.equals(getToday())) {

            ((Calendaractivity) context).setDate(dateTime);

			Cursor c = db.retriveStartDate(separated[0]);
			ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
			if (c.getCount() > 0) {
				while (c.moveToNext()) {
					Calendarpojo pojo = new Calendarpojo();
					pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
					pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
					pojo.setCategory(c.getString(c.getColumnIndex("Category")));
					pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
					pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
					pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
					pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
					list.add(pojo);
				}
			}

			Cursor end = db.retriveEnddate(separated[0]);
			if (end.getCount() > 0) {
				while (end.moveToNext()) {
					Calendarpojo pojo = new Calendarpojo();
					pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
					pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
					pojo.setCategory(end.getString(end.getColumnIndex("Category")));
					pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
					pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
					pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
					pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
					list.add(pojo);
				}
			}
			if (list.size() != 0) {
				((Calendaractivity) context).setList(list);
			} else if (list.size() == 0) {
				((Calendaractivity) context).setList2();
			}

		}*/
		// Somehow after setBackgroundResource, the padding collapse.
		// This is to recover the padding
		cellView.setPadding(leftPadding, topPadding, rightPadding,
				bottomPadding);

		// Set custom color if required
		setCustomResources(dateTime, cellView, tv1);

		return cellView;
	}

}
