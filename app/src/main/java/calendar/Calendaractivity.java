package calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.CalendarCustomView;
import com.dci.edukool.teacher.CalenderCategoryAdapter;
import com.dci.edukool.teacher.GridAdapter;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;
import com.imanoweb.calendarview.CalendarListener;
import com.imanoweb.calendarview.CustomCalendarView;
import com.imanoweb.calendarview.DayDecorator;
import com.imanoweb.calendarview.DayView;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CalendarHelper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import Utilities.DatabaseHandler;
import Utilities.Service;
import books.Attendancewithcheckparentlayout;
import books.OnDateSelect;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.RoundedImageView;
import helper.Staffloingdetails;
import hirondelle.date4j.DateTime;
import Utilities.*;


/**
 * Created by kirubakaranj on 5/26/2017.
 */

public class Calendaractivity extends BaseActivity {

    public static boolean frompopup;
    LinearLayout calendarfrag;
    ListView firstlist_view;
    FirstListAdapter fadapter;
    ArrayList<helper.Calendarpojo> catelist;
    DatabaseHandler db;
    Dialog dialog1;
    List<Date> dayValueInCells;
    String calstart="",calend="";
    ListView desc_view;
    SecondListAdapter second_adapter;
    ArrayList<Calendarpojo> desclist;
    ImageView back, down;
    Button today;
    //SharedPreferences pref;
    TextView staffclass, todat, notsch,classname;
    Staffloingdetails logindetails;
    RoundedImageView profileimage;
    Button createEvent;
    PopupWindow popupWindow;
    String setFrom = "", setTo = "", event_category = "";
    int eventcategoryid;
    Spinner event;
    ArrayAdapter<String> dataAdapter;
    EditText popupFromdate;
    EditText popupTodate, event_title, event_desc;
    int eveid;
    BroadcastReceiver startquizz;
    Utilss utils;
    String calenderpath;
    TextView schedule,cal;
    ImageView handraise;
    ImageView dnd;
    CustomCalendarView calendarview;
    CalenderCategoryAdapter adapter;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    List<helper.Calendarpojo> list;
    RecyclerView recycle_cal;
    ImageView close;
    CalendarCustomView calender_view;
    Boolean onetimeToast = false;
    private CaldroidFragment caldroidFragment;


    public static String headerdate() {
        Calendar c = Calendar.getInstance();
        //System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        notsch = (TextView) findViewById(R.id.notsch);
        back = (ImageView) findViewById(R.id.back);
        down = (ImageView) findViewById(R.id.down);
        today = (Button) findViewById(R.id.today);
        todat = (TextView) findViewById(R.id.todat);
       // calender_view = (CalendarCustomView) findViewById(R.id.calender_view);
        calendarview = (CustomCalendarView) findViewById(R.id.calendarview);
        cal= (TextView) findViewById(R.id.cal);
        staffclass = (TextView) findViewById(R.id.staffclas);
        classname= (TextView) findViewById(R.id.class_name);
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        createEvent = (Button) findViewById(R.id.createEvent);
        recycle_cal = (RecyclerView) findViewById(R.id.recycle_cal);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();
        handraise = (ImageView) findViewById(R.id.handrise);
        schedule = (TextView) findViewById(R.id.schedule);
        dialog1 = new Dialog(this);
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
        utils=new Utilss(this);
        utils.setTextviewtypeface(3,cal);
        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(Calendaractivity.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(Calendaractivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* calender_view.setOnDateSelect(new OnDateSelect() {
            @Override
            public void OnDateSelect(Date date, int pos) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                getcurrent(df.format(date));
                Toast.makeText(Calendaractivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }


        });*/


        dnd = (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("dnd", false)) {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata = senddata + "@false";
                    dnd.setImageResource(R.drawable.inactive);

                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                } else {
                    String senddata = "DND";

                    senddata = senddata + "@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    //  dnd=dnd+1;
                    dnd.setImageResource(R.drawable.active);

                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
            }
        });


        utils = new Utilss(Calendaractivity.this);
        utils.setTextviewtypeface(4, schedule);



        calendarview.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                getcurrent(df.format(date));
                Toast.makeText(Calendaractivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMonthChanged(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
               // getcurrent(df.format(date));

                Toast.makeText(Calendaractivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });


        getSupportActionBar().hide();
       /* caldroidFragment = new CaldroidSampleCustomFragment();

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar, caldroidFragment);
        t.commit();*/



        firstlist_view = (ListView) findViewById(R.id.calendarevent);
        desc_view = (ListView) findViewById(R.id.desriptionview);

        catelist = new ArrayList<>();
        list = new ArrayList<>();
        // db=new DatabaseHandler(this);
        db = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);


        logindetails = db.staffdetails();
        setbackground(profileimage, logindetails.getPhotoFilename());

        Cursor c = db.retriveCategory_Calendartype();
        while (c.moveToNext()) {
            helper.Calendarpojo pojo = new helper.Calendarpojo();
            pojo.setCalendarname(c.getString(c.getColumnIndex("Name")));
            pojo.setCalendarimage(c.getString(c.getColumnIndex("Image")));

            list.add(pojo);
        }
        utils.setTextviewtypeface(5, notsch);

        fadapter = new FirstListAdapter(this, catelist);
        firstlist_view.setAdapter(fadapter);

        adapter = new CalenderCategoryAdapter(list, Calendaractivity.this);
        recycle_cal.setLayoutManager(new LinearLayoutManager(Calendaractivity.this,
                LinearLayoutManager.HORIZONTAL, false));
        recycle_cal.setAdapter(adapter);


        desclist = new ArrayList<>();

        // desclist=db.retriveCalendar();
        // Log.d("listsize",desclist.size()+"");
        second_adapter = new SecondListAdapter(this, db.retriveCalendar());

        desc_view.setAdapter(second_adapter);


        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTime today = CalendarHelper.convertDateToDateTime(new Date());
                moveTo(today);
                todat.setText(formatdate(getToday().toString()));
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendaractivity.this.finish();
            }
        });


        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        staffclass.setText(pref.getString("classname", ""));
        classname.setText(pref.getString("classname", ""));
        // todat.setText(headerdate());


        calenderpath = pref.getString("calender", "");



       /* handrise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // new validateUserTask().execute("");
            }
        });*/

        createEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new validateUserTask().execute("");
            }
        });

        new validateUserTask().execute("");


       /* Date ca = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(ca);
        Toast.makeText(this, formattedDate, Toast.LENGTH_SHORT).show();
        getcurrent(formattedDate);*/

    }



    private class DisabledColorDecorator implements DayDecorator {
        @Override
        public void decorate(DayView dayView) {
            Date ca = dayView.getDate();
            long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
            long endTime = 0;
            long curTime = 0;
            Boolean start = false, stop = false;
            int color = Color.parseColor("#e61737");
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(ca);
            /*Log.i("dayview",formattedDate);
            Log.i("calstart",calstart+calend);*/

            List<Date> dates = new ArrayList<Date>();

            String str_date = calstart;
            String end_date = calend;

            DateFormat formatter;

            formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = null;
            try {
                startDate = (Date) formatter.parse(str_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date endDate = null;
            try {
                endDate = (Date) formatter.parse(end_date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
                curTime = startDate.getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }

            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
            for (int i = 0; i < dates.size(); i++) {
                Date lDate = (Date) dates.get(i);
                String ds = formatter.format(lDate);
                if (formattedDate.equals(ds)) {
                    dayView.setTextColor(getResources().getColor(R.color.redcolor));
                  //  dayView.setTypeface(dayView.getTypeface(), Typeface.BOLD);
                }
                Log.i("mydate", ds);
            }
            /*if (formattedDate.equals(calstart)){
                dayView.setTextColor(color);
                start = true;
            }
            if (start){
                dayView.setTextColor(color);
            }
            if (formattedDate.equals("2019-05-23")){


            }*/
        }
    }

    @Override
    public int getlayout() {
        return R.layout.calender;
    }

    public void Datepick ( final int chk){
        onetimeToast = false;
        final Date value = new Date();
        final Calendar cal = Calendar.getInstance();
        cal.setTime(value);
        new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view,
                                          int y, int m, int d) {
                        cal.set(Calendar.YEAR, y);
                        cal.set(Calendar.MONTH, m);
                        cal.set(Calendar.DAY_OF_MONTH, d);

                        m++;
                        DecimalFormat mFormat = new DecimalFormat("00");
                        String month = mFormat.format(Double.valueOf(m));
                        String dat = mFormat.format(Double.valueOf(d));

                        Log.e("calf", y + "-" + month + "-" + dat + "");
                        String[] separated = new String[3];
                        setFrom = y + "-" + month + "-" + dat;

                        if (chk == 0) {
                            if (popupTodate.getText().toString().isEmpty()) {

                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                    Date date1 = sdf.parse(setFrom);
                                    Date date2 = sdf.parse(Service.currendatetimecheck());
                                    if (date1.compareTo(date2) > 0 || date1.compareTo(date2) == 0) {

                                        popupFromdate.setText(setFrom);

                                    } else {
                                        Toast.makeText(Calendaractivity.this, "Valid date", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (Exception e) {

                                }
                                // popupFromdate.setText(setFrom);

                            } else {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                    Date date1 = sdf.parse(popupTodate.getText().toString());
                                    Date date2 = sdf.parse(setFrom);

                                    System.out.println("date1 : " + sdf.format(date1));
                                    System.out.println("date2 : " + sdf.format(date2));

                                    if (date1.compareTo(date2) < 0) {
                                        Toast.makeText(Calendaractivity.this, "Event start date should always fall before the event end date or be same as the event end date.", Toast.LENGTH_SHORT).show();
                                    } else {

                                        Date date3 = sdf.parse(setFrom);
                                        Date date4 = sdf.parse(Service.currendatetimecheck());
                                        if (date3.compareTo(date4) > 0 || date3.compareTo(date4) == 0) {

                                            popupFromdate.setText(setFrom);

                                        } else {
                                            Toast.makeText(Calendaractivity.this, "Valid date", Toast.LENGTH_SHORT).show();

                                        }
                                        // popupFromdate.setText(setFrom);
                                    }


                                   /* if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                    } else {
                                        System.out.println("How to get here?");
                                    }*/
                                } catch (Exception e) {

                                }
                            }

                        } else {
                            if (popupFromdate.getText().toString().isEmpty()) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                    Date date1 = sdf.parse(setFrom);
                                    Date date2 = sdf.parse(Service.currendatetimecheck());
                                    if (date1.compareTo(date2) > 0 || date1.compareTo(date2) == 0) {

                                        popupTodate.setText(setFrom);

                                    } else {
                                        Toast.makeText(Calendaractivity.this, "Valid date", Toast.LENGTH_SHORT).show();

                                    }


                                } catch (Exception e) {

                                }


                            } else {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                                    Date date1 = sdf.parse(setFrom);
                                    Date date2 = sdf.parse(popupFromdate.getText().toString());

                                    System.out.println("date1 : " + sdf.format(date1));
                                    System.out.println("date2 : " + sdf.format(date2));

                                    if (date1.compareTo(date2) < 0) {
                                        Toast.makeText(Calendaractivity.this, "Event start date should always fall before the event end date or be same as the event end date.", Toast.LENGTH_SHORT).show();
                                    } else {


                                        Date date3 = sdf.parse(setFrom);
                                        Date date4 = sdf.parse(Service.currendatetimecheck());
                                        if (date3.compareTo(date4) > 0 || date3.compareTo(date4) == 0) {

                                            popupTodate.setText(setFrom);

                                        } else {
                                            Toast.makeText(Calendaractivity.this, "Valid date", Toast.LENGTH_SHORT).show();

                                        }

                                    }


                                   /* if (date1.compareTo(date2) > 0) {
                                        System.out.println("Date1 is after Date2");
                                    } else if (date1.compareTo(date2) < 0) {
                                        System.out.println("Date1 is before Date2");
                                    } else if (date1.compareTo(date2) == 0) {
                                        System.out.println("Date1 is equal to Date2");
                                    } else {
                                        System.out.println("How to get here?");
                                    }*/
                                } catch (Exception e) {

                                }
                            }

                        }

                        /*if(!onetimeToast)*//*{
                        if(chk==0) {
                            setFrom=y+"-"+month+"-"+dat;
                           // setTo="2017-06-20";
                            //&& setTo!=null && !setTo.equals("")
                            if(!setTo.isEmpty() || setTo!="") {
                                separated = setTo.split("-");
                           }


                        if(!setTo.isEmpty() || setTo!="") {
                            if (y <= Integer.parseInt(separated[0])) {
                                if (Integer.parseInt(month) <= Integer.parseInt(separated[1])) {
                                    if (Integer.parseInt(dat) < Integer.parseInt(separated[2])) {
                                        popupFromdate.setText(setFrom);
                                    } else {
                                      //  popupFromdate.setError(null);
                                      //  popupFromdate.setError("Please give date Less than End date");
                                       Toast.makeText(getApplicationContext(), "Please give Minimum date than End date", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                  //  popupFromdate.setError(null);
                                 //   popupFromdate.setError("Please give date Less than End date");
                                    Toast.makeText(getApplicationContext(), "Please give Minimum date than End date", Toast.LENGTH_LONG).show();
                                }
                            } else {
                              //  popupFromdate.setError(null);
                             //   popupFromdate.setError("Please give date Less than End date");
                                Toast.makeText(getApplicationContext(), "Please give Minimum date than End date", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            popupFromdate.setText(setFrom);
                        }


                        }
                        else if(chk==1){
                            setTo=y+"-"+month+"-"+dat;



                            if(setFrom!="" ) {
                                separated = setFrom.split("-");
                            }


                            if(setFrom!="" ) {
                                if (y >= Integer.parseInt(separated[0])) {
                                    if (Integer.parseInt(month) >= Integer.parseInt(separated[1])) {
                                        if (Integer.parseInt(dat) > Integer.parseInt(separated[2])) {
                                            popupTodate.setText(setTo);
                                        } else {
                                           // popupTodate.setError(null);
                                         //   popupTodate.setError("Please give date greater than FromDate");
                                            Toast.makeText(getApplicationContext(), "Please give Maximum date than FromDate", Toast.LENGTH_LONG).show();
                                        }
                                    } else {
                                      //  popupTodate.setError(null);
                                    //    popupTodate.setError("Please give date greater than FromDate");

                                        Toast.makeText(getApplicationContext(), "Please give Maximum date than FromDate", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                //    popupTodate.setError(null);
                                //    popupTodate.setError("Please give date greater than FromDate");
                                    Toast.makeText(getApplicationContext(), "Please give Maximum date than FromDate", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                popupTodate.setText(setTo);
                            }
                         //   popupTodate.setText(setTo);
                        }}*/
                        onetimeToast = true;
                        // now show the time picker
                        /*new TimePickerDialog(NoteEditor.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override public void onTimeSet(TimePicker view,
                                                                    int h, int min) {
                                        cal.set(Calendar.HOUR_OF_DAY, h);
                                        cal.set(Calendar.MINUTE, min);
                                        value = cal.getTime();
                                    }
                                }, cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE), true).show();*/
                    }
                }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show();
    }


    public DateTime getToday () {
        DateTime today = null;
        if (today == null) {
            today = CalendarHelper.convertDateToDateTime(new Date());
        }
        return today;
    }

    public void showPopup () {
        LayoutInflater inflater = (LayoutInflater) Calendaractivity.this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.event_popup,
                (ViewGroup) findViewById(R.id.popup_element));
        frompopup = true;
//        popupWindow = new PopupWindow(layout, 750, 500, true);
//        popupWindow.showAtLocation(layout, Gravity.CENTER, 0, 0);


        dialog1.setCancelable(false);

        dialog1.setContentView(layout);
        popupFromdate = (EditText) layout.findViewById(R.id.fromdat);
        TextView titles = (TextView) layout.findViewById(R.id.titles);
        TextView msgtitle = (TextView) layout.findViewById(R.id.msgtitle);
        TextView fromdeno = (TextView) layout.findViewById(R.id.fromdeno);
        TextView todeno = (TextView) layout.findViewById(R.id.todeno);
        TextView tit = (TextView) layout.findViewById(R.id.tit);
        TextView de = (TextView) layout.findViewById(R.id.de);
        Button closes = (Button) layout.findViewById(R.id.close);

        utils.setTextviewtypeface(5, msgtitle);
        utils.setTextviewtypeface(3, titles);
        utils.setTextviewtypeface(5, fromdeno);
        utils.setTextviewtypeface(5, todeno);
        utils.setTextviewtypeface(5, tit);
        utils.setTextviewtypeface(5, de);
        utils.setButtontypeface(1, closes);

        event_title = (EditText) layout.findViewById(R.id.title);
        event_desc = (EditText) layout.findViewById(R.id.desc);
        popupTodate = (EditText) layout.findViewById(R.id.popuptodate);
        event = (Spinner) layout.findViewById(R.id.event);
        close = (ImageView) layout.findViewById(R.id.closeimage);
        popupFromdate.setInputType(InputType.TYPE_NULL);
        popupTodate.setInputType(InputType.TYPE_NULL);

        popupFromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Datepick(0);
                //   popupFromdate.setText(popdate);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();

            }
        });

        popupTodate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Datepick(1);

            }
        });

        DatabaseHandler db = new DatabaseHandler(Calendaractivity.this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        Cursor c = db.retriveCategory_Calendar();
        final ArrayList<String> eventlist = new ArrayList<>();

        final ArrayList<helper.Calendarpojo> getlist = db.getmastercalendar();


        for (helper.Calendarpojo cal : getlist) {
            eventlist.add(android.text.Html.fromHtml(cal.getCalendarname()).toString());
        }
       /* while(c.moveToNext()){
            eventlist.add(c.getString(c.getColumnIndex("Category")));
        }*/


        dataAdapter = new ArrayAdapter<String>(layout.getContext(),
                R.layout.custom_spinner_item, eventlist);
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner_item);
        event.setAdapter(dataAdapter);
        String eventname;

        event.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), eventlist.get(position) + "", Toast.LENGTH_LONG).show();
                eventcategoryid = getlist.get(position).getCalendarid();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Button btnDismiss = (Button) layout.findViewById(R.id.close);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (popupFromdate.getText().toString().isEmpty()) {
                    Toast.makeText(Calendaractivity.this, "Select Start date", Toast.LENGTH_SHORT).show();

                } else if (popupTodate.getText().toString().isEmpty()) {
                    Toast.makeText(Calendaractivity.this, "Select End date", Toast.LENGTH_SHORT).show();

                } else if (event_title.getText().toString().isEmpty()) {
                    Toast.makeText(Calendaractivity.this, "Please Fill Event title", Toast.LENGTH_SHORT).show();

                } else if (event_desc.getText().toString().isEmpty()) {
                    Toast.makeText(Calendaractivity.this, "Please Fill Event description", Toast.LENGTH_SHORT).show();

                } else {
                    DatabaseHandler db = new DatabaseHandler(Calendaractivity.this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

                    // DatabaseHandler db = new DatabaseHandler(Calendaractivity.this);
                    Cursor c = db.retriveCalendarCommon();
                    if (c.getCount() > 0) {

                        eveid = c.getCount() + 1;

                   /* c.moveToNext();
                    eveid=c.getInt(c.getColumnIndex("EventID"));*/
                    }


               /*  String senddata="Calendar@"+event_category+"@"+popupFromdate.getText().toString()+
                         "@"+popupTodate.getText().toString()+"@"+event_title.getText().toString()+"@"+
                         event_desc.getText().toString()+"@"+eveid;*/
                    try {
                        JSONObject objcalendaer = new JSONObject();
                        objcalendaer.put("EventName", event_title.getText().toString());
                        objcalendaer.put("EventDescription", event_desc.getText().toString());
                        objcalendaer.put("EventStartDate", popupFromdate.getText().toString());
                        objcalendaer.put("EventEndDate", popupTodate.getText().toString());
                        objcalendaer.put("Status", "1");
                        objcalendaer.put("CategoryID", eventcategoryid);


                        String senddata = "Calendar@" + objcalendaer.toString();
                        //  { "EventName" : "", "EventDescription" : "", "EventStartDate" : "", "EventEndDate" : "", "Status" : 1, "CategoryID" : 5 }
                        stratExamCompletedPopup(senddata);
                    } catch (Exception e) {

                    }


                    // popupWindow.dismiss();

                }
            }
        });
        dialog1.show();

        //  popupWindow.showAsDropDown(layout, 0, 0);
    }

    public void setList (ArrayList < Calendarpojo > desclist2) {

        Log.e("sizelist", "" + desclist2.size());

        for (int i = 0; i < desclist2.size(); i++) {
            Log.i("setlist", desclist2.get(i).getEventstart());
            Log.i("setlist", desclist2.get(i).getEventend());
        }


        if (desclist2.size() != 0) {
            desc_view.setVisibility(View.VISIBLE);
            notsch.setVisibility(View.GONE);
            second_adapter = new SecondListAdapter(this, desclist2);
            desc_view.setAdapter(second_adapter);
        }

    }

    public void setList2 () {

        desc_view.setVisibility(View.GONE);
        notsch.setVisibility(View.VISIBLE);

    }

    public void setDate (DateTime datetime){
        String dateString = datetime.toString();
        String[] separated = dateString.split(" ");

        todat.setText(formatdate(separated[0]));
    }

    public String formatdate (String fdate){
        String datetime = null;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat d = new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date convertedDate = inputFormat.parse(fdate);
            datetime = d.format(convertedDate);

        } catch (ParseException e) {

        }
        return datetime;


    }

    public void moveTo (DateTime datetime){
        caldroidFragment.moveToDateTime(datetime);

        String dateString = datetime.toString();
        String[] separated = dateString.split(" ");

        Cursor c = db.retriveStartDate(separated[0]);
        ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                Calendarpojo pojo = new Calendarpojo();
                pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
                pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
                pojo.setCategory(c.getString(c.getColumnIndex("Category")));
                pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
                pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
                pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
                pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
                list.add(pojo);
            }
        }

        Cursor end = db.retriveEnddate(separated[0]);
        if (end.getCount() > 0) {
            while (end.moveToNext()) {
                Calendarpojo pojo = new Calendarpojo();
                pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
                pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
                pojo.setCategory(end.getString(end.getColumnIndex("Category")));
                pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
                pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
                pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
                pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
                list.add(pojo);
            }
        }
        if (list.size() != 0) {
            setList(list);
        } else if (list.size() == 0) {
            setList2();
        }
    }


    void setbackground (RoundedImageView view, String filepath){
        try {


            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {

        }
    }

    public void stratExamCompletedPopup ( final String sendata){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        TextView content = (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialog1.dismiss();

                // finish();
            }
        });
        title.setText("Event");
        content.setText("Share Event to?");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // File file=new File(video.getVideopath());

                //  String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();
                //  String senddata="Quiz@Open";
                new clientThread(MultiThreadChatServerSync.thread, sendata).start();
                // Toast.makeText(Calendaractivity.this,sendata,Toast.LENGTH_SHORT).show();
                Intent in = new Intent("calendarevent");
                sendBroadcast(in);
              /*  Intent in=new Intent("calendar");
                sendBroadcast(in);*/
              /*  if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                dialog.dismiss();
                dialog1.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  File file=new File(video.getVideopath());

             /*   if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                // String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                //  String senddata="Quiz@Open";

                Intent in = new Intent(Calendaractivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("Title", "Calendar Share");
                in.putExtra("senddata", sendata);
                startActivity(in);


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void downloadfile (String urls, String filepath, String filename){

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/" + filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


    }

    private class validateUserTask extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(Calendaractivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            String userid = "";
            //   String userid = "Function:DownloadStudentEvents|UserName:" + stdlogin_id + "|StudentId:"+ stdid;
            userid = "Function:DownloadEvents|StaffId:" + pref.getString("staffid", "0") + "|UserName:" + pref.getString("portalstaffid", "0")/*"|UserType:Staff"*//*+"|Portal UserID:"+pref.getString("portalstaffid", "0")*/;

            String res = null;
            byte[] data;

            try {
                data = userid.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                if (utils.hasConnection()) {
                    postParameters.clear();
                    postParameters.add(new BasicNameValuePair("WS", base64_register));
                    Service sr = new Service(Calendaractivity.this);
                    res = sr.getLogin(postParameters, Url.baseurl
                            /* "http://api.schoolproject.dci.in/api/"*/);
                    /* response = CustomHttpClient.executeHttpPost(params[0], postParameters);*/
                    // res = response.toString();
                    Log.e("response", "resoibse" + res + ",,");
                } else {
                    utils.Showalert();
                }
                // res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                if (dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {

            if (dia.isShowing())
                dia.cancel();

            try {

                JSONObject jObj = new JSONObject(result);
                String statusCode = jObj.getString("StatusCode");

                if (statusCode.toString().equalsIgnoreCase("200")) {
                    JSONArray CalendarArray = jObj.getJSONArray("Calender");
                    ArrayList<Calendarpojo> calendarlist = new ArrayList<Calendarpojo>();


                    Log.e("calenlensize", CalendarArray.length() + "");
                    for (int c = 0; c < CalendarArray.length(); c++) {
                        Calendarpojo cpojo = new Calendarpojo();
                        JSONObject con = CalendarArray.getJSONObject(c);
                        //  g_content.setBatchID();

                        cpojo.setEventid(Integer.parseInt(con.getString("ID")));
                        cpojo.setEventname(con.getString("EventName"));
                        cpojo.setEventDesc(con.getString("EventDescription"));
                        cpojo.setEventstart(con.getString("EventStartDate"));
                        cpojo.setEventend(con.getString("EventEndDate"));
                        cpojo.setCategory(con.getString("Category"));
                        cpojo.setCategoryid(Integer.parseInt(con.getString("CategoryID")));

                        String calfile = con.getString("CategoryImage").substring(con.getString("CategoryImage").lastIndexOf("/") + 1, con.getString("CategoryImage").length());

                        try {
                            downloadfile(con.getString("CategoryImage"), calenderpath, calfile);
                        } catch (Exception e) {
                        }

                        cpojo.setCategoryIcon(con.getString("CategoryImage"));
                        calendarlist.add(cpojo);

                    }
                    db.TableCalendar(calendarlist);

                }//status code=200

            } catch (Exception e) {

            }

            caldroidFragment = new CaldroidSampleCustomFragment();

            FragmentTransaction t = getSupportFragmentManager().beginTransaction();
            t.replace(R.id.calendar, caldroidFragment);
            t.commit();

            second_adapter = new SecondListAdapter(Calendaractivity.this, db.retriveCalendar());

            desc_view.setAdapter(second_adapter);


            new validateUserTask2(result).execute("");
        }
    }

    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response, statusres;

        ProgressDialog dia;

        validateUserTask2(String statusres) {
            this.statusres = statusres;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(Calendaractivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:SyncComplete|StaffId:" + pref.getString("staffid", "0") + "|Portal User ID:" + pref.getString("portalstaffid", "0") + "|Type:event";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(Calendaractivity.this);

                        res = sr.getLogin(postParameters, Url.baseurl
                                /*"http://api.schoolproject.dci.in/api/"*/);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                } else {
                    Log.e("error", "ss");
                }

            } catch (Exception e) {
                if (dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if (dia.isShowing())
                dia.cancel();
            if (dia.isShowing())
                dia.cancel();
            try {
                JSONObject jObj = new JSONObject(result);
                if (jObj.getString("StatusCode").equals("200")) {
                    Toast.makeText(getApplicationContext(), "Calendar data downloaded successfully.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Downloading data failed.", Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {

            }
            //Toast.makeText(getApplicationContext(), result,Toast.LENGTH_LONG).show();


        }//close onPostExecute
    }// close validateUserTask


    private void getcurrent(String format1) {
        String stdate = "", nndate = "";

        DatabaseHandler db = new DatabaseHandler(this.getApplicationContext(), pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);

        ArrayList<Calendarpojo> calendarlist = db.retriveCalendar();

        Log.i("calendarlist", "" + calendarlist.size());

        for (int i = 0; i < calendarlist.size(); i++) {

            Log.e("size", calendarlist.size() + "");
            //Log.e("eventst",calendarlist.get(i).getEventstart());
            //Log.e("eveend",calendarlist.get(i).getEventend());

            Date beforedate = null;
            Date enddate = null;
            Date comparedate = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                beforedate = format.parse(calendarlist.get(i).getEventstart());
                enddate = format.parse(calendarlist.get(i).getEventend());
                comparedate = format.parse(format1);
            } catch (Exception e) {
                Log.e("issuedate", "" + e.toString());
            }

            if (beforedate.compareTo(comparedate) == 0) {
                Log.e("evid", calendarlist.get(i).getEventid() + "");
                stdate = calendarlist.get(i).getEventstart();


                String catname = "";
                int id = calendarlist.get(i).getCategoryid();

                Cursor typecur = db.retriveCalendarType(id);
                while (typecur.moveToNext()) {
                    catname = typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
                }

            }
            else {
               /* if (i == calendarlist.size() - 1) {
                    stdate = "";
                }*/
            }


            if (comparedate.after(beforedate) && comparedate.before(enddate)) {
                Log.e("evid", calendarlist.get(i).getEventend() + "");
                nndate = calendarlist.get(i).getEventend();

                String catname = "";
                int id = calendarlist.get(i).getCategoryid();

                Cursor typecur = db.retriveCalendarType(id);
                while (typecur.moveToNext()) {
                    catname = typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
                }

            }
            else {
               /* if (i == calendarlist.size() - 1) {
                    nndate = "";
                }*/
            }

            if (enddate.compareTo(comparedate) == 0) {
                Log.e("evid", calendarlist.get(i).getEventend() + "");
                nndate = calendarlist.get(i).getEventend();

                String catname = "";
                int id = calendarlist.get(i).getCategoryid();

                Cursor typecur = db.retriveCalendarType(id);
                while (typecur.moveToNext()) {
                    catname = typecur.getString(typecur.getColumnIndex("Name"));
//     typecur.getString(typecur.getColumnIndex("Image"));
                }

            }
            else {
                /*if (i == calendarlist.size() - 1) {
                    nndate = "";
                }*/
            }

            ArrayList<Calendarpojo> list = new ArrayList<Calendarpojo>();
            Log.i("newtv2", stdate);
            Log.i("newtv3", nndate);
            if (stdate != null || stdate != "") {
                Cursor c = db.retriveStartDate(stdate);


                if (c.getCount() > 0) {
                    while (c.moveToNext()) {
                        Calendarpojo pojo = new Calendarpojo();
                        pojo.setEventid(c.getInt(c.getColumnIndex("EventID")));
                        pojo.setEventname(c.getString(c.getColumnIndex("EventName")));
                        pojo.setCategory(c.getString(c.getColumnIndex("Category")));
                        pojo.setCategoryIcon(c.getString(c.getColumnIndex("CategoryIcon")));
                        pojo.setEventDesc(c.getString(c.getColumnIndex("EventDescription")));
                        pojo.setEventstart(c.getString(c.getColumnIndex("EventStartDate")));
                        pojo.setEventend(c.getString(c.getColumnIndex("EventEndDate")));
                        pojo.setCategoryid(c.getInt(c.getColumnIndex("CategoryID")));
                        list.add(pojo);
                    }

                }
            }
            if (!stdate.equalsIgnoreCase(nndate)) {
                if (nndate != null || nndate != "") {
                    Cursor end = db.retriveEnddate(nndate);
                    if (end.getCount() > 0) {
                        while (end.moveToNext()) {
                            Calendarpojo pojo = new Calendarpojo();
                            pojo.setEventid(end.getInt(end.getColumnIndex("EventID")));
                            pojo.setEventname(end.getString(end.getColumnIndex("EventName")));
                            pojo.setCategory(end.getString(end.getColumnIndex("Category")));
                            pojo.setCategoryIcon(end.getString(end.getColumnIndex("CategoryIcon")));
                            pojo.setEventDesc(end.getString(end.getColumnIndex("EventDescription")));
                            pojo.setEventstart(end.getString(end.getColumnIndex("EventStartDate")));
                            pojo.setEventend(end.getString(end.getColumnIndex("EventEndDate")));
                            pojo.setCategoryid(end.getInt(end.getColumnIndex("CategoryID")));
                            list.add(pojo);
                        }
                    }

                }
            }
            if (list.size() != 0) {
                setList(list);
            } else if (list.size() == 0) {
                setList2();

            }
            for (int k = 0; k < list.size(); k++) {
                calstart = list.get(k).getEventstart();
                calend = list.get(k).getEventend();
            }


        }

        setCal();
    }

    public void setCal(){

        List<Date> dates = new ArrayList<Date>();

        String str_date = calstart;
        String end_date = calend;
        long interval = 24 * 1000 * 60 * 60; // 1 hour in millis
        long endTime = 0;
        long curTime = 0;

        DateFormat formatter;

        formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        try {
            endDate = (Date) formatter.parse(end_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            endTime = endDate.getTime(); // create your endtime here, possibly using Calendar or Date
            curTime = startDate.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        while (curTime <= endTime) {
            dates.add(new Date(curTime));
            curTime += interval;
        }
        for (int i = 0; i < dates.size(); i++) {
            Date lDate = (Date) dates.get(i);
            String ds = formatter.format(lDate);
            Log.i("mydate", ds + "date size" + dates.size());
        }
        CalendarCustomView ca = new CalendarCustomView(Calendaractivity.this, dates);

        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        List<DayDecorator> decorators = new ArrayList<>();
        decorators.add(new DisabledColorDecorator());
        calendarview.setDecorators(decorators);
        calendarview.refreshCalendar(currentCalendar);


    }


    @Override
    protected void onResume () {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        startquizz = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (frompopup) {

                    ContentValues calendarcv = new ContentValues();
                    calendarcv.put("EventID", eveid);
                    calendarcv.put("EventName", event_title.getText().toString());
                    calendarcv.put("EventDescription", event_title.getText().toString());
                    calendarcv.put("EventStartDate", popupFromdate.getText().toString());
                    calendarcv.put("EventEndDate", popupTodate.getText().toString());
                    calendarcv.put("CategoryID", eventcategoryid);
                    Cursor c = db.retriveTypeCalendar(eventcategoryid);
                    while (c.moveToNext()) {
                        calendarcv.put("Category", c.getString(c.getColumnIndex("Name")));
                    }
                    db.insertCalendar(calendarcv);
                    try {
                        if (popupWindow != null) {
                            if (popupWindow.isShowing())
                                popupWindow.dismiss();
                        }
                    } catch (Exception e) {

                    }

                }
            }
        };
        IntentFilter quizfilter = new IntentFilter("calendarevent");
        registerReceiver(startquizz, quizfilter);

        if (pref.getBoolean("dnd", false)) {
            dnd.setImageResource(R.drawable.active);

        } else {
            dnd.setImageResource(R.drawable.inactive);

        }
        if (LoginActivity.handraise.size() > 0) {
            handraise.setImageResource(R.drawable.handraiseenable);

            // Utils.Listpopup(BookBinActivity.this);
        } else {
            handraise.setImageResource(R.drawable.handrise);

            //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        unregisterReceiver(startquizz);
    }
}








