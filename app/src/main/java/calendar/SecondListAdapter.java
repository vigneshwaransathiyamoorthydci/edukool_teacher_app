package calendar;

/**
 * Created by kirubakaranj on 5/26/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.dci.edukool.teacher.R;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Utilities.DatabaseHandler;
import Utilities.Utilss;

public class SecondListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Calendarpojo> items;
    DatabaseHandler obj;
    Utilss utils;

    public SecondListAdapter(Context context, ArrayList<Calendarpojo> items) {
        this.context = context;
        this.items = items;
        utils=new Utilss((Activity) context);
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.desc_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

         Calendarpojo currentItem = (Calendarpojo) getItem(position);
    try {

        if (currentItem.getCategory().equalsIgnoreCase("holiday")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mholiday);
        }
        if (currentItem.getCategory().equalsIgnoreCase("Competition")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mcompetition);
        }
        if (currentItem.getCategory().equalsIgnoreCase("event")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mevent);
        }
        if (currentItem.getCategory().equalsIgnoreCase("exam")) {
            viewHolder.cateicon.setBackgroundResource(R.drawable.mexam);
        }
    }
    catch(NullPointerException e){

    }
        viewHolder.event.setText(currentItem.getEventname());
//        Log.e("secondlist",currentItem.getEventname());

        viewHolder.desc.setText(Html.fromHtml(currentItem.getEventDesc()));
        viewHolder.catid.setText(currentItem.getCategoryid()+"");


       /* SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        String date = formatter.format(Date.parse(currentItem.getEventstart()));
        String edate = formatter.format(Date.parse(currentItem.getEventend()));*/

      ///for gone TextView
        viewHolder.stdat.setText(currentItem.getEventstart());
        viewHolder.enddat.setText(currentItem.getEventend());
        utils.setTextviewtypeface(5,viewHolder.dat);
        utils.setTextviewtypeface(1, viewHolder.event);
        utils.setTextviewtypeface(0, viewHolder.desc);
      //gone TextView

        viewHolder.dat.setText(formatdate(currentItem.getEventstart())+" To "+formatdate(currentItem.getEventend()));


        //  viewHolder.stdname.setText(currentItem.get());

        viewHolder.send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View parentview=(View)v.getParent();

                TextView event_click = (TextView) parentview.findViewById(R.id.event);
                TextView desc_click = (TextView) parentview.findViewById(R.id.desc);
                TextView catid_click = (TextView) parentview.findViewById(R.id.catid);

                TextView stdat_click = (TextView) parentview.findViewById(R.id.stdat);
                TextView end_click = (TextView) parentview.findViewById(R.id.enddat);
                Calendaractivity.frompopup=false;
                try {
                    JSONObject objcalendaer = new JSONObject();
                    objcalendaer.put("EventName", event_click.getText().toString());
                    objcalendaer.put("EventDescription", desc_click.getText().toString());
                    objcalendaer.put("EventStartDate", stdat_click.getText().toString());
                    objcalendaer.put("EventEndDate", end_click.getText().toString());
                    objcalendaer.put("Status", "1");
                    objcalendaer.put("CategoryID",  catid_click.getText().toString());

                    if(context instanceof Calendaractivity){
                        ((Calendaractivity) context).stratExamCompletedPopup("Calendar@"+objcalendaer.toString());
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }




                    //showPopup(msg.getText().toString(),stdname.getText().toString(),content.getText().toString(),dat.getText().toString());
            }
        });


        return convertView;
    }
    public String formatdate(String fdate)
    {
        String datetime=null;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat d= new SimpleDateFormat("dd-MMM-yyyy");
        try {
            Date convertedDate = inputFormat.parse(fdate);
            datetime = d.format(convertedDate);

        }catch (ParseException e)
        {

        }
        return  datetime;


    }
    //ViewHolder inner class
    private class ViewHolder {


        TextView msg, event, desc, dat,catid;
        RelativeLayout rel;
        ImageView cateicon;
        Button send;

        //goneTextView
        TextView stdat,enddat;
        public ViewHolder(View view) {

            event = (TextView) view.findViewById(R.id.event);
            desc = (TextView) view.findViewById(R.id.desc);
            cateicon = (ImageView) view.findViewById(R.id.cateicon);
            dat = (TextView) view.findViewById(R.id.dat);
            catid = (TextView) view.findViewById(R.id.catid);
            send = (Button) view.findViewById(R.id.send);


            stdat = (TextView) view.findViewById(R.id.stdat);
            enddat = (TextView) view.findViewById(R.id.enddat);
            // rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }


}