package calendar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;

import Utilities.DatabaseHandler;
import helper.RoundedImageView;

/**
 * Custom list adapter, implementing BaseAdapter
 */
public class FirstListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList< helper.Calendarpojo> items;
    DatabaseHandler obj;

    public FirstListAdapter(Context context, ArrayList< helper.Calendarpojo> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total item in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns the item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(context).inflate(R.layout.category_item, parent, false);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);


        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        helper.Calendarpojo currentItem = (helper.Calendarpojo ) getItem(position);


       /* if(currentItem.getCalendarname().equalsIgnoreCase("holiday")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mholiday);
        }
        if(currentItem.getCalendarname().equalsIgnoreCase("Competition")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mcompetition);
        }
        if(currentItem.getCalendarname().equalsIgnoreCase("event")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mevent);
        }
        if(currentItem.getCategory().equalsIgnoreCase("exam")){
            viewHolder.cateicon.setBackgroundResource(R.drawable.mexam);
        }*/

        viewHolder.stdname.setText(currentItem.getCalendarname().toUpperCase());
        try
        {

            setbackground(viewHolder.cateicon,currentItem.getCalendarimage());
        }
        catch (Exception e)
        {

        }
       /* if(currentItem.getCategory().equalsIgnoreCase("holiday")){
            viewHolder.stdname.setText("Holiday");
        }
        if(currentItem.getCategory().equalsIgnoreCase("Competition")){
            viewHolder.stdname.setText("Competition");
        }
        if(currentItem.getCategory().equalsIgnoreCase("event")){
            viewHolder.stdname.setText("Event");
        }
        if(currentItem.getCategory().equalsIgnoreCase("exam")){
            viewHolder.stdname.setText("Exam");
        }*/
        //viewHolder.stdname.setText(currentItem.getCategory());



      //  viewHolder.stdname.setText(currentItem.get());

      /*  viewHolder.rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //showPopup(msg.getText().toString(),stdname.getText().toString(),content.getText().toString(),dat.getText().toString());
            }
        });
*/

        return convertView;
    }

    //ViewHolder inner class
    private class ViewHolder {

        TextView comid;
        TextView msg, stdname, content, dat;
        RelativeLayout rel;
        ImageView cateicon;

        public ViewHolder(View view) {

            stdname = (TextView) view.findViewById(R.id.category);
            content = (TextView) view.findViewById(R.id.id);
            cateicon = (ImageView) view.findViewById(R.id.cateicon);
           // rel = (RelativeLayout) view.findViewById(R.id.rel);
        }
    }

    void setbackground1(RoundedImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }
}