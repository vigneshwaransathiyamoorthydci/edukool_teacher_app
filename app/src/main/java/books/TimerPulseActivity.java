package books;




import android.app.Activity;
        import android.app.Dialog;
        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.content.IntentFilter;
        import android.content.SharedPreferences;
        import android.graphics.drawable.ColorDrawable;
        import android.os.Bundle;
        import android.os.CountDownTimer;
        import android.view.View;
        import android.view.Window;
        import android.view.WindowManager;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;

        import com.dci.edukool.teacher.R;
        import com.github.anastr.flattimelib.FlatClockView;

        import java.util.ArrayList;
        import java.util.List;

        import Utilities.Utilss;
        import connection.MultiThreadChatServerSync;
        import connection.clientThread;

/**
 * Created by abimathi on 30-May-17.
 */
public class TimerPulseActivity extends Activity {
    TextView timer;
    ImageView close;
    public static List<Integer> questionID = new ArrayList<Integer>();

    RelativeLayout bottom;
    TextView newquizz;
    // ImageView start;
    //ImageView reset;
    TextView timertext2;
    TextView timertext3;


    BroadcastReceiver startquizz;
    CountDownTimer timercount;
    BroadcastReceiver closealert;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    TextView newquiz,semicolon1,semicolon2;
    Utilss utils;
    FlatClockView clockImageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);
        setContentView(R.layout.timer_buzz);
        Intent intent1 = getIntent();
        utils=new Utilss(this);
        clockImageView=(FlatClockView)findViewById(R.id.clock_face) ;
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        edit=pref.edit();
        timer= (TextView) findViewById(R.id.timertext);
        semicolon1= (TextView) findViewById(R.id.semicolon1);
        semicolon2= (TextView) findViewById(R.id.semicolon2);
        newquiz= (TextView) findViewById(R.id.newquiz);
        timertext2= (TextView) findViewById(R.id.timertext2);
        timertext3= (TextView) findViewById(R.id.timertext3);
        close = (ImageView) findViewById(R.id.close);
        bottom= (RelativeLayout) findViewById(R.id.bottomlayout);
        newquizz = (TextView) findViewById(R.id.newquiz);
        // start = (ImageView) findViewById(R.id.start);
        //  reset= (ImageView) findViewById(R.id.reset);
        bottom.setEnabled(false);
        timercreate();
        timercount.start();
        edit.putBoolean("quizz", false);
        edit.commit();
        clockImageView.setTime("00:00:00");
       /* close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });*/
        newquizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newquizz.setEnabled(false);
                //img_start.setEnabled(true);
                bottom.setEnabled(true);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



//        img_start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                stratExamCompletedPopup("");
//                // img_start.setEnabled(false);
//
//
//           /*   timercount=  new CountDownTimer(35000, 1) {
//
//                    public void onTick(long millisUntilFinished) {
//                        //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
//                        //here you can have your logic to set text to edittext
//                        timer.setText("" + millisUntilFinished / 1000);
//                        timertext3.setText("" + millisUntilFinished % 1000);
//
//
//                    }
//
//                    public void onFinish() {
//                        img_start.setEnabled(true);
//                        timer.setText("00");
//                        timertext3.setText("00");
//                        //finish();
//                    }
//
//                }.start();*/
//
//
//            }
//        });

        bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String senddata="Quiz@reset";
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();

                finish();
            }
        });


/*
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///dialog.dismiss();
            }
        });*/
        utils.setTextviewtypeface(3,semicolon1);
        utils.setTextviewtypeface(3,semicolon2);
        utils.setTextviewtypeface(3,timer);
        utils.setTextviewtypeface(3,timertext2);
        utils.setTextviewtypeface(3,timertext3);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startquizz=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //img_start.setEnabled(false);

                timercount.start();
              /*  if(buzz==null)
                {
                    buzz=new Buzzerclass();
                }
                else
                {
                    try
                    {
                        buzz.closesocket();
                        buzz=new Buzzerclass();
                    }
                    catch (Exception e)
                    {
                        buzz=new Buzzerclass();
                    }
                }*/


            }
        };
        IntentFilter quizfilter=new IntentFilter("quizz");
        registerReceiver(startquizz,quizfilter);

        closealert=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                finish();
            }
        };
        IntentFilter fileter=new IntentFilter("closealert");
        registerReceiver(closealert,fileter);
    }
    void timercreate()
    {

        timercount=  new CountDownTimer(5000, 1) {

            public void onTick(long millisUntilFinished) {
                //timer.setText("00:"+millisUntilFinished / 1000+":"+millisUntilFinished%1000);
                //here you can have your logic to set text to edittext
                timer.setText("" + millisUntilFinished / 1000);
                timertext3.setText("" + millisUntilFinished % 1000);


            }

            public void onFinish() {
                try {
                   // img_start.setEnabled(true);
                    timer.setText("00");
                    timertext3.setText("00");
                    finish();
                    Intent myIntent = new Intent(TimerPulseActivity.this, PulseBarActivity.class);

                    startActivity(myIntent);
                }
                catch (Exception e){
                    finish();
                    Intent myIntent = new Intent(TimerPulseActivity.this, PulseBarActivity.class);

                    startActivity(myIntent);
                    e.printStackTrace();
                }

            }

        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(startquizz);
        unregisterReceiver(closealert);
    }


    public void stratExamCompletedPopup(String sendata) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        TextView content= (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close= (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                // finish();
            }
        });
        title.setText("QUIZZ");
        content.setText("Share Quizz to?");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // File file=new File(video.getVideopath());

                //  String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();
                String senddata="Quiz@Open";
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();
                Intent in=new Intent("quizz");
                sendBroadcast(in);
              /*  if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  File file=new File(video.getVideopath());

             /*   if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }*/

                // String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Academic"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                String senddata="Quiz@Open";

                Intent in=new Intent(TimerPulseActivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("Title","QUIZZ");
                in.putExtra("senddata",senddata);
                startActivity(in);


                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }
}

