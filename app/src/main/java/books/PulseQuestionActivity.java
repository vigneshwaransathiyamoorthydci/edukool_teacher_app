package books;
/**
 * Created by pratheeba on 5/26/2017.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.R;
import org.json.JSONArray;
import Utilities.DatabaseHandler;
import Utilities.Utilss;
import pulseexam.PulseExamPojo;

public class PulseQuestionActivity extends BaseActivity {
    private ListView mCompleteListView;
    private ArrayList<PulseExamPojo> mItems;
    private List<Integer> mQuestionId;
    private Pulse mListAdapter;
    DatabaseHandler db;
    ImageView close;
    BroadcastReceiver questionreceiver1;
    TextView create,choose_pulse,class_name;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    Utilss utils;
    ImageView back;

    TextView title,tv_english,tv_maths,tv_science,tv_tamil,tv_soscience;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
       // setContentView(R.layout.pulse_main_layout);
        this.setFinishOnTouchOutside(false);
        utils=new Utilss(PulseQuestionActivity.this);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        edit = pref.edit();
        title=(TextView) findViewById(R.id.title);
        create=(TextView) findViewById(R.id.create);
        choose_pulse=(TextView) findViewById(R.id.choose_pulse);
        title.setText("PUlse");
        back=(ImageView) findViewById(R.id.back);
        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_english=(TextView) findViewById(R.id.tv_english);
        tv_maths=(TextView) findViewById(R.id.tv_maths);
        tv_science=(TextView) findViewById(R.id.tv_science);
        tv_tamil=(TextView) findViewById(R.id.tv_tamil);
        class_name = (TextView)findViewById(R.id.class_name);
        tv_soscience=(TextView) findViewById(R.id.tv_soscience);
        utils.setTextviewtypeface(3,title);
        utils.setTextviewtypeface(5,tv_english);
        utils.setTextviewtypeface(5,tv_maths);
        utils.setTextviewtypeface(5,tv_science);
        utils.setTextviewtypeface(5,tv_tamil);
        utils.setTextviewtypeface(5,tv_soscience);
        utils.setTextviewtypeface(3,create);
        utils.setTextviewtypeface(5,choose_pulse);
        pulse_lay.setBackgroundColor(getResources().getColor(R.color.oldbg));
        pulse_icon.setImageDrawable(getResources().getDrawable(R.drawable.cardiogram));
        tv_pulse.setTextColor(getResources().getColor(R.color.white));

        class_name.setText( pref.getString("classname", ""));


        //db=new DatabaseHandler(this);
        db= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
       // db=new DatabaseHandler(this);
        mItems = new ArrayList<PulseExamPojo>();
        mListAdapter = new Pulse(this, mItems,pref);
        initViews();

        mCompleteListView.setAdapter(mListAdapter);

        close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSubjectbackround();
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void setSubjectbackround(){
        if (pref.getString("subjectname","")!=null&&!pref.getString("subjectname","").equals("")){
            String subname=pref.getString("subjectname","").toLowerCase();

            switch (subname){
                case "english":
                    tv_english.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_english.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "maths":
                    tv_maths.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_maths.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "tamil":
                    tv_tamil.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_tamil.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "sceince":
                    tv_science.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_science.setTextColor(getResources().getColor(R.color.white));
                    break;
                case "social science":
                    tv_soscience.setBackground(getResources().getDrawable(R.drawable.subject_bg));
                    tv_soscience.setTextColor(getResources().getColor(R.color.white));
                    break;
            }
        }

    }

    @Override
    public int getlayout() {
        return R.layout.pulse_main_layout;
    }

    private void initViews() {
        mCompleteListView = (ListView) findViewById(R.id.completeList);
        close= (ImageView) findViewById(R.id.close);
        create= (TextView) findViewById(R.id.create);
        addItemsToList();


        create.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               // finish();
                Intent myIntent = new Intent(PulseQuestionActivity.this, CreatePulseActivity.class);
                startActivity(myIntent);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        questionreceiver1=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                try {
                    finish();
                    // loadQuestions();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        };
        IntentFilter intent1=new IntentFilter("PulseStart");
        registerReceiver(questionreceiver1, intent1);




    }

    private void addItemsToList() {
        JSONArray optionArray = new JSONArray();
        List<PulseExamPojo> pulsedetailsList = db.getAllPulseExamDetails();
        for (PulseExamPojo cn : pulsedetailsList) {
            String log = "PulseQuestionID: " + cn.getPulseQuestionID() + " ,PulseQuestion: " + cn.getPulseQuestion() + " ,PulseType: " +
                    cn.getPulseType() + " ,AnswerOption_A: " +
                    cn.getAnswerOption_A() + " ,AnswerOption_B: " + cn.getAnswerOption_B() + " ,AnswerOption_C: " +
                    cn.getAnswerOption_C() + " ,AnswerOption_D: " + cn.getAnswerOption_D() + " ,CorrectAnswerOption: " + cn.getCorrectAnswerOption() +
                    " ,VersionPulse " +
                    cn.getVersionPulse() + " ,StaffIDPluse: " + cn.getStaffIDPluse() + " ,SchoolIDPulse: " + cn.getSchoolIDPulse() +
                    " ,IsActivePulse: " + cn.getIsActivePulse() + " ,CreatedOnPluse: " + cn.getCreatedOnPluse()+ " ,UpdatedOn: " + cn.getUpdatedOn();

            optionArray.put(cn.getAnswerOption_A());
            optionArray.put(cn.getAnswerOption_B());
            optionArray.put(cn.getAnswerOption_C());
            optionArray.put(cn.getAnswerOption_D());

            // Writing Contacts to log
            Log.d("PulseQuestionID: ", log);
            mItems.add(new PulseExamPojo(cn.getPulseQuestionID(),cn.getPulseQuestion(),
                    cn.getAnswerOption_A(),cn.getAnswerOption_B(),
                    cn.getAnswerOption_C(),cn.getAnswerOption_D(),
                    cn.getCorrectAnswerOption(),cn.getCreatedOnPluse(),
                    cn.getUpdatedOn()));

        }


        mListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(questionreceiver1);
    }


}

