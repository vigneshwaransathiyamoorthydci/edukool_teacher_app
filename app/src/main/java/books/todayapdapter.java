package books;

/**
 * Created by abimathi on 26-May-17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import helper.RoundedImageView;
import helper.Studentdetails;


/**
 * Created by iyyapparajr on 5/14/2017.
 */

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class todayapdapter extends ArrayAdapter<Studentdetails> {

    private ArrayList<Studentdetails> studentinfo;
    Context con;
    SharedPreferences pref;
    DatabaseHandler db;
    Utilss utils;

    // CoolReader mactivity;

    public todayapdapter(Context context, int textViewResourceId,
                          ArrayList<Studentdetails> studentinfo,SharedPreferences pref) {
        super(context, textViewResourceId, studentinfo);
        this.studentinfo = studentinfo;
        this.pref=pref;

        con=context;
        utils=new Utilss((Activity) con);
        // this.studentinfo.addAll(studentinfo);
       // db=new DatabaseHandler((Activity)con);

        db= new DatabaseHandler((Activity)con,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

    }

    private class ViewHolder {

        RoundedImageView studentimage;
        TextView studentrollnumber;
        TextView studentname;
       /* TextView overallcredits;
        TextView obtaincredits;
        LinearLayout back;
        TextView tilldate;
        TextView thismonth;*/
        RelativeLayout parentlayout;
        LinearLayout background;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.todayitemnew, null);

            holder = new ViewHolder();
            holder.studentimage= (RoundedImageView) convertView.findViewById(R.id.studentpoto);
            holder.studentname= (TextView) convertView.findViewById(R.id.name);
            holder.studentrollnumber= (TextView) convertView.findViewById(R.id.rollno);
            holder.background= (LinearLayout) convertView.findViewById(R.id.background);
            utils.setTextviewtypeface(5,holder.studentname);

            utils.setTextviewtypeface(5,holder.studentrollnumber);
           /* holder.overallcredits= (TextView) convertView.findViewById(R.id.totalcredits);
            holder.obtaincredits= (TextView) convertView.findViewById(R.id.obtainedcredits);
            holder.tilldate= (TextView) convertView.findViewById(R.id.tildate);
            holder.thismonth= (TextView) convertView.findViewById(R.id.thismonth);
            holder.background= (LinearLayout) convertView.findViewById(R.id.background);*/
            // holder.back= (LinearLayout) convertView.findViewById(R.id.back);

            convertView.setTag(holder);


        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }


        Studentdetails studentdetails=studentinfo.get(position);


        try
        {
            setbackground(holder.studentimage,studentdetails.getPhotoFilename());
        }
        catch (Exception e)
        {

        }
        if(position%2==0)
        {
            holder.background.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.white)));

        }
        else
        {
            holder.background.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.white)));

        }
        //holder.studentname.setText(studentdetails.getFirstName());
        //holder.studentrollnumber.setText(studentdetails.getRollNo().toString());
       // holder.overallcredits.setText(""+studentdetails.getTeachercredits());
        //holder.obtaincredits.setText(""+studentdetails.getStudentcredits());
        holder.background.removeAllViews();
        LayoutInflater inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        final View hiddenInfo5 = inflater.inflate(
                R.layout.imagelayout, null, false);
        //TextView text= (TextView) hiddenInfo5.findViewById(R.id.dyn);
        //text.setText(studentdetails.getFirstName());
       RoundedImageView imageeview= (RoundedImageView) hiddenInfo5.findViewById(R.id.studentpoto);
        setbackground(imageeview,studentdetails.getPhotoFilename());
        LinearLayout.LayoutParams imageparame = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo5.setLayoutParams(imageparame);
        holder.background.addView(hiddenInfo5);


        final View hiddenInfo3 = inflater.inflate(
                R.layout.todaytimedynamic, null, false);
        TextView text= (TextView) hiddenInfo3.findViewById(R.id.dyn);
        if(!studentdetails.getRollNo().equalsIgnoreCase("0")) {
            text.setText(studentdetails.getRollNo());
        }
        else
        {
            text.setText("-");

        }

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo3.setLayoutParams(param);
        holder.background.addView(hiddenInfo3);


       // LayoutInflater inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        final View hiddenInfo4 = inflater.inflate(
                R.layout.todaytimedynamic, null, false);
        TextView textname= (TextView) hiddenInfo4.findViewById(R.id.dyn);
        textname.setText(studentdetails.getFirstName());

        LinearLayout.LayoutParams paramname = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo4.setLayoutParams(paramname);
        holder.background.addView(hiddenInfo4);


        for (int f = 0; f < studentdetails.getListofpresent().size(); f++) {

            final View hiddenInfo2 = inflater.inflate(
                    R.layout.todaytimedynamic, null, false);
            TextView rollname= (TextView) hiddenInfo2.findViewById(R.id.dyn);
            rollname.setText(studentdetails.getListofpresent().get(f));
            LinearLayout.LayoutParams paramroll = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );

            hiddenInfo2.setLayoutParams(paramroll);
          holder.background.addView(hiddenInfo2);


        }



        double tildat=((double)studentdetails.getStudentcredits()/(double)studentdetails.getTeachercredits())*100;
       // double month=((double)studentdetails.getThismonthcreditforstudent()/(double)studentdetails.getThismonthteachercredit())*100;

        String til = String.format("%.2f", tildat);
        //String mon = String.format("%.2f", month);
        final View hiddenInfo2 = inflater.inflate(
                R.layout.todaytimedynamic, null, false);
        TextView thismonth= (TextView) hiddenInfo2.findViewById(R.id.dyn);
        thismonth.setText(til+"%");
        LinearLayout.LayoutParams paramthismonth = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo2.setLayoutParams(paramthismonth);
        holder.background.addView(hiddenInfo2);

        //holder.thismonth.setText(""+tildat+ "%");
        //holder.tilldate.setText(""+month+ "%");


      /*  if(studentinfo.get(position).isVisibornot())
        {
            convertView.setVisibility(View.VISIBLE);
        }
        else
        {
            convertView.setVisibility(View.G);
        }*/

        //Toast.makeText(con,studentinfo.get(position).getStudentname(),Toast.LENGTH_SHORT).show();
        //Toast.makeText(con,details.getPhotoFilename(),Toast.LENGTH_SHORT).show();

        // holder.code.setText(" (" +  country.getCode() + ")");
      /*  holder.name.setText(videoname.getName());

        holder.name.setTag(videoname);
*/


        return convertView;

    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch ( Exception e)
        {

        }
    }

}