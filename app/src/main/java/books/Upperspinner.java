package books;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;

import helper.Subjectnameandid;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Upperspinner extends ArrayAdapter {

    private Context context;
    private ArrayList<Subjectnameandid> itemList;
    public Upperspinner(Context context, int textViewResourceId,ArrayList<Subjectnameandid> itemList) {

       super(context, textViewResourceId,itemList);
        this.context=context;
        this.itemList=itemList;
    this.itemList=itemList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
       //v.setTypeface(myTypeFace);
        make.setText(itemList.get(position).getName());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        make.setText(itemList.get(position).getName());

        return row;
    }

}