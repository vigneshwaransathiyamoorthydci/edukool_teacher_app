package books;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Utilities.Utilss;
import Utilities.*;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Attendancepojo;
import helper.RoundedImageView;
import helper.Studentdetails;

/**
 * Created by iyyapparajr on 5/14/2017.
 */
public class Todayattendance extends Activity {

    RoundedImageView profileimage;
    TextView batchname,attendance;
    ListView studentlist;
    EditText search;
    TextView searchheading;
    TextView studentphoto;
    TextView studentrollno;
    TextView name;

    TextView totalcredits;

    TextView obtained;
    TextView month;
    TextView tildate;
    ImageView handraise;

    ListView attendancreditslist;
    ArrayList<Studentdetails> student;
    ArrayList<Studentdetails> student1;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    DatabaseHandler dp;
    Attendancedb db;
    int teachercredits;
    int thismonthcredits;
    ImageView back;

    todayapdapter adapter;
    LinearLayout header;

ImageView dnd;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.todayattendance);
      //  dp=new DatabaseHandler(this);
       // db=new Attendancedb(this);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        dp= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
       db= new Attendancedb(this,pref.getString("attendancedb",""),Attendancedb.DATABASE_VERSION);
        profileimage= (RoundedImageView) findViewById(R.id.profileimage);
        batchname= (TextView) findViewById(R.id.textView);
        attendance= (TextView) findViewById(R.id.classname);
        searchheading= (TextView) findViewById(R.id.creditsubject);
        search= (EditText) findViewById(R.id.creditsearch);
        studentphoto= (TextView) findViewById(R.id.studentpoto);
        name= (TextView) findViewById(R.id.studentname);
        back= (ImageView) findViewById(R.id.back);
         header= (LinearLayout) findViewById(R.id.header);
        studentrollno= (TextView) findViewById(R.id.rollno);

        month= (TextView) findViewById(R.id.thismonth);

        tildate= (TextView) findViewById(R.id.tildate);

        totalcredits= (TextView) findViewById(R.id.totalcredits);

        obtained= (TextView) findViewById(R.id.obtainedcredits);
        studentlist= (ListView) findViewById(R.id.creidtlist);


        handraise= (ImageView) findViewById(R.id.handrise);

        setbackground(profileimage, pref.getString("image", ""));
        batchname.setText(pref.getString("classname", ""));
        searchheading.setText(pref.getString("subjectname", ""));
        dnd= (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pref.getBoolean("dnd", false)) {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata = senddata + "@false";
                    dnd.setImageResource(R.drawable.inactive);


                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                } else {
                    String senddata = "DND";

                    senddata = senddata + "@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    dnd.setImageResource(R.drawable.active);

                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
            }
        });
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                ArrayList<Studentdetails> searchstring = new ArrayList<Studentdetails>();
                if (student.size() > 0) {
                    for (int i = 0; i < student.size(); i++) {
                        if (student.get(i).getFirstName().toLowerCase().contains(s.toString().toLowerCase())) {
                            searchstring.add(student.get(i));
                        }
                    }
                    if (searchstring.size() > 0) {
                        adapter = new todayapdapter(Todayattendance.this, R.layout.creditsitem, searchstring, pref);
                        attendancreditslist.setAdapter(adapter);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

       /* for(int f=0; f<12; f++) {

            final View hiddenInfo2 = getLayoutInflater().inflate(
                    R.layout.todaytimedynamic, null, false);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            hiddenInfo2.setLayoutParams(param);
            header.addView(hiddenInfo2);


        }
        final View hiddenInfo2 = getLayoutInflater().inflate(
                R.layout.todaytimedynamic, null, false);
        TextView text= (TextView) hiddenInfo2.findViewById(R.id.dyn);
        text.setText("THIS MONTH");
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo2.setLayoutParams(param);
        header.addView(hiddenInfo2);*/



        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<Studentdetails> searchstring = new ArrayList<Studentdetails>();
                if (student.size() > 0) {
                    for (int i = 0; i < student.size(); i++) {
                        if (student.get(i).getFirstName().contains(s.toString())) {
                            searchstring.add(student.get(i));
                        }
                    }
                    if (searchstring.size() > 0) {
                        adapter = new todayapdapter(Todayattendance.this, R.layout.creditsitem, searchstring, pref);
                        attendancreditslist.setAdapter(adapter);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(Todayattendance.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(Todayattendance.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        attendancreditslist= (ListView) findViewById(R.id.creidtlist);


        teachercredits=db.gettotalcreditsforsubject(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")));

        /*thismonthcredits=db.gettotalcreditsforsubjectthismonth(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")), Integer.parseInt(Service.currentmonth()), Integer.parseInt(Service.getyear()));
      */

        student1=dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")),  Integer.parseInt(pref.getString("staffid", "0")));

        student=new ArrayList<>();
        for(int j=0; j<student1.size(); j++)
        {
            Studentdetails studentdetails=student1.get(j);

            boolean addtostudent=false;
           /* String rooms[]=studentdetails.getRoominfo().split(",");

            for(int r=0; r<rooms.length; r++)
            {
                if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                {
                    addtostudent=true;
                    break;
                }
            }
            if(addtostudent)
            {*/
                student.add(studentdetails);
           // }


        }

       // student= dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")),  Integer.parseInt(pref.getString("staffid", "0")));


        ArrayList<Attendancepojo> pojo=db.todayatttendance(Integer.parseInt(pref.getString("batchid", "0")),

                Integer.parseInt(pref.getString("staffid", "0")),
                getIntent().getStringExtra("date"));


        header.removeAllViews();
        final View hiddenInfo3 =  getLayoutInflater().inflate(
                R.layout.todaytimedynamic, null, false);
        TextView textofstudent= (TextView) hiddenInfo3.findViewById(R.id.dyn);
        textofstudent.setText("STUDENT PHOTO");

        LinearLayout.LayoutParams studentparam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo3.setLayoutParams(studentparam);
        header.addView(hiddenInfo3);


        // LayoutInflater inflater = (LayoutInflater) con.getSystemService( Context.LAYOUT_INFLATER_SERVICE );

        final View hiddenInfo4 =  getLayoutInflater().inflate(
                R.layout.todaytimedynamic, null, false);
        TextView textname1= (TextView) hiddenInfo4.findViewById(R.id.dyn);
        textname1.setText("ROLL NO");

        LinearLayout.LayoutParams paramname = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo4.setLayoutParams(paramname);
        header.addView(hiddenInfo4);

        final View hiddenInfo5 =  getLayoutInflater().inflate(
                R.layout.todaytimedynamic, null, false);
        TextView teextname= (TextView) hiddenInfo5.findViewById(R.id.dyn);
        teextname.setText("NAME");

        LinearLayout.LayoutParams paramofname = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo5.setLayoutParams(paramofname);
        header.addView(hiddenInfo5);


        for(int f=0; f<pojo.size(); f++) {

            final View hiddenInfo2 = getLayoutInflater().inflate(
                    R.layout.todaytimedynamic, null, false);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );
            TextView text= (TextView) hiddenInfo2.findViewById(R.id.dyn);

            try {
                Date date = null;
                SimpleDateFormat toFullDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date = toFullDate.parse(pojo.get(f).getAttendanceDateTime());
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                String shortTimeStr = sdf.format(date);
                text.setText(shortTimeStr);
                System.out.println(shortTimeStr);
            } catch (Exception e) {
                // To change body of catch statement use File | Settings | File Templates.
                e.printStackTrace();
            }
           // text.setText("THIS MONTH");

            hiddenInfo2.setLayoutParams(param);
            header.addView(hiddenInfo2);


        }
        final View hiddenInfo2 = getLayoutInflater().inflate(
                R.layout.todaytimedynamic, null, false);
        TextView text= (TextView) hiddenInfo2.findViewById(R.id.dyn);
        text.setText("THIS MONTH");
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                1.0f
        );
        hiddenInfo2.setLayoutParams(param);
        header.addView(hiddenInfo2);


        for(int i=0; i<student.size(); i++)
        {

            student.get(i).setTeachercredits(teachercredits);
            int studentcredits=db.gettotalcreditsforsubjectstudent(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")),student.get(i).getStudentID());


                   /* Service.attendenacedate()*/
          /*  int thisstudentcredits=db.gettotalcreditsforsubjectstudentthismonth(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")), student.get(i).getStudentID(), Integer.parseInt(Service.currentmonth()), Integer.parseInt(Service.getyear()));
       */     student.get(i).setStudentcredits(studentcredits);
            if(pojo.size()>0)
            {

                ArrayList<String>listofpreseent=new ArrayList<>();

                for(int p=0; p<pojo.size(); p++)
                {
                    String present=db.getpresentornot(student.get(i).getStudentID(),pojo.get(p).getAttendanceDateTime());
                    listofpreseent.add(present);

                }
                student.get(i).setListofpresent(listofpreseent);

            }


           // student.get(i).setThismonthteachercredit(thismonthcredits);
          //  student.get(i).setThismonthcreditforstudent(thisstudentcredits);

        }
        adapter=new todayapdapter(Todayattendance.this,R.layout.creditsitem,student,pref);
        attendancreditslist.setAdapter(adapter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        if(pref.getBoolean("dnd",false))
        {
            dnd.setImageResource(R.drawable.active);

        }
        else
        {
            dnd.setImageResource(R.drawable.inactive);

        }
        if(LoginActivity.handraise.size()>0)
        {
            handraise.setImageResource(R.drawable.handraiseenable);

            // Utils.Listpopup(BookBinActivity.this);
        }
        else
        {                    handraise.setImageResource(R.drawable.handrise);

            //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
        }

    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }
}
