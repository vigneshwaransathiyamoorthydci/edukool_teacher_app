package books;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.dci.edukool.teacher.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Utilities.Utilss;


public class BirthdayAdapter extends BaseAdapter {
    Utilss utils;
    public BirthdayAdapter(ArrayList<helper.Studentdetails> studentDetailsList, Context context) {
        this.studentDetailsList = studentDetailsList;
        this.context = context;
        utils=new Utilss((Activity) context);
    }

    ArrayList<helper.Studentdetails> studentDetailsList;
    Context context;

    @Override
    public int getCount() {
        return studentDetailsList.size();
    }

    @Override
    public helper.Studentdetails getItem(int position) {
        return studentDetailsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.birthydayitem, null);
        }
        ImageView studentimage= (ImageView) convertView.findViewById(R.id.studentimage);
        TextView studenttext= (TextView) convertView.findViewById(R.id.studentname);
        TextView studentage=(TextView)convertView.findViewById(R.id.studentage);
        utils.setTextviewtypeface(3,studentage);
        utils.setTextviewtypeface(5,studenttext);
        studenttext.setText(studentDetailsList.get(position).getFirstName());
        studentage.setText("Age :"+getAge(studentDetailsList.get(position).getDOB()));
        Log.d("ONEEE","profilepic"+studentDetailsList.get(position).getPhotoFilename());
        try {
                File imgfile = new File(studentDetailsList.get(position).getPhotoFilename());
                Picasso.with(context).load(imgfile).into(studentimage);
        }
        catch (Exception e)
        {

        }
        return convertView;
    }

    public int getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return age;
    }

}
