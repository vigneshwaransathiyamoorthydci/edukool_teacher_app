package books;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import Utilities.DatabaseHandler;
import Utilities.Service;
import connection.Individualshare;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.RoundedImageView;
import helper.Studentinfo;

/**
 * Created by abimathi on 16-May-17.
 */
public class Attendancewithcheckparentlayout extends Activity {


    RoundedImageView profileimage;
    TextView batchname,attendance;
    GridView attendancegridview;
    ImageView back;
    EditText search;
    TextView submit;
    ArrayList<Studentinfo> info;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    // Attendanceadapter adapter;
    Attendancewithcheckbox adapter;
    DatabaseHandler dp;
    TextView dateset,datatime;
    ImageView share;
    TextView classname;
    String sendData;
    public  static boolean starttimer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendselectlayout);
        profileimage= (RoundedImageView) findViewById(R.id.profileimage);
        classname= (TextView) findViewById(R.id.secondclassname);
        batchname= (TextView) findViewById(R.id.textView);
        attendance= (TextView) findViewById(R.id.classname);
        dateset= (TextView) findViewById(R.id.setdate);
        search= (EditText) findViewById(R.id.search);
        submit= (TextView) findViewById(R.id.absent);
        datatime= (TextView) findViewById(R.id.Settime);
        attendancegridview= (GridView) findViewById(R.id.studentgrid);
        back= (ImageView) findViewById(R.id.back);
        share= (ImageView) findViewById(R.id.share);
        Bundle bundle = getIntent().getExtras();
        sendData = bundle.getString("senddata");
        pref=getSharedPreferences("Teacher", MODE_PRIVATE);
        dp=new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        //  dp=new DatabaseHandler(this);
       // dp.getstudentdetails();
        dateset.setText(Service.currentdate());
        datatime.setText(Service.currenttime());
        edit=pref.edit();
        attendancegridview= (GridView) findViewById(R.id.studentgrid);
        info=new ArrayList<>();
        setbackground(profileimage, pref.getString("image", ""));
        batchname.setText(pref.getString("classname", ""));
        classname.setText(pref.getString("classname",""));
        starttimer=false;

        try
        {
            attendance.setText(getIntent().getStringExtra("Title"));
        }
        catch (Exception e)
        {

        }

        for(int i=0; i< MultiThreadChatServerSync.thread.size(); i++)
        {
            clientThread thread = MultiThreadChatServerSync.thread.get(i);
            info.add(thread.info);

            //info.add(MultiThreadChatServerSync.thread.get(i).);
        }
        //()
        adapter=new Attendancewithcheckbox(this,
                R.layout.takeattendance,info,pref);
        attendancegridview.setAdapter(adapter);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<Integer, Studentinfo>get= Attendancewithcheckbox.send;
                ArrayList<Studentinfo>send=new ArrayList<Studentinfo>();
                if(get.size()>0)
                {
                    if(!get.isEmpty()){

                        Iterator it=get.entrySet().iterator();

                        while(it.hasNext()){

                            Map.Entry obj=(Map.Entry) it.next();
                            System.out.print(obj.getKey()+" ");
                            System.out.println(obj.getValue());
                            Studentinfo info= (Studentinfo) obj.getValue();
                            send.add(info);

                        }
                        new Individualshare(send,sendData).start();
                        Toast.makeText(Attendancewithcheckparentlayout.this,"Shared to Particular Students",Toast.LENGTH_SHORT).show();

                        if(attendance.getText().toString().equalsIgnoreCase("QUIZZ"))
                        {
                            starttimer=true;
                           /* Intent in=new Intent("quizz");
                            sendBroadcast(in);*/
                        }
                        if(attendance.getText().toString().equalsIgnoreCase("Calendar Share"))
                        {
                            Intent in=new Intent("calendarevent");
                            sendBroadcast(in);
                        }
                        finish();
                    }




                }
                else {
                    Toast.makeText(Attendancewithcheckparentlayout.this,"Select atleast one student",Toast.LENGTH_SHORT).show();

                }
            }
        });

    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


}

