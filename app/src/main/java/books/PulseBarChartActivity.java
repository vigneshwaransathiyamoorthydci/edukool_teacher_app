/**
 * Copyright (C) 2009 - 2013 SC 4ViewSoft SRL
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package books;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart.Type;
import org.achartengine.chart.IDemoChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.chart.TimeChart;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.json.JSONArray;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import Utilities.DatabaseHandler;
import pulseexam.PulseExamPojo;
import pulseexam.PulseSurveyPojo;


public class PulseBarChartActivity extends ListActivity {
    private static final int SERIES_NR = 4;
    ArrayList<Integer> studentAns = new ArrayList<Integer>();
    private String[] mMenuText;
    DatabaseHandler db;
    String PulseQuestion;
    String correctA;
    String correctB;
    String correctC;
    String correctD;
    JSONArray optionArray;

SharedPreferences pref;
    private String[] mMenuSummary;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // I know, I know, this should go into strings.xml and accessed using
        // getString(R.string....)
        /*mMenuText = new String[] { "Line chart", "Scatter chart", "Time chart", "Bar chart" };
        mMenuSummary = new String[] { "Line chart with randomly generated values",
                "Scatter chart with randomly generated values",
                "Time chart with randomly generated values", "Bar chart with randomly generated values" };
        setListAdapter(new SimpleAdapter(this, getListValues(), android.R.layout.simple_list_item_2,
                new String[] { IDemoChart.NAME, IDemoChart.DESC }, new int[] { android.R.id.text1, android.R.id.text2 }));*/
       // db=new DatabaseHandler(this);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        //db=new DatabaseHandler(this);
        db= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
        optionArray=  new JSONArray();
        Intent intent1 = getIntent();
        Bundle bundle = intent1.getExtras();
        int SurveyPulseQuestionID= bundle.getInt("SurveyPulseQuestionID");

        List<PulseExamPojo> pulsedetailsList = db.getAllPulseExamDetailsLastUsingId(SurveyPulseQuestionID);
        for (PulseExamPojo cn1 : pulsedetailsList) {


            String log1 = "PulseQuestionID: " + cn1.getPulseQuestionID() + " ,PulseQuestion: " + cn1.getPulseQuestion() + " ,PulseType: " +
                    cn1.getPulseType() + " ,AnswerOption_A: " +
                    cn1.getAnswerOption_A() + " ,AnswerOption_B: " + cn1.getAnswerOption_B() + " ,AnswerOption_C: " +
                    cn1.getAnswerOption_C() + " ,AnswerOption_D: " + cn1.getAnswerOption_D() + " ,CorrectAnswerOption: " + cn1.getCorrectAnswerOption() +
                    " ,VersionPulse " +
                    cn1.getVersionPulse() + " ,StaffIDPluse: " + cn1.getStaffIDPluse() + " ,SchoolIDPulse: " + cn1.getSchoolIDPulse() +
                    " ,IsActivePulse: " + cn1.getIsActivePulse() + " ,CreatedOnPluse: " + cn1.getCreatedOnPluse() + " ,UpdatedOn: " + cn1.getUpdatedOn();
                PulseQuestion =  cn1.getPulseQuestion();
            optionArray.put(cn1.getAnswerOption_A());
            optionArray.put(cn1.getAnswerOption_B());
            //if(!(cn1.getAnswerOption_C().equalsIgnoreCase(" "))){
                optionArray.put(cn1.getAnswerOption_C());

           // }
           // if(!(cn1.getAnswerOption_D().equalsIgnoreCase(" "))){
                optionArray.put(cn1.getAnswerOption_D());

           // }


            // Writing Contacts to log
            Log.d("PulseQuestionID: ", log1);

        }
            List<PulseSurveyPojo> PulseSurveyPojo = db.PulseSurveyUsingExamId(SurveyPulseQuestionID);
        for (PulseSurveyPojo cn : PulseSurveyPojo) {
            String log = "getPulseSurveyID: " + cn.getPulseSurveyID() +
                    " ,getPulseQuestionID: " + cn.getPulseQuestionID() +
                    " ,getOptionA_Percentage: " + cn.getOptionA_Percentage() + " ,getOptionB_Percentage: "
                    + cn.getOptionB_Percentage() + " ,getOptionC_Percentage: " + cn.getOptionC_Percentage()
                    +  " ,getOptionD_Percentage: " + cn.getOptionD_Percentage();
            // Writing Contacts to log
            Log.d("survey: ", log);
            studentAns.add(Math.round(cn.getOptionA_Percentage()*10));
            studentAns.add(Math.round(cn.getOptionB_Percentage())*10);
            studentAns.add(Math.round(cn.getOptionC_Percentage())*10);
            studentAns.add(Math.round(cn.getOptionD_Percentage())*10);
        }

        Intent intent = ChartFactory.getLineChartIntent(this, getDemoDataset(), getDemoRenderer());
        XYMultipleSeriesRenderer renderer = getBarDemoRenderer();
        setChartSettings(renderer);
        intent = ChartFactory.getBarChartIntent(this, getBarDemoDataset(), renderer, Type.DEFAULT);
        startActivity(intent);
    }

    private List<Map<String, String>> getListValues() {
        List<Map<String, String>> values = new ArrayList<Map<String, String>>();
        int length = mMenuText.length;
        for (int i = 0; i < length; i++) {
            Map<String, String> v = new HashMap<String, String>();
            v.put(IDemoChart.NAME, mMenuText[i]);
            v.put(IDemoChart.DESC, mMenuSummary[i]);
            values.add(v);
        }
        return values;
    }


    private XYMultipleSeriesDataset getDemoDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        final int nr = 1;
        Random r = new Random();
        for (int i = 0; i < SERIES_NR; i++) {
            XYSeries series = new XYSeries("Option " + (i + 1));
           /* for (int k = 0; studentAns.size() > nr; k++) {
                series.add(k, studentAns.get(k));
                //series.add(k, 20 + r.nextInt() % 100);
            }*/
           /* for (int k = 0;  k < 1; k++) {
                series.add(k, studentAns.get(k));
            }*/

            dataset.addSeries(series);
        }
        return dataset;
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private XYMultipleSeriesDataset getDateDemoDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        final int nr = 1;
        long value = new Date().getTime() - 3 * TimeChart.DAY;
        Random r = new Random();
        for (int i = 0; i < SERIES_NR; i++) {
            TimeSeries series = new TimeSeries("Option " + (i + 1));
            for (int k = 0; k < nr; k++) {
                series.add(new Date(value + k * TimeChart.DAY / 4), 20 + r.nextInt() % 100);
            }
            dataset.addSeries(series);
        }
        return dataset;
    }

    private XYMultipleSeriesDataset getBarDemoDataset() {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        final int nr = 1;
        Random r = new Random();
        try {
            for (int i = 0; i < SERIES_NR; i++) {
                XYSeries series = new XYSeries(optionArray.get(i).toString());

                // CategorySeries series = new CategorySeries("Option " + (i + 1));
           /* for (int k = 0; studentAns.size() < nr; k++) {
                //series.add(100 + r.nextInt() % 100);
               series.add(studentAns.get(k));
            }*/

                for (int k = i; k < (i + 1); k++) {
                    series.add(i, studentAns.get(k));
                }

                dataset.addSeries(series);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return dataset;
    }

    private XYMultipleSeriesRenderer getDemoRenderer() {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(15);
        renderer.setLegendTextSize(15);
        renderer.setPointSize(5f);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setColor(Color.BLUE);
        r.setPointStyle(PointStyle.SQUARE);
        r.setFillBelowLine(true);
        r.setFillBelowLineColor(Color.WHITE);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);
        r = new XYSeriesRenderer();
        r.setPointStyle(PointStyle.CIRCLE);
        r.setColor(Color.GREEN);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);
        r.setColor(Color.MAGENTA);
        r.setPointStyle(PointStyle.SQUARE);
        r.setFillBelowLine(true);
        r.setFillBelowLineColor(Color.WHITE);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);
        r.setColor(Color.YELLOW);
        r.setPointStyle(PointStyle.SQUARE);
        r.setFillBelowLine(true);
        r.setFillBelowLineColor(Color.WHITE);
        r.setFillPoints(true);
        renderer.addSeriesRenderer(r);

        renderer.setAxesColor(Color.DKGRAY);
        renderer.setLabelsColor(Color.LTGRAY);
        return renderer;
    }

    public XYMultipleSeriesRenderer getBarDemoRenderer() {
        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
        renderer.setAxisTitleTextSize(16);
        renderer.setChartTitleTextSize(20);
        renderer.setLabelsTextSize(15);
        renderer.setBarWidth(50f);
        renderer.setLegendTextSize(15);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        XYSeriesRenderer r = new XYSeriesRenderer();
        r.setColor(Color.BLUE);
        renderer.addSeriesRenderer(r);
        r = new XYSeriesRenderer();
        r.setColor(Color.RED);
        renderer.addSeriesRenderer(r);
        r = new XYSeriesRenderer();
        r.setColor(Color.GREEN);
        renderer.addSeriesRenderer(r);
        r = new XYSeriesRenderer();
        r.setColor(Color.MAGENTA);
        renderer.addSeriesRenderer(r);
        return renderer;
    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onResume();
    }

    private void setChartSettings(XYMultipleSeriesRenderer renderer) {
        renderer.setChartTitle(PulseQuestion);
        renderer.setXTitle("Options");
        renderer.setYTitle("Student Answer");
        renderer.setXAxisMin(0.5);
        renderer.setXAxisMax(10.5);
        renderer.setYAxisMin(0);
        renderer.setYAxisMax(210);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        switch (position) {
            case 0:
                Intent intent = ChartFactory.getLineChartIntent(this, getDemoDataset(), getDemoRenderer());
                startActivity(intent);
                break;
            case 1:
                intent = ChartFactory.getScatterChartIntent(this, getDemoDataset(), getDemoRenderer());
                startActivity(intent);
                break;
            case 2:
                intent = ChartFactory.getTimeChartIntent(this, getDateDemoDataset(), getDemoRenderer(), null);
                startActivity(intent);
                break;
            case 3:
                XYMultipleSeriesRenderer renderer = getBarDemoRenderer();
                setChartSettings(renderer);
                intent = ChartFactory.getBarChartIntent(this, getBarDemoDataset(), renderer, Type.DEFAULT);
                startActivity(intent);
                break;
        }
    }
}