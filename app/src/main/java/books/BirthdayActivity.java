package books;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import adapter.AdapterBirthDay;
import connection.Examzipfile;
import connection.Filesharingtoclient;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Subjectnameandid;
import helper.Tablecontent;
import models.BirthdayTestData;

public class BirthdayActivity extends BaseActivity {


    public ArrayList<helper.Studentdetails> studentdetails;
    public ArrayList<helper.Studentdetails> birthydaystudentdetails;
    LinearLayout hlvCustomList;
    // ListView shelflistView;
    GridView bookbinlist;
    List<String> li;
    Multimediaadapter dataAdapter = null;
    ArrayList<Videoname> videos;
    ImageView exit;
    ImageView back;
    Filesharingtoclient startshare;
    Utilss utils;
    //ArrayList<>bookshelf;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    View header;
    DatabaseHandler dp;
    ArrayList<Tablecontent> table;
    GridView listView;
    ArrayList<Subjectnameandid> subjectname;
    ImageView profilImageView;
    TextView classname, bookheader;
    ImageView handraise;
    Examzipfile zipfile;
    ImageView dnd, down;
    String academicpath, refpath;
    TextView dateTextview, title;
    BirthdayAdapter birthdayadapter;
    GridView birthdayGrid;
    TextView today,this_week,next_week;
    private int mQuestionIndex = 0;
    RecyclerView recyclerView;
    //   SelfTestQuestionAdapter selfevalutation_adapter;
    private Button layoutinputButton;

    public List<BirthdayTestData> getdata() {
        List<BirthdayTestData> list = new ArrayList<>();
        BirthdayTestData data = new BirthdayTestData();
        data.setData("Sudha");
        list.add(data);

        data = new BirthdayTestData();
        data.setData("Ram");
        list.add(data);
        data = new BirthdayTestData();
        data.setData("Dinesh");
        list.add(data);
        data = new BirthdayTestData();
        data.setData("Ravi");
        list.add(data);
        return list;

    }

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        // dp=new DatabaseHandler(this);
        today = (TextView) findViewById(R.id.today );
        this_week = (TextView) findViewById(R.id.this_week );
        next_week = (TextView) findViewById(R.id.next_week );
        hlvCustomList = (LinearLayout) findViewById(R.id.hlvCustomList);
        handraise = (ImageView) findViewById(R.id.handrise);
        title = (TextView) findViewById(R.id.title);
        exit = (ImageView) findViewById(R.id.exit);
        back = (ImageView) findViewById(R.id.back);
        down = (ImageView) findViewById(R.id.down);
        classname= (TextView) findViewById(R.id.class_name);
        bookheader = (TextView) findViewById(R.id.classname);
        bookheader.setText(getString(R.string.Student_birthyday));
        birthdayGrid = (GridView) findViewById(R.id.booklist);
        profilImageView = (ImageView) findViewById(R.id.profileimage);
        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        dp = new DatabaseHandler(this, pref.getString("staffdbname", ""), DatabaseHandler.DATABASE_VERSION);
        dateTextview = (TextView) findViewById(R.id.date);
        edit = pref.edit();
        //setbackground(profilImageView, pref.getString("image", ""));
        classname.setText( pref.getString("classname", ""));
        table = dp.getcontentdetails();
        if (!pref.getBoolean("roomornot", false)) {
            subjectname = dp.getbatchdetails(Integer.parseInt(pref.getString("bookbin", "0")),
                    Integer.parseInt(pref.getString("batchid", "0"))
                    , Integer.parseInt(pref.getString("staffid", "0")));
        } else {
            subjectname = dp.getbatchdetailsfrommasterinfo();
        }
        recyclerView= (RecyclerView) findViewById(R.id.recycle_data);


        utils = new Utilss(BirthdayActivity.this);
        utils.setTextviewtypeface(1, title);
        utils.setTextviewtypeface(1,this_week);
        utils.setTextviewtypeface(1,today);
        utils.setTextviewtypeface(1,next_week);
        utils.setTextviewtypeface(5, dateTextview);
        this_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        academicpath = pref.getString("academic", "");
        refpath = pref.getString("reference", "");
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c);
        dateTextview.setText(formattedDate);
        studentdetails = new ArrayList<>();
        birthydaystudentdetails = new ArrayList<>();
        studentdetails = dp.getstudentdetails();

        for (int y = 0; y < studentdetails.size(); y++) {
            SimpleDateFormat currentdate = new SimpleDateFormat("dd-MM");
            String cureentformattedDate = currentdate.format(c);

            String mytime = studentdetails.get(y).getDOB();
            Log.d("VIKIs", "mytime" + mytime);
            if (!mytime.equals("-0001-11-30")) ;
            {


                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-dd-MM");

                Date myDate = null;
                try {
                    myDate = dateFormat.parse(mytime);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat timeFormat = new SimpleDateFormat("MM-dd");
                String finalDate = timeFormat.format(myDate);

                System.out.println(finalDate);
                if (finalDate.equals(cureentformattedDate)) {

                    Log.d("VIKIs", "student birthyday" + mytime);
                    birthydaystudentdetails.add(studentdetails.get(y));
                }
            }
        }
        if (birthydaystudentdetails.size() > 0) {

            AdapterBirthDay adapter=new AdapterBirthDay(birthydaystudentdetails,this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);

            /*birthdayadapter = new BirthdayAdapter(birthydaystudentdetails, getBaseContext());
            birthdayGrid.setAdapter(birthdayadapter);*/

        } else {
            Toast.makeText(getBaseContext(), "No Student Birthday's Today", Toast.LENGTH_SHORT).show();
        }

        /*edit.putString("bookbin",""+gettable.get(position).getClassID());
        edit.commit();*/

        //bookshelf=new ArrayList<>();


		/*bookshelf.add("Academic");
		bookshelf.add("Reference");*/
      /*  li=new ArrayList<String>();
        li.add("All");
        li.add("Reference");
      */

        videos = new ArrayList<>();

        // shelflistView = (ListView) findViewById(R.id.selflisview);


        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dnd = (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((!pref.getBoolean("break", false))) {
                    if (pref.getBoolean("dnd", false)) {
                        dnd.setImageResource(R.drawable.dci);
                        edit.putBoolean("dnd", false);
                        edit.commit();

                        String senddata = "DND";

                        senddata = senddata + "@false";

                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                        dnd.setImageResource(R.drawable.inactive);


                    } else {
                        dnd.setImageResource(R.drawable.active);

                        String senddata = "DND";

                        senddata = senddata + "@true";
                        edit.putBoolean("dnd", true);
                        edit.commit();
                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    }
                } else {
                    Toast.makeText(BirthdayActivity.this, "You can access this feature once you come out of the session break.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(BirthdayActivity.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(BirthdayActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        today.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markClickedView(v);
            }
        });
        this_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markClickedView(v);
            }
        });
        next_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markClickedView(v);
            }
        });


    }
    public void markClickedView(View v){
        next_week.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
        today.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
        this_week.setBackground(getResources().getDrawable(R.drawable.btn_grey_bg));
        next_week.setTextColor(getResources().getColor(R.color.oldbg));
        today.setTextColor(getResources().getColor(R.color.oldbg));
        this_week.setTextColor(getResources().getColor(R.color.oldbg));
        if (v.getId()==R.id.next_week){
            next_week.setBackground(getResources().getDrawable(R.drawable.btn_bg));
            next_week.setTextColor(getResources().getColor(R.color.white));
        }
        if (v.getId()==R.id.today){
            today.setBackground(getResources().getDrawable(R.drawable.btn_bg));
            today.setTextColor(getResources().getColor(R.color.white));
        }
        if (v.getId()==R.id.this_week){
            this_week.setBackground(getResources().getDrawable(R.drawable.btn_bg));
            this_week.setTextColor(getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getlayout() {
        return R.layout.activity_birthyday;
    }


    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = Settings.System.getInt(
                    getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }


}




