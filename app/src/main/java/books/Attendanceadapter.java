package books;

/**
 * Created by iyyapparajr on 5/14/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;


import Utilities.DatabaseHandler;
import Utilities.Utilss;
import helper.Studentdetails;
import helper.Studentinfo;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Attendanceadapter extends ArrayAdapter<Studentinfo> {

    private ArrayList<Studentinfo> studentinfo;
    Context con;
    SharedPreferences pref;
    DatabaseHandler db;
    Utilss utils;
    //CoolReader mactivity;

    public Attendanceadapter(Context context, int textViewResourceId,
                             ArrayList<Studentinfo> studentinfo,SharedPreferences pref) {
        super(context, textViewResourceId, studentinfo);
        this.studentinfo = new ArrayList<Studentinfo>();
        this.pref=pref;
        utils=new Utilss((Activity) context);
        con=context;
        this.studentinfo.addAll(studentinfo);
        db=new DatabaseHandler((Activity)con,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
        //db=new DatabaseHandler((Activity)con);
    }

    private class ViewHolder {
        ImageView studentimage,tickicon;
        TextView studenttext;
        RelativeLayout back;
        RelativeLayout parentlayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.takeattendance, null);
            holder = new ViewHolder();
            holder.tickicon=(ImageView)convertView.findViewById(R.id.tickicon) ;
            holder.studentimage= (ImageView) convertView.findViewById(R.id.studentimage);
            holder.studenttext= (TextView) convertView.findViewById(R.id.studentname);
            holder.back= (RelativeLayout) convertView.findViewById(R.id.back);
            convertView.setTag(holder);


        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(studentinfo.get(position).isChangebackground())
        {
            holder.tickicon.setVisibility(View.VISIBLE);
           // holder.back.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.grey)));
          //  holder.studenttext.setTextColor(con.getResources().getColor(R.color.subject_title_grey));
        }
        else
        {
            holder.tickicon.setVisibility(View.GONE);
//            holder.back.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.white)));
//            holder.studenttext.setTextColor(con.getResources().getColor(R.color.oldbg));
//            holder.back.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.white)));

        }
        try {
            Studentdetails details = db.getdetails(Integer.parseInt(studentinfo.get(position).getStudentrollnumber()),

                    Integer.parseInt(pref.getString("schoolid", "0")),
                    Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("staffid", "0")));
            holder.studenttext.setText(studentinfo.get(position).getStudentname().toUpperCase());

            try {
                File imgfile=new File(details.getPhotoFilename());
                Picasso.with(con).load(imgfile).placeholder(R.drawable.user_icons).into(holder.studentimage);
            }
            catch (Exception e)
            {

            }
            // setbackground(holder.studentimage, details.getPhotoFilename());
        }
        catch (Exception e)
        {

        }
      /*  if(studentinfo.get(position).isVisibornot())
        {
            convertView.setVisibility(View.VISIBLE);
        }
        else
        {
            convertView.setVisibility(View.G);
        }*/

        //Toast.makeText(con,studentinfo.get(position).getStudentname(),Toast.LENGTH_SHORT).show();
        //Toast.makeText(con,details.getPhotoFilename(),Toast.LENGTH_SHORT).show();

        // holder.code.setText(" (" +  country.getCode() + ")");
      /*  holder.name.setText(videoname.getName());

        holder.name.setTag(videoname);
*/
      utils.setTextviewtypeface(5,holder.studenttext);


        return convertView;

    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

}