package books;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import Utilities.Attendancedb;
import Utilities.Service;

import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Attendancecredentialpojo;
import helper.Attendancepojo;
import helper.RoundedImageView;
import helper.StudentAttendancecredenitpojo;
import helper.Studentdetails;
import helper.Studentinfo;

/**
 * Created by iyyapparajr on 5/14/2017.
 */
public class Attendacecapture extends Activity {
    RoundedImageView profileimage;
    TextView batchname,attendance;
    GridView attendancegridview;
    ImageView back;
    EditText search;
    TextView submit;
    String credits_str;
    ArrayList<Studentinfo> info;
    //  ArrayList<Studentinfo> common;
    ArrayList<Studentinfo> present;
    ArrayList<Studentinfo> absent;
    ArrayList<Studentinfo> online;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    Attendanceadapter adapter;
    Utilss utils;
    //Attendancewithcheckbox adapter;
    DatabaseHandler dp;

    TextView dateset,datatime;
    TextView submitattendance;
    Attendancedb db;
    TextView total,presentcount,absentcount;
    TextView totaltext,presencounttext,absentcounttext;
    boolean presentsearchtext;
    HashMap<Integer,Studentinfo> studenitem=new HashMap<>();
    HashMap<Integer,Studentinfo> studenitem1=new HashMap<>();
    TextView classname;
    RelativeLayout presentrel,absentrel;
    boolean presentornabsent;
    ArrayList<Studentdetails>student;
    ArrayList<Studentinfo>presentsearch;
    ImageView capture;
    ImageView dnd;
    ImageView handraise;
    TextView presentbutton,absentrelbtn;
    int uploaded;
    TextView classes;
    ArrayList<Studentdetails>studenbelongclass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendanceparentlayout);
        presentbutton= (TextView) findViewById(R.id.presentbutton);
        absentrelbtn= (TextView) findViewById(R.id.absentrelbtn);
        utils=new Utilss(this);
        profileimage= (RoundedImageView) findViewById(R.id.profileimage);

        classname= (TextView) findViewById(R.id.secondclassname);
        classes=(TextView)findViewById(R.id.classname);
        batchname= (TextView) findViewById(R.id.textView);
        attendance= (TextView) findViewById(R.id.classname);
        dateset= (TextView) findViewById(R.id.setdate);
        search= (EditText) findViewById(R.id.search);
        submit= (TextView) findViewById(R.id.absent);
        capture= (ImageView) findViewById(R.id.captureattendance);
        total= (TextView) findViewById(R.id.Total);
        totaltext= (TextView) findViewById(R.id.totalcount);
        presentcount= (TextView) findViewById(R.id.present);
        presencounttext= (TextView) findViewById(R.id.presentcount);
        absentcount= (TextView) findViewById(R.id.absentes);
        absentcounttext= (TextView) findViewById(R.id.absentcount);
        absent=new ArrayList<>();
        present=new ArrayList<>();
        online=new ArrayList<>();
        presentsearch=new ArrayList<>();
        presentsearchtext=false;
        submit.setText("Select Student");
        presentornabsent=true;
        uploaded=0;

        presentrel= (RelativeLayout) findViewById(R.id.presentbuttonrel);
        absentrel= (RelativeLayout) findViewById(R.id.absenteesbuttonrel);

        datatime= (TextView) findViewById(R.id.Settime);
        attendancegridview= (GridView) findViewById(R.id.studentgrid);
        back= (ImageView) findViewById(R.id.back);
       // db=new Attendancedb(this);
        submitattendance= (TextView) findViewById(R.id.submit);

        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
      //  dp=new DatabaseHandler(this);
        db=new Attendancedb(this,pref.getString("attendancedb",""),Attendancedb.DATABASE_VERSION);
        dp=new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
       // dp.getstudentdetails();
        dateset.setText(Service.currentdate());
        datatime.setText(Service.currenttime());
        edit=pref.edit();
        attendancegridview= (GridView) findViewById(R.id.studentgrid);
        info=new ArrayList<>();
        //  common=new ArrayList<>();
        setbackground(profileimage,pref.getString("image",""));
        batchname.setText(pref.getString("classname", ""));
        classname.setText(pref.getString("classname", ""));
        handraise= (ImageView) findViewById(R.id.handrise);

        utils.setTextviewtypeface(5,batchname);
        utils.setTextviewtypeface(5,dateset);
        utils.setTextviewtypeface(5,batchname);
        utils.setTextviewtypeface(5,search);
        utils.setTextviewtypeface(5,submit);
        utils.setTextviewtypeface(5,presentcount);
        utils.setTextviewtypeface(5,absentcount);
        utils.setTextviewtypeface(5,absentcounttext);
        utils.setTextviewtypeface(5,total);
        utils.setTextviewtypeface(5,presentbutton);
        utils.setTextviewtypeface(5,absentrelbtn);
        utils.setTextviewtypeface(5,classes);
        utils.setTextviewtypeface(5,classname);




        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(Attendacecapture.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(Attendacecapture.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });

        dnd= (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getBoolean("dnd",false))
                {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata=senddata+"@false";
                    dnd.setImageResource(R.drawable.active);


                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                }
                else
                {
                    dnd.setImageResource(R.drawable.inactive);

                    String senddata = "DND";

                    senddata=senddata+"@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
            }
        });

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
Toast.makeText(Attendacecapture.this,
        ""+db.getoverallcreditsforteacherthismonth(Integer.parseInt(Service.currentmonth()),Integer.parseInt(Service.getyear()),
                Integer.parseInt(pref.getString("staffid", "0")),
                Integer.parseInt(pref.getString("subjectid", "0"))),
                Toast.LENGTH_LONG).show();
*/

                db.getoverallcreditsforstudentthismonth(Integer.parseInt(Service.currentmonth()),Integer.parseInt(Service.getyear()),
                        Integer.parseInt(pref.getString("staffid", "0")),
                        Integer.parseInt(pref.getString("subjectid", "0")),
                        Integer.parseInt(pref.getString("batchid", "0")));
            }
        });

       /* String studendetails[] = getmessage.split("\\@");
        Studentinfo info = new Studentinfo();
        info.setStudentip(studendetails[2]);
        info.setStudentrollnumber(studendetails[3]);
        info.setStudentschooid(studendetails[4]);
        info.setStudentname(studendetails[5]);
        info.setStudentid(studendetails[6]);


        Attendancepojo details=new Attendancepojo();
        details.setSchoolID(Integer.parseInt(pref.getString("schoolid","0")));
        details.setBatchID(Integer.parseInt(pref.getString("batchid", "0")));
        details.setRollNo(Integer.parseInt(info.getStudentrollnumber()));
        details.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
        details.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));
        details.setStudentID(Integer.parseInt(info.getStudentid()));
        details.setAttendanceDateTime(studendetails[7]);
        if(!attenddb.attendancecheckpojo(details.getRollNo(),details.getBatchID(),details.getSubjectID()))
            attenddb.attendancetaken(details);
        else
            attenddb.updateattendance(details);*/

        for(int i=0; i< MultiThreadChatServerSync.thread.size(); i++)
        {
            clientThread thread = MultiThreadChatServerSync.thread.get(i);
            Studentinfo info=thread.info;
          //  Toast.makeText(Attendacecapture.this,info.getStudentip(),Toast.LENGTH_SHORT).show();
            info.setManual("0");
            online.add(info);

            //info.add(MultiThreadChatServerSync.thread.get(i).);
        }
        studenitem.clear();
        absent.clear();
        info.clear();
        studenbelongclass=new ArrayList<>();
        student= dp.getdetailsofbatch(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("staffid", "0")), Integer.parseInt(pref.getString("batchid", "0")));/*dp.getstudentdetails();*//*dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")),  Integer.parseInt(pref.getString("staffid", "0")));*/
        for(int j=0; j<student.size(); j++)
        {
            Studentinfo info1=new Studentinfo();
            info1.setStudentname(student.get(j).getFirstName());
            info1.setStudentid("" + student.get(j).getStudentID());
            info1.setStudentrollnumber(student.get(j).getRollNo());
            info1.setStudentclassid(student.get(j).getClassID());

            info1.setChangebackground(false);
         //   String rooms[]=student.get(j).getRoominfo().split(",");
          /*  boolean addtostudent=false;
            for(int r=0; r<rooms.length; r++)
            {
                if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                {
                    addtostudent=true;
                    break;
                }
            }*/
         //  if(addtostudent) {
               info.add(info1);
               studenbelongclass.add(student.get(j));

               boolean addtoabsent = false;
               for (int n = 0; n < online.size(); n++) {

                   if (info1.getStudentid().equalsIgnoreCase("" + online.get(n).getStudentid())) {
                       addtoabsent = true;
                       break;
                   }
               }
               if (!addtoabsent) {
                   absent.add(info1);
               }
         //  }

        }

        totaltext.setText("" + studenbelongclass.size());
      /*  common.clear();
        common.addAll(info);*/



        //()
        count();
        presentrel.performClick();

        attendancegridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(!presentornabsent) {
                    if (!absent.get(position).isChangebackground()) {
                        absent.get(position).setChangebackground(true);
                        adapter.notifyDataSetChanged();

                        Studentinfo getinfo=absent.get(position);
                        getinfo.setManual("0");
                        //getinfo.setChangebackground(false);
                        studenitem.put(position, getinfo);

                    } else {
                        absent.get(position).setChangebackground(false);
                        adapter.notifyDataSetChanged();
                        studenitem.remove(position);
                    }
                }
                else if(!presentsearchtext)
                {
                    if (!online.get(position).isChangebackground()) {
                        online.get(position).setChangebackground(true);
                        online.get(position).setManual("1");
                        adapter.notifyDataSetChanged();

                        Studentinfo getinfo=online.get(position);
                        //getinfo.setChangebackground(false);
                        studenitem.put(position, getinfo);

                    } else {
                        online.get(position).setChangebackground(false);
                        adapter.notifyDataSetChanged();
                        studenitem.remove(position);
                    }
                }
                else
                {
                    if (!presentsearch.get(position).isChangebackground()) {
                        presentsearch.get(position).setChangebackground(true);
                        adapter.notifyDataSetChanged();

                        Studentinfo getinfo=presentsearch.get(position);
                        //getinfo.setChangebackground(false);
                        studenitem.put(position, getinfo);

                    } else {
                        presentsearch.get(position).setChangebackground(false);
                        adapter.notifyDataSetChanged();
                        studenitem.remove(position);
                    }
                }


                if (studenitem.size() > 0) {
                    if(presentornabsent)
                        submit.setText("MARK ABSENT");
                    else
                        submit.setText("MARK PRESENT");


                } else {
                    submit.setText("SELECT STUDENT");

                }

            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                // search.setFocusable(false);
                presentsearchtext=false;
                if(submit.getText().toString().equalsIgnoreCase("MARK ABSENT"))
                {
                    studenitem1.putAll(studenitem);
                    studenitem.clear();
                    for ( Map.Entry<Integer,Studentinfo> entry : studenitem1.entrySet()) {


                        Studentinfo geinfo=entry.getValue();
                        // geinfo.setChangebackground(false);
                        for(int i=0; i<online.size(); i++)
                        {
                            if(online.get(i).getStudentid().equalsIgnoreCase(""+geinfo.getStudentid()))
                            {
                                online.remove(i);
                            }

                        }

                        // online.add(geinfo);
                        // String key = entry.getKey();
                        // Tab tab = entry.getValue();
                        // do something with key and/or tab
                    }
                    studenitem1.clear();
                    marksasabsent();
                    submit.setText("SELECT STUDENT");

                }
                else  if(submit.getText().toString().equalsIgnoreCase("MARK PRESENT"))
                {
                    studenitem1.putAll(studenitem);
                    studenitem.clear();
                    for ( Map.Entry<Integer,Studentinfo> entry : studenitem1.entrySet()) {


                        Studentinfo geinfo=entry.getValue();
                        geinfo.setChangebackground(false);
                        geinfo.setManual("1");
                        //geinfo.set
                        online.add(geinfo);
                        // String key = entry.getKey();
                        // Tab tab = entry.getValue();
                        // do something with key and/or tab
                    }
                    studenitem1.clear();
                    markaspresent();
                    submit.setText("SELECT STUDENT");

                }
                count();
            }

        });


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String get = s.toString().toUpperCase();
              /*  if (get.length() > 0) {
                    common.clear();
                    for (int j = 0; j < info.size(); j++) {
                        if (info.get(j).getStudentname().contains(get)) {
                            common.add(info.get(j));
                            info.get(j).setVisibornot(true);
                        } else {
                            info.get(j).setVisibornot(false);

                        }
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    info.clear();
                    for (int j = 0; j < student.size(); j++) {
                        Studentinfo info1 = new Studentinfo();
                        info1.setStudentname(student.get(j).getFirstName());
                        info1.setStudentid("" + student.get(j).getStudentID());
                        info1.setStudentrollnumber(student.get(j).getRollNo());
                        info1.setVisibornot(true);
                        info.add(info1);


                    }
                    common.clear();
                    common.addAll(info);
                    adapter = new Attendanceadapter(Attendacecapture.this,
                            R.layout.takeattendance, common, pref);
                    attendancegridview.setAdapter(adapter);


                }*/
                if(!presentornabsent) {
                    markaspresentsearch(get);
                    /*if (!absent.get(position).isChangebackground()) {
                        absent.get(position).setChangebackground(true);
                        adapter.notifyDataSetChanged();

                        Studentinfo getinfo=absent.get(position);
                        //getinfo.setChangebackground(false);
                        studenitem.put(position, getinfo);

                    } else {
                        absent.get(position).setChangebackground(false);
                        adapter.notifyDataSetChanged();
                        studenitem.remove(position);
                    }*/
                }
                else
                {
                    marksasabsentsearch(get);
                   /* if (!online.get(position).isChangebackground()) {
                        online.get(position).setChangebackground(true);
                        adapter.notifyDataSetChanged();

                        Studentinfo getinfo=online.get(position);
                        //getinfo.setChangebackground(false);
                        studenitem.put(position, getinfo);

                    } else {
                        online.get(position).setChangebackground(false);
                        adapter.notifyDataSetChanged();
                        studenitem.remove(position);
                    }*/
                }
                count();



            }
        });
        submitattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  int batchid,int subjectid,int staffid,String startdate,String endate )

              /*  details.setSchoolID(Integer.parseInt(pref.getString("schoolid", "0")));
                details.setBatchID(Integer.parseInt(pref.getString("batchid", "0")));
                details.setRollNo(Integer.parseInt(info.get(i).getStudentrollnumber()));
                details.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
                details.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));*/
                if(db.alereadytakeattendance(Integer.parseInt(pref.getString("batchid", "0")),
                        Integer.parseInt(pref.getString("subjectid", "0")),
                        Integer.parseInt(pref.getString("staffid", "0")),
                        Service.getdateforattendance(),
                        Service.attendenacedate(),Integer.parseInt(pref.getString("bookbin","0"))))
                {
                    //here takeattendancepopup

                    Attendancepojo pojo=db.alereadytakeattendancelatestrecord(Integer.parseInt(pref.getString("batchid", "0")),
                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")),
                            Service.getdateforattendance(),
                            Service.attendenacedate(),Integer.parseInt(pref.getString("bookbin","0")));


                    uploaded=pojo.getIsUploaded();
                    stratExamCompletedPopup(pojo.getAttendanceDateTime());

                }
                else
                {
                    showpopup();
                }

            }
        });



        presentrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentornabsent=true;
                presentsearchtext=false;
                absentrelbtn.setTextColor(getResources().getColor(R.color.oldbg));
                presentbutton.setTextColor(getResources().getColor(R.color.white));

                presentrel.setBackgroundResource(R.drawable.btn_bg);
                absentrel.setBackgroundResource(R.drawable.btn_grey_bg);


                //absentrel.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.oldbg)));
                adapter=new Attendanceadapter(Attendacecapture.this,
                        R.layout.takeattendance,online,pref);
                attendancegridview.setAdapter(adapter);


              /*  for(int j=0; j<student.size(); j++)
                {
                    Studentinfo info1=new Studentinfo();
                    info1.setStudentname(student.get(j).getFirstName());
                    info1.setStudentid("" + student.get(j).getStudentID());
                    info1.setStudentrollnumber(student.get(j).getRollNo());
                    info1.setChangebackground(false);
                    info.add(info1);


                }
                common.clear();
                common.addAll(info);



                //()
                adapter=new Attendanceadapter(Attendacecapture.this,
                        R.layout.takeattendance,common,pref);
                attendancegridview.setAdapter(adapter);*/
            }
        });
        absentrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presentornabsent=false;
                presentsearchtext=false;
                absentrelbtn.setTextColor(getResources().getColor(R.color.white));
                presentbutton.setTextColor(getResources().getColor(R.color.oldbg));


                absentrel.setBackgroundResource(R.drawable.btn_bg);
                presentrel.setBackgroundResource(R.drawable.btn_grey_bg);
                markaspresent();
                /*adapter=new Attendanceadapter(Attendacecapture.this,
                        R.layout.takeattendance,absent,pref);
                attendancegridview.setAdapter(adapter);*/
                /*for(int j=0; j<student.size(); j++)
                {
                    Studentinfo info1=new Studentinfo();
                    info1.setStudentname(student.get(j).getFirstName());
                    info1.setStudentid("" + student.get(j).getStudentID());
                    info1.setStudentrollnumber(student.get(j).getRollNo());
                    info1.setChangebackground(false);
                    info.add(info1);


                }
                common.clear();
                common.addAll(info);



                //()
                adapter=new Attendanceadapter(Attendacecapture.this,
                        R.layout.takeattendance,common,pref);
                attendancegridview.setAdapter(adapter);*/

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        openattendance();

      //  showpopupattendance();

    }
    public void openattendance()
    {
        presentornabsent=false;
        presentsearchtext=false;
        absentrelbtn.setTextColor(getResources().getColor(R.color.white));
        presentbutton.setTextColor(getResources().getColor(R.color.oldbg));


        absentrel.setBackgroundResource(R.drawable.btn_bg);
        presentrel.setBackgroundResource(R.drawable.btn_grey_bg);
        markaspresent();
    }


    void showpopupattendance()
    {

        if(db.alereadytakeattendance(Integer.parseInt(pref.getString("batchid", "0")),
                Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")),
                Service.getdateforattendance(),
                Service.attendenacedate(), Integer.parseInt(pref.getString("bookbin", "0"))))
        {
            //here takeattendancepopup

            Attendancepojo pojo=db.alereadytakeattendancelatestrecord(Integer.parseInt(pref.getString("batchid", "0")),
                    Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")),
                    Service.getdateforattendance(),
                    Service.attendenacedate(),Integer.parseInt(pref.getString("bookbin","0")));


            uploaded=pojo.getIsUploaded();
            stratExamCompletedPopup(pojo.getAttendanceDateTime());

        }
        else
        {
            stratExamCompletedPopup();
           // showpopupattendance();
           // showpopup();
        }

    }

    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    public void showpopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.creditesspinnerlayout);
        dialog.setCancelable(false);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.ok);
        Button revisit = (Button) dialog.findViewById(R.id.cancel);
        ImageView down =   (ImageView) dialog.findViewById(R.id.down);
        Spinner csspinner= (Spinner) dialog.findViewById(R.id.creditspinner);
        final ArrayList<String>getstring=new ArrayList<>();
        getstring.add("1");
        getstring.add("2");
        getstring.add("3");
        getstring.add("4");
        csspinner.setSelection(0);
        csspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                credits_str = getstring.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                csspinner.performClick();
            }
        });

        creditspinner credits   = new creditspinner(Attendacecapture.this, R.layout.custom_spinner_item, getstring);
        csspinner.setAdapter(credits);
        //  title.setText("ACADEMIC BOOK SHARING");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });



        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String datetime=Service.attendenacedate();

                //  info= dp.getstudentdetails();
                for (int i = 0; i < info.size(); i++) {
                    Attendancepojo details = new Attendancepojo();
                    details.setSchoolID(Integer.parseInt(pref.getString("schoolid", "0")));
                    details.setBatchID(Integer.parseInt(pref.getString("batchid", "0")));
                    details.setRollNo(Integer.parseInt(info.get(i).getStudentrollnumber()));
                    details.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
                    details.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));
                    details.setStudentID(Integer.parseInt(info.get(i).getStudentid()));
                   details.setClassid(info.get(i).getStudentclassid());
                    details.setAttendanceDateTime(datetime);
                    details.setStudName(info.get(i).getStudentname());
                    details.setCreatedOn(Service.attendenacedate());
                    details.setModifiedOn(Service.attendenacedate());
                    details.setManualAttendance(0);
                    details.setRoomid(0);
                    details.setCredits(Integer.parseInt(credits_str));
                    boolean onlinebool=false;
                    for(int on=0; on<online.size(); on++)
                    {
                        if(online.get(on).getStudentid().equalsIgnoreCase(""+info.get(i).getStudentid()))
                        {
                            onlinebool=true;
                            if(online.get(on).getManual().equalsIgnoreCase("1"))
                            {
                                details.setManualAttendance(1);
                            }

                            break;
                        }
                    }
                    if(onlinebool)
                        details.setAttendance("P");
                    else
                        details.setAttendance("A");


                    db.attendancetaken(details);

                    StudentAttendancecredenitpojo stu=new StudentAttendancecredenitpojo();
                    stu.setStaffID(details.getStaffID());
                    stu.setBatchID(details.getBatchID());
                    stu.setSubjectID(details.getSubjectID());
                    if(details.getAttendance().equalsIgnoreCase("P")) {
                        stu.setCredits(Integer.parseInt(credits_str));
                    }
                    else
                    {
                        stu.setCredits(0);
                    }
                    stu.setMonth(Integer.parseInt(Service.currentmonth()));
                    stu.setStudentID(details.getStudentID());
                    stu.setModifiedOn(Service.attendenacedate());
                    stu.setCreatedOn(Service.attendenacedate());
                    stu.setYear(Service.getyear());


                    if (db.checkcreditsforthismontstudent(Integer.parseInt(Service.currentmonth()),

                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")),
                            Integer.parseInt(pref.getString("batchid", "0")), stu.getStudentID(),
                            Service.getyear()))
                    {

                        StudentAttendancecredenitpojo pojo = db.getstudentcreditsrecord(Integer.parseInt(Service.currentmonth()),

                                Integer.parseInt(pref.getString("subjectid", "0")),
                                Integer.parseInt(pref.getString("staffid", "0")),
                                Integer.parseInt(pref.getString("batchid", "0")), stu.getStudentID(),
                                Service.getyear());
                        pojo.setCredits(pojo.getCredits() + stu.getCredits());/*Integer.parseInt(credits_str));*/
                        if (db.updatecreditstostudentforthismonth(Integer.parseInt(Service.currentmonth()),

                                Integer.parseInt(pref.getString("subjectid", "0")),
                                Integer.parseInt(pref.getString("staffid", "0")),
                                Integer.parseInt(pref.getString("batchid", "0")), stu.getStudentID(),
                                Service.getyear(),pojo))
                        {
                            Log.e("student","updated");
                        }


                    }
                    else {
                        Log.e("student","inserting");
                        db.insertstudentcredits(stu);
                    }
/*
                    "\"StudentID\" integer,\n" +
                            "\"Month\" integer,\n" +
                            "\"Credits\" integer,\n" +
                            "\"StaffID\" integer,\n" +
                            "\"SubjectID\" integer,\n" +
                            "\"BatchID\" integer,\n" +
                            "\"IsPosted\" integer,\n" +
                            "\"CreatedOn\" datetime,\n" +
                            "\"ModifiedOn\" datetime \n" +
                            ")";*/
                }
                // (int month,int subjectid,int staffid,int batchid ,String year)

                //  int month,int subjectid,int staffid,int batchid ,String year)
                if(db.checkcreditsforthismont(Integer.parseInt(Service.currentmonth()),

                        Integer.parseInt(pref.getString("subjectid", "0")),
                        Integer.parseInt(pref.getString("staffid", "0")),
                        Integer.parseInt(pref.getString("batchid", "0")),
                        Service.getyear()))
                {
                    Attendancecredentialpojo apojo=db.getteachercreditsrecord(Integer.parseInt(Service.currentmonth()),

                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")),
                            Integer.parseInt(pref.getString("batchid", "0")),
                            Service.getyear());

                    apojo.setCredits(apojo.getCredits()+Integer.parseInt(credits_str));
                    if(db.updatecreditstoteacherforthismonth(Integer.parseInt(Service.currentmonth()),

                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")),
                            Integer.parseInt(pref.getString("batchid", "0")),
                            Service.getyear(),apojo))
                    {
                        Log.e("staff","updated");
                    }

                }
                else
                {

                    /*"\"Month\" integer,\n" +
                            "\"Credits\" integer,\n" +
                            "\"StaffID\" integer,\n" +
                            "\"SubjectID\" integer,\n" +
                            "\"BatchID\" integer,\n" +
                            "\"IsPosted\" integer,\n" +
                            "\"CreatedOn\" datetime,\n" +
                            "\"ModifiedOn\" datetime, \n" +
                            "\"Year\" varchar"+*/
                    Attendancecredentialpojo apojo=new Attendancecredentialpojo();
                    apojo.setBatchID( Integer.parseInt(pref.getString("batchid", "0")));
                    apojo.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));
                    apojo.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
                    apojo.setMonth(Integer.parseInt(Service.currentmonth()));
                    apojo.setCredits(Integer.parseInt(credits_str));
                    apojo.setIsPosted(0);
                    apojo.setYear(Service.getyear());
                    db.attendancecredetinal(apojo);

                }



                dialog.dismiss();

                Toast.makeText(Attendacecapture.this,"Attendance Capture Successfully",Toast.LENGTH_SHORT).show();
                finish();

            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    public void stratExamCompletedPopup() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title = (TextView) dialog.findViewById(R.id.textView2);
        TextView attendance = (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button revisit = (Button) dialog.findViewById(R.id.button2);
        Button valuate  = (Button) dialog.findViewById(R.id.button3);

        revisit.setText("NO");
        valuate.setText("Yes");
        title.setText("Attendance");
        attendance.setText("Attendance for "+pref.getString("classname", "")+" is captured. Would you like to save this attendance??");
revisit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        dialog.dismiss();

    }
});
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();

                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

    }

    public void stratExamCompletedPopup(final String date) {
        final  Dialog  dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        TextView attendance= (TextView) dialog.findViewById(R.id.notsaved);
        ImageView close= (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);

        revisit.setText("Update Attendance");
        valuate.setText("New Attendance");
        title.setText("Attendance");
        attendance.setText("You have already taken attendance at " + date);
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String attentime = Service.attendenacedate();
                for (int i = 0; i < info.size(); i++) {
                    Attendancepojo details = new Attendancepojo();
                    details.setSchoolID(Integer.parseInt(pref.getString("schoolid", "0")));
                    details.setBatchID(Integer.parseInt(pref.getString("batchid", "0")));
                    details.setRollNo(Integer.parseInt(info.get(i).getStudentrollnumber()));
                    details.setSubjectID(Integer.parseInt(pref.getString("subjectid", "0")));
                    details.setStaffID(Integer.parseInt(pref.getString("staffid", "0")));
                    details.setStudentID(Integer.parseInt(info.get(i).getStudentid()));
                    details.setClassid(info.get(i).getStudentclassid());
                    details.setRoomid(0);

                    details.setClassid(info.get(i).getStudentclassid());
                    details.setAttendanceDateTime(attentime);
                    details.setStudName(info.get(i).getStudentname());

                    details.setCreatedOn(Service.attendenacedate());
                    details.setModifiedOn(Service.attendenacedate());
                    if(i==0)
                    {
                        uploaded=db.uplooaddateforstudent(Integer.parseInt(pref.getString("batchid", "0")),
                                Integer.parseInt(pref.getString("subjectid", "0")),
                                Integer.parseInt(pref.getString("staffid", "0")), date, details);
                    }
                    if(uploaded==1)
                    {
                        details.setIsnew(0);

                    }
                    else
                    {
                        details.setIsnew(1);

                    }
                    details.setIsUploaded(uploaded);
                    boolean onlinebool=false;
                    for(int on=0; on<online.size(); on++)
                    {
                        if(online.get(on).getStudentid().equalsIgnoreCase(""+info.get(i).getStudentid()))
                        {
                            onlinebool=true;
                            if(online.get(on).getManual().equalsIgnoreCase("1"))
                            {
                                details.setManualAttendance(1);
                            }

                            break;
                        }
                    }
                    if(onlinebool)
                        details.setAttendance("P");
                    else
                        details.setAttendance("A");

                    // details.setAttendance("A");


                    Attendancepojo getpojo = db.alereadytakeattendancelatestrecordindividual(Integer.parseInt(pref.getString("batchid", "0")),
                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")), Service.getdateforattendance(),
                            details.getStudentID(),date);
                    if (!getpojo.getAttendance().equalsIgnoreCase(details.getAttendance())) {
                        if (details.getAttendance().equalsIgnoreCase("P")) {
                            StudentAttendancecredenitpojo pojo = db.getstudentcreditsrecord(Integer.parseInt(Service.currentmonth()),

                                    Integer.parseInt(pref.getString("subjectid", "0")),
                                    Integer.parseInt(pref.getString("staffid", "0")),
                                    Integer.parseInt(pref.getString("batchid", "0")), details.getStudentID(),//studentid
                                    Service.getyear());
                            pojo.setCredits(pojo.getCredits() + getpojo.getCredits());
                            if(db.updatecreditstostudentforthismonth(Integer.parseInt(Service.currentmonth()),

                                    Integer.parseInt(pref.getString("subjectid", "0")),
                                    Integer.parseInt(pref.getString("staffid", "0")),
                                    Integer.parseInt(pref.getString("batchid", "0")), details.getStudentID(),
                                    Service.getyear(),pojo))
                            {
                                Log.e("student","updated");
                            }


                        } else {
                            StudentAttendancecredenitpojo pojo = db.getstudentcreditsrecord(Integer.parseInt(Service.currentmonth()),

                                    Integer.parseInt(pref.getString("subjectid", "0")),
                                    Integer.parseInt(pref.getString("staffid", "0")),
                                    Integer.parseInt(pref.getString("batchid", "0")), details.getStudentID(),//studentid
                                    Service.getyear());
                            if (pojo.getCredits() > 0) {
                                pojo.setCredits(pojo.getCredits() - getpojo.getCredits());
                            }

                            if(db.updatecreditstostudentforthismonth(Integer.parseInt(Service.currentmonth()),

                                    Integer.parseInt(pref.getString("subjectid", "0")),
                                    Integer.parseInt(pref.getString("staffid", "0")),
                                    Integer.parseInt(pref.getString("batchid", "0")), details.getStudentID(),
                                    Service.getyear(),pojo))
                            {
                                Log.e("student", "updated");
                            }

                        }


                    }

                    if (db.updateattendancedb(Integer.parseInt(pref.getString("batchid", "0")),
                            Integer.parseInt(pref.getString("subjectid", "0")),
                            Integer.parseInt(pref.getString("staffid", "0")), date, details)) {
                        Log.e("update", "updateing");
                    }
                }

                // TODO Auto-generated method stub
                dialog.dismiss();
                Toast.makeText(Attendacecapture.this,"Attendance Capture Successfully",Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showpopup();

                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    void markaspresent()
    {
        final ArrayList<Studentdetails>student=dp.getdetailsofbatch(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("staffid", "0")), Integer.parseInt(pref.getString("batchid", "0")));/*dp.getstudentdetails();*/

        absent.clear();
        for(int j=0; j<student.size(); j++)
        {
            Studentinfo info1=new Studentinfo();
            info1.setStudentname(student.get(j).getFirstName());
            info1.setStudentid("" + student.get(j).getStudentID());
            info1.setStudentrollnumber(student.get(j).getRollNo());
            info1.setStudentclassid(student.get(j).getClassID());
            info1.setChangebackground(false);
            boolean addtoabsent=false;
            for(int n=0; n<online.size(); n++) {

                if(info1.getStudentid().equalsIgnoreCase(""+online.get(n).getStudentid()))
                {
                    addtoabsent=true;
                    break;
                }
            }
            if(!addtoabsent)
            {
               /* boolean addtostudent=false;
                String rooms[]=student.get(j).getRoominfo().split(",");

                for(int r=0; r<rooms.length; r++)
                {
                    if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                    {
                        addtostudent=true;
                        break;
                    }
                }
                if(addtostudent)*/
                    absent.add(info1);
            }

            adapter=new Attendanceadapter(Attendacecapture.this,
                    R.layout.takeattendance,absent,pref);
            attendancegridview.setAdapter(adapter);
        }
    }
    void marksasabsent()
    {
        adapter=new Attendanceadapter(Attendacecapture.this,
                R.layout.takeattendance,online,pref);
        attendancegridview.setAdapter(adapter);
    }
    void marksasabsentsearch(String get)
    {
        presentsearch.clear();
        presentsearchtext=true;
        for(int j=0; j<online.size(); j++)
        {

            if (online.get(j).getStudentname().toUpperCase().contains(get.toUpperCase()))
                presentsearch.add(online.get(j));


           /* Studentinfo info1=new Studentinfo();
            info1.setStudentname(online.get(j).getFirstName());
            info1.setStudentid("" + online.get(j).getStudentID());
            info1.setStudentrollnumber(student.get(j).getRollNo());
            info1.setChangebackground(false);
            boolean addtoabsent=false;
            for(int n=0; n<online.size(); n++) {

                if(info1.getStudentid().equalsIgnoreCase(""+online.get(n).getStudentid()))
                {
                    addtoabsent=true;
                    break;
                }
            }
            if(!addtoabsent)
            {
                if (info1.getStudentname().toUpperCase().contains(get.toUpperCase()))
                    absent.add(info1);
            }*/

        }
        adapter=new Attendanceadapter(Attendacecapture.this,
                R.layout.takeattendance,presentsearch,pref);
        attendancegridview.setAdapter(adapter);


       /* adapter=new Attendanceadapter(Attendacecapture.this,
                R.layout.takeattendance,online,pref);
        attendancegridview.setAdapter(adapter);*/
    }
    void markaspresentsearch(String get)
    {
        final ArrayList<Studentdetails>student=/*dp.getstudentdetails();*//*dp.getdetails(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("staffid", "0")));*/dp.getdetailsofbatch(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("staffid", "0")), Integer.parseInt(pref.getString("batchid", "0")));/*dp.getstudentdetails();*/

        absent.clear();
        for(int j=0; j<student.size(); j++)
        {
            Studentinfo info1=new Studentinfo();
            info1.setStudentname(student.get(j).getFirstName());
            info1.setStudentid("" + student.get(j).getStudentID());
            info1.setStudentrollnumber(student.get(j).getRollNo());
            info1.setStudentclassid(student.get(j).getClassID());
            info1.setChangebackground(false);
            boolean addtoabsent=false;
            for(int n=0; n<online.size(); n++) {

                if(info1.getStudentid().equalsIgnoreCase(""+online.get(n).getStudentid()))
                {
                    addtoabsent=true;
                    break;
                }
            }
            if(!addtoabsent)
            {
                /*if (info1.getStudentname().toUpperCase().contains(get.toUpperCase())) {

                    boolean addtostudent=false;
                    String rooms[]=student.get(j).getRoominfo().split(",");

                    for(int r=0; r<rooms.length; r++)
                    {
                        if(rooms[r].equalsIgnoreCase(pref.getString("roomid","0")))
                        {
                            addtostudent=true;
                            break;
                        }
                    }
                    if(addtostudent)*/
                if (info1.getStudentname().toUpperCase().contains(get.toUpperCase())) {
                    absent.add(info1);
                }

               // }
            }

            adapter=new Attendanceadapter(Attendacecapture.this,
                    R.layout.takeattendance,absent,pref);
            attendancegridview.setAdapter(adapter);
        }
    }
    void count()
    {
        if(absent.size()>online.size()) {
            absentcounttext.setText("" + (studenbelongclass.size() - online.size()));

        }
        else
        {
            absentcounttext.setText(""+(studenbelongclass.size()-online.size()));
        }
        presencounttext.setText("" + online.size());
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(pref.getBoolean("dnd",false))
        {
            dnd.setImageResource(R.drawable.active);

        }
        else
        {
            dnd.setImageResource(R.drawable.inactive);

        }

        if(LoginActivity.handraise.size()>0)
        {
            handraise.setImageResource(R.drawable.handraiseenable);

            // Utils.Listpopup(BookBinActivity.this);
        }
        else
        {                    handraise.setImageResource(R.drawable.handrise);

            //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
}
