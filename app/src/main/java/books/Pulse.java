package books;
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;


import org.json.JSONArray;
import org.json.JSONObject;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import pulseexam.PulseExamPojo;

public class Pulse extends BaseAdapter {
    private Activity mContext;
    static ArrayList<PulseExamPojo> planListAdapter = new ArrayList<PulseExamPojo>();
    DatabaseHandler db;
    public static int AnswerA;
    public static  int AnswerB ;
    public static int AnswerC;
    public static int AnswerD;
    public static int none;
    public static int totalResponse;
    public static String optionA;
    public static String optionB;
    public static String optionC;
    public static String optionD;
    public static String createQuestion;
    SharedPreferences pref;
    Utilss utils;






    private LayoutInflater mLayoutInflater = null;
    public Pulse(Activity context, ArrayList<PulseExamPojo> list,SharedPreferences pref) {
        mContext = context;
        planListAdapter = list;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        utils=new Utilss(context);
        this.pref=pref;
    }
    @Override
    public int getCount() {
        return planListAdapter.size();
    }
    @Override
    public Object getItem(int pos) {
        return planListAdapter.get(pos);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
       db= new DatabaseHandler(mContext,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        View v = convertView;
        CompleteListViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.pulse_question_layout, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        } else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }


        viewHolder.relativeParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                JSONArray optionArray = new JSONArray();
                JSONArray optionArray1 = new JSONArray();

                JSONObject list1 = new JSONObject();


                Intent in=new Intent("PulseStart");
                Bundle mBundle = new Bundle();
                in.putExtras(mBundle);
                mContext.sendBroadcast(in);
                try {


                    List<PulseExamPojo> pulsedetailsList = db.getAllPulseExamDetailsUsingId(planListAdapter.get(position).getPulseQuestionID());
                    for (PulseExamPojo cn : pulsedetailsList) {


                        String log = "PulseQuestionID: " + cn.getPulseQuestionID() + " ,PulseQuestion: " + cn.getPulseQuestion() + " ,PulseType: " +
                                cn.getPulseType() + " ,AnswerOption_A: " +
                                cn.getAnswerOption_A() + " ,AnswerOption_B: " + cn.getAnswerOption_B() + " ,AnswerOption_C: " +
                                cn.getAnswerOption_C() + " ,AnswerOption_D: " + cn.getAnswerOption_D() + " ,CorrectAnswerOption: " + cn.getCorrectAnswerOption() +
                                " ,VersionPulse " +
                                cn.getVersionPulse() + " ,StaffIDPluse: " + cn.getStaffIDPluse() + " ,SchoolIDPulse: " + cn.getSchoolIDPulse() +
                                " ,IsActivePulse: " + cn.getIsActivePulse() + " ,CreatedOnPluse: " + cn.getCreatedOnPluse()+ " ,UpdatedOn: " + cn.getUpdatedOn();

                        optionArray.put(cn.getAnswerOption_A());
                        optionArray.put(cn.getAnswerOption_B());
                        if(!(cn.getAnswerOption_C().equalsIgnoreCase(" "))){
                            optionArray.put(cn.getAnswerOption_C());

                        }
                        if(!(cn.getAnswerOption_D().equalsIgnoreCase(" "))){
                            optionArray.put(cn.getAnswerOption_D());

                        }

                        // Writing Contacts to log
                        Log.d("PulseQuestionID: ", log);

                    }


                    for(int c=0; c<optionArray.length(); c++){
                        optionArray1.put(optionArray.get(c));
                    }
                    parent.put("PulseQuestionId", planListAdapter.get(position).getPulseQuestionID()).toString();
                    parent.put("PulseQuestion", planListAdapter.get(position).getPulseQuestion());
                    parent.put("Options", optionArray1);

                    String senddata = "Pulsequestion" + "@" + parent.toString();
                    String jsonFormattedString = senddata.replaceAll("\\\\", "");

                    Log.d("output", parent.toString());
                    new clientThread(MultiThreadChatServerSync.thread, jsonFormattedString).start();

                    Intent myIntent = new Intent(mContext, TimerPulseActivity.class);
                    TimerPulseActivity.questionID.add(planListAdapter.get(position).getPulseQuestionID());
                    mContext.startActivity(myIntent);

                    Pulse.none=0;
                    Pulse.AnswerA=0;
                    Pulse.AnswerB=0;
                    Pulse.AnswerC=0;
                    Pulse.AnswerD=0;
                    Pulse.totalResponse=0;

                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
        viewHolder.mTVItem.setText(planListAdapter.get(position).getPulseQuestion());
        utils.setTextviewtypeface(1,viewHolder.mTVItem);
        utils.setTextviewtypeface(5,viewHolder.pulseBtn);

        return v;
    }
}
class CompleteListViewHolder {
    public TextView mTVItem;
    public TextView pulseBtn;
    public RelativeLayout relativeParent;
    public CompleteListViewHolder(View base) {
        pulseBtn= (TextView) base.findViewById(R.id.pulseBtn);
        mTVItem = (TextView) base.findViewById(R.id.label);
        relativeParent = (RelativeLayout) base.findViewById(R.id.relativeParent);


    }
}
