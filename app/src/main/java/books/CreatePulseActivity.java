package books;

/**
 * Created by pratheeba on 5/26/2017.
 */
import java.util.ArrayList;
import java.util.List;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.edukool.teacher.R;

import org.json.JSONArray;
import org.json.JSONObject;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import pulseexam.PulseExamPojo;

public class CreatePulseActivity extends Activity  {
    private ListView mCompleteListView;
    private ArrayList<PulseExamPojo> mItems;
    private List<Integer> mQuestionId;
    private Pulse mListAdapter;
    DatabaseHandler db;
    EditText pulsequestion,option4,option2,option3,option1;
    ImageView close;
    String selectedAnswer;
    BroadcastReceiver questionreceiver1;
    RadioButton none,radio1,radio2,radio3,radio4;
    Button create,pulse,cancel;
    TextView choose_pulse,title_options,canvas;
    Utilss utils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.create_pulse_lay);
       // db=new DatabaseHandler(this);
        this.setFinishOnTouchOutside(false);
        utils=new Utilss(this);
        cancel=(Button)findViewById(R.id.cancel);
        choose_pulse=(TextView)findViewById(R.id.choose_pulse);
        title_options=(TextView)findViewById(R.id.title_options);
        canvas=(TextView)findViewById(R.id.canvas);
        close= (ImageView) findViewById(R.id.close);
        pulsequestion= (EditText) findViewById(R.id.pulsequestion);
        option1= (EditText) findViewById(R.id.option1);
        option2= (EditText) findViewById(R.id.option2);
        option3= (EditText) findViewById(R.id.option3);
        option4= (EditText) findViewById(R.id.option4);
        none = (RadioButton)findViewById(R.id.none);
        radio1 = (RadioButton)findViewById(R.id.radio1);
        radio2 = (RadioButton)findViewById(R.id.radio2);
        radio3 = (RadioButton)findViewById(R.id.radio3);
        radio4 = (RadioButton)findViewById(R.id.radio4);
        pulse = (Button)findViewById(R.id.pulse);
        utils.setTextviewtypeface(3,choose_pulse);
        utils.setTextviewtypeface(5,option1);
        utils.setTextviewtypeface(5,option2);

        utils.setTextviewtypeface(5,option3);
        utils.setTextviewtypeface(5,option4);
        utils.setButtontypeface(1,pulse);
        utils.setTextviewtypeface(4,canvas);
        utils.setTextviewtypeface(4,title_options);






        close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        none.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = "none";
                radio1.setChecked(false);
                radio2.setChecked(false);
                radio3.setChecked(false);
            }
        });
        radio1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = "A";
                none.setChecked(false);
                radio2.setChecked(false);
                radio3.setChecked(false);
            }
        });
        radio2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = "B";
                radio1.setChecked(false);
                none.setChecked(false);
                radio3.setChecked(false);

            }
        });
        radio3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = "C";
                radio1.setChecked(false);
                radio2.setChecked(false);
                none.setChecked(false);

            }
        });
        radio4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedAnswer = "D";
            }
        });

        pulse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
              if(pulsequestion.getText().toString().equalsIgnoreCase("")){
                  Toast.makeText(getApplicationContext(), "Please enter pulse question", Toast.LENGTH_LONG).show();
              }
                else if(option1.getText().toString().equalsIgnoreCase("")){
                  Toast.makeText(getApplicationContext(), "Please enter option-A question", Toast.LENGTH_LONG).show();
              }
                else if(option2.getText().toString().equalsIgnoreCase("")){
                  Toast.makeText(getApplicationContext(), "Please enter option-B question", Toast.LENGTH_LONG).show();
              }


                else{
                  JSONObject parent = new JSONObject();
                  JSONArray optionArray1 = new JSONArray();

                  optionArray1.put(option1.getText().toString());
                  optionArray1.put(option2.getText().toString());

                  if(!option3.getText().toString().equalsIgnoreCase("")){
                      optionArray1.put(option3.getText().toString());

                  }
                  if(!option4.getText().toString().equalsIgnoreCase("")) {
                      optionArray1.put(option4.getText().toString());
                  }

                  try {
                      parent.put("PulseQuestionId", "0");
                      parent.put("PulseQuestion", pulsequestion.getText().toString());
                      parent.put("Options", optionArray1);

                      String senddata = "Pulsequestion" + "@" + parent.toString();
                      String jsonFormattedString = senddata.replaceAll("\\\\", "");
                      new clientThread(MultiThreadChatServerSync.thread, jsonFormattedString).start();
                      Log.d("output", parent.toString());
                      Pulse.createQuestion = pulsequestion.getText().toString();
                      Pulse.optionA =option1.getText().toString();
                      Pulse.optionB =option2.getText().toString();
                      Pulse.optionC =option3.getText().toString();
                      Pulse.optionD =option4.getText().toString();
                      finish();
                      Intent myIntent = new Intent(getApplicationContext(), TimerPulseActivity.class);
                      TimerPulseActivity.questionID.add(0);
                      startActivity(myIntent);
                  }
                  catch (Exception e){
                      e.printStackTrace();
                  }


              }

            }
        });

    }



}

