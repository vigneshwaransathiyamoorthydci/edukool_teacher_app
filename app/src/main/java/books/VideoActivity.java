package books;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.dci.edukool.teacher.R;

import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;


public class VideoActivity extends Activity {


    private VideoView videoView;
    private int position = 0;
    private MediaController mediaController;
    ImageView back;
    ImageView sync,full;
    Boolean fullscreen= false;
    ImageView pulse;
    String videopath;
    TextView bookname;
    RelativeLayout relativeVideo;
    String data,booknameintent;
    boolean ac;
    boolean videosyn;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videolayout);
       back=(ImageView)findViewById(R.id.exit);
        sync= (ImageView) findViewById(R.id.videsyn);

        videoView = (VideoView) findViewById(R.id.videoView);
        relativeVideo = (RelativeLayout)findViewById(R.id.relativeVideo);
        bookname = (TextView) findViewById(R.id.bookname);
        full = (ImageView) findViewById(R.id.full);
        Intent i=getIntent();
        Bundle bun=i.getExtras();
        videopath= bun.getString("path");
        data=getIntent().getStringExtra("senddata");
        booknameintent = getIntent().getStringExtra("bookname");
        if (booknameintent!= null && !booknameintent.equals("")){
            bookname.setText(booknameintent);
        }
        videosyn=false;
        ac=getIntent().getBooleanExtra("ac",false);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);

        pulse= (ImageView) findViewById(R.id.pulse);
        pulse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoActivity.this, PulseQuestionActivity.class);
                startActivity(intent);
            }
        });

        // Set the media controller buttons
        if (mediaController == null) {
            mediaController = new MediaController(VideoActivity.this);

            // Set the videoView that acts as the anchor for the MediaController.
           /* mediaController.setAnchorView(videoView);*/


            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }

        full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fullscreen){
                    RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                    params.addRule(RelativeLayout.BELOW, 0);
                    params.addRule(RelativeLayout.ABOVE, 0);
                    params.setMargins(0,0,0,0);
                    relativeVideo.setLayoutParams(params);
                    fullscreen = true;
                    full.setBackgroundResource(R.drawable.fullscreenexit);
                }
                else {
                    RelativeLayout.LayoutParams params= new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
                    params.addRule(RelativeLayout.BELOW, R.id.parentlayout);
                    params.addRule(RelativeLayout.ABOVE, R.id.bottomlayout);
                    params.setMargins(10,10,10,10);
                    relativeVideo.setLayoutParams(params);
                    full.setBackgroundResource(R.drawable.fullscreenentry);
                    fullscreen = false;
                }

            }
        });


        try {
            Log.d("videopath", "onCreate: "+videopath);
            // ID of video file.
            int id = this.getRawResIdByName("isac");
            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + id));
          videoView.setVideoURI(Uri.parse(videopath));
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }


        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!videosyn) {
                        new clientThread(MultiThreadChatServerSync.thread, data).start();

                        sync.setImageResource(R.drawable.unsync);
                        videosyn = true;

                    } else {
                        videosyn = false;
                        sync.setImageResource(R.drawable.bookbinsync);

                        new clientThread(MultiThreadChatServerSync.thread, "FileClose").start();

                    }
                }
                else
                {
                    Utilss.lockpoopup(VideoActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                }
            }
        });
        videoView.requestFocus();


        // When the video file ready for playback.
        videoView.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  change;
                if(ac)
                {
                    change="Academic Book";
                }
                else {
                    change="References";

                }
                if(!videosyn)
                VideoActivity.this.finish();
                else
                {
                    Utilss.lockpoopup(VideoActivity.this, change, getResources().getString(R.string.whiteboardsync1));

                }
            }
        });
    }

    // Find ID corresponding to the name of the resource (in the directory raw).
    public int getRawResIdByName(String resName) {
        String pkgName = this.getPackageName();
        // Return 0 if not found.
        int resID = this.getResources().getIdentifier(resName, "raw", pkgName);
        Log.i("AndroidVideoView", "Res Name: " + resName + "==> Res ID = " + resID);
        return resID;
    }


    // When you change direction of phone, this method will be called.
    // It store the state of video (Current position)
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // Store current position.
        savedInstanceState.putInt("CurrentPosition", videoView.getCurrentPosition());
        videoView.pause();
    }


    // After rotating the phone. This method is called.
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Get saved position.
        position = savedInstanceState.getInt("CurrentPosition");
        videoView.seekTo(position);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }
}