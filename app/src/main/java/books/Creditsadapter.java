package books;

/**
 * Created by abimathi on 26-May-17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import helper.RoundedImageView;
import helper.Studentdetails;


/**
 * Created by iyyapparajr on 5/14/2017.
 */

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Creditsadapter extends ArrayAdapter<Studentdetails> {

    private ArrayList<Studentdetails> studentinfo;
    Context con;
    SharedPreferences pref;
    DatabaseHandler db;
     Utilss utils;
    // CoolReader mactivity;

    public Creditsadapter(Context context, int textViewResourceId,
                             ArrayList<Studentdetails> studentinfo,SharedPreferences pref) {
        super(context, textViewResourceId, studentinfo);
        this.studentinfo = studentinfo;
        this.pref=pref;
        utils=new Utilss((Activity) context);
        con=context;
       // this.studentinfo.addAll(studentinfo);
       // db=new DatabaseHandler((Activity)con);
      db= new DatabaseHandler((Activity)con,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
    }

    private class ViewHolder {

        RoundedImageView studentimage;
        TextView studentrollnumber;
        TextView studentname;
        TextView overallcredits;
        TextView obtaincredits;
        LinearLayout back;
        TextView tilldate;
        TextView thismonth;
        RelativeLayout parentlayout;
        LinearLayout background;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.creditsitem, null);
            holder = new ViewHolder();
            holder.studentimage= (RoundedImageView) convertView.findViewById(R.id.studentpoto);
            holder.studentname= (TextView) convertView.findViewById(R.id.name);
            holder.studentrollnumber= (TextView) convertView.findViewById(R.id.rollno);
            holder.overallcredits= (TextView) convertView.findViewById(R.id.totalcredits);
            holder.obtaincredits= (TextView) convertView.findViewById(R.id.obtainedcredits);
            holder.tilldate= (TextView) convertView.findViewById(R.id.tildate);
            holder.thismonth= (TextView) convertView.findViewById(R.id.thismonth);
            holder.background= (LinearLayout) convertView.findViewById(R.id.background);
           // holder.back= (LinearLayout) convertView.findViewById(R.id.back);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }


        Studentdetails studentdetails=studentinfo.get(position);


        try
        {
            try {
                File imgfile=new File(studentdetails.getPhotoFilename());
                Picasso.with(con).load(imgfile).placeholder(R.drawable.user_icons).into(holder.studentimage);
            }
            catch (Exception e)
            {

            }
           // setbackground(holder.studentimage,studentdetails.getPhotoFilename());
        }
        catch (Exception e)
        {

        }
        if(position%2==0)
        {
          //  holder.background.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.newbackcolor)));

        }
        else
        {
           // holder.background.setBackgroundDrawable(new ColorDrawable(con.getResources().getColor(R.color.oldbg)));

        }
        holder.studentname.setText(studentdetails.getFirstName());
        if(!studentdetails.getRollNo().equalsIgnoreCase("0")) {
            holder.studentrollnumber.setText(studentdetails.getRollNo().toString());
        }
        else
        {
            holder.studentrollnumber.setText("-");

        }
        holder.overallcredits.setText(""+studentdetails.getTeachercredits());
        holder.obtaincredits.setText(""+studentdetails.getStudentcredits());
        double tildat=((double)studentdetails.getStudentcredits()/(double)studentdetails.getTeachercredits())*100;
        double month=((double)studentdetails.getThismonthcreditforstudent()/(double)studentdetails.getThismonthteachercredit())*100;
        String til = String.format("%.2f", tildat);
        String mon = String.format("%.2f", month);


        holder.thismonth.setText(""+mon+ "%");
        holder.tilldate.setText(""+til+ "%");
        utils.setTextviewtypeface(5,holder.thismonth);
        utils.setTextviewtypeface(5,holder.overallcredits);
        utils.setTextviewtypeface(5,holder.obtaincredits);
        utils.setTextviewtypeface(5,holder.studentrollnumber);
        utils.setTextviewtypeface(5,holder.studentname);
        utils.setTextviewtypeface(5,holder.tilldate);




      /*  if(studentinfo.get(position).isVisibornot())
        {
            convertView.setVisibility(View.VISIBLE);
        }
        else
        {
            convertView.setVisibility(View.G);
        }*/

        //Toast.makeText(con,studentinfo.get(position).getStudentname(),Toast.LENGTH_SHORT).show();
        //Toast.makeText(con,details.getPhotoFilename(),Toast.LENGTH_SHORT).show();

        // holder.code.setText(" (" +  country.getCode() + ")");
      /*  holder.name.setText(videoname.getName());

        holder.name.setTag(videoname);
*/


        return convertView;

    }
    void setbackground(ImageView view,String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }


}