package books;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import Utilities.DatabaseHandler;
import pulseexam.PulseExamPojo;

public class PulseBarActivity extends BaseActivity {
    BarDataSet bardataset;
    ArrayList<String> labels;
    DatabaseHandler db;
    String PulseQuestion;
    float correctA;
    float correctB;
    float correctC;
    float correctD;
    float none;
    BarChart barChart;
    int n=1;
    int m=1;
    int o=1;
    int p=1;
    int q=1;
    public static List<Integer> correctASize = new ArrayList<Integer>();
    public static List<Integer> correctBSize = new ArrayList<Integer>();
    public static List<Integer> correctCSize = new ArrayList<Integer>();
    public static List<Integer>  correctDSize = new ArrayList<Integer>();
    public static List<Integer> noneSize = new ArrayList<Integer>();

    public static List<String> correctApercentage = new ArrayList<String>();
    public static List<String> correctBpercentage = new ArrayList<String>();
    public static List<String> correctCpercentage = new ArrayList<String>();
    public static List<String>  correctDpercentage = new ArrayList<String>();
    public static List<String> nonepercentage = new ArrayList<String>();

    JSONArray optionArray;
    ImageView close;
    TextView class_name;
    SharedPreferences pref;
    TextView questionpul;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        this.setFinishOnTouchOutside(false);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);

        //db=new DatabaseHandler(this);
        db= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        optionArray = new JSONArray();
        barChart = (BarChart) findViewById(R.id.barchart);
        close  = (ImageView) findViewById(R.id.close);
        questionpul= (TextView) findViewById(R.id.txtques);
        class_name = (TextView) findViewById(R.id.class_name);
        class_name.setText( pref.getString("classname", ""));
        int SurveyPulseQuestionID= TimerPulseActivity.questionID.get(0);
       if(SurveyPulseQuestionID==0){
          // PulseQuestion = Pulse.createQuestion;
           //optionArray.put(Pulse.optionA);
          // optionArray.put(Pulse.optionB);
           //if(!(cn1.getAnswerOption_C().equalsIgnoreCase(" "))){
           //optionArray.put(Pulse.optionC);

           // }
           // if(!(cn1.getAnswerOption_D().equalsIgnoreCase(" "))){
           //optionArray.put(Pulse.optionD);

           questionpul.setText(Pulse.createQuestion);
           labels = new ArrayList<String>();
           labels.add(Pulse.optionA);
           labels.add(Pulse.optionB);
           labels.add(Pulse.optionC);
           labels.add(Pulse.optionD);
           labels.add("none");

       }
        else{
           List<PulseExamPojo> pulsedetailsList =
                   db.getAllPulseExamDetailsLastUsingId(SurveyPulseQuestionID);

           if(pulsedetailsList.size()==0){
               questionpul.setText(PulseQuestion);
               labels = new ArrayList<String>();
               labels.add("test1");
               labels.add("test2");
               labels.add("test3");
               labels.add("test4");
               labels.add("none");
           }else {
               for (PulseExamPojo cn1 : pulsedetailsList) {


                   String log1 = "PulseQuestionID: " + cn1.getPulseQuestionID() + " ,PulseQuestion: " + cn1.getPulseQuestion() + " ,PulseType: " +
                           cn1.getPulseType() + " ,AnswerOption_A: " +
                           cn1.getAnswerOption_A() + " ,AnswerOption_B: " + cn1.getAnswerOption_B() + " ,AnswerOption_C: " +
                           cn1.getAnswerOption_C() + " ,AnswerOption_D: " + cn1.getAnswerOption_D() + " ,CorrectAnswerOption: " + cn1.getCorrectAnswerOption() +
                           " ,VersionPulse " +
                           cn1.getVersionPulse() + " ,StaffIDPluse: " + cn1.getStaffIDPluse() + " ,SchoolIDPulse: " + cn1.getSchoolIDPulse() +
                           " ,IsActivePulse: " + cn1.getIsActivePulse() + " ,CreatedOnPluse: " + cn1.getCreatedOnPluse() + " ,UpdatedOn: " + cn1.getUpdatedOn();
                   PulseQuestion = cn1.getPulseQuestion();
                   optionArray.put(cn1.getAnswerOption_A());
                   optionArray.put(cn1.getAnswerOption_B());
                   //if(!(cn1.getAnswerOption_C().equalsIgnoreCase(" "))){
                   optionArray.put(cn1.getAnswerOption_C());

                   // }
                   // if(!(cn1.getAnswerOption_D().equalsIgnoreCase(" "))){
                   optionArray.put(cn1.getAnswerOption_D());

                   // }

                   questionpul.setText(PulseQuestion);
                   labels = new ArrayList<String>();
                   labels.add(cn1.getAnswerOption_A());
                   labels.add(cn1.getAnswerOption_B());
                   labels.add(cn1.getAnswerOption_C());
                   labels.add(cn1.getAnswerOption_D());
                   labels.add("none");

                   // Writing Contacts to log
                   Log.d("PulseQuestionID: ", log1);

               }
           }
       }

   /*     List<PulseSurveyPojo> PulseSurveyPojo = db.PulseSurveyUsingExamId(SurveyPulseQuestionID);
        for (PulseSurveyPojo cn : PulseSurveyPojo) {

            String log = "getPulseSurveyID: " + cn.getPulseSurveyID() +
                    " ,getPulseQuestionID: " + cn.getPulseQuestionID() +
                    " ,getOptionA_Percentage: " + cn.getOptionA_Percentage() + " ,getOptionB_Percentage: "
                    + cn.getOptionB_Percentage() + " ,getOptionC_Percentage: " + cn.getOptionC_Percentage()
                    +  " ,getOptionD_Percentage: " + cn.getOptionD_Percentage();
            // Writing Contacts to log
            Log.d("survey: ", log);
            correctApercentage.add(cn.getOptionA_Percentage() + "");
            correctBpercentage.add(cn.getOptionB_Percentage()+"");
            correctCpercentage.add(cn.getOptionC_Percentage()+"");
            correctDpercentage.add(cn.getOptionD_Percentage()+"");
            nonepercentage.add(cn.getNoResponse_Percentage()+"");

        }*/

/*
        for( int i=0; i<PulseSurveyPojo.size();i++){
            if (correctApercentage.get(i) == "1.0") {
                correctASize.add(i);
            } else if (correctBpercentage.get(i) == "2.0") {
                correctBSize.add(i);
            } else if (correctCpercentage.get(i) == "3.0") {
                correctCSize.add(i);
            } else if (correctDpercentage.get(i) == "4.0") {
                correctDSize.add(i);
            } else if (nonepercentage.get(i) =="5.0"){
                noneSize.add(i);
            }

        }*/



        correctA =Pulse.AnswerA;
        correctB =Pulse.AnswerB;
        correctC =Pulse.AnswerC;
        correctD =Pulse.AnswerD;
        none =Pulse.none;
        ArrayList<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(Math.round(correctA),0)); //x:0.0 y:0.0
        entries.add(new BarEntry(Math.round(correctB), 1)); //x:1.0 y:1.0
        entries.add(new BarEntry(Math.round(correctC),2)); //x:0.0 y:2.0
        entries.add(new BarEntry(Math.round(correctD), 3)); //x:0.0 y:3.0
        entries.add(new BarEntry(Math.round(none),4));     //x:0.0 y 4.0
       /* entries.add(new BarEntry(0,Math.round(correctA) )); //x:0.0 y:0.0
        entries.add(new BarEntry(1, Math.round(correctB))); //x:1.0 y:1.0
        entries.add(new BarEntry(2,Math.round(correctC) )); //x:0.0 y:2.0
        entries.add(new BarEntry(3, Math.round(correctD))); //x:0.0 y:3.0
        entries.add(new BarEntry(4,Math.round(none)));     //x:0.0 y 4.0*/
        bardataset = new BarDataSet(entries, "Responses");

        BarData data = new BarData(labels,bardataset);
        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(ContextCompat.getColor(this, R.color.baryes));
        colors.add(ContextCompat.getColor(this, R.color.barno));
        colors.add(ContextCompat.getColor(this, R.color.bookbg));
        colors.add(ContextCompat.getColor(this, R.color.green));
        colors.add(ContextCompat.getColor(this, R.color.transparent));

        bardataset.setColors(colors);
        bardataset.setBarSpacePercent(70f);
       // barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
        barChart.setData(data);
     //   bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        barChart.animateY(3000);
        barChart.setDrawGridBackground(false);
        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
      //  barChart.getAxisRight().setDrawGridLines(false);
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);
      //  barChart.getDescription().setEnabled(false);
//<---------------------------------------------------->

        // create BarEntry for Bar Group 1
        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
        bargroup1.add(new BarEntry(8f, 0));
        bargroup1.add(new BarEntry(2f, 1));
        bargroup1.add(new BarEntry(5f, 2));
        bargroup1.add(new BarEntry(20f, 3));
        bargroup1.add(new BarEntry(15f, 4));
        bargroup1.add(new BarEntry(19f, 5));

// create BarEntry for Bar Group 1
        ArrayList<BarEntry> bargroup2 = new ArrayList<>();
        bargroup2.add(new BarEntry(6f, 0));
        bargroup2.add(new BarEntry(10f, 1));
        bargroup2.add(new BarEntry(5f, 2));
        bargroup2.add(new BarEntry(25f, 3));
        bargroup2.add(new BarEntry(4f, 4));
        bargroup2.add(new BarEntry(17f, 5));

        // creating dataset for Bar Group1
        BarDataSet barDataSet1 = new BarDataSet(bargroup1, "Bar Group 1");

//barDataSet1.setColor(Color.rgb(0, 155, 0));
       // barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
        // creating dataset for Bar Group 2
        BarDataSet barDataSet2 = new BarDataSet(bargroup2, "Bar Group 2");
      //  barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("2016");
        labels.add("2015");
        labels.add("2014");
        labels.add("2013");
        labels.add("2012");
        labels.add("2011");


// initialize the Bardata with argument labels and dataSet
        //BarData data1 = new BarData(barDataSet1);
      //  barChart.setData(data1);
//<---------------------------------------------------------------------------------->
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)



            {
                TimerPulseActivity.questionID.clear();
                db.deletesurveyresponse();
                db.deletesurvey();
                Pulse.none=0;
                Pulse.AnswerA=0;
                Pulse.AnswerB=0;
                Pulse.AnswerC=0;
                Pulse.AnswerD=0;
                Pulse.totalResponse=0;
                finish();
            }
        });


//        HorizontalBarChart barChart = (HorizontalBarChart) findViewById(R.id.barchart);
//
//        // create BarEntry for Bar Group 1
//        ArrayList<BarEntry> bargroup1 = new ArrayList<>();
//        bargroup1.add(new BarEntry(8f, 0));
//        bargroup1.add(new BarEntry(2f, 1));
//        bargroup1.add(new BarEntry(5f, 2));
//        bargroup1.add(new BarEntry(20f, 3));
//        bargroup1.add(new BarEntry(15f, 4));
//        bargroup1.add(new BarEntry(19f, 5));
//
//        // create BarEntry for Bar Group 1
//        ArrayList<BarEntry> bargroup2 = new ArrayList<>();
//        bargroup2.add(new BarEntry(6f, 0));
//        bargroup2.add(new BarEntry(10f, 1));
//        bargroup2.add(new BarEntry(5f, 2));
//        bargroup2.add(new BarEntry(25f, 3));
//        bargroup2.add(new BarEntry(4f, 4));
//        bargroup2.add(new BarEntry(17f, 5));
//
//        BarDataSet barDataSet1 = new BarDataSet(bargroup1, "Bar Group 1");  // creating dataset for group1
//
//        //barDataSet1.setColor(Color.rgb(0, 155, 0));
//        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
//
//        BarDataSet barDataSet2 = new BarDataSet(bargroup2, "Brand 2"); // creating dataset for group1
//        barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
//
//        ArrayList<String> labels = new ArrayList<String>();
//        labels.add("2016");
//        labels.add("2015");
//        labels.add("2014");
//        labels.add("2013");
//        labels.add("2012");
//        labels.add("2011");
//
//        ArrayList<BarDataSet> dataSets = new ArrayList<>();  // combined all dataset into an arraylist
//        dataSets.add(barDataSet1);
//        dataSets.add(barDataSet2);
//        BarData data = new BarData(labels, dataSets); // initialize the Bardata with argument labels and dataSet
//        barChart.setData(data);
//
//
//
//
//        barChart.setDescription("Set Bar Chart Description");  // set the description
//
//        barChart.animateY(5000);

    }

    @Override
    public int getlayout() {
        return R.layout.bar_lay;
    }


    @Override
    public void onBackPressed() {


    }

    @Override
    protected void onResume() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onResume();
    }
}
