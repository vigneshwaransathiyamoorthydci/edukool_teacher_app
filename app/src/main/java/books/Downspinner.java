package books;

/**
 * Created by iyyapparajr on 5/8/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;
import Utilities.Utilss;


import helper.tblclasses;

/**
 * Created by iyyapparajr on 5/6/2017.
 */
public class Downspinner extends ArrayAdapter {


    Utilss utils;
    private Context context;
    private ArrayList<tblclasses> itemList;
    Activity act;
    public Downspinner(Context context, int textViewResourceId,ArrayList<tblclasses> itemList) {
        super(context, textViewResourceId,itemList);
        this.context=context;
        act= (Activity) context;
        this.itemList=itemList;
        utils=new Utilss(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.dropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        if(!itemList.get(position).isRommornot())
        make.setText(itemList.get(position).getClassCode());
        else
            make.setText(itemList.get(position).getClassName());
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.downdropdownview, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.downdropdownview);
        /*Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");
        v.setTypeface(myTypeFace);
        v.setText(itemList.get(position));*/
        utils.setTextviewtypeface(2,make);

        if(!itemList.get(position).isRommornot())
            make.setText(itemList.get(position).getClassCode());
        else
            make.setText(itemList.get(position).getClassName());
        return row;
    }

}
