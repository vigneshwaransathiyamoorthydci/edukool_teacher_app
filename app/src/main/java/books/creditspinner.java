package books;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;

import Utilities.Utilss;

/**
 * Created by abimathi on 16-May-17.
 */
public class creditspinner extends ArrayAdapter {


    Utilss utils;
    private Context context;
    private ArrayList<String> itemList;
    Activity act;
    public creditspinner(Context context, int textViewResourceId,ArrayList<String> itemList) {

        super(context, textViewResourceId,itemList);
        this.context=context;
        act= (Activity) context;
        this.itemList=itemList;
        utils=new Utilss(act);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.creditspinneritem, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
       make.setText(itemList.get(position));
        return row;
    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.creditspinneritem, parent,
                false);
        TextView make = (TextView) row.findViewById(R.id.dropdwonview);
        utils.setTextviewtypeface(2,make);
       /* Typeface myTypeFace = Typeface.createFromAsset(context.getAssets(),
                "fonts/gilsanslight.otf");*/
        //v.setTypeface(myTypeFace);
        make.setText(itemList.get(position));

      //  make.setText(itemList.get(position).getClassCode());

        return row;
    }

}