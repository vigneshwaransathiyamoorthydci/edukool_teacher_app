package books;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import Utilities.Utilss;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
 public class Multimediaadapter extends ArrayAdapter<Videoname> {

    private ArrayList<Videoname> countryList;
    Context con;
    Utilss util;
   // CoolReader mactivity;

    public Multimediaadapter(Context context, int textViewResourceId,
                           ArrayList<Videoname> countryList) {
        super(context, textViewResourceId, countryList);
        this.countryList = new ArrayList<Videoname>();
        util=new Utilss((Activity)context);
        con=context;
        this.countryList.addAll(countryList);
    }

    private class ViewHolder {
        ImageView bookiamge;
        TextView  name,view_button;
        RelativeLayout parentlayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.booklistitem, null);

            holder = new ViewHolder();
         
            holder.name = (TextView) convertView.findViewById(R.id.studentnametext);
            holder.view_button=(TextView) convertView.findViewById(R.id.view_button);
            holder.bookiamge = (ImageView) convertView.findViewById(R.id.studntimageview);
            holder.parentlayout = (RelativeLayout) convertView.findViewById(R.id.parentlayout);
            util.setTextviewtypeface(4,holder.name);
            convertView.setTag(holder);

           
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Videoname videoname = countryList.get(position);
       /* holder.parentlayout.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
            
            	
            	//mactivity.startActivity(intent)
            	Uri uri = Uri.parse(videoname.getVideopath());
    			
    			//Uri uri = Uri.parse("file:///android_asset/" + TEST_FILE_NAME);
    			Intent intent = new Intent(con, com.artifex.mupdfdemo.MuPDFActivity.class);
    			intent.putExtra("linkhighlight", true);
    			intent.setAction(Intent.ACTION_VIEW);
    			intent.setData(uri);
    			
    			//if document protected with password
    			intent.putExtra("password", "encrypted PDF password");

    			//if you need highlight link boxes
    			intent.putExtra("linkhighlight", true);

    			//if you don't need device sleep on reading document
    			intent.putExtra("idleenabled", false);
    			
    			//set true value for horizontal page scrolling, false value for vertical page scrolling
    			intent.putExtra("horizontalscrolling", true);

    			//document name
    			intent.putExtra("docname", "PDF document name");
    			
    			con.startActivity(intent);
  
            }
        });*/
       // holder.code.setText(" (" +  country.getCode() + ")");\

        Log.d("dinesh", "getView: "+videoname.getTitlename());

      holder.name.setText(videoname.getTitlename());
      util.setTextviewtypeface(4, holder.name);
        holder.name.setTag(videoname);

        String path=videoname.getVideopath().substring(videoname.getVideopath().lastIndexOf("/") + 1, videoname.getVideopath().length());


        String Splitoffile[]=path.split("\\.");
        ArrayList<String>pdfname=new ArrayList<String>();
        pdfname.add(".pdf");
        pdfname.add(".doc");
        pdfname.add(".ppt");

        ArrayList<String>imagename=new ArrayList<String>();
        imagename.add(".jpeg");
        imagename.add(".png");
        imagename.add(".bmp");
        imagename.add(".jpg");

        ArrayList<String>filename=new ArrayList<String>();
        filename.add(".MP4");
        filename.add(".WAV");
        filename.add(".MP3");
        filename.add(".3GP");
        filename.add(".MP4") ;
        filename.add(".M4A");
        filename.add(".AAC");
        filename.add(".TS");

        filename.add(".wav");


        if(pdfname.contains("."+Splitoffile[1].toLowerCase())) {


            if (position % 10 == 0) {
                holder.bookiamge.setImageResource(R.drawable.bookrose);
            } else if (position % 10 == 1) {
                holder.bookiamge.setImageResource(R.drawable.booklevender);
            } else if (position % 10 == 2) {
                holder.bookiamge.setImageResource(R.drawable.bookblue);
            } else if (position % 10 == 3) {
                holder.bookiamge.setImageResource(R.drawable.bookyellow);
            } else if (position % 10 == 4) {
                holder.bookiamge.setImageResource(R.drawable.bookgreen);
            } else if (position % 10 == 5) {
                holder.bookiamge.setImageResource(R.drawable.bookorange);
            } else if (position % 10 == 6) {
                holder.bookiamge.setImageResource(R.drawable.bookram);
            } else if (position % 10 == 7) {
                holder.bookiamge.setImageResource(R.drawable.bookred);
            } else if (position % 10 == 8) {
                holder.bookiamge.setImageResource(R.drawable.bookliteorange);
            } else if (position % 10 == 9) {
                holder.bookiamge.setImageResource(R.drawable.booksky);
            } else {
                holder.bookiamge.setImageResource(R.drawable.bookrose);
            }

        }
        else  if(imagename.contains("."+Splitoffile[1].toLowerCase())) {
            holder.bookiamge.setImageResource(R.drawable.imageview);

        }
        else
        {
            holder.bookiamge.setImageResource(R.drawable.videoview);


        }
        String pattern1=".jpg";
        String pattern2=".png";
        String pattern3=".JPEG";
        String pattern4=".PNG";
        String pattern5=".pdf";

        try {
            if (videoname.getVideopath().toUpperCase().contains(".mp4".toUpperCase()) || videoname.getVideopath().toUpperCase().contains(".3gp".toUpperCase()) || videoname.getVideopath().toUpperCase().contains(".wmv".toUpperCase())) {

                holder.bookiamge.setImageResource(R.drawable.notebook);
                holder.view_button.setText("View Video");
            } else if (videoname.getVideopath().toUpperCase().endsWith(pattern1.toUpperCase()) || videoname.getVideopath().toUpperCase().endsWith(pattern2.toUpperCase()) || videoname.getVideopath().toUpperCase().endsWith(pattern3.toUpperCase()) || videoname.getVideopath().toUpperCase().endsWith(pattern4.toUpperCase())) {
                holder.bookiamge.setImageResource(R.drawable.notebook);
                holder.view_button.setText("View PDF");
            } else if (videoname.getVideopath().toUpperCase().endsWith(pattern5.toUpperCase())) {
                holder.bookiamge.setImageResource(R.drawable.notebook);
                holder.view_button.setText("View PDF");
            }
        }catch(NullPointerException e){
            Log.d("nullex",e.toString());
        }
        return convertView;

    }

}