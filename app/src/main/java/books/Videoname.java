package books;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Videoname {


    String name = null;



    String videopath=null;
    boolean selected = false;
    String subjectid;
    String titlename;
    String contentid;

    public String getContentid() {
        return contentid;
    }

    public void setContentid(String contentid) {
        this.contentid = contentid;
    }

    public String getTitlename() {
        return titlename;
    }

    public void setTitlename(String titlename) {
        this.titlename = titlename;
    }

    public String getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(String subjectid) {
        this.subjectid = subjectid;
    }

    public Videoname(String videopath, String name, boolean selected,String id,String title,String contentid) {
        super();
        this.videopath = videopath;
        this.name = name;
        this.selected = selected;
        this.subjectid=id;
        this.titlename=title;
        this.contentid=contentid;
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getVideopath() {
        return videopath;
    }

    public void setVideopath(String videopath) {
        this.videopath = videopath;
    }

}
