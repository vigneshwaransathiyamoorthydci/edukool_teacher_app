package books;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import Utilities.*;

import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.MainActivity;
import com.dci.edukool.teacher.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.ebookdroid.ui.viewer.ViewerActivity;
import org.json.JSONArray;
import org.json.JSONObject;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import connection.Examzipfile;
import connection.Filesharingtoclient;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Subjectnameandid;
import helper.Tablecontent;

public class VideoBinActivity extends BaseActivity {

    private int mQuestionIndex = 0;
    LinearLayout hlvCustomList;
    //   SelfTestQuestionAdapter selfevalutation_adapter;
    private Button layoutinputButton;
    // ListView shelflistView;
    GridView bookbinlist;
    List<String> li;
    Videoadapter dataAdapter = null;
    ArrayList<Videoname>videos;
    ImageView exit;
    ImageView back;

    //ArrayList<>bookshelf;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    View header;
    DatabaseHandler dp;
    ArrayList<Tablecontent>table;
    ListView listView;
    ArrayList<Subjectnameandid>subjectname;
    ImageView profilImageView;
    TextView classname,bookheader,title;
    Examzipfile zipfile;
    ImageView handraise;

    ImageView dnd,down;
    String academicpath,refpath;
    Utilss utils;
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
       //dp=new DatabaseHandler(this);
        handraise= (ImageView) findViewById(R.id.handrise);
        title= (TextView) findViewById(R.id.title);
        title.setText("References");
        hlvCustomList = (LinearLayout)findViewById(R.id.hlvCustomList);
        exit= (ImageView) findViewById(R.id.exit);
        back= (ImageView) findViewById(R.id.back);
        down= (ImageView) findViewById(R.id.down);
        classname= (TextView) findViewById(R.id.class_name);
        profilImageView= (ImageView) findViewById(R.id.profileimage);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);
        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dp= new DatabaseHandler(this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

        edit=pref.edit();
        setbackground(profilImageView,pref.getString("image",""));
        classname.setText(pref.getString("classname", ""));
        table=dp.getcontentdetails();
        if(!pref.getBoolean("roomornot",false)) {
            subjectname = dp.getbatchdetails(Integer.parseInt(pref.getString("bookbin", "0")), Integer.parseInt(pref.getString("batchid", "0")) ,Integer.parseInt(pref.getString("staffid","0")));
        }
        else
        {
            subjectname = dp.getbatchdetailsfrommasterinfo();

        }
        bookheader= (TextView) findViewById(R.id.classname);
        bookheader.setText("REFERENCES");


        utils=new Utilss(VideoBinActivity.this);
        academicpath=pref.getString("academic","");
        refpath=pref.getString("reference","");

        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(VideoBinActivity.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {
                    Toast.makeText(VideoBinActivity.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });


        /*edit.putString("bookbin",""+gettable.get(position).getClassID());
        edit.commit();*/

        //bookshelf=new ArrayList<>();


		/*bookshelf.add("Academic");
		bookshelf.add("Reference");*/
      /*  li=new ArrayList<String>();
        li.add("All");
        li.add("Reference");
      */
        videos=new ArrayList<>();
        // shelflistView = (ListView) findViewById(R.id.selflisview);

        try {
            bookbinload();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //   Search_Dir(Environment.getExternalStorageDirectory());
        dataAdapter = new Videoadapter(this,
                R.layout.booklistitem,videos );
        listView= (ListView) findViewById(R.id.booklist);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        loadbookshelf(0);
        if(subjectname.size()>0)
        {
            loadinit();
        }

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dnd= (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((!pref.getBoolean("break",false))) {


                if(pref.getBoolean("dnd",false))
                {
                    dnd.setImageResource(R.drawable.dci);
                    edit.putBoolean("dnd", false);
                    edit.commit();

                    String senddata = "DND";

                    senddata=senddata+"@false";
                    dnd.setImageResource(R.drawable.inactive);


                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                }
                else
                {
                    dnd.setImageResource(R.drawable.active);

                    String senddata = "DND";

                    senddata=senddata+"@true";
                    edit.putBoolean("dnd", true);
                    edit.commit();
                    //  dnd=dnd+1;
                    new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                }
                }
                else
                {
                    Toast.makeText(VideoBinActivity.this,"You can access this feature once you come out of the session break.",Toast.LENGTH_SHORT).show();
                }
            }
        });


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Videoname video = videos.get(position);

                Uri uri = Uri.parse(video.getVideopath());
                File file=new File(video.getVideopath());
                String path=video.getVideopath().substring(video.getVideopath().lastIndexOf("/") + 1, video.getVideopath().length());

                ArrayList<String>filename=new ArrayList<String>();
                filename.add(".MP4");
                filename.add(".WAV");
                filename.add(".MP3");
                filename.add(".3GP");
                filename.add(".MP4") ;
                filename.add(".M4A");
                filename.add(".AAC");
                filename.add(".TS");

                filename.add(".wav");

                ArrayList<String>pdfname=new ArrayList<String>();
                pdfname.add(".pdf");
              /*  pdfname.add(".doc");
                pdfname.add(".ppt");*/

                ArrayList<String>imagename=new ArrayList<String>();
                imagename.add(".jpeg");
                imagename.add(".png");
                imagename.add(".bmp");
                imagename.add(".jpg");


                String Splitoffile[]=path.split("\\.");



            /*    , , , , , , FLAC, OGG, MKA, WAV, MID, XMF, MXMF, RTTTL, RTX, OTA, IMY.*/

                if(filename.contains("."+Splitoffile[1].toUpperCase())) {

                    String senddata="fileopen"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename()+"@"+0;


                    Intent intent = new Intent(VideoBinActivity.this,
                            VideoActivity.class);
                    Bundle bun = new Bundle();
                    //  Log.d("selectimage", );

                    bun.putString("path", video.getVideopath());
                    intent.putExtra("senddata",senddata);
                    intent.putExtra("bookname",video.getTitlename());
                    intent.putExtras(bun);
                    startActivity(intent);

              /*  //Uri uri = Uri.parse("file:///android_asset/" + TEST_FILE_NAME);
                Intent intent = new Intent(.this, com.artifex.mupdfdemo.MuPDFActivity.class);
                intent.putExtra("linkhighlight", true);
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(uri);

                //if document protected with password
                intent.putExtra("password", "encrypted PDF password");

                //if you need highlight link boxes
                intent.putExtra("linkhighlight", true);

                //if you don't need device sleep on reading document
                intent.putExtra("idleenabled", false);

                //set true value for horizontal page scrolling, false value for vertical page scrolling
                intent.putExtra("horizontalscrolling", true);

                //document name
                intent.putExtra("docname", "PDF document name");

                startActivity(intent);
                }
*/
                }
                else if(pdfname.contains("."+Splitoffile[1].toLowerCase())) {


                    String senddata="fileopen"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename()+"@"+0;

                    Intent intent = new Intent(Intent.ACTION_VIEW,  Uri.fromFile(file));
                    intent.setClass(VideoBinActivity.this, ViewerActivity.class);
                    intent.putExtra("senddata", senddata);
                    intent.putExtra("bookname",video.getTitlename());
                    //intent.putExtra("bookname", Uri.fromFile(file).toString());
                    startActivity(intent);

                   /* Intent intent = new Intent(VideoBinActivity.this, com.artifex.mupdfdemo.MuPDFActivity.class);
                    intent.putExtra("linkhighlight", true);
                    intent.putExtra("senddata", senddata);
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(uri);

                    //if document protected with password
                    intent.putExtra("password", "encrypted PDF password");

                    //if you need highlight link boxes
                    intent.putExtra("linkhighlight", true);

                    //if you don't need device sleep on reading document
                    intent.putExtra("idleenabled", false);

                    //set true value for horizontal page scrolling, false value for vertical page scrolling
                    intent.putExtra("horizontalscrolling", true);

                    //document name
                    intent.putExtra("docname", video.getTitlename());

                    startActivity(intent);*/


                }
                else if(imagename.contains("."+Splitoffile[1].toLowerCase()))
                {
                    String senddata="fileopen"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename()+"@"+0;


                    Intent intent = new Intent(VideoBinActivity.this,
                            ImageActivity.class);
                    Bundle bun = new Bundle();
                    //  Log.d("selectimage", );

                    bun.putString("path", video.getVideopath());
                    intent.putExtra("senddata",senddata);
                    intent.putExtras(bun);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(VideoBinActivity.this,"Format not supported",Toast.LENGTH_SHORT).show();
                }
                }
        });
        /*listView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


              /*  File file = new File(
                        Environment.getExternalStorageDirectory(),
                        "screenshots.zip");*/
                if(!pref.getString("roomname","").equalsIgnoreCase("Session Break")) {
                    Videoname video = videos.get(position);

                    File file = new File(video.getVideopath());


                    stratExamCompletedPopup(video);
                }
                else {
                    Utilss.lockpoopup(VideoBinActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                    //  Utils.lockpoopup(VideoBinActivity.this, "Break", "You are in Session Break");

                }
            /*    String senddata="Filesharing"+"@"+file.getName()+"@"+file.length();
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();
                if (startshare == null) {
                    startshare = new Filesharingtoclient(file);
                    startshare.start();


                } else {
                    startshare.stopsocket();
                    startshare = null;
                    startshare = new Filesharingtoclient(file);
                    startshare.start();
                }*/

                return true;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new validateUserTask().execute("");
            }
        });
        utils.setTextviewtypeface(3,title);


    }

    @Override
    public int getlayout() {
        return R.layout.bookbinmain;
    }


    private class validateUserTask extends AsyncTask<String, Void, String> {
        String response;
        ProgressDialog dia;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia = new ProgressDialog(VideoBinActivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            //  String userid = "";
            String userid = "Function:SyncReference|UserId:" + pref.getString("staffid", "0") + "|UserType:Staff"+"|Portal User ID:"+pref.getString("portalstaffid", "0");

            String res = null;
            byte[] data;

            try {
                data = userid.getBytes("UTF-8");
                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                if (utils.hasConnection()) {
                    postParameters.clear();
                    postParameters.add(new BasicNameValuePair("WS", base64_register));
                    Service sr = new Service(VideoBinActivity.this);
                    res = sr.getLogin(postParameters,Url.baseurl
                           /* "http://api.schoolproject.dci.in/api/"*/);
                   /* response = CustomHttpClient.executeHttpPost(params[0], postParameters);*/
                    // res = response.toString();
                    Log.e("response", "resoibse" + res + ",,");
                } else {
                    utils.Showalert();
                }
                // res= res.replaceAll("\\s+","");
            } catch (Exception e) {
               /* if (dia.isShowing())
                    dia.cancel();*/
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(final String result) {

            final DatabaseHandler db= new DatabaseHandler(VideoBinActivity.this,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);

         //   final DatabaseHandler db=new DatabaseHandler(VideoBinActivity.this);
            try {

                final JSONObject jObj = new JSONObject(result);
                String statusCode = jObj.getString("StatusCode");

                if (statusCode.toString().equalsIgnoreCase("200")){

                    new AsyncTask<Void,Void,Void>()
                    {

                        @Override
                        protected Void doInBackground(Void... params) {
                            try
                            {
                                JSONArray content = jObj.getJSONArray("contentDetails");
                                for (int c = 0; c < content.length(); c++) {
                                    Tablecontent g_content = new Tablecontent();
                                    JSONObject con = content.getJSONObject(c);
                                    //  g_content.setBatchID();

                                    g_content.setContentID(Integer.parseInt(con.getString("ID")));
                                    g_content.setContentFilename(con.getString("ContentFileName"));
                                    g_content.setContentCatalogType(con.getString("ContentType"));
                                    g_content.setContentDescription(con.getString("ContentTitle"));
                                    g_content.setVaporize(con.getString("Vaporize"));
                                    g_content.setSubject(con.getJSONArray("Subject").toString());
                                    g_content.setClasses(con.getJSONArray("Class").toString());

                                    g_content.setType(con.getString("Catalog"));


                                    String contentfilename = g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/") + 1, g_content.getContentFilename().length());
                                    Log.e("contentfile", contentfilename);

                                    if (con.getString("Catalog").equalsIgnoreCase("Academic")) {

                                        try {
                                            downloadfile(g_content.getContentFilename(), academicpath, contentfilename);

                                        } catch (Exception e) {
                                            Log.e("third", "third" + e.getMessage());
                                        }
                                        g_content.setContentFilename(academicpath + "/" + contentfilename/*g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/")+1,g_content.getContentFilename().length())*/);

                                    } else {
                                        Log.e("catlog", "reference");

                                        try {
                                            downloadfile(g_content.getContentFilename(), refpath, contentfilename);

                                        } catch (Exception e) {
                                            Log.e("video", "third" + e.getMessage());
                                        }

                                        g_content.setContentFilename(refpath + "/" + contentfilename/*g_content.getContentFilename().substring(g_content.getContentFilename().lastIndexOf("/")+1,g_content.getContentFilename().length())*/);

                                    }

                                    Cursor cursor=db.reriveformcontentid(g_content.getContentID());
                                    if(cursor.getCount()>0) {
                                        db.updatecontent(g_content);
                                    }
                                    else {
                                        db.content(g_content);
                                    }
                                    // db.content(g_content);
                                }

                            }
                            catch (Exception e)
                            {

                            }
                            return null;
                        }

                        @Override
                        protected void onPostExecute(Void aVoid) {
                            super.onPostExecute(aVoid);
                            if (dia.isShowing())
                                dia.cancel();
                            table=dp.getcontentdetails();

                            new validateUserTask2(result).execute("");

                        }
                    }.execute();


                }
                else
                {
                    if (dia.isShowing())
                        dia.cancel();
                    Toast.makeText(VideoBinActivity.this,result,Toast.LENGTH_SHORT).show();
                }
            }
            catch(Exception e){
                if (dia.isShowing())
                    dia.cancel();
            }
         //   downloadSuccess("");

        }
    }


    private class validateUserTask2 extends AsyncTask<String, Void, String> {
        String response,statusres;

        ProgressDialog dia;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dia=new ProgressDialog(VideoBinActivity.this);
            dia.setMessage("DOWNLOADING");
            dia.setCancelable(false);
            dia.show();}

        validateUserTask2(String statusres){
            this.statusres=statusres;
        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            String res = null;


            try {
                JSONObject jObj = new JSONObject(statusres);

                String statusCode = jObj.getString("StatusCode");
                if (statusCode.equalsIgnoreCase("200")) {


                    ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();

                    String userid = "Function:SyncComplete|StaffId:" +pref.getString("staffid", "0")+ "|Portal User ID:" + pref.getString("portalstaffid", "0") +"|Type:media";


                    byte[] data;


                    data = userid.getBytes("UTF-8");
                    String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                    if (utils.hasConnection()) {
                        postParameters.clear();
                        postParameters.add(new BasicNameValuePair("WS", base64_register));
                        Service sr = new Service(VideoBinActivity.this);

                        res = sr.getLogin(postParameters,
                                Url.baseurl);
                        // response = CustomHttpClient.executeHttpPost(params[0], postParameters);
                        res = res.toString();
                        Log.e("response", res + ",,");
                        //  return res;
                    } else {
                        utils.Showalert();
                    }
                    // res= res.replaceAll("\\s+","");

                } else {
                    Log.e("error", "ss");
                }

            }
            catch(Exception e){
                if(dia.isShowing())
                    dia.cancel();
                // txt_Error.setText(e.toString());
                Log.e("Exception", e.toString());
            }


            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(dia.isShowing())
                dia.cancel();
            try {
                JSONObject jObj = new JSONObject(result);
                if(jObj.getString("StatusCode").equals("200")) {
                    downloadSuccess("References data downloaded successfully.");
                  //  Toast.makeText(getApplicationContext(), "References data downloaded successfully.", Toast.LENGTH_LONG).show();
                }
                else {
                    downloadSuccess("Downloading data failed.");
                   // Toast.makeText(getApplicationContext(), "Downloading data failed.", Toast.LENGTH_LONG).show();

                }
            }
            catch (Exception e)
            {

            }


        }//close onPostExecute
    }// close validateUserTask




    private void bookbinload() throws Exception {}

    public void Search_Dir(File dir) {

        String videopatten2 = ".pdf";

        //String videopatten1 = ".wav";
      /*  String videopatten = ".pdf";
        String videopatten = ".pdf";
*/
        Log.d("check",
                "Environment.getExternalStorageDirectory()------"
                        + dir.getName());

        File FileList[] = dir.listFiles();
        Log.d("check",
                "filelist length---- "
                        + FileList.length);

        if (FileList != null) {
            for (int i = 0; i < FileList.length; i++) {

                if (FileList[i].isDirectory()) {
                    Search_Dir(FileList[i]);
                } else {

                    Log.d("check",
                            "for check from .pdf---- "
                                    + FileList[i].getName());
                    if (FileList[i].getName().endsWith(videopatten2)) {
                        // here you have that file.
                        // pdfArrayList.add(FileList[i].getPath());
                        // Videoname video=new Videoname(FileList[i].getPath(),FileList[i].getName(),false);

                        //videos.add(video);
                    }
                }
            }
        }

    }

    void loadbookshelf(int show)
    {
        hlvCustomList.removeAllViews();
        for(int i=0; i<subjectname.size(); i++)
        {
            View hiddenInfo = getLayoutInflater().inflate(
                    R.layout.bookself, hlvCustomList, false);
            final TextView bookshelftext=(TextView) hiddenInfo.findViewById(R.id.shelftext);
            bookshelftext.setText(subjectname.get(i).getName());
            bookshelftext.setTag(i);
            bookshelftext.setTextAppearance(this,R.style.Custom_medium);
           // bookshelftext.setTextSize(TypedValue.COMPLEX_UNIT_SP,R.dimen.);
            utils.setTextviewtypeface(5,bookshelftext);
            bookshelftext.setTextColor(getResources().getColor(R.color.black));
            if(show==i)
            {


                hiddenInfo.setBackgroundResource(R.drawable.subject_bg);
                bookshelftext.setTextColor(getResources().getColor(R.color.white));
            }


            hiddenInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    videos.clear();
                    int tag = Integer.parseInt(bookshelftext.getTag().toString());

                    for (int i = 0; i < table.size(); i++) {

                        String subject = table.get(i).getSubject();
                        String subarray = subject;
                        String classes = table.get(i).getClasses();

                        String classessarray = classes;
                        classessarray = classessarray.replaceAll("\\[", "").replaceAll("\\]", "");
                        classessarray = classessarray.replace("\"", "");
                        String csplit[] = classessarray.split(",");
                        if (!pref.getBoolean("roomornot", false)) {
                            boolean classefound = false;
                            try {
                                for (int c = 0; c < csplit.length; c++) {
                                    if (Integer.parseInt(csplit[c]) == Integer.parseInt(pref.getString("bookbin", "0"))) {
                                        classefound = true;
                                        break;
                                    }
                                }
                            } catch (Exception e) {

                            }
                            if (classefound) {
                                subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                                subarray = subarray.replace("\"", "");
                                String sSplit[] = subarray.split(",");
                                for (int s = 0; s < sSplit.length; s++) {
                                    if (sSplit[s].equalsIgnoreCase(subjectname.get(tag).getId())) {


                                        File file = new File(table.get(i).getContentFilename());
                                        String videopatten2 = ".pdf";
                                        if (!table.get(i).getType().equalsIgnoreCase("Academic")) {
                               /* if(table.get(i).getContentCatalogType().equalsIgnoreCase("videos")||table.get(i).getContentCatalogType().equalsIgnoreCase("images"))

                                { *///   if (file.getName().endsWith(videopatten2)) {
                                            Videoname video = new Videoname(file.getAbsolutePath(), file.getName(), false, subjectname.get(tag).getId(), table.get(i).getContentDescription(), "" + table.get(i).getContentID());

                                            videos.add(video);
                                        }
                                        // }
                                    }
                                }
                                //  subarray=String str = "[Chrissman-@1]";

                            }
                        }
                        else
                        {
                            subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                            subarray = subarray.replace("\"", "");
                            String sSplit[] = subarray.split(",");
                            for (int s = 0; s < sSplit.length; s++) {
                                if (sSplit[s].equalsIgnoreCase(subjectname.get(tag).getId())) {


                                    File file = new File(table.get(i).getContentFilename());
                                    String videopatten2 = ".pdf";
                                    if (!table.get(i).getType().equalsIgnoreCase("Academic")) {
                               /* if(table.get(i).getContentCatalogType().equalsIgnoreCase("videos")||table.get(i).getContentCatalogType().equalsIgnoreCase("images"))

                                { *///   if (file.getName().endsWith(videopatten2)) {
                                        Videoname video = new Videoname(file.getAbsolutePath(), file.getName(), false, subjectname.get(tag).getId(), table.get(i).getContentDescription(), "" + table.get(i).getContentID());

                                        videos.add(video);
                                    }
                                    // }
                                }
                            }
                        }
                    }

                    dataAdapter = new Videoadapter(VideoBinActivity.this,
                            R.layout.booklistitem, videos);
                    listView.setAdapter(dataAdapter);

                    Log.d("videodata", "onClick: "+videos.size());

                    if(videos.size()>0)
                    {

                    }
                    else
                    {
                        Toast.makeText(getApplication(), "NO CONTENT FOUND IN " + subjectname.get(tag).getName().toUpperCase(), Toast.LENGTH_SHORT).show();
                    }


                    //  dataAdapter.notifyDataSetChanged();
                    loadbookshelf(Integer.parseInt(bookshelftext.getTag().toString()));


                }

            });
          /*  if(i==0)
            {
            hiddenInfo.performClick();
            }*/
            hlvCustomList.addView(hiddenInfo);

        }



    }
    void loadinit()
    {

        videos.clear();
        for (int i = 0; i < table.size(); i++) {

            String subject = table.get(i).getSubject();
            String subarray = subject;
            String classes=table.get(i).getClasses();

            String classessarray=classes;
            classessarray = classessarray.replaceAll("\\[", "").replaceAll("\\]", "");
            classessarray = classessarray.replace("\"", "");
            String csplit[] = classessarray.split(",");
            if (!pref.getBoolean("roomornot", false)) {
                boolean classefound = false;
                try {


                    for (int c = 0; c < csplit.length; c++) {
                        if (Integer.parseInt(csplit[c]) == Integer.parseInt(pref.getString("bookbin", "0"))) {
                            classefound = true;
                            break;
                        }
                    }
                } catch (Exception e) {

                }
                if (classefound) {
                    subarray = subarray.replaceAll("\\[", "").replaceAll("\\]", "");
                    subarray = subarray.replace("\"", "");
                    String sSplit[] = subarray.split(",");
                    for (int s = 0; s < sSplit.length; s++) {
                        //int tag = Integer.parseInt(subjectname.get(0).getName());
                        if (sSplit[s].equalsIgnoreCase(subjectname.get(0).getId())) {


                            File file = new File(table.get(i).getContentFilename());
                            // String videopatten2 = ".pdf";

                    /*if(table.get(i).getContentCatalogType().equalsIgnoreCase("videos")||table.get(i).getContentCatalogType().equalsIgnoreCase("images"))

                    {*/
                            if (!table.get(i).getType().equalsIgnoreCase("Academic")) {

                  /*  if (file.getName().endsWith(videopatten2)) {*/
                                Videoname video = new Videoname(file.getAbsolutePath(), file.getName(), false, subjectname.get(0).getId(), table.get(i).getContentDescription(), "" + table.get(i).getContentID());

                                videos.add(video);
                            }
                        }
                    }
                }
            }
                else
            {
                subarray = subarray.replace("\"", "");
                String sSplit[] = subarray.split(",");
                for (int s = 0; s < sSplit.length; s++) {
                    //int tag = Integer.parseInt(subjectname.get(0).getName());
                    if (sSplit[s].equalsIgnoreCase(subjectname.get(0).getId())) {


                        File file = new File(table.get(i).getContentFilename());
                        // String videopatten2 = ".pdf";

                    /*if(table.get(i).getContentCatalogType().equalsIgnoreCase("videos")||table.get(i).getContentCatalogType().equalsIgnoreCase("images"))

                    {*/
                        if (!table.get(i).getType().equalsIgnoreCase("Academic")) {

                  /*  if (file.getName().endsWith(videopatten2)) {*/
                            Videoname video = new Videoname(file.getAbsolutePath(), file.getName(), false, subjectname.get(0).getId(), table.get(i).getContentDescription(), "" + table.get(i).getContentID());

                            videos.add(video);
                        }
                    }
                }
            }
            //  subarray=String str = "[Chrissman-@1]";


        }
        dataAdapter = new Videoadapter(VideoBinActivity.this,
                R.layout.booklistitem, videos);
        listView.setAdapter(dataAdapter);

        if(videos.size()>0)
        {

        }
        else
        {
            Toast.makeText(getApplication(), "NO CONTENT FOUND IN " + subjectname.get(0).getName().toUpperCase(), Toast.LENGTH_SHORT).show();
        }


    }

   /*
    void loadbookself(int i)
    {


    }
    */


    public void stratExamCompletedPopup(final Videoname video) {
        final  Dialog  dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.filesharing);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView title= (TextView) dialog.findViewById(R.id.textView2);
        Button valuate = (Button) dialog.findViewById(R.id.button2);
        Button revisit = (Button) dialog.findViewById(R.id.button3);
        ImageView close= (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        title.setText("REFERENCE BOOK SHARING");
        revisit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                File file=new File(video.getVideopath());

                String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();
                new clientThread(MultiThreadChatServerSync.thread,senddata).start();

                if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                dialog.dismiss();
                // TODO Auto-generated method stub
                // dialog.dismiss();
            }
        });
        valuate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file=new File(video.getVideopath());
              //  File file=new File(video.getVideopath());

                if ( MainActivity.startshare == null) {
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();


                } else {
                    MainActivity.startshare.stopsocket();
                    MainActivity.startshare = null;
                    MainActivity.startshare = new Filesharingtoclient(file);
                    MainActivity.startshare.start();
                }

                String senddata="Filesharing"+"@"+file.getName()+"@"+file.length()+"@Reference"+"@"+video.getSubjectid()+"@"+video.getContentid()+"@"+video.getTitlename();

                Intent in=new Intent(VideoBinActivity.this, Attendancewithcheckparentlayout.class);
                in.putExtra("senddata",senddata);
                startActivity(in);
                dialog.dismiss();


            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if(pref.getBoolean("dnd",false))
        {
            dnd.setImageResource(R.drawable.active);

        }
        else
        {
            dnd.setImageResource(R.drawable.inactive);

        }
        if (LoginActivity.handraise!=null){
            if(LoginActivity.handraise.size()>0)
            {
                handraise.setImageResource(R.drawable.handraiseenable);

                // Utils.Listpopup(BookBinActivity.this);
            }
            else
            {                    handraise.setImageResource(R.drawable.handrise);

                //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
            }
        }



    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }


    void downloadfile(String urls,String filepath,String filename)
    {

        int count;
        try {
            //  String filename=urls.substring(urls.lastIndexOf("/")+1,urls.length());
            URL url = new URL(urls);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream

            OutputStream output = new FileOutputStream(filepath
                    + "/"+filename);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


    }

}




