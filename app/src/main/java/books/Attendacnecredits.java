package books;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import com.dci.edukool.teacher.R;

import java.util.ArrayList;

import Utilities.Attendancedb;
import Utilities.DatabaseHandler;
import helper.Studentdetails;

/**
 * Created by abimathi on 26-May-17.
 */
public class Attendacnecredits extends Activity {

    ListView attendancreditslist;
    TextView subjectname;
    ArrayList<Studentdetails> student;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    DatabaseHandler dp;
    Attendancedb db;
    int teachercredits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendancecreditlayour);
       // dp=new DatabaseHandler(this);
       // db=new Attendancedb(this);
        subjectname= (TextView) findViewById(R.id.creditsubject);
        attendancreditslist= (ListView) findViewById(R.id.creidtlist);


        teachercredits=db.gettotalcreditsforsubject(Integer.parseInt(pref.getString("batchid",
                "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                Integer.parseInt(pref.getString("staffid", "0")));


        student= dp.getdetails(Integer.parseInt(pref.getString("bookbin",
                "0")),  Integer.parseInt(pref.getString("staffid", "0")));

        for(int i=0; i<student.size(); i++)
        {
            student.get(i).setTeachercredits(teachercredits);
            int studentcredits=db.gettotalcreditsforsubjectstudent(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")),
                    Integer.parseInt(pref.getString("staffid", "0")),student.get(i).getStudentID());
            student.get(i).setStudentcredits(studentcredits);
        }
    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
}
