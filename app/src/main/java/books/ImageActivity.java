package books;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.dci.edukool.teacher.R;

import java.io.File;

import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;


/**
 * Created by kirubakaranj on 5/30/2017.
 */

public class ImageActivity extends Activity {

    String imagepath;
    ImageView image;
    ImageView close;
    ImageView sync;
    boolean videosyn;
    String data;
    ImageView pulse;
    boolean ac;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity);
        image=(ImageView)findViewById(R.id.image);
        close=(ImageView)findViewById(R.id.close);
        pref=getSharedPreferences("Teacher",MODE_PRIVATE);

        Intent i=getIntent();
        Bundle bun=i.getExtras();
        sync= (ImageView) findViewById(R.id.videsyn);
        ac=getIntent().getBooleanExtra("ac",false);


        imagepath= bun.getString("path");
        data=getIntent().getStringExtra("senddata");
        pulse= (ImageView) findViewById(R.id.pulse);
        pulse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ImageActivity.this, PulseQuestionActivity.class);
                startActivity(intent);
            }
        });

        sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!videosyn) {
                        new clientThread(MultiThreadChatServerSync.thread, data).start();

                        sync.setImageResource(R.drawable.unsync);
                        videosyn = true;

                    } else {
                        videosyn = false;
                        sync.setImageResource(R.drawable.bookbinsync);

                        new clientThread(MultiThreadChatServerSync.thread, "FileClose").start();

                    }
                }
                else
                {
                    Utilss.lockpoopup(ImageActivity.this, "EduKool", "You can access this feature once you come out of the session break.");

                }
            }
        });

        if (imagepath != null) setbackground(image, imagepath);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  change;
                if(ac)
                {
                    change="Academic Book";
                }
                else {
                    change="References";

                }
                if(!videosyn)
                    ImageActivity.this.finish();
                else
                {
                    Utilss.lockpoopup(ImageActivity.this, change, getResources().getString(R.string.whiteboardsync1));

                }
            }
        });

    }

    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float)255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }

    @Override
    public void onBackPressed() {
    }
}
