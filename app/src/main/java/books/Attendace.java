package books;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.artifex.mupdfdemo.AsyncTask;
import com.dci.edukool.teacher.BaseActivity;
import com.dci.edukool.teacher.CalendarCustomView;
import com.dci.edukool.teacher.LoginActivity;
import com.dci.edukool.teacher.R;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import Utilities.Utilss;
import connection.MultiThreadChatServerSync;
import connection.clientThread;
import helper.Attendancepojo;
import helper.RoundedImageView;
import Utilities.*;
import newfragments.FragmentAttendanceCapture;

/**
 * Created by iyyapparajr on 5/13/2017.
 */
public class Attendace extends BaseActivity

{
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    LinearLayout captureattendance, posttoserver, reports,container;
    TextView capturetext, reporttext, posttext;
    RoundedImageView profileimage;
    TextView batchname, attendance, title;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    TextView classname, bookheader;
    ImageView back;
    ImageView dnd;
    ImageView handraise;
    Attendancedb db;
    public Utilss utils;
    boolean posted;
    final CaldroidListener listener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
          /*  Toast.makeText(getApplicationContext(), formatter.format(date),
                    Toast.LENGTH_SHORT).show();*/
            if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                ArrayList<Attendancepojo> pojo = db.todayatttendance(Integer.parseInt(pref.getString("batchid", "0")),

                        Integer.parseInt(pref.getString("staffid", "0")),
                        formatter.format(date));


                posted = false;

                if (pojo.size() > 0) {
                    if (!pref.getBoolean("roomornot", false)) {
                        Intent in = new Intent(Attendace.this, Todayattendance.class);
                        in.putExtra("date", formatter.format(date));
                        startActivity(in);
                    } else {
                        Intent in = new Intent(Attendace.this, Todayattendanceforroom.class);
                        in.putExtra("date", formatter.format(date));
                        startActivity(in);
                    }
                } else {
                    Utilss.lockpoopup(Attendace.this, "Reports", "No Reports");
                }
            } else {
                breakmethod();


            }


        }

        @Override
        public void onChangeMonth(int month, int year) {
            String text = "month: " + month + " year: " + year;
/*
            Toast.makeText(getApplicationContext(), text,
                    Toast.LENGTH_SHORT).show();
*/
        }

        @Override
        public void onLongClickDate(Date date, View view) {
          /*  Toast.makeText(getApplicationContext(),
                    "Long click " + formatter.format(date),
                    Toast.LENGTH_SHORT).show();*/
        }

        @Override
        public void onCaldroidViewCreated() {
            /*Toast.makeText(getApplicationContext(),
                    "Caldroid view is created",
                    Toast.LENGTH_SHORT).show();*/
        }

    };
    NestedScrollView nestedscroll;
    CalendarCustomView calender_view;
    String login_str;
    String datetime;
    String shortTimeStr = new String();
    int credits;
    int isnew;
    Service sr;
    String jsonResponseString;
    ProgressDialog dia;
    ArrayList<NameValuePair> login = new ArrayList<NameValuePair>();
    private CaldroidFragment caldroidFragment;
    private Fragment fragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        title = (TextView) findViewById(R.id.title);
        title.setText("Attendance");
        container = (LinearLayout) findViewById(R.id.container);
        classname = (TextView)findViewById(R.id.class_name);
        nestedscroll = (NestedScrollView) findViewById(R.id.nestedscroll);
        nestedscroll.scrollTo(0,0);
        calender_view = (CalendarCustomView) findViewById(R.id.calender_view);
        utils = new Utilss(this);
        calender_view.setOnDateSelect(new OnDateSelect() {
            @Override
            public void OnDateSelect(Date date, int pos) {
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    ArrayList<Attendancepojo> pojo = db.todayatttendance(Integer.parseInt(pref.getString("batchid", "0")),

                            Integer.parseInt(pref.getString("staffid", "0")),
                            formatter.format(date));


                    posted = false;

                    if (pojo.size() > 0) {
                        if (!pref.getBoolean("roomornot", false)) {
                            Intent in = new Intent(Attendace.this, Todayattendance.class);
                            in.putExtra("date", formatter.format(date));
                            startActivity(in);
                        } else {
                            Intent in = new Intent(Attendace.this, Todayattendanceforroom.class);
                            in.putExtra("date", formatter.format(date));
                            startActivity(in);
                        }
                    } else {
                        Utilss.lockpoopup(Attendace.this, "Reports", "No Reports");
                    }
                } else {
                    breakmethod();
                }
            }
        });

        getSupportActionBar().hide();
        // db = new Attendancedb(this);
        sr = new Service(Attendace.this);
        //final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        profileimage = (RoundedImageView) findViewById(R.id.profileimage);
        batchname = (TextView) findViewById(R.id.textView);
        attendance = (TextView) findViewById(R.id.classname);
        captureattendance = (LinearLayout) findViewById(R.id.capturelinear);
        posttoserver = (LinearLayout) findViewById(R.id.postlinear);
        reports = (LinearLayout) findViewById(R.id.reportslinear);
        capturetext = (TextView) findViewById(R.id.capturetext);
        reporttext = (TextView) findViewById(R.id.reporttext);
        posttext = (TextView) findViewById(R.id.posttext);
        back = (ImageView) findViewById(R.id.back);
        handraise = (ImageView) findViewById(R.id.handrise);
        utils.setTextviewtypeface(3, title);

        handraise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (LoginActivity.handraise.size() > 0) {
                    Utilss.Listpopup(Attendace.this);
                    handraise.setImageResource(R.drawable.handraiseenable);
                } else {

                    Toast.makeText(Attendace.this, getResources().getString(R.string.hand), Toast.LENGTH_SHORT).show();
                }
            }
        });
        posttoserver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {


                    posted = false;
                    ArrayList<Attendancepojo> dates1 = db.uploadtoservertoall();

                    //   ArrayList<Attendancepojo> dates1 = db.uploadtoserver(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")));

                    if (dates1.size() > 0) {
                        Utilss utils = new Utilss(Attendace.this);
                        if (utils.hasConnection()) {

                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected void onPreExecute() {
                                    super.onPreExecute();
                                    dia = new ProgressDialog(Attendace.this);
                                    dia.setMessage("UPLOADING");
                                    dia.setCancelable(false);
                                    dia.show();
                                }

                                @Override
                                protected Void doInBackground(Void... params) {

                                    //   ArrayList<Attendancepojo> dates = db.uploadtoserver(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")));

                                    ArrayList<Attendancepojo> dates = db.uploadtoservertoall();

                                    for (Attendancepojo pojo : dates) {
//starthere

                                        ArrayList<Attendancepojo> classid = db.uploadtoserver(pojo.getAttendanceDateTime());

                                        String subjectid = "";
                                        for (Attendancepojo classes : classid) {

                                            ArrayList<Attendancepojo> studentdetails = db.getuploadattendforstudents(pojo.getAttendanceDateTime(), classes.getClassid());
                                            final ArrayList<Integer> present = new ArrayList<Integer>();
                                            final ArrayList<Integer> absent = new ArrayList<Integer>();
                                            credits = 0;
                                            int i = 0;
                                            Attendancepojo getstudent = null;
                                            String presentstr = null;
                                            String absentstr = null;
                                            for (Attendancepojo student : studentdetails) {
                                                getstudent = student;

                                                if (i == 0) {
                                                    datetime = student.getAttendanceDateTime();
                                                    credits = student.getCredits();
                                                    isnew = student.getIsnew();
                                                    subjectid = "" + student.getSubjectID();
                                                    i = 1;
                                                }

                                                if (student.getAttendance().equalsIgnoreCase("P")) {
                                                    present.add(student.getStudentID());
                                                } else {
                                                    absent.add(student.getStudentID());
                                                }


                                                try {
                                                    Date date = null;
                                                    SimpleDateFormat toFullDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                    date = toFullDate.parse(datetime);
                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                                                    shortTimeStr = sdf.format(date);
                                                    // text.setText(shortTimeStr);
                                                    System.out.println(shortTimeStr);
                                                } catch (Exception e) {
                                                    // To change body of catch statement use File | Settings | File Templates.
                                                    e.printStackTrace();
                                                }

                                            }
                                            if (present.size() > 0) {

                                            } else {
                                                present.add(0);
                                            }
                                            presentstr = present.toString();
                                            presentstr = presentstr.replaceAll("\\[", "");
                                            presentstr = presentstr.replaceAll("\\]", "");

                                            if (absent.size() > 0) {

                                            } else {
                                                absent.add(0);
                                            }

                                            absentstr = absent.toString();
                                            absentstr = absentstr.replaceAll("\\[", "");
                                            absentstr = absentstr.replaceAll("\\]", "");

                                            login_str = "StaffID:" + pref.getString("staffid", "") +
                                                    "|ClassID:" + getstudent.getClassid() +
                                                    "|BatchID:" + getstudent.getBatchID() +
                                                    "|AttendanceDateTime:" + shortTimeStr +
                                                    "|Credits:" + "" + credits +
                                                    "|isNew:" + "" + isnew +
                                                    "|StudentPresent:" + presentstr +
                                                    "|StudentAbsent:" + absentstr +
                                                    "|Portal User ID:" + pref.getString("portalstaffid", "") +
                                                    "|Subject:" + subjectid/*pref.getString("subjectid", "")*/ + "|Function:SyncAttendance";


                                            try {// login.clear();
                                                byte[] data;

                                                data = login_str.getBytes("UTF-8");
                                                String base64_register = Base64.encodeToString(data, Base64.DEFAULT);
                                                login.clear();
                                                login.add(new BasicNameValuePair("WS", base64_register));

                                                jsonResponseString = sr.getLogin(login,
                                                        Url.baseurl);


                                                JSONObject obj = new JSONObject(jsonResponseString);

                                                if (obj.getString("StatusCode").equalsIgnoreCase("200")) {
                                                    posted = true;
                                                    db.updatepostedattendance(datetime);
                                                    //  db.updatepostedattendance(Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("subjectid", "0")), datetime);
                                                }


                                            } catch (Exception e) {
                                                Log.e("Exception ", "exception" + e.getMessage());
                                            }
                                        }

                                    }
                                    // }

                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    try {
                                        if (dia.isShowing())
                                            dia.dismiss();

                                        if (posted) {
                                            Dialog dialog = new Dialog(Attendace.this);
                                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            dialog.setContentView(R.layout.layout_download_success);
                                            dialog.setCancelable(false);

                                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                                            TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);
                                            TextView logout = (TextView) dialog.findViewById(R.id.logout);
                                            TextView textView5 = (TextView) dialog.findViewById(R.id.textView5);
                                            TextView textView6 = (TextView) dialog.findViewById(R.id.textView6);
                                            utils.setTextviewtypeface(5,ok_button);
                                            utils.setTextviewtypeface(5,logout);
                                            utils.setTextviewtypeface(3,textView5);
                                            utils.setTextviewtypeface(5,textView6);
                                            textView6.setText("Sent to Server Successfully");

                                            ok_button.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            dialog.setCanceledOnTouchOutside(false);
                                            dialog.show();
                                        } else {

                                            Toast.makeText(Attendace.this, "Upload failed please try again after sometime", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (Exception e) {

                                    }
                                }
                            }.execute();

                        } else {
                            utils.Showalert();

                        }
                    } else {
                        Toast.makeText(Attendace.this, "NO DATA TO SEND SERVER", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    breakmethod();
                }
            }
        });

        dnd = (ImageView) findViewById(R.id.dnd);
        dnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ((!pref.getBoolean("break", false))) {


                    if (pref.getBoolean("dnd", false)) {
                        dnd.setImageResource(R.drawable.dci);
                        edit.putBoolean("dnd", false);
                        edit.commit();

                        String senddata = "DND";

                        senddata = senddata + "@false";
                        dnd.setImageResource(R.drawable.inactive);


                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();


                    } else {
                        String senddata = "DND";

                        senddata = senddata + "@true";
                        edit.putBoolean("dnd", true);
                        edit.commit();
                        dnd.setImageResource(R.drawable.active);

                        //  dnd=dnd+1;
                        new clientThread(MultiThreadChatServerSync.thread, senddata).start();
                    }

                } else {
                    Toast.makeText(Attendace.this, "You can access this feature once you come out of the session break.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        pref = getSharedPreferences("Teacher", MODE_PRIVATE);
        edit = pref.edit();

        db = new Attendancedb(this, pref.getString("attendancedb", ""), Attendancedb.DATABASE_VERSION);

        setbackground(profileimage, pref.getString("image", ""));
        batchname.setText(pref.getString("classname", ""));
        classname.setText(pref.getString("classname", ""));

        // Setup caldroid fragment
        // **** If you want normal CaldroidFragment, use below line ****
        caldroidFragment = new CaldroidFragment();


        // //////////////////////////////////////////////////////////////////////
        // **** This is to show customized fragment. If you want customized
        // version, uncomment below line ****
//		 caldroidFragment = new CaldroidSampleCustomFragment();

        // Setup arguments

        // If Activity is created after rotation
        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        }
        // If activity is created from fresh
        else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);

            // Uncomment this to customize startDayOfWeek
            // args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
            // CaldroidFragment.TUESDAY); // Tuesday

            // Uncomment this line to use Caldroid in compact mode
            // args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

            // Uncomment this line to use dark theme
//            args.putInt(CaldroidFragment.THEME_RESOURCE, com.caldroid.R.style.CaldroidDefaultDark);

            caldroidFragment.setArguments(args);
        }

        setCustomResourceForDates();

        // Attach to the activity
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calender1, caldroidFragment);
        t.commit();
        captureattendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!pref.getBoolean("roomornot", false)) {
                      //  calender_view.setVisibility(View.GONE);
//                        container.setVisibility(View.VISIBLE);
//                        setFragment(new FragmentAttendanceCapture());


                        startActivity(new Intent(Attendace.this, Attendacecapture.class));
                    } else {
                        startActivity(new Intent(Attendace.this, Attendanceforroom.class));

                    }
                } else {
                    breakmethod();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragment!=null){
                    if (fragment instanceof FragmentAttendanceCapture){
                        calender_view.setVisibility(View.VISIBLE);
                        container.setVisibility(View.GONE);
                    }
                    else {
                        finish();
                    }
                }
                else {
                    finish();
                }

            }
        });
        caldroidFragment.setCaldroidListener(listener);

        reports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!pref.getString("roomname", "").equalsIgnoreCase("Session Break")) {

                    if (!pref.getBoolean("roomornot", false)) {
                       // setFragment(new FragmentAttendanceCredits());

                       startActivity(new Intent(Attendace.this, attendancecredits.class));
                    } else {
                        startActivity(new Intent(Attendace.this, Roomattendancecredits.class));

                    }
                } else {
                    breakmethod();
                }

            }
        });
        utils.setTextviewtypeface(3, title);
        nestedscroll.scrollTo(0, 0);

    }

    @Override
    public int getlayout() {
        return R.layout.attendancemainlayout;
    }

    //caldroidFragment.setCaldroidListener(listener);

    private void setCustomResourceForDates() {
        Calendar cal = Calendar.getInstance();

        // Min date is last 7 days
        cal.add(Calendar.DATE, -7);
        Date blueDate = cal.getTime();

        // Max date is next 7 days
        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 7);
        Date greenDate = cal.getTime();

        if (caldroidFragment != null) {
            ColorDrawable blue = new ColorDrawable(getResources().getColor(R.color.oldbg));
            ColorDrawable green = new ColorDrawable(Color.GREEN);
            caldroidFragment.setBackgroundDrawableForDate(blue, blueDate);
            caldroidFragment.setBackgroundDrawableForDate(green, greenDate);

            caldroidFragment.setTextColorForDate(R.color.white, blueDate);
            caldroidFragment.setTextColorForDate(R.color.white, greenDate);
        }
    }

    void setbackground(ImageView view, String filepath) {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

                /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
                 */
                view.setImageBitmap(myBitmap);

            }
        } catch (Exception e) {

        }
    }
    public void setFragment(Fragment fragments) {
        fragment = fragments;
        if (fragment != null) {

            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, "About Us");



            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        float curBrightnessValue = 0;

        try {
            curBrightnessValue = android.provider.Settings.System.getInt(
                    getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        float brightness = curBrightnessValue / (float) 255;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (pref.getBoolean("dnd", false)) {
            dnd.setImageResource(R.drawable.active);

        } else {
            dnd.setImageResource(R.drawable.inactive);

        }
        if (LoginActivity.handraise != null) {
            if (LoginActivity.handraise.size() > 0) {
                handraise.setImageResource(R.drawable.handraiseenable);

                // Utils.Listpopup(BookBinActivity.this);
            } else {
                handraise.setImageResource(R.drawable.handrise);

                //   Toast.makeText(BookBinActivity.this,getResources().getString(R.string.hand),Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    void breakmethod() {
        Utilss.lockpoopup(Attendace.this, "EduKool", "You can access this feature once you come out of the session break.");

    }

}
