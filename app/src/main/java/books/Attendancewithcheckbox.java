package books;

/**
 * Created by abimathi on 15-May-17.
 */
//Attendancewithcheckbox

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.edukool.teacher.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import Utilities.DatabaseHandler;
import Utilities.Utilss;
import helper.Studentdetails;
import helper.Studentinfo;

/**
 * Created by iyyapparajr on 4/6/2017.
 */
public class Attendancewithcheckbox extends ArrayAdapter<Studentinfo> {

    private ArrayList<Studentinfo> studentinfo;
    Context con;
    SharedPreferences pref;
    DatabaseHandler db;
    Utilss utils;
    // CoolReader mactivity;
  public static HashMap<Integer, Studentinfo> send=new HashMap<>();
  //  public static ArrayList<Studentinfo> studentinfo_send;
    public Attendancewithcheckbox(Context context, int textViewResourceId,
                             ArrayList<Studentinfo> studentinfo,SharedPreferences pref) {
        super(context, textViewResourceId, studentinfo);
        this.studentinfo = new ArrayList<Studentinfo>();
       // studentinfo_send=new ArrayList<Studentinfo>();
        this.pref=pref;
        utils=new Utilss((Activity) context);
        con=context;
        send.clear();
        this.studentinfo.addAll(studentinfo);
       // db=new DatabaseHandler((Activity)con);
        db=new DatabaseHandler((Activity)con,pref.getString("staffdbname",""),DatabaseHandler.DATABASE_VERSION);
    }

    private class ViewHolder {
        ImageView studentimage;
        TextView studenttext;
        CheckBox check;
        RelativeLayout parentlayout;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        Log.v("ConvertView", String.valueOf(position));

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)con.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.takeattendancewithcheckbox, null);

            holder = new ViewHolder();
            holder.studentimage= (ImageView) convertView.findViewById(R.id.studentimage);
            holder.studenttext= (TextView) convertView.findViewById(R.id.studentname);
            holder.check= (CheckBox) convertView.findViewById(R.id.checkid);
            convertView.setTag(holder);
            utils.setTextviewtypeface(5,holder.studenttext);


        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            Studentdetails details = db.getdetails(Integer.parseInt(studentinfo.get(position).getStudentrollnumber()),

                    Integer.parseInt(pref.getString("schoolid", "0")),
                    Integer.parseInt(pref.getString("batchid", "0")), Integer.parseInt(pref.getString("staffid", "0")));


            holder.studenttext.setText(studentinfo.get(position).getStudentname());

            try {
                File imgfile=new File(details.getPhotoFilename());
                Picasso.with(con).load(imgfile).into(holder.studentimage);
            }
            catch (Exception e)
            {

            }
             //   setbackground(holder.studentimage, details.getPhotoFilename());

        }
        catch (Exception e)
        {

        }

holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
        {
            studentinfo.get(position).setSetchecked(isChecked);
            //HashMap<Integer,Studentinfo> in=new HashMap<Integer, Studentinfo>();
            //in.put(p4osition,studentinfo.get(position));
            send.put(position,studentinfo.get(position));

        }
        else
        {
            if(studentinfo.get(position).isSetchecked())
            {


                send.remove(position);
                studentinfo.get(position).setSetchecked(isChecked);

              /*  if(!send.isEmpty()){

                    Iterator it=hashmap.entrySet().iterator();

                    while(it.hasNext()){

                        Map.Entry obj=(Map.Entry) it.next();
                        System.out.print(obj.getKey()+" ");
                        System.out.println(obj.getValue());

                    }
                }
*/
            }
            //)
        }




    }
});

        //Toast.makeText(con,studentinfo.get(position).getStudentname(),Toast.LENGTH_SHORT).show();
        //Toast.makeText(con,details.getPhotoFilename(),Toast.LENGTH_SHORT).show();

        // holder.code.setText(" (" +  country.getCode() + ")");
      /*  holder.name.setText(videoname.getName());

        holder.name.setTag(videoname);
*/


        return convertView;

    }
    void setbackground(ImageView view,String filepath)
    {
        try {
            File imgFile = new File(filepath);
            if (imgFile.exists()) {

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

        /*ImageView myImage = (ImageView) findViewById(R.id.imageviewTest);
*/
                view.setImageBitmap(myBitmap);

            }
        }
        catch (Exception e)
        {

        }
    }

}
