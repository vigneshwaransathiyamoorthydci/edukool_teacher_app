package pulseexam;

/**
 * Created by pratheeba on 5/25/2017.
 */
public class PulseExamPojo {


    public int getPulseQuestionID() {
        return PulseQuestionID;
    }

    public void setPulseQuestionID(int pulseQuestionID) {
        PulseQuestionID = pulseQuestionID;
    }

    int PulseQuestionID ;
       String PulseQuestion ;
       String PulseType ;
       String AnswerOption_A;



    public String getPulseQuestion() {
        return PulseQuestion;
    }

    public void setPulseQuestion(String pulseQuestion) {
        PulseQuestion = pulseQuestion;
    }

    public String getPulseType() {
        return PulseType;
    }

    public void setPulseType(String pulseType) {
        PulseType = pulseType;
    }

    public String getAnswerOption_A() {
        return AnswerOption_A;
    }

    public void setAnswerOption_A(String answerOption_A) {
        AnswerOption_A = answerOption_A;
    }

    public String getAnswerOption_B() {
        return AnswerOption_B;
    }

    public void setAnswerOption_B(String answerOption_B) {
        AnswerOption_B = answerOption_B;
    }

    public String getAnswerOption_C() {
        return AnswerOption_C;
    }

    public void setAnswerOption_C(String answerOption_C) {
        AnswerOption_C = answerOption_C;
    }

    public String getAnswerOption_D() {
        return AnswerOption_D;
    }

    public void setAnswerOption_D(String answerOption_D) {
        AnswerOption_D = answerOption_D;
    }




    public String getCreatedOnPluse() {
        return CreatedOnPluse;
    }

    public void setCreatedOnPluse(String createdOnPluse) {
        CreatedOnPluse = createdOnPluse;
    }

    public String getUpdatedOn() {
        return UpdatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        UpdatedOn = updatedOn;
    }

    public PulseExamPojo(){

    }

    public PulseExamPojo(int PulseQuestionID, String PulseQuestion, String AnswerOption_A,String AnswerOption_B,String AnswerOption_C ,String AnswerOption_D,String CorrectAnswerOption,  String CreatedOnPluse,String UpdatedOn){
        this.PulseQuestionID = PulseQuestionID;
        this.PulseQuestion = PulseQuestion;
        this.AnswerOption_A = AnswerOption_A;
        this.AnswerOption_B = AnswerOption_B;
        this.AnswerOption_C = AnswerOption_C;
        this.AnswerOption_D = AnswerOption_D;
        this.CorrectAnswerOption = CorrectAnswerOption;
        this.CreatedOnPluse = CreatedOnPluse;
        this.UpdatedOn = UpdatedOn;

    }
    public PulseExamPojo(int PulseQuestionID, String PulseQuestion,String PulseType, String AnswerOption_A,String AnswerOption_B,String AnswerOption_C ,String AnswerOption_D,String CorrectAnswerOption,int VersionPulse,int  StaffIDPluse,int SchoolIDPulse,int IsActivePulse,String CreatedOnPluse,String UpdatedOn){
        this.PulseQuestionID = PulseQuestionID;
        this.PulseQuestion = PulseQuestion;
        this.PulseType = PulseType;
        this.AnswerOption_A = AnswerOption_A;
        this.AnswerOption_B = AnswerOption_B;
        this.AnswerOption_C = AnswerOption_C;
        this.AnswerOption_D = AnswerOption_D;
        this.CorrectAnswerOption = CorrectAnswerOption;
        this.VersionPulse = VersionPulse;
        this.StaffIDPluse = StaffIDPluse;
        this.SchoolIDPulse = SchoolIDPulse;
        this.IsActivePulse = IsActivePulse;
        this.CreatedOnPluse = CreatedOnPluse;
        this.UpdatedOn = UpdatedOn;

    }


    public PulseExamPojo(int PulseQuestionID, String PulseQuestion,String PulseType, String AnswerOption_A,String AnswerOption_B,String AnswerOption_C ,String CorrectAnswerOption,int VersionPulse,int  StaffIDPluse,int SchoolIDPulse,int IsActivePulse,String CreatedOnPluse,String UpdatedOn){
        this.PulseQuestionID = PulseQuestionID;
        this.PulseQuestion = PulseQuestion;
        this.PulseType = PulseType;
        this.AnswerOption_A = AnswerOption_A;
        this.AnswerOption_B = AnswerOption_B;
        this.AnswerOption_C = AnswerOption_C;
        this.CorrectAnswerOption = CorrectAnswerOption;
        this.VersionPulse = VersionPulse;
        this.StaffIDPluse = StaffIDPluse;
        this.SchoolIDPulse = SchoolIDPulse;
        this.IsActivePulse = IsActivePulse;
        this.CreatedOnPluse = CreatedOnPluse;
        this.UpdatedOn = UpdatedOn;

    }

    public PulseExamPojo(int PulseQuestionID, String PulseQuestion,String PulseType, String AnswerOption_A,String AnswerOption_B,String CorrectAnswerOption,int VersionPulse,int  StaffIDPluse,int SchoolIDPulse,int IsActivePulse,String CreatedOnPluse,String UpdatedOn){
        this.PulseQuestionID = PulseQuestionID;
        this.PulseQuestion = PulseQuestion;
        this.PulseType = PulseType;
        this.AnswerOption_A = AnswerOption_A;
        this.AnswerOption_B = AnswerOption_B;
        this.CorrectAnswerOption = CorrectAnswerOption;
        this.VersionPulse = VersionPulse;
        this.StaffIDPluse = StaffIDPluse;
        this.SchoolIDPulse = SchoolIDPulse;
        this.IsActivePulse = IsActivePulse;
        this.CreatedOnPluse = CreatedOnPluse;
        this.UpdatedOn = UpdatedOn;

    }
    String AnswerOption_B;
       String AnswerOption_C ;
       String AnswerOption_D;


    public String getCorrectAnswerOption() {
        return CorrectAnswerOption;
    }

    public void setCorrectAnswerOption(String correctAnswerOption) {
        CorrectAnswerOption = correctAnswerOption;
    }

    String CorrectAnswerOption ;
       int VersionPulse ;

    public int getVersionPulse() {
        return VersionPulse;
    }

    public void setVersionPulse(int versionPulse) {
        VersionPulse = versionPulse;
    }

    public int getIsActivePulse() {
        return IsActivePulse;
    }

    public void setIsActivePulse(int isActivePulse) {
        IsActivePulse = isActivePulse;
    }

    public int getSchoolIDPulse() {
        return SchoolIDPulse;
    }

    public void setSchoolIDPulse(int schoolIDPulse) {
        SchoolIDPulse = schoolIDPulse;
    }

    public int getStaffIDPluse() {
        return StaffIDPluse;
    }

    public void setStaffIDPluse(int staffIDPluse) {
        StaffIDPluse = staffIDPluse;
    }

    int StaffIDPluse;
       int SchoolIDPulse ;
       int IsActivePulse ;
       String CreatedOnPluse ;
       String UpdatedOn ;

}
