package pulseexam;

/**
 * Created by pratheeba on 5/30/2017.
 */
public class PulseSurveyPojo {

    public  PulseSurveyPojo() {

    }
    public PulseSurveyPojo(int PulseSurveyID, int PulseQuestionID,int ContentID, int BatchID,int SubjectID,int StaffID ,String PulseDatetime,
                           int PageNumber,int Posted,int  optionA_Percentage,int optionB_Percentage,
                           int optionC_Percentage,int optionD_Percentage,int NoResponse_Percentage,int NoOfPresentees,
                           String CreatedOn,
                           String ModifiedOn
    ){
        this.PulseSurveyID = PulseSurveyID;
        this.PulseQuestionID = PulseQuestionID;
        this.ContentID = ContentID;
        this.BatchID = BatchID;
        this.SubjectID = SubjectID;
        this.StaffID = StaffID;
        this.PulseDatetime = PulseDatetime;
        this.PageNumber = PageNumber;
        this.Posted = Posted;
        this.optionA_Percentage = optionA_Percentage;
        this.optionB_Percentage = optionB_Percentage;
        this.optionC_Percentage = optionC_Percentage;
        this.optionD_Percentage = optionD_Percentage;
        this.NoResponse_Percentage = NoResponse_Percentage;
        this.NoOfPresentees = NoOfPresentees;
        this.CreatedOn = CreatedOn;
        this.ModifiedOn = ModifiedOn;

    }


    int PulseSurveyID;
            int PulseQuestionID;
             int ContentID;
            int BatchID;
            int SubjectID;
            int StaffID;
            String PulseDatetime;
            int PageNumber;
            int Posted;
            float optionA_Percentage;
            float optionB_Percentage;
            float optionC_Percentage;
            float optionD_Percentage;

    public int getPulseSurveyID() {
        return PulseSurveyID;
    }

    public void setPulseSurveyID(int pulseSurveyID) {
        PulseSurveyID = pulseSurveyID;
    }

    public int getPulseQuestionID() {
        return PulseQuestionID;
    }

    public void setPulseQuestionID(int pulseQuestionID) {
        PulseQuestionID = pulseQuestionID;
    }

    public int getContentID() {
        return ContentID;
    }

    public void setContentID(int contentID) {
        ContentID = contentID;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public int getStaffID() {
        return StaffID;
    }

    public void setStaffID(int staffID) {
        StaffID = staffID;
    }

    public String getPulseDatetime() {
        return PulseDatetime;
    }

    public void setPulseDatetime(String pulseDatetime) {
        PulseDatetime = pulseDatetime;
    }

    public int getPageNumber() {
        return PageNumber;
    }

    public void setPageNumber(int pageNumber) {
        PageNumber = pageNumber;
    }

    public int getPosted() {
        return Posted;
    }

    public void setPosted(int posted) {
        Posted = posted;
    }

    public float getOptionA_Percentage() {
        return optionA_Percentage;
    }

    public void setOptionA_Percentage(float optionA_Percentage) {
        this.optionA_Percentage = optionA_Percentage;
    }

    public float getOptionB_Percentage() {
        return optionB_Percentage;
    }

    public void setOptionB_Percentage(float optionB_Percentage) {
        this.optionB_Percentage = optionB_Percentage;
    }

    public float getOptionC_Percentage() {
        return optionC_Percentage;
    }

    public void setOptionC_Percentage(float optionC_Percentage) {
        this.optionC_Percentage = optionC_Percentage;
    }

    public float getOptionD_Percentage() {
        return optionD_Percentage;
    }

    public void setOptionD_Percentage(float optionD_Percentage) {
        this.optionD_Percentage = optionD_Percentage;
    }

    public float getNoResponse_Percentage() {
        return NoResponse_Percentage;
    }

    public void setNoResponse_Percentage(float noResponse_Percentage) {
        NoResponse_Percentage = noResponse_Percentage;
    }

    public int getNoOfPresentees() {
        return NoOfPresentees;
    }

    public void setNoOfPresentees(int noOfPresentees) {
        NoOfPresentees = noOfPresentees;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    float NoResponse_Percentage;
            int NoOfPresentees;
            String CreatedOn;
            String ModifiedOn;

}
