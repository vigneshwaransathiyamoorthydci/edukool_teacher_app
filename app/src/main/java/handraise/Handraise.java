package handraise;

/**
 * Created by abimathi on 22-May-17.
 */
public class Handraise {

    String Studentname;
    String Studentrollnumber;
String ip;
    String studentid;

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getStudentname() {
        return Studentname;
    }

    public void setStudentname(String studentname) {
        Studentname = studentname;
    }

    public String getStudentrollnumber() {
        return Studentrollnumber;
    }

    public void setStudentrollnumber(String studentrollnumber) {
        Studentrollnumber = studentrollnumber;
    }
}
